#pragma once


// CGameDialog 对话框

class CGameDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CGameDialog)

public:
	CGameDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CGameDialog();

// 对话框数据
	enum { IDD = IDD_GAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnLoadFile();

private:
	lua_State *m_luaState;
public:
	virtual BOOL OnInitDialog();
	CString m_strString;
protected:
	afx_msg LRESULT OnMsg(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
