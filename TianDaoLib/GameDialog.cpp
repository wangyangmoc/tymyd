// GameDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "TianDaoLib.h"
#include "GameDialog.h"
#include "afxdialogex.h"
#include <string>

extern std::string g_wgDir;
//#include "lua_helper.h"

// CGameDialog 对话框
//来自于脚本交互

IMPLEMENT_DYNAMIC(CGameDialog, CDialogEx)

CGameDialog::CGameDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGameDialog::IDD, pParent)
	, m_strString(_T(""))
{
}

CGameDialog::~CGameDialog()
{
	//lua_close(m_luaState);
}

void CGameDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strString);
}


BEGIN_MESSAGE_MAP(CGameDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_LOAD_FILE, &CGameDialog::OnBnClickedBtnLoadFile)
	ON_MESSAGE(WM_MSG, &CGameDialog::OnMsg)
	ON_BN_CLICKED(IDC_BUTTON1, &CGameDialog::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CGameDialog::OnBnClickedButton2)
END_MESSAGE_MAP()


// CGameDialog 消息处理程序

void CGameDialog::OnBnClickedBtnLoadFile()
{
	// TODO:  在此添加控件通知处理程序代码
	if (g_bSysFlag)
	{
		AfxMessageBox(_T("代码正在执行.."));
		return;
	}
	CString strFileName;
	GetDlgItem(IDC_EDIT_FILE)->GetWindowText(strFileName);
	CStringA str(strFileName);
	strcpy_s(g_strCmd, "runfile");
	strcpy_s(g_strArg, (LPCSTR)str);
	g_bSysFlag = true;
// 	if (0 != luaL_dofile(m_luaState, (LPCSTR)str))
// 	{
// 		PRT("LoadFile err:%s", lua_tostring(m_luaState, -1));
// 	}
}


BOOL CGameDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	// TODO:  在此添加额外的初始化
	g_hMsgWnd = this->GetSafeHwnd();
#if 0//
	m_luaState = luaL_newstate();
	luaL_openlibs(m_luaState);
#if LUA_VERSION_NUM == 501
	InitLuaHelper(m_luaState);
#else
	luaL_requiref(m_luaState, "msk", InitLuaHelper, 0);
#endif
	lua_pop(m_luaState, 1);  /* remove lib */
#endif
	std::string strTestFile = R"(GameTest)";//g_wgDir;
	//strTestFile += R"(\..\UserScript\GameTest.lua)";
	::SetWindowTextA(GetDlgItem(IDC_EDIT_FILE)->GetSafeHwnd(), strTestFile.c_str());
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


afx_msg LRESULT CGameDialog::OnMsg(WPARAM wParam, LPARAM lParam)
{
	UpdateData(TRUE);
	auto msg = (const char *)(wParam);
	m_strString += msg;
	m_strString += "\r\n";
	UpdateData(FALSE);
	return 0;
}


void CGameDialog::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	strcpy_s(g_strCmd, "stopall");
	strcpy_s(g_strArg, "~");
	g_bSysFlag = true;
}


void CGameDialog::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	strcpy_s(g_strCmd, "pause");
	strcpy_s(g_strArg, "~");
	g_bSysFlag = true;
}
