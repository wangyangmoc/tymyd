#include "stdafx.h"
#include "../macro/macro.h"
extern std::string g_wgDir;

//namespace {
	using namespace macro;
	DWORD _CreateFileW = (DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW") + 5;
	HANDLE
	WINAPI
	MyCreateFileW(
			LPCWSTR lpFileName, 
			DWORD dwDesiredAccess,
			DWORD dwShareMode, 
			LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
			DWORD dwCreationDisposition,
			DWORD dwFlagsAndAttributes, 
			HANDLE hTemplateFile)
	{
		__asm jmp _CreateFileW
	}
	DWORD trueSendData = 0x5C04F370;
	DWORD trueSendDataNext = 0x5C04F370;
	__declspec(naked) void SendDataJmp()
	{
		__asm
		{
			nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				jmp trueSendDataNext
		}
	}
	
	void WINAPI PrintSendData(DWORD head, DWORD data, DWORD len)
	{
		//PRT("PrintSendData:%x-%d", data, len);
		//return;
		auto &L = g_gameLuaState;
		if (L != NULL){
			lua_getglobal(L, "OnGameSend");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, head);
				lua_pushnumber(L, (DWORD)data);
				lua_pushnumber(L, len);
				if (0 != lua_pcall(L, 3, 0, 0)){
					PRT("OnSend error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				PRT("OnSend type error :%s", luaL_typename(L, -1));
				lua_pop(L, 1);
			}
		}
		return;
	}
	/*
	+4 head
	+8 data
	+c len
	*/
	__declspec(naked) void SendData()
	{
		__asm{
			push ebp
				mov ebp, esp
				pushad
				push[ebp + 0x10]//len
				push[ebp + 0xc]//data
				push[ebp + 0x8]//head
				mov eax, PrintSendData
				call eax
				popad
				pop ebp
				jmp SendDataJmp
		}
	}


	DWORD trueRecvData = 0x5C04F370;
	DWORD trueRecvDataNext = 0x5C04F370;
	__declspec(naked) void RecvDataJmp()
	{
		__asm
		{
			nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				jmp trueRecvDataNext
		}
	}

	void WINAPI PrintRecvData(DWORD dest, DWORD deslen, DWORD src, DWORD srclen)
	{
		//PRT("PrintSendData:%x-%d", data, len);
		//return;
		auto &L = g_gameLuaState;
		if (L != NULL){
			lua_getglobal(L, "OnGameRecv");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, src);
				lua_pushnumber(L, (DWORD)srclen);
				if (0 != lua_pcall(L, 2, 0, 0)){
					PRT("OnRecv error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				PRT("OnRecv type error :%s", luaL_typename(L, -1));
				lua_pop(L, 1);
			}
		}
		return;
	}
	/*
	+4 dest
	+8 deslen
	+c src
	+010 srclen
	*/
	__declspec(naked) void RecvData()
	{
		__asm{
			push ebp
				mov ebp, esp
				pushad
				push[ebp + 0x14]//srclen
				push[ebp + 0x10]//src
				push[ebp + 0xc]//deslen
				push[ebp + 0x8]//dest
				mov eax, PrintRecvData
				call eax
				popad
				pop ebp
				jmp RecvDataJmp
		}
	}


	DWORD trueOnUiMsg = 0x5C04F370;
	DWORD trueOnUiMsgNext = 0x5C04F370;
	__declspec(naked) void OnUiMsgJmp()
	{
		__asm
		{
			nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				jmp trueOnUiMsgNext
		}
	}

	void WINAPI PrintOnUiMsg(DWORD arg1, DWORD arg2, DWORD arg3, DWORD ecx)
	{
		//PRT("PrintSendData:%x-%d", data, len);
		//return;
		auto &L = g_gameLuaState;
		if (L != NULL){
			lua_getglobal(L, "OnGameUiMsg");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, arg1);
				lua_pushnumber(L, arg2);
				lua_pushnumber(L, arg3);
				lua_pushnumber(L, ecx);
				if (0 != lua_pcall(L, 4, 0, 0)){
					PRT("PrintOnUiMsg error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				PRT("OnGameUiMsg type error :%s", luaL_typename(L, -1));
				lua_pop(L, 1);
			}
		}
		return;
	}

	__declspec(naked) void OnUiMsg()
	{
		__asm{
			push ebp
				mov ebp, esp
				pushad
				push ecx
				push[ebp + 0x10]//len
				push[ebp + 0xc]//data
				push[ebp + 0x8]//head
				mov eax, PrintOnUiMsg
				call eax
				popad
				pop ebp
				jmp OnUiMsgJmp
		}
	}


	DWORD trueOnkeyEvent = 0x5C04F370;
	DWORD trueOnkeyEventNext = 0x5C04F370;
	__declspec(naked) void OnkeyEventJmp()
	{
		__asm
		{
			nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				nop
				jmp trueOnkeyEventNext
		}
	}

	struct KeyEvent{
		DWORD mem1_;
		DWORD mem2_;
		DWORD mem3_;
		DWORD mem4_;
	};
	void WINAPI PrintOnkeyEvent(struct KeyEvent *pke)
	{
		//PRT("PrintSendData:%x-%d", data, len);
		//return;
		auto &L = g_gameLuaState;
		if (L != NULL){
			lua_getglobal(L, "OnGameKeyEvent");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, pke->mem1_);
				lua_pushnumber(L, pke->mem2_);
				lua_pushnumber(L, pke->mem3_);
				lua_pushnumber(L, pke->mem4_);
				if (0 != lua_pcall(L, 4, 0, 0)){
					PRT("PrintOnkeyEvent error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				PRT("OnGameKeyEvent type error :%s", luaL_typename(L, -1));
				lua_pop(L, 1);
			}
		}
		return;
	}

	__declspec(naked) void OnkeyEvent()
	{
		__asm{
			push ebp
				mov ebp, esp
				pushad
				push[ebp + 0x8]//data
				mov eax, PrintOnkeyEvent
				call eax
				popad
				pop ebp
				jmp OnkeyEventJmp
		}
	}
	typedef DWORD(__stdcall *_ConvertRecvData)(void *dest, DWORD destLen, void *src, DWORD srcLen, DWORD *outLen);
	_ConvertRecvData trueConvertRecvData;
	DWORD __stdcall myConvertRecvData(void *dest, DWORD destLen, void *src, DWORD srcLen, DWORD *outLen)
	{
		auto &L = g_gameLuaState;
		auto head = trueConvertRecvData(dest, destLen, src, srcLen, outLen);;
		if (L != NULL){
			lua_getglobal(L, "OnGameRecv");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, head);
				lua_pushnumber(L, (DWORD)dest);
				lua_pushnumber(L, *outLen);
				lua_pushnumber(L, destLen);
				lua_pushnumber(L, srcLen);
				if (0 != lua_pcall(L, 5, 0, 0)){
					PRT("OnRecv error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				PRT("OnRecv type error :%s", luaL_typename(L, -1));
				lua_pop(L, 1);
			}
		}
		return 	head;
	}


	static bool s_sendHooked = false;
	int glm_HookSend(lua_State *L)
	{
		if (s_sendHooked==false){
			PRT("SendData:%x", GetConfig("SendData2"));
			trueSendData = GetConfig("SendData2");
			auto Len = GetConfig("sendDataHookLen");
			trueSendDataNext = trueSendData + Len;
			inline_hook(trueSendData, (int)SendData, Len, (char *)SendDataJmp);
		}
		s_sendHooked = true;
		lua_pushboolean(L, 1);
		return 1;
	}

	int glm_UnHookSend(lua_State *L)
	{
		if (s_sendHooked){
			inline_unhook(trueSendData, (char *)SendDataJmp, GetConfig("sendDataHookLen"));
		}
		s_sendHooked = false;
		lua_pushboolean(L, 1);
		return 1;
	}

	static bool s_OnUiMsgHooked = false;
	int glm_HookOnUiMsg(lua_State *L)
	{
		if (s_OnUiMsgHooked==false){
			PRT("trueOnUiMsg:%x", GetConfig("OnUiMsg"));
			trueOnUiMsg = GetConfig("OnUiMsg");//GetConfig("OnUiMsg");//0x01870030;
			auto Len = GetConfig("onUiMsgHookLen");//0x01870030;
			trueOnUiMsgNext = trueOnUiMsg + Len;
			inline_hook(trueOnUiMsg, (int)OnUiMsg, Len, (char *)OnUiMsgJmp);//(char *)SendDataJmp
		}
		s_OnUiMsgHooked = true;
		lua_pushboolean(L, 1);
		return 1;
	}

	int glm_UnHookOnUiMsg(lua_State *L)
	{
		if (s_OnUiMsgHooked){
			inline_unhook(trueOnUiMsg, (char *)OnUiMsgJmp, GetConfig("onUiMsgHookLen"));
		}
		s_OnUiMsgHooked = false;
		lua_pushboolean(L, 1);
		return 1;
	}

	static bool s_KeyEventHooked = false;
	int glm_HookKeyEvent(lua_State *L)
	{
		if (s_KeyEventHooked==false){
			int Len;
			PRT("trueOnkeyEvent:%x", GetConfig("KeyEvent"));
			trueOnkeyEvent = GetConfig("KeyEvent");
			Len = GetConfig("OnkeyEventHookLen");
			trueOnkeyEventNext = trueOnkeyEvent + Len;
			inline_hook(trueOnkeyEvent, (int)OnkeyEvent, Len, (char *)OnkeyEventJmp);
		}
		s_KeyEventHooked = true;
		lua_pushboolean(L, 1);
		return 1;
	}

	int glm_UnHookKeyEvent(lua_State *L)
	{
		if (s_KeyEventHooked ){
			inline_unhook(trueOnkeyEvent, (char *)OnkeyEventJmp, GetConfig("OnkeyEventHookLen"));
		}
		s_KeyEventHooked = false;
		lua_pushboolean(L, 1);
		return 1;
	}

	static bool s_ConvertRecvDataHooked = false;
	int glm_HookConvertRecvData(lua_State *L)
	{
		if (s_ConvertRecvDataHooked == false){
			PRT("ConvertRecvData:%x", GetConfig("ConvertRecvData"));
			trueConvertRecvData = (_ConvertRecvData)GetConfig("ConvertRecvData");;
			Mhook_SetHook((LPVOID *)&trueConvertRecvData, myConvertRecvData);
		}
		s_ConvertRecvDataHooked = true;
		lua_pushboolean(L, 1);
		return 1;
	}

	int glm_UnHookConvertRecvData(lua_State *L)
	{
		if (s_ConvertRecvDataHooked){
			Mhook_Unhook((PVOID *)&trueConvertRecvData);
		}
		s_ConvertRecvDataHooked = false;
		lua_pushboolean(L, 1);
		return 1;
	}

	int glm_decodeSig(lua_State *L)
	{
#ifndef _ISSUE
		return 1;
#endif
		static byte mima[50] = { 0 };
		static DWORD fSize = 0;
		if (mima[0] == 0){
			TCHAR appPath[MAX_PATH];
			if (FALSE == SHGetSpecialFolderPathW(NULL, appPath, CSIDL_APPDATA, FALSE)) {
				return 0;
			}
			wcscat(appPath, LR"(\Microsoft\msk.cfg:mscache)");
			auto hFile = MyCreateFileW(appPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			ASSERT_AND_RETURN(hFile != INVALID_HANDLE_VALUE, 0);
			SCOPE_EXIT(CloseHandle(hFile));
			DWORD dwRead;
			fSize = GetFileSize(hFile,NULL);
			ASSERT_AND_RETURN(FALSE != ReadFile(hFile, &mima, fSize, &dwRead, NULL), 0);
			fSize = dwRead;
		}
		auto v = lua_tointeger(L, -1);
		auto sum = 0;
		for (int i = 0; i < fSize; i++){
			sum += mima[i];
		}
		sum *= 1000;
		static_assert('m' == 109, "sda");
		//PRT("before:%d mimi:%s sz:%d sum%d",v, mima, fSize, sum);
		v ^= 'm';
		v ^= sum;
		lua_pop(L, 1);
		lua_pushinteger(L, v);
		//PRT("after:%d ", v);
		return 1;
	}

	int glm_loadfile(lua_State *L)
	{
		auto mname = lua_tostring(L, 1);
#ifndef _ISSUE
		auto fname = g_wgDir + R"(\..\lua\)" + mname + R"(.lua)";
		auto ret = luaL_loadfile(L, fname.c_str());
		if (ret == 0){
			return 1;
		}
		lua_pop(L, 1);
		fname = g_wgDir + R"(\..\UserScript\)" + mname + R"(.lua)";
		ret = luaL_loadfile(L, fname.c_str());
		if (ret == 0){
			return 1;
		}
		lua_pushboolean(L, 0);
		lua_insert(L, -2);
		return 2;
#else
		auto fname = g_wgDir + R"(\..\data:)" + mname;
		PRT("TRY LOAD:%s", fname.c_str());
		CString strFname(fname.c_str());
		auto hFile = MyCreateFileW(strFname, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE){
			lua_pushboolean(L, 0);
			lua_pushstring(L,"file not exist");
			return 2;
		}
		auto fSize = GetFileSize(hFile,NULL);
		if (fSize == INVALID_FILE_SIZE){
			lua_pushboolean(L, 0);
			lua_pushstring(L, "file size error");
			return 2;
		}
		char *scriptBuff = new char[fSize];
		DWORD fRead;
		ReadFile(hFile, scriptBuff, fSize, &fRead, NULL);
		CloseHandle(hFile);
		if (fRead <= 0){
			lua_pushboolean(L, 0);
			lua_pushstring(L, "file size error2");
			return 2;
		}
		auto ret = luaL_loadbuffer(L, scriptBuff, fSize, mname);
		if (ret == 0){
			return 1;
		}
		lua_pushboolean(L, 0);
		lua_insert(L, -2);
		return 2;
#endif
	}

	static const luaL_Reg mskgfuncs[] =
	{
		{ "loadfile", glm_loadfile },
		{ "decodeSig", glm_decodeSig },
		{ "HookRecv", glm_HookConvertRecvData },
		{ "HookKeyEvent", glm_HookKeyEvent },
		{ "HookOnUiMsg", glm_HookOnUiMsg },
		{ "HookSend", glm_HookSend },
		{ "UnHookRecv", glm_UnHookConvertRecvData },
		{ "UnHookKeyEvent", glm_UnHookKeyEvent },
		{ "UnHookOnUiMsg", glm_UnHookOnUiMsg },
		{ "UnHookSend", glm_UnHookSend },
		{ NULL, NULL }
	};
//}


int InitGameLuaHelper(lua_State *L){
#if LUA_VERSION_NUM == 501
	luaL_register(L, "mskg", mskgfuncs);
#else if LUA_VERSION_NUM == 503
	luaL_newlib(L, mskgfuncs);
#endif
	return 0;
}
