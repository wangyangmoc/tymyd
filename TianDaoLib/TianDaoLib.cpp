// TianDaoLib.cpp : 定义 DLL 的初始化例程。
//

#include "stdafx.h"
#include "TianDaoLib.h"

#include <Psapi.h>
#include <functional>
#include <DbgHelp.h>

#include "GameDialog.h"

#include "GameLuaHelper.h"
#include "lua_helper.h"
#include "winapi.h"
#include "../luasocket/luasocket.h"
#include "../macro/macro.h"
std::string g_wgDir;

#define MSKBOX MessageBoxA(NULL,"","",MB_OK)
extern "C"  int luaopen_pack(lua_State *L);
extern "C" int luaopen_bit(lua_State *L);
extern "C" int luaopen_profiler(lua_State *L);
#ifdef WIN32  

#include <direct.h>  
#include <io.h>  

#elif LINUX  

#include <stdarg.h>  
#include <sys/stat.h>  

#endif  

#ifdef WIN32  

#define ACCESS _access  
#define MKDIR(a) _mkdir((a))  

#elif LINUX  

#define ACCESS access  
#define MKDIR(a) mkdir((a),0755)  

#endif 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO:  如果此 DLL 相对于 MFC DLL 是动态链接的，
//		则从此 DLL 导出的任何调入
//		MFC 的函数必须将 AFX_MANAGE_STATE 宏添加到
//		该函数的最前面。
//
//		例如: 
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// 此处为普通函数体
//		}
//
//		此宏先于任何 MFC 调用
//		出现在每个函数中十分重要。  这意味着
//		它必须作为函数中的第一个语句
//		出现，甚至先于所有对象变量声明，
//		这是因为它们的构造函数可能生成 MFC
//		DLL 调用。
//
//		有关其他详细信息，
//		请参阅 MFC 技术说明 33 和 58。
//

// CTianDaoLibApp

BEGIN_MESSAGE_MAP(CTianDaoLibApp, CWinApp)
END_MESSAGE_MAP()


// CTianDaoLibApp 构造

CTianDaoLibApp::CTianDaoLibApp()
{
	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CTianDaoLibApp 对象

CTianDaoLibApp theApp;

unsigned int WINAPI MainThread(void *)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	HINSTANCE hOldRes = AfxGetResourceHandle();
	AfxSetResourceHandle(theApp.m_hInstance);

	CGameDialog dialog;
	dialog.DoModal();

	AfxSetResourceHandle(hOldRes);

	return 0;
}

unsigned int WINAPI LoadMainThread(void *)
{
	return 0;
}

int CreatDir(char *pszDir)
{
	int i = 0;
	int iRet;
	int iLen = strlen(pszDir);
	// 创建目录  
	for (i = 0; i <= iLen; i++){
		if (pszDir[i] == '\\' || pszDir[i] == '/'){
			pszDir[i] = '\0';

			//如果不存在,创建  
			iRet = ACCESS(pszDir, 0);
			if (iRet != 0){
				iRet = MKDIR(pszDir);
				if (iRet != 0){
					return -1;
				}
			}
			//支持linux,将所有\换成/  
			pszDir[i] = '/';
		}
	}
	return 0;
}

LPTOP_LEVEL_EXCEPTION_FILTER orgSUEF = NULL;

LONG WINAPI MYSEFFilter(
	_In_ struct _EXCEPTION_POINTERS *ExceptionInfo
	)
{
	if (ExceptionInfo->ExceptionRecord->ExceptionCode == 0xc000005) {
		PRT("error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		HANDLE hDumpFile = CreateFileA("mskdump.dmp", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		auto MDWD = (decltype(MiniDumpWriteDump) *)GetProcAddress(GetModuleHandleA("DbgHelp.dll"), "MiniDumpWriteDump");
		// Dump信息
		//
		MINIDUMP_EXCEPTION_INFORMATION dumpInfo;
		dumpInfo.ExceptionPointers = ExceptionInfo;
		dumpInfo.ThreadId = GetCurrentThreadId();
		dumpInfo.ClientPointers = TRUE;

		// 写入Dump文件内容
		//
		MDWD(GetCurrentProcess(), GetCurrentProcessId(), hDumpFile, MiniDumpNormal, &dumpInfo, NULL, NULL);

		CloseHandle(hDumpFile);
		PRT("error!!!!")
	}
	if (NULL != orgSUEF)
		return orgSUEF(ExceptionInfo);
	return EXCEPTION_CONTINUE_SEARCH;
}

decltype(luaL_loadfile) * __luaL_loadfile;
decltype(luaL_loadbuffer) * __luaL_loadbuffer;
bool g_blockPair = false;

int myluaL_loadfile(lua_State *L, const char *filename){
	return __luaL_loadfile(L, filename);
}

int myluaL_loadbuffer(lua_State *L, const char *buff, size_t size,const char *name)
{
	PRT("myluaL_loadbuffer:%s", (name == NULL || name == buff) ? "??" : name);
#if 0//记录所有脚本
	if (name)
	{
		char dir[MAX_PATH] = { R"(D:\code\td\)" };
		strcat(dir, name);
		CreatDir(dir);
		auto f = fopen(dir, "wb");
		if (f)
		{
			fwrite(buff, size, 1, f);
			fclose(f);
		}
		else
		{
			PRT("fopen error:%s %d", dir, errno);
		}
	}
#else
	if (name && strcmp(R"(Data/Script/Preload.lua)", name) == 0){
		InitLuaHelper(L);
		InitGameLuaHelper(L);
		lua_pop(L, 1);
		//pack
		luaopen_pack(L);
		//bit
		luaopen_bit(L);
		lua_pop(L, 1);

		//加载socket
		lua_getglobal(L,"package");
		lua_getfield(L, -1, "loaded");
		luaopen_socket_core(L);
		lua_setfield(L, -2, "socket.core");
		//luaprofiler
		luaopen_profiler(L);
		lua_setfield(L, -2, "profiler");
		lua_pop(L, 2);

		lua_pushnumber(L, (DWORD)&g_blockPair);
		lua_setglobal(L, "g_blockPairAddr");
		lua_pushstring(L, g_wgDir.c_str());
		lua_setglobal(L, "g_wgDir");
#if 0		
		std::string preloadFile = g_wgDir + R"(Data\Script\Preload.lua)";
		auto f = fopen(preloadFile.c_str(), "wb");
		if (f){
			fwrite(buff, size, 1, f);
			fclose(f);
		}
		else{
			PRT("fopen error:%s %d", preloadFile.c_str(), errno);
		}
#endif
		std::string buffer = R"(local DoFile = _G.dofile;DoFile[[)";
		buffer += g_wgDir;
		buffer += R"(\..\Script\Preload.lua]];DoFile[[)";
		buffer += g_wgDir;
#ifndef _ISSUE
		buffer += R"(\..\UserScript\test.lua]])";
#else
		buffer += R"(\..\data:test]])";
#endif
		PRT("buffer:%s", buffer.c_str());
		auto ret = __luaL_loadbuffer(L, buffer.c_str(), buffer.size(), name);
		if (ret != 0){
			PRT("luaL_loadbuffer error;%s-> %s", name, lua_tostring(L, -1));
		}
		
		Mhook_Unhook((PVOID *)&__luaL_loadbuffer);
		g_gameLuaState = L;
		return ret;
	}
#endif
	auto ret = __luaL_loadbuffer(L, buff, size, name);
	if (ret != 0){
		PRT("luaL_loadbuffer error;%s-> %s", name, lua_tostring(L, -1));
	}
	return ret;
}

int TianDaoLib(int a,int b)
{
	return a + b;
}



HOOKAPIPRE("User32.dll",PeekMessageW);
BOOL WINAPI myPeekMessageW(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMsg)
{
	if (g_bSysFlag){
		PRT("Get a call");
		g_bSysFlag = false;
	}
	return truePeekMessageW(lpMsg, hWnd, wMsgFilterMin, wMsgFilterMax, wRemoveMsg);
}

#include <string>
#include <fstream>
std::wstring strFileName;
bool bSelfOpen = false;
bool bSFCOpen = false;

HOOKAPIPRE("Kernel32.dll",CreateFileW);
HANDLE
WINAPI
myCreateFileW(
_In_ LPCWSTR lpFileName,
_In_ DWORD dwDesiredAccess,
_In_ DWORD dwShareMode,
_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
_In_ DWORD dwCreationDisposition,
_In_ DWORD dwFlagsAndAttributes,
_In_opt_ HANDLE hTemplateFile
)
{
	HANDLE ret;
	if (lpFileName)
	{
		if (wcsstr(lpFileName, L"\\Windows\\") == NULL 
			&& wcsstr(lpFileName, L"天涯明月刀\\") == NULL 
			&& wcsstr(lpFileName, L"Program") == NULL
			&& (wcsstr(lpFileName, L"exe") || wcsstr(lpFileName, L"EXE"))){
			static DWORD lastThreadId = 0;
			if (lastThreadId == GetCurrentThreadId()){
				if (g_blockPair){
					//Sleep(2000);
				}
			}
			else{
				lastThreadId = GetCurrentThreadId();
			}
			return trueCreateFileW(L"H:\\天涯明月刀\\WuXia.exe", dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
		}
	}

	ret = trueCreateFileW(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
	return ret;
	if (!bSelfOpen && (GENERIC_READ&dwDesiredAccess) && ret != INVALID_HANDLE_VALUE && lpFileName != NULL){
		PRTW(L"myCreateFileW:%s", lpFileName);
	}
	else{
		bSFCOpen = false;
	}
	return ret;
}

DWORD trueLogSFC;
_declspec(naked) void LogSFCJmp()
{
	__asm
	{
		nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			jmp trueLogSFC
	}
}

#define fCreateFileW CreateFileW
CRITICAL_SECTION css;
void LogSFC(void* data ,DWORD len,char * fname)
{
	using std::ifstream;
	static auto bOk = false;
	if (bOk) return;
	EnterCriticalSection(&css);
	//注入准备
	{
		if (strcmp(fname, R"(DATA\SCRIPT\PRELOAD.LUA)") == 0){
			memset(data, '\n', len);
#ifdef _REPLACE_CONTEXT
			strcpy_s((char *)data, len, R"(dofile"inject.lua")");
#else
			//fopen不行 原因未知
			HANDLE hOpenFile = fCreateFileW(LR"(H:\天涯明月刀\inject.lua)", GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (INVALID_HANDLE_VALUE == hOpenFile){
				PRT("fCreateFileW error:%d", GetLastError())
			}
			else{
				DWORD rsz;
				ReadFile(hOpenFile, data, len, &rsz, NULL);
				CloseHandle(hOpenFile);
				PRT("PRELOAD read:%d-%d", len, rsz);
			}
#endif // _REPLACE_CONTEXT

		}
		LeaveCriticalSection(&css);
		return;
	}

	char dir[MAX_PATH] = { R"(info\)" };
	strcat(dir, fname);
	if (fname && strstr(fname, ".LUA")){
		PRT("lua:%s", fname);
	}	
	else{
		LeaveCriticalSection(&css);
		return;
	}
		
	//如果不存在,创建  
	auto iRet = ACCESS(dir, 0);//只写不存在的文件
	if (iRet == 0){
		LeaveCriticalSection(&css);
		return;
	}
	if (-1 == CreatDir(dir)){
		PRT("CreatDir error:%s", dir);
		LeaveCriticalSection(&css);
		return;
	}
	auto f = fopen(dir, "wb");
	if (NULL == f){
		PRT("fopen error:%s %d", dir,errno);
		LeaveCriticalSection(&css);
		return;
	}
	fwrite(data, len, 1, f);
	fclose(f);
	f = fopen(R"(info\filelen.txt)", "a");
	if (f){
		sprintf_s(dir, "%s:%d\n", fname, len);
		fwrite(dir, strlen(dir), 1, f);
		fclose(f);
	}
	LeaveCriticalSection(&css);
}

__declspec(naked) void NakeLogSFC()
{
	__asm
	{
		pushad
		push [esp+0x28]
		push ebx
		push eax
		mov ecx, LogSFC
		call ecx
		add esp,0xc
		popad
		jmp LogSFCJmp
	}
}



typedef enum enumSYSTEM_INFORMATION_CLASS
{
	SystemBasicInformation,
	SystemProcessorInformation,
	SystemPerformanceInformation,
	SystemTimeOfDayInformation,
}SYSTEM_INFORMATION_CLASS;

typedef struct tagPROCESS_BASIC_INFORMATION
{
	DWORD ExitStatus;
	DWORD PebBaseAddress;
	DWORD AffinityMask;
	DWORD BasePriority;
	ULONG UniqueProcessId;
	ULONG InheritedFromUniqueProcessId;
}PROCESS_BASIC_INFORMATION;

typedef LONG(WINAPI *PNTQUERYINFORMATIONPROCESS)(HANDLE, UINT, PVOID, ULONG, PULONG);
PNTQUERYINFORMATIONPROCESS  NtQueryInformationProcess = NULL;

#define PRINT_LINE  printf("---------------------------------------------\n")  

int GetParentProcessID(DWORD dwId)
{
	LONG                      status;
	DWORD                     dwParentPID = 0;
	HANDLE                    hProcess;
	PROCESS_BASIC_INFORMATION pbi;

	hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, dwId);
	if (!hProcess)
		return -1;

	status = NtQueryInformationProcess(hProcess, SystemBasicInformation, (PVOID)&pbi, sizeof(PROCESS_BASIC_INFORMATION), NULL);
	if (!status)
		dwParentPID = pbi.InheritedFromUniqueProcessId;

	CloseHandle(hProcess);
	return dwParentPID;
}
UINT g_uDirFlag = 0;
// char dir[] = { "天涯明月刀" };
// _strlwr_s(dir);
// auto i = 0;
// UINT ecx = 0;
// while (dir[i]){
// 	ecx = ecx * 0x43 + dir[i];
// 	i++;
// }
// printf("%u %x", ecx, ecx);
auto g_tryMuiltOpen = false;
HOOKAPIPRE("Kernel32.dll",CreateEventA);
HANDLE
WINAPI
myCreateEventA(
_In_opt_ LPSECURITY_ATTRIBUTES lpEventAttributes,
_In_ BOOL bManualReset,
_In_ BOOL bInitialState,
_In_opt_ LPCSTR lpName
)
{
	char newName[MAX_PATH];
	if (g_tryMuiltOpen && lpName){
		auto top = lua_gettop(g_cfgL);
		lua_getglobal(g_cfgL, "string");
		lua_getfield(g_cfgL, -1, "match");
		lua_pushstring(g_cfgL, lpName);
		lua_pushstring(g_cfgL, "(%w+)_%d+_(%d+)");
		auto ret = lua_pcall(g_cfgL, 2, 2, 0);
		if (ret != 0){
			auto err = lua_tostring(g_cfgL,-1);
			PRT("匹配失败:%s", err);
		}
		else if(lua_isstring(g_cfgL,-1)){
			PRT("myCreateEventA 过滤:%s", lpName);
			DWORD pid = GetCurrentProcessId();
			lua_getglobal(g_cfgL, "filesystem");
			if (lua_toboolean(g_cfgL, -1)){
				pid = GetParentProcessID(pid);
			}
			lua_pop(g_cfgL, 1);
			sprintf_s(newName, "%s_%d_%d", lua_tostring(g_cfgL, -2), pid, lua_tointeger(g_cfgL, -1));
			PRT("new myCreateEventA:%s", newName);
			lpName = newName;
		}
		lua_settop(g_cfgL, top);
	}
	if (lpName && strstr(lpName, "GFSStartEvent")){
		lua_getglobal(g_cfgL, "filesystem");
		if (lua_toboolean(g_cfgL, -1) == 0){
			PRT("SET luaL_loadbuffer:%x", GetConfig("luaL_loadbuffer"));
			SET(__luaL_loadbuffer, GetConfig("luaL_loadbuffer")); //0x136f010
			Mhook_SetHook((PVOID*)&__luaL_loadbuffer, myluaL_loadbuffer);
			if (!g_tryMuiltOpen)
				UNHOOKAPI(CreateEventA);
		}
		lua_pop(g_cfgL, 1);
	}
	return trueCreateEventA(lpEventAttributes, bManualReset, bInitialState, lpName);
}


HOOKAPIPRE("Kernel32.dll", CreateFileMappingA);
HANDLE
WINAPI
myCreateFileMappingA(
_In_     HANDLE hFile,
_In_opt_ LPSECURITY_ATTRIBUTES lpFileMappingAttributes,
_In_     DWORD flProtect,
_In_     DWORD dwMaximumSizeHigh,
_In_     DWORD dwMaximumSizeLow,
_In_opt_ LPCSTR lpName
)
{
	char newName[MAX_PATH];
	if (lpName && true){
		auto top = lua_gettop(g_cfgL);
		lua_getglobal(g_cfgL, "string");
		lua_getfield(g_cfgL, -1, "match");
		lua_pushstring(g_cfgL, lpName);
		lua_pushstring(g_cfgL, "(%w+)_%d+_(%d+)");
		auto ret = lua_pcall(g_cfgL, 2, 2, 0);
		if (ret != 0){
			auto err = lua_tostring(g_cfgL, -1);
			PRT("匹配失败:%s", err);
		}
		else if (lua_isstring(g_cfgL, -1)){
			PRT("myCreateFileMappingA 过滤:%s", lpName);
			DWORD pid = GetCurrentProcessId();
			lua_getglobal(g_cfgL, "filesystem");
			if (lua_toboolean(g_cfgL, -1)){
				pid = GetParentProcessID(pid);
			}
			lua_pop(g_cfgL, 1);
			sprintf_s(newName, "%s_%d_%d", lua_tostring(g_cfgL, -2), pid, lua_tointeger(g_cfgL, -1));
			PRT("new CreateFileMappingA:%s", newName);
			lpName = newName;
		}
		lua_settop(g_cfgL, top);
	}
	return trueCreateFileMappingA(hFile, lpFileMappingAttributes, flProtect, dwMaximumSizeHigh, dwMaximumSizeLow, lpName);
}

HOOKAPIPRE("KernelBase.dll",CreateRemoteThreadEx);
DWORD WINAPI NoopThread(
	LPVOID lpThreadParameter
	)
{
	while (true){
		Sleep(3000);
	}
	return 1;
}


HANDLE
WINAPI
myCreateRemoteThreadEx(
_In_ HANDLE hProcess,
_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
_In_ SIZE_T dwStackSize,
_In_ LPTHREAD_START_ROUTINE lpStartAddress,
_In_opt_ LPVOID lpParameter,
_In_ DWORD dwCreationFlags,
_In_opt_ LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList,
_Out_opt_ LPDWORD lpThreadId
)
{
	if ((DWORD)lpStartAddress == 0x7718233b || (DWORD)lpStartAddress == 0x325275f){
		PRT("skip Thread:%p\n", lpStartAddress);
		return trueCreateRemoteThreadEx(hProcess, lpThreadAttributes, dwStackSize, NoopThread, lpParameter, dwCreationFlags, lpAttributeList, lpThreadId);
	}
	return trueCreateRemoteThreadEx(hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpAttributeList, lpThreadId);
}

HOOKAPIPRE("Kernel32.dll", LoadLibraryW);
HMODULE
WINAPI
myLoadLibraryW(
_In_ LPCWSTR lpLibFileName
)
{
	auto ret = trueLoadLibraryW(lpLibFileName);
	if (ret != NULL && lpLibFileName && wcsstr(lpLibFileName, L"ProtocalHandler.dll")){
		PRT("LoadLibraryExW ok:%#x", ret);
	}
	else if (lpLibFileName){
		PRTW(L"LoadLibraryExW ok:%s", lpLibFileName);
	}
	return ret;
}

HOOKAPIPRE("ws2_32.dll", send);
int PASCAL FAR mysend(
	_In_ SOCKET s,
	_In_reads_bytes_(len) const char FAR * buf,
	_In_ int len,
	_In_ int flags)
{
	auto &L = g_gameLuaState;
	if (L != NULL){
			lua_getglobal(L, "OnSend");
			if (lua_isfunction(L, -1)){
				lua_pushnumber(L, len);
				if (0 != lua_pcall(L, 1, 0, 0)){
					PRT("OnSend error:%s", lua_tostring(L, -1));
					lua_pop(L, 1);
				}
			}
			else{
				lua_pop(L, 1);
			}
	}
	return truesend(s, buf, len,flags);
}
HOOKAPIPRE("Kernel32.dll", CreateThread);
HANDLE
WINAPI
myCreateThread(
	_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
	_In_ SIZE_T dwStackSize,
	_In_ LPTHREAD_START_ROUTINE lpStartAddress,
	_In_opt_ __drv_aliasesMem LPVOID lpParameter,
	_In_ DWORD dwCreationFlags,
	_Out_opt_ LPDWORD lpThreadId
	)
{
	auto ret = trueCreateThread(lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);
	int tid = 0;
	if (lpThreadId) tid = *lpThreadId;
	PRT("CreateThread:%x-%p", tid, lpStartAddress);
	return ret;
}

HOOKAPIPRE("Kernelbase.dll", GetCurrentDirectoryA);
DWORD
WINAPI
myGetCurrentDirectoryA(
_In_ DWORD nBufferLength,
_Out_writes_to_opt_(nBufferLength, return +1) LPSTR lpBuffer
)
{
	static auto ctime = GetCurrentTime();
	auto ret = trueGetCurrentDirectoryA(nBufferLength, lpBuffer);
	if (lpBuffer){
		PRT("开始更改目录:%s", lpBuffer);
		sprintf_s(lpBuffer, nBufferLength, "%s%d", lpBuffer, ctime);
	}
	return ret;
}

#include <sstream>
#include <exception>
using std::stringstream;
class Assert : public std::exception
{
public:
	Assert(std::string msg) :SMART_ASSERT_A(*this), SMART_ASSERT_B(*this){ msg_ = msg+"\n"; }
	Assert& SMART_ASSERT_A = *this;
	Assert& SMART_ASSERT_B = *this;
	//whatever member functions
	enum Assert_LV {
		lv_error,
		lv_debug,
		lv_fatal,
		lv_warn,
	};

	//暂时不执行什么东西
	void level(Assert_LV lv = lv_debug)
	{
		switch (lv){
		case lv_debug:
			return debug();
		case lv_error:
			return error();
		case lv_fatal:
			return fatal();
		case lv_warn:
			return warn();
		}
	}
	void error(){}
	void debug(){}
	void fatal(){}
	void warn(){}

	Assert &print_context(std::string fname, int line)
	{
		PRT("exp Assertion failed in %s:%d", fname.c_str(), line);
		return *this;
	}
	Assert &print_current_val(int v, std::string vname)
	{
		PRT("exp:%s",vname.c_str())
		return *this;
	}
	Assert &print_current_val(std::string v, std::string vname)
	{
		PRT("exp:%s:%s", vname.c_str(), v.c_str());
		return *this;
	}
	
	virtual const char* what() const throw()
	{
		return "Assert";
	}

private:
	std::string msg_;
};

Assert make_assert(std::string msg)
{
	throw Assert{msg};
}

std::string GetLastErrorStr()
{
	DWORD dwError = GetLastError();
	//FormatMessage内部分配内存，我们需要hlocal指向它  
	HLOCAL hlocal = NULL;
	//系统默认语言，值貌似是0  
	DWORD systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
	BOOL fOk = FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM |     //系统预定义错误  
		FORMAT_MESSAGE_IGNORE_INSERTS |  //忽略%符，和printf中的%有点相似  
		FORMAT_MESSAGE_ALLOCATE_BUFFER,  //内部分配内存  
		NULL,            //在系统中查找错误代码  
		dwError,        //错误代码  
		systemLocale,   //用什么语言显示错误文本  
		(PSTR)&hlocal, //指向内部分配内存的指针  
		0,              //分配内存大小，因是FormatMessage分配，我们不用管  
		NULL            //可变参数列表  
		);

	// Do something (PCTSTR)LocalLock(hlocal)  
	std::string str{ (PSTR)LocalLock(hlocal) };
	LocalUnlock(hlocal);
	LocalFree(hlocal);
	return str;
}

#define SMART_ASSERT_A(x) SMART_ASSERT_OP(x,B)
#define SMART_ASSERT_B(x) SMART_ASSERT_OP(x,A)
#define SMART_ASSERT_OP(x,next) \
		SMART_ASSERT_A.print_current_val((x), #x).SMART_ASSERT_##next
#define SMART_ASSERT(expr) \
	if ((expr)); \
	else make_assert(#expr).print_context(__FILE__, __LINE__).SMART_ASSERT_A

#define  ENSURE SMART_ASSERT
#define ENSURE_WIN32(exp) ENSURE(exp)(GetLastErrorStr())
#define ENSURE_SUCCEEDED(hr) \
    if(SUCCEEDED(hr)) \
else ENSURE(SUCCEEDED(hr))(Win32ErrorMessage(hr))


__declspec(dllexport) BOOL _DllInit()
{
	//return TRUE;
	try{
		PRT("TDLIB loaded....");
		//LoadLibraryA("sxs.dll");
		char dir[MAX_PATH] = { 0 };
		GetModuleFileNameA(NULL, dir, MAX_PATH);
		PRT("CTianDaoLibApp::InitInstance:%s", dir);
		_strupr(dir);
		//return TRUE;
#if 0
		g_tryMuiltOpen = true;
		if (strstr(dir, "GAMEFILESYSTEM.EXE") == NULL && strstr(dir, "WUXIA_CLIENT.EXE") == NULL){
			return TRUE;
		}
		char mutexName[MAX_PATH];
		sprintf_s(mutexName, "%s%d", "MSCTF.Asm.MutexDefault", GetCurrentProcessId());
		auto hMutex = CreateMutexA(NULL, FALSE, mutexName);
		if (GetLastError() == ERROR_ALREADY_EXISTS){
			CloseHandle(hMutex);
			return TRUE;
		}
		HOOKAPI(CreateEventA);
		HOOKAPI(CreateFileMappingA);
		g_cfgL = luaL_newstate();
		luaL_openlibs(g_cfgL);
		if (strstr(dir, "GAMEFILESYSTEM.EXE") != NULL){
			lua_pushboolean(g_cfgL, 1);
			lua_setglobal(g_cfgL, "filesystem");
			return TRUE;
		}
		else{
			lua_pushboolean(g_cfgL, 0);
			lua_setglobal(g_cfgL, "filesystem");
		}
#else
		if (strstr(dir, "WUXIA_CLIENT.EXE") == NULL){
			return TRUE;
		}
		g_tryMuiltOpen = false;
		HOOKAPI(CreateEventA);
		g_cfgL = luaL_newstate();
		luaL_openlibs(g_cfgL);
		if (strstr(dir, "GAMEFILESYSTEM.EXE") != NULL){
			lua_pushboolean(g_cfgL, 1);
			lua_setglobal(g_cfgL, "filesystem");
			return TRUE;
		}
		else{
			lua_pushboolean(g_cfgL, 0);
			lua_setglobal(g_cfgL, "filesystem");
		}
#endif
		PRT("Create Lua");
		char appPath[MAX_PATH];
		if (FALSE == SHGetSpecialFolderPathA(NULL, appPath, CSIDL_APPDATA, FALSE)) {
			PRT("无法获取配置文件");
			return TRUE;
		}
		strcat(appPath, R"(\Microsoft\config.ini)");
		auto fp = fopen(appPath, "r");
		fgets(appPath, MAX_PATH, fp);
		fclose(fp);
		PRT("配置路径:%s", appPath);
		g_wgDir = appPath;
		//g_wgDir = g_wgDir.substr(0, g_wgDir.size() - 12);
		if (NULL == g_cfgL){
			g_cfgL = luaL_newstate();
			luaL_openlibs(g_cfgL);
		}
		char sigsPath[MAX_PATH];
#ifdef _ISSUE
		sprintf_s(sigsPath, "%s%s", g_wgDir.c_str(), R"(\..\tools\sigs.txt)");
#else
		sprintf_s(sigsPath, "%s%s", g_wgDir.c_str(), R"(\..\tools\sigs2.txt)");
#endif
		PRT("Load sigs:%s", sigsPath);
		auto ret = luaL_dofile(g_cfgL, sigsPath);
		ENSURE(ret == 0)(ret);
		FILETIME ftCreate, ftAccess, ftWrite;
		SYSTEMTIME stUTC, stLocal;
		DWORD dwRet;
		PRT("Get File Times");
		auto hFile = CreateFileA(dir, GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, 0, NULL);
		ENSURE_WIN32(hFile != INVALID_HANDLE_VALUE)(dir);
		GetFileTime(hFile, NULL, NULL, &ftWrite);
		FileTimeToSystemTime(&ftWrite, &stUTC);
		SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);
		CloseHandle(hFile);
		PRT("Get Config Times");
		auto year = GetConfigTime("year");
		auto month = GetConfigTime("month");
		auto day = GetConfigTime("day");
		auto hour = GetConfigTime("hour");

		if (stLocal.wMonth > month || (stLocal.wMonth == month && stLocal.wDay > day) || (stLocal.wMonth == month && stLocal.wDay == day && stLocal.wHour > hour)){
			PRT("游戏版本过高:%d-%d-%d : %d-%d-%d", month, day, hour, stLocal.wMonth, stLocal.wDay, stLocal.wHour);
			return TRUE;
		}
		//PRT("CreateEventA");
		//HOOKAPI(CreateEventA);
		//HOOKAPI(CreateFileMappingA);

#ifndef _ISSUE
		//CloseHandle((HANDLE)::_beginthreadex(0, 0, MainThread, 0, 0, 0));
#endif // _DEBUG
		PRT("All OK");
		return TRUE;
	}
	catch (const std::exception&e){
		MessageBoxA(NULL, e.what(), "exception", MB_OK);
	}
	return FALSE;
}

BOOL CTianDaoLibApp::InitInstance()
{
	MessageBoxA(0, "xxxxx", "", MB_OK);
	PRT("aaaaaa");
	return CWinApp::InitInstance();
}
