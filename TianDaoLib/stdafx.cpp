// stdafx.cpp : 只包括标准包含文件的源文件
// TianDaoLib.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include <functional>

bool g_bSysFlag = false;
HANDLE g_hCallEvent = NULL;
DWORD g_dwSafeCallJmp = 0;
HWND g_hMsgWnd = NULL;
char g_strCmd[32] = { 0 };
char g_strArg[2046] = { 0 };
lua_State *g_gameLuaState = NULL;
lua_State *g_cfgL = NULL;

int GetConfig(const char* key)
{
	lua_getglobal(g_cfgL, "_g_sig");
	lua_getfield(g_cfgL, -1, key);
	glm_decodeSig(g_cfgL);
	auto ret = lua_tointeger(g_cfgL, -1);
	lua_pop(g_cfgL, 2);
	return ret;
}

int GetConfigTime(const char* key)
{
	lua_getglobal(g_cfgL, "_g_sig");
	lua_getfield(g_cfgL, -1, "updatetime");
	lua_getfield(g_cfgL, -1, key);
	auto ret = lua_tointeger(g_cfgL, -1);
	lua_pop(g_cfgL, 3);
	return ret;
}

//HOOKAPIPRE("Kernel32.dll",VirtualProtect);
void inline_hook(int hookaddr, int newaddr, int codelen, char *code)
{
	if (codelen < 5)
		MessageBoxA(NULL, "coodelen too short", "error", MB_OK);
	DWORD old;
	auto proret = VirtualProtectEx(GetCurrentProcess(), (LPVOID)hookaddr, codelen, PAGE_EXECUTE_READWRITE, &old);
	PRT("protect hookaddr:%d %u", proret, GetLastError());
	if (code != nullptr)
	{
		DWORD old2;
		PRT("protect code");
		VirtualProtectEx(GetCurrentProcess(), (LPVOID)code, codelen, PAGE_EXECUTE_READWRITE, &old2);
		memcpy(code, (void *)hookaddr, codelen);
		PRT("restore code");
		VirtualProtectEx(GetCurrentProcess(), (LPVOID)code, codelen, old2, &old2);
	}
	PRT("write hookaddr");
	int jmpaddr = newaddr - hookaddr - 5;
	*(byte *)hookaddr = 0xe9;
	hookaddr++;
	*(int *)hookaddr = jmpaddr;
	hookaddr += 4;
	for (int i = 0; i < (codelen - 5); ++i)
	{
		*(byte *)hookaddr = 0x90;
		hookaddr++;
	}
	PRT("restore hookaddr");
	VirtualProtectEx(GetCurrentProcess(), (LPVOID)hookaddr, codelen, old, &old);
}


void inline_unhook(int hookaddr, char *code,int codelen)
{
	DWORD old,old2;
	auto proret = VirtualProtectEx(GetCurrentProcess(), (LPVOID)hookaddr, codelen, PAGE_EXECUTE_READWRITE, &old);
	PRT("protect hookaddr:%d %u", proret, GetLastError());
	if (code != nullptr){
		VirtualProtectEx(GetCurrentProcess(), (LPVOID)code, codelen, PAGE_EXECUTE_READWRITE, &old2);
		memcpy((void *)hookaddr, code, codelen);
		VirtualProtectEx(GetCurrentProcess(), (LPVOID)code, codelen, old2, &old2);
	}
	VirtualProtectEx(GetCurrentProcess(), (LPVOID)hookaddr, codelen, old, &old);
}