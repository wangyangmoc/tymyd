// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 从 Windows 头中排除极少使用的资料
#endif

#include "targetver.h"
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 某些 CString 构造函数将是显式的

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE 类
#include <afxodlgs.h>       // MFC OLE 对话框类
#include <afxdisp.h>        // MFC 自动化类
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>                      // MFC ODBC 数据库类
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>                     // MFC DAO 数据库类
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxcontrolbars.h>

#define _UTF_STR_//游戏UI是UTF8字串

#include "../lua51/lua.hpp"
#include "../mhook-lib/mhook.h"
#define HOOKAPIPRE(md,fun) auto true##fun = (decltype(fun) *)GetProcAddress(GetModuleHandleA(md),#fun)
#define HOOKAPI(fun) Mhook_SetHook((LPVOID *)& true##fun, my##fun)
#define UNHOOKAPI(fun) Mhook_Unhook((LPVOID *)& true##fun)
#define HOOKAPINEW(fun) decltype(fun) my##fun
#define CAST(tp,v) (tp)(v)
#define SET(var,val) var = CAST(decltype(var),(val))

#ifndef _ISSUE
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#define PRTW(fmt,...) {wchar_t msg##__LINE__[4096]; swprintf_s(msg##__LINE__,L"msk_td->"fmt,__VA_ARGS__); OutputDebugStringW(msg##__LINE__);}
#else
#define PRT(fmt,...)
#define PRTW(fmt,...)
#endif
#define WM_MSG WM_USER+1
extern bool g_bSysFlag;
extern HANDLE g_hCallEvent;
extern const char *g_strCallEvent;
extern const char *g_strCallArgs;
extern char g_strCmd[32];
extern char g_strArg[2046];
extern HWND g_hMsgWnd;
extern DWORD g_dwSafeCallJmp;
extern lua_State *g_cfgL;
extern lua_State *g_gameLuaState;
int glm_decodeSig(lua_State *L);
int GetConfig(const char* key);
int GetConfigTime(const char* key);
void inline_hook(int hookaddr, int newaddr, int codelen, char *code);
void inline_unhook(int hookaddr, char *code, int codelen);