#include "stdafx.h"
#include <Psapi.h>
#include <atlconv.h>
#include <functional>
#include <locale>
#include <codecvt>

#ifdef _VER_RELEASE
namespace 
{
#endif
#define SAFECALL(x) __asm push x \
	__asm mov x, g_dwSafeCallJmp \
	__asm call x

#define E_V_BYTE 0
#define E_V_WORD 1
#define E_V_DWORD 2
#define E_V_FLOAT 3
#define E_V_ASCII 4
#define E_V_UNICODE 5

	template <typename T>
	T _CommonCall(DWORD callAddress, DWORD ecxAddress, DWORD *argArray, int len)
	{
		T retInt;
		DWORD dEsp = len * 4;
		__asm pushad
		__asm{
				mov ecx, len
				mov edx, 0
				mov eax, argArray
			PUSHARG :
				cmp edx, ecx
				jae GAMECALL
				push[eax + edx * 4]
				inc edx
				jmp PUSHARG
			GAMECALL :
				mov ecx, ecxAddress
				mov eax, callAddress
				SAFECALL(eax)
			ENDCODE :
				mov retInt, eax
		}
		if (ecxAddress == 0x12345678)
			__asm add esp, dEsp
		__asm popad
		return retInt;
	}

	//lua function by msk
	int lm_newthread(lua_State *L)
	{
		lua_State *nl = lua_newthread(L);
		return 1;
	}

	int lm_syscall(lua_State *L)
	{
		if (!g_bSysFlag) return 0;
		auto ret = 0;
		if (*g_strCmd && *g_strArg){
			lua_pushstring(L, g_strCmd);
			lua_pushstring(L, g_strArg);
			memset(g_strCmd, 0, sizeof(g_strCmd));
			memset(g_strArg, 0, sizeof(g_strArg));
			ret = 2;
		}
		g_bSysFlag = false;
		SetEvent(g_hCallEvent);
		return ret;
	}

	int lm_pid(lua_State *L)
	{
		lua_pushinteger(L, GetCurrentProcessId());
		return 1;
	}

	int lm_currentdir(lua_State *L)
	{
		char dir[MAX_PATH];
		GetCurrentDirectoryA(MAX_PATH, dir);
		lua_pushstring(L, dir);
		return 1;
	}
	

	int lm_tid(lua_State *L)
	{
		lua_pushinteger(L, GetCurrentThreadId());
		return 1;
	}

	int lm_exit(lua_State *L)
	{
		ExitProcess(lua_tointeger(L, -1));
		return 0;
	}

	int lm_todword(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushinteger(L, *(DWORD *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushinteger(L, 0);
		}
		return 1;
	}

	int lm_toword(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushinteger(L, *(WORD *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushinteger(L, 0);
		}
		return 1;
	}

	int lm_tobyte(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushinteger(L, *(byte *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushinteger(L, 0);
		}
		return 1;
	}

	int lm_tobool(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushboolean(L, *(byte *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushboolean(L, 0);
		}
		return 1;
	}

	int lm_tofloat(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushnumber(L, *(float *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_todouble(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushnumber(L, *(double *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_tolonglong(lua_State *L)
	{
		__try{
			LONGLONG *addrss = (LONGLONG *)(DWORD)lua_tonumber(L, 1);
			lua_pushinteger(L, *addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_todata(lua_State *L)
	{
		__try{
			const char *addrss = (char *)(DWORD)lua_tonumber(L, 1);
			size_t sz = lua_tointeger(L, 2);
			lua_pushlstring(L, addrss, sz);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_setdata(lua_State *L)
	{
		__try{
			char *addrss = (char *)(DWORD)lua_tonumber(L, 1);
			size_t sz;
			const char *data = lua_tolstring(L, 2, &sz);
			memcpy(addrss, data, sz);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_changetolonglong(lua_State *L)
	{
		__try{
			LONGLONG ret = lua_tonumber(L, 1);
			lua_pushnumber(L, ret);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_changetodouble(lua_State *L)
	{
		__try{
			double addrss = lua_tonumber(L, 1);
			lua_pushnumber(L, addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			lua_pushnumber(L, 0);
		}
		return 1;
	}

	int lm_copydata(lua_State *L)
	{
		__try{
			void *addrss = (void *)(DWORD)lua_tonumber(L, 1);
			const void *addrss2 = (void *)(DWORD)lua_tonumber(L, 2);
			size_t sz = lua_tointeger(L, 3);
			memcpy(addrss, addrss2, sz);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setdword(lua_State *L)
	{
		__try{
			DWORD *addrss = (DWORD *)(DWORD)lua_tonumber(L, 1);
			DWORD var = lua_tointeger(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setword(lua_State *L)
	{
		__try{
			WORD *addrss = (WORD *)(DWORD)lua_tonumber(L, 1);
			WORD var = lua_tointeger(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setbyte(lua_State *L)
	{
		__try{
			byte *addrss = (byte *)(DWORD)lua_tonumber(L, 1);
			byte var = lua_tointeger(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setbool(lua_State *L)
	{
		__try{
			bool *addrss = (bool *)(DWORD)lua_tonumber(L, 1);
			bool var = lua_tointeger(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setfloat(lua_State *L)
	{
		__try{
			float *addrss = (float *)(DWORD)lua_tonumber(L, 1);
			float var = lua_tonumber(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setdouble(lua_State *L)
	{
		__try{
			double *addrss = (double *)(DWORD)lua_tonumber(L, 1);
			double var = lua_tonumber(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_setlonglong(lua_State *L)
	{
		__try{
			LONGLONG *addrss = (LONGLONG *)(DWORD)lua_tonumber(L, 1);
			LONGLONG var = lua_tonumber(L, 2);
			*addrss = var;
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_swstr(lua_State *L)
	{
		DWORD memAddress = lua_tonumber(L, 1);
		__try{
			size_t sz;
			const char * str = lua_tolstring(L, 2,&sz);
			USES_CONVERSION;
			wcscpy_s((wchar_t *)memAddress,sz, A2W(str));
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
		}
		return 0;
	}

	int lm_new(lua_State *L)
	{
		size_t len = lua_tointeger(L, 1);
		size_t ret = (size_t)new byte[len];
		memset((void *)ret, 0, len);
		lua_pushinteger(L, ret);
		return 1;
	}

	int lm_print(lua_State *L)
	{
		size_t sz;
		auto szUtf8 = lua_tolstring(L, 1, &sz);
		if (szUtf8 == nullptr || sz == 0) return 0;
		auto lv = lua_tointeger(L, 2);
		try{
#if 1
#ifdef _UTF_STR_
			char *tmp = nullptr;
			int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);
			if (n != sz)
			{
				std::wstring_convert<std::codecvt_utf8<wchar_t> > conv;
				auto wstring = conv.from_bytes(szUtf8);
				OutputDebugStringW(wstring.c_str());
			}
			else
			{
				OutputDebugStringA(szUtf8);
			}
#else
			pstr = str;
			if (pstr)
			{
				if (g_hMsgWnd && lv < 1)//0才输出到对话框
				{
					::SendMessage(g_hMsgWnd, WM_MSG, (WPARAM)pstr, NULL);
				}
				OutputDebugStringA(pstr);
			}
#endif
#else
			int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);
			if (n == sz){
				OutputDebugStringA(szUtf8);
			}
			else{
				char *pstr = new char[n + 10];
				memset(pstr, 0, n + 10);
				WCHAR * wszGBK = new WCHAR[sizeof(WCHAR)* n];
				memset(wszGBK, 0, sizeof(WCHAR)* n);
				MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, wszGBK, n);

				n = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
				WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, pstr, n, NULL, NULL);

				delete[]wszGBK;
				wszGBK = NULL;
				OutputDebugStringA(pstr);
				delete[]pstr;
			}
#endif
		}
		catch (std::exception e)
		{
			OutputDebugStringA(szUtf8);
			//PRT("catch a exception:%s", e.what());
		}
		return 0;
	}

	int lm_delete(lua_State *L)
	{
		DWORD memAddress = lua_tointeger(L, 1);
		delete[](byte *)memAddress;
		return 0;
	}

	int lm_tick(lua_State *L)
	{
		lua_pushinteger(L, GetTickCount());
		return 1;
	}


	int lm_sleep(lua_State *L)
	{
		DWORD tm = lua_tointeger(L, 1);
		Sleep(tm);
		return 0;
	}

	int lm_w2a(lua_State *L)
	{
		size_t len;
		const wchar_t *str = (wchar_t*)lua_tolstring(L, 1, &len);
		USES_CONVERSION;
		lua_pushstring(L, W2A(str));
		return 1;
	}

	int lm_a2w(lua_State *L)
	{
		size_t len;
		const char *str = lua_tostring(L, 1);
		USES_CONVERSION;
		const wchar_t *wstr = A2W(str);
		lua_pushlstring(L, (char *)wstr, wcslen(wstr) * 2 + 2);
		return 1;
	}

	int lm_a2u8(lua_State *L)
	{
		sprintf_s()
		const char *szGbk = lua_tostring(L, -1);
		char *tmp = new char[strlen(szGbk) * 3 + 10];
		memset(tmp, 0, strlen(szGbk) * 3 + 10);
		int n = MultiByteToWideChar(CP_ACP, 0, szGbk, -1, NULL, 0);
		// 字符数乘以 sizeof(WCHAR) 得到字节数  
		WCHAR *str1 = new WCHAR[sizeof(WCHAR)* n];
		// 转换  
		MultiByteToWideChar(CP_ACP,  // MultiByte的代码页Code Page  
			0,            //附加标志，与音标有关  
			szGbk,        // 输入的GBK字符串  
			-1,           // 输入字符串长度，-1表示由函数内部计算  
			str1,         // 输出  
			n             // 输出所需分配的内存  
			);

		// 再将宽字符（UTF-16）转换多字节（UTF-8）  
		n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);
		WideCharToMultiByte(CP_UTF8, 0, str1, -1, tmp, n, NULL, NULL);
		lua_pushlstring(L, tmp, strlen(tmp));
		delete[]str1;
		str1 = NULL;
		delete[]tmp;
		return 1;
	}

	int lm_u82a(lua_State *L)
	{
		DWORD memAddress = lua_tonumber(L, -2);
		size_t len = lua_tonumber(L, -1);
		char szUtf8[2048] = { 0 };
		memcpy(szUtf8, (void *)memAddress, len);
		int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);
		char *tmp = new char[n + 10];
		memset(tmp, 0, n + 10);
		WCHAR * wszGBK = new WCHAR[sizeof(WCHAR)* n];
		memset(wszGBK, 0, sizeof(WCHAR)* n);
		MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, wszGBK, n);

		n = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, tmp, n, NULL, NULL);

		delete[]wszGBK;
		wszGBK = NULL;
		lua_pushstring(L, tmp);
		delete[]tmp;
		return 1;
	}

	int lm_i2f(lua_State *L)
	{
		DWORD v = lua_tonumber(L, 1);
		auto tv = *(float *)&v;
		lua_pushnumber(L, tv);
		return 1;
	}

	int lm_f2i(lua_State *L)
	{
		float v = lua_tonumber(L, 1);
		auto tv = *(DWORD *)&v;
		lua_pushnumber(L, tv);
		return 1;
	}

	int lm_towstr(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushlstring(L, (char *)addrss, wcslen((wchar_t *)addrss) * 2);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			return 0;
		}
		return 1;
	}

	int lm_tostr(lua_State *L)
	{
		__try{
			DWORD addrss = (DWORD)lua_tonumber(L, 1);
			lua_pushstring(L, (char *)addrss);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			return 0;
		}
		return 1;
	}
	//windows 内核对象
	int lm_closehandle(lua_State *L){
		HANDLE hc = lua_touserdata(L,1);
		__try{
			CloseHandle(hc);
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
		}
		return 0;
	}

	int lm_createevent(lua_State *L){
		HANDLE hEvent = CreateEventA(NULL, lua_toboolean(L, 1), lua_toboolean(L, 2), lua_tostring(L, 3));
		lua_pushlightuserdata(L, hEvent);
		return 1;
	}

	int lm_createmutex(lua_State *L){
		HANDLE hMutex = CreateMutexA(NULL, lua_toboolean(L, 1), lua_tostring(L, 2));
		lua_pushlightuserdata(L, hMutex);
		return 1;
	}

	int lm_setevent(lua_State *L){
		HANDLE hEvent = lua_touserdata(L, 1);
		SetEvent(hEvent);
		return 0;
	}

	int lm_signalandwait(lua_State *L){
		HANDLE hEvent = lua_touserdata(L, 1);
		HANDLE hEvent2 = lua_touserdata(L, 2);
		DWORD ret = SignalObjectAndWait(hEvent, hEvent2, lua_tointeger(L, 3), FALSE);
		lua_pushnumber(L, ret);
		return 1;
	}


	int lm_waithandle(lua_State *L){
		DWORD ret = WaitForSingleObject(lua_touserdata(L, 1), lua_tointeger(L, 2));
		lua_pushinteger(L, ret);
		return 1;
	}

	int lm_waitmultihandle(lua_State *L){
		HANDLE arrHandle[MAX_PATH] = { 0 };
		DWORD dwCount = lua_gettop(L) - 2;
		for (int i = 0; i < dwCount; ++i){
			arrHandle[i] = lua_touserdata(L, i + 3);
		}
		DWORD ret = WaitForMultipleObjects(dwCount, arrHandle, lua_tointeger(L, 1), lua_tointeger(L, 2));
		lua_pushinteger(L, ret);
		return 1;
	}

	int lm_createmapfile(lua_State *L)
	{
		const char *filename = lua_tostring(L, 1);
		int sz = lua_tointeger(L, 2);
		HANDLE hmapfile = CreateFileMappingA(INVALID_HANDLE_VALUE,
			NULL,
			PAGE_READWRITE,
			0,
			sz,
			filename);
		if (NULL == hmapfile)
		{
			return 0;
		}
		LPVOID pmapfile = MapViewOfFile(hmapfile,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			0);
		if (NULL == pmapfile) {
			CloseHandle(hmapfile);
			return 0;
		}
		lua_pushlightuserdata(L, hmapfile);
		lua_pushlightuserdata(L, pmapfile);
		return 2;
	}

	int lm_closemapfile(lua_State *L)
	{
		HANDLE hmapfile = lua_touserdata(L, 1);
		LPVOID pmapfile = lua_touserdata(L, 2);
		auto ok = UnmapViewOfFile(pmapfile) && CloseHandle(hmapfile);
		lua_pushboolean(L,ok);
		return 1;
	}

	int lm_getmodulecalladdress(lua_State *L)
	{
		const char* mname = lua_tostring(L, 1);
		DWORD callAddress = 0;
		if (lua_isnumber(L, 2)){
			DWORD index = (DWORD)(lua_tonumber(L, 2));
			callAddress = (DWORD)GetProcAddress(GetModuleHandleA(mname), (char *)(index));
		}
		else{
			callAddress = (DWORD)GetProcAddress(GetModuleHandleA(mname), lua_tostring(L, 2));
		}
		lua_pushnumber(L, callAddress);
		return 1;
	}

	int lm_getmodulebase(lua_State *L)
	{
		const char* name = lua_tostring(L, -1);
		HMODULE  hFind = GetModuleHandleA(name);
		MODULEINFO mi;
		memset(&mi, 0, sizeof(MODULEINFO));
		GetModuleInformation(GetCurrentProcess(), hFind, &mi, sizeof(mi));
		lua_pushnumber(L, (DWORD)hFind);
		lua_pushnumber(L, mi.SizeOfImage);
		return 2;
	}

	int lm_createsection(lua_State *L)
	{
		CRITICAL_SECTION *pNewCs = new CRITICAL_SECTION;
		InitializeCriticalSection(pNewCs);
		lua_pushlightuserdata(L, pNewCs);
		return 1;
	}

	int lm_closesection(lua_State *L){
		CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
		if (pNewCs == NULL){ return 0; };
		DeleteCriticalSection(pNewCs);
		pNewCs = NULL;
		return 0;
	}

	int lm_entersection(lua_State *L){
		CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
		if (pNewCs == NULL){ return 0; };
		EnterCriticalSection(pNewCs);
		return 0;
	}

	int lm_leavesection(lua_State *L){
		CRITICAL_SECTION *pNewCs = (CRITICAL_SECTION *)lua_touserdata(L, 1);
		if (pNewCs == NULL){ return 0; };
		LeaveCriticalSection(pNewCs);
		return 0;
	}

	int lm_qcall(lua_State *L)
	{
		__try{
			DWORD callAddress = (DWORD)lua_tonumber(L, 1);
			DWORD ecxAddress = (DWORD)lua_tonumber(L, 2);
			int retTpye = (int)lua_tonumber(L, 3);
			int len = (int)lua_gettop(L) - 3;
			DWORD argArray[10] = { 0 };//最多10个参数
			for (int i = 0; i < len; i++)
				argArray[i] = (DWORD)lua_tonumber(L, i + 4);
			int retInt = 0;
			retInt = _CommonCall<int>(callAddress, ecxAddress, argArray, len);
			if (retTpye == E_V_UNICODE){
				USES_CONVERSION;
				lua_pushstring(L, W2A((wchar_t *)retInt));
			}
			else if (retTpye == E_V_ASCII){
				lua_pushstring(L, (char *)retInt);
			}
			else if (retTpye == E_V_BYTE){
				lua_pushnumber(L, (byte)retInt);
			}
			else if (retTpye == E_V_WORD){
				lua_pushnumber(L, (WORD)retInt);
			}
			else if (retTpye == E_V_DWORD){
				lua_pushnumber(L, (DWORD)retInt);
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER){
			PRT("lm_qcall error");
			lua_pushnil(L);
			//lua_pushnumber(L, 0);
		}
		return 1;
	}
/*
	int lm_qcall(lua_State *L)
	{
		DWORD retInt;
		size_t len;
		DWORD _EAX = lua_tointeger(L, 1);
		DWORD _EBX = lua_tointeger(L, 2);
		DWORD _ECX = lua_tointeger(L, 3);
		DWORD _EDX = lua_tointeger(L, 4);
		DWORD _ESI = lua_tointeger(L, 5);
		DWORD _EDI = lua_tointeger(L, 6);
		DWORD callAddress = lua_tointeger(L, 7);
		auto restoreesp = lua_toboolean(L, 8);
		auto argArray = lua_tolstring(L, 9, &len);
		int argn = len / 4;
		__asm pushad
		__asm
		{
				mov ecx, argn
				mov edx, 0
				mov eax, argArray
			PUSHARG :
				cmp edx, ecx
				jae GAMECALL
				push[eax + edx * 4]
				inc edx
				jmp PUSHARG
			GAMECALL :
				cmp _EAX, 0x9876541
				je CALL_EAX
				cmp _EBX, 0x9876541
				je CALL_EBX
				cmp _ECX, 0x9876541
				je CALL_ECX
				cmp _EDX, 0x9876541
				je CALL_EDX
				cmp _ESI, 0x9876541
				je CALL_ESI
				cmp _EDI, 0x9876541
				je CALL_EDI
			CALL_EAX:
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov eax, callAddress
				SAFECALL(eax)
				jmp ENDCODE
			CALL_ESI :
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov esi, callAddress
				SAFECALL(esi)
				jmp ENDCODE
			CALL_EBX :
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov ebx, callAddress
				SAFECALL(ebx)
				jmp ENDCODE
			CALL_ECX:
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov ecx, callAddress
				SAFECALL(ecx)
				jmp ENDCODE
			CALL_EDX :
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov edx, callAddress
				SAFECALL(edx)
				jmp ENDCODE
			CALL_EDI :
				mov eax, _EAX
					mov ebx, _EBX
					mov ecx, _ECX
					mov edx, _EDX
					mov esi, _ESI
					mov edi, _EDI
				mov edi, callAddress
				SAFECALL(edi)
				jmp ENDCODE
			ENDCODE :
				mov retInt, eax
		}
		if (restoreesp)
			__asm add esp, len
		__asm popad
		lua_pushnumber(L, retInt);
		return 1;
	}*/
	
	int lm_call(lua_State *L)
	{
		luaL_error(L, "lm_call 暂时废弃");
		int qcallret = 0;
		//g_fSysCall = [&L, &qcallret](){qcallret = lm_qcall(L); };
		g_bSysFlag = true;
		WaitForSingleObject(g_hCallEvent, INFINITE);
		return qcallret;
	}

	int lm_dofile(lua_State *L)
	{
		auto fname = lua_tostring(L, 1);
		auto ret = luaL_dofile(L, fname);
		if (ret == 0){
			return 0;
		}
		lua_error(L);
		return 1;
	}


	int lm_loadfile(lua_State *L)
	{
		auto fname = lua_tostring(L, 1);
		auto ret = luaL_loadfile(L, fname);
		if (ret == 0){
			return 1;
		}
		lua_pushboolean(L, 0);
		lua_insert(L, -2);
		return 2;
	}

	static const luaL_Reg mskfuncs[] =
	{
		{ "dofile", lm_dofile },
		{ "loadfile", lm_loadfile },
		{ "pid", lm_pid },
		{ "tid", lm_tid },
		{ "tick", lm_tick },
		{ "currentdir", lm_currentdir },
		
		{ "exit", lm_exit },
		{ "syscall", lm_syscall },
		{ "closemapfile", lm_closemapfile },
		{ "createmapfile", lm_createmapfile },
		{ "print", lm_print },
		{ "qcall", lm_qcall },
		{ "call", lm_call },

		{ "newthread", lm_newthread },
		{ "dword", lm_todword },
		{ "word", lm_toword },
		{ "byte", lm_tobyte },
		{ "bool", lm_tobool },
		{ "float", lm_tofloat },
		{ "double", lm_todouble },
		{ "data", lm_todata },
		{ "str", lm_tostr },
		{ "wstr", lm_towstr },
		{ "longlong", lm_tolonglong },

		{ "clonglong", lm_changetolonglong },
		{ "cdouble", lm_changetodouble },

		{ "i2f",lm_i2f },
		{ "f2i", lm_f2i },
		{ "u82a", lm_u82a },
		{ "a2u8", lm_a2u8 },
		{ "a2w", lm_a2w },
		{ "w2a", lm_w2a },

		{ "new", lm_new },
		{ "delete", lm_delete },
		{ "sleep", lm_sleep },

		{ "sdata", lm_setdata },
		{ "copydata", lm_copydata },
		{ "sdword", lm_setdword },
		{ "sword", lm_setword },
		{ "sbyte", lm_setbyte },
		{ "sbool", lm_setbool },
		{ "sfloat", lm_setfloat },
		{ "sdouble", lm_setdouble },
		{ "slonglong", lm_setlonglong },

		{ "signalandwait", lm_signalandwait },
		{ "setevent", lm_setevent },
		{ "getcalladdress", lm_getmodulecalladdress },
		{ "createevent", lm_createevent },
		{ "createmutex", lm_createmutex },
		{ "waithandle", lm_waithandle },
		{ "waitmultihandle", lm_waitmultihandle },
		{ "closehandle", lm_closehandle },
		{ "getmodulebase", lm_getmodulebase },
		{ "createsection", lm_createsection },
		{ "closesection", lm_closesection },
		{ "entersection", lm_entersection },
		{ "leavesection", lm_leavesection },
		{ NULL, NULL }
	};
#ifdef _VER_RELEASE
}
#endif
int InitLuaHelper(lua_State *L){
#if LUA_VERSION_NUM == 501
	luaL_register(L,"msk",mskfuncs);
#else if LUA_VERSION_NUM == 503
	luaL_newlib(L, mskfuncs);
#endif
	//luaL_setfuncs(L, mskfuncs,);
#ifndef _ISSUE
	lua_pushboolean(L, 0);
#else
	lua_pushboolean(L, 1);
#endif
	lua_setfield(L,-2, "g_issue");
	if (g_hCallEvent == NULL)
		g_hCallEvent = CreateEventW(NULL, FALSE, FALSE, NULL);
	if (g_dwSafeCallJmp == 0)
	{
		static UCHAR g_ucSafeCallJmpSaved[16] = { 0 };
		static int s_iRet;
		g_dwSafeCallJmp = (DWORD)GetModuleHandleW(L"user32.dll") + 0x57edb;
		DWORD old;
		VirtualProtect((LPVOID)g_dwSafeCallJmp, 16, PAGE_EXECUTE_READWRITE, &old);
		memcpy(g_ucSafeCallJmpSaved, (void *)g_dwSafeCallJmp, 15);
		BYTE *pByte = (BYTE *)g_dwSafeCallJmp;
		*pByte = 0x5b;//pop eax                                  			;  弹出返回地址
		pByte++;
		*pByte = 0x89;
		pByte++;
		*pByte = 0x1d;
		pByte++;
		*(DWORD *)pByte = (DWORD)&s_iRet;
		pByte += 4;
		*pByte = 0x5b;
		pByte++;
		*pByte = 0xff;
		pByte++;
		*pByte = 0xd3;
		pByte++;
		*pByte = 0x8b;
		pByte++;
		*pByte = 0xdb;
		pByte++;
		*pByte = 0xff;
		pByte++;
		*pByte = 0x25;
		pByte++;
		*(DWORD *)pByte = (DWORD)&s_iRet;
		pByte += 4;
		VirtualProtect((LPVOID)g_dwSafeCallJmp, 16, old, &old);
	}
	return 1;
}
