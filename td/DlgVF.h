#pragma once


// CDlgVF dialog

class CDlgVF : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgVF)

public:
	CDlgVF(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgVF();

// Dialog Data
	enum { IDD = IDD_VF };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
private:
	lua_State *l_;
public:
	afx_msg void OnBnClickedLog();
	afx_msg void OnBnClickedReg();
	afx_msg void OnBnClickedChage();
	afx_msg void OnBnClickedUnbind();
	lt::table AskData();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
