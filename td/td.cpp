
// td.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "td.h"
#include "tdDlg.h"

#include <TlHelp32.h>

#include "../macro/macro.h"
#include "../mskhelpertools/mskhelptools.h"
#include "DlgVF.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTdApp

BEGIN_MESSAGE_MAP(CTdApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTdApp 构造

CTdApp::CTdApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CTdApp 对象

CTdApp theApp;


DWORD GetProcessidFromName(LPCTSTR name)
{
	PROCESSENTRY32 pe;
	DWORD id = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	pe.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnapshot, &pe))
		return 0;
	while (1)
	{
		if (_wcsicmp(pe.szExeFile, name) == 0)
		{
			id = pe.th32ProcessID;
			break;
		}
		pe.dwSize = sizeof(PROCESSENTRY32);
		if (Process32Next(hSnapshot, &pe) == FALSE)
			break;
	}
	CloseHandle(hSnapshot);
	return id;
}

#include <Psapi.h>
#include "ProcessNotification.h"
#pragma optimize( "g", off )
BOOL CTdApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CWinApp::InitInstance();
	
	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}
	HRESULT hr;
	hr = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hr))
	{
		AfxMessageBox(_T("COM组件初始化失败1"));
		return FALSE;
	}
	hr = CoInitializeSecurity(
		NULL,                      // Security descriptor    
		-1,                        // COM negotiates authentication service
		NULL,                      // Authentication services
		NULL,                      // Reserved
		RPC_C_AUTHN_LEVEL_DEFAULT, // Default authentication level for proxies
		RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation level for proxies
		NULL,                        // Authentication info
		EOAC_NONE,                   // Additional capabilities of the client or server
		NULL);                       // Reserved

	if (FAILED(hr))
	{
		AfxMessageBox(_T("COM组件初始化失败2"));
		return FALSE;
	}
	VMProtectBeginVirtualization("appInit");
#ifdef _DEBUG
	AfxBeginThread(ProcessMonitor, NULL);
#else
	{
		//自动改名 和清理旧文件
		char szFileFullPath[MAX_PATH], szProcessName[MAX_PATH];
		::GetModuleFileNameA(NULL, szFileFullPath, MAX_PATH);//获取文件路径
		auto nfname = theTools.GetRandomString(5) + ".exe";
		MoveFileExA(szFileFullPath, nfname.c_str(), MOVEFILE_REPLACE_EXISTING);
		auto f = fopen("pname", "r");
		if (f != NULL){
			char buff[MAX_PATH] = {0};
			fread(buff, MAX_PATH, 1, f);
			if (StrCmpIA(nfname.c_str(), buff) != 0)
				DeleteFileA(buff);
			fclose(f);
		}
		f = fopen("pname", "w");
		if (f != NULL){
			fwrite(nfname.c_str(), nfname.size(), 1, f);
			fclose(f);
		}
	}
#endif
	//启动数据库
	if (GetProcessidFromName(_T("app2.exe")) == 0){
#ifdef _DEBUG
		auto hins = ShellExecuteA(0, "open", R"(..\控制台\app2.exe)", R"(..\控制台\app2.cfg)", "", SW_HIDE);
#else
		auto hins = ShellExecuteA(0, "open", "app2.exe", "app2.cfg", "", SW_HIDE);
#endif
		if (unsigned int(hins) <= 32){
			AfxMessageBox(_T("组件加载失败"));
			return FALSE;
		}
	}

	AfxEnableControlContainer();

	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO:  应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
	{
		//配合自动登陆的bat
		std::string strCommand = GetCommandLineA();
		if (strCommand.find("chongqi") != std::string::npos){
			g_autoRun = true;
		}
		if (strCommand.find("zidongdenglu") != std::string::npos){
			g_autoLog = true;
		}
	}

#if 0
	auto L = luaL_newstate();
	luaL_openlibs(L);
	const char *replaceStr = R"(dofile[[..\Script\PRELOAD.LUA]];dofile[[..\Data\UserScript\test.lua]])";
	auto ret = luaL_loadbuffer(L, replaceStr, strlen(replaceStr), "PELOAD");

#endif
#if  _ISSUE 
	//获取或生成机器标识码
	using namespace macro;
	TCHAR szPath[MAX_PATH] = { 0 };
	SHGetSpecialFolderPath(NULL, szPath, CSIDL_APPDATA, TRUE);
	CString strDir = szPath;
	strDir += "\\Microsoft";
	if (FALSE == PathIsDirectory(strDir)){
		BOOL bCreate = CreateDirectory(strDir, NULL);
	}
	strDir += "\\msk.cfg";
	if (PathFileExists(strDir) == FALSE){
		HANDLE hTFile = fCreateFileW(strDir, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		strDir += ":mscache";
		DWORD dWrite;
		const char *szWrite = "[配置]";
		WriteFile(hTFile, szWrite, strlen(szWrite), &dWrite, NULL);
		CloseHandle(hTFile);
		hTFile = fCreateFileW(strDir, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		std::string strDymac = theTools.GetRandomString(15);
		auto bCreate = WriteFile(hTFile, strDymac.c_str(), strDymac.size(), &dWrite, NULL);
		CloseHandle(hTFile);
	}
	VMProtectEnd();
	//开始验证
	CDlgVF dlg0;
	if (dlg0.DoModal() != IDOK){
		return FALSE;
	}
#endif
	//启动主窗口
	CTdDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}
	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}
#pragma optimize( "g", on )

