
// stdafx.cpp : 只包括标准包含文件的源文件
// td.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

bool g_autoRun = false;
bool g_autoLog = false;
DWORD _CreateFileW = (DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW") + 5;
HANDLE
WINAPI
MyCreateFileW(
LPCWSTR lpFileName,
DWORD dwDesiredAccess,
DWORD dwShareMode,
LPSECURITY_ATTRIBUTES lpSecurityAttributes,
DWORD dwCreationDisposition,
DWORD dwFlagsAndAttributes,
HANDLE hTemplateFile)
{
	__asm jmp _CreateFileW
}