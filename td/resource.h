//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by td.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TD_DIALOG                   102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_VF                          131
#define IDD_DIALOG1                     132
#define IDR_QQMENU                      133
#define IDC_EDIT1                       1000
#define EDIT_ZHANGHAO                   1000
#define IDC_KEY                         1000
#define IDC_BUTTON1                     1001
#define EDIT_MIMA                       1001
#define IDC_USER                        1001
#define BTN_ADD                         1003
#define BTN_DEL                         1004
#define EDIT_DUOKAI                     1005
#define COM_JIAOBEN                     1006
#define COM_DAQU                        1007
#define COM_FUWUQI                      1008
#define CHECK_GO                        1011
#define IDC_LIST1                       1012
#define BTN_TEST                        1013
#define EDIT_MULU2                      1014
#define EDIT_MULU1                      1015
#define BTN_MULU1                       1016
#define BTN_MULU2                       1017
#define EDIT_FANWEI_L                   1018
#define EDIT_FANWEI_H                   1019
#define BTN_CONTROLLOG                  1020
#define IDC_TESTGAME                    1023
#define BTN_PAUSE                       1024
#define BTN_LOG                         1025
#define BTN_CLEAR                       1025
#define BTN_CHAGE                       1028
#define BTN_REG                         1029
#define BTN_UNBIND                      1030
#define ID_QQ_32771                     32771
#define ID_QQ_RESETSTATUS               32772
#define ID_QQ_32773                     32773
#define ID_PAUSEQQ                      32774
#define ID_QQ_32775                     32775
#define ID_RESUMEGAME                   32776
#define ID_RESUMEQQ                     32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
