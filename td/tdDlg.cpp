
// tdDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "td.h"
#include "tdDlg.h"
#include "afxdialogex.h"
#include "ProcessNotification.h"
#include <string>
#include <vector>

#include <TlHelp32.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define WM_UPATE WM_USER+1
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#pragma comment(lib,R"(lua5.1.lib)")
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTdDlg 对话框



CTdDlg::CTdDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTdDlg::IDD, pParent)
	, m_strString(_T("")), L(m_luaState)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strString);
	DDX_Control(pDX, IDC_LIST1, m_listQQ);
}

BEGIN_MESSAGE_MAP(CTdDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(CHECK_GO, &CTdDlg::OnBnClickedGo)
	ON_BN_CLICKED(BTN_ADD, &CTdDlg::OnBnClickedAdd)
	ON_BN_CLICKED(BTN_DEL, &CTdDlg::OnBnClickedDel)
//	ON_CBN_SELENDOK(COM_DAQU, &CTdDlg::OnCbnSelendokDaqu)
//	ON_NOTIFY(NM_THEMECHANGED, COM_DAQU, &CTdDlg::OnNMThemeChangedDaqu)
ON_CBN_SELCHANGE(COM_DAQU, &CTdDlg::OnCbnSelchangeDaqu)
ON_BN_CLICKED(BTN_TEST, &CTdDlg::OnBnClickedTest)
ON_WM_TIMER()
ON_BN_CLICKED(BTN_MULU1, &CTdDlg::OnBnClickedMulu1)
ON_BN_CLICKED(BTN_MULU2, &CTdDlg::OnBnClickedMulu2)
ON_WM_DROPFILES()
ON_BN_CLICKED(IDC_TESTGAME, &CTdDlg::OnBnClickedTestgame)
ON_BN_CLICKED(BTN_PAUSE, &CTdDlg::OnBnClickedPause)
ON_BN_CLICKED(BTN_CLEAR, &CTdDlg::OnBnClickedClear)
ON_BN_CLICKED(BTN_CONTROLLOG, &CTdDlg::OnBnClickedControllog)
ON_NOTIFY(NM_RCLICK, IDC_LIST1, &CTdDlg::OnNMRClickList1)
ON_COMMAND(ID_QQ_RESETSTATUS, &CTdDlg::OnQqResetstatus)
ON_NOTIFY(NM_CLICK, IDC_LIST1, &CTdDlg::OnNMClickList1)
ON_COMMAND(ID_PAUSEQQ, &CTdDlg::OnPauseqq)
ON_COMMAND(ID_RESUMEQQ, &CTdDlg::OnResumeqq)
END_MESSAGE_MAP()


// CTdDlg 消息处理程序
static CTdDlg *g_mainDlg;
static int UpdateQQ(lua_State *L)
{
	g_mainDlg->m_listQQ.SetItemText(lua_tointeger(L, 1), 0, (const TCHAR*)CString(lua_tostring(L, 2)));
	return 0;
}

static int PrintStr(lua_State *L)
{
	auto str = lua_tostring(L, 1);
	if (str == nullptr) return 0;
#ifdef _DEBUG
	TRACE(str);
#else
	char msg[4096]; 
	sprintf_s(msg,"msk_tdc->%s",str); 
	OutputDebugStringA(msg);
#endif
	return 0;
}

static int InsetQQItem(lua_State *L)
{
	auto text = lua_tostring(L, 1);
	auto index = g_mainDlg->m_listQQ.InsertItem(g_mainDlg->m_listQQ.GetItemCount(), (const TCHAR*)CString(text));
	lua_pushinteger(L, index);
	return 1;
}

static int SetUiText(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	auto text = lua_tostring(L, 2);
	CString strText(text);
	g_mainDlg->GetDlgItem(iid)->SetWindowText(strText);
	return 0;
}

static int GetUiText(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	char buff[50];
	::GetDlgItemTextA(g_mainDlg->GetSafeHwnd(), iid, buff, 50);
	lua_pushstring(L, buff);
	return 1;
}

static int GetUiSel(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	auto text = lua_tostring(L, 2);
	auto ctrl = (CComboBox*)g_mainDlg->GetDlgItem(iid);
	lua_pushinteger(L, ctrl->GetCurSel());
	return 1;
}


static int AddComString(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	auto text = lua_tostring(L, 2);
	auto ctrl = (CComboBox*)g_mainDlg->GetDlgItem(iid);
	ctrl->AddString(CString(text));
	return 0;
}

static int SelectComString(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	auto text = lua_tostring(L, 2);
	auto ctrl = (CComboBox*)g_mainDlg->GetDlgItem(iid);
	ctrl->SelectString(-1,CString(text));
	return 0;
}

static int ClearComString(lua_State *L)
{
	int iid = (int)lua_tonumber(L, 1);
	auto ctrl = (CComboBox*)g_mainDlg->GetDlgItem(iid);
	while (ctrl->GetCount() > 0)
	{
		ctrl->DeleteString(ctrl->GetCount() - 1);
	}
	return 0;
}

static int U82a(lua_State *L)
{
	auto szUtf8 = lua_tostring(L, 1);
	int n = MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, NULL, 0);
	char *tmp = new char[n + 10];
	memset(tmp, 0, n + 10);
	WCHAR * wszGBK = new WCHAR[sizeof(WCHAR)* n];
	memset(wszGBK, 0, sizeof(WCHAR)* n);
	MultiByteToWideChar(CP_UTF8, 0, szUtf8, -1, wszGBK, n);

	n = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, tmp, n, NULL, NULL);

	delete[]wszGBK;
	wszGBK = NULL;
	lua_pushstring(L, tmp);
	delete[]tmp;
	return 1;
}

static int IsProcessIdRun(lua_State *L)
{
	std::vector<DWORD> pids;
	auto pidNum = lua_gettop(L);
	for (auto i = 0; i < pidNum;++i){
		pids.push_back(lua_tointeger(L, i + 1));
	}
	DWORD rid = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 pe;
	pe.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnapshot, &pe)){
		CloseHandle(hSnapshot);
		lua_pushboolean(L, 0);
		return 1;
	}
	while (1){
		pe.dwSize = sizeof(PROCESSENTRY32);
		for (auto id : pids){
			if (id == pe.th32ProcessID){
				CloseHandle(hSnapshot);
				lua_pushboolean(L, 1);
				return 1;
			}
		}
		if (Process32Next(hSnapshot, &pe) == FALSE)
			break;
	}
	CloseHandle(hSnapshot);
	lua_pushboolean(L, 0);
	return 1;
}

static int GetProcessNameNum(lua_State *L)
{
	CStringArray arrNames;
	auto pidNum = lua_gettop(L);
	for (auto i = 0; i < pidNum; ++i){
		arrNames.Add(CString(lua_tostring(L, i + 1)));
	}
	DWORD rid = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 pe;
	auto num = 0;
	pe.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnapshot, &pe)){
		CloseHandle(hSnapshot);
		lua_pushnumber(L, num);
		return 1;
	}
	while (1){
		pe.dwSize = sizeof(PROCESSENTRY32);
		for (auto i = 0; i < arrNames.GetSize(); ++i){
			if (StrStrIW(pe.szExeFile, arrNames.GetAt(i))){
				num++;
			}
		}
		if (Process32Next(hSnapshot, &pe) == FALSE)
			break;
	}
	CloseHandle(hSnapshot);
	lua_pushnumber(L, num);
	return 1;
}


static int ExitProcessName(lua_State *L)
{
	CStringArray arrNames;
	auto pidNum = lua_gettop(L);
	for (auto i = 0; i < pidNum; ++i){
		arrNames.Add(CString(lua_tostring(L, i + 1)));
	}
	DWORD rid = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 pe;
	auto num = 0;
	pe.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnapshot, &pe)){
		CloseHandle(hSnapshot);
		lua_pushnumber(L, num);
		return 1;
	}
	while (1){
		pe.dwSize = sizeof(PROCESSENTRY32);
		for (auto i = 0; i < arrNames.GetSize(); ++i){
			if (StrStrIW(pe.szExeFile, arrNames.GetAt(i))){
				auto handle = OpenProcess(PROCESS_TERMINATE, FALSE, pe.th32ProcessID);
				if (handle){
					TerminateProcess(handle, 0);
					CloseHandle(handle);
				}
			}
		}
		if (Process32Next(hSnapshot, &pe) == FALSE)
			break;
	}
	CloseHandle(hSnapshot);
	lua_pushnumber(L, num);
	return 1;
}

static int IsProcessNameRun(lua_State *L)
{
	CStringArray arrNames;
	auto pidNum = lua_gettop(L);
	for (auto i = 0; i < pidNum; ++i){
		arrNames.Add(CString(lua_tostring(L, i+1)));
	}
	DWORD rid = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 pe;
	pe.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hSnapshot, &pe)){
		CloseHandle(hSnapshot);
		lua_pushboolean(L, 0);
		return 1;
	}
	while (1){
		pe.dwSize = sizeof(PROCESSENTRY32);
		for (auto i = 0; i < arrNames.GetSize();++i){
			if (StrCmpIW(pe.szExeFile, arrNames.GetAt(i)) == 0){
			//if (StrStrIW(pe.szExeFile, arrNames.GetAt(i))){
				CloseHandle(hSnapshot);
				lua_pushboolean(L, 1);
				return 1;
			}
		}
		if (Process32Next(hSnapshot, &pe) == FALSE)
			break;
	}
	CloseHandle(hSnapshot);
	lua_pushboolean(L, 0);
	return 1;
}

static int mShellExecute(lua_State *L)
{
	auto apps = lua_tostring(L, 1);
	auto cmds = lua_tostring(L, 2);
#if 1
	SHELLEXECUTEINFOA ShExecInfo = { 0 };
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
	ShExecInfo.lpFile = apps;
	ShExecInfo.lpParameters = cmds;
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_SHOW;
	ShExecInfo.hInstApp = NULL; 
	if (ShellExecuteExA(&ShExecInfo)){
		if (lua_gettop(L) == 3)
			WaitForSingleObject(ShExecInfo.hProcess,INFINITE);
	}
	else{
		auto err = GetLastError();
		char buf[256];
		FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buf, 256, NULL);
		luaL_error(L, buf);
	}
	//ShellExecuteA(0, "open", apps, cmds, "", SW_SHOWNORMAL);
#else
	PROCESS_INFORMATION pi;
	STARTUPINFOA si;
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES;//|0x80000
	si.wShowWindow = SW_SHOWNORMAL;
	auto bProcess = CreateProcessA(apps, (char *)cmds, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	if (bProcess == FALSE){
		auto err = GetLastError();
		char buf[256];
		FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buf, 256, NULL);
		luaL_error(L, buf);
	}
	if (bProcess && lua_gettop(L) == 3){
		WaitForSingleObject(pi.hProcess, INFINITE);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
#endif
	return 0;
}

static int LExitProcess(lua_State *L)
{
	auto pid = lua_tointeger(L, 1);
	auto handle = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
	if (handle){
		TerminateProcess(handle,0);
		CloseHandle(handle);
	}
	return 0;
}

static int MiniSizeWIndow(lua_State *L)
{
	ShowWindow(g_mainDlg->GetSafeHwnd(), SW_SHOWMINIMIZED);
	return 0;
}

BOOL CALLBACK MyEnumWindowsProc(
	_In_ HWND   hwnd,
	_In_ LPARAM lParam
	)
{
	char wndClass[MAX_PATH];
	GetClassNameA(hwnd, wndClass, MAX_PATH);
	if (_strcmpi(wndClass, (const char *)lParam) == 0){
		ShowWindow(hwnd, SW_SHOWMINIMIZED);
	}
	return TRUE;
}
static int HideAllWindow(lua_State *L)
{
	auto wndClass = lua_tostring(L, 1);
	EnumWindows(MyEnumWindowsProc, (LPARAM)wndClass);
	return 0;
}
static int DeleteAllItems(lua_State *L)
{
	g_mainDlg->m_listQQ.DeleteAllItems();
	return 0;
}

static int MsgBox(lua_State *L)
{
	auto msg = lua_tostring(L, 1);
	auto ret = MessageBoxA(NULL, msg, "提示", MB_OKCANCEL);
	lua_pushinteger(L, ret);
	return 1;
}

static int GetCheck(lua_State *L)
{
	auto index = lua_tointeger(L, 1);
	auto ret = ListView_GetCheckState(g_mainDlg->m_listQQ.GetSafeHwnd(), index);
	lua_pushinteger(L, ret);
	return 1;
}

static int SetCheck(lua_State *L)
{
	WPARAM  ItemIndex = lua_tointeger(L, 1);
	BOOL bCheck = lua_tointeger(L, 2);
	ListView_SetItemState(g_mainDlg->m_listQQ.m_hWnd, ItemIndex,
		UINT((int(bCheck) + 1) << 12), LVIS_STATEIMAGEMASK);
	return 1;
}

static void show_error(const char *err)
{
	PRT("%s",err);
	MessageBoxA(NULL, err, "脚本错误", MB_OK);
}

static int RomoteWriteDword(lua_State *L)
{
	auto pid = lua_tointeger(L, 1);
	void *addr = (void *)lua_tointeger(L, 2);
	auto val = lua_tointeger(L, 3);
	auto handle = OpenProcess(PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, pid);
	if (NULL != handle){
		if (FALSE == WriteProcessMemory(handle, addr, &val, 4, NULL)){
			PRT("写入进程失败:%d", GetLastError());
		}
		CloseHandle(handle);
	}
	else{
		PRT("打开进程失败:%d", GetLastError());
	}
	return 0;
}


void MonitorGameClient(LONG, void *)
{

}
BOOL CTdDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	m_listQQ.SetExtendedStyle(m_listQQ.GetExtendedStyle() | LVS_EX_CHECKBOXES);
	//监测游戏进程创建
	AfxBeginThread(ProcessMonitor, NULL);

	// TODO:  在此添加额外的初始化代码
	m_luaState = luaL_newstate();
	luaL_openlibs(m_luaState);
	
	lt::def(m_luaState, "_ALERT", show_error);
#ifdef _DEBUG
	lua_pushstring(m_luaState, R"(..\控制台)");
	lua_setglobal(m_luaState, "g_dir");
#else
	char dir[MAX_PATH];
	GetCurrentDirectoryA(MAX_PATH, dir);
	lua_pushstring(m_luaState, dir);
	lua_setglobal(m_luaState, "g_dir");
#endif
	char text[50] = "账号出错";
	auto SIZE = sizeof text;
	WCHAR w[sizeof text] = { 0 };

	MultiByteToWideChar(CP_ACP, 0, text, -1, w, SIZE); // ANSI to UNICODE
	WideCharToMultiByte(CP_UTF8, 0, w, -1, text, SIZE, 0, 0); // UNICODE to UTF-8
	lua_pushinteger(L, GetCurrentProcessId());
	lua_setglobal(L, "g_pid");
	lua_pushboolean(L, g_autoRun);
	lua_setglobal(L, "g_autoRun");
	lua_pushstring(L, text);
	lua_setglobal(L, "g_accError");
#ifdef _ISSUE
	lua_pushboolean(L,1);
	lua_setglobal(L, "g_issue");
#endif
	
	//注册一下相关函数
	m_listQQ.InsertColumn(0, _T("..."), 0, 500);
	g_mainDlg = this;
	lua_register(m_luaState, "cRomoteWriteDword", RomoteWriteDword);
	lua_register(m_luaState, "cSetCheck", SetCheck);
	lua_register(m_luaState, "cGetCheck", GetCheck);
	lua_register(m_luaState, "cMsgBox", MsgBox);
	lua_register(m_luaState, "cUpdateQQ", UpdateQQ);
	lua_register(m_luaState, "cPrint", PrintStr);
	lua_register(m_luaState, "cInsetQQItem", InsetQQItem);
	lua_register(m_luaState, "cSetUiText", SetUiText);
	lua_register(m_luaState, "cGetUiText", GetUiText);
	lua_register(m_luaState, "cAddComString", AddComString);
	lua_register(m_luaState, "cU82a", U82a);
	lua_register(m_luaState, "cSelectComString", SelectComString);
	lua_register(m_luaState, "cClearComString", ClearComString);
	lua_register(m_luaState, "cIsProcessIdRun", IsProcessIdRun);
	lua_register(m_luaState, "cIsProcessNameRun", IsProcessNameRun);
	lua_register(m_luaState, "cShellExecute", mShellExecute);
	lua_register(m_luaState, "cExitProcess", LExitProcess);
	lua_register(m_luaState, "cGetUiSel", GetUiSel);
	lua_register(m_luaState, "cGetProcessNameNum", GetProcessNameNum);
	lua_register(m_luaState, "cMiniSizeWIndow", MiniSizeWIndow);
	lua_register(m_luaState, "cHideAllWindow", HideAllWindow);
	lua_register(m_luaState, "cDeleteAllItems", DeleteAllItems);
	lua_register(m_luaState, "cExitProcessName", ExitProcessName);
	//注册控件ID
#define REGISTER_ITEMID(iid) lua_pushnumber(m_luaState, (iid));lua_setglobal(m_luaState,#iid)
	REGISTER_ITEMID(COM_JIAOBEN);
	REGISTER_ITEMID(COM_DAQU);
	REGISTER_ITEMID(COM_FUWUQI);
	REGISTER_ITEMID(EDIT_ZHANGHAO);
	REGISTER_ITEMID(EDIT_MIMA);
	REGISTER_ITEMID(EDIT_MULU1);
	REGISTER_ITEMID(EDIT_MULU2);
	//REGISTER_ITEMID(EDIT_ZUDUI);
	REGISTER_ITEMID(EDIT_FANWEI_L);
	REGISTER_ITEMID(EDIT_FANWEI_H);
	REGISTER_ITEMID(EDIT_DUOKAI);
	BeginWaitCursor();
	SCOPE_EXIT(EndWaitCursor(););
#ifdef _DEBUG
	if (0 != luaL_dofile(L, R"(..\控制台\SCRIPT.lua)")){
#else
	if (0 != luaL_dofile(L, R"(SCRIPT.lua)")){
#endif
		std::string errMsg = "初始化失败:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}

#ifdef _ISSUE
	//这几个按钮仅用于测试
	GetDlgItem(IDC_TESTGAME)->ShowWindow(FALSE);
	GetDlgItem(BTN_PAUSE)->ShowWindow(FALSE);
	GetDlgItem(BTN_CLEAR)->ShowWindow(FALSE);
#endif // _ISSUE

	GetDlgItem(BTN_TEST)->ShowWindow(TRUE);
	if (g_autoRun){
		SetTimer(1001, 5000, NULL);
	}
	//lua_register(m_luaState, "cPrint", Print);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTdDlg::OnSysCommand(UINT nID, LPARAM lParam)
{

	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTdDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTdDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTdDlg::OnBnClickedSave()
{
	// TODO: Add your control notification handler code here
	
}


void CTdDlg::OnBnClickedGo()
{
	// TODO: Add your control notification handler code here
	PRT("ONGO");
	auto goCheck = IsDlgButtonChecked(CHECK_GO);
	if (goCheck == 1){
		lua_getglobal(m_luaState, "OnGo");
		if (0 != lua_pcall(m_luaState, 0, 1, 0)){
			std::string errMsg = "运行失败:";
			errMsg += lua_tostring(m_luaState, -1);
			MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
			lua_pop(m_luaState, 1);
			return;
		}
		auto goOk = lua_toboolean(L, -1);
		lua_pop(m_luaState, 1);
		if (!goOk){
			((CButton*)GetDlgItem(CHECK_GO))->SetCheck(FALSE);
			return;
		}
	}
	if (goCheck == 1){
		m_timerCount = 0;
		goCheck = 0;
		this->SetTimer(WM_UPATE, 10000, NULL);
	}else
	{
		this->KillTimer(WM_UPATE);
		goCheck = 1;
	}
	GetDlgItem(COM_JIAOBEN)->EnableWindow(goCheck);
	GetDlgItem(COM_DAQU)->EnableWindow(goCheck);
	GetDlgItem(COM_FUWUQI)->EnableWindow(goCheck);
	GetDlgItem(EDIT_ZHANGHAO)->EnableWindow(goCheck);
	GetDlgItem(EDIT_MIMA)->EnableWindow(goCheck);
	GetDlgItem(EDIT_MULU1)->EnableWindow(goCheck);
	GetDlgItem(EDIT_MULU2)->EnableWindow(goCheck);
	GetDlgItem(BTN_MULU1)->EnableWindow(goCheck);
	GetDlgItem(BTN_MULU2)->EnableWindow(goCheck);
	GetDlgItem(BTN_ADD)->EnableWindow(goCheck);
	GetDlgItem(BTN_DEL)->EnableWindow(goCheck);
	GetDlgItem(BTN_TEST)->EnableWindow(goCheck);
	//GetDlgItem(EDIT_ZUDUI)->EnableWindow(goCheck);
	GetDlgItem(EDIT_FANWEI_L)->EnableWindow(goCheck);
	GetDlgItem(EDIT_FANWEI_H)->EnableWindow(goCheck);
	GetDlgItem(EDIT_DUOKAI)->EnableWindow(goCheck);
}


void CTdDlg::OnBnClickedAdd()
{
	// TODO: Add your control notification handler code here
	auto index = m_listQQ.InsertItem(m_listQQ.GetItemCount(), _T(""));
	lua_getglobal(m_luaState, "OnQQAdd");
	lua_pushinteger(m_luaState,index);
	if (0 != lua_pcall(m_luaState, 1, 0, 0)){
		std::string errMsg = "添加失败:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnBnClickedDel()
{
	// TODO: Add your control notification handler code here
	lua_getglobal(m_luaState, "OnQQClear");
	if (0 != lua_pcall(m_luaState, 0, 0, 0)){
		std::string errMsg = "删除失败:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	
}


//void CTdDlg::OnCbnSelendokDaqu()
//{
//	// TODO: Add your control notification handler code here
//	
//}


//void CTdDlg::OnNMThemeChangedDaqu(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	// This feature requires Windows XP or greater.
//	// The symbol _WIN32_WINNT must be >= 0x0501.
//	// TODO: Add your control notification handler code here
//	*pResult = 0;
//	lua_getglobal(m_luaState, "OnDaquChange");
//	if (0 != lua_pcall(m_luaState, 0, 0, 0)){
//		std::string errMsg = "错误:";
//		errMsg += lua_tostring(m_luaState, -1);
//		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
//		lua_pop(m_luaState, 1);
//	}
//}


void CTdDlg::OnCbnSelchangeDaqu()
{
	// TODO: Add your control notification handler code here
	auto ctrl = (CComboBox*)g_mainDlg->GetDlgItem(COM_DAQU);
	auto sel = ctrl->GetCurSel();
	lua_getglobal(m_luaState, "OnDaquChange");
	lua_pushinteger(m_luaState, sel);
	if (0 != lua_pcall(m_luaState, 1, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnBnClickedTest()
{
	// TODO: Add your control notification handler code here
	lua_getglobal(m_luaState, "OnTest");
	if (0 != lua_pcall(m_luaState, 0, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


BOOL CTdDlg::DestroyWindow()
{
	// TODO: Add your specialized code here and/or call the base class
	lua_getglobal(m_luaState, "OnExit");
	lua_pcall(m_luaState, 0, 0, 0);
	CDialogEx::PostNcDestroy();
	return CDialogEx::DestroyWindow();
}


void CTdDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == 1001){
		((CButton*)GetDlgItem(CHECK_GO))->SetCheck(TRUE);
		OnBnClickedGo();
		KillTimer(1001);
	}
	if (nIDEvent == WM_UPATE && IsDlgButtonChecked(CHECK_GO) == TRUE){
		lua_getglobal(m_luaState, "OnUpdate");
		lua_pushinteger(m_luaState, m_timerCount);
		if (0 != lua_pcall(m_luaState, 1, 0, 0)){
			lua_pop(m_luaState, 1);
		}
		m_timerCount++;
	}
	CDialogEx::OnTimer(nIDEvent);
}

BOOL GetfolderPath(CString &folderPath)
{
	BROWSEINFO bi;
	TCHAR buf[MAX_PATH];

	//初始化入口参数bi
	bi.hwndOwner = NULL;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = buf;	//此参数如为NULL则不能显示对话框
	bi.lpszTitle = _T("选择路径");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.lpfn = NULL;
	bi.iImage = 0;

	LPITEMIDLIST pIDList = SHBrowseForFolder(&bi);	//调用显示选择对话框
	if (pIDList)
	{
		SHGetPathFromIDList(pIDList, buf);			//取得文件夹路径到buf里
		folderPath = buf;		//将路径保存在一个CString对象里
		return TRUE;
	}
	return FALSE;
}


void CTdDlg::OnBnClickedMulu1()
{
	// TODO: Add your control notification handler code here
	CString path;
	if (GetfolderPath(path)){
		GetDlgItem(EDIT_MULU1)->SetWindowText(path);
	}
}


void CTdDlg::OnBnClickedMulu2()
{
	// TODO: Add your control notification handler code here
	CString path;
	if (GetfolderPath(path)){
		GetDlgItem(EDIT_MULU2)->SetWindowText(path);
	}
}




void CTdDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: Add your message handler code here and/or call default
	UINT count;
	char filePath[MAX_PATH];
	count = DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0);   //系统的API函数         
	if (count)
	{
		for (UINT i = 0; i < count; i++)
		{
			DragQueryFileA(hDropInfo, i, filePath, MAX_PATH);  //API函数                             
			lua_getglobal(m_luaState, "OnDropFile");
			lua_pushstring(m_luaState, filePath);
			if (0 != lua_pcall(m_luaState, 1, 0, 0)){
				std::string errMsg = "错误:";
				errMsg += lua_tostring(m_luaState, -1);
				MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
				lua_pop(m_luaState, 1);
			}
		}
	}
	DragFinish(hDropInfo);   //API函数
	CDialogEx::OnDropFiles(hDropInfo);
}


BOOL CTdDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CTdDlg::OnBnClickedBtnSetmulu()
{
	// TODO: Add your control notification handler code here
	lua_getglobal(m_luaState, "OnMulu");
	if (0 != lua_pcall(m_luaState, 0, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnBnClickedTestgame()
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_listQQ.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;
	auto nItem = m_listQQ.GetNextSelectedItem(pos);
	lua_getglobal(m_luaState, "OnTestGame");
	lua_pushinteger(m_luaState, nItem+1);
	if (0 != lua_pcall(m_luaState, 1, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnBnClickedPause()
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_listQQ.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;
	auto nItem = m_listQQ.GetNextSelectedItem(pos);
	lua_getglobal(m_luaState, "OnPauseGame");
	lua_pushinteger(m_luaState, nItem + 1);
	if (0 != lua_pcall(m_luaState, 1, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}

void CTdDlg::OnBnClickedClear()
{
	// TODO: Add your control notification handler code here
	POSITION pos = m_listQQ.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;
	auto nItem = m_listQQ.GetNextSelectedItem(pos);
	lua_getglobal(m_luaState, "OnClearGame");
	lua_pushinteger(m_luaState, nItem + 1);
	if (0 != lua_pcall(m_luaState, 1, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnBnClickedControllog()
{
	// TODO: Add your control notification handler code here
	lua_getglobal(m_luaState, "OnControlLog");
	if (0 != lua_pcall(m_luaState, 0, 0, 0)){
		std::string errMsg = "错误:";
		errMsg += lua_tostring(m_luaState, -1);
		MessageBoxA(NULL, errMsg.c_str(), "脚本错误", MB_OK);
		lua_pop(m_luaState, 1);
	}
}


void CTdDlg::OnNMRClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	POSITION pos = m_listQQ.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;
	CMenu menu, *pSubMenu;//定义下面要用到的cmenu对象  
	menu.LoadMenu(IDR_QQMENU);//装载自定义的右键菜单  
	pSubMenu = menu.GetSubMenu(0);//获取第一个弹出菜单，所以第一个菜单必须有子菜单  
	CPoint oPoint;//定义一个用于确定光标位置的位置  
	GetCursorPos(&oPoint);//获取当前光标的位置，以便使得菜单可以跟随光标  
	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN, oPoint.x, oPoint.y, this);   //在指定位置显示弹出菜单
}


void CTdDlg::OnQqResetstatus()
{
	// TODO: Add your command handler code here
	auto sitems = GetAllSelectItem();
	lt::call<void>(m_luaState, "OnResetStatus", sitems);
}


void CTdDlg::OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
	UINT flag;
	int nItem = m_listQQ.HitTest(pNMItemActivate->ptAction, &flag);
	flag &= LVHT_ONITEMSTATEICON;
	if (flag == 0 || pNMItemActivate->iSubItem != 0) return;
	auto sitems = GetAllSelectItem(pNMItemActivate->iItem);
	auto bCheck = m_listQQ.GetCheck(pNMItemActivate->iItem);
	if (bCheck == 1)
		bCheck = 0;
	else
		bCheck = 1;
	lt::call<void>(m_luaState, "OnCheckSet", sitems, bCheck);
}


lt::table CTdDlg::GetAllSelectItem(int eIincludeItem)
{
	POSITION pos = m_listQQ.GetFirstSelectedItemPosition();
	lt::table sitem(m_luaState);
	int tbIndex = 1;
	while (pos){
		int nItem = m_listQQ.GetNextSelectedItem(pos);
		if (nItem != eIincludeItem){
			sitem.set(tbIndex, nItem+1);
			tbIndex++;
		}
	}
	return sitem;
}


void CTdDlg::OnPauseqq()
{
	// TODO: Add your command handler code here
	auto sitems = GetAllSelectItem();
	lt::call<void>(m_luaState, "OnPauseQQ", sitems);
}


void CTdDlg::OnResumeqq()
{
	// TODO: Add your command handler code here
	auto sitems = GetAllSelectItem();
	lt::call<void>(m_luaState, "OnResumeQQ", sitems);
}
