
// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 从 Windows 头中排除极少使用的资料
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常可放心忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展


#include <afxdisp.h>        // MFC 自动化类



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // 功能区和控件条的 MFC 支持


#include <afxsock.h>            // MFC 套接字扩展







#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#ifdef _ISSUE
//#define USEVMP
#endif

#include "../macro/macro.h"
#include "../common/define.h"
using namespace macro;
#include "../lua51/lua.hpp"
#include "../luatinker/lua_tinker.h"
#define lt lua_tinker
HANDLE
WINAPI
MyCreateFileW(
LPCWSTR lpFileName,
DWORD dwDesiredAccess,
DWORD dwShareMode,
LPSECURITY_ATTRIBUTES lpSecurityAttributes,
DWORD dwCreationDisposition,
DWORD dwFlagsAndAttributes,
HANDLE hTemplateFile);
#define fCreateFileW  MyCreateFileW
#define fReadFile  ReadFile
#define fWriteFile  WriteFile
#define fCloseHandle  CloseHandle
#define fGetFileSize  GetFileSize
#define  fCreateToolhelp32Snapshot CreateToolhelp32Snapshot
#define  fProcess32First Process32First
#define  fProcess32Next Process32Next


#ifndef _ISSUE1
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#define PRTW(fmt,...) {wchar_t msg##__LINE__[4096]; swprintf_s(msg##__LINE__,L"msk_td->"fmt,__VA_ARGS__); OutputDebugStringW(msg##__LINE__);}
#else
#define PRT(fmt,...)
#define PRTW(fmt,...)
#endif

extern bool g_autoRun;
extern bool g_autoLog;