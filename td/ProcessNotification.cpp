#include "stdafx.h"
#include "../memoryModule/MemoryModule.h"
#include "ProcessNotification.h"

#include "../mskhelpertools/mskhelptools.h"

#define _WIN32_DCOM
#include <iostream>
using namespace std;
#include <comdef.h>
#include <Wbemidl.h>
#include <TlHelp32.h>

# pragma comment(lib, "wbemuuid.lib")



static BOOL PauseResumeThreadList(DWORD dwOwnerPID, bool bResumeThread)
{
	HANDLE        hThreadSnap = NULL;
	BOOL          bRet = FALSE;
	THREADENTRY32 te32 = { 0 };

	// Take a snapshot of all threads currently in the system. 

	hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap == INVALID_HANDLE_VALUE)
		return (FALSE);

	// Fill in the size of the structure before using it. 

	te32.dwSize = sizeof(THREADENTRY32);

	// Walk the thread snapshot to find all threads of the process. 
	// If the thread belongs to the process, add its information 
	// to the display list.

	if (Thread32First(hThreadSnap, &te32))
	{
		do
		{
			if (te32.th32OwnerProcessID == dwOwnerPID)
			{
				HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, te32.th32ThreadID);
				if (bResumeThread)
				{
					//cout << _T("Resuming Thread 0x") << cout.setf(ios_base::hex) << te32.th32ThreadID << '\n';
					ResumeThread(hThread);
				}
				else
				{
					//cout << _T("Suspending Thread 0x") << cout.setf(ios_base::hex) << te32.th32ThreadID << '\n';
					SuspendThread(hThread);
				}
				CloseHandle(hThread);
			}
		} while (Thread32Next(hThreadSnap, &te32));
		bRet = TRUE;
	}
	else
		bRet = FALSE;          // could not walk the list of threads 

	// Do not forget to clean up the snapshot object. 
	CloseHandle(hThreadSnap);

	return (bRet);
}
#include <sstream>
#pragma optimize( "g", off )
UINT ProcessMonitor(LPVOID)
{
	VMProtectBeginVirtualization("ProcessMonitor");
	PRT("开始监视");
	HRESULT hres;

	hres = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (FAILED(hres))
	{
		return hres;
	}

	IWbemLocator *pLoc = 0;
	HRESULT hr;

	hr = CoCreateInstance(CLSID_WbemLocator, 0,
		CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID *)&pLoc);

	if (FAILED(hr))
	{
		return hr;     // Program has failed.
	}

	IWbemServices *pSvc = 0;

	bstr_t strNetworkResource("ROOT\\CIMV2");

	hr = pLoc->ConnectServer(
		strNetworkResource,
		NULL, NULL, 0, NULL, 0, 0, &pSvc);

	if (FAILED(hr))
	{
		pLoc->Release();
		CoUninitialize();
		return hr;      // Program has failed.
	}

	// Set the proxy so that impersonation of the client occurs.
	hr = CoSetProxyBlanket(pSvc,
		RPC_C_AUTHN_WINNT,
		RPC_C_AUTHZ_NONE,
		NULL,
		RPC_C_AUTHN_LEVEL_CALL,
		RPC_C_IMP_LEVEL_IMPERSONATE,
		NULL,
		EOAC_NONE
		);

	if (FAILED(hr))
	{
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return hr;
	}

	bstr_t strLang("WQL");
	//监视taskmgr.exe进程创建
	bstr_t strQuery("SELECT * FROM __InstanceCreationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_Process' AND TargetInstance.Name = 'WuXia_Client.exe'");
	IEnumWbemClassObject* pResult = NULL;
	_variant_t vtProp;
	_variant_t pid;
	_variant_t vtName;
	IWbemClassObject* TargetInstance;
	PRT("等待进程启动");
	hr = pSvc->ExecNotificationQuery(strLang, strQuery, WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY, NULL, &pResult);
	if (SUCCEEDED(hr))
	{
		do{
			IWbemClassObject* pObject = NULL;
			ULONG lCnt = 0;
			hr = pResult->Next(WBEM_INFINITE, 1, &pObject, &lCnt);
			if (SUCCEEDED(hr) && pObject)
			{
				for (int i = 0; i < lCnt; ++i)
				{
					if (SUCCEEDED(pObject[i].Get(_bstr_t(L"TargetInstance"), 0, &vtProp, 0, 0)))
					{
						hr = ((IUnknown*)vtProp)->QueryInterface(IID_IWbemClassObject, (void **)&TargetInstance);
						if (SUCCEEDED(hr))
						{
							--hr = TargetInstance->Get(L"Name", 0, &vtName, NULL, NULL);							
							hr = TargetInstance->Get(L"ProcessId", 0, &pid, NULL, NULL);
							if (SUCCEEDED(hr))
							{
								//PRT("游戏登录器已启动:%ws\n", vtName.bstrVal);
								//SendMessage(g_hwndView, WM_GAME_CREATED, pid, 0);
								//Sleep(1000);
								std::stringstream ss;
								char cdir[MAX_PATH];
								GetCurrentDirectoryA(MAX_PATH, cdir);
								strcat_s(cdir,R"(\app3.dll)");
								ss << "app4.exe -p " << pid.ulVal << " -d " << cdir;
								PRT("app4 %s\n", ss.str().c_str());
#ifdef _DEBUG
								ShellExecuteA(0, "open", R"(..\控制台\app4.exe)", ss.str().c_str(), "", SW_HIDE);
#else
								ShellExecuteA(0, "open", R"(app4.exe)", ss.str().c_str(), "", SW_HIDE);
#endif
							}
							TargetInstance->Release();
						}
						((IUnknown*)vtProp)->Release();
					}
					pObject[i].Release();
				}
			}
		} while (true);
	}



	pSvc->Release();
	pLoc->Release();
	CoUninitialize();
	CoUninitialize();
	return 0;   // Program successfully completed.
	VMProtectEnd();
}
#pragma optimize( "g", on )