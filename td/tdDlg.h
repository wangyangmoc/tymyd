
// tdDlg.h : 头文件
//

#pragma once
#include "../lua51/lua.hpp"
#include "afxwin.h"
#include "afxcmn.h"

// CTdDlg 对话框
class CTdDlg : public CDialogEx
{
// 构造
public:
	CTdDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_TD_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	lua_State *m_luaState;
	lua_State *&L;
// 实现
protected:
	HICON m_hIcon;
	int m_timerCount = 0;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString m_strString;
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedGo();
	CListCtrl m_listQQ;
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedDel();
	virtual void PostNcDestroy();
//	afx_msg void OnCbnSelendokDaqu();
//	afx_msg void OnNMThemeChangedDaqu(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeDaqu();
	afx_msg void OnBnClickedTest();
	virtual BOOL DestroyWindow();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedMulu1();
	afx_msg void OnBnClickedMulu2();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedBtnSetmulu();
	afx_msg void OnBnClickedTestgame();
	afx_msg void OnBnClickedPause();
	afx_msg void OnBnClickedClear();
	afx_msg void OnBnClickedControllog();
	afx_msg void OnNMRClickList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnQqResetstatus();
	afx_msg void OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult);
	lt::table GetAllSelectItem(int eIincludeItem=-1);
	afx_msg void OnPauseqq();
	afx_msg void OnResumeqq();
};
