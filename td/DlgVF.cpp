// DlgVF.cpp : implementation file
//

#include "stdafx.h"
#include "td.h"
#include "DlgVF.h"
#include "afxdialogex.h"
#include "../mskhelpertools/mskhelptools.h"
#include <TlHelp32.h>
// CDlgVF dialog

static DWORD s_lastLogCount = 0;

IMPLEMENT_DYNAMIC(CDlgVF, CDialogEx)

CDlgVF::CDlgVF(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgVF::IDD, pParent)
{

}

CDlgVF::~CDlgVF()
{
}

void CDlgVF::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgVF, CDialogEx)
	ON_BN_CLICKED(BTN_LOG, &CDlgVF::OnBnClickedLog)
	ON_BN_CLICKED(BTN_REG, &CDlgVF::OnBnClickedReg)
	ON_BN_CLICKED(BTN_CHAGE, &CDlgVF::OnBnClickedChage)
	ON_BN_CLICKED(BTN_UNBIND, &CDlgVF::OnBnClickedUnbind)
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgVF message handlers

#pragma optimize( "g", off )
BOOL CDlgVF::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	VMProtectBeginVirtualization("VFInit");
	// TODO:  Add extra initialization here
	l_ = luaL_newstate();
	luaL_openlibs(l_);
	char szPathA[MAX_PATH] = { 0 };
	SHGetSpecialFolderPathA(NULL, szPathA, CSIDL_APPDATA, FALSE);
	strcat(szPathA, "\\Microsoft\\msk.cfg");
	lua_pushstring(l_, szPathA);
	lua_setglobal(l_, "gvConfigPath");

	char szUser[50] = { 0 };
	char szPwd[50] = { 0 };
	GetPrivateProfileStringA("vf", "user", "", szUser, 49, szPathA);
	GetPrivateProfileStringA("vf", "pwd", "", szPwd, 49, szPathA);

	SetDlgItemTextA(this->GetSafeHwnd(), IDC_USER, szUser);
	SetDlgItemTextA(this->GetSafeHwnd(), IDC_KEY, szPwd);

	auto userData = lt::table(l_, "gtUserData");
	char mac[20] = { 0 };
	if (FALSE == theTools.GetMac(mac)){
		AfxMessageBox(_T("发生了错误!"));
	}
	userData.set("mac", mac);
	BeginWaitCursor();
	SCOPE_EXIT(EndWaitCursor(););

	CString strDir(szPathA);
	strDir += ":mscache";
	HANDLE hFile = fCreateFileW(strDir, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	char szDymac[16] = { 0 };
	if (hFile != INVALID_HANDLE_VALUE){
		DWORD dwRead = (hFile, NULL);
		ReadFile(hFile, szDymac, 15, &dwRead, NULL);
		CloseHandle(hFile);
	}
	userData.set("dymac", szDymac);

#ifdef _DEBUG
	lua_pushstring(l_, R"(..\控制台)");
	lua_setglobal(l_, "g_dir");
#else
	char dir[MAX_PATH];
	GetCurrentDirectoryA(MAX_PATH, dir);
	lua_pushstring(l_, dir);
	lua_setglobal(l_, "g_dir");
#endif
#ifdef _DEBUG
	if (0 != luaL_dofile(l_, R"(..\控制台\SCRIPT2.lua)")){
		if (!g_autoLog)
			MessageBoxA(NULL, lua_tostring(l_,-1), "脚本错误", MB_OK);
		lua_pop(l_,1);
	}
#else
	if (0 != luaL_dofile(l_, R"(SCRIPT2.lua)")){
		if (!g_autoLog)
			MessageBoxA(NULL, lua_tostring(l_,-1), "脚本错误", MB_OK);
		lua_pop(l_, 1);
}
#endif
	if (g_autoLog)
		SetTimer(1001, 2000, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	VMProtectEnd();
	// EXCEPTION: OCX Property Pages should return FALSE
}
#pragma optimize( "g", on )

void CDlgVF::OnBnClickedLog()
{
	// TODO: Add your control notification handler code here
	
	auto userData = lt::table(l_, "gtUserData");
	userData.set("cmd", "log");
	auto retTable = AskData();
	s_lastLogCount = GetTickCount();
	if (strcmp("failed", retTable.get<const char *>("result")) == 0){
		if (!g_autoLog)
			MessageBoxA(NULL, retTable.get<const char *>("data"), "失败", MB_OK);
		return;
	}
	//auto globalT = lt::table(m_l, "_G");
	//globalT.set("gtServerData", retTable);
	auto szConfigPath = lt::get<const char *>(l_, "gvConfigPath");
	WritePrivateProfileStringA("vf", "user", userData.get<const char *>("user"), szConfigPath);
	WritePrivateProfileStringA("vf", "pwd", userData.get<const char *>("pwd"), szConfigPath);
	OnOK();
}


void CDlgVF::OnBnClickedReg()
{
	// TODO: Add your control notification handler code here
	{
		auto userData = lt::table(l_, "gtUserData");
		userData.set("cmd", "reg");
	}
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功" : "失败", MB_OK);

}


void CDlgVF::OnBnClickedChage()
{
	// TODO: Add your control notification handler code here
	auto userData = lt::table(l_, "gtUserData");
	userData.set("cmd", "rec");
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功" : "失败", MB_OK);
}


void CDlgVF::OnBnClickedUnbind()
{
	// TODO: Add your control notification handler code here
	// TODO:  在此添加控件通知处理程序代码
	if (MessageBoxA(NULL, "解绑扣2天,是否确定?", "解绑", MB_OKCANCEL) != IDOK)
		return;
	auto hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	USES_CONVERSION;
	if (INVALID_HANDLE_VALUE != hProcessSnap)
	{
		PROCESSENTRY32 pe32;
		pe32.dwSize = sizeof(PROCESSENTRY32);

		if (Process32First(hProcessSnap, &pe32))
		{
			do
			{
				if (pe32.th32ProcessID != GetCurrentProcessId() && (_tcscmp(pe32.szExeFile, _T("msk.exe")) == 0 || _tcscmp(pe32.szExeFile, _T("WuXia_Client.exe")) == 0))
				{
					auto hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
					if (hProcess)
					{
						TerminateProcess(hProcess, 0);
						CloseHandle(hProcess);
					}
				}
			} while (Process32Next(hProcessSnap, &pe32));
		}
		CloseHandle(hProcessSnap);
	}
	auto userData = lt::table(l_, "gtUserData");
	userData.set("cmd", "rebind");
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功" : "失败", MB_OK);

}

#pragma optimize( "g", off )
lt::table CDlgVF::AskData()
{
	VMProtectBeginVirtualization("AskData");
	BeginWaitCursor();
	SCOPE_EXIT(EndWaitCursor(););
	auto userData = lt::table(l_, "gtUserData");
	char szUser[50] = { 0 };
	char szPwd[50] = { 0 };
	GetDlgItemTextA(this->GetSafeHwnd(), IDC_USER, szUser, 49);
	GetDlgItemTextA(this->GetSafeHwnd(), IDC_KEY, szPwd, 49);
	userData.set("user", szUser);
	userData.set("pwd", szPwd);
	//auto sSata = lt::call<const char *>(m_l, "pickle", userData);
	return lt::call<lt::table>(l_, "AskData", userData);
	VMProtectEnd();
}
#pragma optimize( "g", on )

void CDlgVF::OnDestroy()
{
	lua_close(l_);
	// TODO: Add your message handler code here
}


void CDlgVF::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class
	lt::call<lt::table>(l_, "OnExit");
	CDialogEx::OnCancel();
}


void CDlgVF::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == 1001 && GetTickCount() - s_lastLogCount > 5000){
		PRT("TRY LOG...")
		OnBnClickedLog();
	}
	CDialogEx::OnTimer(nIDEvent);
}
