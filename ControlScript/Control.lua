﻿package.path = package.path..[[;D:\code\td\Data\lua\?.lua]]
local json = require "dkjson"
local qconfig = {}
local qqs = {}
function GetConfig(qq)
	local str = g_client:hget("config",qq)
	return str and json.decode(str)
end

function SetConfig(t,qq)
	local str = json.encode(t)
	g_client:hset("config",qq,str)
end

function FormatQQStr(t)
	return string.format("% 12d% 12s",t.
end

function OnQQAdd(qq,pwd)
	qqs[#qq+1] = {qq = qq,pwd = pwd}
	local fmtstr = FormatQQStr(qqs[#qq])
	msk.AddList(fmtstr,#qqs)
end

function OnQQDel(qq,pwd)
	qqs[#qq+1] = {qq = qq,pwd = pwd}
	for i=1,#qqs do
		msk.UpDateList(FormatQQStr(qqs[i]),i)
	end
end

function OnGoClick(jiaoben,daqu,fuwuqi,richang)
	qconfig["脚本"] = jiaoben
	qconfig["大区"] = daqu
	qconfig["服务器"] = fuwuqi
	qconfig["日常"] = richang
	for qq,t in pairs(qqs) do
		SetConfig(qconfig,qq)
	end
end