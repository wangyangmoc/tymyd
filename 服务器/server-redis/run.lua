require 'std'
require 'copas'
require 'coxpcall'
require 'logging.file'
require 'lfs'
require 'luasql.sqlite3'
require 'md5'
require 'base64'
require 'des56'
local socket = require("socket")
math.randomseed(os.time())

----[[ config init]]
local CfgHost = '*'
local CfgPort = 10001
local CfgPrintLimit = 200
local CfgCardChars = '1234567890_qwertyuiopasdfghjklzxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM'

----[[replace old function]]
local oldPrint = print
local numPrint = 0
--clear screen
local function print(...)
	numPrint = numPrint+1
	if numPrint >= CfgPrintLimit then
		os.execute 'cls'
	end
	oldPrint(...)
end
--avoids the addition of error position information
local trueAssert = assert
local function MskAssert(v,message,f,...)
	if v then return v end
	if f then f(...) end
	error('msk'..message,0)
end



----[[database init]]
print 'database init...'
local TableCreateString = {
	Logs = 'create table Logs (User text,Date datetime,Type text,Message text)',
	Users = 'create table Users (User text,Password text,Time datetime,StaticMac text,DynamicMac text)',
	Cards = 'create table Cards (Number text,CreateTime datetime,SellTime datetime,Time interger,User text)',
}
local env = assert(luasql.sqlite3())
local con = assert(env:connect('MskServer.db'))
do
	local cur = assert(con:execute("select * from sqlite_master where type = 'table'"))
	local row = cur:fetch ({})
	while row do
		--remove if exists
		TableCreateString[row[2]] = nil
		row = cur:fetch (row)
	end
	cur:close()
	--create the table if not exists
	for k,v in pairs(TableCreateString) do
		assert(con:execute(v))
	end
end
--close database
local savedCur = table.new(10)
for i =1,10 do
	savedCur[i] = false
end
function SqlClose()
	print 'Sql Close'
	for i=#savedCur,1,-1 do
		if savedCur[i] then
			savedCur[i]:close()
			savedCur[i] = false
		end
	end
	con:commit()
	con:close()
	env:close()
end
--log auto commit status
local stAutoCommit = true
function SetAutoCommit(ac)
	stAutoCommit = ac
	if ac == true then
		con:commit()
	end
	con:setautocommit(ac)
end
function CheckAutoCommit()
	for i=#savedCur,1,-1 do
		if savedCur[i] then
			savedCur[i]:close()
			savedCur[i] = false
		end
	end
	if stAutoCommit == false then
		stAutoCommit = true
		con:setautocommit(true)
	end
end

function Execute(cmd)
	cmd = cmd or 'null'
	--print('Execute:',cmd)
	local ret,err = con:execute(cmd)
	if not ret then
		error('sql error cmd:'..cmd..' err:'..err,2)
	end
	if type(ret) ~= 'number' then
		for i=1,#savedCur do
			if false == savedCur[i] then
				savedCur[i] = ret
				break
			end
		end
	else
		--print('effect:',ret)
	end
	return ret
end

----[[ logger init ]]
print 'logger init...'
local logger = logging.file("MskServerError%s.log", "%Y-%m-%d")
logger:setLevel(logging.WARN)

----[[ server init]]
local assert,pcall,xpcall = MskAssert,copcall,copxcall
sigData = {}
local f = io.open("sigvals.lua","rb")
if f then
	sigData = f:read("*all")
	f:close()
	sigData = eval(sigData) or {}
end
--print('sigData:')
--print(prettytostring(sigData))
--sigData = "i am sig data"
local function GetSigData(ct)
	local sig = sigData[ct.ver]
	if not sig and ct.debug then
		print('找不到版本:',ct.ver)
		for k,v in pairs(sigData) do
			sig = v
			break
		end
	elseif ct.debug then
		ct.debug = false
	end
	assert(sig,'错误的游戏版本:'..(ct.ver or "??"))
	return des56.crypt(sig,ct.dymac)
end

local function LogOption(ct,msg)
	msg = msg or '无'
	local sqlcmd = "insert into Logs(User,Date,Type,Message) values('%s',datetime('now','+8 hour'),'%s','%s')"
	sqlcmd = string.format(sqlcmd,ct.user,ct.cmd,msg)
	con:execute(sqlcmd)
end

local trueCopasSend = copas.send
function copas.send(s,data,ct)--it's not local
	--data:gsub('\n','|')
	data = pickle(data)
	data = base64.encode(data)..'\n'
	trueCopasSend(s,data)
end

local savedAdminCmd
local function ProcessSelf(ct)
	assert(ct.user == 'safasgjlkaskgjl98687585%&&asgklaghasg' and ct.pwd == 'asfasfas8758*&%2412846436asf','账号密码错误')
	print ('    管理员操作:',ct.user,ct.cmd)
	local skt = ct.client
	local ccmd = ct.cmd:match('+(%w*)')
	if ccmd then savedAdminCmd = ccmd == '' and nil or ccmd return '状态切换成功:'..(ccmd or 'nil') end
	savedAdminCmd = savedAdminCmd or ct.cmd
	if ct.cmd == 'bye' or ct.cmd == 'restart' then
		SqlClose()
		if ct.cmd == 'restart' then
			dofile 'Main.lua'
		else
			os.exit(0)
		end
		return 'ok'
	elseif ct.cmd == 'cls' then
		os.execute 'cls'
		return 'ok'
	end
	local adminret
	if savedAdminCmd == 'cc' then
		local cmds = ct.cmd:split('|')
		local num,tm = tonumber(cmds[1]),tonumber(cmds[2])
		assert(num and num >= 1 and tm and tm >=1,'生成卡号格式错误->cc|数量|时间')
		local cur = Execute(("select count(*) from Cards"))
		local maxid = cur:fetch()
		maxid = maxid or 0
		cur:close()
		local cardt = table.new(num)
		local tmpstr = table.new(15)
		for i=1,num do
			for j = 1,15 do
				tmpstr[j]= CfgCardChars[math.random(1,#CfgCardChars)]
			end
			cardt[i] = table.concat(tmpstr)..(maxid+i)
		end
		SetAutoCommit(false)
		local cmd = "insert into Cards(Number,CreateTime,Time) values('%s',datetime('now','+8 hour'),"..tm..")"
		local allok = false
		local cardnum = 0
		for i=1,#cardt do
			cardnum = cardnum + Execute((string.format(cmd,cardt[i])))
			--assert(con:execute(string.format(cmd,cardt[i])),'插入卡号失败',function () SetAutoCommit(true) con:rollback() end)
		end
		SetAutoCommit(true)
		print('共生成:'..cardnum..' 张 '..tm..'天 充值卡')
		for i=1,cardnum do
			print('-->',cardt[i])
		end
		local f = assert(io.open('Cards.txt','a'))
		f:write('管理员:',ct.user,' ',os.date(),' ',num ,'张X',tm, '天\n','\n')
		for i=1,#cardt do
			f:write(cardt[i],'\n')
		end
		f:write('\n','\n')
		f:close()
		adminret = cardt
	elseif savedAdminCmd =='sql' then
		local cur = Execute((ct.cmd))
		if type(cur) == 'number' then
			return cur..' lines effect'
		end
		local rstring = ""
		local rdata = {}
		local row = cur:fetch ({})
		while row do
			rdata[#rdata+1] = row
			--print("sql->",row)
			--rstring = rstring..tostring(row)..'|'
			row = cur:fetch ({})
		end
		cur:close()
		adminret = rdata
	elseif savedAdminCmd =='sig' then
		print ('    数据已更新',#ct.data,ct.cmd)
		local f = io.open("sigvals.lua","wb")
		sigData[ct.cmd] = ct.data
		f:write(pickle(sigData))
		f:close()
		adminret = '数据更新完毕:'..#ct.data..' '..ct.cmd
	end
	if ct.savedcmd~='sql' then
		pcall(LogOption,ct,'管理员')
	end
	return adminret or error('msk未知操作',0)
end

local function ProcessOther(ct)
	assert(ct.user and ct.pwd and ct.mac,'数据不全')
	local retstring
	local skt = ct.client
	if ct.cmd == 'reg' then
		assert(ct.dymac,'数据不全')
		assert(nil ~= ct.user:match("^[^']+$") and #ct.user > 0,'数据格式不正确0')
		assert(nil ~= ct.pwd:match("^[^']+$") and #ct.pwd > 0,'数据格式不正确1')
		assert(ct.mac:match('%w%w%-%w%w%-%w%w%-%w%w%-%w%w%-%w%w') ~= nil,'数据格式不正确2')
		local cur = Execute(("select User from Users where User = '"..ct.user.."'"))
		local ret = cur:fetch()
		assert(ret == nil,'用户已存在',cur.close,cur)
		local sqlcmd = "insert into Users(User,Password,Time,StaticMac,DynamicMac) values('"..ct.user.."','"..ct.pwd.."',datetime('now','+8 hour'),'"..ct.mac.."','"..ct.dymac.."')"
		--print(' 注册命令:',sqlcmd)
		cur = Execute((sqlcmd))
		assert(type(cur) == 'number','注册失败了')
		retstring = '注册成功:'..ct.user..'-'..ct.pwd
	elseif ct.cmd == 'rec' then
		ct.card = ct.card or ct.pwd
		local cur = Execute(("select DynamicMac from Users where User = '"..ct.user.."'"))
		local ret = cur:fetch()
		assert(ret ~= nil,'用户不存在',cur.close,cur)
		cur = Execute("select Time from Cards where Number = '"..ct.card.."' and SellTime is null")
		local ret = cur:fetch()
		--print('rc',ret,type(cur))
		assert(ret ~= nil,'卡号不存在/已使用',cur.close,cur)
		local tm = tonumber(ret)
		cur = Execute(("select Time from Users where User = '"..ct.user.."' and Time > datetime('now','+8 hour')"))
		local ret = cur:fetch()
		ret = ret or "now','+8 hour"
		ret = string.format("datetime('%s','+%d day')",ret,tm)
		SetAutoCommit(false)
		cur = Execute(("Update Cards set User = '"..ct.user.."',SellTime = datetime('now','+8 hour') where Number = '"..ct.card.."'"))
		cur = Execute(("Update Users set Time = "..ret.." where User = '"..ct.user.."'"))
		SetAutoCommit(true)
		retstring = '充值成功'
	elseif ct.cmd == 'log' then
		local cur = Execute(("select Password,StaticMac,DynamicMac from Users where User = '"..ct.user.."'"))
		local ret = cur:fetch({},'a')
		assert(ret ~= nil,'用户不存在')
		cur:close()
		assert(ret.Password == ct.pwd,'密码错误')
		local newbind = ret.DynamicMac == 'rebind'
		assert(ret.StaticMac == ct.mac or newbind,'非绑定机器')
		assert(ret.DynamicMac == ct.dymac or newbind,'非绑定几器')
		cur = Execute(("select Time from Users where User = '"..ct.user.."' and Time > datetime('now','+8 hour')"))
		local tm = cur:fetch()
		assert(tm ~= nil,'时间不足',cur.close,cur)
		if newbind then
			cur = Execute(("Update Users set DynamicMac = '"..ct.dymac.."',StaticMac = '"..ct.mac.."' where user = '"..ct.user.."'"))
		end
		--cur:close()
		local sig = GetSigData(ct)
		if ct.debug then
			tm = '错误的版本:'..(ct.ver or '??')
		end
		retstring = {time = tm,sig = sig}
	elseif ct.cmd == 'rebind' then
		local cur = Execute(("select Password,StaticMac,DynamicMac,Time from Users where User = '"..ct.user.."'"))
		local ret = cur:fetch({},'a')
		assert(ret ~= nil,'用户不存在')
		cur:close()
		assert(ret.Password == ct.pwd,'密码错误')
		assert(ret.StaticMac == ct.mac,'非绑定机器')
		assert(ret.DynamicMac == ct.dymac,'非绑定几器')
		local tmstr = "datetime('"..ret.Time.."','-2 day')"
		local sqlstr = "Update Users set Time = "..tmstr..", DynamicMac = 'rebind'".." where User = '"..ct.user.."'"
		print('解绑操作:',sqlstr)
		cur = Execute(sqlstr)
		retstring = '解绑成功,下次登陆将绑定新机器'
	end
	if nil == retstring then
		error('msk未知操作',0)
	else
		pcall(LogOption,ct,'成功')
	end
	return retstring
end


local function SocketHandle(sktt)
	local ip,port = sktt:getpeername()
	--print('  client connect:',ip)
	local skt = copas.wrap(sktt)
	local ProcessProc = ProcessOther
	local admin = true
	if not admin and ip == '127.0.0.1' then
		admin = true
		ProcessProc = ProcessSelf
	end
	local questContext
	while true do
		local data,err = skt:receive()
		--print('Get data:',data)
		if data == "quit" or data == nil then
			sktt:close()
		    break
		end
		data = base64.decode(data)
		questContext = eval(data)
		questContext.client = skt
		CheckAutoCommit()
		if admin and questContext.cmd == '+am' then
			admin = true
			ProcessProc = ProcessSelf
			local rt = {result = 'suc',data = '已切换至管理员测试模式'}
			skt:send(rt,ct)
		elseif admin and questContext.cmd == '+us' then
			ProcessProc = ProcessOther
			admin = true
			local rt = {result = 'suc',data = '已切换至用户测试模式'}
			skt:send(rt,ct)
		else
			local ret,err,noclose = pcall(ProcessProc,questContext)
			--print(' processproc ret:'..tostring(err))
			if not ret and err:sub(1,3) ~= 'msk' then
				logger:error(err)
				print ('  error:',err)
				err = {result = 'failed',data = '服务器脚本错误'}
			else
				err = {result = ret and 'suc' or 'failed',data = ret and err or err:sub(4,-1)}
			end
			skt:send(err,ct)
			if false == admin then
				sktt:close()
				break
			end
		end
		CheckAutoCommit()
	end
	CheckAutoCommit()
	--print('  client leave:',ip)
end

if GvServerStart == true then return end
local server = assert(socket.bind(CfgHost, CfgPort))
--print(server)
copas.addserver(server, SocketHandle)
print('server start:'..CfgHost..' '..CfgPort)
print('cur dir:',lfs.currentdir())
GvServerStart = true
copas.loop()

--[[
print('server start')
SocketHandle(server:accept())
print 'pragram exit...'
do return end
--]]


print 'pragram exit...'
SqlClose()
GvServerStart = false
