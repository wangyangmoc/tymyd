require 'std'
require 'copas'
require 'json'
require 'redis'
require 'base64'
local hp = require "gamehelper"

--处理配置
print('开始加载更新配置...')
local cfg = {}
local k,v
for data in io.lines("配置.txt") do
	k,v = data:match("(.+)=(.+)")
	cfg[k] = v
	print('    配置',k,'->',v)
end
assert(cfg['游戏路径'])
assert(cfg['大区索引'])
assert(cfg['游戏版本'])

--启动游戏
local gcfg = cfg['游戏路径']..' servergrp='..cfg['大区索引']..' servername='..(cfg['大区名称'] or '随便')
print('开始以配置启动游戏...',gcfg)
local pid = hp.createprocess(gcfg)
assert(pid)
local stm = tonumber(cfg['等待时间']) or 20

for i=stm,0,-5 do
	print('    等待游戏初始化...',i)
	hp.sleep(5)
end

local function hex(str)
	return string.format("%x",str)
end 
print('开始更新数据...')
local sig_data = {}
local t = {}
for k,v in pairs(sigNatureData) do
	local size
	base,size = hp.getmodulebase(pid,k)
	imageBase = base
	local data = hp.readmem(base,size)
	local val
	for k1,v1 in pairs(v) do
		val = searchSig(data,v1)
		t[k1] = val
		print('    获得数据 ',k1,'=',hex(val))
	end
end

local tKeys,tVals = {0,0},{1,1}
for k,v in pairs(sigNature) do
	if k == "moSkip" then
		tKeys[1] = k
		tVals[1] = v
	elseif k == "moTar" then
		tKeys[2] = k
		tVals[2] = v
	else
		tKeys[#tKeys+1] = k
		tVals[#tVals+1] = v
	end
end

local pt = ">"..string.rep('I',#tVals)
local sigdata = string.pack(pt,unpack(tVals))

local sokcet = require 'socket'
local new_skt = require'new_skt'
local base64 = base64
local copas = copas
local json = json


local sdata = {
	user = 'asf**(&',
	pwd = '4354asf&$#^%',
}
local rdata
local ver = cfg['游戏版本']
sdata.data = 'hset sig '..ver..' '..sigdata
local client = assert(socket.connect("23.239.198.7", 6080))
client = new_skt(client)
client:send(sdata)
rdata = client:receive()
print('数据更新结束','版本->',ver,'结果->',rdata)
client:close()
