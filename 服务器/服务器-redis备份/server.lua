require 'std'
require 'copas'
require 'json'
require 'redis'
require 'base64'
require 'des56'
require 'logging.file'
local bit = require "bit"
local bxor = bit.bxor
local new_skt = require'new_skt'
local base64 = base64
local copas = copas
local json = json
local redis = Redis
local db = redis.connect('127.0.0.1',6381)
local true_assert = assert
local function my_assert(v,message,...)
	if v then return v,message,... end
	error('msk'..message,0)
end

print 'logger init...'
local logger = logging.file("error.log")
logger:setLevel(logging.WARN)

----[[ server init]]
local assert,pcall,xpcall = my_assert,copcall,copxcall
local sigstr =  db:get('g_sig')
if sigstr then
	loadstring(sigstr)()
end
local function get_sigdata(ct)
	if sigstr == nil then
		loadstring(sigstr)()
	end
	assert(_g_sig,'没有数据')
	local sum = 0
	for i=1,#ct.dymac do
		sum = sum+ string.byte(ct.dymac:sub(i,i))
	end
	sum = sum * 1000
	--print("sum",sum)
	local newsig ={}
	for k,v in pairs(_g_sig) do
		if type(v) == "number" then
			v = bxor(v,sum)
			v = bxor(v,string.byte('m'))
		end
		newsig[k] = v
	end
	return newsig
end

--管理员命令处理
function admin_handler(skt)
	local ip,port = skt:getpeername()
	print('管理员进入 ',ip)
	local nskt = new_skt(skt)
	local data,err,cmd,si,cmds,ok
	while true do
		data,err = nskt:receive()
		if data == nil then break end
		if data.user ~= 'asf**(&' or data.pwd ~= '4354asf&$#^%' then
			break
		end
		cmds = {}
		for k in data.data:gmatch('%S+') do
			cmds[#cmds+1] = k
		end
		cmd = cmds[1]
		ok,data = pcall(db[cmd],db,select(2,unpack(cmds)))
		data = data or 'nil'
		nskt:send(data)
		if ok and cmd[1] == "set" and cmd[2] == "g_sig" then
			sigstr = cmd[3]
			loadstring(sigstr)()
		end
	end
	skt:close()
	print('管理员离开 ',ip)
end

local function process_user(ct,skt)
	assert(ct.user and ct.pwd and ct.mac,'数据不全')
	local retstring
	local skt = ct.client
	if ct.cmd == 'reg' then
		assert(ct.dymac,'数据不全')
		assert(nil ~= ct.user:match("^[^']+$") and #ct.user > 0,'数据格式不正确0')
		assert(nil ~= ct.pwd:match("^[^']+$") and #ct.pwd > 0,'数据格式不正确1')
		assert(ct.mac:match('%w%w%-%w%w%-%w%w%-%w%w%-%w%w%-%w%w') ~= nil,'数据格式不正确2')
		local user = db:hget('user',ct.user)
		assert(user == nil,'用户已存在')
		ct.time = 0
		ct.debug = nil
		local uinfo = json.encode(ct)
		assert(db:hset('user',ct.user,uinfo),'注册失败了')
		retstring = '注册成功:'..ct.user..'-'..ct.pwd
	elseif ct.cmd == 'rec' then
		local user = db:hget('user',ct.user)
		assert(user ~= nil,'用户不存在')
		user = json.decode(user)
		ct.card = ct.card or ct.pwd
		local cnum = ct.card:match('tzj_(%d+).+')
		local card = cnum and db:hget('card',cnum)
		card = card and json.decode(card)
		assert(card ~= nil and tonumber(card.selltime) == 0,'卡号不存在/已使用')
		local tm = tonumber(user.time)
		if tm < os.time() then tm = os.time() end
		tm = tm + tonumber(card.time)*24*60*60
		card.selltime = os.time()
		user.time = tm
		db:hset('user',ct.user,json.encode(user))
		db:hset('card',ct.card,json.encode(card))
		tm = tonumber(user.time)+8*60*60
		tm = os.date("%Y-%m-%d %H:%M",tm)
		retstring = '充值成功 到期时间:'..tm
	elseif ct.cmd == 'log' then
		local user = db:hget('user',ct.user)
		assert(user ~= nil,'用户不存在')
		user = json.decode(user)
		assert(user.pwd == ct.pwd,'密码错误')
		local newbind = user.dymac == 'rebind'
		assert(user.mac == ct.mac or newbind,'非绑定机器')
		assert(user.dymac == ct.dymac or newbind,'非绑定几器')
		assert(tonumber(user.time) > os.time(),'时间不足')
		if newbind then
			user.dymac = ct.dymac
			db:hset('user',ct.user,json.encode(user))
		end
		local tm
		local sig = get_sigdata(ct)
		if ct.debug then
			tm = '错误的版本:'..(ct.ver or '??')
		else
			tm = tonumber(user.time)+8*60*60
			tm = os.date("%Y-%m-%d %H:%M",tm)
		end
		retstring = {time = tm,sig = sig}
	elseif ct.cmd == 'rebind' then
		local user = db:hget('user',ct.user)
		assert(user ~= nil,'用户不存在')
		user = json.decode(user)
		assert(user.pwd == ct.pwd,'密码错误')
		assert(user.dymac ~= 'rebind','账号未绑定机器')
		assert(user.mac == ct.mac,'非绑定机器')
		assert(user.dymac == ct.dymac,'非绑定几器')
		assert(tonumber(user.time) > os.time(),'时间不足')
		user.dymac = 'rebind'
		user.time = tonumber(user.time)-2*24*60*60
		local uinfo = json.encode(user)
		local ret = db:hset('user',ct.user,uinfo)
		--assert(,'解绑失败')
		tm = tonumber(user.time)+8*60*60
		tm = os.date("%Y-%m-%d %H:%M",tm)
		retstring = '解绑成功,下次登陆将绑定新机器 到期时间:'..tm
	end
	if nil == retstring then
		error('msk未知操作',0)
	else
		pcall(LogOption,ct,'成功')
	end
	return retstring
end

--普通用户命令处理
function user_handler(skt)
	local ip,port = skt:getpeername()
	--print('管理员进入 ',ip)
	local nskt = new_skt(skt)
	local data,err = nskt:receive()
	if data then
		local ret,err,noclose = pcall(process_user,data,skt)
		if not ret and err:sub(1,3) ~= 'msk' then
			logger:error(err)
			print ('  error:',err)
			err = {result = 'failed',data = '服务器脚本错误'}
		else
			err = {result = ret and 'suc' or 'failed',data = ret and err or err:sub(4,-1)}
		end
		nskt:send(err)
	end
	skt:close()
	--print('管理员离开 ',ip)
end

--绑定服务器
local admin_server = assert(socket.bind('*', 6080))
copas.addserver(admin_server, admin_handler)
local user_server = assert(socket.bind('*', 10001))
copas.addserver(user_server, user_handler)
copas.loop()