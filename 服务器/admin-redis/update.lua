package.path = package.path..';./lua/?.lua'
package.cpath = package.cpath..';./clibs/?.dll'
require 'std'
require 'copas'
require 'json'
require 'redis'
require 'base64'
require 'search_pe'
require 'signature_data'
local hp = gamehelper or require "gamehelper"
--local hp = gamehelper
--处理配置
print('开始加载更新配置...')
local cfg = {}
local k,v
for data in io.lines("配置.txt") do
	k,v = data:match("(.+)=(.+)")
	cfg[k] = v
	print('  配置',k,'->',v)
end
assert(cfg['游戏路径'])
assert(cfg['大区索引'])
assert(cfg['游戏版本'])

--启动游戏
local gcfg = cfg['游戏路径']..' servergrp='..cfg['大区索引']..' servername='..(cfg['大区名称'] or '随便')
print('开始以配置启动游戏...',gcfg)
local pid = hp.createprocess(gcfg)
assert(pid)
local stm = tonumber(cfg['等待时间']) or 15

for i=1,stm,3 do
	print('  等待游戏初始化...',stm-i+1)
	hp.sleep(3)
end

local function hex(str)
	return string.format("%x",str)
end 
print('开始更新数据...')
local sig_data = {}
local t = {}
for k,v in pairs(sigNatureData) do
	local size
	base,size = hp.getmodulebase(pid,k)
	print('  搜索模块 名称,基址,大小->',k,hex(base),size)
	imageBase = base
	local data = hp.readmem(pid,base,size)
	local val
	for k1,v1 in pairs(v) do
		val = searchSig(data,v1)
		t[k1] = val
	end
end

local tKeys,tVals = {0,0},{1,1}
for k,v in pairs(t) do
	if k == "moSkip" then
		tKeys[1] = k
		tVals[1] = v
	elseif k == "moTar" then
		tKeys[2] = k
		tVals[2] = v
	else
		tKeys[#tKeys+1] = k
		tVals[#tVals+1] = v
	end
end
--0f b6 70 02
tKeys[#tKeys+1] = "packHookAddr"
tVals[#tVals+1] = 0x1391e9c
tKeys[#tKeys+1] = "packHookLen"
tVals[#tVals+1] = 5
tKeys[#tKeys+1] = "multiaddr1"
tVals[#tVals+1] = 0x01311000
tKeys[#tKeys+1] = "multiaddr2"
tVals[#tVals+1] = 0x01312000

print("-------------当前数据---------------")
for i=1,#tKeys do
	print('    ',tKeys[i],'=',hex(tVals[i]))
end

--value
local pt = ">I"..string.rep('I',#tVals)
local sigdata = string.pack(pt,#tVals,unpack(tVals))
--key
pt = ">I"..string.rep('a',#tKeys)
local keydata = string.pack(pt,#tKeys,unpack(tKeys))

local sokcet = require 'socket'
local new_skt = require'new_skt'
local base64 = base64
local copas = copas
local json = json

local client = assert(socket.connect("23.239.198.7", 6080))
client = new_skt(client)

local sdata = {
	user = 'asf**(&',
	pwd = '4354asf&$#^%',
}
local rdata
local ver = cfg['游戏版本']
sdata.args = {"hset","sig",ver,sigdata}
--sdata.data = 'hset sig '..ver..' '..sigdata
print('开始更新数据1',#sigdata)
client:send(sdata)
rdata = client:receive()
print('数据1更新结束','版本->',ver,'结果->',rdata)

sdata.args = {"hset","key",ver,keydata}
--sdata.data = 'hset key '..ver..' '..keydata
print('开始更新数据2',#keydata)
client:send(sdata)
rdata = client:receive()
print('数据2更新结束','版本->',ver,'结果->',rdata)
client:close()
