require 'std'
local oo = require 'loop.base'
local copas = require 'copas'
------------��װ����������--------------
local new_skt = oo.class()
function new_skt:__init(skt)
	local t= {_skt = copas.wrap(skt)}
	return oo.rawnew(self, t)
end
function new_skt:send(data)
	data = pickle(data)
	data = base64.encode(data)..'\n'
	self._skt:send(data)
end
function new_skt:receive(pattern)
	data = self._skt:receive(pattern)
	if data == nil then return data end
	data = base64.decode(data)
	return eval(data)
end

function new_skt:close()
	return self._skt:close()
end

return new_skt