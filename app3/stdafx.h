// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

//#define USEVMP
#include "../common/define.h"
// TODO: reference additional headers your program requires here
#ifndef _ISSUE1
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#define PRTW(fmt,...) {wchar_t msg##__LINE__[4096]; swprintf_s(msg##__LINE__,L"msk_td->"fmt,__VA_ARGS__); OutputDebugStringW(msg##__LINE__);}
#else
#define PRT(fmt,...)
#define PRTW(fmt,...)
#endif
