// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "../memoryModule/MemoryModule.h"
#include <fstream>
#include <Psapi.h>


typedef BOOL(*_DllInit)();
#ifdef USEVMP
#pragma optimize( "g", off )
#endif // USEVMP
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:{
		//加载核心DLL后推出
		VMProtectBeginVirtualization("dllLoad");
		char mdPath[MAX_PATH];
		GetModuleFileNameA(GetModuleHandleA("app3.dll"), mdPath, MAX_PATH);
		auto len = strlen(mdPath);
		mdPath[len - 5] = '1';
#ifndef _ISSUE
		PRT("Load lib:%s", mdPath);
		auto handle = LoadLibraryA(mdPath);
		auto DataInit = (_DllInit)GetProcAddress(handle, "_DllInit");

		PRT("DataInit:%p", DataInit);
		DataInit();
		return FALSE;
#else
		PRT("Load Mem:%s", mdPath);
		auto fp = fopen(mdPath, "rb");
		if (fp == NULL)
		{
			PRT("Can't open DLL file \"%s\".", mdPath);
			return FALSE;
		}
		fseek(fp, 0, SEEK_END);
		auto size = ftell(fp);
		auto data = (unsigned char *)malloc(size);
		fseek(fp, 0, SEEK_SET);
		fread(data, 1, size, fp);
		fclose(fp);
		auto handle = MemoryLoadLibrary(data);
		PRT("MoudleBase:%p", handle);
		auto DataInit = (_DllInit)MemoryGetProcAddress(handle, "_DllInit");//初始化
		PRT("DataInit:%p", DataInit);
		DataInit();
		free(data);
		return FALSE;
		//*/
#endif
		VMProtectEnd();
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
#ifdef USEVMP
#pragma optimize( "g", on )
#endif // USEVMP
