#pragma once
#include <memory>
#include <string>
#define MSKBG namespace MSK{
#define MSKED }
using namespace std;
class _ptr
{
public:
	_ptr(void *data = nullptr, size_t size = 0, bool del = false){ _data = data, _size = size; _del = del; }
	_ptr(_ptr &&rd){ _data = rd._data; _size = rd._size; _del = rd._del; rd._del = false; }
	~_ptr(){ if (false == _del) return; delete[]_data; _size = 0; }
	void *GetPtr(){ return _data; }
	size_t GetSize(){ return _size; }
private:
	void *_data;
	size_t _size;
	bool _del;
	//不允许的访问方式
	_ptr(_ptr &ref){}
};

class CTools
{

public:
	CTools();
	virtual ~CTools();
	_ptr GetData(string  strFileName);
	void SetData(string strFileName, _ptr pt);
	bool Inject(unsigned long pid, string dllpath, int noelevateprivs = 1, int freedll = 0, int sysdir = 0);
private:
	string m_strDataFile;
public:
	int GetMac(char * mac);
	int GetRandomNum(int iMin, int iMax);
	string GetRandomString(int num);
	void Assert(bool ret, const char *errormsg);
	string GetTimeString();
	void Log(const char *msg, const char *fname = "ErrorLog.txt");
};

extern CTools theTools;
