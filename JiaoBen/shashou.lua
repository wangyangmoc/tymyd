--暗杀
local sleep = msk.sleep
local bin2hex = msk.tools.bin2hex
local hex2bin = msk.tools.hex2bin
local hex2bin2= msk.tools.hex2bin2
local mconfig = msk.config
local SendStr = msk.pack.SendStr
local Sendf = msk.pack.Sendf
local Send = msk.pack.Send
local anshaMoneyId = g_sig.anshaMoneyId or 0xb4
local GetDailyData = msk.tools.GetDailyData
local SetDailyData = msk.tools.SetDailyData
local anshaDataBase = g_sig.anshaDataBase
local anshaUseCountOff= g_sig.anshaUseCountOff
local anshaCountOff = g_sig.anshaCountOff
local GetNextPageAnshaCall = g_sig.GetNextPageAnsha
local tools = msk.tools
local g_acceptPreTask = false--领取预悬赏的任务
local function __Time(tm)
	return os.date("%H:%M",tm)
end

local function __GetAnshCount()
	local base = msk.dword(anshaDataBase)
	local hasLastCount = msk.dword(base+anshaUseCountOff)
	local leftCount = msk.dword(base+anshaUseCountOff+4)
	if leftCount ~= 0 then
		local logTime = msk.dword(base+anshaUseCountOff)
		local ctime = qs.GetServerTime()
		logTime = os.date("*t",logTime)
		ctime = os.date("*t",ctime)
		if logTime.year < ctime.year then
			leftCount = 0
		elseif logTime.month < ctime.month then
			leftCount = 0
		elseif logTime.day < ctime.day then
			leftCount = 0
		end
	end
	return leftCount
	--return tonumber(GetDailyData("ansha_count")) or 0
end

local function AnshaWunian(id1,id2,pl)
	pl = pl or msk.npc.GetMainPlayer()
	for i=1,10 do
		if qs.CheckHasBuff(0x49c) then
			return true
		end
		pl:Select(id1,id2)
		if msk.api.UseSkillByName("暗杀") then
			sleep(0.5)
			return true
		end
		sleep(0.5)
	end
	if qs.CheckHasBuff(0x49c) == false then
		AnshaWunian2(id1,id2)
		sleep(0.5)
	end
end

local function GetAnshaRewardStar(id,dataAddr)
	if dataAddr == nil then
		local base = msk.gdata.GetDataBase("Data.Table.PveAssassinationTable")
		local head = msk.dword(base+0xc)
		local ed = msk.dword(base+0x10)
		
		for addr = head,ed-1,0x90 do
			if id == msk.dword(addr+0xc) then
				dataAddr = addr
				break
			end
		end
	end
	if dataAddr == nil then
		LogWarning("未知的奖励:%x",id)
		return 0 
	end
	local rewardAddr = msk.dword(dataAddr+0x30)
	rewardAddr = rewardAddr + 0x40--第二档
	local rwMoney = msk.dword(rewardAddr+0x8)
	if rwMoney == 40000 then 
		return 1 
	elseif rwMoney == 50000 then 
		return 2
	elseif rwMoney == 60000 then 
		return 3
	end
	rwMoney = msk.dword(rewardAddr+0xc)
	if rwMoney == 40000 then 
		return 1 
	elseif rwMoney == 50000 then 
		return 2
	elseif rwMoney == 60000 then 
		return 3
	end
	local rwItemNumAddr = msk.dword(rewardAddr+0x2c)
	if rwItemNumAddr == 0 then 
		LogWarning("未知的奖励:%x",id)
		return 0 
	end
	local rwItemNum = msk.dword(rwItemNumAddr)
	if rwItemNum == 5 then
		return 4
	elseif rwItemNum == 10 then
		return 5
	end
	LogWarning("未知的奖励:%x",id)
	return 0 
end

local __cacheData = {}
local function GetAssassinationData(id,idx)
	idx = idx or 0
	local t = __cacheData[id] and __cacheData[id][idx]
	if t then return t end
	t = {}
	local base = msk.gdata.GetDataBase("Data.Table.PveAssassinationTable")
	local head = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local name,mapname,mapid,x,y,posBase,star,mapPos
	for addr = head,ed-1,0x90 do
		if id == msk.dword(addr+0xc) then
			posBase = msk.dword(addr+0x20)
			posBase = posBase + idx*12
			if posBase >= msk.dword(addr+0x24) then return end
			name = msk.dword(addr+0x10)
			name = msk.str(name)
			mapid = msk.dword(addr+0x8c)
			mapPos = msk.dword(posBase)
			assert(mapid == msk.dword(posBase+4))
			star = GetAnshaRewardStar(id,addr)
			t ={name,mapid,mapPos,star}
			break
		end
	end
	if t then
		__cacheData[id] = __cacheData[id] or {}
		__cacheData[id][idx] = t
	end
	return t
end

local function GetAnshaData()
	local base = msk.dword(g_sig.anshaDataBase)
	local head = msk.dword(base + g_sig.anshaDataHead)
	local ed = msk.dword(base + g_sig.anshaDataHead+4)
	if ed <= head then return nil end
	local tm = msk.dword(base + g_sig.anshaTimeOff)
	local cutTm = msk.tick()
	local pid1 = msk.dword(head)
	local pid2 = msk.dword(head+4)
	local tid1 = msk.dword(head+8)
	local tid2 = msk.dword(head+0xc)
	local tmap = msk.dword(head+0x10)
	local qid = msk.dword(head+0x1c)
	return qid,tid1,tid2,tmap
end

local function MoveToAnsha()
	local x,y,name = msk.api.GetNearListNpc("Sha")
	if x and y then
		for i=1,3 do
			if msk.api.MoveToPosition(x,y,true) then
				return true
			end
			sleep(5)
		end
	end
	return msk.api.MoveToPosition(2185,961,nil,nil,nil,nil,10012)
end

local function BuyShengSiQian(iname,funcount)
	funcount = funcount or 0
	iname = iname or "中级生死签"
	local config = msk.config
	local buyCount = config.anshaCount+1
	local curCount = __GetAnshCount()
	buyCount = buyCount - curCount
	local _,t = msk.item.GetItemByName(iname)
	curCount = t and t.num or 0 
	buyCount = buyCount - curCount
	local buyret = "不需要购买"
	local hasBuy = false
	if buyCount > 0 then--如果需要买的话 先去收一下邮件
		if not msk.api.ProcessMail() then
			msk.api.QuitGame("取邮件失败")
		end
		--sleep(2)
		buyCount = buyCount + curCount
		_,t = msk.item.GetItemByName(iname)
		curCount = t and t.num or 0 
		buyCount = buyCount - curCount
		LogDebuging("需要购买:%d",buyCount)
		buyret,hasBuy = BuyPaiMaiItems(iname,buyCount,config.shengsiQianMoney)
		UIShowWindow("QSUIAuctionHousePanel",false)
		if hasBuy then
			sleep(10)
			if not msk.api.ProcessMail() then
				msk.api.QuitGame("取邮件失败")
			end
			_,t = msk.item.GetItemByName(iname) 
			if funcount < 10 and (not t or t.num <= 0) then
				LogWarning("买物品失败")
				return BuyShengSiQian(iname,funcount+1)
			end
		end
		
	end
	local ret = t and t.num > 0
	if not ret and buyret == "购买失败" then
		msk.api.QuitGame("购买生死签失败")
	end
	return ret,buyret
end

local function ExchangeShengSiQian(iname,ipos,costItem,npcName,costItemCount,realNpcName)
	iname = iname or "中级生死签"
	costItem = costItem or "中级百业令"
	npcName = npcName or "中级身份兑换商"
	costItemCount = costItemCount or 35
	ipos = ipos or 1
	local config = msk.config
	local buyCount = config.anshaCount+1
	local curCount = __GetAnshCount()
	buyCount = buyCount - curCount
	local _,t = msk.item.GetItemByName(iname)
	curCount = t and t.num or 0 
	buyCount = buyCount - curCount
	LogDebuging("需要兑换:%d",buyCount)
	if buyCount > 0 then--如果需要买的话 先去收一下邮件
		if not msk.api.ProcessMail() then
			msk.api.QuitGame("取邮件失败")
		end
		--sleep(2)
		msk.api.BuyIdentityItem(ipos,buyCount,costItemCount,costItem,npcName,realNpcName)
		sleep(1)
		_,t = msk.item.GetItemByName(iname) 
	else
		
	end
	return t and t.num > 0
end

function NextAnshaPage(page,ui)
	page = page or 1
	ui = ui or msk.ui.GetPaneByName("QSUIAssassinationInfoPanel",true)
	if ui == nil then
		LogWarning("暗杀面板无法获取")
		return false
	end
	msk.sdword(ui+0xc0,page)
	local bgCount = page*8-8
	msk.qcall(GetNextPageAnshaCall,ui,0,bgCount)
end

local function CheckQualification()
	if (__GetAnshCount()) >= msk.config.anshaCount then
		ScriptExit("次数完成")
	end
	local base = msk.dword(g_sig.liveNessBase)
	if msk.dword(base) < 20 then--功绩
		ScriptExit("没有功绩")
	end
	
	--功绩 次数
	if qs.GetMainplayerLevel() > 60 then
		if msk.item.GetItemByName("高级生死签") ~= nil or ExchangeShengSiQian("高级生死签",1,"高级百业令","高级身份兑换商",35,"身份高级兑换") then return true end
		local ok,ret = BuyShengSiQian("高级生死签") 
		return ok or ScriptExit(ret)
	elseif qs.GetMainplayerLevel() > 40 and qs.GetMainplayerLevel() <= 60 then
		if msk.item.GetItemByName("中级生死签") ~= nil or ExchangeShengSiQian("中级生死签",1,"中级百业令","中级身份兑换商",35,"身份中级兑换") then return true end
		local ok,ret = BuyShengSiQian("中级生死签") 
		return ok or ScriptExit(ret)
	elseif qs.GetMainplayerLevel() > 30 and qs.GetMainplayerLevel() <= 40 then
		return msk.item.GetItemByName("初级生死签") ~= nil or ExchangeShengSiQian("初级生死签",1,"初级百业令","初级身份兑换商",4,"身份初级兑换") or ScriptExit("生死签不足")
	end
	ScriptExit("未知条件")
end
local function __CheckStar(star)
	if star == 1 then
		return mconfig.anshaStar1 == 1
	elseif star == 2 then
		return mconfig.anshaStar2 == 1
	elseif star == 3 then
		return mconfig.anshaStar3 == 1
	elseif star == 4 then
		return mconfig.anshaStar4 == 1
	elseif star == 5 then
		return mconfig.anshaStar5 == 1
	end
	return false
end

local __lastItem = 0
local function AcceptAnSha(loopcount,bg,snum)
	if GetAnshaData() then return true end--已经有暗杀了
	CheckQualification()
	LogDebuging("准备接任务了...")
	local moveCount = 31
	repeat
		LogDebuging("移动到暗杀板...")
		if MoveToAnsha() then
			break
		end
		moveCount = moveCount -1
	until moveCount > 0
	if moveCount <=0 then
		msk.api.QuitGame("无法移动到暗杀板")
	end
	local atm =  tonumber(g_client:hget("ansha_atime",g_mainId)) or 0
	LogDebuging("上次接暗杀时间:%d %d",atm,os.time() - atm)
	--测试
	while os.time() - atm < 360 do
		sleep(5)
	end
	bg = bg or 0
	local pageNum = 8
	snum = snum or pageNum
	local npc = msk.npc.GetNpcByName("暗杀情报板")
	if npc == nil then
		LogWarning("没有 暗杀情报板")
		sleep(2)
		return AcceptAnSha()
	end
	local ui
	while ui == nil or UIIsVisible("QSUIAssassinationInfoPanel") == false do
		msk.api.MoveToPosition(npc.x,npc.y,true)
		npc:Open()
		sleep(1)
		ui = msk.ui.GetPaneByName("QSUIAssassinationInfoPanel",true)
	end
	if __lastItem < 0 then __lastItem = 0 end
	local page = 250--math.floor(__lastItem/8+1)
	qs.ShowFontWithFx("正在接取暗杀。。。") 
	loopcount = loopcount or 0--第一遍接6金的
	local curCount = __GetAnshCount()
	local ct = __star4
	local acceptCount = 0
	local allNum = 2000
	local ast,curAnshaIndex,allPage
	local flytm = msk.api.GetFlyCd()+qs.GetServerTime()+8*60
	local wtUseTm,wtLeftTm
	local totalWaitTm = mconfig.anshaPinlv
	LogDebuging("lasttm:%.2f",totalWaitTm)
	local usedItem = 0
	local invertPage = mconfig.invertPage
	local bgPage
	local pageDif = -1
	if invertPage == 0 then
		pageDif = 1
		page = 1
	end
	local lastAllNum = 2000
	local base = msk.dword(anshaDataBase)
	local tid1,tid2,stas,id,map,tm,tm2,name
	local idx,hd,ed
	local waitRefresh = false
	local pl = msk.npc.GetMainPlayer()
	local lastRefreshPage = 1
	local lastNotAcceptPage
	local pageLoop = 1
	local qqConfig =  msk.tools.GetConfig()
	while true do
		if pl.hp == 0 then
			msk.api.ReviveNear()
			return AcceptAnSha(loopcount+1)
		elseif msk.byte(ui+0x81) == 0 then--不显示了
			qs.ShowFontWithFx("UI消失 重来")
			return AcceptAnSha(loopcount+1)
		end
		allPage =  math.floor(allNum/8+1)
		bgPage = allPage-invertPage
		if bgPage < 1 then bgPage = 1 end
		if invertPage == 0 then--不倒序
			if page > allPage then
				qs.ShowFontWithFx("一轮结束 休息一下")
				--sleep(2)
				page = 1
				pageLoop = pageLoop+1
			end
			if allNum > lastAllNum then--刷新了!
				lastRefreshPage = math.floor(lastAllNum/8)-2
				qs.ShowFontWithFx("刷新了!")
				page = lastRefreshPage+3--allPage--新模式
			end
		else
			if allNum > lastAllNum then--刷新了!
				lastRefreshPage = math.floor(lastAllNum/8)-2
				if lastRefreshPage < 1 then lastRefreshPage = 1 end
				waitRefresh = false
				
				qs.ShowFontWithFx("刷新了!")
				--新模式 跳到刷新的初始页
				if qqConfig.index%2 == 1 then
					page = lastRefreshPage+3--allPage--新模式
					pageDif = 1--新模式
					LogDebuging("正序前翻")
				else
					--老模式 跳到刷新的最后页
					page = allPage
					LogDebuging("倒序后翻")
				end
			elseif waitRefresh then
				--sleep(0.5)
			end
			if page > allPage then
				if pageDif == 1 then
					page = allPage-1
				else
					page = allPage
				end
				pageDif = -1
			end
			if page < bgPage then
				pageLoop = pageLoop+1
				qs.ShowFontWithFx("等待刷新....")
				--sleep(2)
				waitRefresh = true
				lastRefreshPage = 1
				page = allPage
			elseif pageLoop > 1 and page < lastRefreshPage then
				qs.ShowFontWithFx("等待刷新....")
				--sleep(2)
				lastRefreshPage = 1
				waitRefresh = true
			end
		end
		lastAllNum = allNum
		wtUseTm = 0
		curAnshaIndex = msk.dword(ui+0xc4)
		NextAnshaPage(page,ui)
		--LogDebuging("page:%d",page)
		hd = 1
		ed = 1
		for i=1,10 do
			wtUseTm = wtUseTm + 0.1
			sleep(0.1)
			if msk.dword(ui+0xc4) ~= curAnshaIndex then
				allNum = msk.dword(base+ anshaCountOff)
				idx = 0
				hd = msk.dword(base+ anshaCountOff-20)
				ed = msk.dword(base+ anshaCountOff-16)
				break
			elseif wtUseTm > 2 then
				break
			end
		end
		--LogDebuging("位置:%d-%d 耗时:%.2f",curAnshaIndex,msk.dword(ui+0xc4),wtUseTm)
		if allNum > lastAllNum then
			wtLeftTm = 0.8-wtUseTm
		else
			wtLeftTm = totalWaitTm-wtUseTm
		end
		for addr = hd,ed-1,0x21 do
			tid1 = msk.dword(addr)
			tid2 = msk.dword(addr+4)
			--map = msk.dword(addr+8)
			tm = msk.dword(addr+0x10)
			stas = msk.byte(addr+21)
			id = msk.dword(addr+25)
			tm2 = msk.dword(addr+29)
			ast = GetAssassinationData(id)
			name,map = ast[1],ast[2]
			--and 10002 ~= map
			if false and __CheckStar(ast[4]) and stas == 1 and tm > flytm and (g_acceptPreTask or qs.GetServerTime() > tm2) then--id <= anshaMoneyId and  不去东越 太多卡点
				LogDebuging("找到合适对象:%d-%x:%s %x,%x",page,id,name,tid1,tid2)
				__AcceptAnSha(tid1,tid2,npc.id1,npc.id2)
				sleep(0.6)
				if GetAnshaData() then 
					sleep(2)
					g_client:hset("ansha_atime",g_mainId,os.time())
					qs.ShowFontWithFx(string.format("接取暗杀完毕:%d %s",id,name))
					SetDailyData("ansha_count",curCount+1)
					--g_client:hincrby(anshaDailyStr,g_qq,1)
					UIShowWindow("QSUIAssassinationInfoPanel",false)
					__lastItem = curAnshaIndex-usedItem
					sleep(1)
					return true 
				else
					if lastNotAcceptPage ~= page then
						lastNotAcceptPage = page
						acceptCount = acceptCount + 1
					end
					if acceptCount > 5 then
						qs.ShowFontWithFx("接取暗杀失败!")
						UIShowWindow("QSUIAssassinationInfoPanel",false)
						msk.api.QuitGame("接取暗杀失败")
						--ScriptExit("接取暗杀失败")
					else
						wtLeftTm = wtLeftTm-0.6
						LogDebuging("接单失败 翻页")
						break
					end
				end
			elseif stas ~= 1  then
				if stas ~= 1 then
					usedItem = usedItem + 1
				end
				LogDebuging("跳过:%d-%d %s %d星",page,idx,name,ast[4])
				ScriptExit("xx00")
			elseif __CheckStar(ast[4]) then
				LogDebuging("未知原因跳过:%s %d星",name,ast[4])
			end
			idx = idx + 1
		end
		sleep(wtLeftTm)
		page = page + pageDif
	end
	qs.ShowFontWithFx(string.format("接取暗杀失败"))
	UIShowWindow("QSUIAssassinationInfoPanel",false)
	sleep(5)
	__lastItem = curAnshaIndex-usedItem
	return AcceptAnSha(loopcount+1)
end

local function RunAnsha()
	local pos,_,_,qid1,qid2,tid1,tid2,x,y
	local qid,tmap
	local __UseSkill = msk.api.__UseSkill
	local tWantx,tWanty
	local name,map,mapPos
	local step1Ok 
	local posIndex = 0
	local tnpc,ast
	local tx,ty
	while true do
		qid,tid1,tid2,tmap = GetAnshaData()
		if tmap then--msk.api.ChangeMap(msk.api.GetCurrentMapId(),tmap)
			LogDebuging("监视暗杀:%x %x,%x",qid,tid1,tid2)
			step1Ok = true
			msk.api.Heal()
			for i=posIndex,10 do
				ast = GetAssassinationData(qid,i)
				if ast then
					map,mapPos = ast[2],ast[3]
				end
				if map then
					tnpc = msk.npc.GetNpcById(tid1,tid2)
					if tnpc == nil then
						tx,ty = msk.api.GetMapInsData(map,mapPos,true)
						if tx and ty and msk.api.ChangeMap(nil,map,nil,tx,ty) then
							msk.api.ChangeLine(1)
							msk.mcall(20,msk.api.MoveToPosition,tx,ty,true,nil,nil,nil,map)
							tnpc = msk.npc.GetNpcById(tid1,tid2)
						elseif msk.api.ChangeMap(nil,map) then
							msk.api.ChangeLine(1)
							--LogDebuging("找不到地点:%d-%d",map,mapPos)
							msk.api.MoveById(map,mapPos,nil,true)
							sleep(1)
							tnpc = msk.npc.GetNpcById(tid1,tid2)
						else
							LogDebuging("找不到地点:%d-%d",map,mapPos)
							sleep(3)
						end
					end
					if tnpc and msk.api.MoveToPosition(tnpc.x,tnpc.y,true,nil,nil,nil,map) and tnpc.id1 == tid1 and tnpc.id2 == tid2 then
						local pl = msk.npc.GetMainPlayer()
						if qs.CheckHasBuff(0x49c) == false then
							if tnpc.tarid1 ~= pl.id1 then
								pl:Select(tid1,tid2)
								sleep(0.1)
								LogDebuging("转移面向")
								while not tnpc:IsFaceTo(pl.x,pl.y) do
									pl:FaceTo(tnpc.x,tnpc.y)
									msk.api.SetMainPlayerMove(true)
									sleep(0.7)
								end
								msk.api.SetMainPlayerMove(false)
								while tnpc:IsFaceTo(pl.x,pl.y) do
									sleep(0.5)
								end
								LogDebuging("暗杀")
								AnshaWunian(tid1,tid2,pl)
								sleep(0.1)
							end
						end
						if qs.CheckHasBuff(0x49c) then
							LogDebuging("使用技能")
							local g_skillSlotOff =  msk.dword(g_sig.QSUISkillPanelSlotInfoBase+0x8)
							local _,_,off = msk.api.GetSlotIdByName("狙杀",g_skillSlotOff)
							local asTime = 30
							while off ~= 0 and tnpc.tarid1 ~= pl.id1 and asTime > 0 do
								pl:Select(tid1,tid2)
								if msk.api.__UseSkill(off-1) then
									sleep(0.5)
								else
									--LogDebuging("使用技能失败了:%x",off)
									if qs.CheckHasBuff(0x3f4) then
										msk.api.SetMainPlayerMove(true)
									else
										msk.api.SetMainPlayerMove(false)
									end
									sleep(0.5)
								end
								asTime = asTime - 0.5
							end 
							LogDebuging("战斗了:%x %x %x",off,tnpc.tarid1,pl.id1)
							tnpc = msk.npc.GetNpcById(tid1,tid2)
							for i=1,10 do
								if msk.api.FightNpc(tnpc,300) then
									break
								end
								sleep(0.1)
								tnpc = msk.npc.GetNpcById(tid1,tid2)
							end
						end
						posIndex = i
						break
					end
				end
			end
		else
			break
		end
	end
	sleep(2)
	LogDebuging("over")
end


local _noLeveUp = true
if msk.g_issue then
	_noLeveUp= false
end
if not _noLeveUp then
	if mconfig.lockLv41 == 1 then
		RunScriptFile("1-41")
	else
		RunScriptFile("liveup")
	end
end
local iid = msk.api.GetIdentityId()
if iid == 0 then
	msk.api.TransferIdentity()
	iid = msk.api.GetIdentityId()
end
if iid ~= qs.IDENTITY_TYPE_SHA_SHOU then
	ScriptExit("身份错误")
end

--学习一下天赋
local shashouSkills = {
	iid = 10,
	{49,1},--龙鳞刺
	{16,1},--外装.杀决长夜
	{9,1},--疾风
	{15,4},--信条
	{11,3},--利刃
	{10,3},--伺机
	{12,3},--致伤
}
if not msk.api.LearnIdentitySkills(shashouSkills) then
	LogDebuging("没有学完")
	RunScriptFile("hanghui")
	msk.api.LearnIdentitySkills(shashouSkills)
end

if not _noLeveUp and qs.GetMainplayerLevel() < 41 then
	ScriptExit("等级错误(41-60)")
end

g_cacheData.curScript = "shashou"
local weaPon = msk.item.GetEquipByPos(0)
if weaPon and weaPon.dura <= 300 then--耐久太低
	msk.api.BuyDrag(50,50)
	LogDebuging("就近修理装备:%d",weaPon.dura)
	msk.api.RepairAtNearlistNpc()
else
	if msk.api.BuyDrag(10,50) then
		msk.api.RepairAtNearlistNpc()
	end
end
--刷点功绩
if not _noLeveUp and msk.config.dailyLiveNess == 1 and g_cacheData.script ~= "liveup" and msk.tools.GetDailyData("dangkou_liveness") ~= "yes" then
	RunScriptFile("dangkou","liveness")
	msk.tools.SetDailyData("dangkou_liveness","yes")
end
--签到
if qs.GetMainplayerLevel() >= 41 and qs.CheckActorVip(0) and msk.tools.GetDailyData("qiandao") ~= "yes" then
	UIShowWindow("QSUIDailyCheckInPanel",true)
	if UIIsVisible("QSUIDailyCheckInPanel") then
		msk.ui.UIMsgCall("QSUIDailyCheckInPanel","A2C_GET_TODAY_AWARD")
		sleep(1)
		UIShowWindow("QSUIDailyCheckInPanel",false)
		sleep(1)
		msk.tools.SetDailyData("qiandao","yes")
	end
end
mconfig.anshaCount = 12
local xtData = msk.api.GetCareerData(10,15)
if xtData and xtData.level > 1 then
	mconfig.anshaCount = mconfig.anshaCount+xtData.level-1
end
if xtData and xtData.level > 1 then
	g_acceptPreTask = true
end
LogDebuging("当前暗杀次数:%d/%d",__GetAnshCount(),mconfig.anshaCount)
local bAccept = false
repeat
	g_cacheData.moveLock = true
	__lastItem = 0
	bAccept = AcceptAnSha()
	g_cacheData.moveLock = false
	if bAccept then
		RunAnsha()
	end
until false
