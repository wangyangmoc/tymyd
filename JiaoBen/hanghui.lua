local sleep = msk.sleep
local dailyQuestBase = g_sig.dailyQuestBase
local dqMt = {}
local dqOffset = {
	type = 0,
	count = 4,
	total = 8,
}
dqMt.__index = function(t,k)
	return msk.dword(dailyQuestBase + dqOffset[k])
end
local dq = {}
setmetatable(dq,dqMt)
if qs.GetMainplayerLevel() >= 36 then--暂时只支持减负令
	for i=1,100 do
		if dq.count >= dq.total then
			break
		end
		local _,_,bindPoint = msk.item.GetMoney()
		if qs.CheckActorVip(0) == false and bindPoint < 1280 and msk.item.UseItemByName("天涯会员30天") == false then
			break
		end
		sleep(1)
		if qs.CheckActorVip(0) == false and msk.item.UseItemByName("天涯会员30天") == false  then
			WaitMoneyProtect()
			BuyShangChengItem(0x74,1,1280)
			sleep(1)
			msk.item.UseItemByName("天涯会员30天")
			sleep(5)
		end
		_,_,bindPoint = msk.item.GetMoney()
		if qs.CheckActorVip(0) then
			if bindPoint < 100 then
				break
			end
			for j = dq.count,dq.total-1 do
				if bindPoint < 100 then
					break
				end
				if false == msk.item.UseItemByName("上班族减负令·贰") then
					WaitMoneyProtect()
					BuyShangChengItem(0x141,1,100)
					sleep(1)
					msk.item.UseItemByName("上班族减负令·贰")
				end
			end
		end
	end
else
	LogDebuging("行会日常放弃:%d",msk.config.pointDaily)
end
ScriptExit("成功")
