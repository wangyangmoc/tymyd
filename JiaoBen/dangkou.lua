--dangkou

local cmds = {...}
local fightPositions31 = {
	{3510,1804},
	{3673,1738},
	{3701,1677},
	{3729,1642},
	--{3655,1512},
	--{3533,1525},
	--{3430,1512},
}
local fightPositions41 = {
	{3287,2175},
	{3386,2196},
	{3407,2076},
	{3476,2087},
	{3529,2075},
	{3477,2034},
	{3400,1984},
}


local sleep = msk.sleep
local GetDailyData = msk.tools.GetDailyData
local SetDailyData = msk.tools.SetDailyData

local function EnterDangKou(lv)
	local npc = msk.npc.GetByName("江湖人士")
	if npc then
		__EnterDangKou(lv, npc.id1,npc.id2)
		return true
	end
	return false
end
local function CanDangKouBegin()
	local tm = os.date("*t",qs.GetServerTime())
	--LogDebuging("时机:%d",tm.min)
	if tm.min >= 20 and tm.min <= 25 then return true end
	if tm.min >= 40 and tm.min <= 45 then return true end
	if tm.min >= 0 and tm.min <= 5 then return true end
	--LogDebuging("时机不对:%d",tm.min)
	return false
end

local function GetDangkouCount()
	return tonumber(GetDailyData("dangkou_count")) or 0
end

local function __IsLvOk()
	return g_cacheData.dailyLv and g_cacheData.dailyLv <= msk.npc.GetMainPlayer().lv
end

local function __IsDangKouLogOk(eid)
	if GetDangkouCount() >= 10 then
		LogDebuging("荡口次数满：%d",GetDangkouCount())
		return true 
	end
	if cmds[1] == "liveness" then--这个是活跃的
		local base = msk.dword(g_sig.liveNessBase)
		LogDebuging("活跃:%d",msk.dword(base+4))
		return msk.dword(base+4) >= 750
	end
	return __IsLvOk() or (GetDailyData("dangkou_yueli") == "0" and GetDailyData("dangkou_xiuwei") == "0")--GetDailyData("dangkou") == "yes"
end

--ScriptExit("测试")
if cmds[1] ~= "liveness" and msk.item.UseItemByName("上班族减负令·叁") then
	ScriptExit("荡寇成功")
end
local dkCount = GetDangkouCount()
--LogDebuging("荡寇次数已满:%d %d",minNum,maxNum)
if __IsDangKouLogOk() then
	--printf("%s",__IsDangKouLogOk() and "dk ok" or "lv ok")
	ScriptExit("荡寇成功")
end
local eid
local moveOk
local dangkouLv = 0
local fightX,fightY
local pl
local dangkouData = {}
local function InitDangkouData()
	pl = msk.npc.GetMainPlayer()
	if pl.lv >= 51 then
		eid = 11147
	elseif pl.lv >= 41 then
		eid = 28011 --杭州荡寇·吴王陵擒水匪
		local idx = math.random(1,#fightPositions41)
		fightX,fightY = fightPositions41[idx][1],fightPositions41[idx][2]
		escapeX,escapeY = 3346,2086
	elseif pl.lv >= 31 then--东越
		eid = 11021
		local idx = math.random(1,#fightPositions31)
		fightX,fightY = fightPositions31[idx][1],fightPositions31[idx][2]
		--fightX = 3588
		--fightY = 1548
		escapeX,escapeY = 3363,1670
	end
end
local function MoveToDangKouNpc()
	local pl = msk.npc.GetMainPlayer()
	local fogName = qs.GetFogName(msk.api.GetCurrentMapId(),pl.x,pl.y)
	LogDebuging("当前 eid:%d 小地图:%s",eid,fogName)
	moveOk = false
	if eid == 11021 then
		if msk.api.ChangeMap(nil,10002,nil,305400,150500) then-- and msk.api.FlyToNearListTeleport(10002,305400,150500) then
			for i=1,5 do
				if msk.api.MoveToPosition(3054,1505,nil,nil,nil,nil,10002) then-- or (msk.api.MoveToPosition(3276,1528,nil,nil,nil,nil,10002) and msk.api.MoveToPosition(3054,1505,nil,nil,nil,nil,10002)) then
					moveOk = true
					break
				end
				sleep(1)
			end
		else
			if msk.api.MoveToPosition(2334,1298,nil,nil,nil,nil,10012) then
				moveOk = true
			end
		end
	elseif eid == 28011 then
		if msk.api.ChangeMap(nil,10010,nil,313500,215300) then--msk.api.FlyToNearListTeleport(10010,313500,215300) then
			for i=1,5 do
				if msk.api.MoveToPosition(3135,2153,nil,nil,nil,nil,10010) then
					moveOk = true
					break
				end
				sleep(1)
			end
		else
			if msk.api.MoveToPosition(2334,1298,nil,nil,nil,nil,10012) then
				moveOk = true
			end
		end
	else
		if msk.api.ChangeMap(nil,10001,nil,260800,114400) then--msk.api.FlyToNearListTeleport(10001,260800,114400) then
			for i=1,5 do
				if msk.api.MoveToPosition(2608,1144,nil,nil,nil,nil,10001) then
					moveOk = true
					break
				end
				sleep(1)
			end
		else
			if msk.api.MoveToPosition(2334,1298,nil,nil,nil,nil,10012) then
				moveOk = true
			end
		end
	end
	return moveOk
	--msk.ensure(moveOk,"__荡寇失败 无法移动到目标点")
end
repeat
	InitDangkouData()
	if eid ~= msk.api.GetCurrentEventID() then
		MoveToDangKouNpc()
	end
	if qs.IsPlayerInTeam() then
		LogDebuging("退出已有队伍")
		LeveaTeam()
		sleep(2)
	end
	LogDebuging("eid:%d",eid)
	local enterCount = 0
	while eid ~= msk.api.GetCurrentEventID() do--准备进入荡寇
		if enterCount > 10 then
			ScriptExit("无法进入荡寇")
		end
		LogDebuging("等待进入荡寇时间...")
		if qs.CheckHasBuff(0x2b09) then
			msk.api.UseSkill(0x10e822)
			sleep(1)
		end
		if not qs.CheckHasBuff(0x44e) then
			msk.api.UseSkill(0x201ac1)
		end
		while MoveToDangKouNpc() == false do
			sleep(1)
		end
		while CanDangKouBegin() == false do
			sleep(10,true)
		end
		LogDebuging("开始进入荡寇...")
		if false == EnterDangKou(dangkouLv) then
			MoveToDangKouNpc()
		else
			enterCount = enterCount + 1
			sleep(3)
		end
		if eid == msk.api.GetCurrentEventID() then
			dkCount = dkCount + 1
			SetDailyData("dangkou_count",dkCount)
		end
	end

	local event_instance =  _G.DgnEventFind(eid)
	local dangkouDataMt = {}
	function dangkouDataMt.__index(t,k)
		if k == "jifeng" then
			return event_instance:GetActorTable(qs.GetMainPlayerId(),8)
		elseif k == "xiuwei" then
			return event_instance:GetActorTable(qs.GetMainPlayerId(),6)
		elseif k == "yueli" then
			return event_instance:GetActorTable(qs.GetMainPlayerId(),5)
		elseif k == "zongjifeng" then
			return event_instance:GetInt(1)
		end
	end
	setmetatable(dangkouData, dangkouDataMt)
	while eid == msk.api.GetCurrentEventID() do
		--if cmds[1] == "liveness" then
		if (dangkouData.yueli == 0 and dangkouData.xiuwei == 0) or __IsLvOk() or cmds[1] == "liveness" then
			--msk.api.MoveToPosition(escapeX,escapeY)--脱离战斗等待结束	
			if qs.GetMainplayerLevel() >= 41 and msk.config.dailyLiveNess == 1 and msk.tools.GetDailyData("dangkou_liveness") ~= "yes" and cmds[1] ~= "liveness" then
				g_cacheData.dailyLv = nil
				cmds[1] = "liveness"
			end
			if qs.IsPlayerInBattle() then
				LogDebuging("战斗中 脱离:%d,%d",escapeX,escapeY)
				msk.api.MoveToPosition(escapeX,escapeY)--脱离战斗等待结束
				sleep(10)
			elseif GetDangkouCount() < 10 then
				LogDebuging("经验修为够了 小退")
				g_client:hset("time",g_qq,os.time())
				msk.api.BackAndEnterWorld()
			else
				break
				--LogDebuging("战斗中 脱离:%d,%d",escapeX,escapeY)
				--sleep(10)
			end
		else
			if not msk.api.MoveToPosition(fightX,fightY) then--东越
				InitDangkouData()
				sleep(2)
			end
			if g_cacheData.skipNpcNum ~= 0 then
				g_cacheData.skipNpcAddrs = {}
				g_cacheData.skipNpcNum = 0
			end
			local npc
			while (dangkouData.yueli ~= 0 or dangkouData.xiuwei ~= 0) and eid == msk.api.GetCurrentEventID() do
				if __IsLvOk() then break end
				npc = msk.npc.GetByName()
				if npc then 
					msk.api.FightNpc(npc)
					sleep(0.5)
				else
					msk.api.MoveToPosition(fightX,fightY)
					sleep(1)
				end
			end
		end
	end
	if GetDangkouCount() >= 10 then--次数没了 怎么都要退
		SetDailyData("dangkou","yes")
		break
	elseif __IsLvOk() then--达到设定的等级
		break
	elseif cmds[1] == "liveness" then--这个是活跃的
		local base = msk.dword(g_sig.liveNessBase)
		if msk.dword(base+4) >= 750 then
			break
		end
	elseif dangkouData.yueli == 0 and dangkouData.xiuwei == 0 then--次数没满 没到等级 也不是刷活跃 那么检查经验
		break
	end		
until false
SetDailyData("dangkou_yueli",dangkouData.yueli)
SetDailyData("dangkou_xiuwei",dangkouData.xiuwei)
if eid == msk.api.GetCurrentEventID() then
	msk.api.BackAndEnterWorld()
end
ScriptExit("荡寇成功")