#include <iostream>
#include "Coroutine.h"
using namespace std;
using norlit::coroutine::Coroutine;

void* coroutine(void* arg) {
	printf("coroutine(%p)\n", Coroutine::yield());
	return new char[1];
}

int main()
{
    Coroutine cor { coroutine };
	void* ret = cor.resume(new char[1]);
	ret = cor.resume(new char[1]);
	printf("Back to main (%p)\n", ret);
    cout << "Hello world!" << endl;
    return 0;
}
