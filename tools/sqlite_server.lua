require 'logging.file'
require 'copas'
require "pack"
local sqlite3 = require "lsqlite3"

local logger = logging.file("sqlite3_error.txt")
local copas = copas
local db,vm

function printf(fmt,...)
    print(string.format(fmt,...))
end


function safedeal(skt,data,datalen)
	local newdata
	while datalen > #data do
		newdata = assert(skt:receive())
		data = data..'\n'..newdata
	end
	assert(datalen == #data)
	local pos,head,dec,len,account,pc,time,bdata = data:unpack(">nanaaaa")
	--assert(pos > #data,pos..":"..#data)
	--print(#data,pos,head,dec,len,account,pc,time,bdata and #bdata or "nil")
	assert(vm:bind(1,string.format("%04x",head)) == sqlite3.OK)
	assert(vm:bind(2,dec) == sqlite3.OK)
	assert(vm:bind(3,len) == sqlite3.OK)
	assert(vm:bind(4,account) == sqlite3.OK)
	assert(vm:bind(5,pc) == sqlite3.OK)
	assert(vm:bind(6,time) == sqlite3.OK)
	assert(vm:bind(7,bdata) == sqlite3.OK)
	assert(vm:step() == sqlite3.DONE)
	assert(vm:reset() == sqlite3.OK)
end

function tg_pack_handler(skt)
	local ip = skt:getpeername()
	print('天之禁封包-用户进入 ',ip)
    skt = copas.wrap(skt)
    local datalen,data,err,newdata
	while true do
		data = assert(skt:receive())
        if not data then 
            printf("%s recv err:%s",ip,err)
            break 
        end
		datalen,data = data:match("(%d+)_(.+)")
		datalen = tonumber(datalen)
		local ok,err = copcall(safedeal,skt,data,datalen)
		if not ok then
			g_logger:error(err)
		end
	end
	print('天之禁封包-用户离开 ',ip)
end

print("1:天之禁封包统计 其他:退出")
io.write("选择功能:")
local op = "1"--io.read()
local handlef
if op == "1" then
    handlef = tg_pack_handler
    db = sqlite3.open("tg.db")
    local err
    vm,err = db:prepare([[insert into pack(head,dec,len,account,pc,time,data) values(?,?,?,?,?,?,?)]])
    assert(vm,err)
else
    return
end

local user_server = assert(socket.bind('*', 10002))
copas.addserver(user_server, handlef)
copas.loop()
