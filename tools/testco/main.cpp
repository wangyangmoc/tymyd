#include <iostream>
#include <stdio.h>
#include "Coroutine.h"
using namespace std;
using co = norlit::coroutine::Coroutine;

void * coroutine() {
	for(int i =0;i <10;++i){
        printf("hi->%d",i);
        co::yield((void *)i);
	}
	return 0;
}

int main()
{
    co cor { coroutine };
    int ret;
    while(true)
    {
        ret = (int)cor.resume();
        printf("co ret:%d",ret);
        if (0 == ret) break;
    }
    cout << "Hello world!" << endl;
    return 0;
}
