require 'pack'
------------获取PE头部偏移-------------
function getPeHeader(f)
	if fstore.peHeader then return fstore.peHeader end
	local e_lfanew = 60--PE头RVA偏移
	f:seek("set",e_lfanew)--e_lfanew = 60
	_,fstore.peHeader = f:read(4):unpack('I')--读取偏移的值
	return fstore.peHeader
end

------------是否PE文件-------------
function isPeFile(f)
	f:seek("set")--移回文件起始
	local mz = f:read(2)
	if mz ~= "MZ" then print "MZ" return false end
	local peHeaderOff = getPeHeader(f)
	f:seek("set",peHeaderOff)--移动到PE头部
	local _,signature = f:read(4):unpack('I')--读取Signature
	if signature ~= 0x00004550 then  print "signature" return false end
	return true
end

--------------获取段数量-------------
function getNumberOfSections(f)
	if fstore.numOffSections then return fstore.numOffSections end
	local peFileHeader = getPeHeader(f)+4--文件头部
	f:seek("set",peFileHeader+2)--移动到NumberOfSections
	_,fstore.numOffSections =  f:read(2):unpack('H')
	return fstore.numOffSections
end

--------------根据名字获取段数据-------------
function getSectionDataByName(f,sName)
	local peSectionHeader = getPeHeader(f)+4+20+224
	f:seek("set",peSectionHeader)
	local sectionHeader = 0
	local numberOfSections = getNumberOfSections(f)
	for i = 1,numberOfSections do
		f:seek("set",peSectionHeader+(i-1)*40)--移动到NumberOfSections名字
		local name = f:read(8)
		if name:find(sName) then--获取段 
			sectionHeader = peSectionHeader+(i-1)*40
			break
		end
	end
	if sectionHeader == 0 then print"没有找到.text 段" return end
	f:seek("set",peSectionHeader+12)
	local _,virtualAddress = f:read(4):unpack('I')
	imageBase = imageBase+virtualAddress
	local _,sizeOfRawData = f:read(4):unpack('I')
	local _,pointerToRawData = f:read(4):unpack('I')
	f:seek("set",pointerToRawData)
	local data = f:read(sizeOfRawData)
	return data
end

------------获取文件数据段(.text)-------------
function getPeSectionData(fname,sName)
	fstore = {}--存储已有数据
	local f = assert(io.open(fname, "rb"))
	if not isPeFile(f) then
		print("不是PE文件！")
		f:close()
		return
	end
	local data = getSectionDataByName(f,sName)
	f:close()
	if data then 
		return data
	else
		print("寻找区段失败!")
	end
end
------------特征码字串转换为lua string-------------
local keyWord = {"(",")",".","%","+","-","*","?","[","]","^","$"}
local function IsPatkeyWord(c)
  for _,v in ipairs(keyWord) do--替换掉lua的模式关键字 不然lua会按照模式匹配进行查找 这个版本暂不支持模式匹配
		if c == v then
      return true
    end
	end
  return false
end
function transSignatureToByte(sig)
	sig = string.gsub(sig,' ','')--去掉空格
	if false and sig:len()%2 ~= 0 then 
		error('特征码长度必须为2的倍数')
	else
		local str = ""
    local c,hx
	local strLen
    local ssig = ""
	local i = 1
	while i <= sig:len() do
		c = sig:sub(i,i)
		if IsPatkeyWord(c) == false then--非模式匹配
			ssig = ssig..c
			if #ssig == 2 then
					hx = tonumber("0x"..ssig)
					assert(hx,"失败的??:"..ssig)
					ssig = ""
					c = string.char(hx)
				if hx == 0 then
					c = "%z"
				elseif IsPatkeyWord(c) then
					c = '%'..c
				end
				str = str..c
			end
		else
			str = str..c
		end
		i = i + 1
	end
    assert(ssig == "",'特征码长度必须为2的倍数') 
		--local strLen = str:len()
		-- for _,v in ipairs(keyWord) do--替换掉lua的模式关键字 不然lua会按照模式匹配进行查找 这个版本暂不支持模式匹配
			-- local f = string.find(str,"%"..v)
			-- str = string.gsub(str,"%"..v,"%%%"..v)
		-- end
	return str,strLen
	end
end

function findString(lStr,sStr,bg)
	bg = bg or 1
	return string.find(lStr,sStr,bg)
	--[[
	local len = #sStr
	for i=bg,#lStr-#sStr do
		if lStr:sub(i,i+len-1) == sStr then
			return i
		end
	end
	--]]
end

------------搜索特征码-------------
function searchSig(data,sig)
	local bytes,len = transSignatureToByte(sig[1])
	local address,result
	local num = sig[5] or 1
	local offset = 0
	local offend
	for i =1,num do
		offset,offend = findString(data,bytes,offset+1)--string.find(data,bytes,offset+1)
	end
	if not offset then 
		result = 0
		address = 0
		return address,result
	end
	local dis = sig[3]
	if sig[4] == 1 then 
		--dis = sig[3]+len 
		offset = offend+dis+1
	elseif sig[4] == 2 then 
		offset = offset + dis
	else 
		offset = offset-dis
		--dis = sig[3] 
	end
	--offset = offset+dis*sig[4]
	address =  offset+imageBase-1
	--print("address:",string.format("%x",address))
	if sig[2] == "base" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		--print("offset:",string.format("%x",offset))
		_,result = data:unpack('I',offset)
	elseif sig[2] == "base:1" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		_,result = data:unpack('b',offset)
	elseif sig[2] == "address" then
		result = address
	elseif sig[2] == "call" then
		if sig[4] < 0 then 
			offset = offset-4 
			address = address-4
		end
		local _,off = data:unpack('I',offset)
		result = address+off+4
	end
	if sig[6] then
		print("---------address:",string.format("%x",address))
	end
	return result,address
end
