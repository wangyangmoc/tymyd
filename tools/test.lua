--require "pack"
--require 'std'


function printf(fmt,...)
    print(string.format(fmt,...))
end

function bin2hex(s)
	local t= {}
	for i=1,#s do
		t[#t+1] = string.format("%02X ",string.byte(s,i,i))
	end
    return table.concat(t,' ')
end

local h2b = {
    ["0"] = 0,
    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["A"] = 10,
    ["B"] = 11,
    ["C"] = 12,
    ["D"] = 13,
    ["E"] = 14,
    ["F"] = 15
}

function hex2bin( hexstr )
    local s = string.gsub(hexstr, "(.)(.)%s%s", function ( h, l )
         return string.char(h2b[h]*16+h2b[l])
    end)
    return s
end

function encodevfstr(keys,vals)
	local str = ""
	for i=1,#keys do
		str = str..string.pack("p",keys[i])
	end
	str = str..string.char(0,0,0,0,0,0)
	for i=1,#vals do
		str = str..string.pack("P",vals[i])
	end
	str = str..string.char(0,0,0,0,0,0,0,0,0,0,0,0)
	return str
end

function decodevfstr(str)
	local pos,key,val = 1,0,nil
	local keys = {}
	local vals = {}
	local head,len
	pos,len,head = str:unpack("h2")
    printf('%x:%d',head,len)
	pos,key = str:unpack('p',pos)
	local idx = 1
	while key and key ~= "" do
        key = key:sub(1,-2)
		--print(idx,#key,key,'\n')
		keys[#keys+1] = key
		pos,key = str:unpack('p',pos)
		idx = idx + 1
	end
	pos = pos+5
	
	pos,val = str:unpack('P',pos)
	idx = 1
	while val and val ~= "" do
        val = val:sub(1,-2)
		--print(idx,#val,val)
		vals[#vals+1] = val
		pos,val = str:unpack('P',pos)
		idx = idx + 1
	end
	return keys,vals
end

function test_redis_subscribe()
	local redis = require "redis"
	redis = redis.connect('127.0.0.1',6379)
    for msg, abort in redis:pubsub({ subscribe = "testnews" }) do
        if msg.kind == 'subscribe' then
            print('Subscribed to channel '..msg.channel)
        elseif msg.kind == 'message' then
            if msg.channel == 'control_channel' then
                if msg.payload == 'quit_loop' then
                    print('Aborting pubsub loop...')
                    abort()
                else
                    print('Received an unrecognized command: '..msg.payload)
                end
            else
                print('Received the following message from '..msg.channel.."\n  "..msg.payload.."\n")
            end
        end
    end
end

function test_pack44f0()
	redis = redis.connect('127.0.0.1',6379)
    local memsizeidx = 1
    local data = redis:lindex(0x44f0,0)
    print(#data-4,bin2hex(data:sub(5,-1)))
    local keys,vals = decodevfstr(data)
    for i=1,#keys do
        if keys[i] == 'memory\0' then
            if nil == memval then
                local user = "wangyangmoc"--msk.getglobal("account_user")
                local rseed = 0
                for i=1,#user do
                    rseed = rseed + user:byte(i,i)
                end
                math.randomseed(rseed)
                local memsize = {4094,2048,4094}
                local usemem = math.random(620,950)
                memval = usemem..'/'..memsize[memsizeidx]..'\0'
            end
            vals[i] = memval
        elseif keys[i] == 'fps\0' then
            vals[i] = math.random(50,100)..'\0'
        elseif keys[i] == 'screen_size\0' then
            if nil == screen_sizeval then
                local scsize = {'1024*768\0','1440*900\0','1280*1024\0'}
                screen_sizeval = scsize[math.random(1,3)]
            end
            vals[i] = screen_sizeval
        elseif keys[i] == 'ping\0' then 
            vals[i] = math.random(50,250)..'\0'
        elseif keys[i] == 'ClientCount\0' then 
            vals[i] = '1\0'
        end
    end
    data = encodevfstr(keys,vals)
    print(#data,bin2hex(data))
    --[[
    local n = redis:llen(0x44f0)
    local keys,vals
    for i=0,n-1 do
        print("---------------------------------------\n")
        keys,vals = decodevfstr(redis:lindex(0x44f0,i))
        --assert(keys[1]=='time\0')
        for j=1,#keys do
            print(#keys[j],keys[j]..' = '..vals[j])
        end
    end
    --]]
end

function test_parse_all_pack()
end

function test_packinfo()
	redis = redis.connect('127.0.0.1',6379)
	
	local key
    local datas = {}
	local users = {"lipinru613","wangwenxin1988","chenwangming75"}
	--ݿȡ 
	local alen
    for i=1,3 do
        datas[i] = {}
        datas[i].heads = redis:smembers(users[i])
        datas[i].counts = {}
        datas[i].bgtm = {}
        datas[i].edtm = {}
        datas[i].Intv = {}
		datas[i].lens = {}
        for j=1,#datas[i].heads do
            key = datas[i].heads[j]
            datas[i].counts[key] = redis:hget(users[i]..'count',key)
            datas[i].bgtm[key] = redis:hget(users[i]..'ftm',key)
            datas[i].edtm[key] = redis:hget(users[i]..'ltm',key)
            datas[i].Intv[key] = (tonumber(datas[i].edtm[key])-tonumber(datas[i].bgtm[key]))/tonumber(datas[i].counts[key])
			alen = redis:hget(users[i]..'len',key) or 0
			datas[i].lens[key] = alen/tonumber(datas[i].counts[key])
        end
    end
	
	--ÿʽԤ
	 local sortdatas = {}
    local st,fd
    for i=1,3 do
        sortdatas[i] = {}
        --ͷ ֱ
        sortdatas[i][1] = datas[i].heads 
		print("ͷ",i,#datas[i].heads )
        --
        local meedsorts = {datas[i].counts,datas[i].Intv,datas[i].lens,datas[i].bgtm,datas[i].edtm}
        local ct
        for j = 1,#meedsorts do
            ct= {}
            st = {}
            for k,v in pairs(meedsorts[j]) do
                fd = false
                for m=1,#st do
                    if tonumber(v) < tonumber(st[m]) or (tonumber(v) == tonumber(st[m]) and tonumber(k) < tonumber(ct[m])) then
                        table.insert(st,m,v)
                        table.insert(ct,m,k)
                        fd = true
                        break
                    end
                end
                if not fd then
                    st[#st+1] = v
                    ct[#ct+1] = k
                end
            end
            sortdatas[i][j+1] = ct
        end
    end
	
	--׼ʾ
	local datafmt = "%4x %4d %4d %4d %9s %9s"--ʽ
    local headstr = string.format("%4s %4s %4s %4s %9s %9s","ͷ","","","","ʼʱ","ʱ")--ͷ
    headstr = " "..headstr..' | '..headstr..' | '..headstr--ͷ
	local sorttp = 1--
	local sortac = 1--׼˺
    local cst,tm1,tm2
    local hlen = #sortdatas[1][1]
	if hlen < #sortdatas[2][1] then hlen = #sortdatas[2][1] end
	if hlen < #sortdatas[3][1] then hlen = #sortdatas[3][1] end
    local astrt = {}
	local prehead = string.format("     %-39s | %-39s | %-39s","lipinru613","wangwenxin1988","chenwangming75")
	local alspce = string.rep(".",39)
	local uinput
    repeat
        os.execute("cls")
		
		--ʽÿһ(Żռ ͬһ˺,ͬһĸʽǲ)
        for i=1,3 do
            strt = {}
            cst = sortdatas[i][sorttp]
            for j=1,#cst do
                key = cst[j]
                tm1 = os.date("%X",datas[i].bgtm[key])
                tm2 = os.date("%X",datas[i].edtm[key])
                strt[j] = string.format(datafmt,key,datas[i].counts[key],datas[i].Intv[key],datas[i].lens[key],tm1,tm2)
            end
            astrt[i] = strt
        end
		
		--ͷͱ
		print(prehead)
        print(headstr)
		--ʼָ
		local minhead,str1,str2,str3
		local idx1,idx2,idx3 = 1,1,1
		local didx1,didx2,didx3 = 1,1,1
		local idx = 1
		local cnum
        repeat
			--ͷ ͬ˺ӵеİͷһ
			didx1,didx2,didx3 = 1,1,1
			minhead = sortdatas[1][1][idx1]
			--ȡСֵ
			cnum = tonumber(sortdatas[2][1][idx2])
			if not minhead or (cnum and tonumber(minhead) > cnum) then
				minhead = sortdatas[2][1][idx2]
			end
			cnum = tonumber(sortdatas[3][1][idx3])
			if not minhead or (cnum and tonumber(minhead) > cnum) then
				minhead = sortdatas[3][1][idx3]
			end
			if not minhead then break end
			--϶ ûİͲƶһ
			if not sortdatas[3][1][idx3] or minhead ~= sortdatas[3][1][idx3] then
				if sorttp == 1 then didx3 = 0 end
			end
			if not sortdatas[2][1][idx2] or minhead ~= sortdatas[2][1][idx2] then
				if sorttp == 1 then didx2 = 0 end
			end
			if not sortdatas[1][1][idx1] or minhead ~= sortdatas[1][1][idx1] then
				if sorttp == 1 then didx1 = 0 end
			end
			
			--ʽ ǰû(ûԼѾ)Ļ ...()
			str1 = (sorttp ~= 1 or minhead == sortdatas[1][1][idx1]) and astrt[1][idx1] or alspce
			str2 = (sorttp ~= 1 or minhead == sortdatas[2][1][idx2]) and astrt[2][idx2] or alspce
			str3 = (sorttp ~= 1 or minhead == sortdatas[3][1][idx3]) and astrt[3][idx3] or alspce
            print(string.format("%4d ",idx)..str1..' | '..str2..' | '..str3)
			idx1,idx2,idx3 = idx1+didx1,idx2+didx2,idx3+didx3
			idx = idx + 1
        until idx1 > #sortdatas[1][1] and idx1 > #sortdatas[2][1] and idx1 > #sortdatas[3][1]
		
		--ʾϢ
		--print("ǰ:"..sorttp,"ǰ˺:"..users[sortac])
		print("ǰ:"..sorttp)
        print("1:ͷ 2: 3: 4: 5:ʼʱ 6:ʱ")
		--print("7:ѡ˺-lipinru613 8:ѡ˺-wangwenxin1988  9:ѡ˺-chenwangming75")
		print("˳...")
		io.flush ()
		io.write("ѡ:")
        uinput = io.read()
		uinput = tonumber(uinput)
		if not uinput or uinput <= 0 or uinput > 6 then break end
		if uinput > 0 and uinput < 7 then
			sorttp = uinput
		elseif uinput > 6 and uinput < 10 then
			sortac = uinput-6
		end
    until not uinput or uinput <= 0 or uinput > 9
end

function test_Hook8088()
	local copas = require 'copas'
	local function admin_handler(skt)
		local ip,port = skt:getpeername()
		print('ͻ ',ip,port)
		skt = copas.wrap(skt)
		while true do
			data,err = skt:receive()
			print('recv:',data,err)
			if not data then break end
		end
		--skt:close()
		print('ͻ뿪 ',ip,port)
	end
	print("ʼ8088......")
	local admin_server = assert(socket.bind('127.0.0.1', 8088))
	copas.addserver(admin_server, admin_handler)
	copas.loop()
	print("")
end

function testmagic(m)
    for a=1,9 do
        for b=1,9 do
            for c=1,9 do
                n = (122 * a) + (212 * b) + (221 * c)
                if m == n then
					return (100 * a) + (10 * b) + c
				end
            end
        end
    end
	return 0
end

function test_macparse()
	local cidx = 1
	local idx=1
	for line in io.lines([[G:\bitbuket\tzj\Release\˵\ͻID.txt]]) do
		if #line > 2 then
			if cidx == idx then
				local cguid,mac = line:match("(%w+) *(%w+)")
				local macnum=""
				for i=1,#mac,2 do
					macnum = macnum..string.char(tonumber('0x'..mac:sub(i,i+1)))
				end
				mskg.setmac(macnum)
				break
			end
			idx = idx + 1
		end
	end
end

function test_sqlite3()
	require"luasql.sqlite3"
	local env = luasql.sqlite3()
	local db = assert(env:connect([[G:\bitbuket\tzj\Release\other\tg.db]]))
	db:close()
	env:close()
end

function test_lsqlite3()
	local sqlite3 = require("lsqlite3")
	local width = 78
	local function line(pref, suff)
		pref = pref or ''
		suff = suff or ''
		local len = width - 2 - string.len(pref) - string.len(suff)
		print(pref .. string.rep('-', len) .. suff)
	end

	local db, vm
	local assert_, assert = assert, function (test)
		if (not test) then
			error(db:errmsg(), 2)
		end
		return test
	end
	db = sqlite3.open('tg.db')
	vm = assert(db:prepare[[insert into pack(head,dec,len,account,pc,time,data) values(?,?,?,?,?,?,?)]])
	assert(vm:bind(1,0x10) == sqlite3.OK)
	assert(vm:bind(2,'??') == sqlite3.OK)
	assert(vm:bind(3,4) == sqlite3.OK)
	assert(vm:bind(4,"xx") == sqlite3.OK)
	assert(vm:bind(5,"msk") == sqlite3.OK)
	assert(vm:bind(6,os.date("%c")) == sqlite3.OK)
	assert(vm:bind_blob(7,"sfa2515125125123") == sqlite3.OK)
	assert(vm:step() == sqlite3.DONE)
	assert(vm:finalize() == sqlite3.OK)
	assert(db:close() == sqlite3.OK)
end

function test_FileLen()
    local f= assert(io.open[[H:\µ\info\DATA\SCRIPT\PRELOAD.LUA]],'rb')--H:\µ\inject.lua
    local str = f:read('*all')
    f:close()
	
end
dofile[[G:\bitbuket\td\Data\Script\Preload.lua]]
dofile[[G:\bitbuket\td\Data\UserScript\test.lua]]
