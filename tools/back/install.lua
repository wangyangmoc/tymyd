cmds = {...}
local appdata = os.getenv("appdata")
local gamedir=cmds[1]
local wgdir=cmds[2]
local hookdll=cmds[3]
local orzdllName = cmds[4]
local orzdll = gamedir..[[\]]..hookdll..[[.dll]]
local function Copy(src,des)
	local f1,err = io.open(src,"rb")
	if not f1 then
		print("源文件不存在:"..src,err)
		return
	end
	local f2,err = assert(io.open(des,"wb"))
	if not f2 then
		print("目标文件无法打开:"..des,err)
		f1:close()
		return
	end
	f2:write(f1:read("*all"))
	f2:close()
	f1:close()
end
--先卸载
print "卸载辅助..."
local f = io.open(appdata..[[\Microsoft\config.ini:dllname]])
if f then
	local dllname = f:read("*all")
	f:close()
	if dllname:sub(-1,-1) == "\n" then
		dllname = dllname:sub(1,-2)
	end
	print("恢复文件..",dllname..".dll",gamedir..[[\]]..dllname..[[.dll]])
	Copy(dllname..".dll",gamedir..[[\]]..dllname..[[.dll]])
else
	print("辅助未安装..")
end


