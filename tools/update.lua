package.path = package.path..';./lua/?.lua'
package.cpath = package.cpath..';./clibs/?.dll'
package.path = package.path..[[;D:/code/td/Data/tools/?.lua]]
package.cpath = package.cpath..[[;D:/code/td/Data/tools/?.dll]]
package.path = package.path..[[;D:/code/td/Data/tools/lua/?.lua]]
package.cpath = package.cpath..[[;D:/code/td/Data/tools/clibs/?.dll]]
require 'std'
require 'copas'
require 'json'
require 'redis'
require 'base64'
require 'search_pe'
require 'signature_data'
local hp = gamehelper or require "gamehelper"
--配置
local pname = "WuXia_Client.exe"


--执行功能
local pid = hp.findpidbyname(pname)
if not pid then
	print("没有找到游戏进程!")
	io.write("输入游戏ID:")
	pid = tonumber(io.read())
	if not pid or pid == 0  then
		error(0,"进程ID无效")
		return
	end
end
print("游戏ID:",pid)

local function printf(fmt,...)
	local data = string.format(fmt,...)
	print(data)
end

local function hex(str)
	return string.format("%#x",str)
end 

local function bin2hex(s,flag)
	local t= {}
	for i=1,#s do
		t[#t+1] = string.format("%02X",string.byte(s,i,i))
	end
    return table.concat(t,flag or ' ')
end

local szt = {}

local function char(c) return ("\\%3d"):format(c:byte()) end
local function szstr(s) return ('"%s"'):format(s:gsub("[^ !#-~]", char)) end
local function szfun(f) return "loadstring"..szstr(string.dump(f)) end
local function szany(...) return szt[type(...)](...) end
local function sznum(n) return string.format("%#x",n) end

local function sztbl(t,code,var)
  for k,v in pairs(t) do
    local ks = szany(k,code,var)
    local vs = szany(v,code,var)
    code[#code+1] = ("%s[%s]=%s"):format(var[t],ks,vs)
  end
  return "{}"
end

local function memo(sz)
  return function(d,code,var)
    if var[d] == nil then
      var[1] = var[1] + 1
      var[d] = ("_[%d]"):format(var[1])
      local index = #code+1
      code[index] = "" -- reserve place during recursion
      code[index] = ("%s=%s"):format(var[d],sz(d,code,var))
    end
    return var[d]
  end
end

szt["nil"]      = tostring
szt["boolean"]  = tostring
szt["number"]   = sznum
szt["string"]   = szstr
szt["function"] = memo(szfun)
szt["table"]    = memo(sztbl)

function serialize(d)
  local code = { "local _ = {}" }
  local value = szany(d,code,{0})
  code[#code+1] = "return "..value
  if #code == 2 then return code[2]
  else return table.concat(code, "\n")
  end
end

print('开始更新数据...')
local sig_data = {}
local t = {}
for k,v in pairs(sigNatureData) do
	local size
	base,size = hp.getmodulebase(pid,k)
	print('  搜索模块 名称,基址,大小->',k,hex(base),hex(size))
	imageBase = base
	local data = hp.readmem(pid,base,size)
	local val
	local lasttm = os.clock()
	local delayT = {}
	--延迟的后面遍历
	for k1,v1 in pairs(v) do
		if v1[1]:find("@%w+@") then
			printf("延迟:%s",k1)
			delayT[k1] = v1
		end
	end
	for k1,v1 in pairs(delayT) do
		v[k1] = nil
	end
	for k1,v1 in pairs(v) do
   -- if k1 == "isHaveAction" then
    --  print("try",k1)
   -- end
		val = searchSig(data,v1)
		t[k1] = val
		curtm = os.clock()
		printf("%s = %#x 耗时:%d",k1,val,curtm-lasttm)
		lasttm = curtm
	end
	local kw,ov1
	for k1,v1 in pairs(delayT) do
		kw = v1[1]:match("@(%w+)@")
		kw = string.pack("L",t[kw])
		kw = bin2hex(kw)
		ov1 = v1[1]
		v1[1] = v1[1]:gsub("@%w+@",kw)
		printf("替换:%s-%s",ov1,v1[1])
		val = searchSig(data,v1)
		t[k1] = val
		curtm = os.clock()
		printf("%s = %#x 耗时:%d",k1,val,curtm-lasttm)
		lasttm = curtm
	end
end
----[[
--初始化数据包
printf("开始初始化数据包。。。")
local R_DW = function(addr)
	local pos,data = hp.readmem(pid,addr,4):unpack("L")
	return data
end
local R_STR = function(addr,len)
	local data =  hp.readmem(pid,addr,len)
	local cb = string.byte(data:sub(1,1))
	local cb2 = string.byte(data:sub(2,2))
	local minB = string.byte('A')
	local maxB = string.byte('z') 
	if cb >= minB and cb <= maxB and cb2 >= minB and cb2 <= maxB then
		pos,data = data:unpack("z")
	else
		data = nil
	end
	return data
end
local addr,name
local bgAddr = R_DW(t.dataBase)
for off = 0x10,0x640,4 do
	addr = R_DW(bgAddr+off)
	name = R_DW(addr+0x4)
	if name ~= 0 then
		name = R_STR(name,50)
		if name then
			if t[name] then
				printf("重复:%s",name)
			else
				t[name] = off
			end
		end
	end
end
--]]
--发包HOOK
t.recvDataHookLen = 5
t.OnkeyEventHookLen = 9
t.onUiMsgHookLen = 5
t.updatetime = os.date("*t")

--一些静态数据
t.anshaMoneyId = 0xb4--这之前的都是给钱的
t.UserBarSkillArg3 = 0x0136B500
t.UISlotInfoBase = 0x10230000 

--更新后需要注意的数据
t.sendDataHookLen = 6
t.UISlotInfoMax = 0x6c
t.movingOff = 0x2ac
t.npcOffBase = 0xb0-0xa8--NPC属性便宜
t.actorNameOff = 0x560
t.mapIdOff = 0x12c
t.slotIdxOff = 0x40--格子在背包的索引
t.bagBagIdx = 120--包裹在背包的位置
local skillStringBase = -4--更新会导致整体偏移
t.useSlotNormalOff = 0x20
t.useSlotEquipOff = 0xc
t.normalMapBegin = 0x2711
t.normalMapEnd = 0x2727

t.skillStringOff = {
	desc = 0x114+skillStringBase,
	poem = 0x118+skillStringBase,
	name = 0x11c+skillStringBase,
	icon = 0x120+skillStringBase,
	type = 0x128+skillStringBase,
}
--这个每次都要处理一下
t.slotPosOff = {
	bag = 0,
	bagSlot = 0x78,
	equip = 0x32e,
	skill = 0x295,
}
t.slotPosNum = {
	bag = 0x78,
	bagSlot = 4,
	equip = 0x28,
	skill = 40,
}
t.slotOff = {
	type = 0xc,
	id = 0x3c,
	num = 0x40,
	cd = 0x24,
}
t.item_off0 = {
	id1 = 0x18,
	id2 = 0x1c,
	dura = 0x70,
	type = 0x28,
	num = 0x68,
}
t.item_off1 = {
	sex = 0x38,
	id = 0,
	quality = 0x8,--颜色
	lv = 0x10,
	qualityLv = 0xc,
}
t.item_off2 = {
	menpai = 0x18,
	pos = 0xe8,
}

local f= io.open([[D:\天涯明月刀\TCLS\mmog_data.xml]])
local verData = f:read"*all"
f:close()
local ver = verData:match([[<Version>(.+)</Version>]])
t.ver = ver
print("开始记录数据")
printf("%d %s",t.GetNpcBase,tostring(t.GetNpcBase))
local tstr = serialize(t)--prettytostring(t)
tstr = tstr:gsub("return","_g_sig =")
local f = io.open("sigs.txt",'w')
--f:write("_g_sig = ")
f:write(tstr,"\n")
f:close()
t.shoulieBase = os.time()+30*34*60*60
tstr = serialize(t)
tstr = tstr:gsub("return","_g_sig =")
f = io.open("sigs2.txt",'w')
--f:write("_g_sig = ")
f:write(tstr,"\n")
f:close()


--[[
f:write("updatetime","=\"",os.date(),"\"\n")
for k,v in pairs(t) do
	print(k,'=',hex(v))
	f:write(k,'=',hex(v),',\n')
end
--]]
