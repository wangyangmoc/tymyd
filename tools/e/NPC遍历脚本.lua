local npcBase = 0x2117B2C
local mainPlaerBase = 0x2118154
local actorBase = 0x2091c50
midx,midy = 400,300
g_Scale = 1.0
local pid = 0
local DrawLine = 画直线
local DrawPoint = 画点
local ReadText = 读文本
local ReadInt = 读整数
local ReadFloat = 读小数
local ReadByte = 读字节
local GetPidByName = 进程名取ID
local print = 输出文本
local function printf(fmt,...)
	print(string.format(fmt,...))
end

function ReadInts(...)
	local t = {...}
	local v = 0
	for i=1,#t do
		v = ReadInt(pid,v+t[i])
	end
	return v
end

function PrintAll()
	local mainPlayer = ReadInt(pid,mainPlaerBase)
	--printf("main:%x",mainPlayer)
	local mname = ReadInt(pid,mainPlayer+0xa8)
	local mlv = ReadByte(pid,mainPlayer+0xd0)
	mlv = mlv + 0x25
	if mlv > 255 then
		mlv = mlv - 256
	end
	--lv = lv - 254
	mname = ReadText(pid,mname)
	local others = {}
	others[1] = {0,0,name_ = mname,lv_ = mlv}
	--printf("%s:%d",mname,mlv)
	local eax = npcBase
	eax = ReadInt(pid,eax+0x4)
	local ecx = ReadInt(pid,eax+4)
	local maxnum = ReadInt(pid,ecx+0xc)
	local num  = ReadInt(pid,ecx+0x10)
	ecx = ReadInt(pid,ecx+0x8)--数组
	local node,data,nextnode,id1,name,x,y,lv,id2
	for i=0,maxnum-1 do
		node = ReadInt(pid,ecx+i*4)
		id1 = ReadInt(pid,node)
		id2 = ReadInt(pid,node+4)
		while node ~= 0 and id1 ~= 0 do--数组的每一个位置都是一个链表 求余,多余数据插入链表
			data = ReadInt(pid,node+0x8)
			if data ~= mainPlayer then--自己就不打印了
				name = ReadInt(pid,data+0xa8)
				lv = ReadByte(pid,data+0xd0)
				lv = lv + 0x25
				if lv > 255 then
					lv = lv - 256
				end
				--lv = lv - 254
				name = ReadText(pid,name)
				others[#others+1] = {id1,id2,name_ = name,lv_ = lv}
				--printf("%x %s:%d %d,%d %x",data,name,lv,x/100,y/100,id1)
			else
				printf("try:%x",id1)
				others[1][1] = id1
				others[1][2] = id2
			end
			node = ReadInt(pid,node+0x10)
			id1 = ReadInt(pid,node)
			id2 = ReadInt(pid,node+4)
		end
	end
	
	local GeMapActorManager = ReadInts(actorBase,0x5c0,0x8)
	local bg = ReadInt(pid,GeMapActorManager+0x10)
	local ed = ReadInt(pid,GeMapActorManager+0x14)
	local id1,id2,posdata,x,y,z,data,npc
	--print("total:%d",(ed-bg)/4)
	for i=bg,ed,4 do
		data = ReadInt(pid,i)
		--printf("data:%x",data)
		id1 = ReadInt(pid,data+0xd8)
		id2 = ReadInt(pid,data+0xdc)
		--printf("id:%x-%x",id1,id2)
		posdata = ReadInt(pid,data+0xa0)
		for i=1,#others do
			if others[i][1] == id1 and others[i][2] == id2 then
				others[i].x_ = ReadFloat(pid,posdata+0x90)
				others[i].y_ = ReadFloat(pid,posdata+0x94)
				others[i].z_ = ReadFloat(pid,posdata+0x95)
				--printf("fit:%d,%d",others[i].x_/100,others[i].y_/100)
				break
			end
		end
	end
	
	--先画自己
	local txt,ot,mt,x,y,mx,my
	if #others > 1 then
		mt = others[1]
		mx = mt.x_/100
		my = mt.y_/100
		txt = string.format("%s:%d %d,%d",mt.name_,mt.lv_,mx,my)
		DrawPoint(midx,midy,"红色",txt)
	end
	for i=2,#others do
		ot = others[i]
		x = ot.x_
		y = ot.y_
		if x then
			x = x/100
			y = y/100
			txt = string.format("%s:%d %d,%d",ot.name_,ot.lv_,x,y)
			DrawPoint(midx+(x-mx)*g_Scale,midy+(my-y)*g_Scale,"绿色",txt)
			DrawLine(midx,midy,midx+(x-mx)*g_Scale,midy+(my-y)*g_Scale)
		end
	end
	
end

function 更新数据()
	--print("更新数据")
	if not pid or pid == 0 then
		pid = GetPidByName("WuXia_Client.exe")
		printf("获得进程ID:%d",pid)
	end
	if not pid or pid == 0  then
		return
	end
	local ok,err = pcall(PrintAll)
	if not ok then
		printf("err:%s",err)
	end
end

print("加载脚本完毕")