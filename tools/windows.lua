local alien = require 'alien'

local winapi = {}
local winstruct = {}

local GetLastError = alien.Kernel32.GetLastError
GetLastError:types{ ret = "ulong"}
winapi.GetLastError = GetLastError

local ReadProcessMemory = alien.Kernel32.ReadProcessMemory
ReadProcessMemory:types{ ret = "int", "pointer", "pointer", "pointer","ulong","pointer" }
winapi.ReadProcessMemory = ReadProcessMemory


--
winstruct.STARTUPINFO = alien.defstruct{
	{"cb",   "ulong"},
	{"lpReserved",   "string"},
	{"lpDesktop",   "string"},
	{"lpTitle",   "string"},
	{"dwX",   "ulong"},
	{"dwY",   "ulong"},
	{"dwXSize",   "ulong"},
	{"dwYSize",   "ulong"},
	{"dwXCountChars",   "ulong"},
	{"dwYCountChars",   "ulong"},
	{"dwFillAttribute",   "ulong"},
	{"dwFlags",   "ulong"},
	{"wShowWindow",   "ushort"},
	{"cbReserved2",   "ushort"},
	{"lpReserved2",   "pointer"},
	{"hStdInput",   "pointer"},
	{"hStdOutput",   "pointer"},
	{"hStdError",   "pointer"},
}

winstruct.PROCESS_INFORMATION = alien.defstruct{
    {"hProcess" ,"pointer" },
    {"hThread" ,"pointer" },
    {"dwProcessId" ,"ulong" },
    {"dwThreadId" ,"ulong" },
}

local CreateProcessA = alien.Kernel32.CreateProcessA
CreateProcessA:types{ ret = "int", "string", "string", "pointer","pointer","int","ulong","pointer","pointer","pointer","pointer" }
local si,pi = winstruct.STARTUPINFO:new(),winstruct.PROCESS_INFORMATION:new()

local ret = CreateProcessA("notepad.exe",[[C:\Users\Administrator\Desktop\asfsaf.txt]],nil,nil,0,0,nil,[[C:\Users\Administrator\Desktop\]],si,pi)
print( ret)
if ret == 0 then
	print(winapi.GetLastError())
end

return winapi

--[==[
BOOL WINAPI ReadProcessMemory(
  _In_   {"pointer", hProcess,
  _In_   LPCVOID lpBaseAddress,
  _Out_  LPVOID lpBuffer,
  _In_   SIZE_T nSize,
  _Out_  SIZE_T *lpNumberOfBytesRead
)"},



BOOL WINAPI CreateProcess(
  _In_opt_     LPCTSTR lpApplicationName,
  _Inout_opt_  LPTSTR lpCommandLine,
  _In_opt_     LPSECURITY_ATTRIBUTES lpProcessAttributes,
  _In_opt_     LPSECURITY_ATTRIBUTES lpThreadAttributes,
  _In_         BOOL bInherit{"pointer",s,
  _In_         DWORD, dwCreationFlags,
  _In_opt_     LPVOID lpEnvironment,
  _In_opt_     LPCTSTR lpCurrentDirectory,
  _In_         LPSTARTUPINFO lpStartupInfo,
  _Out_        LPPROCESS_INFORMATION lpProcessInformation
)"},




--]==]