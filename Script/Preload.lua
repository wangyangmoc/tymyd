
-- basic define

__ROOT_DIR__ = __ROOT_DIR__ or "./"
__SHIPPING__ = __SHIPPING__ or false
__RELOAD__ = __RELOAD__ or false

_G.PrintLog = function(...)
    local str = ""
    if arg.n > 0 then
        str = tostring(arg[1])
    end
    for i=2, arg.n  do
        str = str .. " " .. tostring(arg[i])
    end
    qs.QSPrint(str)
end

_G.PrintLogErr = function(...)
    local str = ""
    if arg.n > 0 then
        str = tostring(arg[1])
    end
    for i = 2, arg.n do
        str = str .. " " .. tostring(arg[i])
    end
    qs.QSPrintErr(str)
end

if __ROOT_DIR__ ~= "./" then
    _G.print = nil
    if not __SHIPPING__ then
        _G.print = PrintLog
        _G.printErr = PrintLogErr

        _G.disable_log = function(disable)
            if disable then
                _G.print = function(...) end
                _G.printErr = function(...)  end
            else
                _G.print = PrintLog
                _G.printErr = PrintLogErr
            end
        end
    else
        _G.print = function(...) end
        _G.printErr = function(...)  end
    end

else
    _G.printErr = _G.print
end

function EnablePrint(enable)
    if enable then
        _G.print = PrintLog
        --_G.printErr = PrintLogErr
    else
        _G.print = function(...) end
        --_G.printErr = function(...)  end
    end 
end

EnablePrint(true)

function g_log(channel, msg)
	print(channel.." -- "..msg)
end

function g_error(errorMsg)
    printErr("ERROR" .. " -- " .. errorMsg)
end


function g_warning(warningMsg)
    printErr("WARNING", " -- ", warningMsg)
end


function g_info(infoMsg)
    g_log("INFO", infoMsg)
end

function g_file_exists(name)
   --local f=io.open(name,"r")
   --if f~=nil then io.close(f) return true else return false end
   return qs.QSFileExist(name)
end


-- files
--dofile(__ROOT_DIR__.."Loader.lua")
local loaderFunc, loaderErrorMsg = loadfile(__ROOT_DIR__.."Loader.lua")
local loaderStatus = false
if loaderFunc ~= nil then
    loaderStatus, loaderErrorMsg = pcall(loaderFunc)
end
if not loaderStatus then
    g_error("file loading failed: " .. __ROOT_DIR__.."Loader.lua" .. ": " .. loaderErrorMsg)
    return 
end

-- initialize random system
math.randomseed(os.time())

function loadRecursively(dir, config_file)
    config_file = config_file or "LoadConfig.lua"

    local local_config_file = dir .. config_file
    if not g_file_exists(local_config_file) then
        local fileList = qs.QSDirDataVec()
		qs.QSDir(dir, fileList)
        local fileNum = fileList:size()

        for idx = 0, fileNum - 1 do
            local file = fileList[idx]
            local filename = tostring(file.name)
            if filename ~= "." and filename ~= ".." then
                local filePath = dir .. filename
                if not file.isDir then
                    if string.lower(string.match(filePath, "^.*(%..*)$")) == ".lua" then
                        Loader.DoFile(filePath)
                    end
                else
                    filePath = filePath .. "/"
                    loadRecursively(filePath)
                end
            end
        end

        return
    end

	local ok, loadcfg = Loader.DoFile(local_config_file)
	if not ok then
		g_error("Load config file failed: " .. local_config_file)
		return
	end

	for _, item in pairs(loadcfg) do
		if item.name ~= nil then
			if item.type == nil then
				-- if string.find(item.name, ".lua") ~= nil then
                if string.lower(string.match(item.name, "^.*(%..*)$")) == ".lua" then
					item.type = "file"
				else
					item.type = "dir"
				end
			end

			local item_path = dir .. item.name

			if item.type == "file" then
				Loader.DoFile(item_path)
            else
                item_path = item_path .. "/"
                loadRecursively(item_path, item.load_config)
			end

		end
	end
end

function loadAll()
    loadRecursively(__ROOT_DIR__, "LoadConfig.lua")
    loadRecursively(__ROOT_DIR__ .. "../ScriptCustom/")
end

loadAll()

-- create all display object in lua
Logic.UICore.PanelSystem:Init()
TrackMgr.Init()


function LoadSingleLua(filename) -- /fff.lua
    Loader.DoFile(filename)
end

