g_dataMaxOffset = 0x600--数据表最大值
--传送点
g_flyMapPos = {
	["万蝶坪"] = 3,
	["天香谷"] = 5,
	["望江门"] = 0x1a,
	["涌金门"] = 0x1b,
	["铸神谷"] = 0x1f,
	["开封"] = 0x3f,
	["松江分舵"] = 0x61,
}

--地图车夫
g_mapTansData = {
	["杭州"] = {
		{
			name = "杭州车夫",
			x = 2095,
			y = 1324,
			tar =   {
				{"开封",8000771,2349,1204},	
				{"江南",8000703,702,2487},	
				{"九华",8000709,2288,2561},	
				{"东越",8000715,416,3029},	
				{"襄州",8000723,1650,3265},	
				{"秦川",8000729,2982,1791},	
				{"巴蜀",8000735,1231,1091},	
				{"徐海",8000743,2990,1695},		
				{"燕云",8000749,1993,2488},
				{"荆湖",8000757,1818,1231},				
				{"云滇",8000763,1834,1395},
			},
		},
		{
			name = "杭州车夫",
			x = 2219,
			y = 1861,
			tar =   {
				{"东越",8002401,3083,1327},	
			},
		},
		{
			name = "杭州船夫",
			x = 1571,
			y = 284,
			tar =   {
				{"江南",8002501,702,2487},	
			},
		},
		{
			name = "门派车夫",
			x = 2093,
			y = 1322,
			tar =   {
				{"秦川",8003702,2223,3163},	
				{"燕云",8003708,1991,2490},	
				{"巴蜀",8003714,1231,1091},	
				{"荆湖",8003722,1818,1231},	
				{"襄州",8003728,1650,3265},	
				{"东越",8003734,1373,1240},	
				{"云滇",8003741,3453,1187},	
			},
		},
		{
			name = "杭州车夫",
			x = 1099,
			y = 2621,
			tar =   {
				{"九华",-1,3003,2835},	
			},
		},
	},
	["开封"] = {
		{
			name = "开封车夫",
			x = 2353,
			y = 1201,
			tar =   {
				{"杭州",8003471,2227,1857},	
				{"江南",8003403,702,2487},	
				{"九华",8003409,2288,2561},	
				{"东越",8003415,416,3029},	
				{"襄州",8003423,1650,3265},	
				{"秦川",8003429,2982,1791},	
				{"巴蜀",8003435,1231,1091},	
				{"徐海",8003443,2990,1695},	
				{"燕云",8003449,1993,2488},
				{"荆湖",8003457,1818,1231},
				{"云滇",8003463,1834,1395},
			},
		},
		{
			name = "门派车夫",
			x = 2350,
			y = 1201,
			tar =   {
				{"秦川",8003702,2223,3163},	
				{"燕云",8003708,1991,2490},	
				{"巴蜀",8003714,1231,1091},	
				{"荆湖",8003722,1818,1231},	
				{"襄州",8003728,1650,3265},	
				{"东越",8003734,1373,1240},	
				{"云滇",8003741,3453,1187},	
			},
		},
	},
	["江南"] = {
		{
			name = "江南船夫",
			x = 702,
			y = 2487,
			tar =   {
				{"杭州",8001001,1575,287},	
				{"开封",8001007,2349,1204},	
			},
		},
		{
			name = "江南车夫",
			x = 2119,
			y = 1487,
			tar =   {
				{"东越",8001101,413,3004},	
			},
		},
	},
	["东越"] = {
		{
			name = "东越船夫",
			x = 3012,
			y = 1402,
			tar =   {
				{"杭州",8001401,2227,1857},	
				{"开封",8001407,2349,1204},	
			},
		},
		{
			name = "东越车夫",
			x = 415,
			y = 3029,
			tar =   {
				{"江南",8001201,2119,1487},	
				{"杭州",8001207,2227,1857},	
				{"开封",8001213,2349,1204},	
			},
		},
		
		{
			name = "天香传令弟子",
			x = 1458,
			y = 1362,
			tar =   {
				{"杭州",8003201,1259,932},	
				{"开封",8003207,2349,1204},	
			},
		},
	},
	["九华"] = {
		{
			name = "九华车夫",
			x = 2295,
			y = 2567,
			tar =   {
				{"徐海",8002101,3022,1715},	
				{"杭州",8002107,2227,1857},	
				{"开封",8002113,2349,1204},	
			},
		},
		{
			name = "门派车夫",
			x = 2720,
			y = 3569,
			tar =   {
				--{"秦川",8003702,2223,3163},	
				{"燕云",8003708,1991,2490},	
				{"巴蜀",8003714,1231,1091},	
				{"荆湖",8003722,1818,1231},	
				{"襄州",8003728,1650,3265},	
				{"东越",8003734,1373,1240},	
				{"云滇",8003741,3453,1187},	
			},
		},
	},
	["徐海"] = {
		{
			name = "徐海车夫",
			x = 3021,
			y = 1722,
			tar =   {
				{"九华",8002301,2288,2561},	
				{"杭州",8002307,2092,1326},	
				{"开封",8002313,2349,1204},
			},
		},
	},
	["襄州"] = {
		{
			name = "知客道长",
			x = 2516,
			y = 3101,
			layer = 1,
			tar =   {
				{"九华",1011804900,2745,3563},	
				{"开封",1011804909,1259,932},	
				{"杭州",1011804903,2227,1857},
			},
		},
	},
	["秦川"] = {
		{
			name = "许长生",
			x = 2223,
			y = 3150,
			tar =   {
				{"九华",1010300101,2745,3563},	
			},
		},
		{
			name = "秦川车夫",
			x = 2982,
			y = 1791,
			tar =   {
				{"开封",8004301,2349,1204},	
				{"杭州",8004307,2227,1857},
			},
		},
		{
			name = "秦川车夫",
			x = 1809,
			y = 952,
			tar =   {
				{"开封",8004301,2349,1204},	
				{"杭州",8004307,2227,1857},
			},
		},
		{
			name = "太白传令弟子",
			x = 2857,
			y = 3400,
			tar =   {
				{"开封",8000307,1259,932},	
				{"杭州",8000301,2227,1857},
			},
		},
	},
	["燕云"] = {
		{
			name = "神威传令弟子",
			x = 1662,
			y = 2781,
			tar =   {
				{"杭州",8000501,1259,932},	 
				{"开封",8000507,2349,1204},	
			},
		},
	},
	["荆湖"] = {
		{
			name = "丐帮传令弟子",
			x = 1818,
			y = 1231,
			tar =   {
				{"杭州",8003001,1259,932},	
				{"开封",8003007,2349,1204},	
			},
		},
	},
	["巴蜀"] = {
		{
			name = "唐门传令弟子",
			x = 1229,
			y = 1081,
			layer = 1,
			tar =   {
				{"九华",8002900,2745,3563},
			},
		},
	},
	["云滇"] = {
		{
			name = "云滇车夫",
			x = 1831,
			y = 1393,
			tar =   {
				{"杭州",8003501,2084,1325},	
				{"开封",8003507,2349,1204},	
			},
			
		},
		{
			name = "传令弟子",
			x = 3453,
			y = 1187,
			tar =   {
				{"杭州",1012103001,1259,932},	
				--{"开封",8003507,2349,1204},	
			},
			
		},
	},
}

--有些任务点是不对的 需要人工调整
g_replaceMapId = {
	[10703] = {
		[9989] = function()--夜照天心 任务
			return msk.api.MoveToPosition(3108,1115)
		end
	},
	[10010] = {
		[11563] = function()--前往[PathNpc,江南,10010,11563]
			if msk.api.ChangeMap(msk.api.GetCurrentMapId(),10011) then
				return msk.api.MoveToPosition(697,2477)
			end
		end,
		[13675] = function()--游历杭州
			if not msk.api.ChangeMap(msk.api.GetCurrentMapId(),10010) then
				return false
			end
			if msk.npc.GetMainPlayer():IsNear(157100,28400,5000) then
				msk.api.MoveToPosition(1600,294)
			end
			return msk.api.MoveToPosition(2199,816)
		end,
	},
	[10903] = {
		[735] = function()--龙凤劫·援手江湖侣
			return msk.api.MoveToPosition(1103,2774)
		end,
	},
	[10901] = {
		[100778] = function()--铸神谷
			return msk.api.MoveToPosition(1587,2704)
		end,
	},
	[10912] = {
		[300894] = function()--狮子坡
			if msk.api.MoveToPosition(2922,2524) then
				for i=1,24 do
					if msk.api.GetCurrentEventID() == -1 then
						break
					end
					msk.sleep(5)
				end
				return true
			end
			return false
		end,
	},
	[10002] = {
		[40099] = function()--仙客来
			return msk.api.MoveToPosition(1061,988,nil,nil,nil,nil,10002)
		end,
		[40100] = function()--子风藤
			return msk.api.MoveToPosition(1054,1002,nil,nil,nil,nil,10002)
		end,
	},
}

--有一些任务 执行之前需要特殊处理一下 一般是对话后跳跃
g_speciaQuestBefireRun = {
	[0x1dcf] = {
		[1] = function (qid,pl)
			if pl:IsNear(204700,81200)  then--声东击西
				msk.api.NpcTalkCompleteByName("孟长风",1010514200,0,0,0,0,0,0)
				msk.sleep(3)
			end
		end,
	},
	[0x1d53] = {
		[1] = function (qid,pl,desc)
			if pl:IsNear(284500,184800)  then--擒贼擒王
				msk.quest.TrySubmitNpcTalk(qid,nil,"唐二",desc,true)
				msk.sleep(3)				
			end
			msk.api.MoveDown(2731,1722,pl,6680)	
		end,
	},
	[0x1cfe] = {
		[7] = function (qid,pl)--水波龙吟
			if msk.api.GetCurrentMapId() == 10002 then
				msk.api.FlyMap("铸神谷")
				msk.sleep(3)
			end
		end,
	},
	[9531] = {
		[8] = function (qid,pl,desc)
			if pl:IsNear(174000,243600)  then--后续事宜
				msk.api.MoveForWard(1716,2142,20)		
			end
		end,
	},
	[10178] = {--盟会介绍
		[7] = function (qid,pl,desc)
			if msk.api.GetCurrentMapId() == 10002 then
				msk.api.FlyMap("涌金门")
				msk.sleep(3)
			end
		end,
	},
}

for k,v in pairs(g_speciaQuestBefireRun) do
	v[2] = v[1]--类型1 和类型2 用的是同一个处理,都是打怪
end

--存储一些通讯的数据
g_cacheData = {
	skipNpcNum = 0,--任务中需要跳过的NPC
	skipNpcAddrs = {},
	hookRecv = 0,
	hookSend = 0,
}

--城市切换时应该找哪些NPC
g_changeMapFun = {
	[10002] = {--东越
		[10010] = function (cmap,tmap)--杭州
			local pl = msk.npc.GetMainPlayer()
			local area_name = qs.GetFogName(10002,pl.x,pl.y)
			if area_name == "" then
			elseif msk.api.MoveToNpc(10002,2982) then
				msk.api.NpcTalkCompleteByName("天香传令弟子",8003201,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10012] = function (cmap,tmap)--开封
			if msk.api.MoveToNpc(10002,2982) then
				msk.api.NpcTalkCompleteByName("天香传令弟子",0x007a1e87,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10011] = function (cmap,tmap,tid)--江南
			if msk.api.MoveToPosition(415,3029) then
				msk.api.NpcTalkCompleteByName("东越车夫",	8001201,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,

	},
	[10001] = {--九华
		[1002] = function (cmap,tmap)--东越
			if msk.api.MoveToNpc(2720,3569) then
				msk.api.NpcTalkCompleteByName("门派车夫",0x007a2096,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
	},
	[10010] = {--杭州
		[10012] = function (cmap,tmap)--开封
			if msk.api.MoveToPosition(2095,1324) then
				msk.api.NpcTalkCompleteByName("杭州车夫",8000771,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10011] = function (cmap,tmap)--江南
			if msk.api.MoveToPosition(2095,1324) then
				msk.api.NpcTalkCompleteByName("杭州车夫",8000703,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10002] = function (cmap,tmap)--天香
			if msk.api.MoveToPosition(2094,1322) then
				msk.api.NpcTalkCompleteByName("门派车夫",0x007a2096,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
	},
	[10011] = {--江南
		[10002] = function (cmap,tmap,tid)--东越
			printf("前往东越")
			if msk.api.MoveToPosition(2119,1487) then
				local fid
				if tid == 2981 then--回天香谷(门派)
					fid = 8001141
				else
					fid = 8001101
				end
				msk.api.NpcTalkCompleteByName("江南车夫",fid,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10010] = function (cmap,tmap)--杭州
			if msk.api.MoveToPosition(838,2641) and msk.api.MoveToPosition(702,2489) then
				msk.api.NpcTalkCompleteByName("江南船夫",8001001,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
	},
	[10012] = {--开封
		[10010] = function (cmap,tmap)--杭州
			if msk.api.MoveToPosition(2353,1201) then
				msk.api.NpcTalkCompleteByName("开封车夫",8003471,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
		[10011] = function (cmap,tmap)--江南
			if msk.api.MoveToPosition(2353,1201) then
				msk.api.NpcTalkCompleteByName("开封车夫",8003403,0,0,0,0,0,0)
				msk.sleep(5)
			else
				return false
			end
		end,
	},
}
--挑战前置的江湖志
g_TestPre = {
	[0x24] = 1010408911,--于百石
	[0x26] = 1010406717,--白邓通
}

--有些装备因为数据不对 不要处理
g_skipEquip = {
	"劫道风尘","猎弓",
}
--退下的装备处理
g_unUseEquip = {
	lv = 40,--60以下的装备
	quality = 2,--0-3 白 绿 蓝 紫   蓝装一下处理掉
	measure = "分解",--处理手段是分解
}
--物品处理
g_itemProcess = {
	["天涯会员30天"] = "使用",
	["略有小成礼盒"] = "使用",
	["芳草·天涯"] = "使用",
	["清风·明月"] = "使用",
	["江湖议事礼.壹"] = "使用",
	["江湖议事礼.贰"] = "使用",
	["江湖议事礼.叁"] = "使用",
	["江湖议事礼.肆"] = "使用",
	["江湖议事礼.伍"] = "使用",
	["江湖议事礼.陆"] = "使用",
	["江湖议事礼.柒"] = "使用",
	["江湖议事礼.捌"] = "使用",
	["江湖议事礼.玖"] = "使用",
	["江湖议事礼.拾"] = "使用",
	["登陆礼.壹"] = "使用",
	["登陆礼.贰"] = "使用",
	["登陆礼.叁"] = "使用",
	["登陆礼.肆"] = "使用",
	["登陆礼.伍"] = "使用",
	["登陆礼.陆"] = "使用",
	["登陆礼.柒"] = "使用",
	["登陆礼.捌"] = "使用",
	["登陆礼.玖"] = "使用",
	["登陆礼.拾"] = "使用",
	["见面礼.壹"] = "使用",
	["见面礼.贰"] = "使用",
	["见面礼.叁"] = "使用",
	["见面礼.肆"] = "使用",
	["见面礼.伍"] = "使用",
	["见面礼.陆"] = "使用",
	["见面礼.柒"] = "使用",
	["见面礼.捌"] = "使用",
	["见面礼.玖"] = "使用",
	["见面礼.拾"] = "使用",
	["天下四盟大礼盒"] = "使用",
	["休闲躺"] = "使用",
	["天意"] = "使用",
	["绑定点券500点"] = "使用",
	["展示动作-请战"] = "使用",
	["展示动作-害羞"] = "使用",
	["展示动作-喝酒"] = "使用",
	["展示动作-磕头"] = "使用",
	["展示动作-哭"] = "使用",
	["展示动作-说话"] = "使用",
	["展示动作-挑衅"] = "使用",
	["展示动作-笑"] = "使用",
	["展示动作-摇头"] = "使用",
	["展示动作-再见"] = "使用",
	["展示动作-战斗失败"] = "使用",
	["展示动作-指向"] = "使用",
	["展示动作-赞"] = "使用",
	["初入江湖礼盒"] = "使用",
	["杀决长夜"] = "使用",
	["一级历练丹"] = "使用",
	["二级历练丹"] = "使用",
	["日活跃奖励修为丹"] = "使用",
	["5级天涯明月礼"] = "使用",
	["20级天涯明月礼"] = "使用",
	["10级天涯明月礼"] = "使用",
	["30级天涯明月礼"] = "使用",
	["40级天涯明月礼"] = "使用",
	["50级天涯明月礼"] = "使用",
	["会员每日补给包"] = "使用",
	["天防棱石·一"] = "使用",
	["天防棱石·二"] = "使用",
	["天防棱石·三"] = "使用",
	["长久棱石·一"] = "使用",
	["长久棱石·二"] = "使用",
	["长久棱石·三"] = "使用",
	["力道原石·一"] = "使用",
	["力道原石·二"] = "使用",
	["力道原石·三"] = "使用",
	["力道圆石·一"] = "使用",
	["力道圆石·二"] = "使用",
	["力道圆石·三"] = "使用",
	["天攻小石·一"] = "使用",
	["天攻小石·二"] = "使用",
	["天攻小石·三"] = "使用",
	["虎纹猫"] = "使用",
	["葫芦猫"] = "使用",
	["卷毛野猪"] = "使用",
	["《归鞘》"] = "使用",
}

--任务怪的名字和追踪的有可能不一样
function GetQuestName(name,eid,t)
	if name == "卢文锦" then
		return "琵琶行 卢文锦"
	elseif name == "钟怡" then
		return "铸神谷丫鬟钟怡"
	elseif eid == 16302 and name == "滋事头目" then
		return "毛贼头目"
	elseif 16300 == eid then
		return name:gsub("白云","白云观")
	elseif 16301 == eid and name == "寻找线索" then
		return "剑穗"
	elseif name == "流杀绿巾刀手" then
		return "流杀门绿巾刀手"
	elseif name == "孟府家仆" then
		return "家仆"
	elseif name == "蒋婳笙" then
		return "蒋画笙"
	elseif name == "梁知音" then
		return "空谷留香 梁知音"
	elseif name == "林弃霜" then
		return "天元星 林弃霜"
	elseif name == "白鹭洲" then
		return "乱红弦 白鹭洲"
	elseif name == "唐青铃" then
		return "飞灵伞 唐青铃"
	elseif name == "云文君" then
		return "天香 云文君"
	elseif name == "银煞" then
		return "白邓通"
	elseif name == "镇民" then
		return "盘龙镇村民"
	elseif name == "蒋地生" then
		return "流沙门 蒋地生"	
	elseif name == "朱武" then
		return "赛天蓬 朱武"	
	elseif name == "田中龙二" then
		return "青坊主 田中龙二"	
	elseif name == "都镭" then
		return "快刀手 都镭"		
	elseif name == "流杀门 薛果" then
		return "薛果"	
	elseif name == "线索" then
		return "玉佩"	
	elseif name == "匣子" then
		return "暗器匣子"	
	elseif name == "伤员" then
		return "鲲鹏会伤员"	
	elseif name == "欧顺" then
		return "万里鲲 欧顺"	
	elseif name == "逆水坞围攻刺客" then
		return "逆水坞刺客"	
	elseif name == "逆水坞围攻小头目" then
		return "逆水坞小头目"	
	elseif eid == 16013 and name == "万象门精锐" then
		return "万象门暗哨"
	elseif name == "夏竹佩" then
		return "千里香 夏竹佩"	
	else
		return name
	end
end

--剧情里 怪物ID可能和任务配置的不一样
function GetQuestMonster(id,t,eid)
	if id == 20105562 then
		return 20105564
	elseif id == 10105027 then
		return 10105091
	elseif id == 20105502 then
		return 20105538
	elseif id == 20105563 then
		t[#t+1] = 0x132c95d
		t[#t+1] = 0x132c94f
	elseif id == 10105094 then
		t[#t+1] = 0x9a3107
		t[#t+1] = 0x9a3106
	elseif id == 20104053 then
		t[#t+1] = 20104053
		t[#t+1] = 20104055	
	elseif id == 20101004 then
		return 201010051
	elseif id == 20104501 then
		return 10104543
	elseif id == 20104502 then
		return 10104544
	else
		return id
	end
end
g_names = {"天", "籁", "狐", "狸", "魅", "舞", "梅", "川", "战", "神", "朝", "阳", "寂", "静", "林", "川", "乖", "妩", "媚", "随", "风", "幽", "影", "酷", "吖", "头", "久", "舞", "关", "也", "珑", "王", "魄", "神", "春", "意", "善", "衣", "春", "天", "花", "非", "漫", "雨", "大", "若", "天", "枫", "战", "神", "Aso", "天", "空", "泡", "タ", "灰", "洋", "白", "菜", "墨", "子", "莫", "名", "泪", "落", "樱", "桃", "大", "尛", "唐", "王", "奈", "泪", "夜", "凄", "傷", "裳", "倾", "城", "寶", "贝", "潙", "伱", "誰", "与", "Go", "图", "找", "不", "夜", "倾", "小", "莪", "花", "落", "神", "猪", "火", "神", "午", "门", "九", "龙", "霸", "业", "忆", "载", "龙", "不", "懂", "宝", "贝", "无", "名", "铁", "血", "鏖", "战", "多", "领", "龙", "王", "伤", "科", "明", "月", "商", "王", "不", "可", "慧", "神", "鬼", "魔", "妖", "暧", "昧", "清", "清", "傲", "雪", "天", "策", "火", "军", "天", "下", "铭", "燕", "言", "花", "烟", "花", "清", "风", "霸", "楚", "铃", "铛", "笨", "贲", "小", "猪", "狐", "狸", "兔", "子", "寒", "冰", "辉", "烟", "尘", "麦", "黑", "子", "缘", "大", "神", "杀", "星", "孤", "狼", }
printf("global_tables ok")