--paimai.lua
local sleep = msk.sleep
local bin2hex = msk.tools.bin2hex
local hex2bin = msk.tools.hex2bin
local hex2bin2= msk.tools.hex2bin2
local mconfig = msk.config
local SendStr = msk.pack.SendStr
local Sendf = msk.pack.Sendf
local Send = msk.pack.Send
--[==[
--]==]
local function MoveToPaiMai()
	local x,y,tname = msk.api.GetNearListNpc("Auction")
	return (x and msk.api.MoveToPosition(x,y,true) and tname) or msk.api.MoveToPosition(2515,876,nil,nil,nil,nil,10012)
end

g_sig.paimaiHeadLen = 21
g_sig.paimaiNumMoneyPos = 130
g_sig.paimaiIdPos = 171
local paimaiHeadLen = g_sig.paimaiHeadLen 
local paimaiNumMoneyPos = g_sig.paimaiNumMoneyPos
local paimaiIdPos = g_sig.paimaiIdPos
function BuyPaiMaiItems(bname,buyCount,moneyLimit,pcount,lasthasBuy)
	--处理一下买卖东西
	pcount = pcount or 0
	if pcount > 10 then 
		return "拍卖行物品不够",lasthasBuy
	end--超出次数了
	buyCount = buyCount or 0
	if buyCount <= 0 then 
		LogDebuging("购买物品太少:%d",buyCount)
		return lasthasBuy and "成功" or "未知错误2",lasthasBuy
	end--不用买了...
	LogDebuging("拍卖行购买物品:%s %d",bname,buyCount)
	local allMoney = qs.GetCurMoney()
	if allMoney < 5000 then 
		return "钱太少",lasthasBuy
	end
	if not msk.mcall(21,MoveToPaiMai) then
		msk.api.QuitGame("移动到拍卖失败")
		--return "移动到拍卖失败",lasthasBuy
	end
	local npc = msk.npc.GetByName("金百万") or msk.npc.GetByName("拍卖行")
	if npc == nil then 
		msk.api.QuitGame("找拍卖NPC失败")
		--return "找拍卖NPC失败",lasthasBuy
	end
	msk.api.MoveToPosition(npc.x,npc.y,true)
	WaitMoneyProtect()
	if UIIsVisible("QSUIAuctionHousePanel") == false then
		npc:Open()
		sleep(1)
	end
	SearchAuction(npc.id1,npc.id2,bname)
	sleep(1)
	local buyItem = {}
	local base = msk.dword(g_sig.paimaiDataBase)
	local allCount = msk.dword(base+8)
	local hd = msk.dword(base+0x44)
	local ed = msk.dword(base+0x48)
	local num,money,pos,id1,id2
	local itemId,itemType,data
	local dataPos = 1
	for addr = hd,ed-1,4 do
		data = msk.dword(addr)
		itemType = msk.word(data+dataPos)
		itemId = msk.dword(data+dataPos+2)
		if qs.GetItemName(itemId) == bname then
			money = msk.dword(data+dataPos+paimaiNumMoneyPos)
			num = msk.dword(data+dataPos+paimaiNumMoneyPos+4)
			id1 = msk.dword(data+dataPos+paimaiIdPos)
			id2 = msk.dword(data+dataPos+paimaiIdPos+4)
			if money <= moneyLimit then
				num = buyCount > num and num or buyCount
				if num*money > allMoney then
					num = math.floor (allMoney/money)
				end
				if num == 0 then
					buyCount = 0
					allMoney = 0
					break 
				end
				allMoney = allMoney - num*money
				buyCount = buyCount - num
				buyItem[#buyItem+1] = {id1,id2,num}
				if buyCount <= 0 or allMoney <= 0 then break end
			end
		end
		--LogDebuging("%s-%x:%x,%x 钱:%d n:%d",qs.GetItemName(itemId),itemId,id1,id2,money,num)
	end
	if #buyItem > 0 then lasthasBuy = true end
	msk.api.HideAllMsgBox()
	for i=1,#buyItem do
		__BuyPaiMaiItem(buyItem[i][1],buyItem[i][2],buyItem[i][3],npc.id1,npc.id2)
		sleep(0.5)
		--UIShowWindow("QSUIDialogPanel",false)
		msk.ui.UIMsgCall("QSUIDialogPanel","CMD_OK")
		UIShowWindow("QSUIDialogPanel",false)
	end
	msk.api.HideAllMsgBox("CMD_OK")
	if buyCount > 0 and allMoney > 0 then
		sleep(2)
		return BuyPaiMaiItems(bname,buyCount,moneyLimit,pcount+1,lasthasBuy)
	end
	if allMoney <= 0 and #buyItem <= 0 then
		if not lasthasBuy and qs.GetCurMoney() > 40000 then
			msk.api.QuitGame("购买异常")
		end
		return "钱不够",lasthasBuy
	elseif #buyItem <= 0 then 
		return "拍卖行物品不够",lasthasBuy
	end
	return lasthasBuy and "成功" or "未知错误",lasthasBuy
end