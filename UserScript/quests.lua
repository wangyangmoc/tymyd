g_allQuests = {
--------1,楔子：初出茅庐上九华--------
--茶寮小憩-小憩
[31] = function(idx)
	--找[PathNpc,明月心,10001,927]交谈
	if idx == 0 then
		CALL("任务点对话",10101010,"明月心",31,0,10001,927)
	end
end,
--茶寮小憩-打探
[32] = function(idx)
	--找[PathNpc,南宫无灭,10001,136]交谈
	if idx == 1 then
		CALL("任务点对话",10101003,"南宫无灭",32,1,10001,136)
	end
	--找[PathNpc,司徒振廷,10001,143]交谈
	if idx == 2 then
		CALL("任务点对话",10101025,"司徒振廷",32,2,10001,143)
	end
end,
--穿越竹林
[2] = function(idx)
	--找[PathNpc,卢铁涵,10001,117]带路
	if idx == 0 then
		CALL("任务功能对话",2,10200,"卢铁涵","找[PathNpc,卢铁涵,10001,117]带路",10001,117)
	end
end,
--竹林之战
[3] = function(idx)
	--和[PathNpc,凌沐阳,10001,146]对话
	if idx == 0 then
		CALL("任务点对话",10101027,"凌沐阳",3,0,10001,146)
	end
end,
--云笈水榭
[4] = function(idx)
	--进入[Map_X_Y,云笈水榭,10001,2703,3293]
	if idx == 0 then
		CALL("坐标点移动",2703,3293,10001)
	end
end,
--山路追凶-循迹追凶
[17] = function(idx)
	--前往[Map_X_Y,九华后山,10001,2918,3079] 
	if idx == 0 then
		CALL("坐标点移动",2918,3079,10001)
	end
end,
--山路追凶-受伤男子
[18] = function(idx)
	--和[PathNpc,孟延贤,10001,133]交谈
	if idx == 1 then
		CALL("任务点对话",10101022,"孟延贤",18,1,10001,133)
	end
end,
--山路追凶-除恶务尽
[19] = function(idx)
	--[PathMon,炎堂高手,10001,100363]
	if idx == 2 then
		local monsters = {"炎堂高手","炎堂弟子"}
		CALL("任务点杀怪",monsters,10001,100364)
	end
	--[PathMon,炎堂弟子,10001,100364]
	if idx == 3 then
		local monsters = {"炎堂弟子"}
		CALL("任务点杀怪",monsters,10001,100364)
	end
end,
--山路追凶-意外之声
[20] = function(idx)
	--和[PathNpc,孟延信,10001,134]交谈
	if idx == 4 then
		CALL("任务点对话",10101023,"孟延信",20,4,10001,134)
	end
end,
--继续搜寻
[7] = function(idx)
	--[Map_X_Y,搜索院子,10001,3034,2987] 
	if idx == 0 then
		CALL("坐标点移动",3034,2987,10001)
	end
end,
--向山而行-往北而去
[21] = function(idx)
	--继续[PathNpc,上山,10001,212]追寻孟怀楚
	if idx == 1 then
		CALL("任务点移动",10001,212)
	end
end,
--向山而行-报信僧人
[22] = function(idx)
	--和[PathNpc,僧人,10001,212]对话
	if idx == 0 then
		CALL("任务点对话",10101028,"僧人",22,0,10001,212)
	end
end,
--山寺之战
[9] = function(idx)
	--到达[Map_X_Y,化清寺,10001,2952,2757]
	if idx == 0 then
		CALL("坐标点移动",2952,2757,10001)
	end
end,
--扑朔迷离
[10] = function(idx)
	--和[PathNpc,燕南飞,10001,137]交谈
	if idx == 0 then
		CALL("任务功能对话",10,26004,"燕南飞","和[PathNpc,燕南飞,10001,137]交谈",10001,137)
	end
end,
--返回师门
[11] = function(idx)
	--与[PathNpc,许长生,10009,1]交谈
	if idx == 0 then
		CALL("任务点对话",10103001,"许长生",11,0,10009,1)
	end
end,
--返回师门
[12] = function(idx)
	--[PathArea,神威堡,10004,301136]
	if idx == 0 then
		CALL("任务点移动",10004,301136)
	end
end,
--返回师门
[13] = function(idx)
	--[PathNpc,聚义厅,10003,1]
	if idx == 0 then
		CALL("任务点移动",10003,1)
	end
end,
--返回师门
[14] = function(idx)
	--[PathNPCLayer,唐慧,10005,1,1]
	if idx == 0 then
		--桥上?
		CALL("任务点对话",10116001,"唐慧",14,0,10005,1,1)
	end
end,
--返回师门
[15] = function(idx)
	--[PathArea,真武山,10007,300007]
	if idx == 0 then
		CALL("任务点移动",10007,300007)
	end
end,
--返回师门
[16] = function(idx)
	--[PathNpc,返回天香,10002,2374]
	if idx == 0 then
		CALL("任务点移动",10002,2374)
	end
end,
--返回师门
[23] = function(idx)
	--[PathNpc,返回五毒,10006,1422]
	if idx == 0 then
		CALL("任务点移动",10006,1422)
	end
end,

--------2,创角001--------
--创角001
[65534] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------3,创角010--------
--创角010
[65533] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------4,道姑之赐--------
--道姑之赐
[65531] = function(idx)
end,

--------601,霜雪连天龙踪现--------
--山骨林风-秦川冻原
[18300] = function(idx)
	--[PathArea,赶往秦川,10009,300605]
	if idx == 1 then
		CALL("任务点移动",10009,300605)
	end
end,
--山骨林风-太白弟子
[18301] = function(idx)
	--[PathNpc,孙晓倩,10009,499]
	if idx == 0 then
		CALL("任务点对话",10111001,"孙晓倩",18301,0,10009,499)
	end
end,
--山骨林风-青龙扰世
[18302] = function(idx)
	--[PathMon,寒露殿探子,10009,100806]
	if idx == 2 then
		local monsters = {"寒露殿探子","寒露殿斥候"}
		CALL("任务点杀怪",monsters,10009,100806)
	end
	--[PathMon,寒露殿斥候,10009,100806]
	if idx == 3 then
		local monsters = {"寒露殿斥候"}
		CALL("任务点杀怪",monsters,10009,100806)
	end
end,
--曲径寒天-抵达桥边
[18303] = function(idx)
	--[PathArea,桥边,10009,300606]
	if idx == 0 then
		CALL("任务点移动",10009,300606)
	end
end,
--曲径寒天-密探尸体
[18304] = function(idx)
	--[PathNpc,霜堂探子尸体,10009,500]
	if idx == 1 then
		CALL("任务点对话",10111017,"霜堂探子尸体",18304,1,10009,500)
	end
end,
--曲径寒天-血洒湖畔
[18305] = function(idx)
	--[PathEnt,血迹,10009,40008]
	if idx == 2 then
		CALL("任务点采集","血迹",10009,40008)
	end
end,
--曲径寒天-巧遇师姐
[18306] = function(idx)
	--[PathNpc,江婉儿,10009,1210]
	if idx == 3 then
		CALL("任务点对话",10111070,"江婉儿",18306,3,10009,1210)
	end
end,
--曲径寒天-流风回雪
[18351] = function(idx)
	--[PathEnt,雪里飞,10009,40170]
	if idx == 4 then
		CALL("任务点采集","雪里飞",10009,40170)
	end
end,
--青流滔滔-飞鸿印雪
[18307] = function(idx)
	--[PathNpc,江婉儿,10009,1210]
	if idx == 0 then
		CALL("任务点对话",10111070,"江婉儿",18307,0,10009,1210)
	end
end,
--青流滔滔-寒露为霜
[18308] = function(idx)
	--[PathMon,寒露殿刺客,10009,100815]
	if idx == 1 then
		local monsters = {"寒露殿刺客","寒露殿镖客"}
		CALL("任务点杀怪",monsters,10009,100815)
	end
	--[PathMon,寒露殿镖客,10009,100815]
	if idx == 2 then
		local monsters = {"寒露殿镖客"}
		CALL("任务点杀怪",monsters,10009,100815)
	end
end,
--青流滔滔-囊血射天
[18309] = function(idx)
	--[PathMon,寒露殿剑客,10009,100876]
	if idx == 3 then
		local monsters = {"寒露殿剑客","寒露殿伏击手"}
		CALL("任务点杀怪",monsters,10009,100876)
	end
	--[PathMon,寒露殿伏击手,10009,100876]
	if idx == 4 then
		local monsters = {"寒露殿伏击手"}
		CALL("任务点杀怪",monsters,10009,100876)
	end
end,
--青流滔滔-金玉其外
[18310] = function(idx)
	--[PathArea,玉泉院,10009,300603]
	if idx == 5 then
		CALL("任务点移动",10009,300603)
	end
end,
--沃野秋蓬-微弱呼救
[18311] = function(idx)
	--[PathNpc,重伤的道士,10009,501]
	if idx == 0 then
		CALL("任务点对话",10111019,"重伤的道士",18311,0,10009,501)
	end
end,
--沃野秋蓬-心狠手辣
[18312] = function(idx)
	--[PathMon,寒露殿投手,10009,100902]
	if idx == 1 then
		local monsters = {"寒露殿投手","寒露殿女贼"}
		CALL("任务点杀怪",monsters,10009,100902)
	end
	--[PathMon,寒露殿女贼,10009,100902]
	if idx == 2 then
		local monsters = {"寒露殿女贼"}
		CALL("任务点杀怪",monsters,10009,100902)
	end
end,
--沃野秋蓬-后院杀机
[18313] = function(idx)
	--[PathMon,寒露殿弟子,10009,100919]
	if idx == 3 then
		local monsters = {"寒露殿弟子","寒露殿精锐"}
		CALL("任务点杀怪",monsters,10009,100919)
	end
	--[PathMon,寒露殿精锐,10009,100919]
	if idx == 4 then
		local monsters = {"寒露殿精锐"}
		CALL("任务点杀怪",monsters,10009,100919)
	end
end,
--玉泉灵踪
[18006] = function(idx)
	--[PathArea,侧院入口,10009,300604]
	if idx == 0 then
		CALL("任务点移动",10009,300604)
	end
end,
--踏雪登云-太白云海
[18314] = function(idx)
	--[PathNpc,公孙剑,10009,579]
	if idx == 0 then
		CALL("任务点对话",10111061,"公孙剑",18314,0,10009,579)
	end
end,
--沧林偃月
[18008] = function(idx)
	--[PathArea,到达北斗坪,10009,300607]
	if idx == 0 then
		CALL("任务点移动",10009,300607)
	end
end,
--乾坤倒转-有始有终
[18316] = function(idx)
	--[PathNpc,唐林,10009,528]
	if idx == 0 then
		CALL("任务点对话",10111020,"唐林",18316,0,10009,528)
	end
end,
--乾坤倒转-青竹丹枫
[18317] = function(idx)
	--[PathNpc,唐青枫,10009,529]
	if idx == 4 then
		CALL("任务点对话",10111021,"唐青枫",18317,4,10009,529)
	end
end,
--乾坤倒转-北斗坪深
[18318] = function(idx)
	--[PathArea,出去查探,10009,300868]
	if idx == 3 then
		CALL("任务点移动",10009,300868)
	end
end,
--乾坤倒转-青龙过隙
[18352] = function(idx)
	--[PathMon,乾坤会伏兵,10009,100949]
	if idx == 1 then
		local monsters = {"乾坤会伏兵","乾坤会刺客"}
		CALL("任务点杀怪",monsters,10009,100949)
	end
	--[PathMon,乾坤会刺客,10009,100949]
	if idx == 2 then
		local monsters = {"乾坤会刺客"}
		CALL("任务点杀怪",monsters,10009,100949)
	end
end,
--山巅之约
[18010] = function(idx)
	--[PathArea,蛟龙岭,10009,300600]
	if idx == 0 then
		CALL("任务点移动",10009,300600)
	end
end,

--------5,天香冲穴点补偿--------
--天香冲穴点补偿
[65530] = function(idx)
end,

--------602,剑池遗案引杀机--------
--景星麟凤-事成而归
[18333] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 0 then
		CALL("任务点对话",10111012,"独孤若虚",18333,0,10009,531)
	end
end,
--景星麟凤-寒江弟子
[18334] = function(idx)
	--[PathNpc,温景梵,10009,532]
	if idx == 1 then
		CALL("任务点对话",10111024,"温景梵",18334,1,10009,532)
	end
end,
--景星麟凤-即刻启程
[18335] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 2 then
		CALL("任务点对话",10111012,"独孤若虚",18335,2,10009,531)
	end
end,
--碧落黄泉-失魂落魄
[18319] = function(idx)
	--[PathNpc,公孙剑,10009,556]
	if idx == 0 then
		CALL("任务点对话",10111006,"公孙剑",18319,0,10009,556)
	end
end,
--碧落黄泉-甘棠遗爱
[18320] = function(idx)
	--[PathEnt,棣棠花,10009,40023]
	if idx == 1 then
		CALL("任务点采集","棣棠花",10009,40023)
	end
end,
--碧落黄泉-天人永隔
[18321] = function(idx)
	--[PathNpc,公孙剑,10009,556]
	if idx == 2 then
		CALL("任务点对话",10111006,"公孙剑",18321,2,10009,556)
	end
end,
--小雪初晴-亦步亦趋
[18322] = function(idx)
	--[PathMon,初雪殿刺客,10009,100965]
	if idx == 0 then
		local monsters = {"初雪殿刺客","初雪殿密探"}
		CALL("任务点杀怪",monsters,10009,100965)
	end
	--[PathMon,初雪殿密探,10009,100965]
	if idx == 1 then
		local monsters = {"初雪殿密探"}
		CALL("任务点杀怪",monsters,10009,100965)
	end
end,
--小雪初晴-片甲不留
[18323] = function(idx)
	--[PathMon,初雪殿杀手,10009,100996]
	if idx == 2 then
		local monsters = {"初雪殿杀手","初雪殿密使"}
		CALL("任务点杀怪",monsters,10009,100996)
	end
	--[PathMon,初雪殿密使,10009,100996]
	if idx == 3 then
		local monsters = {"初雪殿密使"}
		CALL("任务点杀怪",monsters,10009,100996)
	end
end,
--小雪初晴-覆军杀将
[18324] = function(idx)
	--[PathMon,初雪殿精锐,10009,101014]
	if idx == 4 then
		local monsters = {"初雪殿精锐","初雪殿剑客"}
		CALL("任务点杀怪",monsters,10009,101014)
	end
	--[PathMon,初雪殿剑客,10009,101014]
	if idx == 5 then
		local monsters = {"初雪殿剑客"}
		CALL("任务点杀怪",monsters,10009,101014)
	end
end,
--冻原小镇-好问决疑
[18325] = function(idx)
	--[PathNpc,鹦哥镇守卫,10009,580]
	if idx == 1 then
		CALL("任务点对话",10111062,"鹦哥镇守卫",18325,1,10009,580)
	end
end,
--冻原小镇-虚怀若谷
[18326] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 0 then
		CALL("任务点对话",10111012,"独孤若虚",18326,0,10009,531)
	end
end,
--相敬如宾
[18015] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 0 then
		CALL("任务功能对话",18015,18033,"独孤若虚","[PathNpc,独孤若虚,10009,531]",10009,531)
	end
end,
--笑月湖湾-东捱西问
[18327] = function(idx)
	--[PathNpc,鹦哥镇守卫,10009,543]
	if idx == 0 then
		CALL("任务点对话",10111055,"鹦哥镇守卫",18327,0,10009,543)
	end
end,
--笑月湖湾-一举两得
[18328] = function(idx)
	--[PathEnt,桃儿七,10009,40041]
	if idx == 1 then
		CALL("任务点采集","桃儿七",10009,40041)
	end
	--[PathEnt,接骨丹,10009,40026]
	if idx == 2 then
		CALL("任务点采集","接骨丹",10009,40026)
	end
	--[PathMon,初雪殿香主,10009,101055]
	if idx == 3 then
		local monsters = {"初雪殿香主","初雪殿女婢","初雪殿护卫"}
		CALL("任务点杀怪",monsters,10009,101055)
	end
	--[PathMon,初雪殿女婢,10009,101055]
	if idx == 4 then
		local monsters = {"初雪殿女婢","初雪殿护卫"}
		CALL("任务点杀怪",monsters,10009,101055)
	end
	--[PathMon,初雪殿护卫,10009,101055]
	if idx == 5 then
		local monsters = {"初雪殿护卫"}
		CALL("任务点杀怪",monsters,10009,101055)
	end
end,
--笑月湖湾-满载而归
[18329] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 6 then
		CALL("任务点对话",10111012,"独孤若虚",18329,6,10009,531)
	end
end,
--医国妙手-稍等片刻
[18330] = function(idx)
	--稍等片刻
	if idx == 0 then
		CALL("等待任务完成",18330)
	end
end,
--医国妙手-灵丹妙药
[18331] = function(idx)
	--[PathNpc,独孤若虚,10009,531]
	if idx == 1 then
		CALL("任务点对话",10111012,"独孤若虚",18331,1,10009,531)
	end
end,
--医国妙手-药到病除
[18332] = function(idx)
	--[PathNpc,寒江城伤患,10009,542]
	if idx == 2 then
		CALL("任务点对话",10111030,"寒江城伤患",18332,2,10009,542)
	end
	--[PathNpc,寒江城伤患,10009,540]
	if idx == 3 then
		CALL("任务点对话",10111029,"寒江城伤患",18332,3,10009,540)
	end
	--[PathNpc,鹦哥镇伤患,10009,541]
	if idx == 4 then
		CALL("任务点对话",10111028,"鹦哥镇伤患",18332,4,10009,541)
	end
	--[PathNpc,鹦哥镇伤患,10009,539]
	if idx == 5 then
		CALL("任务点对话",10111027,"鹦哥镇伤患",18332,5,10009,539)
	end
	--[PathNpc,鹦哥镇伤患,10009,538]
	if idx == 6 then
		CALL("任务点对话",10111026,"鹦哥镇伤患",18332,6,10009,538)
	end
end,
--草海伏兵-鹦哥镇外
[18336] = function(idx)
	--[PathMon,初雪殿伏兵,10009,101062]
	if idx == 0 then
		local monsters = {"初雪殿伏兵","初雪殿斥候"}
		CALL("任务点杀怪",monsters,10009,101062)
	end
	--[PathMon,初雪殿斥候,10009,101062]
	if idx == 1 then
		local monsters = {"初雪殿斥候"}
		CALL("任务点杀怪",monsters,10009,101062)
	end
end,
--草海伏兵-深入腹地
[18337] = function(idx)
	--[PathMon,无极会杀手,10009,101118]
	if idx == 2 then
		local monsters = {"无极会杀手","无极会刺客"}
		CALL("任务点杀怪",monsters,10009,101118)
	end
	--[PathMon,无极会刺客,10009,101118]
	if idx == 3 then
		local monsters = {"无极会刺客"}
		CALL("任务点杀怪",monsters,10009,101118)
	end
end,
--无穷无极-龙潭虎穴
[18338] = function(idx)
	--[PathArea,药王谷口,10009,300608]
	if idx == 0 then
		CALL("任务点移动",10009,300608)
	end
end,
--无穷无极-一网打尽
[18339] = function(idx)
	--[PathMon,无极会弟子,10009,101132]
	if idx == 1 then
		local monsters = {"无极会弟子","无极会凶徒"}
		CALL("任务点杀怪",monsters,10009,101132)
	end
	--[PathMon,无极会凶徒,10009,101132]
	if idx == 2 then
		local monsters = {"无极会凶徒"}
		CALL("任务点杀怪",monsters,10009,101132)
	end
end,

--------1001,回师门路遇挑衅--------
--君山脚下
[28801] = function(idx)
	--[PathNpc,李阳白,10003,1]
	if idx == 0 then
		CALL("任务点对话",10117001,"李阳白",28801,0,10003,1)
	end
end,
--寻衅上门
[28802] = function(idx)
	--[PathNpc,前去比试,10003,26]
	if idx == 0 then
		CALL("任务点移动",10003,26)
	end
	--[PathMon,挑衅高手,10003,26]
	if idx == 1 then
		local monsters = {"挑衅高手"}
		CALL("任务点杀怪",monsters,10003,26)
	end
end,
--回到总舵-辞行
[28851] = function(idx)
	--[PathNpc,李阳白,10003,1]
	if idx == 0 then
		CALL("任务功能对话",28851,12206,"李阳白","[PathNpc,李阳白,10003,1]",10003,1)
	end
end,
--回到总舵-叮嘱
[28852] = function(idx)
	--[PathNpc,秦玲,10003,2]
	if idx == 1 then
		CALL("任务点对话",10117002,"秦玲",28852,1,10003,2)
	end
end,
--帮内小憩-关怀
[10192] = function(idx)
	--[PathNpc,解俊,10003,3]
	if idx == 0 then
		CALL("任务点对话",10117003,"解俊",10192,0,10003,3)
	end
end,
--帮内小憩-关怀
[10193] = function(idx)
	--[PathNpc,苏紫菡,10003,5]
	if idx == 2 then
		CALL("任务点对话",10117005,"苏紫菡",10193,2,10003,5)
	end
end,
--帮内小憩-关怀
[10194] = function(idx)
	--[PathNpc,冉敬宏,10003,4]
	if idx == 1 then
		CALL("任务点对话",10117004,"冉敬宏",10194,1,10003,4)
	end
end,
--授艺师兄-督促技艺
[28853] = function(idx)
	--[PathNpc,裘墨阳,10003,6]
	if idx == 0 then
		CALL("任务点对话",10117006,"裘墨阳",28853,0,10003,6)
	end
end,
--授艺师兄-心有灵犀
[28854] = function(idx)
	--[PathNpc,打坐一分钟,10003,6]
	if idx == 1 then
		CALL("等待任务完成",28854)
	end
end,
--师门复命
[28806] = function(idx)
	--[PathNpc,骆子渔,10003,7]
	if idx == 0 then
		CALL("任务点对话",10117007,"骆子渔",28806,0,10003,7)
	end
end,
--青竹来意-疑问
[28855] = function(idx)
	--[PathNpc,骆子渔,10003,7]
	if idx == 0 then
		CALL("任务点对话",10117007,"骆子渔",28855,0,10003,7)
	end
end,
--青竹来意-求证
[28856] = function(idx)
	--[PathNpc,莫奇,10003,8]
	if idx == 1 then
		CALL("任务点对话",10117008,"莫奇",28856,1,10003,8)
	end
end,
--执法弟子
[28808] = function(idx)
	--[PathNpc,王林,10003,9]
	if idx == 0 then
		CALL("任务点对话",10117009,"王林",28808,0,10003,9)
	end
end,
--寻找江山
[28809] = function(idx)
	--[PathNpc,江山,10003,10]
	if idx == 0 then
		CALL("任务功能对话",28809,12200,"江山","[PathNpc,江山,10003,10]",10003,10)
	end
end,
--事出有因
[28810] = function(idx)
	--[PathNpc,江山,10003,10]
	if idx == 0 then
		CALL("任务点对话",10117014,"江山",28810,0,10003,10)
	end
end,

--------900481,??--------
--原野茫茫-驿站问路
[14501] = function(idx)
	--[PathNpc,古陶驿使,10013,2]
	if idx == 0 then
		CALL("任务点对话",10109001,"古陶驿使",14501,0,10013,2)
	end
end,
--原野茫茫-镖走天下
[14502] = function(idx)
	--[PathNpc,郑天石,10013,3]
	if idx == 1 then
		CALL("任务点对话",10109002,"郑天石",14502,1,10013,3)
	end
end,
--荡平马贼-小贼聚众
[14503] = function(idx)
	--[PathMon,徐海马贼,10013,100458]
	if idx == 0 then
		local monsters = {"徐海马贼","马贼哨子","马贼杆首"}
		CALL("任务点杀怪",monsters,10013,100458)
	end
	--[PathMon,马贼哨子,10013,100458]
	if idx == 1 then
		local monsters = {"马贼哨子","马贼杆首"}
		CALL("任务点杀怪",monsters,10013,100458)
	end
	--[PathMon,马贼杆首,10013,100458]
	if idx == 2 then
		local monsters = {"马贼杆首"}
		CALL("任务点杀怪",monsters,10013,100458)
	end
end,
--荡平马贼-贼人嚣张
[14504] = function(idx)
	--[PathMon,马贼彪汉,10013,100286]
	if idx == 3 then
		local monsters = {"马贼彪汉","马贼精锐"}
		CALL("任务点杀怪",monsters,10013,100286)
	end
	--[PathMon,马贼精锐,10013,100286]
	if idx == 4 then
		local monsters = {"马贼精锐"}
		CALL("任务点杀怪",monsters,10013,100286)
	end
end,
--荡平马贼-马贼头目
[14505] = function(idx)
	--[PathMon,马贼头目,10013,95]
	if idx == 5 then
		local monsters = {"马贼头目"}
		CALL("任务点杀怪",monsters,10013,95)
	end
end,
--终有眉目-小人难缠
[14506] = function(idx)
	--[PathNpc,郑天石,10013,3]
	if idx == 0 then
		CALL("任务点对话",10109002,"郑天石",14506,0,10013,3)
	end
end,
--终有眉目-琐碎小事
[14507] = function(idx)
	--[PathEnt,镖局货物,10013,40027]
	if idx == 1 then
		CALL("任务点采集","镖局货物",10013,40027)
	end
end,
--终有眉目-终开金口
[14508] = function(idx)
	--[PathNpc,郑天石,10013,3]
	if idx == 2 then
		CALL("任务点对话",10109002,"郑天石",14508,2,10013,3)
	end
end,
--一路奔波-取径古镇
[14509] = function(idx)
	--[PathArea,古陶官道,10013,300029]
	if idx == 0 then
		CALL("任务点移动",10013,300029)
	end
end,
--一路奔波-节外生枝
[14510] = function(idx)
	--[PathArea,呼救路人,10013,300030]
	if idx == 1 then
		CALL("任务点移动",10013,300030)
	end
end,
--一路奔波-奇毒奇草
[14511] = function(idx)
	--[PathEnt,五叶草,10013,40006]
	if idx == 2 then
		CALL("任务点采集","五叶草",10013,40006)
	end
end,
--一路奔波-事出有因
[14512] = function(idx)
	--[PathNpc,杜季,10013,23]
	if idx == 3 then
		CALL("任务点对话",10109008,"杜季",14512,3,10013,23)
	end
end,
--路见不平-杜家仇敌
[14513] = function(idx)
	--[PathMon,寻衅强徒,10013,100413]
	if idx == 0 then
		local monsters = {"寻衅强徒","寻衅恶汉","强徒头领"}
		CALL("任务点杀怪",monsters,10013,100413)
	end
	--[PathMon,寻衅恶汉,10013,100413]
	if idx == 1 then
		local monsters = {"寻衅恶汉","强徒头领"}
		CALL("任务点杀怪",monsters,10013,100413)
	end
	--[PathMon,强徒头领,10013,100413]
	if idx == 2 then
		local monsters = {"强徒头领"}
		CALL("任务点杀怪",monsters,10013,100413)
	end
end,
--路见不平-奇人莽汉
[14514] = function(idx)
	--[PathMon,寻仇莽汉 燕俢,10013,96]
	if idx == 3 then
		local monsters = {"寻仇莽汉 燕俢"}
		CALL("任务点杀怪",monsters,10013,96)
	end
end,
--古镇重逢-徐海小镇
[14515] = function(idx)
	--[PathArea,古陶镇,10013,300049]
	if idx == 0 then
		CALL("任务点移动",10013,300049)
	end
end,
--古镇重逢-路遇佳人
[14516] = function(idx)
	--[PathNpc,明月心,10013,646]
	if idx == 1 then
		CALL("任务点对话",10109137,"明月心",14516,1,10013,646)
	end
end,
--古镇重逢-酒逢知己
[14517] = function(idx)
	--[PathNpc,明月心,10013,646]
	if idx == 2 then
		CALL("任务功能对话",14517,16101,"明月心","[PathNpc,明月心,10013,646]",10013,646)
	end
end,
--各怀鬼胎-危机四伏
[14518] = function(idx)
	--[PathNpc,燕南飞,10013,645]
	if idx == 0 then
		CALL("任务点对话",10109132,"燕南飞",14518,0,10013,645)
	end
end,
--各怀鬼胎-杜二当家
[14519] = function(idx)
	--[PathNpc,杜仲,10013,24]
	if idx == 1 then
		CALL("任务功能对话",14519,10509015,"杜仲","[PathNpc,杜仲,10013,24]",10013,24)
	end
end,
--各怀鬼胎-金玉其外
[14520] = function(idx)
	--[PathNpc,竹官儿,10013,364]
	if idx == 2 then
		CALL("任务功能对话",14520,10509016,"竹官儿","[PathNpc,竹官儿,10013,364]",10013,364)
	end
end,
--初结佛缘-离开古陶
[14521] = function(idx)
	--[PathArea,离开古陶,10013,300034]
	if idx == 0 then
		CALL("任务点移动",10013,300034)
	end
end,
--初结佛缘-僧人稽首
[14522] = function(idx)
	--[PathNpc,天龙寺僧人,10013,97]
	if idx == 1 then
		CALL("任务点对话",10109012,"天龙寺僧人",14522,1,10013,97)
	end
end,
--初结佛缘-此消彼长
[14523] = function(idx)
	--[PathMon,乾堂帮众,10013,100726]
	if idx == 2 then
		local monsters = {"乾堂帮众","乾堂哨子","帮众头领"}
		CALL("任务点杀怪",monsters,10013,100726)
	end
	--[PathMon,乾堂哨子,10013,100726]
	if idx == 3 then
		local monsters = {"乾堂哨子","帮众头领"}
		CALL("任务点杀怪",monsters,10013,100726)
	end
	--[PathMon,帮众头领,10013,100726]
	if idx == 4 then
		local monsters = {"帮众头领"}
		CALL("任务点杀怪",monsters,10013,100726)
	end
end,
--初结佛缘-善恶有报
[14524] = function(idx)
	--[PathMon,乾堂精锐,10013,100035]
	if idx == 5 then
		local monsters = {"乾堂精锐","精锐头领"}
		CALL("任务点杀怪",monsters,10013,100035)
	end
	--[PathMon,精锐头领,10013,100035]
	if idx == 6 then
		local monsters = {"精锐头领"}
		CALL("任务点杀怪",monsters,10013,100035)
	end
end,
--各怀鬼胎-市井伙夫
[14577] = function(idx)
	--[PathNpc,庞三刀,10013,397]
	if idx == 3 then
		CALL("任务功能对话",14577,10509017,"庞三刀","[PathNpc,庞三刀,10013,397]",10013,397)
	end
end,
--各怀鬼胎-奇侠奇缘
[14578] = function(idx)
	--[PathNpc,杜十七,10013,98]
	if idx == 4 then
		CALL("任务功能对话",14578,10509018,"杜十七","[PathNpc,杜十七,10013,98]",10013,98)
	end
end,
--各怀鬼胎-青龙目的
[14579] = function(idx)
	--[PathNpc,燕南飞,10013,645]
	if idx == 5 then
		CALL("任务点对话",10109132,"燕南飞",14579,5,10013,645)
	end
end,
--天龙古刹-天龙古刹
[14593] = function(idx)
	--[PathArea,天龙古刹,10013,300047]
	if idx == 0 then
		CALL("任务点移动",10013,300047)
	end
end,
--天龙古刹-又见故人
[14594] = function(idx)
	--[PathNpc,明月心,10013,647]
	if idx == 1 then
		CALL("任务点对话",10109138,"明月心",14594,1,10013,647)
	end
end,

--------603,反客为主护悲赋--------
--恶贯满盈-担惊受怕
[18340] = function(idx)
	--[PathNpc,惊慌的行商,10009,545]
	if idx == 0 then
		CALL("任务点对话",10111057,"惊慌的行商",18340,0,10009,545)
	end
end,
--恶贯满盈-言颠语倒
[18341] = function(idx)
	--[PathNpc,太白弟子,10009,547]
	if idx == 1 then
		CALL("任务点对话",10111059,"太白弟子",18341,1,10009,547)
	end
end,
--恶贯满盈-罪魁祸首
[18342] = function(idx)
	--[PathMon,无极会精英,10009,101157]
	if idx == 2 then
		local monsters = {"无极会精英","无极会密探"}
		CALL("任务点杀怪",monsters,10009,101157)
	end
	--[PathMon,无极会密探,10009,101157]
	if idx == 3 then
		local monsters = {"无极会密探"}
		CALL("任务点杀怪",monsters,10009,101157)
	end
end,
--药谷龙踪
[18023] = function(idx)
	--[PathNpc,公孙剑,10009,557]
	if idx == 0 then
		CALL("任务功能对话",18023,18034,"公孙剑","[PathNpc,公孙剑,10009,557]",10009,557)
	end
end,
--赤梅似血-天山往事
[18343] = function(idx)
	--[PathNpc,钟舒文,10009,558]
	if idx == 0 then
		CALL("任务点对话",10111031,"钟舒文",18343,0,10009,558)
	end
end,
--赤梅似血-虚张声势
[18344] = function(idx)
	--[PathNpc,公孙剑,10009,559]
	if idx == 1 then
		CALL("任务点对话",10111008,"公孙剑",18344,1,10009,559)
	end
end,
--赤梅似血-单刀赴会
[18345] = function(idx)
	--[PathMon,青龙会狂徒,10009,101203]
	if idx == 2 then
		local monsters = {"青龙会狂徒","青龙会杀手","青龙会刺客"}
		CALL("任务点杀怪",monsters,10009,101203)
	end
	--[PathMon,青龙会杀手,10009,101203]
	if idx == 3 then
		local monsters = {"青龙会杀手","青龙会刺客"}
		CALL("任务点杀怪",monsters,10009,101203)
	end
	--[PathMon,青龙会刺客,10009,101203]
	if idx == 4 then
		local monsters = {"青龙会刺客"}
		CALL("任务点杀怪",monsters,10009,101203)
	end
end,
--千醉慕白-长驱深入
[18346] = function(idx)
	--[PathArea,醉白池前,10009,300609]
	if idx == 0 then
		CALL("任务点移动",10009,300609)
	end
end,
--千醉慕白-云飞烟灭
[18347] = function(idx)
	--[PathMon,青龙会精锐,10009,101237]
	if idx == 1 then
		local monsters = {"青龙会精锐","青龙会斥候"}
		CALL("任务点杀怪",monsters,10009,101237)
	end
	--[PathMon,青龙会斥候,10009,101237]
	if idx == 2 then
		local monsters = {"青龙会斥候"}
		CALL("任务点杀怪",monsters,10009,101237)
	end
end,
--惊世双骄
[18026] = function(idx)
	--[PathNpc,公孙剑,10009,560]
	if idx == 0 then
		CALL("任务功能对话",18026,18035,"公孙剑","[PathNpc,公孙剑,10009,560]",10009,560)
	end
end,
--帝王寒江-水落归槽
[18348] = function(idx)
	--[PathNpc,公孙剑,10009,561]
	if idx == 0 then
		CALL("任务点对话",10111010,"公孙剑",18348,0,10009,561)
	end
end,
--帝王寒江-不遑宁息
[18349] = function(idx)
	--[PathNpc,独孤若虚,10009,562]
	if idx == 1 then
		CALL("任务点对话",10111013,"独孤若虚",18349,1,10009,562)
	end
end,
--帝王寒江-太白驿站
[18350] = function(idx)
	--[PathNpc,何荐华,10009,568]
	if idx == 2 then
		CALL("任务点对话",10111033,"何荐华",18350,2,10009,568)
	end
end,
--尘埃落定
[18028] = function(idx)
	--[PathArea,太白沉剑池,10009,300601]
	if idx == 0 then
		CALL("任务点移动",10009,300601)
	end
end,
--再续燕云
[18029] = function(idx)
	--[PathNpc,燕南飞,10009,590]
	if idx == 0 then
		CALL("任务点对话",10111014,"燕南飞",18029,0,10009,590)
	end
end,
--天涯未远
[18354] = function(idx)
	--未完待续
	if idx == 0 then
		LogWarning("未知的任务处理:%s","未完待续")
	end
end,
--界限突破陆
[18355] = function(idx)
	--等待突破：沧海月明(90级)
	if idx == 0 then
		CALL("界限突破",18355)
	end
end,

--------1002,洗清白同门联手--------
--打探消息-龙头阁
[28871] = function(idx)
	--[Map_X_Y,龙头阁,10003,2027,1579]
	if idx == 0 then
		CALL("坐标点移动",2027,1579,10003)
	end
end,
--打探消息-拦路人
[28872] = function(idx)
	--[PathNPCLayer,守山弟子,10003,11,1]
	if idx == 1 then
		--桥上?
		CALL("任务点对话",10117015,"守山弟子",28872,1,10003,11,1)
	end
end,
--打探消息-碰头
[28873] = function(idx)
	--[PathNpc,江山,10003,13]
	if idx == 2 then
		CALL("任务点对话",10117017,"江山",28873,2,10003,13)
	end
end,
--联手闯阁
[28812] = function(idx)
	--[PathNpc,江山,10003,13]
	if idx == 0 then
		CALL("任务功能对话",28812,12201,"江山","[PathNpc,江山,10003,13]",10003,13)
	end
end,
--冲破拦截
[28813] = function(idx)
	--前往[PathNpc,龙头阁,10003,100202]前
	if idx == 0 then
		CALL("任务点移动",10003,100202)
	end
	--[PathMon,竹坞堂高手,10003,100202]
	if idx == 1 then
		local monsters = {"竹坞堂高手","竹坞堂打手","竹坞堂弟子"}
		CALL("任务点杀怪",monsters,10003,100202)
	end
	--[PathMon,竹坞堂打手,10003,100202]
	if idx == 2 then
		local monsters = {"竹坞堂打手","竹坞堂弟子"}
		CALL("任务点杀怪",monsters,10003,100202)
	end
	--[PathMon,竹坞堂弟子,10003,100202]
	if idx == 3 then
		local monsters = {"竹坞堂弟子"}
		CALL("任务点杀怪",monsters,10003,100202)
	end
end,
--寻找江山-会合
[28845] = function(idx)
	--[PathNpc,江山,10003,14]
	if idx == 0 then
		CALL("任务点对话",10117022,"江山",28845,0,10003,14)
	end
end,
--寻找江山-疗伤
[28846] = function(idx)
	--[PathNpc,散瘀兰,10003,40010]
	if idx == 1 then
		CALL("任务点采集","散瘀兰",10003,40010)
	end
end,
--寻找江山-送药
[28847] = function(idx)
	--[PathNpc,秦岭,10003,14]
	if idx == 2 then
		CALL("任务点对话",10117022,"秦岭",28847,2,10003,14)
	end
end,
--夺青竹令-剪除羽翼
[28874] = function(idx)
	--[PathMon,竹笑堂精锐,10003,100034]
	if idx == 0 then
		local monsters = {"竹笑堂精锐","竹笑堂打手"}
		CALL("任务点杀怪",monsters,10003,100034)
	end
	--[PathMon,竹笑堂打手,10003,100034]
	if idx == 1 then
		local monsters = {"竹笑堂打手"}
		CALL("任务点杀怪",monsters,10003,100034)
	end
end,
--夺青竹令-夺回令牌
[28875] = function(idx)
	--找到[PathMon,方必穹,10003,31]
	if idx == 2 then
		LogWarning("未知的任务处理:%s","找到[PathMon,方必穹,10003,31]")
	end
	--夺回[PathNpc,青竹令,10003,31]
	if idx == 3 then
		local monsters = {"青竹令"}
		CALL("任务点杀怪",monsters,10003,40026)
	end
	--取得[PathNpc,酒仙酿,10003,40026]
	if idx == 4 then
		CALL("任务点采集","酒仙酿",10003,40026)
	end
end,
--寻找江山
[28816] = function(idx)
	--[PathNpc,江山,10003,15]
	if idx == 0 then
		CALL("任务点对话",10117023,"江山",28816,0,10003,15)
	end
end,
--破围而出
[28817] = function(idx)
	--[PathNpc,江山,10003,15]
	if idx == 0 then
		CALL("任务功能对话",28817,12202,"江山","[PathNpc,江山,10003,15]",10003,15)
	end
end,
--商量对策
[28818] = function(idx)
	--[PathNpc,江山,10003,17]
	if idx == 0 then
		CALL("任务点对话",10117029,"江山",28818,0,10003,17)
	end
end,
--入夜回帮
[28819] = function(idx)
	--[PathNpc,江山,10003,17]
	if idx == 0 then
		CALL("任务功能对话",28819,12205,"江山","[PathNpc,江山,10003,17]",10003,17)
	end
end,
--商量对策-下落
[10198] = function(idx)
	--[PathNpc,江山,10003,33]
	if idx == 0 then
		CALL("任务点对话",10117024,"江山",10198,0,10003,33)
	end
end,

--------900482,??--------
--奇书风波-约定之日
[14416] = function(idx)
	--[PathNpc,明月心,10013,656]
	if idx == 1 then
		CALL("任务功能对话",14416,16106,"明月心","[PathNpc,明月心,10013,656]",10013,656)
	end
end,
--荒野追命-旷野疾奔
[14525] = function(idx)
	--[PathArea,云眉道,10013,300033]
	if idx == 0 then
		CALL("任务点移动",10013,300033)
	end
end,
--荒野追命-密林遭伏
[14526] = function(idx)
	--[PathMon,午堂帮众,10013,100063]
	if idx == 1 then
		local monsters = {"午堂帮众","午堂哨子"}
		CALL("任务点杀怪",monsters,10013,100063)
	end
	--[PathMon,午堂哨子,10013,100063]
	if idx == 2 then
		local monsters = {"午堂哨子"}
		CALL("任务点杀怪",monsters,10013,100063)
	end
end,
--荒野追命-勇往直前
[14527] = function(idx)
	--[PathMon,午堂打手,10013,100778]
	if idx == 3 then
		local monsters = {"午堂打手","午堂追兵","追兵头领"}
		CALL("任务点杀怪",monsters,10013,100780)
	end
	--[PathMon,午堂追兵,10013,100778]
	if idx == 4 then
		local monsters = {"午堂追兵","追兵头领"}
		CALL("任务点杀怪",monsters,10013,100780)
	end
	--[PathMon,追兵头领,10013,100780]
	if idx == 5 then
		local monsters = {"追兵头领"}
		CALL("任务点杀怪",monsters,10013,100780)
	end
end,
--剑绝轩前-燕子扶摇
[14528] = function(idx)
	--[PathArea,剑绝轩,10013,300032]
	if idx == 0 then
		CALL("任务点移动",10013,300032)
	end
end,
--剑绝轩前-相约而至
[14529] = function(idx)
	--[PathNpc,明月心,10013,648]
	if idx == 1 then
		CALL("任务点对话",10109139,"明月心",14529,1,10013,648)
	end
end,
--深入虎穴-杀上山门
[14530] = function(idx)
	--[PathMon,午堂守卫,11308,101263]
	if idx == 0 then
		local monsters = {"午堂守卫","午堂剑客","剑客精锐"}
		CALL("任务点杀怪",monsters,11308,100203)
	end
	--[PathMon,午堂剑客,11308,101265]
	if idx == 1 then
		local monsters = {"午堂剑客","剑客精锐"}
		CALL("任务点杀怪",monsters,11308,100203)
	end
	--[PathMon,剑客精锐,11308,100203]
	if idx == 2 then
		local monsters = {"剑客精锐"}
		CALL("任务点杀怪",monsters,11308,100203)
	end
end,
--深入虎穴-午堂逼命
[14531] = function(idx)
	--[PathMon,午堂高手,11308,101299]
	if idx == 3 then
		local monsters = {"午堂高手","午堂女卫","午堂头领"}
		CALL("任务点杀怪",monsters,11308,101317)
	end
	--[PathMon,午堂女卫,11308,101310]
	if idx == 4 then
		local monsters = {"午堂女卫","午堂头领"}
		CALL("任务点杀怪",monsters,11308,101317)
	end
	--[PathMon,午堂头领,11308,101317]
	if idx == 5 then
		local monsters = {"午堂头领"}
		CALL("任务点杀怪",monsters,11308,101317)
	end
end,
--深入虎穴-大战不休
[14532] = function(idx)
	--[PathMon,玄剑,11308,677]
	if idx == 6 then
		local monsters = {"玄剑"}
		CALL("任务点杀怪",monsters,11308,677)
	end
end,
--红颜知己-暂缓毒性
[14533] = function(idx)
	--[PathNpc,搜山虎,10013,40072]
	if idx == 1 then
		CALL("任务功能对话",14533,10509006,"搜山虎","[PathNpc,搜山虎,10013,40072]",10013,40072)
	end
end,
--红颜知己-天真无邪
[14534] = function(idx)
	--[PathNpc,秋小清,10013,657]
	if idx == 2 then
		CALL("任务功能对话",14534,10509007,"秋小清","[PathNpc,秋小清,10013,657]",10013,657)
	end
end,
--红颜知己-童真暖人
[14535] = function(idx)
	--[PathNpc,溪流里,10013,300925]
	if idx == 3 then
		LogWarning("未知的任务处理:%s","[PathNpc,溪流里,10013,300925]")
	end
end,
--循迹而追-枫林挽阳
[14536] = function(idx)
	--[PathArea,骅阳林,10013,300031]
	if idx == 0 then
		CALL("任务点移动",10013,300031)
	end
end,
--循迹而追-一路血踪
[14537] = function(idx)
	--[PathEnt,调查血迹,10013,40040]
	if idx == 1 then
		CALL("任务点采集","调查血迹",10013,40040)
	end
end,
--循迹而追-误见贼人
[14538] = function(idx)
	--[PathMon,骅阳林毛贼,10013,100840]
	if idx == 2 then
		local monsters = {"骅阳林毛贼","毛贼哨子"}
		CALL("任务点杀怪",monsters,10013,100840)
	end
	--[PathMon,毛贼哨子,10013,100840]
	if idx == 3 then
		local monsters = {"毛贼哨子"}
		CALL("任务点杀怪",monsters,10013,100840)
	end
end,
--循迹而追-逼问贼寇
[14539] = function(idx)
	--[PathMon,骅阳林强徒,10013,100134]
	if idx == 4 then
		local monsters = {"骅阳林强徒","骅阳林恶汉"}
		CALL("任务点杀怪",monsters,10013,100134)
	end
	--[PathMon,骅阳林恶汉,10013,100134]
	if idx == 5 then
		local monsters = {"骅阳林恶汉"}
		CALL("任务点杀怪",monsters,10013,100134)
	end
end,
--神刀往事-询问叶开
[14540] = function(idx)
	--[PathNpc,叶开,10013,53]
	if idx == 0 then
		CALL("任务点对话",10109032,"叶开",14540,0,10013,53)
	end
end,
--神刀往事-前辈震怒
[14541] = function(idx)
	--[PathArea,花白凤,10013,300040]
	if idx == 1 then
		CALL("任务点移动",10013,300040)
	end
end,
--神刀往事-天魔逆徒
[14542] = function(idx)
	--[PathNpc,花无心,10013,55]
	if idx == 2 then
		CALL("任务点对话",10109034,"花无心",14542,2,10013,55)
	end
end,
--为友奔走-火爆护法
[14543] = function(idx)
	--[PathNpc,冷皓轩,10013,58]
	if idx == 0 then
		CALL("任务点对话",10109036,"冷皓轩",14543,0,10013,58)
	end
end,
--为友奔走-父女迥异
[14544] = function(idx)
	--[PathNpc,冷君怡,10013,59]
	if idx == 1 then
		CALL("任务点对话",10109035,"冷君怡",14544,1,10013,59)
	end
end,
--为友奔走-称取草药
[14545] = function(idx)
	--[PathEnt,青木香,10013,40080]
	if idx == 2 then
		CALL("任务点采集","青木香",10013,40080)
	end
	--[PathEnt,半枝莲,10013,40081]
	if idx == 3 then
		CALL("任务点采集","半枝莲",10013,40081)
	end
end,
--为友奔走-研磨草药
[14546] = function(idx)
	--[PathEnt,磨药石,10013,40082]
	if idx == 4 then
		CALL("任务点采集","磨药石",10013,40082)
	end
end,
--为友奔走-探望大侠
[14547] = function(idx)
	--[PathNpc,傅红雪,10013,300914]
	if idx == 5 then
		LogWarning("未知的任务处理:%s","[PathNpc,傅红雪,10013,300914]")
	end
end,
--为友奔走-巡视营地
[14548] = function(idx)
	--[PathArea,狂刀营东门,10013,300041]
	if idx == 6 then
		CALL("任务点移动",10013,300041)
	end
end,
--红颜知己-佳人忧心
[14575] = function(idx)
	--[PathNpc,明月心,10013,656]
	if idx == 0 then
		CALL("任务点对话",10109142,"明月心",14575,0,10013,656)
	end
end,
--红颜知己-采药归来
[14576] = function(idx)
	--[PathNpc,明月心,10013,656]
	if idx == 4 then
		CALL("任务点对话",10109142,"明月心",14576,4,10013,656)
	end
end,
--奇书风波-焦心以待
[14583] = function(idx)
	--等待六十秒。
	if idx == 0 then
		CALL("等待任务完成",14583)
	end
end,
--武林奇书-虚惊一场
[14590] = function(idx)
	--[PathArea,剑阁,10013,300918]
	if idx == 0 then
		CALL("任务点移动",10013,300918)
	end
end,
--武林奇书-话外有意
[14591] = function(idx)
	--[PathNpc,叶开,10013,1624]
	if idx == 1 then
		CALL("任务点对话",10109170,"叶开",14591,1,10013,1624)
	end
end,
--武林奇书-山间溪流
[14592] = function(idx)
	--[PathArea,傅红雪等人落脚处,10013,300924]
	if idx == 2 then
		CALL("任务点移动",10013,300924)
	end
end,

--------1600,帮派建造--------
--建设寻人
[42001] = function(idx)
	--和[PathEnt,开封诸葛俞,10012,1903]交谈
	if idx == 0 then
		CALL("任务点对话",10104632,"开封诸葛俞",42001,0,10012,1903)
	end
end,
--建设寻人
[42002] = function(idx)
	--和[PathEnt,开封雪秋千,10012,1904]交谈
	if idx == 0 then
		CALL("任务点对话",10104633,"开封雪秋千",42002,0,10012,1904)
	end
end,
--建设寻人
[42003] = function(idx)
	--和[PathEnt,开封澜八月,10012,1905]交谈
	if idx == 0 then
		CALL("任务点对话",10104634,"开封澜八月",42003,0,10012,1905)
	end
end,
--建设寻人
[42004] = function(idx)
	--与[PathEnt,开封赤名,10012,1913]交谈
	if idx == 0 then
		CALL("任务点对话",10104635,"开封赤名",42004,0,10012,1913)
	end
end,
--建设寻人
[42005] = function(idx)
	--和[PathEnt,开封海岳,10012,1914]交谈
	if idx == 0 then
		CALL("任务点对话",10104636,"开封海岳",42005,0,10012,1914)
	end
end,
--建设寻人
[42006] = function(idx)
	--和[PathEnt,开封风枪却,10012,1915]交谈
	if idx == 0 then
		CALL("任务点对话",10104637,"开封风枪却",42006,0,10012,1915)
	end
end,
--建设寻人
[42007] = function(idx)
	--和[PathEnt,开封三余,10012,1916]交谈
	if idx == 0 then
		CALL("任务点对话",10104638,"开封三余",42007,0,10012,1916)
	end
end,
--建设寻人
[42008] = function(idx)
	--寻人：找到[PathEnt,开封梓景逸,10012,1917]交谈
	if idx == 0 then
		CALL("任务点对话",10104639,"开封梓景逸",42008,0,10012,1917)
	end
end,
--建设寻人
[42009] = function(idx)
	--寻人：找到[PathEnt,开封尤焕申,10012,1918]交谈
	if idx == 0 then
		CALL("任务点对话",10104640,"开封尤焕申",42009,0,10012,1918)
	end
end,
--建设寻人
[42010] = function(idx)
	--寻人：找到[PathEnt,开封周景,10012,1919]交谈
	if idx == 0 then
		CALL("任务点对话",10104641,"开封周景",42010,0,10012,1919)
	end
end,

--------1003,锋芒露毕现杀机--------
--前往隐湖-参见帮主
[28891] = function(idx)
	--[Map_X_Y,隐湖,10003,1279,916]
	if idx == 0 then
		CALL("坐标点移动",1279,916,10003)
	end
end,
--前往隐湖-回禀内情
[28892] = function(idx)
	--[PathNpc,江匡,10003,19]
	if idx == 1 then
		CALL("任务点对话",10117035,"江匡",28892,1,10003,19)
	end
end,
--前往隐湖-茶和鱼饵
[28848] = function(idx)
	--找[PathNpc,淘淘,10003,220]要鱼饵
	if idx == 2 then
		CALL("任务功能对话",28848,10501032,"淘淘","找[PathNpc,淘淘,10003,220]要鱼饵",10003,220)
	end
	--给[PathNpc,炉灶,10003,40025]添火
	if idx == 3 then
		CALL("任务点采集","炉灶",10003,40025)
	end
end,
--前往隐湖-回复帮主
[28849] = function(idx)
	--[PathNpc,江匡,10003,19]
	if idx == 4 then
		CALL("任务点对话",10117035,"江匡",28849,4,10003,19)
	end
end,
--见机行事
[28822] = function(idx)
	--离开[PathNpc,隐湖,10003,100256]
	if idx == 0 then
		CALL("任务点移动",10003,100256)
	end
	--[PathMon,竹素堂打手,10003,100256]
	if idx == 1 then
		local monsters = {"竹素堂打手","竹素堂精锐"}
		CALL("任务点杀怪",monsters,10003,100256)
	end
	--[PathMon,竹素堂精锐,10003,100256]
	if idx == 2 then
		local monsters = {"竹素堂精锐"}
		CALL("任务点杀怪",monsters,10003,100256)
	end
end,
--莫奇师叔-摆脱
[29983] = function(idx)
	--[PathNpc,循声寻人,10003,32]
	if idx == 0 then
		CALL("任务点移动",10003,32)
	end
end,
--莫奇师叔-汇聚
[28894] = function(idx)
	--[PathNpc,莫奇,10003,32]
	if idx == 1 then
		CALL("任务点对话",10117036,"莫奇",28894,1,10003,32)
	end
end,
--执法无情
[28824] = function(idx)
	--前往[PathNpc,红梅小筑,10003,100144]
	if idx == 0 then
		CALL("任务点移动",10003,100144)
	end
	--[PathMon,执法队长,10003,100144]
	if idx == 1 then
		local monsters = {"执法队长","执法高手","执法弟子"}
		CALL("任务点杀怪",monsters,10003,100144)
	end
	--[PathMon,执法高手,10003,100144]
	if idx == 2 then
		local monsters = {"执法高手","执法弟子"}
		CALL("任务点杀怪",monsters,10003,100144)
	end
	--[PathMon,执法弟子,10003,100144]
	if idx == 3 then
		local monsters = {"执法弟子"}
		CALL("任务点杀怪",monsters,10003,100144)
	end
end,
--再遇同门
[28825] = function(idx)
	--[PathNpc,解俊,10003,34]
	if idx == 0 then
		CALL("任务点对话",10117043,"解俊",28825,0,10003,34)
	end
end,
--红梅小筑-赶赴
[28895] = function(idx)
	--去往[Map_X_Y,红梅小筑,10003,945,647]
	if idx == 0 then
		CALL("坐标点移动",945,647,10003)
	end
end,
--红梅小筑-硬闯
[28896] = function(idx)
	--[PathMon,竹松堂高手,10003,100172]
	if idx == 1 then
		local monsters = {"竹松堂高手","竹松堂弟子"}
		CALL("任务点杀怪",monsters,10003,100172)
	end
	--[PathMon,竹松堂弟子,10003,100172]
	if idx == 2 then
		local monsters = {"竹松堂弟子"}
		CALL("任务点杀怪",monsters,10003,100172)
	end
end,
--营救秦玲
[28827] = function(idx)
	--[Map_X_Y,营救秦岭,10003,945,647]
	if idx == 0 then
		CALL("坐标点移动",945,647,10003)
	end
end,
--赶赴祖祠-奔赴
[28897] = function(idx)
	--[Map_X_Y,前往丐祖祠,10003,1409,728]
	if idx == 0 then
		CALL("坐标点移动",1409,728,10003)
	end
end,
--赶赴祖祠-摆脱
[28898] = function(idx)
	--[PathMon,竹抱堂高手,10003,100285]
	if idx == 1 then
		local monsters = {"竹抱堂高手","竹抱堂精锐","竹抱堂弟子"}
		CALL("任务点杀怪",monsters,10003,100285)
	end
	--[PathMon,竹抱堂精锐,10003,100285]
	if idx == 2 then
		local monsters = {"竹抱堂精锐","竹抱堂弟子"}
		CALL("任务点杀怪",monsters,10003,100285)
	end
	--[PathMon,竹抱堂弟子,10003,100285]
	if idx == 3 then
		local monsters = {"竹抱堂弟子"}
		CALL("任务点杀怪",monsters,10003,100285)
	end
end,
--丐祖祠内
[28829] = function(idx)
	--[PathNpc,江山,10003,20]
	if idx == 0 then
		CALL("任务功能对话",28829,12204,"江山","[PathNpc,江山,10003,20]",10003,20)
	end
end,
--辞别师门-辞行
[28840] = function(idx)
	--[PathNpc,江山,10003,35]
	if idx == 0 then
		CALL("任务点对话",10117044,"江山",28840,0,10003,35)
	end
	--[PathNpc,江匡,10003,36]
	if idx == 1 then
		CALL("任务点对话",10117045,"江匡",28840,1,10003,36)
	end
end,
--辞别师门-杭州
[28841] = function(idx)
	--去杭州找[PathNpc,史晴龙,10010,300771]
	if idx == 2 then
		CALL("任务点移动",10010,300771)
	end
end,

--------900483,??--------
--战火再起-阎罗勾魂
[14549] = function(idx)
	--[PathMon,阎罗勾魂 武琼,10013,112]
	if idx == 0 then
		local monsters = {"阎罗勾魂 武琼"}
		CALL("任务点杀怪",monsters,10013,112)
	end
end,
--战火再起-刀手来袭
[14550] = function(idx)
	--[PathMon,神武门打手,10013,100169]
	if idx == 1 then
		local monsters = {"神武门打手","神武门弟子","打手头目"}
		CALL("任务点杀怪",monsters,10013,100156)
	end
	--[PathMon,神武门弟子,10013,100169]
	if idx == 2 then
		local monsters = {"神武门弟子","打手头目"}
		CALL("任务点杀怪",monsters,10013,100156)
	end
	--[PathMon,打手头目,10013,100156]
	if idx == 3 then
		local monsters = {"打手头目"}
		CALL("任务点杀怪",monsters,10013,100156)
	end
end,
--战火再起-击退敌众
[14551] = function(idx)
	--[PathMon,地字剑客,10013,100649]
	if idx == 4 then
		local monsters = {"地字剑客","地字刀客"}
		CALL("任务点杀怪",monsters,10013,100649)
	end
	--[PathMon,地字刀客,10013,100649]
	if idx == 5 then
		local monsters = {"地字刀客"}
		CALL("任务点杀怪",monsters,10013,100649)
	end
end,
--战火再起-精锐逼战
[14552] = function(idx)
	--[PathMon,地字精锐,10013,100186]
	if idx == 6 then
		local monsters = {"地字精锐","地字护法"}
		CALL("任务点杀怪",monsters,10013,100186)
	end
	--[PathMon,地字护法,10013,100186]
	if idx == 7 then
		local monsters = {"地字护法"}
		CALL("任务点杀怪",monsters,10013,100186)
	end
end,
--苍山路远-荒野疾驰
[14553] = function(idx)
	--[PathArea,藏月湾客栈,10013,300043]
	if idx == 0 then
		CALL("任务点移动",10013,300043)
	end
end,
--苍山路远-探子传讯
[14554] = function(idx)
	--[PathNpc,神刀堂探子,10013,93]
	if idx == 1 then
		CALL("任务点对话",10109043,"神刀堂探子",14554,1,10013,93)
	end
end,
--苍山路远-一坛美酒
[14555] = function(idx)
	--[PathEnt,酒坛,10013,40044]
	if idx == 2 then
		CALL("任务点采集","酒坛",10013,40044)
	end
end,
--苍山路远-酒逢知己
[14556] = function(idx)
	--[PathNpc,燕南飞,10013,644]
	if idx == 3 then
		CALL("任务点对话",10109133,"燕南飞",14556,3,10013,644)
	end
end,
--上苍雪岭-试探底细
[14557] = function(idx)
	--[PathMon,无影阁帮众,10013,100929]
	if idx == 0 then
		local monsters = {"无影阁帮众","无影阁暗卫","无影阁暗哨"}
		CALL("任务点杀怪",monsters,10013,100923)
	end
	--[PathMon,无影阁暗卫,10013,100938]
	if idx == 1 then
		local monsters = {"无影阁暗卫","无影阁暗哨"}
		CALL("任务点杀怪",monsters,10013,100923)
	end
	--[PathMon,无影阁暗哨,10013,100923]
	if idx == 2 then
		local monsters = {"无影阁暗哨"}
		CALL("任务点杀怪",monsters,10013,100923)
	end
end,
--上苍雪岭-杀入阁内
[14558] = function(idx)
	--[PathMon,无影阁打手,10013,1259]
	if idx == 3 then
		local monsters = {"无影阁打手","打手头领"}
		CALL("任务点杀怪",monsters,10013,339)
	end
	--[PathMon,打手头领,10013,339]
	if idx == 4 then
		local monsters = {"打手头领"}
		CALL("任务点杀怪",monsters,10013,339)
	end
end,
--上苍雪岭-无影阁主
[14559] = function(idx)
	--[PathMon,阁主 秦万,10013,113]
	if idx == 5 then
		local monsters = {"阁主 秦万"}
		CALL("任务点杀怪",monsters,10013,113)
	end
end,
--神武门内-循声而去
[14560] = function(idx)
	--[PathNpc,燕南飞,10013,643]
	if idx == 0 then
		CALL("任务点对话",10109134,"燕南飞",14560,0,10013,643)
	end
end,
--神武门内-暗杀守卫
[14561] = function(idx)
	--[PathEnt,神武门打手,10013,700]
	if idx == 1 then
		CALL("任务点采集","神武门打手",10013,700)
	end
end,
--神武门内-悄然打听
[14562] = function(idx)
	--[PathNpc,孟大,10013,222]
	if idx == 2 then
		CALL("任务点对话",10109047,"孟大",14562,2,10013,222)
	end
	--[PathNpc,孟晓,10013,223]
	if idx == 3 then
		CALL("任务点对话",10109048,"孟晓",14562,3,10013,223)
	end
end,
--神武门内-拆除机关
[14563] = function(idx)
	--[PathEnt,木弩机关,10013,40048]
	if idx == 4 then
		CALL("任务点采集","木弩机关",10013,40048)
	end
end,
--背水一战-刀手围攻
[14564] = function(idx)
	--[PathMon,玄刀阁哨子,10013,101012]
	if idx == 0 then
		local monsters = {"玄刀阁哨子","玄刀阁刀手","玄刀阁护法"}
		CALL("任务点杀怪",monsters,10013,100946)
	end
	--[PathMon,玄刀阁刀手,10013,101412]
	if idx == 1 then
		local monsters = {"玄刀阁刀手","玄刀阁护法"}
		CALL("任务点杀怪",monsters,10013,100946)
	end
	--[PathMon,玄刀阁护法,10013,100946]
	if idx == 2 then
		local monsters = {"玄刀阁护法"}
		CALL("任务点杀怪",monsters,10013,100946)
	end
end,
--背水一战-剑断人亡
[14565] = function(idx)
	--[PathMon,断剑阁帮众,10013,101016]
	if idx == 4 then
		local monsters = {"断剑阁帮众","断剑阁精锐","断剑阁长老"}
		CALL("任务点杀怪",monsters,10013,100965)
	end
	--[PathMon,断剑阁精锐,10013,101003]
	if idx == 5 then
		local monsters = {"断剑阁精锐","断剑阁长老"}
		CALL("任务点杀怪",monsters,10013,100965)
	end
	--[PathMon,断剑阁长老,10013,100965]
	if idx == 6 then
		local monsters = {"断剑阁长老"}
		CALL("任务点杀怪",monsters,10013,100965)
	end
end,
--背水一战-霸刀显威
[14566] = function(idx)
	--[PathMon,司空羽,10013,114]
	if idx == 3 then
		local monsters = {"司空羽"}
		CALL("任务点杀怪",monsters,10013,114)
	end
end,
--奇毒配方-孤魂段常
[14567] = function(idx)
	--[PathMon,孤魂 段常,10013,671]
	if idx == 0 then
		local monsters = {"孤魂 段常"}
		CALL("任务点杀怪",monsters,10013,671)
	end
end,
--奇毒配方-搜寻配方
[14568] = function(idx)
	--[PathEnt,宝箱,10013,40084]
	if idx == 1 then
		CALL("任务点采集","宝箱",10013,40084)
	end
end,
--解毒在望-医者相候
[14569] = function(idx)
	--[PathArea,苍雪岭西,10013,300916]
	if idx == 0 then
		CALL("任务点移动",10013,300916)
	end
end,
--解毒在望-妙手仁心
[14570] = function(idx)
	--[PathNpc,冷君怡,10013,649]
	if idx == 1 then
		CALL("任务功能对话",14570,10509013,"冷君怡","[PathNpc,冷君怡,10013,649]",10013,649)
	end
end,
--解毒在望-岭上奇物
[14571] = function(idx)
	--[PathNpc,千年雪参,10013,40079]
	if idx == 3 then
		CALL("任务功能对话",14571,10509012,"千年雪参","[PathNpc,千年雪参,10013,40079]",10013,40079)
	end
end,
--解毒在望-冰雪融泉
[14572] = function(idx)
	--[PathNpc,融雪池,10013,300912]
	if idx == 2 then
		LogWarning("未知的任务处理:%s","[PathNpc,融雪池,10013,300912]")
	end
end,
--大功告成-重返神刀
[14573] = function(idx)
	--[PathNpc,傅红雪,10013,91]
	if idx == 0 then
		CALL("任务点对话",10109037,"傅红雪",14573,0,10013,91)
	end
end,
--大功告成-江湖难料
[14574] = function(idx)
	--[PathNpc,燕南飞,10013,642]
	if idx == 1 then
		CALL("任务点对话",10109135,"燕南飞",14574,1,10013,642)
	end
end,
--上苍雪岭-总舵之内
[14580] = function(idx)
	--[PathArea,神武门总舵,10013,300927]
	if idx == 6 then
		CALL("任务点移动",10013,300927)
	end
end,
--神武门内-乔装打扮
[14581] = function(idx)
	--[PathEnt,僻静之处,10013,300928]
	if idx == 5 then
		LogWarning("未知的任务处理:%s","[PathEnt,僻静之处,10013,300928]")
	end
end,
--解毒在望-燕子相助
[14582] = function(idx)
	--[PathArea,平阳驿站,10013,300929]
	if idx == 4 then
		CALL("任务点移动",10013,300929)
	end
end,

--------1601,帮派建造（非随机）--------
--石敢当·修路忙-寻觅巨石
[21004] = function(idx)
	--寻觅[PathEnt,秦川巨石,10009,40311]
	if idx == 0 then
		CALL("任务点采集","秦川巨石",10009,40311)
	end
end,
--石敢当·修路忙-采集碎石
[21005] = function(idx)
	--采集碎石
	if idx == 1 then
		LogWarning("未知的任务处理:%s","采集碎石")
	end
end,
--参天木·好栋梁-
[21006] = function(idx)
	--寻觅[PathEnt,云滇良材,10006,40113]
	if idx == 0 then
		CALL("任务点采集","云滇良材",10006,40113)
	end
end,
--参天木·好栋梁-
[21007] = function(idx)
	--采集伐倒的树木
	if idx == 1 then
		LogWarning("未知的任务处理:%s","采集伐倒的树木")
	end
end,

--------2000,论剑日常--------
--竞技场战斗
[65000] = function(idx)
	--完成3场竞技场战斗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","完成3场竞技场战斗")
	end
end,
--竞技场战斗
[65001] = function(idx)
	--完成3场竞技场战斗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","完成3场竞技场战斗")
	end
end,
--竞技场战斗
[65002] = function(idx)
	--完成3场竞技场战斗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","完成3场竞技场战斗")
	end
end,
--竞技场战斗
[65003] = function(idx)
	--完成3场竞技场战斗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","完成3场竞技场战斗")
	end
end,
--竞技场战斗
[65004] = function(idx)
	--完成3场竞技场战斗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","完成3场竞技场战斗")
	end
end,

--------21,盟会日常--------
--接待访客
[601] = function(idx)
	--接待镇远镖局访客12分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待镇远镖局访客12分钟")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--击杀惯匪
[602] = function(idx)
	--击杀4名惯匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击杀4名惯匪")
	end
	--击杀7名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀7名敌盟玩家")
	end
end,
--击杀恶盗
[603] = function(idx)
	--击败9名恶盗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败9名恶盗")
	end
	--击杀7名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀7名敌盟玩家")
	end
end,
--问候杀手前辈
[604] = function(idx)
	--拜访杀手接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访杀手接引人")
	end
	--击杀7名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀7名敌盟玩家")
	end
end,
--收集盟会物资
[605] = function(idx)
	--收集6个遗落的物资
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集6个遗落的物资")
	end
	--击杀7名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀7名敌盟玩家")
	end
end,
--接待访客
[606] = function(idx)
	--接待孔雀山庄访客8分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待孔雀山庄访客8分钟")
	end
	--击败4名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击败4名敌盟玩家")
	end
end,
--击杀贼匪
[607] = function(idx)
	--击败1名贼匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败1名贼匪")
	end
	--击杀4名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀4名敌盟玩家")
	end
end,
--击杀毛贼
[608] = function(idx)
	--击败6名毛贼
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败6名毛贼")
	end
	--击败4名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击败4名敌盟玩家")
	end
end,
--问候镖师前辈
[609] = function(idx)
	--拜访镖师接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访镖师接引人")
	end
	--击杀4名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀4名敌盟玩家")
	end
end,
--收集盟会物资
[610] = function(idx)
	--收集3个装满萝卜的麻袋
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集3个装满萝卜的麻袋")
	end
	--击杀4名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀4名敌盟玩家")
	end
end,
--接待访客
[611] = function(idx)
	--接待财神商会访客9分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待财神商会访客9分钟")
	end
	--击杀5名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀5名敌盟玩家")
	end
end,
--击杀悍匪
[612] = function(idx)
	--击败2名悍匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败2名悍匪")
	end
	--击杀5名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀5名敌盟玩家")
	end
end,
--击杀小偷
[613] = function(idx)
	--击败7名小偷
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败7名小偷")
	end
	--击杀5名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀5名敌盟玩家")
	end
end,
--问候乐伶前辈
[614] = function(idx)
	--拜访乐伶接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访乐伶接引人")
	end
	--击杀5名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀5名敌盟玩家")
	end
end,
--收集盟会物资
[615] = function(idx)
	--收集4个桂林三花酒
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集4个桂林三花酒")
	end
	--击杀5名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀5名敌盟玩家")
	end
end,
--接待访客
[616] = function(idx)
	--接待君子门会访客10分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待君子门会访客10分钟")
	end
	--击杀6名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀6名敌盟玩家")
	end
end,
--击杀凶匪
[617] = function(idx)
	--击杀3名凶匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击杀3名凶匪")
	end
	--击杀6名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀6名敌盟玩家")
	end
end,
--击杀惯偷
[618] = function(idx)
	--击败8名惯偷
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败8名惯偷")
	end
	--击杀6名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀6名敌盟玩家")
	end
end,
--问候游侠前辈
[619] = function(idx)
	--拜访游侠接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访游侠接引人")
	end
	--击杀6名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀6名敌盟玩家")
	end
end,
--收集盟会物资
[620] = function(idx)
	--收集5个散落的军火
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集5个散落的军火")
	end
	--击杀6名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀6名敌盟玩家")
	end
end,
--接待访客
[621] = function(idx)
	--接待铸神谷访客11分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待铸神谷访客11分钟")
	end
	--击杀7名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀7名敌盟玩家")
	end
end,
--击杀恶匪
[622] = function(idx)
	--击杀5名恶匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击杀5名恶匪")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--击杀大盗
[623] = function(idx)
	--击败10名大盗
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败10名大盗")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--问候猎户前辈
[624] = function(idx)
	--拜访猎户接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访猎户接引人")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--收集盟会物资
[625] = function(idx)
	--收集7个散落的货物
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集7个散落的货物")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--问候捕快前辈
[626] = function(idx)
	--拜访捕快接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访捕快接引人")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--问候文士前辈
[627] = function(idx)
	--拜访文士接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访文士接引人")
	end
	--击杀8名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀8名敌盟玩家")
	end
end,
--接待访客
[628] = function(idx)
	--接待孔雀山庄访客7分钟
	if idx == 0 then
		LogWarning("未知的任务处理:%s","接待孔雀山庄访客7分钟")
	end
	--击败3名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击败3名敌盟玩家")
	end
end,
--击杀贼匪
[629] = function(idx)
	--击败1名贼匪
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败1名贼匪")
	end
	--击杀3名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀3名敌盟玩家")
	end
end,
--击杀毛贼
[630] = function(idx)
	--击败5名毛贼
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败5名毛贼")
	end
	--击败3名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击败3名敌盟玩家")
	end
end,
--问候镖师前辈
[631] = function(idx)
	--拜访镖师接引人
	if idx == 0 then
		LogWarning("未知的任务处理:%s","拜访镖师接引人")
	end
	--击杀3名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀3名敌盟玩家")
	end
end,
--收集盟会物资
[632] = function(idx)
	--收集2个装满萝卜的麻袋
	if idx == 0 then
		LogWarning("未知的任务处理:%s","收集2个装满萝卜的麻袋")
	end
	--击杀3名敌盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","击杀3名敌盟玩家")
	end
end,

--------22,盟会建造--------
--寻访大师
[42201] = function(idx)
	--和[PathEnt,开封诸葛俞,10012,1903]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104632,"开封诸葛俞",42201,0,10012,1903)
	end
end,
--寻访大师
[42202] = function(idx)
	--和[PathEnt,开封雪秋千,10012,1904]交谈
	if idx == 0 then
		CALL("任务点对话",10104633,"开封雪秋千",42202,0,10012,1904)
	end
end,
--寻访大师
[42203] = function(idx)
	--和[PathEnt,开封澜八月,10012,1905]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104634,"开封澜八月",42203,0,10012,1905)
	end
end,
--寻访大师
[42204] = function(idx)
	--与[PathEnt,开封赤名,10012,1913]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104635,"开封赤名",42204,0,10012,1913)
	end
end,
--寻访大师
[42205] = function(idx)
	--和[PathEnt,开封海岳,10012,1914]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104636,"开封海岳",42205,0,10012,1914)
	end
end,
--寻访大师
[42206] = function(idx)
	--和[PathEnt,开封风枪却,10012,1915]交谈
	if idx == 0 then
		CALL("任务点对话",10104637,"开封风枪却",42206,0,10012,1915)
	end
end,
--寻访大师
[42207] = function(idx)
	--和[PathEnt,开封三余,10012,1916]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104638,"开封三余",42207,0,10012,1916)
	end
end,
--寻访大师
[42208] = function(idx)
	--寻人：找到[PathEnt,开封梓景逸,10012,1917]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104639,"开封梓景逸",42208,0,10012,1917)
	end
end,
--寻访大师
[42209] = function(idx)
	--寻人：找到[PathEnt,开封尤焕申,10012,1918]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104640,"开封尤焕申",42209,0,10012,1918)
	end
end,
--寻访大师
[42210] = function(idx)
	--寻人：找到[PathEnt,开封一云子,10012,1919]交谈(每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点对话",10104641,"开封一云子",42210,0,10012,1919)
	end
end,

--------221,杭州徐海liz见闻条件隐藏--------
--神武门镜头用
[6660] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------23,盟会建造（非随机）--------
--采集石料-寻觅巨石
[21000] = function(idx)
	--寻觅[PathEnt,秦川巨石,10009,40311](每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点采集","秦川巨石",10009,40311)
	end
end,
--采集石料-采集巨石
[21001] = function(idx)
	--拾取石料
	if idx == 1 then
		LogWarning("未知的任务处理:%s","拾取石料")
	end
end,
--寻觅良材-寻觅良材
[21002] = function(idx)
	--寻觅[PathEnt,云滇良材,10006,40113](每天前10个任务可以获得4倍奖励，建筑经验不翻倍)
	if idx == 0 then
		CALL("任务点采集","云滇良材",10006,40113)
	end
end,
--寻觅良材-采集良材
[21003] = function(idx)
	--拾取良材
	if idx == 1 then
		LogWarning("未知的任务处理:%s","拾取良材")
	end
end,

--------421,第十六回·随风追影了无踪--------
--相约九华-九华驿站
[12630] = function(idx)
	--抵达化清寺[PathNpc,佛印台,10001,213]
	if idx == 0 then
		CALL("任务点移动",10001,213)
	end
end,
--相约九华-有僧迎客
[12631] = function(idx)
	--和[PathNpc,慧远,10001,213]交谈
	if idx == 1 then
		CALL("任务点对话",10108001,"慧远",12631,1,10001,213)
	end
end,
--相约九华-燕来小镇
[12632] = function(idx)
	--抵达对岸[Map_X_Y,渡口,10001,2393,2802]
	if idx == 2 then
		CALL("坐标点移动",2393,2802,10001)
	end
end,
--仗义除凶-意外之变
[12633] = function(idx)
	--询问[PathNpc,货郎,10001,214]
	if idx == 0 then
		CALL("任务点对话",10108009,"货郎",12633,0,10001,214)
	end
end,
--仗义除凶-仗义除凶
[12634] = function(idx)
	--[PathMon,江湖帮打手,10105,101751]
	if idx == 1 then
		local monsters = {"江湖帮打手","江湖帮武师","江湖帮高手","吴老大"}
		CALL("任务点杀怪",monsters,10105,101821)
	end
	--[PathMon,江湖帮武师,10105,101801]
	if idx == 2 then
		local monsters = {"江湖帮武师","江湖帮高手","吴老大"}
		CALL("任务点杀怪",monsters,10105,101821)
	end
	--[PathMon,江湖帮高手,10105,101796]
	if idx == 5 then
		local monsters = {"江湖帮高手","吴老大"}
		CALL("任务点杀怪",monsters,10105,101821)
	end
	--[PathMon,吴老大,10105,101821]
	if idx == 3 then
		local monsters = {"吴老大"}
		CALL("任务点杀怪",monsters,10105,101821)
	end
end,
--仗义除凶-恢复宁静
[12635] = function(idx)
	--询问[PathNpc,镇民,10001,215]
	if idx == 4 then
		CALL("任务点对话",10108003,"盘龙镇村民",12635,4,10001,215)
	end
end,
--接踵而至-化外之居
[12639] = function(idx)
	--询问[PathNpc,巩自如,10001,262]
	if idx == 0 then
		CALL("任务点对话",10108012,"巩自如",12639,0,10001,262)
	end
	--询问[PathNpc,满天和,10001,263]
	if idx == 1 then
		CALL("任务点对话",10108013,"满天和",12639,1,10001,263)
	end
end,
--接踵而至-游玩小童
[12640] = function(idx)
	--询问[Map_X_Y,花姑子,10001,1765,3020]
	if idx == 2 then
		CALL("任务点对话",10108018,"花姑子",12640,2,10001,1765,3020)
	end
end,
--接踵而至-细细打听
[12641] = function(idx)
	--聆听[Map_X_Y,花姑子,10001,1765,3020]
	if idx == 3 then
		CALL("任务点对话",10108018,"花姑子",12641,3,10001,1765,3020)
	end
end,
--回谷设法-回谷设法
[12643] = function(idx)
	--回转[Map_X_Y,芳华谷,10001,1677,3015]
	if idx == 0 then
		CALL("坐标点移动",1677,3015,10001)
	end
end,
--回谷设法-智者有方
[12644] = function(idx)
	--与[PathNpc,村长,10001,266]攀谈
	if idx == 1 then
		CALL("任务点对话",10108016,"村长",12644,1,10001,266)
	end
end,
--回谷设法-神秘访客
[12645] = function(idx)
	--与[PathNpc,白衣人,10001,273]攀谈
	if idx == 2 then
		CALL("任务点对话",10108022,"白衣人",12645,2,10001,273)
	end
end,
--回谷设法-桃花佳酿
[12646] = function(idx)
	--向[PathNpc,酒娘,10001,267]求酒
	if idx == 3 then
		CALL("任务点对话",10108017,"酒娘",12646,3,10001,267)
	end
end,
--回谷设法-夫婿未归
[12647] = function(idx)
	--救回[PathNpc,乔慧仁,10001,269]
	if idx == 4 then
		CALL("任务功能对话",12647,10501011,"乔慧仁","救回[PathNpc,乔慧仁,10001,269]",10001,269)
	end
end,
--把酒言欢-以酒会友
[12650] = function(idx)
	--和[PathNpc,白衣人,10001,273]交谈
	if idx == 0 then
		CALL("任务功能对话",12650,10501023,"白衣人","和[PathNpc,白衣人,10001,273]交谈",10001,273)
	end
end,
--把酒言欢-馈赠佳物
[12651] = function(idx)
	--查看白衣人赠物
	if idx == 1 then
		LogWarning("未知的任务处理:%s","查看白衣人赠物")
	end
end,
--回村取酒
[12649] = function(idx)
	--回村找[PathNpc,酒娘,10001,267]
	if idx == 0 then
		CALL("任务功能对话",12649,10501010,"酒娘","回村找[PathNpc,酒娘,10001,267]",10001,267)
	end
end,
--逐北而行-捷径寻踪
[12636] = function(idx)
	--赶往[Map_X_Y,芳华谷,10001,2080,2936]
	if idx == 0 then
		CALL("坐标点移动",2080,2936,10001)
	end
end,
--逐北而行-窃窃语声
[12637] = function(idx)
	--[Map_X_Y,刺探消息,10001,2023,2930]
	if idx == 1 then
		CALL("坐标点移动",2023,2930,10001)
	end
end,
--逐北而行-继续前行
[12638] = function(idx)
	--抵达[Map_X_Y,芳华谷,10001,1831,3022]
	if idx == 2 then
		CALL("坐标点移动",1831,3022,10001)
	end
end,
--为民除害-痛惩恶匪
[12716] = function(idx)
	--[PathMon,江湖帮劫匪,10001,100801]
	if idx == 0 then
		local monsters = {"江湖帮劫匪","江湖帮毛贼","江湖帮匪首"}
		CALL("任务点杀怪",monsters,10001,100801)
	end
	--[PathMon,江湖帮毛贼,10001,100801]
	if idx == 1 then
		local monsters = {"江湖帮毛贼","江湖帮匪首"}
		CALL("任务点杀怪",monsters,10001,100801)
	end
	--[PathMon,江湖帮匪首,10001,100801]
	if idx == 2 then
		local monsters = {"江湖帮匪首"}
		CALL("任务点杀怪",monsters,10001,100801)
	end
end,
--为民除害-接二连三
[12713] = function(idx)
	--[PathMon,江湖帮混混,10001,100918]
	if idx == 3 then
		local monsters = {"江湖帮混混","江湖帮打手"}
		CALL("任务点杀怪",monsters,10001,100918)
	end
	--[PathMon,江湖帮打手,10001,100918]
	if idx == 4 then
		local monsters = {"江湖帮打手"}
		CALL("任务点杀怪",monsters,10001,100918)
	end
end,
--赶赴山庄-百年老庄
[12642] = function(idx)
	--赶往[PathNpc,孔雀山庄,10001,454]
	if idx == 0 then
		CALL("任务点移动",10001,454)
	end
end,
--赶赴山庄-说明情况
[12711] = function(idx)
	--向[PathNpc,南宫玉博,10001,454]说明情况
	if idx == 1 then
		CALL("任务点对话",10108026,"南宫玉博",12711,1,10001,454)
	end
end,
--矿场除凶-清出归路
[12648] = function(idx)
	--[PathMon,矿场守卫,10001,100981]
	if idx == 0 then
		local monsters = {"矿场守卫","守卫头目","许万光"}
		CALL("任务点杀怪",monsters,10001,472)
	end
	--[PathMon,守卫头目,10001,100981]
	if idx == 1 then
		local monsters = {"守卫头目","许万光"}
		CALL("任务点杀怪",monsters,10001,472)
	end
	--[PathMon,许万光,10001,472]
	if idx == 2 then
		local monsters = {"许万光"}
		CALL("任务点杀怪",monsters,10001,472)
	end
end,
--矿场除凶-驱散监工
[12714] = function(idx)
	--[PathMon,矿场监工,10001,101034]
	if idx == 3 then
		local monsters = {"矿场监工","监工队长","监工头目"}
		CALL("任务点杀怪",monsters,10001,101034)
	end
	--[PathMon,监工队长,10001,101034]
	if idx == 4 then
		local monsters = {"监工队长","监工头目"}
		CALL("任务点杀怪",monsters,10001,101034)
	end
	--[PathMon,监工头目,10001,101034]
	if idx == 5 then
		local monsters = {"监工头目"}
		CALL("任务点杀怪",monsters,10001,101034)
	end
end,

--------422,第十七回·青龙探爪孔雀殇--------
--孔雀山庄-进入内庄
[12655] = function(idx)
	--前往[PathArea,内庄,10001,300836]
	if idx == 0 then
		CALL("任务点移动",10001,300836)
	end
end,
--孔雀山庄-飞鹞传书
[12657] = function(idx)
	--放飞信鹞
	if idx == 1 then
		LogWarning("未知的任务处理:%s","放飞信鹞")
	end
end,
--搜寻遗孤-得意酒坊
[12658] = function(idx)
	--赶往[Map_X_Y,得意坊,10001,1766,3441]
	if idx == 0 then
		CALL("坐标点移动",1766,3441,10001)
	end
end,
--搜寻遗孤-灭坊凶手
[12659] = function(idx)
	--[PathMon,狂雷双钩,10001,101091]
	if idx == 1 then
		local monsters = {"狂雷双钩","狂雷刀客","狂雷杀手"}
		CALL("任务点杀怪",monsters,10001,101091)
	end
	--[PathMon,狂雷刀客,10001,101091]
	if idx == 2 then
		local monsters = {"狂雷刀客","狂雷杀手"}
		CALL("任务点杀怪",monsters,10001,101091)
	end
	--[PathMon,狂雷杀手,10001,101091]
	if idx == 3 then
		local monsters = {"狂雷杀手"}
		CALL("任务点杀怪",monsters,10001,101091)
	end
end,
--搜寻遗孤-擒贼擒王
[12802] = function(idx)
	--[PathMon,周俊白,10001,383]
	if idx == 4 then
		local monsters = {"周俊白"}
		CALL("任务点杀怪",monsters,10001,383)
	end
end,
--门派来人
[12608] = function(idx)
	--和[PathNpc,江婉儿,10001,331]交谈
	if idx == 0 then
		CALL("任务点对话",10108038,"江婉儿",12608,0,10001,331)
	end
end,
--择地再见-传达死讯
[12668] = function(idx)
	--和[PathNpc,陶村长,10001,266]交谈
	if idx == 0 then
		CALL("任务点对话",10108016,"陶村长",12668,0,10001,266)
	end
end,
--择地再见-秋氏遗嘱
[12669] = function(idx)
	--询问[PathNpc,老叟,10001,261]来意
	if idx == 1 then
		CALL("任务功能对话",12669,10501022,"老叟","询问[PathNpc,老叟,10001,261]来意",10001,261)
	end
end,
--择地再见-阅读遗嘱
[12670] = function(idx)
	--宣读秋水清遗嘱
	if idx == 2 then
		LogWarning("未知的任务处理:%s","宣读秋水清遗嘱")
	end
end,
--择地再见-挥别乡民
[12671] = function(idx)
	--向[Map_X_Y,花姑子,10001,1765,3020]辞行
	if idx == 3 then
		CALL("任务点对话",10108018,"花姑子",12671,3,10001,1765,3020)
	end
end,
--择地再见-原地休整
[12740] = function(idx)
	--休息六十秒。
	if idx == 4 then
		CALL("等待任务完成",12740)
	end
end,
--林道有阻-林道而行
[12672] = function(idx)
	--前往[Map_X_Y,锦燕林,10001,1823,2800]
	if idx == 0 then
		CALL("坐标点移动",1823,2800,10001)
	end
end,
--林道有阻-慌乱商队
[12673] = function(idx)
	--与[PathNpc,商队二当家,10001,333]交谈
	if idx == 1 then
		CALL("任务点对话",10108048,"商队二当家",12673,1,10001,333)
	end
end,
--林道有阻-拦路抢匪
[12674] = function(idx)
	--[PathMon,江湖帮劫匪,10001,101249]
	if idx == 2 then
		local monsters = {"江湖帮劫匪","江湖帮毛贼","劫匪头子"}
		CALL("任务点杀怪",monsters,10001,1751,2659)
	end
	--[PathMon,江湖帮毛贼,10001,101254]
	if idx == 7 then
		local monsters = {"江湖帮毛贼","劫匪头子"}
		CALL("任务点杀怪",monsters,10001,1751,2659)
	end
	--[PathMon,劫匪头子,10001,101249]
	if idx == 3 then
		local monsters = {"劫匪头子"}
		CALL("任务点杀怪",monsters,10001,1751,2659)
	end
	--[Map_X_Y,商队货物,10001,1751,2659]
	if idx == 4 then
		CALL("任务功能对话",12674,10501012,"商队货物","[Map_X_Y,商队货物,10001,1751,2659]",10001,1751,2659)
	end
end,
--林道有阻-带头大哥
[12675] = function(idx)
	--[PathMon,秦宿,10001,385]
	if idx == 5 then
		local monsters = {"秦宿","长鱼尤道"}
		CALL("任务点杀怪",monsters,10001,384)
	end
	--[PathMon,长鱼尤道,10001,384]
	if idx == 6 then
		local monsters = {"长鱼尤道"}
		CALL("任务点杀怪",monsters,10001,384)
	end
end,
--偶遇故人-峰顶相见
[12677] = function(idx)
	--和[Map_X_Y,唐青枫,10001,1182,2564]交谈
	if idx == 0 then
		CALL("任务点对话",10108051,"唐青枫",12677,0,10001,1182,2564)
	end
end,
--偶遇故人-联袂下山
[12678] = function(idx)
	--赶往[Map_X_Y,嘉荫镇,10001,1355,1831]
	if idx == 1 then
		CALL("坐标点移动",1355,1831,10001)
	end
	--峰顶之试
	if idx == 2 then
		CALL("等待任务完成",12678)
	end
end,
--归还失物
[12676] = function(idx)
	--找到[PathNpc,商队大当家,10001,336]
	if idx == 0 then
		CALL("任务点对话",10108047,"商队大当家",12676,0,10001,336)
	end
end,
--柳暗花明-心事重重
[12652] = function(idx)
	--和[PathNpc,白衣人,10001,273]交谈
	if idx == 0 then
		CALL("任务点对话",10108022,"白衣人",12652,0,10001,273)
	end
end,
--柳暗花明-登高远眺
[12653] = function(idx)
	--继续和[PathNpc,白衣人,10001,273]交谈
	if idx == 1 then
		CALL("任务功能对话",12653,12108,"白衣人","继续和[PathNpc,白衣人,10001,273]交谈",10001,273)
	end
end,
--柳暗花明-入庄寻人
[12745] = function(idx)
	--和[PathNpc,公孙屠,10001,55424]交谈
	if idx == 2 then
		CALL("任务点对话",101080252,"公孙屠",12745,2,10001,55424)
	end
end,
--寻找线索
[12722] = function(idx)
	--找到[Map_X_Y,秋小清家,10001,1845,3442]
	if idx == 0 then
		CALL("坐标点移动",1845,3442,10001)
	end
end,
--回芳华谷
[12723] = function(idx)
	--返回[Map_X_Y,芳华谷,10001,1760,3047]
	if idx == 0 then
		CALL("坐标点移动",1760,3047,10001)
	end
end,

--------900301,??--------
--天外三奇-绿林劫道
[9023] = function(idx)
	--[PathNPC,绿林客,10002,2204]
	if idx == 2 then
		CALL("任务点对话",10109003,"绿林客",9023,2,10002,2204)
	end
end,
--沿路打探-茶摊小憩
[9132] = function(idx)
	--[PathNpc,慕情,10002,3103]
	if idx == 5 then
		CALL("任务点对话",10106147,"慕情",9132,5,10002,3103)
	end
end,
--天外三奇-问讯万象
[9500] = function(idx)
	--向[PathNPC,冷梅,10011,2213]打探消息
	if idx == 0 then
		CALL("任务点对话",10106008,"冷梅",9500,0,10011,2213)
	end
end,
--天外三奇-一行东越
[9501] = function(idx)
	--到达[Map_X_Y,东越,10002,449,3034]
	if idx == 1 then
		CALL("坐标点移动",449,3034,10002)
	end
end,
--倭寇肆虐-小打小闹
[9502] = function(idx)
	--[PathMon,劫道倭寇,10002,100349]
	if idx == 0 then
		local monsters = {"劫道倭寇","劫道浪客"}
		CALL("任务点杀怪",monsters,10002,100349)
	end
	--[PathMon,劫道浪客,10002,100349]
	if idx == 1 then
		local monsters = {"劫道浪客"}
		CALL("任务点杀怪",monsters,10002,100349)
	end
end,
--倭寇肆虐-恶贼嚣张
[9503] = function(idx)
	--[PathMon,倭寇精锐,10002,101207]
	if idx == 2 then
		local monsters = {"倭寇精锐","浪客精锐","倭寇小头目"}
		CALL("任务点杀怪",monsters,10002,101207)
	end
	--[PathMon,浪客精锐,10002,101207]
	if idx == 3 then
		local monsters = {"浪客精锐","倭寇小头目"}
		CALL("任务点杀怪",monsters,10002,101207)
	end
	--[PathMon,倭寇小头目,10002,101207]
	if idx == 4 then
		local monsters = {"倭寇小头目"}
		CALL("任务点杀怪",monsters,10002,101207)
	end
end,
--倭寇肆虐-初会头领
[9504] = function(idx)
	--[PathMon,竹下真一,10002,950]
	if idx == 5 then
		local monsters = {"竹下真一"}
		CALL("任务点杀怪",monsters,10002,950)
	end
end,
--寻访清永-加快脚程
[9505] = function(idx)
	--赶往[PathArea,清永坊,10002,300034]
	if idx == 0 then
		CALL("任务点移动",10002,300034)
	end
end,
--寻访清永-再遇佳人
[9506] = function(idx)
	--[PathNPC,慕情,10002,644]
	if idx == 1 then
		CALL("任务点对话",10106146,"慕情",9506,1,10002,644)
	end
end,
--江湖奇缘-问讯村长
[9507] = function(idx)
	--[PathNPC,路村长,10002,14]
	if idx == 0 then
		CALL("任务点对话",20106080,"路村长",9507,0,10002,14)
	end
end,
--江湖奇缘-村中传讯
[9508] = function(idx)
	--[PathNPC,太白弟子,10002,1816]
	if idx == 1 then
		CALL("任务点对话",10106916,"太白弟子",9508,1,10002,1816)
	end
end,
--江湖奇缘-流窜匪类
[9509] = function(idx)
	--[PathMon,武士精锐,10002,101256]
	if idx == 3 then
		local monsters = {"武士精锐","落草武士","落草忍者"}
		CALL("任务点杀怪",monsters,10002,101256)
	end
	--[PathMon,落草武士,10002,101256]
	if idx == 2 then
		local monsters = {"落草武士","落草忍者"}
		CALL("任务点杀怪",monsters,10002,101256)
	end
	--[PathMon,落草忍者,10002,101256]
	if idx == 4 then
		local monsters = {"落草忍者"}
		CALL("任务点杀怪",monsters,10002,101256)
	end
end,
--沿路打探-打听去路
[9514] = function(idx)
	--[PathNpc,茶小二,10002,105]
	if idx == 0 then
		CALL("任务点对话",10106002,"茶小二",9514,0,10002,105)
	end
end,
--沿路打探-顺水人情
[9515] = function(idx)
	--[PathEnt,化瘀草,10002,40077]
	if idx == 1 then
		CALL("任务点采集","化瘀草",10002,40077)
	end
	--[PathMon,路霸精锐,10002,100830]
	if idx == 3 then
		local monsters = {"路霸精锐","东越路霸"}
		CALL("任务点杀怪",monsters,10002,100830)
	end
	--[PathMon,东越路霸,10002,100830]
	if idx == 2 then
		local monsters = {"东越路霸"}
		CALL("任务点杀怪",monsters,10002,100830)
	end
end,
--沿路打探-沿路问讯
[9516] = function(idx)
	--问讯[PathNpc,丽姝,10002,51]
	if idx == 4 then
		CALL("任务点对话",20106069,"丽姝",9516,4,10002,51)
	end
end,
--路见不平-怒杀草寇
[9517] = function(idx)
	--[PathMon,东越草寇,10002,100738]
	if idx == 0 then
		local monsters = {"东越草寇","劫道恶匪","草寇头领"}
		CALL("任务点杀怪",monsters,10002,101284)
	end
	--[PathMon,劫道恶匪,10002,100738]
	if idx == 1 then
		local monsters = {"劫道恶匪","草寇头领"}
		CALL("任务点杀怪",monsters,10002,101284)
	end
	--[PathMon,草寇头领,10002,101284]
	if idx == 2 then
		local monsters = {"草寇头领"}
		CALL("任务点杀怪",monsters,10002,101284)
	end
end,
--路见不平-勇除头目
[9518] = function(idx)
	--[PathMon,头目龙从云,10002,95]
	if idx == 3 then
		local monsters = {"龙从云"}
		CALL("任务点杀怪",monsters,10002,95)
	end
end,
--路见不平-救人问讯
[9519] = function(idx)
	--[PathNPC,被困女子,10002,113]
	if idx == 4 then
		CALL("任务点对话",10106047,"尤芯芯",9519,4,10002,113)
	end
end,
--寻踪觅影-山间小庙
[9520] = function(idx)
	--[PathArea,老爷庙,10002,300047]
	if idx == 0 then
		CALL("任务点移动",10002,300047)
	end
end,
--寻踪觅影-忧心忡忡
[9521] = function(idx)
	--询问[PathNPC,慕情,10002,3104]
	if idx == 1 then
		CALL("任务点对话",10106148,"慕情",9521,1,10002,3104)
	end
end,
--倭寇肆虐-佳人求助
[9549] = function(idx)
	--[PathNPC,慕情,10002,536]
	if idx == 6 then
		CALL("任务点对话",10106066,"慕情",9549,6,10002,536)
	end
end,

--------423,第十八回·血衣秘境侠影忙--------
--先到一步
[12612] = function(idx)
	--和[PathNpc,江山,10001,464]交谈
	if idx == 0 then
		CALL("任务点对话",10108057,"江山",12612,0,10001,464)
	end
	--和[PathNpc,江婉儿,10001,463]交谈
	if idx == 1 then
		CALL("任务点对话",10108056,"江婉儿",12612,1,10001,463)
	end
	--和[PathNpc,韩振松,10001,465]交谈
	if idx == 2 then
		CALL("任务点对话",10108058,"韩振松",12612,2,10001,465)
	end
end,
--再遇二人-叙旧
[12731] = function(idx)
	--找到[PathNpc,燕南飞,10001,344]
	if idx == 0 then
		CALL("任务点对话",10108054,"燕南飞",12731,0,10001,344)
	end
	--找到[PathNpc,明月心,10001,342]
	if idx == 1 then
		CALL("任务点对话",10108053,"明月心",12731,1,10001,342)
	end
end,
--再遇二人-采芦
[12732] = function(idx)
	--采摘[PathNpc,芦苇,10001,40045]
	if idx == 2 then
		CALL("任务点采集","芦苇",10001,40045)
	end
end,
--再遇二人-归来
[12733] = function(idx)
	--找到[PathNpc,明月心,10001,342]
	if idx == 3 then
		CALL("任务点对话",10108053,"明月心",12733,3,10001,342)
	end
end,
--保障后方-意外之变
[12687] = function(idx)
	--询问[PathNpc,唐青枫,10001,346]
	if idx == 0 then
		CALL("任务点对话",10108063,"唐青枫",12687,0,10001,346)
	end
end,
--保障后方-刺探情报
[12691] = function(idx)
	--找回[Map_X_Y,探子,10001,1640,1805]
	if idx == 1 then
		CALL("坐标点移动",1640,1805,10001)
	end
end,
--加入战局-化解危机
[12694] = function(idx)
	--[PathMon,先锋杀手,10001,101850]
	if idx == 0 then
		local monsters = {"先锋杀手","先锋刺客","杀手高手","刺客高手"}
		CALL("任务点杀怪",monsters,10001,101457)
	end
	--[PathMon,先锋刺客,10001,101850]
	if idx == 1 then
		local monsters = {"先锋刺客","杀手高手","刺客高手"}
		CALL("任务点杀怪",monsters,10001,101457)
	end
	--[PathMon,杀手高手,10001,101457]
	if idx == 7 then
		local monsters = {"杀手高手","刺客高手"}
		CALL("任务点杀怪",monsters,10001,101457)
	end
	--[PathMon,刺客高手,10001,101457]
	if idx == 2 then
		local monsters = {"刺客高手"}
		CALL("任务点杀怪",monsters,10001,101457)
	end
end,
--加入战局-局势缓解
[12695] = function(idx)
	--找到[PathNpc,唐青枫,10001,367]
	if idx == 3 then
		CALL("任务点对话",10108071,"唐青枫",12695,3,10001,367)
	end
end,
--加入战局-救援·壹
[12696] = function(idx)
	--[PathMon,雷堂弟子,10001,101483]
	if idx == 4 then
		local monsters = {"雷堂弟子","雷堂精锐","雷堂高手"}
		CALL("任务点杀怪",monsters,10001,101483)
	end
	--[PathMon,雷堂精锐,10001,101483]
	if idx == 5 then
		local monsters = {"雷堂精锐","雷堂高手"}
		CALL("任务点杀怪",monsters,10001,101483)
	end
	--[PathMon,雷堂高手,10001,101483]
	if idx == 6 then
		local monsters = {"雷堂高手"}
		CALL("任务点杀怪",monsters,10001,101483)
	end
end,
--攻其不备-侧面击破
[12701] = function(idx)
	--[PathMon,血衣楼守卫,10001,101670]
	if idx == 0 then
		local monsters = {"血衣楼守卫","血衣楼守卫高手"}
		CALL("任务点杀怪",monsters,10001,101670)
	end
	--[PathMon,血衣楼守卫高手,10001,101670]
	if idx == 1 then
		local monsters = {"血衣楼守卫高手"}
		CALL("任务点杀怪",monsters,10001,101670)
	end
end,
--攻其不备-雷堂堂主
[12803] = function(idx)
	--[PathMon,屠越龙,10001,414]
	if idx == 2 then
		local monsters = {"屠越龙"}
		CALL("任务点杀怪",monsters,10001,414)
	end
end,
--攻其不备-不幸消息
[12702] = function(idx)
	--[PathNpc,唐青枫,10001,369]
	if idx == 3 then
		CALL("任务点对话",10108073,"唐青枫",12702,3,10001,369)
	end
end,
--攻其不备-迷魂解药
[12703] = function(idx)
	--[PathMon,鬼点头,10001,416]
	if idx == 4 then
		CALL("任务功能对话",12703,10501020,"鬼点头","[PathMon,鬼点头,10001,416]",10001,416)
	end
end,
--攻其不备-解救同道
[12704] = function(idx)
	--营救[PathNpc,正派高手,10001,375]
	if idx == 5 then
		LogWarning("未知的任务处理:%s","营救[PathNpc,正派高手,10001,375]")
	end
end,
--意外之变-暗号之声
[12705] = function(idx)
	--找到[PathNpc,江山,10001,466]
	if idx == 0 then
		CALL("任务点对话",10108076,"江山",12705,0,10001,466)
	end
end,
--意外之变-往西而去
[12706] = function(idx)
	--找到[PathNpc,唐青枫,10001,370]
	if idx == 1 then
		CALL("任务点对话",10108074,"唐青枫",12706,1,10001,370)
	end
end,
--意外之变-投石问路
[12707] = function(idx)
	--[PathMon,禁地拳师,10001,101702]
	if idx == 2 then
		local monsters = {"禁地拳师","禁地高手"}
		CALL("任务点杀怪",monsters,10001,101702)
	end
	--[PathMon,禁地高手,10001,101702]
	if idx == 3 then
		local monsters = {"禁地高手"}
		CALL("任务点杀怪",monsters,10001,101702)
	end
end,
--战事先锋-血衣探子
[12692] = function(idx)
	--[PathMon,血衣楼探子,10001,101379]
	if idx == 0 then
		local monsters = {"血衣楼探子","血衣楼刺客","探子头目"}
		CALL("任务点杀怪",monsters,10001,101379)
	end
	--[PathMon,血衣楼刺客,10001,101379]
	if idx == 1 then
		local monsters = {"血衣楼刺客","探子头目"}
		CALL("任务点杀怪",monsters,10001,101379)
	end
	--[PathMon,探子头目,10001,101379]
	if idx == 2 then
		local monsters = {"探子头目"}
		CALL("任务点杀怪",monsters,10001,101379)
	end
end,
--战事先锋-回报八荒
[12693] = function(idx)
	--和[PathNpc,唐青枫,10001,346]交谈
	if idx == 3 then
		CALL("任务点对话",10108063,"唐青枫",12693,3,10001,346)
	end
end,
--战事先锋-向北而行
[12712] = function(idx)
	--赶往[Map_X_Y,嘉荫镇后门,10001,1835,1533]
	if idx == 4 then
		CALL("坐标点移动",1835,1533,10001)
	end
end,
--阵地中枢-救援·贰
[12697] = function(idx)
	--[PathMon,炎堂弟子,10001,101519]
	if idx == 0 then
		local monsters = {"炎堂弟子","炎堂精锐","炎堂高手"}
		CALL("任务点杀怪",monsters,10001,101519)
	end
	--[PathMon,炎堂精锐,10001,101519]
	if idx == 1 then
		local monsters = {"炎堂精锐","炎堂高手"}
		CALL("任务点杀怪",monsters,10001,101519)
	end
	--[PathMon,炎堂高手,10001,101519]
	if idx == 2 then
		local monsters = {"炎堂高手"}
		CALL("任务点杀怪",monsters,10001,101519)
	end
end,
--阵地中枢-救援·叁
[12698] = function(idx)
	--[PathMon,影堂弟子,10001,101573]
	if idx == 3 then
		local monsters = {"影堂弟子","影堂精锐","影堂高手"}
		CALL("任务点杀怪",monsters,10001,101573)
	end
	--[PathMon,影堂精锐,10001,101573]
	if idx == 4 then
		local monsters = {"影堂精锐","影堂高手"}
		CALL("任务点杀怪",monsters,10001,101573)
	end
	--[PathMon,影堂高手,10001,101573]
	if idx == 5 then
		local monsters = {"影堂高手"}
		CALL("任务点杀怪",monsters,10001,101573)
	end
end,
--深入敌营-侦查归来
[12699] = function(idx)
	--找到[PathNpc,唐青枫,10001,368]
	if idx == 0 then
		CALL("任务点对话",10108072,"唐青枫",12699,0,10001,368)
	end
end,
--深入敌营-寻得捷径
[12700] = function(idx)
	--找到[Map_X_Y,秘密入口,10001,2648,1637]
	if idx == 1 then
		CALL("坐标点移动",2648,1637,10001)
	end
end,
--攻入总舵-强弱悬殊
[12708] = function(idx)
	--和[PathNpc,唐青枫,10001,370]对话
	if idx == 0 then
		CALL("任务点对话",10108074,"唐青枫",12708,0,10001,370)
	end
end,
--攻入总舵-深入总舵
[12709] = function(idx)
	--和[PathNpc,叶知秋,10001,55116]对话
	if idx == 1 then
		CALL("任务点对话",10108175,"叶知秋",12709,1,10001,55116)
	end
end,
--攻入总舵-加入战斗
[12735] = function(idx)
	--[PathMon,总舵精锐,10001,101888]
	if idx == 2 then
		local monsters = {"总舵精锐","总舵杀手","总舵死士"}
		CALL("任务点杀怪",monsters,10001,101888)
	end
	--[PathMon,总舵杀手,10001,101888]
	if idx == 3 then
		local monsters = {"总舵杀手","总舵死士"}
		CALL("任务点杀怪",monsters,10001,101888)
	end
	--[PathMon,总舵死士,10001,101888]
	if idx == 4 then
		local monsters = {"总舵死士"}
		CALL("任务点杀怪",monsters,10001,101888)
	end
end,
--攻入总舵-稍作休憩
[12736] = function(idx)
	--和[PathNpc,叶知秋,10001,55116]对话
	if idx == 5 then
		CALL("任务点对话",10108175,"叶知秋",12736,5,10001,55116)
	end
end,
--攻入总舵-总舵内部
[12710] = function(idx)
	--找到[PathNpc,唐青枫,10001,300734]
	if idx == 6 then
		CALL("任务功能对话",12710,12116,"唐青枫","找到[PathNpc,唐青枫,10001,300734]",10001,300734)
	end
end,
--江湖唏嘘-返回镇上
[12737] = function(idx)
	--返回[PathNpc,嘉荫镇,10001,300835]
	if idx == 0 then
		CALL("任务点移动",10001,300835)
	end
end,
--江湖唏嘘-故人相遇
[12689] = function(idx)
	--和[PathNpc,傅红雪,10001,300834]辞别
	if idx == 1 then
		CALL("任务功能对话",12689,12114,"傅红雪","和[PathNpc,傅红雪,10001,300834]辞别",10001,300834)
	end
end,
--江湖唏嘘-四盟相聚
[12739] = function(idx)
	--和[PathNpc,叶知秋,10001,55155]
	if idx == 2 then
		CALL("任务点对话",10108181,"叶知秋",12739,2,10001,55155)
	end
	--和[PathNpc,唐青枫,10001,55156]
	if idx == 3 then
		CALL("任务点对话",10108182,"唐青枫",12739,3,10001,55156)
	end
	--和[PathNpc,离玉堂,10001,55157]
	if idx == 4 then
		CALL("任务点对话",10108183,"离玉堂",12739,4,10001,55157)
	end
	--和[PathNpc,曲无忆,10001,55158]
	if idx == 5 then
		CALL("任务点对话",10108184,"曲无忆",12739,5,10001,55158)
	end
end,
--精进武艺
[12729] = function(idx)
	--等级到达60
	if idx == 0 then
		CALL("等级限制",12729)
	end
end,
--界限突破肆
[12730] = function(idx)
	--等待突破：九华叠翠(60级)
	if idx == 0 then
		CALL("界限突破",12730)
	end
end,
--九华叠翠
[12750] = function(idx)
	--寻[PathNpc,车夫,10001,947]前往[PathNpc,古陶驿站,10013,2]
	if idx == 0 then
		CALL("任务点移动",10013,2)
	end
end,

--------25,盟会日常--------
--镇守盟会
[674] = function(idx)
	--镇守 [PathNpc,护卫,mapId,instId] 12分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","镇守 [PathNpc,护卫,mapId,instId] 12分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[675] = function(idx)
	--保护 [PathNpc,左锐进,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,左锐进,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[676] = function(idx)
	--保护 [PathNpc,充阳平,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,充阳平,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[677] = function(idx)
	--保护 [PathNpc,葛敏才,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,葛敏才,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[678] = function(idx)
	--保护 [PathNpc,边博雅,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,边博雅,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[679] = function(idx)
	--保护 [PathNpc,荀沭,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,荀沭,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[680] = function(idx)
	--保护 [PathNpc,孙松,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,孙松,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[681] = function(idx)
	--保护 [PathNpc,于高,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,于高,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[682] = function(idx)
	--保护 [PathNpc,向群,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,向群,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[683] = function(idx)
	--保护 [PathNpc,蔺南,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,蔺南,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--保护访客
[684] = function(idx)
	--保护 [PathNpc,张平,mapId,instId] 8分钟[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","保护 [PathNpc,张平,mapId,instId] 8分钟[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找护卫
[685] = function(idx)
	--找到巡逻的万高明[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到巡逻的万高明[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找护卫
[686] = function(idx)
	--找到巡逻的薄和泰[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到巡逻的薄和泰[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找护卫
[687] = function(idx)
	--找到巡逻的贺阳朔[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到巡逻的贺阳朔[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--收集物资
[688] = function(idx)
	--获取军备物资[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","获取军备物资[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[689] = function(idx)
	--击败敌盟 [PathMon,左锐进,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,左锐进,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[690] = function(idx)
	--击败敌盟 [PathMon,充阳平,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,充阳平,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[691] = function(idx)
	--击败敌盟 [PathMon,葛敏才,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,葛敏才,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[692] = function(idx)
	--击败敌盟 [PathMon,边博雅,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,边博雅,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[693] = function(idx)
	--击败敌盟 [PathMon,荀沭,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,荀沭,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[694] = function(idx)
	--击败敌盟 [PathMon,孙松,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,孙松,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[695] = function(idx)
	--击败敌盟 [PathMon,于高,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,于高,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[696] = function(idx)
	--击败敌盟 [PathMon,向群,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,向群,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[697] = function(idx)
	--击败敌盟 [PathMon,蔺南,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,蔺南,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--击败访客
[698] = function(idx)
	--击败敌盟 [PathMon,张平,mapId,instId][ENEMY]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","击败敌盟 [PathMon,张平,mapId,instId][ENEMY]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找情报探子
[699] = function(idx)
	--找到闵奉[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到闵奉[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找情报探子
[700] = function(idx)
	--找到廖宁[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到廖宁[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,
--寻找情报探子
[701] = function(idx)
	--找到慕极[FRIEND]
	if idx == 0 then
		LogWarning("未知的任务处理:%s","找到慕极[FRIEND]")
	end
	--或 击败15名 非本盟玩家
	if idx == 1 then
		LogWarning("未知的任务处理:%s","或 击败15名 非本盟玩家")
	end
end,

--------900302,??--------
--不负相知-白鹭洲头
[9522] = function(idx)
	--[PathArea,白鹭洲渡口,10002,300164]
	if idx == 7 then
		CALL("任务点移动",10002,300164)
	end
end,
--不负相知-一腔怒火
[9523] = function(idx)
	--[PathMon,渡口守卫,10002,101316]
	if idx == 0 then
		local monsters = {"渡口守卫","渡口女忍"}
		CALL("任务点杀怪",monsters,10002,101316)
	end
	--[PathMon,渡口女忍,10002,101316]
	if idx == 6 then
		local monsters = {"渡口女忍"}
		CALL("任务点杀怪",monsters,10002,101316)
	end
end,
--不负相知-天风流寇
[9524] = function(idx)
	--[PathMon,哨探队长,10002,101324]
	if idx == 4 then
		local monsters = {"哨探队长","哨探精锐","天风流哨探"}
		CALL("任务点杀怪",monsters,10002,101324)
	end
	--[PathMon,哨探精锐,10002,101324]
	if idx == 3 then
		local monsters = {"哨探精锐","天风流哨探"}
		CALL("任务点杀怪",monsters,10002,101324)
	end
	--[PathMon,天风流哨探,10002,101324]
	if idx == 1 then
		local monsters = {"天风流哨探"}
		CALL("任务点杀怪",monsters,10002,101324)
	end
end,
--不负相知-鬼斩上忍
[9525] = function(idx)
	--[PathMon,鬼斩上忍,10002,1235]
	if idx == 2 then
		local monsters = {"鬼斩上忍"}
		CALL("任务点杀怪",monsters,10002,1235)
	end
end,
--不负相知-问讯樵夫
[9526] = function(idx)
	--[PathNPC,樵夫,10002,1073]
	if idx == 5 then
		CALL("任务点对话",10106036,"樵夫",9526,5,10002,1073)
	end
end,
--初探桃源-世外胜境
[9527] = function(idx)
	--[PathArea,桃源胜境,10002,300679]
	if idx == 6 then
		CALL("任务点移动",10002,300679)
	end
end,
--初探桃源-大打出手
[9528] = function(idx)
	--[PathMon,劫匪队长,11111,101664]
	if idx == 1 then
		local monsters = {"劫匪队长","天风流巡兵","天风流劫匪"}
		CALL("任务点杀怪",monsters,11111,101664)
	end
	--[PathMon,天风流巡兵,11111,101664]
	if idx == 0 then
		local monsters = {"天风流巡兵","天风流劫匪"}
		CALL("任务点杀怪",monsters,11111,101664)
	end
	--[PathMon,天风流劫匪,111112,101664]
	if idx == 4 then
		local monsters = {"天风流劫匪"}
		CALL("任务点杀怪",monsters,111112,101664)
	end
end,
--初探桃源-源源不绝
[9529] = function(idx)
	--[PathMon,武士队长,11111,101692]
	if idx == 3 then
		local monsters = {"武士队长","下等武士","中等武士"}
		CALL("任务点杀怪",monsters,11111,101692)
	end
	--[PathMon,下等武士,11111,101692]
	if idx == 5 then
		local monsters = {"下等武士","中等武士"}
		CALL("任务点杀怪",monsters,11111,101692)
	end
	--[PathMon,天风流武士,11111,101692]
	if idx == 2 then
		local monsters = {"中等武士"}
		CALL("任务点杀怪",monsters,11111,101692)
	end
end,
--素手摘星-后续事宜
[9530] = function(idx)
	--[PathNPC,曲无忆,10002,2284]
	if idx == 3 then
		CALL("任务点对话",10106149,"曲无忆",9530,3,10002,2284)
	end
end,
--素手摘星-轻功过湖
[9531] = function(idx)
	--到达[PathArea,湖心岛,10002,300059]
	if idx == 0 then
		CALL("任务点移动",10002,300059)
	end
end,
--素手摘星-倪氏七少
[9532] = function(idx)
	--[PathMon,倪江,10002,1154]
	if idx == 1 then
		local monsters = {"倪江"}
		CALL("任务点杀怪",monsters,10002,1154)
	end
end,
--素手摘星-岛上磁石
[9533] = function(idx)
	--[PathEnt,磁石,10002,40066]
	if idx == 2 then
		CALL("任务点采集","磁石",10002,40066)
	end
end,
--素手摘星-渔村会合
[9534] = function(idx)
	--[PathNPC,曲无忆,10002,2286]
	if idx == 4 then
		CALL("任务点对话",10106150,"曲无忆",9534,4,10002,2286)
	end
end,
--藏珍轶闻-待时以动
[9535] = function(idx)
	--[PathNPC,曲无忆,10002,2286]
	if idx == 0 then
		CALL("任务点对话",10106150,"曲无忆",9535,0,10002,2286)
	end
end,
--藏珍轶闻-倪氏七杰
[9536] = function(idx)
	--[PathNPC,慕情,10002,2287]
	if idx == 1 then
		CALL("任务点对话",10106143,"慕情",9536,1,10002,2287)
	end
end,
--初探倪庄-元武别院
[9537] = function(idx)
	--赶往[Map_X_Y,倪庄,10002,1915,2038]
	if idx == 6 then
		CALL("坐标点移动",1915,2038,10002)
	end
end,
--初探倪庄-恶仆阻路
[9538] = function(idx)
	--[PathMon,护院教头,10002,101404]
	if idx == 2 then
		local monsters = {"护院教头","倪庄护院","倪庄恶仆"}
		CALL("任务点杀怪",monsters,10002,101404)
	end
	--[PathMon,倪庄护院,10002,101404]
	if idx == 0 then
		local monsters = {"倪庄护院","倪庄恶仆"}
		CALL("任务点杀怪",monsters,10002,101404)
	end
	--[PathMon,倪庄恶仆,10002,101404]
	if idx == 1 then
		local monsters = {"倪庄恶仆"}
		CALL("任务点杀怪",monsters,10002,101404)
	end
end,
--初探倪庄-长驱直入
[9539] = function(idx)
	--[PathMon,倪庄剑客,10002,101397]
	if idx == 4 then
		local monsters = {"倪庄剑客","倪庄侍卫","倪庄贤士"}
		CALL("任务点杀怪",monsters,10002,101397)
	end
	--[PathMon,倪庄侍卫,10002,101397]
	if idx == 3 then
		local monsters = {"倪庄侍卫","倪庄贤士"}
		CALL("任务点杀怪",monsters,10002,101397)
	end
	--[PathMon,倪庄贤士,10002,101397]
	if idx == 5 then
		local monsters = {"倪庄贤士"}
		CALL("任务点杀怪",monsters,10002,101397)
	end
end,
--另有发现-传音入密
[9541] = function(idx)
	--[Map_X_Y,乌金汊,10002,1921,2091]
	if idx == 0 then
		CALL("坐标点移动",1921,2091,10002)
	end
end,
--另有发现-从长计议
[9542] = function(idx)
	--[PathNPC,慕情,10002,2287]
	if idx == 1 then
		CALL("任务点对话",10106143,"慕情",9542,1,10002,2287)
	end
end,
--另有发现-倪氏幕僚
[9543] = function(idx)
	--[PathNPC,白丹青,10002,690]
	if idx == 2 then
		CALL("任务点对话",10106024,"白丹青",9543,2,10002,690)
	end
end,

--------900901,??--------
--返回师门-蜀地唐门
[27002] = function(idx)
	--[PathNpc,唐慧,10005,1]
	if idx == 0 then
		CALL("任务点对话",10116001,"唐慧",27002,0,10005,1)
	end
end,
--傀儡化形-轻而易举
[27006] = function(idx)
	--[PathNpc,唐笑,10005,16]
	if idx == 0 then
		CALL("任务点对话",10116022,"唐笑",27006,0,10005,16)
	end
end,
--傀儡化形-工欲其事
[27007] = function(idx)
	--[PathNpc,唐云,10005,2]
	if idx == 1 then
		CALL("任务点对话",10116003,"唐云",27007,1,10005,2)
	end
end,
--花雨飞星-唐门绝技
[27016] = function(idx)
	--[PathNpc,唐花雨,10005,4]
	if idx == 0 then
		CALL("任务点对话",10116005,"唐花雨",27016,0,10005,4)
	end
end,
--花雨飞星-观山悟心
[27017] = function(idx)
	--去[PathNpc,后面台阶,10005,300007]
	if idx == 1 then
		CALL("任务点移动",10005,300007)
	end
end,
--内外之争-追魂别离
[27026] = function(idx)
	--[PathNpc,唐离,10005,54]
	if idx == 3 then
		CALL("任务点对话",10116021,"唐离",27026,3,10005,54)
	end
	--[PathNpc,唐别,10005,53]
	if idx == 2 then
		CALL("任务点对话",10116020,"唐别",27026,2,10005,53)
	end
end,
--内外之争-唐门嫡传
[27027] = function(idx)
	--[PathNpc,唐青青,10005,47]
	if idx == 4 then
		CALL("任务点对话",10116051,"唐青青",27027,4,10005,47)
	end
end,
--内外之争-心存怨恨
[27028] = function(idx)
	--[PathNpc,万仙儿,10005,388]
	if idx == 5 then
		CALL("任务点对话",10116101,"万仙儿",27028,5,10005,388)
	end
end,
--内外之争-如诗如雨
[27029] = function(idx)
	--[PathNpc,唐诗雨,10005,69]
	if idx == 6 then
		CALL("任务点对话",10116063,"唐诗雨",27029,6,10005,69)
	end
end,
--傀儡新术-端倪难辨
[27031] = function(idx)
	--[PathNpc,广场那人好像就是唐倪,10005,12]
	if idx == 1 then
		CALL("任务点对话",10116008,"广场那人好像就是唐倪",27031,1,10005,12)
	end
end,
--傀儡新术-遍寻端倪
[27032] = function(idx)
	--[PathNpc,唐倪,10005,8]
	if idx == 0 then
		CALL("任务点对话",10116006,"唐倪",27032,0,10005,8)
	end
end,
--内外之争-蜀地唐门
[27036] = function(idx)
	--返回[PathNpc,议事堂,10005,300001]
	if idx == 1 then
		CALL("任务点移动",10005,300001)
	end
end,
--内外之争-唐门老太
[27037] = function(idx)
	--[PathNpc,唐老太,10005,5]
	if idx == 0 then
		CALL("任务点对话",10116002,"唐老太",27037,0,10005,5)
	end
end,
--暗流涌动-心有余悸
[27041] = function(idx)
	--问问[PathNpc,唐翔,10005,264]
	if idx == 1 then
		CALL("任务点对话",10116082,"唐翔",27041,1,10005,264)
	end
end,
--暗流涌动-唐翔其人
[27042] = function(idx)
	--问问[PathNpc,唐倪,10005,352]
	if idx == 0 then
		CALL("任务点对话",10116100,"唐倪",27042,0,10005,352)
	end
end,
--心有灵犀-就地打坐
[27170] = function(idx)
	--打坐一分钟
	if idx == 0 then
		CALL("等待任务完成",27170)
	end
end,
--心有灵犀-蜀地唐门
[27171] = function(idx)
	--[PathNpc,唐文山,10005,64]
	if idx == 1 then
		CALL("任务功能对话",27171,15014,"唐文山","[PathNpc,唐文山,10005,64]",10005,64)
	end
end,

--------900902,??--------
--一线参天-扑朔迷离
[27056] = function(idx)
	--去找[PathNpc,万仙儿,10005,300003]
	if idx == 1 then
		CALL("任务点移动",10005,300003)
	end
end,
--一线参天-危机初现
[27057] = function(idx)
	--[PathNpc,唐青影,10005,13]
	if idx == 0 then
		CALL("任务点对话",10116017,"唐青影",27057,0,10005,13)
	end
end,
--一线参天-绝境险地
[27058] = function(idx)
	--[PathMon,熟铜傀儡,10005,100160]
	if idx == 2 then
		local monsters = {"熟铜傀儡","鬼公偶","神秘傀儡师"}
		CALL("任务点杀怪",monsters,10005,100141)
	end
	--[PathMon,鬼公偶,10005,100154]
	if idx == 3 then
		local monsters = {"鬼公偶","神秘傀儡师"}
		CALL("任务点杀怪",monsters,10005,100141)
	end
	--[PathMon,神秘傀儡师,10005,100141]
	if idx == 4 then
		local monsters = {"神秘傀儡师"}
		CALL("任务点杀怪",monsters,10005,100141)
	end
end,
--一线参天-寻声而上
[27059] = function(idx)
	--[PathNpc,傀儡室,10005,300002]
	if idx == 6 then
		CALL("任务点移动",10005,300002)
	end
end,
--追魂索命-风云突变
[27061] = function(idx)
	--[PathMon,净琉璃偶,10005,100063]
	if idx == 0 then
		local monsters = {"净琉璃偶","攻玉房叛徒"}
		CALL("任务点杀怪",monsters,10005,100173)
	end
	--[PathMon,攻玉房叛徒,10005,100173]
	if idx == 1 then
		local monsters = {"攻玉房叛徒"}
		CALL("任务点杀怪",monsters,10005,100173)
	end
end,
--追魂索命-风起云涌
[27062] = function(idx)
	--[PathMon,钢爪傀儡,10005,100257]
	if idx == 2 then
		local monsters = {"钢爪傀儡","攻玉房高手"}
		CALL("任务点杀怪",monsters,10005,100264)
	end
	--[PathMon,攻玉房高手,10005,100264]
	if idx == 3 then
		local monsters = {"攻玉房高手"}
		CALL("任务点杀怪",monsters,10005,100264)
	end
end,
--一线参天-再遇唐翔
[27063] = function(idx)
	--[PathNpc,唐翔,10005,14]
	if idx == 5 then
		CALL("任务点对话",10116018,"唐翔",27063,5,10005,14)
	end
end,
--追魂索命-偃师房主
[27066] = function(idx)
	--[PathNpc,唐倪,10005,28]
	if idx == 4 then
		CALL("任务点对话",10116028,"唐倪",27066,4,10005,28)
	end
end,
--火速报信-穷追不舍
[27071] = function(idx)
	--返回[PathArea,傀儡室,10005,300004]
	if idx == 1 then
		CALL("任务点移动",10005,300004)
	end
end,
--火速报信-紧急讯息
[27072] = function(idx)
	--向[PathNpc,老太太,10005,413]汇报
	if idx == 0 then
		CALL("任务点对话",10116108,"老太太",27072,0,10005,413)
	end
end,
--危机四伏-唐门内助
[27076] = function(idx)
	--门主[PathNpc,唐太岳,10005,418]
	if idx == 0 then
		CALL("任务点对话",10116113,"唐太岳",27076,0,10005,418)
	end
	--攻玉房主[PathNpc,唐秀石,10005,414]
	if idx == 1 then
		CALL("任务点对话",10116109,"唐秀石",27076,1,10005,414)
	end
	--描金房主[PathNpc,韩宛如,10005,415]
	if idx == 2 then
		CALL("任务点对话",10116110,"韩宛如",27076,2,10005,415)
	end
end,
--危机四伏-天一之主
[27077] = function(idx)
	--天一房主[PathNpc,唐雅,10005,417]
	if idx == 3 then
		CALL("任务点对话",10116112,"唐雅",27077,3,10005,417)
	end
end,
--危机四伏-辣手追魂
[27078] = function(idx)
	--追魂房主[PathNpc,唐青容,10005,416]
	if idx == 4 then
		CALL("任务点对话",10116111,"唐青容",27078,4,10005,416)
	end
end,
--固守后山-追寻端倪
[27086] = function(idx)
	--与碎星楼前[PathNpc,唐端,10005,24]对话
	if idx == 0 then
		CALL("任务点对话",10116027,"唐端",27086,0,10005,24)
	end
end,
--穷追不舍-扑朔迷离
[27101] = function(idx)
	--[PathNpc,唐端,10005,24]
	if idx == 0 then
		CALL("任务点对话",10116027,"唐端",27101,0,10005,24)
	end
end,
--穷追不舍-寻踪觅迹
[27102] = function(idx)
	--[PathNpc,追踪黑衣人,10005,300039]
	if idx == 1 then
		CALL("任务点移动",10005,300039)
	end
end,
--穷追不舍-三遇唐翔
[27103] = function(idx)
	--[PathNPCLayer,,唐翔,10005,345,1]
	if idx == 2 then
		--桥上?
		CALL("任务点对话",10116083,"唐翔",27103,2,10005,345,1)
	end
end,

--------900305,??--------
--绝代杀手-身份凭证
[10138] = function(idx)
	--[PathNPC,三绝无命 杜枫,10010,10000]
	if idx == 0 then
		CALL("任务点对话",10205113,"杜枫",10138,0,10010,10000)
	end
end,
--绝代杀手-身份玩法
[10139] = function(idx)
	--[PathNPC,三绝无命 杜枫,10010,10000]
	if idx == 1 then
		CALL("任务点对话",10205113,"杜枫",10139,1,10010,10000)
	end
end,
--诗书乾坤-身份凭证
[10140] = function(idx)
	--[PathNPC,妙笔文士 尹书衡,10010,13674]
	if idx == 0 then
		CALL("任务点对话",10205119,"尹书衡",10140,0,10010,13674)
	end
end,
--诗书乾坤-身份玩法
[10141] = function(idx)
	--[PathNPC,妙笔文士 尹书衡,10010,13674]
	if idx == 1 then
		CALL("任务点对话",10205119,"尹书衡",10141,1,10010,13674)
	end
end,
--一代名捕-身份凭证
[10142] = function(idx)
	--[PathNPC,京城名捕 云中燕,10010,10002]
	if idx == 0 then
		CALL("任务点对话",10205118,"云中燕",10142,0,10010,10002)
	end
end,
--一代名捕-身份玩法
[10143] = function(idx)
	--[PathNPC,京城名捕 云中燕,10010,10002]
	if idx == 1 then
		CALL("任务点对话",10205118,"云中燕",10143,1,10010,10002)
	end
end,
--乐闻知音-身份凭证
[10310] = function(idx)
	--[PathNPC,绝代红伶 舒音,10010,9998]
	if idx == 0 then
		CALL("任务点对话",10205109,"舒音",10310,0,10010,9998)
	end
end,
--乐闻知音-身份玩法
[10311] = function(idx)
	--[PathNPC,绝代红伶 舒音,10010,9998]
	if idx == 1 then
		CALL("任务点对话",10205109,"舒音",10311,1,10010,9998)
	end
end,
--狩猎山林-身份凭证
[10312] = function(idx)
	--[PathNPC,百步穿杨 苗胜天,10010,10001]
	if idx == 0 then
		CALL("任务点对话",10205116,"苗胜天",10312,0,10010,10001)
	end
end,
--狩猎山林-身份玩法
[10313] = function(idx)
	--[PathNPC,百步穿杨 苗胜天,10010,10001]
	if idx == 1 then
		CALL("任务点对话",10205116,"苗胜天",10313,1,10010,10001)
	end
end,
--一诺千金-身份凭证
[10314] = function(idx)
	--[PathNPC,一镖千金 万全,10010,9995]
	if idx == 0 then
		CALL("任务点对话",10205105,"万全 ",10314,0,10010,9995)
	end
end,
--一诺千金-身份玩法
[10315] = function(idx)
	--[PathNPC,一镖千金 万全,10010,9995]
	if idx == 1 then
		CALL("任务点对话",10205105,"万全 ",10315,1,10010,9995)
	end
end,
--独行游侠-身份凭证
[10316] = function(idx)
	--[PathNPC,独行千里 迟英,10010,13676]
	if idx == 0 then
		CALL("任务点对话",10205112,"迟英",10316,0,10010,13676)
	end
end,
--独行游侠-身份玩法
[10317] = function(idx)
	--[PathNPC,独行千里 迟英,10010,13676]
	if idx == 1 then
		CALL("任务点对话",10205112,"迟英",10317,1,10010,13676)
	end
end,

--------900903,??--------
--青龙之头-龙纹青衣
[27127] = function(idx)
	--[PathMon,青龙头目,10005,401]
	if idx == 0 then
		local monsters = {"青龙头目"}
		CALL("任务点杀怪",monsters,10005,401)
	end
end,
--尘埃落定-镜花水月
[27141] = function(idx)
	--与[PathNpc,唐翔,10005,78]对话
	if idx == 3 then
		CALL("任务点对话",10116007,"唐翔",27141,3,10005,78)
	end
end,
--尘埃落定-终有始末
[27142] = function(idx)
	--飞出[PathNpc,碎星楼,10005,78]，找[PathNpc,唐倪,10005,21]汇报
	if idx == 1 then
		CALL("任务点对话",10116033,"唐倪",27142,1,10005,21)
	end
end,
--尘埃落定-飞跃对岸
[27143] = function(idx)
	--请[PathNpc,唐倪,10005,21]帮忙
	if idx == 0 then
		CALL("任务点移动",10005,21)
	end
end,
--尘埃落定-叛徒下场
[27144] = function(idx)
	--找[PathNpc,唐端,10005,25]询问
	if idx == 2 then
		CALL("任务点对话",10116034,"唐端",27144,2,10005,25)
	end
end,

--------900306,??--------
--险象环生-闲云野鹤
[9111] = function(idx)
	--[PathArea,野鹤湫,10002,300206]
	if idx == 5 then
		CALL("任务点移动",10002,300206)
	end
end,
--险象环生-青龙帮凶
[9112] = function(idx)
	--[PathMon,万象门打手,10002,101032]
	if idx == 0 then
		local monsters = {"万象门打手","万象门帮众"}
		CALL("任务点杀怪",monsters,10002,101032)
	end
	--[PathMon,万象门帮众,10002,101032]
	if idx == 6 then
		local monsters = {"万象门帮众"}
		CALL("任务点杀怪",monsters,10002,101032)
	end
end,
--险象环生-恶从胆生
[9113] = function(idx)
	--[PathMon,万象门刀手,10002,101043]
	if idx == 1 then
		local monsters = {"万象门刀手","万象门哨子","万象门精锐"}
		CALL("任务点杀怪",monsters,10002,101043)
	end
	--[PathMon,万象门哨子,10002,101043]
	if idx == 2 then
		local monsters = {"万象门哨子","万象门精锐"}
		CALL("任务点杀怪",monsters,10002,101043)
	end
	--[PathMon,万象门精锐,10002,101043]
	if idx == 3 then
		local monsters = {"万象门精锐"}
		CALL("任务点杀怪",monsters,10002,101043)
	end
end,
--险象环生-铁笔书生
[9114] = function(idx)
	--[PathMon,廖明,10002,1898]
	if idx == 4 then
		local monsters = {"廖明"}
		CALL("任务点杀怪",monsters,10002,1898)
	end
end,
--风波再起-一路向东
[9115] = function(idx)
	--[PathArea,取径山隘,10002,300189]
	if idx == 0 then
		CALL("任务点移动",10002,300189)
	end
end,
--风波再起-路人闲话
[9116] = function(idx)
	--[PathNpc,江湖闲话,10002,1899]
	if idx == 1 then
		CALL("任务点对话",10106082,"莽汉邓彪",9116,1,10002,1899)
	end
end,
--辗转宁海-官官相护
[9117] = function(idx)
	--[PathNpc,宁海城守,10002,1904]
	if idx == 0 then
		CALL("任务点对话",10106091,"宁海城守",9117,0,10002,1904)
	end
end,
--辗转宁海-四下打探
[9118] = function(idx)
	--[PathNpc,香客,10002,1902]
	if idx == 1 then
		CALL("任务点对话",10106084,"香客",9118,1,10002,1902)
	end
end,
--辗转宁海-酒色捕头
[9119] = function(idx)
	--[PathNpc,小九,10002,2369]
	if idx == 2 then
		CALL("任务点对话",10106157,"小九",9119,2,10002,2369)
	end
end,
--稍有眉目-醉酒捕快
[9120] = function(idx)
	--[PathNpc,捕头张宽,10002,1907]
	if idx == 0 then
		CALL("任务点对话",10106094,"捕快张宽",9120,0,10002,1907)
	end
end,
--稍有眉目-顺手牵羊
[9121] = function(idx)
	--[PathNpc,搜身,10002,1907]
	if idx == 1 then
		CALL("任务点采集","腰牌",10002,1907)
	end
end,
--稍有眉目-乔装打扮
[9122] = function(idx)
	--[PathNpc,严泽屋外,10002,300687]
	if idx == 2 then
		CALL("任务点使用物品","捕快服",10002,300687)
	end
end,
--随机应变-鱼目混珠
[9123] = function(idx)
	--[PathNpc,严泽,10002,1908]
	if idx == 0 then
		CALL("任务点对话",10106095,"严泽",9123,0,10002,1908)
	end
end,
--随机应变-故友重逢
[9124] = function(idx)
	--[PathNpc,慕情,10002,2294]
	if idx == 1 then
		CALL("任务点对话",10106144,"慕情",9124,1,10002,2294)
	end
end,
--事况有变-互通消息
[9125] = function(idx)
	--[PathNpc,曲无忆,10002,2346]
	if idx == 0 then
		CALL("任务点对话",10106159,"曲无忆",9125,0,10002,2346)
	end
end,
--事况有变-返回客栈
[9126] = function(idx)
	--[PathNpc,客栈,10002,300685]
	if idx == 1 then
		CALL("任务点移动",10002,300685)
	end
end,
--事况有变-意外生变
[9127] = function(idx)
	--[PathNpc,店小二,10002,2366]
	if idx == 2 then
		CALL("任务点对话",10106160,"店小二",9127,2,10002,2366)
	end
end,
--事况有变-商议后续
[9128] = function(idx)
	--[PathNpc,曲无忆,10002,1925]
	if idx == 3 then
		CALL("任务点对话",10106129,"曲无忆",9128,3,10002,1925)
	end
end,
--长乐海岸-前程未卜
[9129] = function(idx)
	--[PathArea,长乐海滩,10002,300684]
	if idx == 0 then
		CALL("任务点移动",10002,300684)
	end
end,
--长乐海岸-寒江引路
[9130] = function(idx)
	--[PathNpc,寒江城探子,10002,3101]
	if idx == 1 then
		CALL("任务点对话",10106170,"寒江城探子",9130,1,10002,3101)
	end
end,
--万象门内-故人指点
[9133] = function(idx)
	--[PathNpc,笑道人,10002,2367]
	if idx == 5 then
		CALL("任务点对话",10106154,"笑道人",9133,5,10002,2367)
	end
end,
--万象门内-兵家所长
[9134] = function(idx)
	--[PathMon,谷梁远,10002,2137]
	if idx == 0 then
		local monsters = {"兵象 谷梁远"}
		CALL("任务点杀怪",monsters,10002,2137)
	end
end,
--万象门内-搜寻监牢
[9135] = function(idx)
	--[PathArea,搜寻监牢,10002,300902]
	if idx == 1 then
		CALL("任务点移动",10002,300902)
	end
end,
--万象门内-深入其内
[9136] = function(idx)
	--[PathNPCLayer,子阳甫,10002,2993,1]
	if idx == 2 then
		local monsters = {"宝象 子阳甫"}
		CALL("任务点杀怪",monsters,10002,2993,1)
	end
end,
--万象门内-高傲书生
[9137] = function(idx)
	--[PathMon,程修涵,10002,2139]
	if idx == 3 then
		local monsters = {"书象 程修涵"}
		CALL("任务点杀怪",monsters,10002,2139)
	end
end,
--世事无常-一无所获
[9138] = function(idx)
	--[PathArea,离开九天阁,10002,300686]
	if idx == 0 then
		CALL("任务点移动",10002,300686)
	end
end,
--世事无常-海岸相聚
[9139] = function(idx)
	--[PathNpc,曲无忆,10002,2122]
	if idx == 1 then
		CALL("任务点对话",10106152,"曲无忆",9139,1,10002,2122)
	end
end,
--世事无常-姐妹情深
[9140] = function(idx)
	--[PathNpc,曲无忆,10002,2122]
	if idx == 2 then
		CALL("任务功能对话",9140,16009,"曲无忆","[PathNpc,曲无忆,10002,2122]",10002,2122)
	end
end,
--万象门内-搜寻外围
[9142] = function(idx)
	--[PathArea,搜寻东北面区域,10002,300905]
	if idx == 4 then
		CALL("任务点移动",10002,300905)
	end
end,
--万象门内-循史问典
[10308] = function(idx)
	--[PathNpc,典象 魏铭史,10002,3098]
	if idx == 6 then
		CALL("任务点对话",20106263,"典象 魏铭史",10308,6,10002,3098)
	end
end,

--------41,昭昭日下雨迢迢--------
--羿·思月
[1201] = function(idx)
	--得到埋藏中的曲谱
	if idx == 0 then
		LogWarning("未知的任务处理:%s","得到埋藏中的曲谱")
	end
end,
--无痕剑谱1
[1202] = function(idx)
	--打败隐居在镜湖的南宫玉博
	if idx == 0 then
		LogWarning("未知的任务处理:%s","打败隐居在镜湖的南宫玉博")
	end
end,
--无痕剑谱2
[1203] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--奇遇·洞若观火
[1204] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--奇遇·金玉良缘
[1205] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--奇遇·学艺不精
[1206] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--门派试练
[1207] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--丐帮归元技能
[1208] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--杭州低级门派试练
[1209] = function(idx)
	--等级25级
	if idx == 0 then
		CALL("等级限制",1209)
	end
end,
--None
[1210] = function(idx)
end,

--------241,第七回：齐心协力夺铸神--------
--连环破神-初来江南
[7202] = function(idx)
	--找[PathNpc,李红渠,10011,861]询问
	if idx == 0 then
		CALL("任务点对话",10105525,"李红渠",7202,0,10011,861)
	end
end,
--连环破神-打探敌情
[7203] = function(idx)
	--找[PathNpc,庐北川,10011,872]
	if idx == 1 then
		CALL("任务点对话",10105530,"卢北川",7203,1,10011,872)
	end
end,
--连环破神-惊魂稍定
[7204] = function(idx)
	--询问[PathNpc,铸神谷管家,10011,150]
	if idx == 2 then
		CALL("任务点对话",10105001,"管家",7204,2,10011,150)
	end
end,
--寻踪觅迹-风铃古道
[7206] = function(idx)
	--[PathArea,风铃道,10011,300030]
	if idx == 0 then
		CALL("任务点移动",10011,300030)
	end
end,
--寻踪觅迹-银铃玉佩
[7207] = function(idx)
	--找[PathEnt,线索,10011,40001]
	if idx == 1 then
		CALL("任务点采集","玉佩",10011,40001)
	end
end,
--飞雪寻梅
[7210] = function(idx)
	--赶往[PathArea,飞雪滩,10011,300031]
	if idx == 0 then
		CALL("任务点移动",10011,300031)
	end
end,
--势不容缓-盘龙七
[7216] = function(idx)
	--找草药[PathEnt,盘龙七,10011,40002]
	if idx == 1 then
		CALL("任务点采集","盘龙七",10011,40002)
	end
end,
--势不容缓-齐落梅
[7217] = function(idx)
	--找[PathNpc,齐落梅,10011,186]
	if idx == 0 then
		CALL("任务点对话",10105003,"齐落梅",7217,0,10011,186)
	end
end,
--折之横渡-横渡大江
[7221] = function(idx)
	--与[PathNpc,齐落梅,10011,186]对话
	if idx == 0 then
		CALL("任务点对话",10105003,"齐落梅",7221,0,10011,186)
	end
end,
--折之横渡-翻江踏浪
[7222] = function(idx)
	--渡江潜入[PathNpc,枫桥镇,10011,300228]
	if idx == 2 then
		CALL("任务点移动",10011,300228)
	end
end,
--折之横渡-偶遇怪人
[7223] = function(idx)
	--渡江后找[PathNpc,说话的人,10011,873]
	if idx == 1 then
		CALL("任务点对话",10105070,"唐二",7223,1,10011,873)
	end
end,
--叶落枫桥
[7225] = function(idx)
	--找[PathNpc,唐二,10011,873]
	if idx == 0 then
		CALL("任务功能对话",7225,19003,"唐二","找[PathNpc,唐二,10011,873]",10011,873)
	end
end,
--同仇敌忾-齐中原
[7231] = function(idx)
	--找[PathNpc,齐中原,10011,362]
	if idx == 0 then
		CALL("任务点对话",10105015,"齐中原",7231,0,10011,362)
	end
end,
--同仇敌忾-斜云小径
[7232] = function(idx)
	--穿过[PathArea,斜云小径,10011,300229]前往铸神谷
	if idx == 4 then
		CALL("任务点移动",10011,300229)
	end
end,
--同仇敌忾-铸神弟子
[7233] = function(idx)
	--找[PathNpc,铸神弟子,10011,1057]
	if idx == 1 then
		CALL("任务点对话",10105555,"铸神谷弟子",7233,1,10011,1057)
	end
end,
--同仇敌忾-铸神弟子
[7249] = function(idx)
	--找[PathNpc,铸神弟子,10011,1058]
	if idx == 5 then
		CALL("任务点对话",10105556,"铸神谷弟子",7249,5,10011,1058)
	end
end,
--同仇敌忾-齐瑶
[7234] = function(idx)
	--找[PathNpc,齐瑶,10011,1039]
	if idx == 2 then
		CALL("任务点对话",10104545,"齐瑶",7234,2,10011,1039)
	end
end,
--同仇敌忾-寻迹
[7235] = function(idx)
	--[PathArea,寻找钟怡,10011,300226]
	if idx == 3 then
		CALL("任务点移动",10011,300226)
	end
end,
--铸神重生
[7236] = function(idx)
	--和[PathNpc,唐二,10011,464]一起进入铸神谷
	if idx == 0 then
		CALL("任务功能对话",7236,19002,"唐二","和[PathNpc,唐二,10011,464]一起进入铸神谷",10011,464)
	end
end,
--碧落云飞-来龙去脉
[7242] = function(idx)
	--与[PathNpc,叶知秋,10011,333]对话
	if idx == 0 then
		CALL("坐标点功能对话","上官小仙",1010501901,0,0,0,0,0,0,1466,2819,10011,200)
		CALL("NPC对话","叶知秋",1010500706,0,0,0,0,0,0)
	end
end,
--碧落云飞-飞渡
[7243] = function(idx)
	--前往[PathArea,龙井茶园,10011,300231]
	if idx == 1 then
		CALL("等待任务完成",7243)
	end
end,
--飞鹤冲天
[7241] = function(idx)
	--请[PathNpc,上官小仙,10011,958]助你上塔
	if idx == 0 then
		CALL("任务点对话",10105019,"上官小仙",7241,0,10011,958)
	end
end,

--------42,天香笺之金风玉露--------
--齐聚开封
[1301] = function(idx)
	--等级达到30级
	if idx == 0 then
		CALL("等级限制",1301)
	end
	--和[PathNpc,赵月芳,10012,1776]交谈
	if idx == 1 then
		CALL("任务点对话",10101112,"赵月芳",1301,1,10012,1776)
	end
end,
--空白信笺
[1302] = function(idx)
	--获得空白天香笺
	if idx == 0 then
		LogWarning("未知的任务处理:%s","获得空白天香笺")
	end
end,
--永结同心
[1303] = function(idx)
	--写满祝福的天香笺
	if idx == 0 then
		LogWarning("未知的任务处理:%s","写满祝福的天香笺")
	end
end,
--有情线索
[1304] = function(idx)
	--和[PathNpc,宇文竹,10012,1777]交谈
	if idx == 0 then
		CALL("任务点对话",10101113,"宇文竹",1304,0,10012,1777)
	end
end,

--------43,天香笺之有情线索--------
--携手天涯-寻访周婷
[1320] = function(idx)
	--和[PathNpc,周婷,10013,92]交谈
	if idx == 0 then
		CALL("任务点对话",10109046,"周婷",1320,0,10013,92)
	end
end,
--携手天涯-我为君歌
[1321] = function(idx)
	--和[PathNpc,周婷,10013,92]交谈
	if idx == 1 then
		CALL("任务功能对话",1321,12122,"周婷","和[PathNpc,周婷,10013,92]交谈",10013,92)
	end
end,
--携手天涯-君心我心
[1322] = function(idx)
	--和[PathNpc,周婷,10013,92]交谈
	if idx == 2 then
		CALL("任务点对话",10109046,"周婷",1322,2,10013,92)
	end
end,
--白首不离-前往东越
[1323] = function(idx)
	--和[PathNpc,楚天璇,10002,1761]交谈
	if idx == 0 then
		CALL("任务点对话",10106022,"楚天璇",1323,0,10002,1761)
	end
end,
--白首不离-出海归来
[1324] = function(idx)
	--和[PathNpc,子桑不寿,10002,1762]交谈
	if idx == 1 then
		CALL("任务点对话",10106023,"子桑不寿",1324,1,10002,1762)
	end
end,
--白首不离-海外奇物
[1325] = function(idx)
	--找到邬雪
	if idx == 3 then
		LogWarning("未知的任务处理:%s","找到邬雪")
	end
end,
--白首不离-君心我心
[1348] = function(idx)
	--和[PathNpc,楚天璇,10002,1761]交谈
	if idx == 2 then
		CALL("任务点对话",10106022,"楚天璇",1348,2,10002,1761)
	end
end,
--欢喜冤家-开封寻人
[1326] = function(idx)
	--和[PathNpc,笑道人,10012,10]交谈
	if idx == 0 then
		CALL("任务点对话",10112003,"笑道人",1326,0,10012,10)
	end
end,
--欢喜冤家-静以待之
[1327] = function(idx)
	--等待笑道人想词
	if idx == 1 then
		CALL("等待任务完成",1327)
	end
end,
--欢喜冤家-君心我心
[1328] = function(idx)
	--和[PathNpc,笑道人,10012,10]交谈
	if idx == 2 then
		CALL("任务点对话",10112003,"笑道人",1328,2,10012,10)
	end
end,
--生死相随-前往杭州
[1329] = function(idx)
	--和[PathNpc,韩莹莹,10010,11686]交谈
	if idx == 0 then
		CALL("任务点对话",10107048,"韩莹莹",1329,0,10010,11686)
	end
end,
--生死相随-武试美人
[1330] = function(idx)
	--战胜[PathNpc,韩莹莹,10010,11686]
	if idx == 1 then
		CALL("任务功能对话",1330,12123,"韩莹莹","战胜[PathNpc,韩莹莹,10010,11686]",10010,11686)
	end
end,
--生死相随-君心我心
[1331] = function(idx)
	--和[PathNpc,韩莹莹,10010,11686]交谈
	if idx == 2 then
		CALL("任务点对话",10107048,"韩莹莹",1331,2,10010,11686)
	end
end,
--早生贵子-杭州寻人
[1332] = function(idx)
	--和[PathNpc,王喜金,10010,9827]交谈
	if idx == 0 then
		CALL("任务点对话",10101116,"王喜金",1332,0,10010,9827)
	end
end,
--早生贵子-笔墨纸砚
[1333] = function(idx)
	--乌丸墨
	if idx == 1 then
		LogWarning("未知的任务处理:%s","乌丸墨")
	end
end,
--早生贵子-君心我心
[1334] = function(idx)
	--和[PathNpc,王喜金,10010,9827]交谈
	if idx == 2 then
		CALL("任务点对话",10101116,"王喜金",1334,2,10010,9827)
	end
end,
--执子之手-开封寻人
[1335] = function(idx)
	--和[PathNpc,雅奴二十七,10012,952]交谈
	if idx == 0 then
		CALL("任务点对话",10112264,"雅奴二十七",1335,0,10012,952)
	end
end,
--执子之手-采花贼人
[1336] = function(idx)
	--[PathMon,采花贼,10012,1791]
	if idx == 1 then
		local monsters = {"采花贼"}
		CALL("任务点杀怪",monsters,10012,1791)
	end
end,
--执子之手-君心我心
[1337] = function(idx)
	--和[PathNpc,雅奴二十七,10012,952]交谈
	if idx == 2 then
		CALL("任务点对话",10112264,"雅奴二十七",1337,2,10012,952)
	end
end,
--国色天香-近在眼前
[1338] = function(idx)
	--和[PathNpc,赵月芳,10012,1776]交谈
	if idx == 0 then
		CALL("任务点对话",10101112,"赵月芳",1338,0,10012,1776)
	end
end,
--国色天香-远在开封
[1339] = function(idx)
	--和[PathNpc,杨延玉,10012,9]交谈
	if idx == 1 then
		CALL("任务点对话",10112004,"杨延玉",1339,1,10012,9)
	end
end,
--国色天香-我为卿狂
[1340] = function(idx)
	--采摘[PathEnt,帝女花,10012,40020]
	if idx == 3 then
		CALL("任务点采集","帝女花",10012,40020)
	end
end,
--国色天香-君心我心
[1347] = function(idx)
	--和[PathNpc,赵月芳,10012,1776]交谈
	if idx == 2 then
		CALL("任务点对话",10101112,"赵月芳",1347,2,10012,1776)
	end
end,

--------900121,??--------
--直出浮云-行若无山
[3902] = function(idx)
	--[PathArea,回山,10009,300942]
	if idx == 1 then
		CALL("任务点移动",10009,300942)
	end
end,
--直出浮云-师兄相迎
[3917] = function(idx)
	--与[PathNpc,苏北辰,10009,3]交谈
	if idx == 0 then
		CALL("任务点对话",10103028,"苏北辰",3917,0,10009,3)
	end
end,
--祭拜剑碑-祭拜剑碑
[3919] = function(idx)
	--[Map_X_Y,阅读碑文,10009,2767,3263]
	if idx == 0 then
		CALL("坐标点移动",2767,3263,10009)
	end
end,
--驱逐出境-恶寇女贼
[3927] = function(idx)
	--[PathMon,擎雷营恶寇,10009,100593]
	if idx == 0 then
		local monsters = {"擎雷营恶寇","擎雷营女贼"}
		CALL("任务点杀怪",monsters,10009,100593)
	end
	--[PathMon,擎雷营女贼,10009,100593]
	if idx == 1 then
		local monsters = {"擎雷营女贼"}
		CALL("任务点杀怪",monsters,10009,100593)
	end
end,
--驱逐出境-一网打尽
[3928] = function(idx)
	--[PathMon,擎雷营杆首,10009,100615]
	if idx == 4 then
		local monsters = {"擎雷营杆首","擎雷营护卫","擎雷营帮众"}
		CALL("任务点杀怪",monsters,10009,100615)
	end
	--[PathMon,擎雷营护卫,10009,100615]
	if idx == 3 then
		local monsters = {"擎雷营护卫","擎雷营帮众"}
		CALL("任务点杀怪",monsters,10009,100615)
	end
	--[PathMon,擎雷营帮众,10009,100615]
	if idx == 5 then
		local monsters = {"擎雷营帮众"}
		CALL("任务点杀怪",monsters,10009,100615)
	end
end,
--驱逐出境-守口如瓶
[3933] = function(idx)
	--与[PathNpc,太白守门弟子,10009,24]交谈
	if idx == 2 then
		CALL("任务点对话",10103035,"太白守门弟子",3933,2,10009,24)
	end
end,
--有因有果-回报于青
[3939] = function(idx)
	--跟[PathNpc,于青,10009,11]交谈
	if idx == 1 then
		CALL("任务点对话",10103003,"于青",3939,1,10009,11)
	end
end,
--有因有果-青龙往事
[3940] = function(idx)
	--与[PathNpc,风无痕,10009,21]交谈
	if idx == 0 then
		CALL("任务点对话",10103002,"风无痕",3940,0,10009,21)
	end
end,
--祭拜剑碑-师兄所在
[3946] = function(idx)
	--找[PathNpc,穆云笙,10009,380]询问公孙剑下落
	if idx == 1 then
		CALL("任务点对话",10103061,"穆云笙",3946,1,10009,380)
	end
end,
--有因有果-风云际会
[3947] = function(idx)
	--与[PathNpc,风无痕,10009,21]交谈
	if idx == 2 then
		CALL("任务点对话",10103002,"风无痕",3947,2,10009,21)
	end
end,
--灵犀一动-心有灵犀
[3949] = function(idx)
	--在灵犀状态下打坐一分钟
	if idx == 0 then
		CALL("等待任务完成",3949)
	end
end,
--灵犀一动-即刻开始
[3951] = function(idx)
	--与[PathNpc,于青,10009,11]交谈
	if idx == 1 then
		CALL("任务点对话",10103003,"于青",3951,1,10009,11)
	end
end,
--回到太白-归山寒暄
[3952] = function(idx)
	--与[PathNpc,许长生,10009,1]交谈
	if idx == 0 then
		CALL("任务点对话",10103001,"许长生",3952,0,10009,1)
	end
end,
--回到太白-贼人目的
[3953] = function(idx)
	--与[PathNpc,追风营俘虏,10009,2162]交谈
	if idx == 1 then
		CALL("任务点对话",10103078,"追风营俘虏",3953,1,10009,2162)
	end
end,
--回到太白-守门师妹
[3954] = function(idx)
	--与[PathNpc,楚飞,10009,2163]交谈
	if idx == 2 then
		CALL("任务点对话",10103079,"楚飞",3954,2,10009,2163)
	end
end,

--------44,天香笺之悄无声息--------
--相见恨晚-铸神谷主
[1341] = function(idx)
	--和[PathNpc,齐落竹,10011,1627]交谈
	if idx == 0 then
		CALL("任务点对话",10105165,"齐落竹",1341,0,10011,1627)
	end
end,
--相见恨晚-等待片刻
[1342] = function(idx)
	--等待齐落竹
	if idx == 1 then
		CALL("等待任务完成",1342)
	end
end,
--相见恨晚-君心何在
[1343] = function(idx)
	--和[PathNpc,齐落竹,10011,1627]交谈
	if idx == 2 then
		CALL("任务点对话",10105165,"齐落竹",1343,2,10011,1627)
	end
end,
--江天一色
[1318] = function(idx)
	--找[PathNpc,明月心,10001,927]交谈
	if idx == 0 then
		CALL("任务点对话",10101010,"明月心",1318,0,10001,927)
	end
end,
--白纸无痕-寻找佳人
[1344] = function(idx)
	--找[PathNpc,白云轩,10002,2929]交谈
	if idx == 0 then
		CALL("任务点对话",10119068,"白云轩",1344,0,10002,2929)
	end
end,
--白纸无痕-才思敏捷
[1345] = function(idx)
	--等待白云轩写完
	if idx == 1 then
		CALL("等待任务完成",1345)
	end
end,
--白纸无痕-君心我心
[1346] = function(idx)
	--找[PathNpc,白云轩,10002,2929]交谈
	if idx == 2 then
		CALL("任务点对话",10119068,"白云轩",1346,2,10002,2929)
	end
end,

--------900122,??--------
--巡山转告-提醒楚鸿
[3905] = function(idx)
	--驻山弟子[PathNpc,楚鸿,10009,26]
	if idx == 0 then
		CALL("任务点对话",10103019,"楚鸿",3905,0,10009,26)
	end
end,
--巡山转告-提醒齐川
[3906] = function(idx)
	--驻山弟子[PathNpc,齐川,10009,27]
	if idx == 1 then
		CALL("任务点对话",10103020,"齐川",3906,1,10009,27)
	end
end,
--巡山转告-提醒许广
[3907] = function(idx)
	--驻山弟子[PathNpc,许广,10009,28]
	if idx == 2 then
		CALL("任务点对话",10103036,"许广",3907,2,10009,28)
	end
end,
--循山而上-不速之客
[3909] = function(idx)
	--[PathMon,山道神秘人,10009,100626]
	if idx == 0 then
		local monsters = {"山道神秘人","神秘镖客","神秘高手"}
		CALL("任务点杀怪",monsters,10009,100626)
	end
	--[PathMon,神秘镖客,10009,100626]
	if idx == 3 then
		local monsters = {"神秘镖客","神秘高手"}
		CALL("任务点杀怪",monsters,10009,100626)
	end
	--[PathMon,神秘高手,10009,100626]
	if idx == 4 then
		local monsters = {"神秘高手"}
		CALL("任务点杀怪",monsters,10009,100626)
	end
end,
--何去何从-非同小可
[3912] = function(idx)
	--与[PathNpc,唐林,10009,347]交谈
	if idx == 1 then
		CALL("任务点对话",10103051,"唐林",3912,1,10009,347)
	end
end,
--凌霄险道-追上凌霄
[3914] = function(idx)
	--赶赴[PathArea,凌霄道,10009,300057]追踪盗剑人
	if idx == 0 then
		CALL("任务点移动",10009,300057)
	end
end,
--循山而上-必经之路
[3921] = function(idx)
	--到[Map_X_Y,丁字路口,10009,3379,3559]查探
	if idx == 1 then
		CALL("坐标点移动",3379,3559,10009)
	end
end,
--深入探究-事实真相
[3922] = function(idx)
	--回报[PathNpc,五爷,10009,78]
	if idx == 0 then
		CALL("任务点对话",10103006,"五爷",3922,0,10009,78)
	end
end,
--巡山转告-提醒刘云
[3929] = function(idx)
	--驻山弟子[PathNpc,刘云,10009,300018]
	if idx == 3 then
		CALL("任务点移动",10009,300018)
	end
end,
--倏忽之变-速查探
[3930] = function(idx)
	--[PathArea,上山查探,10009,300019]
	if idx == 0 then
		CALL("任务点移动",10009,300019)
	end
end,
--倏忽之变-急问询
[3931] = function(idx)
	--与[PathNpc,独孤若虚,10009,30]交谈
	if idx == 1 then
		CALL("任务点对话",10103004,"独孤若虚",3931,1,10009,30)
	end
end,
--循山而上-多加防范
[3932] = function(idx)
	--爬上巨石，与[PathNpc,王青丘,10009,62]交谈
	if idx == 2 then
		CALL("任务点对话",10103014,"王青丘",3932,2,10009,62)
	end
end,
--凌霄险道-重伤弟子
[3934] = function(idx)
	--与[PathNpc,重伤的巡山弟子,10009,334]交谈
	if idx == 1 then
		CALL("任务点对话",10103048,"重伤的巡山弟子",3934,1,10009,334)
	end
end,
--凌霄险道-伤者所指
[3935] = function(idx)
	--[PathArea,继续追查,10009,300060]
	if idx == 4 then
		CALL("任务点移动",10009,300060)
	end
end,
--深入探究-是敌是友
[3938] = function(idx)
	--听[PathNpc,车昊顺,10009,167]诉说
	if idx == 1 then
		CALL("任务点对话",10103077,"车昊顺",3938,1,10009,167)
	end
end,
--倏忽之变-暗观察
[3941] = function(idx)
	--[PathArea,查探前方情况,10009,300058]
	if idx == 2 then
		CALL("任务点移动",10009,300058)
	end
end,
--何去何从-路口寻迹
[3944] = function(idx)
	--询问[PathNpc,王青丘,10009,1044]
	if idx == 0 then
		CALL("任务点对话",10103076,"王青丘",3944,0,10009,1044)
	end
end,
--凌霄险道-接踵而来
[3945] = function(idx)
	--与[PathNpc,易山,10009,90]交谈
	if idx == 3 then
		CALL("任务点对话",10103009,"易山",3945,3,10009,90)
	end
end,
--凌霄险道-急事相告
[3948] = function(idx)
	--与[PathNpc,公孙剑,10009,323]交谈
	if idx == 2 then
		CALL("任务点对话",10103007,"公孙剑",3948,2,10009,323)
	end
end,

--------45,中秋·明月天涯--------
--中秋·明月天涯
[1350] = function(idx)
	--和[PathNpc,明月心,10012,1776]交谈
	if idx == 0 then
		CALL("任务点对话",10101119,"明月心",1350,0,10012,1776)
	end
end,

--------900123,??--------
--知己知彼-询问楚鸿
[3915] = function(idx)
	--询问[PathNpc,楚鸿,10009,26]
	if idx == 0 then
		CALL("任务点对话",10103019,"楚鸿",3915,0,10009,26)
	end
end,
--知己知彼-询问石傅心
[3916] = function(idx)
	--询问[PathNpc,石傅心,10009,209]
	if idx == 1 then
		CALL("任务点对话",10103040,"石傅心",3916,1,10009,209)
	end
end,
--飞掠下山-一臂之力
[3936] = function(idx)
	--与[PathNpc,公孙剑,10009,323]交谈
	if idx == 1 then
	end
end,
--飞掠下山-急相告
[3937] = function(idx)
	--速下山寻掌门[PathNpc,风无痕,10009,21]
	if idx == 0 then
		CALL("任务点对话",10103002,"风无痕",3937,0,10009,21)
	end
end,
--武以修身-御风神行
[3943] = function(idx)
	--与[PathNpc,风无痕,10009,21]交谈
	if idx == 0 then
		CALL("任务点对话",10103002,"风无痕",3943,0,10009,21)
	end
end,

--------46,门派试练--------
--于百石
[1361] = function(idx)
end,
--黄陶朱
[1362] = function(idx)
end,
--白邓通
[1363] = function(idx)
end,
--铁眼
[1364] = function(idx)
end,
--齐落梅
[1365] = function(idx)
end,
--沐摇光
[1366] = function(idx)
end,
--段飞鹏
[1367] = function(idx)
end,
--霍天鹏
[1368] = function(idx)
end,
--孟长风
[1369] = function(idx)
end,
--冷不凡
[1370] = function(idx)
end,
--白丹青
[1371] = function(idx)
end,
--严泽
[1372] = function(idx)
end,
--慕容锦
[1373] = function(idx)
end,
--子桑不寿
[1374] = function(idx)
end,
--何盼理
[1375] = function(idx)
end,
--杨尚砚
[1376] = function(idx)
end,
--柳永
[1377] = function(idx)
end,
--雅奴二十六
[1378] = function(idx)
end,
--黄元文
[1379] = function(idx)
end,
--扫地僧
[1380] = function(idx)
end,
--满天和
[1381] = function(idx)
end,
--秋宿犁
[1382] = function(idx)
end,
--南宫玉博
[1383] = function(idx)
end,
--陶孟翟
[1384] = function(idx)
end,
--端木横
[1385] = function(idx)
end,
--秦飞虹
[1386] = function(idx)
end,
--鸿鹄子
[1387] = function(idx)
end,
--慧远禅师
[1388] = function(idx)
end,
--郑天石
[1389] = function(idx)
end,
--庞三刀
[1390] = function(idx)
end,
--杜十七
[1391] = function(idx)
end,
--度厄尊者
[1392] = function(idx)
end,
--杜伯
[1393] = function(idx)
end,
--石守信
[1394] = function(idx)
end,
--范宽
[1395] = function(idx)
end,
--冷皓轩
[1396] = function(idx)
end,
--花无心
[1397] = function(idx)
end,
--陆十八
[1398] = function(idx)
end,
--陆山川
[1399] = function(idx)
end,
--武莫风
[1400] = function(idx)
end,
--笑道人
[1401] = function(idx)
end,
--吴振英
[1402] = function(idx)
end,
--邓心禅
[1403] = function(idx)
end,
--塔夫大师
[1404] = function(idx)
end,
--杨延玉
[1405] = function(idx)
end,
--温景梵
[1406] = function(idx)
end,
--钟舒文
[1407] = function(idx)
end,
--公孙剑
[1408] = function(idx)
end,
--何荐华
[1409] = function(idx)
end,
--独孤若虚
[1410] = function(idx)
end,

--------1241,第一回：千山绝迹影踪灭--------
--回到五毒
[36001] = function(idx)
	--去[PathArea,五毒山门,10006,300424]
	if idx == 0 then
		CALL("任务点移动",10006,300424)
	end
end,
--杜微慎防
[36002] = function(idx)
	--与[PathNpc,章之滢,10006,1048]交谈
	if idx == 0 then
		CALL("任务功能对话",36002,18109,"章之滢","与[PathNpc,章之滢,10006,1048]交谈",10006,1048)
	end
end,
--药毒双生-古道西风
[36501] = function(idx)
	--[PathNpcLayer,四遗堂,10006,300425,1]
	if idx == 0 then
		CALL("坐标点移动",300425,1,10006)
	end
end,
--药毒双生-意外插曲
[36502] = function(idx)
	--[PathNpc,漆雕小五,10006,1061]
	if idx == 1 then
		CALL("任务点对话",10121010,"漆雕小五",36502,1,10006,1061)
	end
end,
--药毒双生-欣然相助
[36503] = function(idx)
	--[PathNpc,向阿莎,10006,1426]
	if idx == 2 then
		CALL("任务点对话",10121061,"向阿莎",36503,2,10006,1426)
	end
end,
--刀似弯月
[36004] = function(idx)
	--[PathNpc,韩沙影,10006,1146]
	if idx == 0 then
		CALL("任务点对话",10121040,"韩沙影",36004,0,10006,1146)
	end
end,
--银缀繁星
[36005] = function(idx)
	--[PathNpc,黎三彩,10006,1147]
	if idx == 0 then
		CALL("任务点对话",10121041,"黎三彩",36005,0,10006,1147)
	end
end,
--若舞青烟
[36006] = function(idx)
	--[PathNpc,布努,10006,1349]
	if idx == 0 then
		CALL("任务点对话",10121039,"布努",36006,0,10006,1349)
	end
end,
--四遗风骨
[36007] = function(idx)
	--[PathArea,四遗堂,10006,300426]
	if idx == 0 then
		CALL("任务点移动",10006,300426)
	end
end,
--蜜与辛螫-青龙再现
[36504] = function(idx)
	--[PathNpc,方玉峰,10006,1050]
	if idx == 0 then
		CALL("任务点对话",10121000,"方玉峰",36504,0,10006,1050)
	end
end,
--蜜与辛螫-风雨际会
[36505] = function(idx)
	--[PathNpc,方玉峰,10006,1050]
	if idx == 1 then
		CALL("任务点对话",10121000,"方玉峰",36505,1,10006,1050)
	end
end,
--蜜与辛螫-小心翼翼
[36506] = function(idx)
	--[PathArea,巫月坛,10006,300479]
	if idx == 2 then
		CALL("任务点移动",10006,300479)
	end
end,
--蜜与辛螫-立地销魂
[36507] = function(idx)
	--[PathNpc,卯今生,10006,1185]
	if idx == 3 then
		CALL("任务点对话",10121031,"卯今生",36507,3,10006,1185)
	end
end,
--灵犀一动-心有灵犀
[36508] = function(idx)
	--打坐修行
	if idx == 0 then
		CALL("等待任务完成",36508)
	end
end,
--灵犀一动-谢过长老
[36509] = function(idx)
	--[PathNpc,卯今生,10006,1185]
	if idx == 1 then
		CALL("任务点对话",10121031,"卯今生",36509,1,10006,1185)
	end
end,
--巫月圣坛
[36010] = function(idx)
	--[PathArea,巫月坛,10006,300432]
	if idx == 0 then
		CALL("任务点移动",10006,300432)
	end
end,

--------246,第八回：龙井园天圣运筹--------
--天圣初现-阻路小贼
[7352] = function(idx)
	--击败[PathMon,沧海坞无赖,10011,100897]
	if idx == 0 then
		local monsters = {"沧海坞无赖","沧海坞地痞","沧海坞教头"}
		CALL("任务点杀怪",monsters,10011,100897)
	end
	--击败[PathMon,沧海坞地痞,10011,100897]
	if idx == 1 then
		local monsters = {"沧海坞地痞","沧海坞教头"}
		CALL("任务点杀怪",monsters,10011,100897)
	end
	--击败[PathMon,沧海坞教头,10011,100897]
	if idx == 3 then
		local monsters = {"沧海坞教头"}
		CALL("任务点杀怪",monsters,10011,100897)
	end
end,
--天圣初现-隔空传音
[7254] = function(idx)
	--[PathNpc,呼唤之人,10011,311]
	if idx == 2 then
		CALL("任务点对话",10105008,"天圣 皇甫星",7254,2,10011,311)
	end
end,
--龙井飘香-举手之劳
[7358] = function(idx)
	--击败[PathMon,沧海坞打手头目,10011,100921]
	if idx == 2 then
		local monsters = {"沧海坞打手头目","沧海坞恶霸","沧海坞打手"}
		CALL("任务点杀怪",monsters,10011,100921)
	end
	--击败[PathMon,沧海坞恶霸,10011,100921]
	if idx == 0 then
		local monsters = {"沧海坞恶霸","沧海坞打手"}
		CALL("任务点杀怪",monsters,10011,100921)
	end
	--击败[PathMon,沧海坞打手,10011,100921]
	if idx == 1 then
		local monsters = {"沧海坞打手"}
		CALL("任务点杀怪",monsters,10011,100921)
	end
end,
--龙井飘香-迂回包抄
[7359] = function(idx)
	--去[PathArea,松江分舵外围,10011,300042]
	if idx == 3 then
		CALL("任务点移动",10011,300042)
	end
end,
--摇光化玉-沐摇光
[7367] = function(idx)
	--找[PathNpc,沐摇光,10011,449]询问
	if idx == 0 then
		CALL("任务点对话",10105009,"沐摇光",7367,0,10011,449)
	end
end,
--摇光化玉-松江分舵
[7368] = function(idx)
	--进入[PathArea,松江分舵,10011,300093]
	if idx == 1 then
		CALL("任务点移动",10011,300093)
	end
end,
--摇光化玉-突破舵口
[7369] = function(idx)
	--击败[PathMon,松江分舵巡察,10011,100946]
	if idx == 2 then
		local monsters = {"松江分舵巡察","松江分舵精锐"}
		CALL("任务点杀怪",monsters,10011,100945)
	end
	--击败[PathMon,松江分舵精锐,10011,100945]
	if idx == 3 then
		local monsters = {"松江分舵精锐"}
		CALL("任务点杀怪",monsters,10011,100945)
	end
end,
--力斩连环-龙丘首级
[7383] = function(idx)
	--击败[PathMon,龙丘白,10011,2216]
	if idx == 0 then
		local monsters = {"龙丘白"}
		CALL("任务点杀怪",monsters,10011,2216)
	end
end,
--力斩连环-大功告成
[7384] = function(idx)
	--找[PathNpc,沐摇光,10011,449]复命
	if idx == 1 then
		CALL("任务点对话",10105009,"沐摇光",7384,1,10011,449)
	end
end,
--退守鲲鹏-转危为安
[7426] = function(idx)
	--找[PathNpc,胡学铜,10011,373]
	if idx == 0 then
		CALL("任务点对话",10105010,"胡学铜",7426,0,10011,373)
	end
end,
--退守鲲鹏-忧心忡忡
[7427] = function(idx)
	--找[PathNpc,吕浩然,10011,1250]
	if idx == 1 then
		CALL("任务点对话",10105153,"吕浩然",7427,1,10011,1250)
	end
end,
--退守鲲鹏-手足情深
[7428] = function(idx)
	--找[PathNpc,苏游,10011,1249]
	if idx == 2 then
		CALL("任务点对话",10105154,"苏游",7428,2,10011,1249)
	end
end,
--退守鲲鹏-天香白鹭
[7429] = function(idx)
	--找[PathNpc,白鹭洲,10011,908]
	if idx == 3 then
		CALL("任务点对话",10105532,"白鹭洲",7429,3,10011,908)
	end
end,
--妙手续命
[7430] = function(idx)
	--救助附近的[PathNpc,伤员,10011,40017]
	if idx == 0 then
		CALL("任务点采集","鲲鹏会伤员",10011,40017)
	end
end,
--击退追兵-十万火急
[7436] = function(idx)
	--击退[PathMon,横流坞尖兵,10011,101332]
	if idx == 0 then
		local monsters = {"横流坞尖兵","横流坞匪徒","横流坞头目"}
		CALL("任务点杀怪",monsters,10011,101332)
	end
	--击退[PathMon,横流坞匪徒,10011,101332]
	if idx == 1 then
		local monsters = {"横流坞匪徒","横流坞头目"}
		CALL("任务点杀怪",monsters,10011,101332)
	end
	--击退[PathMon,横流坞头目,10011,101332]
	if idx == 2 then
		local monsters = {"横流坞头目"}
		CALL("任务点杀怪",monsters,10011,101332)
	end
end,
--击退追兵-遍寻飞鹏
[7437] = function(idx)
	--去找[PathNpc,段飞鹏,10011,1169]
	if idx == 3 then
		CALL("任务点对话",10105011,"段飞鹏",7437,3,10011,1169)
	end
end,
--痛击倭寇-东瀛刺客
[7446] = function(idx)
	--击退[PathMon,天风流女匪,10011,101003]
	if idx == 0 then
		local monsters = {"天风流女匪","东瀛暗哨"}
		CALL("任务点杀怪",monsters,10011,101003)
	end
	--击退[PathMon,东瀛暗哨,10011,101003]
	if idx == 1 then
		local monsters = {"东瀛暗哨"}
		CALL("任务点杀怪",monsters,10011,101003)
	end
end,
--痛击倭寇-天风忍卫
[7447] = function(idx)
	--击退[PathMon,天风流初忍,10011,101018]
	if idx == 2 then
		local monsters = {"天风流初忍","天风浪人","忍者队长"}
		CALL("任务点杀怪",monsters,10011,101018)
	end
	--击退[PathMon,天风浪人,10011,101018]
	if idx == 3 then
		local monsters = {"天风浪人","忍者队长"}
		CALL("任务点杀怪",monsters,10011,101018)
	end
	--击退[PathMon,忍者队长,10011,101018]
	if idx == 4 then
		local monsters = {"忍者队长"}
		CALL("任务点杀怪",monsters,10011,101018)
	end
end,
--沿坡而上-寻找活口
[7451] = function(idx)
	--找到失踪的[PathNpc,鲲鹏会弟子,10011,1267]
	if idx == 0 then
		CALL("任务点对话",10105149,"鲲鹏会弟子",7451,0,10011,1267)
	end
end,
--沿坡而上-狮子坡头
[7452] = function(idx)
	--与[PathNpc,鲲鹏会弟子,10011,1267]对话。
	if idx == 1 then
		CALL("任务功能对话",7452,15008,"鲲鹏会弟子","与[PathNpc,鲲鹏会弟子,10011,1267]对话。",10011,1267)
	end
end,
--青坊归天-激愤出手
[7456] = function(idx)
	--击溃[PathMon,天风流看守,10011,101038]
	if idx == 0 then
		local monsters = {"天风流看守","东瀛杀手"}
		CALL("任务点杀怪",monsters,10011,101038)
	end
	--击溃[PathMon,东瀛杀手,10011,101038]
	if idx == 1 then
		local monsters = {"东瀛杀手"}
		CALL("任务点杀怪",monsters,10011,101038)
	end
end,
--青坊归天-田中龙二
[7457] = function(idx)
	--击败[PathMon,田中龙二,10011,1293]
	if idx == 2 then
		local monsters = {"青坊主 田中龙二"}
		CALL("任务点杀怪",monsters,10011,1293)
	end
end,
--集结人马
[7640] = function(idx)
	--等待六十秒
	if idx == 0 then
		CALL("等待任务完成",7640)
	end
end,

--------47,江湖试练--------
--杭州低级
[1411] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--江南
[1412] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--东越
[1413] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--杭州高级
[1414] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--九华
[1415] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--徐海
[1416] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--开封
[1417] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--秦川
[1418] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------1242,第二回：青龙夺命步步惊--------
--虽远必诛-研阳奉月
[36526] = function(idx)
	--[PathNpc,百里研阳,10006,1167]
	if idx == 2 then
		CALL("任务点对话",10121006,"百里研阳",36526,2,10006,1167)
	end
end,
--虽远必诛-蜃月乱谷
[36527] = function(idx)
	--[PathMon,莲月弟子,10006,100205]
	if idx == 0 then
		local monsters = {"莲月弟子","黑焰弟子"}
		CALL("任务点杀怪",monsters,10006,100212)
	end
	--[PathMon,黑焰弟子,10006,100212]
	if idx == 1 then
		local monsters = {"黑焰弟子"}
		CALL("任务点杀怪",monsters,10006,100212)
	end
end,
--羲和扶桑
[36012] = function(idx)
	--[PathArea,羲和谷,10006,300433]
	if idx == 0 then
		CALL("任务点移动",10006,300433)
	end
end,
--青龙绝命-五毒天华
[36510] = function(idx)
	--[PathNpc,草鬼婆,10006,1160]
	if idx == 0 then
		CALL("任务点对话",10121037,"草鬼婆",36510,0,10006,1160)
	end
end,
--青龙绝命-诡异内核
[36511] = function(idx)
	--[PathNpc,百里研阳,10006,1435]
	if idx == 1 then
		CALL("任务点对话",10121007,"百里研阳",36511,1,10006,1435)
	end
end,
--百草万宝-步履云间
[36512] = function(idx)
	--前往[PathArea,百草间,10006,300419]
	if idx == 0 then
		CALL("任务点移动",10006,300419)
	end
end,
--百草万宝-龙池飘香
[36513] = function(idx)
	--[PathNpc,柳凤兰,10006,1195]
	if idx == 1 then
		CALL("任务点对话",10121032,"柳凤兰",36513,1,10006,1195)
	end
end,
--飞蛾赴火-直上青云
[36514] = function(idx)
	--[PathMon,精锐弟子,10006,100067]
	if idx == 0 then
		local monsters = {"精锐弟子","影月弟子","青焰弟子"}
		CALL("任务点杀怪",monsters,10006,100094)
	end
	--[PathMon,影月弟子,10006,100088]
	if idx == 1 then
		local monsters = {"影月弟子","青焰弟子"}
		CALL("任务点杀怪",monsters,10006,100094)
	end
	--[PathMon,青焰弟子,10006,100094]
	if idx == 2 then
		local monsters = {"青焰弟子"}
		CALL("任务点杀怪",monsters,10006,100094)
	end
end,
--飞蛾赴火-千日醉魂
[36515] = function(idx)
	--[PathNpc,红料,10006,1192]
	if idx == 3 then
		CALL("任务点对话",10121034,"红料",36515,3,10006,1192)
	end
end,
--飞蛾赴火-红紫乱朱
[36516] = function(idx)
	--[PathMon,朱焰弟子,10006,100107]
	if idx == 4 then
		local monsters = {"朱焰弟子","苍焰弟子"}
		CALL("任务点杀怪",monsters,10006,100112)
	end
	--[PathMon,苍焰弟子,10006,100112]
	if idx == 5 then
		local monsters = {"苍焰弟子"}
		CALL("任务点杀怪",monsters,10006,100112)
	end
end,
--青衣魔蓝
[36016] = function(idx)
	--[PathNpc,何可人,10006,1193]
	if idx == 0 then
		CALL("任务点对话",10121035,"何可人",36016,0,10006,1193)
	end
end,
--六圣奇居
[36017] = function(idx)
	--[PathNpc,何可人,10006,1193]
	if idx == 0 then
		CALL("任务功能对话",36017,18104,"何可人","[PathNpc,何可人,10006,1193]",10006,1193)
	end
end,

--------1243,第三回：日暮星移月蒙明--------
--七巧化骨-枫香圣露
[36517] = function(idx)
	--[PathNpcLayer,蒙天方,10006,1194,1]
	if idx == 0 then
		--桥上?
		CALL("任务点对话",10121033,"蒙天方",36517,0,10006,1194,1)
	end
end,
--七巧化骨-事不宜迟
[36518] = function(idx)
	--前往[PathArea,六圣居后寨,10006,300422]
	if idx == 1 then
		CALL("任务点移动",10006,300422)
	end
end,
--七巧化骨-追风逐日
[36519] = function(idx)
	--[PathMon,苍月刺客,10006,100139]
	if idx == 2 then
		local monsters = {"苍月刺客","赤焰刺客"}
		CALL("任务点杀怪",monsters,10006,100145)
	end
	--[PathMon,赤焰刺客,10006,100145]
	if idx == 3 then
		local monsters = {"赤焰刺客"}
		CALL("任务点杀怪",monsters,10006,100145)
	end
end,
--血魂毒砂-奇局腹地
[36520] = function(idx)
	--前往[PathArea,六圣居后寨,10006,300423]
	if idx == 3 then
		CALL("任务点移动",10006,300423)
	end
end,
--血魂毒砂-蜃楼海市
[36521] = function(idx)
	--[PathMon,猎手精锐,10006,100154]
	if idx == 0 then
		local monsters = {"猎手精锐","莲月猎手","青焰猎手"}
		CALL("任务点杀怪",monsters,10006,100157)
	end
	--[PathMon,莲月猎手,10006,100162]
	if idx == 1 then
		local monsters = {"莲月猎手","青焰猎手"}
		CALL("任务点杀怪",monsters,10006,100157)
	end
	--[PathMon,青焰猎手,10006,100157]
	if idx == 2 then
		local monsters = {"青焰猎手"}
		CALL("任务点杀怪",monsters,10006,100157)
	end
end,
--月陨天华
[36021] = function(idx)
	--前往[PathArea,雷山居,10006,300439]
	if idx == 0 then
		CALL("任务点移动",10006,300439)
	end
end,
--孤注一掷-御风神行
[36522] = function(idx)
	--[PathNpc,方玉峰,10006,1050]
	if idx == 1 then
		CALL("任务点对话",10121000,"方玉峰",36522,1,10006,1050)
	end
end,
--孤注一掷-怅然若失
[36523] = function(idx)
	--[PathNpc,百里研阳,10006,1441]
	if idx == 0 then
		CALL("任务点对话",10121008,"百里研阳",36523,0,10006,1441)
	end
end,
--最后告别
[36023] = function(idx)
	--[PathNpc,百里研阳,10006,1441]
	if idx == 0 then
		CALL("任务功能对话",36023,18106,"百里研阳","[PathNpc,百里研阳,10006,1441]",10006,1441)
	end
end,
--日月轮回-宛若新生
[36524] = function(idx)
	--[PathNpc,蓝奉月,10006,2149]
	if idx == 0 then
		CALL("任务点对话",10121072,"蓝奉月",36524,0,10006,2149)
	end
end,
--日月轮回-拜过掌门
[36525] = function(idx)
	--[PathNpc,方玉峰,10006,1050]
	if idx == 1 then
		CALL("任务点对话",10121000,"方玉峰",36525,1,10006,1050)
	end
end,
--西湖开屏
[36025] = function(idx)
	--去杭州寻找[PathNpc,乌云珠,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,

--------901121,??--------
--密林小径-师妹相迎
[32600] = function(idx)
	--与[PathNpc,天香小师妹,10002,3513]交谈
	if idx == 0 then
		CALL("任务功能对话",32600,18028,"天香小师妹","与[PathNpc,天香小师妹,10002,3513]交谈",10002,3513)
	end
end,
--霓裳羽衣-再会师姐
[32601] = function(idx)
	--[PathNpc,左梁雨,10002,2392]
	if idx == 1 then
		CALL("任务点对话",10119002,"左梁雨",32601,1,10002,2392)
	end
end,
--霓裳羽衣-锦衣绮罗
[32602] = function(idx)
	--[PathNpc,夏洛,10002,2395]
	if idx == 0 then
		CALL("任务点对话",10119003,"夏洛",32602,0,10002,2395)
	end
end,
--一期一会-绝圣弃智
[32605] = function(idx)
	--[PathNpc,轩辕十四,10002,2383]
	if idx == 2 then
		CALL("任务功能对话",32605,10511006,"轩辕十四","[PathNpc,轩辕十四,10002,2383]",10002,2383)
	end
end,
--一期一会-巴蜀来客
[32606] = function(idx)
	--[PathNpc,唐依伊,10002,2386]
	if idx == 1 then
		CALL("任务点对话",10119041,"唐依伊",32606,1,10002,2386)
	end
end,
--一期一会-风尘仆仆
[32607] = function(idx)
	--[PathNpc,盛岚,10002,2387]
	if idx == 0 then
		CALL("任务点对话",10119042,"盛岚",32607,0,10002,2387)
	end
end,
--空谷留香-筹备花会
[32631] = function(idx)
	--[PathNpc,梁知音,10002,2401]
	if idx == 1 then
		CALL("任务点对话",10119000,"空谷留香 梁知音",32631,1,10002,2401)
	end
end,
--灵犀一动-心有灵犀
[32634] = function(idx)
	--在灵犀状态下打坐一分钟
	if idx == 0 then
		CALL("等待任务完成",32634)
	end
end,
--灵犀一动-非如其名
[32635] = function(idx)
	--[PathNpc,柳扶风,10002,2400]
	if idx == 1 then
		CALL("任务点对话",10119008,"柳扶风",32635,1,10002,2400)
	end
end,
--空谷留香-青龙往事
[32636] = function(idx)
	--[PathNpc,梁知音,10002,2401]
	if idx == 0 then
		CALL("任务点对话",10119000,"空谷留香 梁知音",32636,0,10002,2401)
	end
end,

--------248,第九回：龙首山前青鳞现--------
--一触即发-铸神落竹
[7407] = function(idx)
	--找[PathNpc,齐落竹,10011,920]
	if idx == 2 then
		CALL("任务点对话",10105534,"齐落竹",7407,2,10011,920)
	end
end,
--一触即发-大鹏展翅
[7408] = function(idx)
	--找[PathNpc,霍天鹏,10011,919]
	if idx == 1 then
		CALL("任务点对话",10105536,"霍天鹏",7408,1,10011,919)
	end
end,
--一触即发-铸神寒梅
[7409] = function(idx)
	--找[PathNpc,齐落梅,10011,918]
	if idx == 0 then
		CALL("任务点对话",10105535,"齐落梅",7409,0,10011,918)
	end
end,
--扫穴擒渠-扫穴擒渠
[7416] = function(idx)
	--找[PathNpc,孟长风,10011,959]
	if idx == 0 then
		CALL("任务功能对话",7416,11004,"孟长风","找[PathNpc,孟长风,10011,959]",10011,959)
	end
end,
--真武修行
[7420] = function(idx)
	--前往真武寻找修行师傅[PathNPC,莫千瑞,10007,433]
	if idx == 0 then
		CALL("任务点对话",20104740,"莫千瑞",7420,0,10007,433,1)
	end
end,
--江湖百业
[7421] = function(idx)
	--等级提升至30
	if idx == 0 then
		CALL("等级限制",7421)
	end
end,
--水波龙吟
[7422] = function(idx)
	--[PathNpc,唐青枫,10011,1792]
	if idx == 0 then
		CALL("任务点对话",10105173,"唐青枫",7422,0,10011,1792)
	end
end,
--五毒修行
[7423] = function(idx)
	--前往五毒寻找修行师傅[PathNPC, 袁明哲,10006,2352]
	if idx == 0 then
		CALL("任务点对话",1300173," 袁明哲",7423,0,10006,2352)
	end
end,
--霹雳旧址-长江分舵
[7466] = function(idx)
	--前往[PathArea,长江分舵,10011,300259]杀龙丘青
	if idx == 1 then
		CALL("任务点移动",10011,300259)
	end
end,
--霹雳旧址-突遇变故
[7467] = function(idx)
	--找[PathNpc,霍天鹏,10011,1172]
	if idx == 0 then
		CALL("任务点对话",10105139,"霍天鹏",7467,0,10011,1172)
	end
end,
--霹雳旧址-大战在即
[7468] = function(idx)
	--击破[PathMon,连环坞喽啰,10011,101059]
	if idx == 2 then
		local monsters = {"连环坞喽啰"}
		CALL("任务点杀怪",monsters,10011,101059)
	end
	--击破[PathMon,连环坞头目,10011,1379]
	if idx == 3 then
		local monsters = {"连环坞头目"}
		CALL("任务点杀怪",monsters,10011,101059)
	end
	--击破[PathMon,连环坞飞贼,10011,101059]
	if idx == 4 then
		local monsters = {"连环坞飞贼"}
		CALL("任务点杀怪",monsters,10011,101059)
	end
end,
--霹雳旧址-唐门高手
[7469] = function(idx)
	--去找[PathNpc,唐二,10011,1141]
	if idx == 5 then
		CALL("任务点对话",10105140,"唐二",7469,5,10011,1141)
	end
end,
--霹雳长风-疾驰飞奔
[7486] = function(idx)
	--前往[PathArea,长风林,10011,300260]求援
	if idx == 0 then
		CALL("任务点移动",10011,300260)
	end
end,
--霹雳长风-捕风捉影
[7487] = function(idx)
	--击退[PathMon,连环坞捕风,10011,101080]
	if idx == 1 then
		local monsters = {"连环坞捕风","连环坞捉影"}
		CALL("任务点杀怪",monsters,10011,101080)
	end
	--击退[PathMon,连环坞捉影,10011,101080]
	if idx == 2 then
		local monsters = {"连环坞捉影"}
		CALL("任务点杀怪",monsters,10011,101080)
	end
end,
--霹雳长风-霹雳长风
[7488] = function(idx)
	--找[PathNpc,孟长风,10011,1131]
	if idx == 3 then
		CALL("任务点对话",10105141,"孟长风",7488,3,10011,1131)
	end
end,
--鸣雷激战-鸣雷深谷
[7501] = function(idx)
	--击退[PathMon,连环坞暗青子,10011,101108]
	if idx == 0 then
		local monsters = {"连环坞暗青子","连环坞爪牙","连环坞伏击长"}
		CALL("任务点杀怪",monsters,10011,101108)
	end
	--击退[PathMon,连环坞爪牙,10011,101108]
	if idx == 1 then
		local monsters = {"连环坞爪牙","连环坞伏击长"}
		CALL("任务点杀怪",monsters,10011,101108)
	end
	--击退[PathMon,连环坞伏击长,10011,101108]
	if idx == 2 then
		local monsters = {"连环坞伏击长"}
		CALL("任务点杀怪",monsters,10011,101108)
	end
end,
--鸣雷激战-奇踪再现
[7502] = function(idx)
	--找[PathNpc,唐二,10011,1173]
	if idx == 3 then
		CALL("任务点对话",10105150,"唐二",7502,3,10011,1173)
	end
end,
--鸣雷激战-大鹏折翅
[7503] = function(idx)
	--击败[PathMon,欧顺,10011,1397]
	if idx == 4 then
		local monsters = {"万里鲲 欧顺"}
		CALL("任务点杀怪",monsters,10011,1397)
	end
end,
--鸣雷激战-霹雳雷火
[7504] = function(idx)
	--拿到火器[PathNpc,匣子,10011,40032]
	if idx == 5 then
		CALL("任务点采集","暗器匣子",10011,40032)
	end
end,
--鸣雷激战-大功告成
[7506] = function(idx)
	--找[PathNpc,唐二,10011,1173]
	if idx == 6 then
		CALL("任务点对话",10105150,"唐二",7506,6,10011,1173)
	end
end,
--擒贼擒王-一青二白
[7507] = function(idx)
	--除掉[PathMon,龙丘青,10011,418]
	if idx == 0 then
		local monsters = {"龙丘青"}
		CALL("任务点杀怪",monsters,10011,418)
	end
end,
--擒贼擒王-首恶伏诛
[7508] = function(idx)
	--[PathMon,连环坞飘子,10011,101134]
	if idx == 1 then
		local monsters = {"连环坞飘子","分舵守卫"}
		CALL("任务点杀怪",monsters,10011,101134)
	end
	--[PathMon,分舵守卫,10011,101134]
	if idx == 2 then
		local monsters = {"分舵守卫"}
		CALL("任务点杀怪",monsters,10011,101134)
	end
end,
--擒贼擒王-杀出分舵
[7509] = function(idx)
	--冲到[PathArea,分舵外围,10011,300922]
	if idx == 3 then
		CALL("任务点移动",10011,300922)
	end
end,
--四明居士-青枫传讯
[7626] = function(idx)
	--找[PathNpc,唐二,10011,1263]
	if idx == 0 then
		CALL("任务点对话",10105159,"唐二",7626,0,10011,1263)
	end
end,
--四明居士-江南四明
[7627] = function(idx)
	--前往[PathArea,四明居,10011,300258]
	if idx == 1 then
		CALL("任务点移动",10011,300258)
	end
end,
--四明居士-盟主再现
[7629] = function(idx)
	--找[PathNpc,叶知秋,10011,1242]
	if idx == 2 then
		CALL("任务点对话",10105152,"叶知秋",7629,2,10011,1242)
	end
end,
--四明居士-传讯指路
[7634] = function(idx)
	--找[PathNpc,上官小仙,10011,1194]
	if idx == 3 then
		CALL("任务点对话",10105143,"上官小仙",7634,3,10011,1194)
	end
end,
--四明居士-孤军深入
[7636] = function(idx)
	--前往[PathArea,天池分舵,10011,300029]
	if idx == 4 then
		CALL("任务点移动",10011,300029)
	end
end,
--四明居士-唐门少俊
[7637] = function(idx)
	--找[PathNpc,唐二,10011,1197]
	if idx == 5 then
		CALL("任务点对话",10105146,"唐二",7637,5,10011,1197)
	end
end,
--四明居士-鲲鹏长风
[7638] = function(idx)
	--找[PathNpc,孟长风,10011,1196]
	if idx == 6 then
		CALL("任务点对话",10105142,"孟长风",7638,6,10011,1196)
	end
end,
--声东击西-震惊百里
[7631] = function(idx)
	--干掉[PathMon,百里信,10011,1443]
	if idx == 0 then
		local monsters = {"踏风 百里信"}
		CALL("任务点杀怪",monsters,10011,1443)
	end
end,
--声东击西-以暴制暴
[7632] = function(idx)
	--[PathMon,天池分舵武师,10011,101193]
	if idx == 1 then
		local monsters = {"天池分舵武师"}
		CALL("任务点杀怪",monsters,10011,101193)
	end
	--[PathMon,天池分舵暗哨,10011,101193]
	if idx == 4 then
		local monsters = {"天池分舵暗哨"}
		CALL("任务点杀怪",monsters,10011,101193)
	end
	--[PathMon,天池分舵精锐,10011,101193]
	if idx == 5 then
		local monsters = {"天池分舵精锐"}
		CALL("任务点杀怪",monsters,10011,101193)
	end
end,
--声东击西-一箭双雕
[7633] = function(idx)
	--干掉[PathMon,文太白,10011,1448]
	if idx == 2 then
		local monsters = {"活阎罗 文太白"}
		CALL("任务点杀怪",monsters,10011,1448)
	end
	--干掉[PathMon,龙丘复阳,10011,2233]
	if idx == 3 then
		local monsters = {"龙丘复阳"}
		CALL("任务点杀怪",monsters,10011,2233)
	end
end,
--义字坡头
[7635] = function(idx)
	--前往[PathArea,义字坡,10011,300261]
	if idx == 0 then
		CALL("任务点移动",10011,300261)
	end
end,
--界限突破壹
[7645] = function(idx)
	--等待突破：江湖百业(30级)
	if idx == 0 then
		CALL("界限突破",7645)
	end
end,
--丐帮修行
[7646] = function(idx)
	--前往丐帮寻找修行师傅[PathNPC,柏仪礼,10003,500]
	if idx == 0 then
		CALL("任务点对话",20104737,"柏仪礼",7646,0,10003,500)
	end
end,
--唐门修行
[7647] = function(idx)
	--前往唐门寻找修行师傅[PathNPC,唐诗云,10005,138]
	if idx == 0 then
		CALL("任务点对话",20104738,"唐诗云",7647,0,10005,138)
	end
end,
--天香修行
[7648] = function(idx)
	--前往天香寻找修行师傅[PathNPC,宓瑾,10002,2981]
	if idx == 0 then
		CALL("任务点对话",20104739,"宓瑾",7648,0,10002,2981)
	end
end,
--神威修行
[7649] = function(idx)
	--前往神威寻找修行师傅[PathNPC,佟之行,10004,1535]
	if idx == 0 then
		CALL("任务点对话",20104736,"佟之行",7649,0,10004,1535)
	end
end,
--太白修行
[7650] = function(idx)
	--前往太白寻找修行师傅[PathNPC,常衡,10009,1098]
	if idx == 0 then
		CALL("任务点对话",20104735,"常衡",7650,0,10009,1098)
	end
end,

--------901122,??--------
--分内之事-不骄不躁
[32611] = function(idx)
	--[PathMon,东瀛喽啰,10002,101720]
	if idx == 0 then
		local monsters = {"东瀛喽啰","东瀛暴徒"}
		CALL("任务点杀怪",monsters,10002,101809)
	end
	--[PathMon,东瀛暴徒,10002,101809]
	if idx == 1 then
		local monsters = {"东瀛暴徒"}
		CALL("任务点杀怪",monsters,10002,101809)
	end
end,
--听花小筑-独挽斜阳
[32613] = function(idx)
	--[PathNpc,林挽阳,10002,2410]
	if idx == 0 then
		CALL("任务点对话",10119011,"林挽阳",32613,0,10002,2410)
	end
end,
--听花小筑-素问灵枢
[32614] = function(idx)
	--[PathNpc,素问轩弟子,10002,2411]
	if idx == 1 then
		CALL("任务点对话",10119050,"素问轩弟子",32614,1,10002,2411)
	end
end,
--听花小筑-月夕花朝
[32615] = function(idx)
	--[PathEnt,仙客来,10002,40099]
	if idx == 2 then
		CALL("任务点采集","仙客来",10002,40099)
	end
	--[PathEnt,子风藤,10002,40100]
	if idx == 3 then
		CALL("任务点采集","子风藤",10002,40100)
	end
	--[PathEnt,百子莲,10002,40101]
	if idx == 4 then
		CALL("任务点采集","百子莲",10002,40101)
	end
	--[PathEnt,七里香,10002,40102]
	if idx == 5 then
		CALL("任务点采集","七里香",10002,40102)
	end
	--[PathEnt,鹤望兰,10002,40103]
	if idx == 6 then
		CALL("任务点采集","鹤望兰",10002,40103)
	end
end,
--听花小筑-大功告成
[32616] = function(idx)
	--[PathNpc,林挽阳,10002,2410]
	if idx == 7 then
		CALL("任务点对话",10119011,"林挽阳",32616,7,10002,2410)
	end
end,
--听花小筑-云中白鹤
[32617] = function(idx)
	--[PathNpcLayer,白衣人,10002,2413,1]
	if idx == 8 then
		--桥上?
		CALL("坐标点对话",10119012,"白衣人",32617,8,1035,890,10002,1)
	end
end,
--杏雨梨云-举步生风
[32618] = function(idx)
	--[PathMon,东瀛浪人,10002,101733]
	if idx == 0 then
		local monsters = {"东瀛浪人","东瀛刀客"}
		CALL("任务点杀怪",monsters,10002,101745)
	end
	--[PathMon,东瀛刀客,10002,101745]
	if idx == 1 then
		local monsters = {"东瀛刀客"}
		CALL("任务点杀怪",monsters,10002,101745)
	end
end,
--东瀛余孽-不容小觑
[32619] = function(idx)
	--[PathMon,炎斩上忍,10002,101759]
	if idx == 0 then
		local monsters = {"炎斩上忍","天风流死士"}
		CALL("任务点杀怪",monsters,10002,101761)
	end
	--[PathMon,天风流死士,10002,101761]
	if idx == 1 then
		local monsters = {"天风流死士"}
		CALL("任务点杀怪",monsters,10002,101761)
	end
end,
--棋高一着-傲雪凌霜
[32620] = function(idx)
	--[PathNpc,林弃霜,10002,2416]
	if idx == 0 then
		CALL("任务点对话",10119014,"天元星 林弃霜",32620,0,10002,2416)
	end
end,
--棋高一着-后患无穷
[32621] = function(idx)
	--[PathMon,东瀛体忍,10002,101765]
	if idx == 1 then
		local monsters = {"东瀛体忍","东瀛悍匪"}
		CALL("任务点杀怪",monsters,10002,101774)
	end
	--[PathMon,东瀛悍匪,10002,101774]
	if idx == 2 then
		local monsters = {"东瀛悍匪"}
		CALL("任务点杀怪",monsters,10002,101774)
	end
end,
--不请自来-余音绕梁
[32622] = function(idx)
	--[PathNpc,白鹭洲,10002,2419]
	if idx == 0 then
		CALL("任务点对话",10119017,"乱红弦 白鹭洲",32622,0,10002,2419)
	end
end,
--不请自来-片甲不留
[32623] = function(idx)
	--[PathMon,天风流恶徒,10002,101804]
	if idx == 1 then
		local monsters = {"天风流恶徒","天风流刀客"}
		CALL("任务点杀怪",monsters,10002,101798)
	end
	--[PathMon,天风流刀客,10002,101798]
	if idx == 2 then
		local monsters = {"天风流刀客"}
		CALL("任务点杀怪",monsters,10002,101798)
	end
end,
--不请自来-秘而不宣
[32624] = function(idx)
	--[PathNpc,浪人尸体,10002,2420]
	if idx == 3 then
		CALL("任务点对话",20119013,"浪人尸体",32624,3,10002,2420)
	end
end,
--东瀛余孽-叶不沾身
[32637] = function(idx)
	--[PathArea,前往星罗居,10002,300890]
	if idx == 2 then
		CALL("任务点移动",10002,300890)
	end
end,
--分内之事-小筑之前
[32638] = function(idx)
	--[PathArea,前往听花小筑,10002,300896]
	if idx == 3 then
		CALL("任务点移动",10002,300896)
	end
end,
--分内之事-前往杏林
[32639] = function(idx)
	--[PathArea,进入杏林,10002,300889]
	if idx == 2 then
		CALL("任务点移动",10002,300889)
	end
end,
--杏雨梨云-不知深浅
[32640] = function(idx)
	--[PathArea,深入杏林,10002,300897]
	if idx == 2 then
		CALL("任务点移动",10002,300897)
	end
end,

--------901123,??--------
--东瀛残部-不成气候
[32625] = function(idx)
	--[PathMon,天风流强兵,10002,102006]
	if idx == 0 then
		local monsters = {"天风流强兵","天风流残部","天风流余孽"}
		CALL("任务点杀怪",monsters,10002,102030)
	end
	--[PathMon,天风流残部,10002,102012]
	if idx == 1 then
		local monsters = {"天风流残部","天风流余孽"}
		CALL("任务点杀怪",monsters,10002,102030)
	end
	--[PathMon,天风流余孽,10002,102030]
	if idx == 2 then
		local monsters = {"天风流余孽"}
		CALL("任务点杀怪",monsters,10002,102030)
	end
end,
--巧夺天工-青蓝冰水
[32627] = function(idx)
	--[PathNpc,唐青铃,10002,2407]
	if idx == 0 then
		CALL("NPC对话","柳扶风",1011901001,0,0,0,0,0,0,1758,714)
		CALL("任务点对话",10119019,"飞灵伞 唐青铃",32627,0,10002,2407)
	end
end,
--巧夺天工-悬灯结彩
[32628] = function(idx)
	--[PathEnt,花神灯,10002,40107]
	if idx == 1 then
		CALL("任务点采集","花神灯",10002,40107)
	end
end,
--时不我待-死性不改
[32629] = function(idx)
	--[PathNpc,唐青铃,10002,2407]
	if idx == 0 then
		CALL("任务点对话",10119019,"飞灵伞 唐青铃",32629,0,10002,2407)
	end
end,
--时不我待-携灯而返
[32630] = function(idx)
	--[PathNpc,梁知音,10002,2401]
	if idx == 1 then
		CALL("御风神行",5,1821,1451)
		CALL("任务点对话",10119000,"空谷留香 梁知音",32630,1,10002,2401)
	end
end,
--花会将至-话不投机
[32632] = function(idx)
	--[PathNpc,柳扶风,10002,2409]
	if idx == 0 then
		CALL("任务点对话",10119010,"柳扶风",32632,0,10002,2409)
	end
end,

--------61,燕云大漠书旧事--------
--神威堡主-燕云血战
[2002] = function(idx)
	--[PathNpc,韩学信,10004,192]
	if idx == 0 then
		CALL("任务点对话",10102011,"韩学信",2002,0,10004,192)
	end
end,
--神威堡主-百里天刀
[2003] = function(idx)
	--[PathNpc,凌飞营主,10004,301059]
	if idx == 1 then
		CALL("任务点移动",10004,301059)
	end
end,
--异地重逢-又遇旧识
[2006] = function(idx)
	--找[PathNpc,离玉堂,10004,1555]
	if idx == 0 then
		CALL("任务点对话",10102085,"离玉堂",2006,0,10004,1555)
	end
end,
--异地重逢-天刀凌飞
[2007] = function(idx)
	--[PathNpc,天刀凌飞,10004,1556]
	if idx == 1 then
		CALL("任务点对话",10102092,"天刀凌飞",2007,1,10004,1556)
	end
end,
--异地重逢-西夏贼兵
[2008] = function(idx)
	--[PathMon,西夏前哨,10004,100944]
	if idx == 2 then
		local monsters = {"西夏前哨","西夏探子"}
		CALL("任务点杀怪",monsters,10004,100964)
	end
	--[PathMon,西夏探子,10004,100964]
	if idx == 3 then
		local monsters = {"西夏探子"}
		CALL("任务点杀怪",monsters,10004,100964)
	end
end,
--异地重逢-扫荡敌酋
[2009] = function(idx)
	--[PathMon,前哨队副,10004,101419]
	if idx == 4 then
		local monsters = {"前哨队副","西夏巡查","前哨头领"}
		CALL("任务点杀怪",monsters,10004,101411)
	end
	--[PathMon,西夏巡查,10004,101399]
	if idx == 5 then
		local monsters = {"西夏巡查","前哨头领"}
		CALL("任务点杀怪",monsters,10004,101411)
	end
	--[PathMon,前哨头领,10004,101411]
	if idx == 6 then
		local monsters = {"前哨头领"}
		CALL("任务点杀怪",monsters,10004,101411)
	end
end,
--千里寻踪-回禀凌飞
[2011] = function(idx)
	--[PathNpc,天刀凌飞,10004,1556]
	if idx == 0 then
		CALL("任务点对话",10102092,"天刀凌飞",2011,0,10004,1556)
	end
end,
--千里寻踪-人灵思成
[2012] = function(idx)
	--[PathNpc,人灵思成,10004,1560]
	if idx == 1 then
		CALL("任务点对话",10102094,"人灵思成",2012,1,10004,1560)
	end
end,
--千里寻踪-寻求过往
[2013] = function(idx)
	--[PathNpc,队长夏玺,10004,1557]
	if idx == 2 then
		CALL("任务点移动",10004,1557)
	end
end,
--千里寻踪-终有蛛丝
[2014] = function(idx)
	--询问当年燕云血战的线索
	if idx == 3 then
		CALL("等待任务完成",2014)
	end
end,
--蛛丝马迹-绝尘古镇
[2016] = function(idx)
	--前往[PathNpc,绝尘镇,10004,301062]
	if idx == 0 then
		CALL("任务点移动",10004,301062)
	end
end,
--蛛丝马迹-继续追查
[2017] = function(idx)
	--[PathNpc,赫连人圭,10004,1575]
	if idx == 1 then
		CALL("任务点对话",10102099,"赫连人圭",2017,1,10004,1575)
	end
end,
--蛛丝马迹-再接再厉
[2018] = function(idx)
	--[PathNpc,慕容彦,10004,1576]
	if idx == 2 then
		CALL("任务点对话",10102100,"慕容彦",2018,2,10004,1576)
	end
	--[PathNpc,贺兰雪,10004,1577]
	if idx == 3 then
		CALL("任务点对话",10102101,"贺兰雪",2018,3,10004,1577)
	end
end,
--蛛丝马迹-贼寇为患
[2019] = function(idx)
	--[PathNpc,李保正,10004,1562]
	if idx == 4 then
		CALL("任务点对话",10102088,"李保正",2019,4,10004,1562)
	end
end,
--除暴安良-消灭马贼
[2021] = function(idx)
	--[PathMon,马贼前哨,10004,101467]
	if idx == 0 then
		local monsters = {"马贼前哨","马贼喽啰","踩盘打手"}
		CALL("任务点杀怪",monsters,10004,101320)
	end
	--[PathMon,马贼喽啰,10004,101311]
	if idx == 1 then
		local monsters = {"马贼喽啰","踩盘打手"}
		CALL("任务点杀怪",monsters,10004,101320)
	end
	--[PathMon,踩盘打手,10004,101320]
	if idx == 2 then
		local monsters = {"踩盘打手"}
		CALL("任务点杀怪",monsters,10004,101320)
	end
end,
--除暴安良-贼寇为患
[2022] = function(idx)
	--[PathMon,马贼打手,10004,101470]
	if idx == 3 then
		local monsters = {"马贼打手","马贼暗哨"}
		CALL("任务点杀怪",monsters,10004,101345)
	end
	--[PathMon,马贼暗哨,10004,101345]
	if idx == 4 then
		local monsters = {"马贼暗哨"}
		CALL("任务点杀怪",monsters,10004,101345)
	end
end,
--除暴安良-杀鸡儆猴
[2023] = function(idx)
	--[PathMon,马贼杆首,10004,1804]
	if idx == 5 then
		local monsters = {"马贼杆首"}
		CALL("任务点杀怪",monsters,10004,1804)
	end
end,
--除暴安良-回传捷报
[2024] = function(idx)
	--[PathNpc,李保正,10004,1562]
	if idx == 6 then
		CALL("任务点对话",10102088,"李保正",2024,6,10004,1562)
	end
end,
--恩怨分明-柳暗花明
[2026] = function(idx)
	--找[PathNpc,李秉,10004,1563]
	if idx == 0 then
		CALL("任务点对话",10102089,"李秉",2026,0,10004,1563)
	end
end,
--恩怨分明-知恩图报
[2027] = function(idx)
	--与李秉老人交流
	if idx == 1 then
		CALL("等待任务完成",2027)
	end
end,
--恩怨分明-垂髫顽童
[2028] = function(idx)
	--找[PathNpc,李娃,10004,301063]
	if idx == 2 then
		CALL("任务点移动",10004,301063)
	end
end,
--童言无忌-江山秦岭
[2031] = function(idx)
	--找[PathNpc,江山,10004,1565]
	if idx == 0 then
		CALL("任务点对话",10102086,"江山",2031,0,10004,1565)
	end
	--找[PathNpc,秦岭,10004,1566]
	if idx == 1 then
		CALL("任务点对话",10102087,"秦岭",2031,1,10004,1566)
	end
end,
--童言无忌-寻找令牌
[2032] = function(idx)
	--找[PathNpc,慕容敏,10004,1571]
	if idx == 2 then
		CALL("任务点对话",10102095,"慕容敏",2032,2,10004,1571)
	end
	--找[PathNpc,拓跋大勇,10004,1572]
	if idx == 3 then
		CALL("任务点对话",10102096,"拓跋大勇",2032,3,10004,1572)
	end
end,
--童言无忌-燕云大漠
[2033] = function(idx)
	--[PathNpc,李娃,10004,1564]
	if idx == 4 then
		CALL("任务点对话",10102090,"李娃",2033,4,10004,1564)
	end
end,
--童言无忌-小儿难挡
[2034] = function(idx)
	--[PathNpc,五彩石,10004,40028]
	if idx == 5 then
		CALL("任务点采集","五彩石",10004,40028)
	end
end,
--尊字奇令-交换情报
[2036] = function(idx)
	--[PathNpc,李娃,10004,1564]
	if idx == 0 then
		CALL("任务点对话",10102090,"李娃",2036,0,10004,1564)
	end
end,
--尊字奇令-害人不浅
[2037] = function(idx)
	--前往[PathNpc,神木谷,10004,301064]
	if idx == 1 then
		CALL("任务点移动",10004,301064)
	end
end,
--尊字奇令-再战贼寇
[2038] = function(idx)
	--[PathMon,山贼小头目,10004,100980]
	if idx == 2 then
		local monsters = {"山贼小头目","山贼打手","山贼喽啰"}
		CALL("任务点杀怪",monsters,10004,101488)
	end
	--[PathMon,山贼打手,10004,100993]
	if idx == 3 then
		local monsters = {"山贼打手","山贼喽啰"}
		CALL("任务点杀怪",monsters,10004,101488)
	end
	--[PathMon,山贼喽啰,10004,101488]
	if idx == 4 then
		local monsters = {"山贼喽啰"}
		CALL("任务点杀怪",monsters,10004,101488)
	end
end,
--尊字奇令-杀贼取物
[2039] = function(idx)
	--[PathMon,神木山大王,10004,1568]
	if idx == 5 then
		local monsters = {"神木山大王"}
		CALL("任务点杀怪",monsters,10004,1568)
	end
end,
--敌踪尾随-一骑绝尘
[2041] = function(idx)
	--找[PathNpc,江山,10004,1565]
	if idx == 0 then
		CALL("任务点对话",10102086,"江山",2041,0,10004,1565)
	end
end,
--敌踪尾随-必有隐情
[2042] = function(idx)
	--[PathNpc,跟上江山,10004,301065]
	if idx == 1 then
		CALL("任务点移动",10004,301065)
	end
end,
--敌踪尾随-西夏探子
[2043] = function(idx)
	--[PathMon,西夏密探,10004,101478]
	if idx == 2 then
		local monsters = {"西夏密探","暗哨精英"}
		CALL("任务点杀怪",monsters,10004,101376)
	end
	--[PathMon,暗哨精英,10004,101376]
	if idx == 3 then
		local monsters = {"暗哨精英"}
		CALL("任务点杀怪",monsters,10004,101376)
	end
end,
--敌踪尾随-再寻江山
[2044] = function(idx)
	--找[PathNpc,江山,10004,1584]
	if idx == 4 then
		CALL("任务点对话",10102108,"江山",2044,4,10004,1584)
	end
end,

--------62,沧海遗孤祭忠仆--------
--陈年旧事-瀚海小屋
[2056] = function(idx)
	--[PathNpc,找到老人,10004,301091]
	if idx == 0 then
		CALL("任务点移动",10004,301091)
	end
end,
--陈年旧事-燕云血战
[2057] = function(idx)
	--[PathNpc,沈笠翁,10004,1600]
	if idx == 1 then
		CALL("任务点对话",10102124,"沈笠翁",2057,1,10004,1600)
	end
end,
--陈年旧事-真相大白
[2058] = function(idx)
	--[PathNpc,江山,10004,1837]
	if idx == 2 then
		CALL("任务功能对话",2058,15015,"江山","[PathNpc,江山,10004,1837]",10004,1837)
	end
end,
--灭绝人性-返回绝尘
[2061] = function(idx)
	--[PathNpc,绝尘镇,10004,301066]
	if idx == 0 then
		CALL("任务点移动",10004,301066)
	end
end,
--灭绝人性-再起波澜
[2062] = function(idx)
	--[PathNpc,了解情况,10004,1585]
	if idx == 1 then
		CALL("等待任务完成",2062)
	end
end,
--灭绝人性-残忍西夏
[2063] = function(idx)
	--[PathNpc,李娃,10004,1587]
	if idx == 2 then
		CALL("任务点对话",10102111,"李娃",2063,2,10004,1587)
	end
	--[PathNpc,李保正,10004,1585]
	if idx == 3 then
		CALL("任务点对话",10102109,"李保正",2063,3,10004,1585)
	end
	--[PathNpc,贺兰雪,10004,1594]
	if idx == 4 then
		CALL("任务点对话",10102118,"贺兰雪",2063,4,10004,1594)
	end
end,
--村庄之围-保护村民
[2066] = function(idx)
	--击破[PathMon,西夏刽子手,10317,100031]
	if idx == 0 then
		local monsters = {"西夏刽子手","西夏兵卒"}
		CALL("任务点杀怪",monsters,10317,7)
	end
	--击破[PathMon,西夏兵卒,10317,100030]
	if idx == 1 then
		local monsters = {"西夏兵卒"}
		CALL("任务点杀怪",monsters,10317,7)
	end
	--[PathNPC,慕容敏,10317,4]
	if idx == 3 then
		CALL("任务点对话",10102112,"慕容敏",2066,3,10317,4)
	end
	--[PathNPC,元坚,10317,7]
	if idx == 6 then
		CALL("任务点对话",10102123,"元坚",2066,6,10317,7)
	end
end,
--村庄之围-再护村民
[2067] = function(idx)
	--[PathNPC,细封宁丛,10317,6]
	if idx == 5 then
		CALL("任务点对话",10102122,"细封宁丛",2067,5,10317,6)
	end
	--[PathNPC,赫连人圭,10317,5]
	if idx == 4 then
		CALL("任务点对话",10102116,"赫连人圭",2067,4,10317,5)
	end
	--击破[PathMon,西夏刽子手,10317,100031]
	if idx == 7 then
		local monsters = {"西夏刽子手","西夏兵卒"}
		CALL("任务点杀怪",monsters,10317,100030)
	end
	--击破[PathMon,西夏兵卒,10317,100030]
	if idx == 8 then
		local monsters = {"西夏兵卒"}
		CALL("任务点杀怪",monsters,10317,100030)
	end
end,
--村庄之围-先诛首恶
[2068] = function(idx)
	--击破[PathMon,精英队长,10317,1]
	if idx == 2 then
		local monsters = {"精英队长"}
		CALL("任务点杀怪",monsters,10317,1)
	end
end,
--来龙去脉-绝尘如昔
[2071] = function(idx)
	--[PathNpc,李保正,10004,1585]
	if idx == 0 then
		CALL("任务点对话",10102109,"李保正",2071,0,10004,1585)
	end
end,
--来龙去脉-前往神威
[2072] = function(idx)
	--[PathNpc,神威堡千里营,10004,301067]
	if idx == 1 then
		CALL("任务点移动",10004,301067)
	end
end,
--神威在天-堡主驾临
[2076] = function(idx)
	--[PathNpc,韩学信,10004,1601]
	if idx == 0 then
		CALL("任务点对话",10102125,"韩学信",2076,0,10004,1601)
	end
end,
--神威在天-义字当头
[2077] = function(idx)
	--[PathNpc,夏玺,10004,1557]
	if idx == 1 then
		CALL("任务点对话",10102093,"夏玺",2077,1,10004,1557)
	end
end,
--蔷薇绽放-燕归大漠
[2081] = function(idx)
	--[PathNpc,燕南飞,10004,1602]
	if idx == 0 then
		CALL("任务点对话",10102126,"燕南飞",2081,0,10004,1602)
	end
end,
--蔷薇绽放-万里飞沙
[2082] = function(idx)
	--[PathNpc,离玉堂,10004,1603]
	if idx == 1 then
		CALL("任务点对话",10102127,"离玉堂",2082,1,10004,1603)
	end
end,
--蔷薇绽放-武林贩子
[2083] = function(idx)
	--[PathNpc,剑意居,10004,301068]
	if idx == 2 then
		CALL("任务点移动",10004,301068)
	end
end,
--剑意客栈-查无此人
[2086] = function(idx)
	--[PathNpc,客栈掌柜,10004,1610]
	if idx == 0 then
		CALL("任务点对话",10102128,"客栈掌柜",2086,0,10004,1610)
	end
end,
--剑意客栈-以武偿德
[2087] = function(idx)
	--教训[PathMon,西夏逃兵,10004,101010]
	if idx == 1 then
		local monsters = {"西夏逃兵","西夏兵痞"}
		CALL("任务点杀怪",monsters,10004,101022)
	end
	--教训[PathMon,西夏兵痞,10004,101022]
	if idx == 2 then
		local monsters = {"西夏兵痞"}
		CALL("任务点杀怪",monsters,10004,101022)
	end
end,
--剑意客栈-再询掌柜
[2088] = function(idx)
	--[PathNpc,客栈掌柜,10004,1610]
	if idx == 3 then
		CALL("任务点对话",10102128,"客栈掌柜",2088,3,10004,1610)
	end
end,
--丁氏其人-有违道义
[2091] = function(idx)
	--去[PathNpc,绿洲互市,10004,301069]
	if idx == 0 then
		CALL("任务点移动",10004,301069)
	end
end,
--丁氏其人-有所不为
[2092] = function(idx)
	--[PathNpc,客栈掌柜,10004,1610]
	if idx == 1 then
		CALL("任务点对话",10102128,"客栈掌柜",2092,1,10004,1610)
	end
end,
--武林贩子
[2095] = function(idx)
	--去[PathNpc,剑意居后院,10004,301090]
	if idx == 0 then
		CALL("任务功能对话",2095,15010,"剑意居后院","去[PathNpc,剑意居后院,10004,301090]",10004,301090)
	end
end,

--------461,九华见闻隐藏任务--------
--伯阳任务第一步
[13801] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--伯阳任务第二步
[13802] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--伯阳任务第三步
[13803] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--伯阳任务第四步
[13804] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--骆莺前置第一步
[13805] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--骆莺见闻第二步
[13806] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--骆莺见闻第三步
[13807] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--毛毛前置任务
[13810] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--毛毛前置第二步
[13811] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--毛毛前置第三步
[13812] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--高辰前置见闻一
[13820] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--高辰前置任务见闻二
[13821] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--柳云街第一步
[13825] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--柳云街第二步
[13826] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--柳云街第三步
[13827] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--计无言见闻第一步
[13830] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--计无言见闻第二步
[13831] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--伯阳任务第五步
[13832] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--计无言见闻第三步
[13833] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--九华阿飞第一步
[13835] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--九华阿飞第二步
[13836] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--九华阿飞第三步
[13837] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--九华阿飞第四步
[13838] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--徐海阿飞第一步
[13840] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--徐海阿飞第二步
[13841] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--徐海阿飞第三步
[13842] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--白胜男见闻
[13860] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--白胜男见闻拾取木剑
[13861] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--交还木剑
[13862] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--唐林门派
[13865] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--唐林采集
[13866] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------63,龙雀长吟起风云--------
--夺取宝刀-大夏龙雀
[2111] = function(idx)
	--去[PathNpc,绿洲互市,10004,301070]
	if idx == 0 then
		CALL("任务点移动",10004,301070)
	end
end,
--夺取宝刀-左膀右臂
[2112] = function(idx)
	--[PathNpc,黄元文,10004,1611]
	if idx == 1 then
		CALL("任务点对话",10102129,"黄元文",2112,1,10004,1611)
	end
end,
--惊闻密报-截获密报
[2116] = function(idx)
	--偷袭[PathMon,西夏传令兵,10004,1835]
	if idx == 0 then
		local monsters = {"西夏传令兵"}
		CALL("任务点杀怪",monsters,10004,1835)
	end
end,
--惊闻密报-终有收获
[2117] = function(idx)
	--去找[PathNpc,黄元文,10004,301071]
	if idx == 1 then
		CALL("任务点移动",10004,301071)
	end
end,
--惊闻密报-玉堂再临
[2118] = function(idx)
	--[PathNpc,离玉堂,10004,1621]
	if idx == 2 then
		CALL("任务点对话",10102130,"离玉堂",2118,2,10004,1621)
	end
end,
--前哨破敌-一触即发
[2121] = function(idx)
	--[PathMon,风飞谷巡查,10004,101034]
	if idx == 0 then
		local monsters = {"风飞谷巡查","风飞谷守卫"}
		CALL("任务点杀怪",monsters,10004,101039)
	end
	--[PathMon,风飞谷守卫,10004,101039]
	if idx == 1 then
		local monsters = {"风飞谷守卫"}
		CALL("任务点杀怪",monsters,10004,101039)
	end
end,
--前哨破敌-豪气干云
[2122] = function(idx)
	--[PathNpc,黄元文,10004,1626]
	if idx == 2 then
		CALL("任务点对话",10102131,"黄元文",2122,2,10004,1626)
	end
end,
--大夏龙雀
[2125] = function(idx)
	--[PathNpc,黄元文,10004,1626]
	if idx == 0 then
		CALL("任务功能对话",2125,15011,"黄元文","[PathNpc,黄元文,10004,1626]",10004,1626)
	end
end,
--龙山废墟-神兵被夺
[2131] = function(idx)
	--[PathNpc,离玉堂,10004,1621]
	if idx == 0 then
		CALL("任务点对话",10102130,"离玉堂",2131,0,10004,1621)
	end
end,
--龙山废墟-螳螂捕蝉
[2132] = function(idx)
	--追踪[PathNpc,杜云松,10004,301072]
	if idx == 1 then
		CALL("任务点移动",10004,301072)
	end
end,
--龙山废墟-尾随而至
[2133] = function(idx)
	--[PathNpc,万里杀眼线,10004,1627]
	if idx == 2 then
		CALL("任务点对话",10102132,"万里杀眼线",2133,2,10004,1627)
	end
end,
--龙山废墟-龙山废墟
[2134] = function(idx)
	--[PathNpc,黄元文,10004,1628]
	if idx == 3 then
		CALL("任务点对话",10102135,"黄元文",2134,3,10004,1628)
	end
end,
--一鼓作气-龙山废墟
[2136] = function(idx)
	--[PathMon,马前卒,10004,101060]
	if idx == 0 then
		local monsters = {"马前卒","开路先锋"}
		CALL("任务点杀怪",monsters,10004,101078)
	end
	--[PathMon,开路先锋,10004,101078]
	if idx == 1 then
		local monsters = {"开路先锋"}
		CALL("任务点杀怪",monsters,10004,101078)
	end
end,
--一鼓作气-损失惨重
[2137] = function(idx)
	--[PathNpc,万里杀先锋,10004,1656]
	if idx == 2 then
		CALL("任务点对话",10102140,"万里杀先锋",2137,2,10004,1656)
	end
end,
--一鼓作气-针锋相对
[2138] = function(idx)
	--[PathNpc,神威伤员,10004,1654]
	if idx == 3 then
		CALL("任务点对话",10102138,"神威伤员",2138,3,10004,1654)
	end
	--[PathNpc,万里杀伤员,10004,1655]
	if idx == 4 then
		CALL("任务点对话",10102139,"万里杀伤员",2138,4,10004,1655)
	end
end,
--一鼓作气-首当其冲
[2139] = function(idx)
	--[PathMon,余辉,10004,1682]
	if idx == 5 then
		local monsters = {"余辉","白义"}
		CALL("任务点杀怪",monsters,10004,1685)
	end
	--[PathMon,白义,10004,1685]
	if idx == 6 then
		local monsters = {"白义"}
		CALL("任务点杀怪",monsters,10004,1685)
	end
end,
--再接再厉-落花流水
[2141] = function(idx)
	--[PathMon,尖兵队长,10004,101152]
	if idx == 0 then
		local monsters = {"尖兵队长","殿后尖兵"}
		CALL("任务点杀怪",monsters,10004,101433)
	end
	--[PathMon,殿后尖兵,10004,101433]
	if idx == 1 then
		local monsters = {"殿后尖兵"}
		CALL("任务点杀怪",monsters,10004,101433)
	end
end,
--再接再厉-挡者披靡
[2142] = function(idx)
	--[PathMon,护卫高手,10004,1707]
	if idx == 2 then
		local monsters = {"护卫高手","贴身护卫"}
		CALL("任务点杀怪",monsters,10004,1709)
	end
	--[PathMon,贴身护卫,10004,1709]
	if idx == 3 then
		local monsters = {"贴身护卫"}
		CALL("任务点杀怪",monsters,10004,1709)
	end
end,
--狡兔三窟-冲破阻碍
[2146] = function(idx)
	--寻找[PathNpc,杜云松,10004,301073]
	if idx == 0 then
		CALL("任务点移动",10004,301073)
	end
end,
--狡兔三窟-首恶现身
[2147] = function(idx)
	--[PathMon,杜云松,10004,1688]
	if idx == 1 then
		local monsters = {"杜云松"}
		CALL("任务点杀怪",monsters,10004,1688)
	end
end,
--千里奔袭-扑朔迷离
[2151] = function(idx)
	--[PathNpc,黄元文,10004,1628]
	if idx == 0 then
		CALL("任务点对话",10102135,"黄元文",2151,0,10004,1628)
	end
end,
--千里奔袭-亡羊补牢
[2152] = function(idx)
	--返回[PathNpc,千里营,10004,301067]
	if idx == 1 then
		CALL("任务点移动",10004,301067)
	end
end,

--------661,龙首成谜追渊源--------
--辞别故友-明月无心
[20400] = function(idx)
	--[PathNpc,燕南飞,10013,642]
	if idx == 0 then
		CALL("任务点对话",10109135,"燕南飞",20400,0,10013,642)
	end
end,
--辞别故友-徒增伤感
[20401] = function(idx)
	--[PathNpc,傅红雪,10013,91]
	if idx == 1 then
		CALL("任务点对话",10109037,"傅红雪",20401,1,10013,91)
	end
end,
--初入汴京-飞霞野渡
[20402] = function(idx)
	--到达[PathArea,开封飞霞驿,10012,300006]
	if idx == 0 then
		CALL("任务点移动",10012,300006)
	end
end,
--初入汴京-天波故人
[20403] = function(idx)
	--[PathNpc,黄元文,10012,3]
	if idx == 1 then
		CALL("任务点对话",10112006,"黄元文",20403,1,10012,3)
	end
end,
--初入汴京-寻找悬眼
[20479] = function(idx)
	--[PathNpc,悬眼甲子,10012,1701]
	if idx == 2 then
		CALL("任务点对话",10112289,"悬眼甲子",20479,2,10012,1701)
	end
end,
--初入汴京-眼线众多
[20480] = function(idx)
	--[PathNpc,悬眼乙卯,10012,1702]
	if idx == 3 then
		CALL("任务功能对话",20480,10512015,"悬眼乙卯","[PathNpc,悬眼乙卯,10012,1702]",10012,1702)
	end
end,
--初入汴京-一路向前
[20481] = function(idx)
	--[PathNpc,悬眼丙辰,10012,1703]
	if idx == 4 then
		CALL("任务功能对话",20481,10512014,"悬眼丙辰","[PathNpc,悬眼丙辰,10012,1703]",10012,1703)
	end
end,
--初入汴京-悬眼所指
[20482] = function(idx)
	--[PathNpc,悬眼丁亥,10012,1704]
	if idx == 5 then
		CALL("任务功能对话",20482,10512013,"悬眼丁亥","[PathNpc,悬眼丁亥,10012,1704]",10012,1704)
	end
end,
--河岸小镇-村口问路
[20483] = function(idx)
	--[PathNpc,路边书生,10012,1812]
	if idx == 0 then
		CALL("任务点对话",10112305,"路边书生",20483,0,10012,1812)
	end
end,
--河岸小镇-举手之劳
[20484] = function(idx)
	--[PathEnt,翠一品,10012,40026]
	if idx == 1 then
		CALL("任务点采集","翠一品",10012,40026)
	end
end,
--河岸小镇-书生指路
[20485] = function(idx)
	--[PathNpc,朱肖肖,10012,1812]
	if idx == 2 then
		CALL("任务点对话",10112305,"朱肖肖",20485,2,10012,1812)
	end
end,
--小径遇刺
[19804] = function(idx)
	--赶往[PathArea,朱仙镇,10012,300007]
	if idx == 0 then
		CALL("任务点移动",10012,300007)
	end
end,
--暗潮之下-万里扬沙
[20404] = function(idx)
	--[PathNpc,离玉堂,10012,4]
	if idx == 0 then
		CALL("任务点对话",10112005,"离玉堂",20404,0,10012,4)
	end
end,
--暗潮之下-黑帮头目
[20405] = function(idx)
	--[PathNpc,陆十八,10012,5]
	if idx == 1 then
		CALL("任务点对话",10112010,"陆十八",20405,1,10012,5)
	end
end,
--情报买卖-狮子开口
[20406] = function(idx)
	--[PathNpc,离玉堂,10012,4]
	if idx == 0 then
		CALL("任务点对话",10112005,"离玉堂",20406,0,10012,4)
	end
end,
--情报买卖-审时度势
[20407] = function(idx)
	--[PathNpc,陆十八,10012,5]
	if idx == 1 then
		CALL("任务点对话",10112010,"陆十八",20407,1,10012,5)
	end
end,
--剪除耳目-乔装之人
[20408] = function(idx)
	--[PathMon,朱仙镇地痞,11511,401]
	if idx == 0 then
		local monsters = {"朱仙镇地痞","地痞头目"}
		CALL("任务点杀怪",monsters,11511,406)
	end
	--[PathMon,地痞头目,11511,406]
	if idx == 1 then
		local monsters = {"地痞头目"}
		CALL("任务点杀怪",monsters,11511,406)
	end
end,
--剪除耳目-香主真面
[20409] = function(idx)
	--[PathMon,千机子,11511,426]
	if idx == 2 then
		local monsters = {"千机子"}
		CALL("任务点杀怪",monsters,11511,426)
	end
end,
--西水关外-速回报信
[20410] = function(idx)
	--[PathNpc,离玉堂,10012,4]
	if idx == 5 then
		CALL("任务功能对话",20410,10512008,"离玉堂","[PathNpc,离玉堂,10012,4]",10012,4)
	end
end,
--西水关外-前往汴京
[20411] = function(idx)
	--[PathArea,离开朱仙镇,10012,300482]
	if idx == 6 then
		CALL("任务点移动",10012,300482)
	end
end,
--西水关外-霜堂阻路
[20412] = function(idx)
	--[PathMon,龙尾帮众,10012,100145]
	if idx == 0 then
		local monsters = {"龙尾帮众","龙尾刺客","龙尾精锐"}
		CALL("任务点杀怪",monsters,10012,100021)
	end
	--[PathMon,龙尾刺客,10012,100139]
	if idx == 1 then
		local monsters = {"龙尾刺客","龙尾精锐"}
		CALL("任务点杀怪",monsters,10012,100021)
	end
	--[PathMon,龙尾精锐,10012,100021]
	if idx == 2 then
		local monsters = {"龙尾精锐"}
		CALL("任务点杀怪",monsters,10012,100021)
	end
end,
--西水关外-关外恶战
[20476] = function(idx)
	--[PathMon,龙爪精锐,10012,100041]
	if idx == 3 then
		local monsters = {"龙爪精锐","龙爪刺客"}
		CALL("任务点杀怪",monsters,10012,100038)
	end
	--[PathMon,龙爪刺客,10012,100038]
	if idx == 4 then
		local monsters = {"龙爪刺客"}
		CALL("任务点杀怪",monsters,10012,100038)
	end
end,
--东京梦华-繁华之都
[20413] = function(idx)
	--[PathArea,开封西门,10012,300008]
	if idx == 0 then
		CALL("任务点移动",10012,300008)
	end
end,
--东京梦华-古怪委托
[20414] = function(idx)
	--[PathNpc,包袱,10012,300008]
	if idx == 1 then
		LogWarning("未知的任务处理:%s","[PathNpc,包袱,10012,300008]")
	end
end,
--江湖有情-手足之情
[20415] = function(idx)
	--[PathNpc,陆山川,10012,6]
	if idx == 0 then
		CALL("任务点对话",10112015,"陆山川",20415,0,10012,6)
	end
end,
--江湖有情-孝子探亲
[20416] = function(idx)
	--[PathNpc,陆大娘,10012,8]
	if idx == 1 then
		CALL("任务点对话",10112016,"陆大娘",20416,1,10012,8)
	end
end,

--------64,龙雀长吟起风云--------
--整装待发-汇报情况
[2201] = function(idx)
	--[PathNpc,燕南飞,10004,1602]
	if idx == 0 then
		CALL("任务点对话",10102126,"燕南飞",2201,0,10004,1602)
	end
end,
--整装待发-神威堡主
[2202] = function(idx)
	--[PathNpc,韩学信,10004,1601]
	if idx == 3 then
		CALL("任务点对话",10102125,"韩学信",2202,3,10004,1601)
	end
end,
--整装待发-休整小憩
[2203] = function(idx)
	--休息整顿片刻
	if idx == 1 then
		CALL("等待任务完成",2203)
	end
end,
--整装待发-再启行程
[2204] = function(idx)
	--前往[PathNpc,奔马堂,10004,301074]
	if idx == 2 then
		CALL("任务点移动",10004,301074)
	end
end,
--整装待发-神行太保
[2214] = function(idx)
	--[PathNpc,黄元文,10004,1721]
	if idx == 4 then
		CALL("任务点对话",10102142,"黄元文",2214,4,10004,1721)
	end
end,
--万马齐喑-清除外围
[2206] = function(idx)
	--[PathMon,巡逻队长,10004,101178]
	if idx == 0 then
		local monsters = {"巡逻队长","巡逻手"}
		CALL("任务点杀怪",monsters,10004,101163)
	end
	--[PathMon,巡逻手,10004,101163]
	if idx == 1 then
		local monsters = {"巡逻手"}
		CALL("任务点杀怪",monsters,10004,101163)
	end
end,
--万马齐喑-奔马四骏
[2207] = function(idx)
	--[PathMon,龙文,10004,1738]
	if idx == 5 then
		local monsters = {"龙文"}
		CALL("任务点杀怪",monsters,10004,1738)
	end
end,
--万马齐喑-一鼓作气
[2208] = function(idx)
	--[PathMon,奔马精锐,10004,101220]
	if idx == 2 then
		local monsters = {"奔马精锐","奔马死士","奔马分堂主"}
		CALL("任务点杀怪",monsters,10004,101221)
	end
	--[PathMon,奔马死士,10004,101207]
	if idx == 3 then
		local monsters = {"奔马死士","奔马分堂主"}
		CALL("任务点杀怪",monsters,10004,101221)
	end
	--[PathMon,奔马分堂主,10004,101221]
	if idx == 4 then
		local monsters = {"奔马分堂主"}
		CALL("任务点杀怪",monsters,10004,101221)
	end
end,
--万马齐喑-诛灭三骏
[2209] = function(idx)
	--[PathMon,蒲稍,10004,1748]
	if idx == 6 then
		local monsters = {"蒲稍","余目","汉血"}
		CALL("任务点杀怪",monsters,10004,1758)
	end
	--[PathMon,余目,10004,1755]
	if idx == 7 then
		local monsters = {"余目","汉血"}
		CALL("任务点杀怪",monsters,10004,1758)
	end
	--[PathMon,汉血,10004,1758]
	if idx == 8 then
		local monsters = {"汉血"}
		CALL("任务点杀怪",monsters,10004,1758)
	end
end,
--万马齐喑-神武门主
[2224] = function(idx)
	--[PathMon,杜云松,10004,1747]
	if idx == 9 then
		local monsters = {"杜云松"}
		CALL("任务点杀怪",monsters,10004,1747)
	end
end,
--神秘书信-徒劳无功
[2211] = function(idx)
	--[PathNpc,黄元文,10004,1721]
	if idx == 0 then
		CALL("任务点对话",10102142,"黄元文",2211,0,10004,1721)
	end
end,
--神秘书信-神秘书信
[2212] = function(idx)
	--[PathNpc,边陲小镇,10004,301075]
	if idx == 1 then
		CALL("任务点移动",10004,301075)
	end
end,
--神秘书信-真相在此
[2213] = function(idx)
	--[PathNpc,丁钱重,10004,1763]
	if idx == 2 then
		CALL("任务点对话",10102145,"丁钱重",2213,2,10004,1763)
	end
end,
--苍梧迷城-苍梧之城
[2216] = function(idx)
	--[PathNpc,马行空,10004,999]
	if idx == 0 then
		CALL("任务点对话",10102073,"马行空",2216,0,10004,999)
	end
	--[PathNpc,总镖头,10004,968]
	if idx == 1 then
		CALL("任务点对话",10102070,"总镖头",2216,1,10004,968)
	end
end,
--苍梧迷城-耐心等待
[2217] = function(idx)
	--再耐心等待一会儿
	if idx == 2 then
		CALL("等待任务完成",2217)
	end
end,
--苍梧迷城-迷城地图
[2218] = function(idx)
	--[PathNpc,丁钱重,10004,1763]
	if idx == 3 then
		CALL("任务点对话",10102145,"丁钱重",2218,3,10004,1763)
	end
end,
--苍梧迷城-进军迷城
[2219] = function(idx)
	--前往[PathNpc,苍梧城,10004,301076]
	if idx == 4 then
		CALL("任务点移动",10004,301076)
	end
end,
--天王双刀-围攻苍梧
[2221] = function(idx)
	--[PathNpc,离玉堂,10004,1772]
	if idx == 0 then
		CALL("任务点对话",10102146,"离玉堂",2221,0,10004,1772)
	end
end,
--天王双刀-双鬼拍门
[2222] = function(idx)
	--[PathMon,苗天王,10004,1770]
	if idx == 1 then
		local monsters = {"苗天王","苗斩鬼"}
		CALL("任务点杀怪",monsters,10004,1768)
	end
	--[PathMon,苗斩鬼,10004,1768]
	if idx == 2 then
		local monsters = {"苗斩鬼"}
		CALL("任务点杀怪",monsters,10004,1768)
	end
end,
--天王双刀-长驱直入
[2223] = function(idx)
	--杀进[PathNpc,苍梧城,10004,301077]
	if idx == 3 then
		CALL("任务点移动",10004,301077)
	end
end,
--披荆斩棘-青龙探爪
[2226] = function(idx)
	--[PathMon,青龙鳞,10004,101261]
	if idx == 0 then
		local monsters = {"青龙鳞","青龙爪"}
		CALL("任务点杀怪",monsters,10004,101240)
	end
	--[PathMon,青龙爪,10004,101240]
	if idx == 1 then
		local monsters = {"青龙爪"}
		CALL("任务点杀怪",monsters,10004,101240)
	end
end,
--披荆斩棘-举步维艰
[2227] = function(idx)
	--[PathMon,青龙牙,10004,101291]
	if idx == 2 then
		local monsters = {"青龙牙","青龙角"}
		CALL("任务点杀怪",monsters,10004,101266)
	end
	--[PathMon,青龙角,10004,101266]
	if idx == 3 then
		local monsters = {"青龙角"}
		CALL("任务点杀怪",monsters,10004,101266)
	end
end,
--披荆斩棘-精英敌众
[2228] = function(idx)
	--[PathMon,潜堂煞星,10004,1799]
	if idx == 4 then
		local monsters = {"潜堂煞星","潜堂队长"}
		CALL("任务点杀怪",monsters,10004,1778)
	end
	--[PathMon,潜堂队长,10004,1778]
	if idx == 5 then
		local monsters = {"潜堂队长"}
		CALL("任务点杀怪",monsters,10004,1778)
	end
end,
--阴险二人
[2230] = function(idx)
	--冲进[PathNpc,苍梧内城,10004,301078]
	if idx == 0 then
		CALL("任务功能对话",2230,15012,"苍梧内城","冲进[PathNpc,苍梧内城,10004,301078]",10004,301078)
	end
end,
--何去何从-离开苍梧
[2236] = function(idx)
	--离开[PathNpc,苍梧城,10004,301076]
	if idx == 0 then
		CALL("任务点移动",10004,301076)
	end
end,
--何去何从-谆谆教导
[2237] = function(idx)
	--[PathNpc,离玉堂,10004,1772]
	if idx == 3 then
		CALL("任务点对话",10102146,"离玉堂",2237,3,10004,1772)
	end
end,
--何去何从-去见前辈
[2238] = function(idx)
	--[PathNpc,丁钱重,10004,1763]
	if idx == 1 then
		CALL("任务点对话",10102145,"丁钱重",2238,1,10004,1763)
	end
end,
--何去何从-再回绝尘
[2239] = function(idx)
	--[PathNpc,李保正,10004,1562]
	if idx == 4 then
		CALL("任务点对话",10102088,"李保正",2239,4,10004,1562)
	end
	--[PathNpc,夏玺,10004,1911]
	if idx == 5 then
		CALL("任务点对话",10102156,"夏玺",2239,5,10004,1911)
	end
	--[PathNpc,李秉,10004,1563]
	if idx == 7 then
		CALL("任务点对话",10102089,"李秉",2239,7,10004,1563)
	end
	--[PathNpc,慕容敏,10004,1571]
	if idx == 8 then
		CALL("任务点对话",10102095,"慕容敏",2239,8,10004,1571)
	end
end,
--何去何从-神威龙雀
[2242] = function(idx)
	--[PathNpc,韩学信,10004,192]
	if idx == 2 then
		CALL("任务点对话",10102011,"韩学信",2242,2,10004,192)
	end
end,
--何去何从-女儿心思
[2243] = function(idx)
	--[PathNpc,韩莹莹,10004,191]
	if idx == 6 then
		CALL("任务点对话",10102015,"韩莹莹",2243,6,10004,191)
	end
end,
--剑试蔷薇-天涯明月
[2241] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--万里扬尘-打探消息
[2246] = function(idx)
	--找[PathNpc,离玉堂,10004,1555]
	if idx == 0 then
		CALL("任务点对话",10102085,"离玉堂",2246,0,10004,1555)
	end
end,

--------900341,??--------
--江湖试炼-求教
[10702] = function(idx)
	--对话：[PathNpc,于百石,10010,9991]
	if idx == 0 then
		CALL("任务点对话",10104089,"于百石",10702,0,10010,9991)
	end
end,
--江湖试炼-求教
[10704] = function(idx)
	--对话：[PathNpc,于百石,10010,9991]
	if idx == 0 then
		CALL("任务点对话",10104089,"于百石",10704,0,10010,9991)
		--[4064] msk_td->send-1bef:16:D0290000 3F070000 0000D202 00000000
	end
end,

--江湖试炼-试炼
[10705] = function(idx)
	--通关杭州：[PathNPC,于百石,10010,9991]普通试炼
	if idx == 1 then
		CALL("任务点普通试炼","于百石",10010,9991,0x24)
	end
end,

--------662,黑云遮天藏旧案--------
--客栈奇遇
[19811] = function(idx)
	--[PathNpc,偷听,10012,300009]
	if idx == 0 then
		CALL("任务功能对话",19811,16401,"偷听","[PathNpc,偷听,10012,300009]",10012,300009)
	end
end,
--暗中调查-询问小二
[20419] = function(idx)
	--[PathNpc,店小二,10012,7]
	if idx == 0 then
		CALL("任务点对话",10112017,"店小二",20419,0,10012,7)
	end
end,
--暗中调查-搜寻罪证
[20420] = function(idx)
	--[PathArea,上锁的箱子,10012,40001]
	if idx == 1 then
		CALL("任务功能对话",20420,10512009,"上锁的箱子","[PathArea,上锁的箱子,10012,40001]",10012,40001)
	end
end,
--天波府内
[19813] = function(idx)
	--[PathNpc,杨延玉,10012,9]
	if idx == 0 then
		CALL("任务点对话",10112004,"杨延玉",19813,0,10012,9)
	end
end,
--青龙所谋
[19814] = function(idx)
	--[PathNpc,杨延玉,10012,9]
	if idx == 0 then
		CALL("任务功能对话",19814,16402,"杨延玉","[PathNpc,杨延玉,10012,9]",10012,9)
	end
end,
--黑街名号-知己知彼
[20422] = function(idx)
	--[PathNpc,笑道人,10012,10]
	if idx == 0 then
		CALL("任务点对话",10112003,"笑道人",20422,0,10012,10)
	end
end,
--黑街名号-意外相逢
[20423] = function(idx)
	--[PathNpc,唐青枫,10012,305]
	if idx == 1 then
		CALL("任务点对话",10112002,"唐青枫",20423,1,10012,305)
	end
end,
--黑街名号-引路之人
[20491] = function(idx)
	--[PathArea,丰乐楼,10012,300012]
	if idx == 2 then
		CALL("任务点移动",10012,300012)
	end
end,
--四处走动-闻者噤声
[20424] = function(idx)
	--[PathNpc,伙计,10012,12]
	if idx == 0 then
		CALL("任务点对话",10112020,"伙计",20424,0,10012,12)
	end
	--[PathNpc,账房先生,10012,11]
	if idx == 1 then
		CALL("任务点对话",10112021,"账房先生",20424,1,10012,11)
	end
end,
--四处走动-胆大之人
[20425] = function(idx)
	--[PathNpc,布布,10012,13]
	if idx == 2 then
		CALL("任务点对话",10112022,"布布",20425,2,10012,13)
	end
end,
--四处走动-东巷美人
[20426] = function(idx)
	--[PathNpc,兰美人,10012,14]
	if idx == 3 then
		CALL("任务点对话",10112018,"兰美人",20426,3,10012,14)
	end
end,
--美人要求-兰芷芬芳
[20427] = function(idx)
	--[PathEnt,芝兰仙,10012,40002]
	if idx == 0 then
		CALL("任务点采集","芝兰仙",10012,40002)
	end
	--[PathEnt,伽南香,10012,40003]
	if idx == 1 then
		CALL("任务点采集","伽南香",10012,40003)
	end
end,
--美人要求-珍藏宝物
[20428] = function(idx)
	--[PathNpc,齐总管,10012,15]
	if idx == 2 then
		CALL("任务功能对话",20428,10512010,"齐总管","[PathNpc,齐总管,10012,15]",10012,15)
	end
end,
--美人要求-燃香为引
[20429] = function(idx)
	--[PathArea,香炉,10012,300487]
	if idx == 3 then
		CALL("任务点采集","香炉",10012,300487)
	end
end,
--姐妹情深-婕妤召见
[20430] = function(idx)
	--[PathNpc,韩婕妤,10012,20]
	if idx == 0 then
		CALL("任务点对话",10112019,"韩婕妤",20430,0,10012,20)
	end
end,
--姐妹情深-婕妤之请
[20431] = function(idx)
	--[PathEnt,木箱,10012,40013]
	if idx == 1 then
		CALL("任务点采集","木箱",10012,40013)
	end
end,
--姐妹情深-烟火传情
[20432] = function(idx)
	--[PathArea,荷花池边,10012,300477]
	if idx == 2 then
		LogWarning("未知的任务处理:%s","[PathArea,荷花池边,10012,300477]")
	end
end,
--真人露相
[19819] = function(idx)
	--[PathArea,擂台,10012,300013]
	if idx == 0 then
		CALL("任务点移动",10012,300013)
	end
end,
--龙潭虎穴
[19820] = function(idx)
	--[PathNpc,吴振英,10012,856]
	if idx == 0 then
		CALL("任务功能对话",19820,16404,"吴振英","[PathNpc,吴振英,10012,856]",10012,856)
	end
end,

--------1061,太极三清迎开元--------
--归山寒暄-门内寒暄
[31200] = function(idx)
	--[PathNpc,凌玄,10007,81]
	if idx == 0 then
		CALL("任务点对话",10118047,"凌玄",31200,0,10007,81)
	end
end,
--归山寒暄-太极道场
[31242] = function(idx)
	--[PathArea,太极道场,10007,300044]
	if idx == 3 then
		CALL("任务点移动",10007,300044)
	end
end,
--归山寒暄-师兄师弟
[31237] = function(idx)
	--[PathNpc,潇湘子,10007,195]
	if idx == 1 then
		CALL("任务点对话",10118056,"潇湘子",31237,1,10007,195)
	end
end,
--归山寒暄-严厉师叔
[31201] = function(idx)
	--[PathNpc,一云子,10007,3]
	if idx == 2 then
		CALL("任务点对话",10118015,"一云子",31201,2,10007,3)
	end
end,
--归山寒暄-拜见师尊
[31250] = function(idx)
	--[PathNpc,一云子,10007,3]
	if idx == 4 then
		CALL("任务功能对话",31250,16307,"一云子","[PathNpc,一云子,10007,3]",10007,3)
	end
end,
--太极三清-回禀师尊
[31203] = function(idx)
	--[PathNpc,张梦白,10007,2]
	if idx == 0 then
		CALL("任务点对话",10118013,"张梦白",31203,0,10007,2)
	end
end,
--太极三清-真武剑道
[31248] = function(idx)
	--[PathNpc,张梦白,10007,2]
	if idx == 1 then
		CALL("任务点对话",10118013,"张梦白",31248,1,10007,2)
	end
end,
--太极三清-悟剑之法
[31204] = function(idx)
	--[PathNPCLayer,笑道人,10007,1,1]
	if idx == 2 then
		--桥上?
		CALL("任务点对话",10118001,"笑道人",31204,2,10007,1,1)
	end
end,
--添置新衣
[30603] = function(idx)
	--[PathNpc,萧萧,10007,4]
	if idx == 0 then
		CALL("任务点对话",10118034,"萧萧",30603,0,10007,4)
	end
end,
--循序渐进
[30604] = function(idx)
	--[PathNPCLayer,广宁子,10007,83,1]
	if idx == 0 then
		--桥上?
		CALL("任务点对话",10118043,"广宁子",30604,0,10007,83,1)
	end
end,
--开元真意-巡视道场
[31243] = function(idx)
	--[PathNPCLayer,巡视道场,10007,300423,1]
	if idx == 1 then
		CALL("任务点移动",10007,300423,1)
		--CALL("坐标点移动",300423,1,10007)
	end
end,
--开元真意-珠帘洞内
[31241] = function(idx)
	--[PathArea,真武道场,10007,300008]
	if idx == 0 then
		CALL("任务点移动",10007,300008)
	end
end,
--继续巡视
[30606] = function(idx)
	--[PathArea,道场,10007,300009]
	if idx == 0 then
		CALL("任务点移动",10007,300009)
	end
end,
--灵犀一动
[30607] = function(idx)
	--在道场上打坐调息。
	if idx == 0 then
		CALL("等待任务完成",30607)
	end
end,
--再遇蔷薇
[30608] = function(idx)
	--[PathNpc,燕南飞,10007,7]
	if idx == 0 then
		CALL("任务点对话",10118005,"燕南飞",30608,0,10007,7)
	end
end,
--云海有悟
[30609] = function(idx)
	--[PathNpc,燕南飞,10007,7]
	if idx == 0 then
		CALL("任务功能对话",30609,16306,"燕南飞","[PathNpc,燕南飞,10007,7]",10007,7)
	end
end,
--书海无涯
[30610] = function(idx)
	--[PathArea,山海楼,10007,300012]
	if idx == 0 then
		CALL("任务点移动",10007,300012)
	end
end,

--------663,拨开云雾现真颜--------
--另寄希望-开封师爷
[20462] = function(idx)
	--[PathNpc,师爷 黄佳,10012,1065]
	if idx == 2 then
		CALL("任务点对话",10112266,"师爷 黄佳",20462,2,10012,1065)
	end
end,
--另寄希望-开封总捕
[20463] = function(idx)
	--[PathNpc,开封总捕 左苍义,10012,1798]
	if idx == 3 then
		CALL("任务点对话",10112304,"开封总捕 左苍义",20463,3,10012,1798)
	end
end,
--另寄希望-互通消息
[20486] = function(idx)
	--[PathNpc,黄元文,10012,57]
	if idx == 0 then
		CALL("任务点对话",10112094,"黄元文",20486,0,10012,57)
	end
end,
--另寄希望-往居士林
[20487] = function(idx)
	--[PathArea,百鸟林,10012,300016]
	if idx == 1 then
		CALL("任务点移动",10012,300016)
	end
end,
--百鸟惊飞-如影随形
[20444] = function(idx)
	--[PathMon,霜堂赤鳞帮众,10012,100050]
	if idx == 0 then
		local monsters = {"霜堂赤鳞帮众","霜堂赤鳞先锋","霜堂赤鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100052)
	end
	--[PathMon,霜堂赤鳞先锋,10012,100149]
	if idx == 1 then
		local monsters = {"霜堂赤鳞先锋","霜堂赤鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100052)
	end
	--[PathMon,霜堂赤鳞精锐,10012,100052]
	if idx == 2 then
		local monsters = {"霜堂赤鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100052)
	end
end,
--百鸟惊飞-沿路鏖战
[20445] = function(idx)
	--[PathMon,霜堂金鳞帮众,10012,100068]
	if idx == 3 then
		local monsters = {"霜堂金鳞帮众","霜堂金鳞先锋","霜堂金鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100078)
	end
	--[PathMon,霜堂金鳞先锋,10012,100080]
	if idx == 4 then
		local monsters = {"霜堂金鳞先锋","霜堂金鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100078)
	end
	--[PathMon,霜堂金鳞精锐,10012,100078]
	if idx == 5 then
		local monsters = {"霜堂金鳞精锐"}
		CALL("任务点杀怪",monsters,10012,100078)
	end
end,
--霜堂堂主
[19823] = function(idx)
	--[PathMon,霜堂堂主,10012,304]
	if idx == 0 then
		local monsters = {"霜堂堂主"}
		CALL("任务点杀怪",monsters,10012,304)
	end
end,
--上山之路-再会燕子
[20464] = function(idx)
	--[PathNpc,燕南飞,10012,54]
	if idx == 0 then
		CALL("任务点对话",10112001,"燕南飞",20464,0,10012,54)
	end
end,
--上山之路-询问悬眼
[20465] = function(idx)
	--[PathNpc,悬眼壬午,10012,55]
	if idx == 1 then
		CALL("任务点对话",10112095,"悬眼壬午",20465,1,10012,55)
	end
end,
--未竟之事-运功疗伤
[20466] = function(idx)
	--[PathEnt,稍作观察,10012,40006]
	if idx == 1 then
		CALL("等待任务完成",20466)
	end
end,
--未竟之事-抹去标记
[20467] = function(idx)
	--[PathEnt,奇怪树丫,10012,40010]
	if idx == 0 then
		CALL("任务点采集","奇怪树丫",10012,40010)
	end
end,
--未竟之事-上罗汉寨
[20488] = function(idx)
	--[PathEnt,罗汉寨,10012,300014]
	if idx == 2 then
		LogWarning("未知的任务处理:%s","[PathEnt,罗汉寨,10012,300014]")
	end
end,
--居士林内-守备森严
[20468] = function(idx)
	--[PathMon,罗汉寨守卫,10012,100091]
	if idx == 0 then
		local monsters = {"罗汉寨守卫","罗汉寨巡守"}
		CALL("任务点杀怪",monsters,10012,100091)
	end
	--[PathMon,罗汉寨巡守,10012,100091]
	if idx == 1 then
		local monsters = {"罗汉寨巡守"}
		CALL("任务点杀怪",monsters,10012,100091)
	end
end,
--居士林内-恶僧拦阻
[20469] = function(idx)
	--[PathMon,居士林僧侣,10012,100105]
	if idx == 2 then
		local monsters = {"居士林僧侣","居士林高僧","居士林武僧"}
		CALL("任务点杀怪",monsters,10012,100098)
	end
	--[PathMon,居士林高僧,10012,100114]
	if idx == 3 then
		local monsters = {"居士林高僧","居士林武僧"}
		CALL("任务点杀怪",monsters,10012,100098)
	end
	--[PathMon,居士林武僧,10012,100098]
	if idx == 4 then
		local monsters = {"居士林武僧"}
		CALL("任务点杀怪",monsters,10012,100098)
	end
end,
--叫阵寨中
[19827] = function(idx)
	--[PathEnt,居士林寨旗,10012,295]
	if idx == 0 then
		CALL("任务点采集","居士林寨旗",10012,295)
	end
end,
--拨开云雾
[19828] = function(idx)
	--[PathNpc,邓心禅,10012,294]
	if idx == 0 then
		CALL("任务点对话",10112011,"邓心禅",19828,0,10012,294)
	end
end,
--事在人为-尽力一试
[20470] = function(idx)
	--[PathNpc,邓心禅,10012,294]
	if idx == 0 then
		CALL("任务功能对话",20470,16406,"邓心禅","[PathNpc,邓心禅,10012,294]",10012,294)
	end
end,
--事在人为-终开金口
[20471] = function(idx)
	--[PathNpc,邓心禅,10012,400]
	if idx == 1 then
		CALL("任务点对话",10112141,"邓心禅",20471,1,10012,400)
	end
end,
--重返开封
[19830] = function(idx)
	--[PathNpc,燕南飞,10012,56]
	if idx == 0 then
		CALL("任务点对话",10112097,"燕南飞",19830,0,10012,56)
	end
end,

--------1062,青龙弄潮起风云--------
--风起云涌
[30611] = function(idx)
	--[PathNpc,一云子,10007,3]
	if idx == 0 then
		CALL("任务点对话",10118015,"一云子",30611,0,10007,3)
	end
end,
--贼人身份
[30612] = function(idx)
	--[PathNpc,一云子,10007,3]
	if idx == 0 then
		CALL("任务点对话",10118015,"一云子",30612,0,10007,3)
	end
end,
--天地不仁
[30613] = function(idx)
	--[PathArea,太极道场,10007,300013]
	if idx == 0 then
		CALL("任务点移动",10007,300013)
	end
end,
--同门之谊-暴躁师弟
[31209] = function(idx)
	--[PathNpc,凌玄,10007,51]
	if idx == 0 then
		CALL("任务点对话",10118011,"凌玄",31209,0,10007,51)
	end
end,
--同门之谊-妙手仙姬
[31210] = function(idx)
	--[PathNpc,姬灵玉,10007,50]
	if idx == 1 then
		CALL("任务点对话",10118021,"姬灵玉",31210,1,10007,50)
	end
end,
--同门之谊-剑拔弩张
[31211] = function(idx)
	--[PathNpc,凌玄,10007,51]
	if idx == 2 then
		CALL("任务点对话",10118011,"凌玄",31211,2,10007,51)
	end
end,
--无德之人-嚣张道长
[31235] = function(idx)
	--[PathMon,幽隐道长,13704,100040]
	if idx == 1 then
		local monsters = {"幽隐道长","幽隐道童"}
		CALL("任务点杀怪",monsters,13704,100134)
	end
	--[PathMon,幽隐道童,13704,100134]
	if idx == 2 then
		local monsters = {"幽隐道童"}
		CALL("任务点杀怪",monsters,13704,100134)
	end
end,
--无德之人-山中异草
[31236] = function(idx)
	--[PathEnt,血见愁,10007,40117]
	if idx == 0 then
		CALL("任务点采集","血见愁",10007,40117)
	end
end,
--尽心尽力-素笔丹青
[31212] = function(idx)
	--[PathNpc,丹青子,10007,214]
	if idx == 0 then
		CALL("任务点对话",10118058,"丹青子",31212,0,10007,214)
	end
end,
--尽心尽力-山中异草
[31213] = function(idx)
	--[PathNpc,仙鹤草,10007,40139]
	if idx == 1 then
		CALL("任务点采集","仙鹤草",10007,40139)
	end
end,
--尽心尽力-呦呦鹿鸣
[31244] = function(idx)
	--[PathNpc,乐乐,10007,462]
	if idx == 3 then
		CALL("任务点使用物品","仙鹤草",10007,462,"乐乐")
		--LogWarning("未知的任务处理:%s","[PathNpc,乐乐,10007,462]")
	end
end,
--尽心尽力-疗伤丹药
[31249] = function(idx)
	--[PathNpc,丹青子,10007,214]
	if idx == 2 then
		CALL("任务点对话",10118058,"丹青子",31249,2,10007,214)
	end
end,
--剑缘自心
[30617] = function(idx)
	--[PathNpc,姬灵玉,10007,50]
	if idx == 0 then
		CALL("任务功能对话",30617,16303,"姬灵玉","[PathNpc,姬灵玉,10007,50]",10007,50)
	end
end,
--剑法玄妙-燕子心剑
[31214] = function(idx)
	--[PathNpc,燕南飞,10007,76]
	if idx == 0 then
		CALL("任务点对话",10118006,"燕南飞",31214,0,10007,76)
	end
end,
--剑法玄妙-剑中君子
[31215] = function(idx)
	--[PathArea,律令阁,10007,300448]
	if idx == 3 then
		CALL("任务点移动",10007,300448)
	end
end,
--剑法玄妙-节外生枝
[31216] = function(idx)
	--[PathNpc,笑道人,10007,72]
	if idx == 2 then
		CALL("任务点对话",10118002,"笑道人",31216,2,10007,72)
	end
end,
--剑法玄妙-情况紧急
[31245] = function(idx)
	--[PathNpc,寒湘子,10007,52]
	if idx == 1 then
		CALL("任务点对话",10118017,"寒湘子",31245,1,10007,52)
	end
end,
--山野寻踪-龟仙岭上
[31217] = function(idx)
	--[PathNpc,龟仙坪,10007,300014]
	if idx == 0 then
		CALL("任务点移动",10007,300014)
	end
end,
--山野寻踪-宵小同党
[31218] = function(idx)
	--[PathMon,神秘黑衣人,10007,100010]
	if idx == 1 then
		local monsters = {"神秘黑衣人","黑衣剑客"}
		CALL("任务点杀怪",monsters,10007,100010)
	end
	--[PathMon,黑衣剑客,10007,100010]
	if idx == 2 then
		local monsters = {"黑衣剑客"}
		CALL("任务点杀怪",monsters,10007,100010)
	end
end,
--青龙归堂-再战恶贼
[31219] = function(idx)
	--[PathMon,黑衣头领,10007,100014]
	if idx == 2 then
		local monsters = {"黑衣头领"}
		CALL("任务点杀怪",monsters,10007,100014)
	end
	--[PathNpc,身份之证,10007,100014]
	if idx == 0 then
		CALL("任务功能对话",31219,10518005,"身份之证","[PathNpc,身份之证,10007,100014]",10007,100014)
	end
end,
--青龙归堂-贼首身份
[31220] = function(idx)
	--[PathMon,查看令牌,10007,300033]
	if idx == 1 then
		CALL("任务点使用物品","青龙令牌",10007,300033)
		--LogWarning("未知的任务处理:%s","[PathMon,查看令牌,10007,300033]")
	end
end,

--------664,青龙弄潮为奇书--------
--镇国古寺
[19831] = function(idx)
	--[PathArea,相国寺,10012,300015]
	if idx == 0 then
		CALL("任务点移动",10012,300015)
	end
end,
--相国寺内-入寺礼仪
[20472] = function(idx)
	--[PathNpc,迎客僧,10012,29]
	if idx == 0 then
		CALL("任务功能对话",20472,10512006,"迎客僧","[PathNpc,迎客僧,10012,29]",10012,29)
	end
end,
--相国寺内-净手入寺
[20473] = function(idx)
	--[PathArea,净手,10012,300475]
	if idx == 1 then
		LogWarning("未知的任务处理:%s","[PathArea,净手,10012,300475]")
	end
end,
--红尘之外-悟法大师
[20454] = function(idx)
	--[PathNpc,悟法大师,10012,23]
	if idx == 0 then
		CALL("任务功能对话",20454,10512004,"悟法大师","[PathNpc,悟法大师,10012,23]",10012,23)
	end
end,
--红尘之外-焚香礼佛
[20455] = function(idx)
	--[PathArea,香烛,10012,300476]
	if idx == 1 then
		LogWarning("未知的任务处理:%s","[PathArea,香烛,10012,300476]")
	end
end,
--红尘之外-暂抛红尘
[20456] = function(idx)
	--[PathNpc,罗汉殿,10012,300488]
	if idx == 2 then
		CALL("任务点移动",10012,300488)
	end
end,
--红尘之外-静心凝神
[20474] = function(idx)
	--打坐一分钟。
	if idx == 3 then
		CALL("等待任务完成",20474)
	end
end,
--红尘难断-四大皆空
[20475] = function(idx)
	--[PathNpc,戒空大师,10012,24]
	if idx == 0 then
		CALL("任务点对话",10112008,"戒空大师",20475,0,10012,24)
	end
end,
--红尘难断-塔顶观景
[20477] = function(idx)
	--在塔顶俯瞰开封城。
	if idx == 1 then
		CALL("等待任务完成",20477)
	end
end,
--红尘难断-所见所闻
[20478] = function(idx)
	--[PathNpc,戒空大师,10012,24]
	if idx == 2 then
		CALL("任务点对话",10112008,"戒空大师",20478,2,10012,24)
	end
end,
--前尘过往
[19835] = function(idx)
	--[PathNpc,戒空大师,10012,24]
	if idx == 0 then
		CALL("任务功能对话",19835,16408,"戒空大师","[PathNpc,戒空大师,10012,24]",10012,24)
	end
end,
--龙首真面-一观画卷
[20460] = function(idx)
	--画卷
	if idx == 0 then
		LogWarning("未知的任务处理:%s","画卷")
	end
end,
--龙首真面-惊人发现
[20461] = function(idx)
	--[PathNpc,燕南飞,10012,56]
	if idx == 1 then
		CALL("任务点对话",10112097,"燕南飞",20461,1,10012,56)
	end
end,
--回禀将军
[19837] = function(idx)
	--[PathNpc,杨延玉,10012,9]
	if idx == 0 then
		CALL("任务功能对话",19837,16409,"杨延玉","[PathNpc,杨延玉,10012,9]",10012,9)
	end
end,
--奇书所在
[19838] = function(idx)
	--[PathArea,皇城,10012,300017]
	if idx == 0 then
		CALL("任务点移动",10012,300017)
	end
end,
--迟来一步
[19839] = function(idx)
	--[PathArea,护龙河岸,10012,300489]
	if idx == 0 then
		CALL("任务点移动",10012,300489)
	end
end,
--见证之人
[19840] = function(idx)
	--[PathNpc,唐青枫,10012,305]
	if idx == 0 then
		CALL("任务点对话",10112002,"唐青枫",19840,0,10012,305)
	end
	--[PathNpc,笑道人,10012,10]
	if idx == 1 then
		CALL("任务点对话",10112003,"笑道人",19840,1,10012,10)
	end
end,
--东京梦华
[19841] = function(idx)
	--等级提升至75
	if idx == 0 then
		CALL("等级限制",19841)
	end
end,

--------1063,道法自然事随风--------
--追踪线索-循迹而去
[31221] = function(idx)
	--[PathNpc,山道,10007,300015]
	if idx == 0 then
		CALL("任务点移动",10007,300015)
	end
end,
--追踪线索-贼人衣物
[31222] = function(idx)
	--[PathNpc,夜行衣,10007,461]
	if idx == 1 then
		CALL("任务点对话",10118068,"夜行衣",31222,1,10007,461)
	end
end,
--沿路盘问-江湖散客
[31223] = function(idx)
	--[PathNpc,赵都安,10007,66]
	if idx == 0 then
		CALL("任务点对话",10118046,"赵都安",31223,0,10007,66)
	end
end,
--沿路盘问-真武弟子
[31224] = function(idx)
	--[PathNpc,吴涯,10007,47]
	if idx == 1 then
		CALL("任务点对话",10118023,"吴涯",31224,1,10007,47)
	end
end,
--解救同门-青龙恶徒
[31225] = function(idx)
	--[PathMon,龙尾先锋·归堂,10007,100023]
	if idx == 0 then
		local monsters = {"龙尾先锋·归堂","龙尾帮众·归堂","龙尾精锐·归堂"}
		CALL("任务点杀怪",monsters,10007,100023)
	end
	--[PathMon,龙尾帮众·归堂,10007,100023]
	if idx == 1 then
		local monsters = {"龙尾帮众·归堂","龙尾精锐·归堂"}
		CALL("任务点杀怪",monsters,10007,100023)
	end
	--[PathMon,龙尾精锐·归堂,10007,100023]
	if idx == 2 then
		local monsters = {"龙尾精锐·归堂"}
		CALL("任务点杀怪",monsters,10007,100023)
	end
end,
--解救同门-化险为夷
[31226] = function(idx)
	--[PathNpc,陈有,10007,48]
	if idx == 3 then
		CALL("任务点对话",10118024,"陈有",31226,3,10007,48)
	end
end,
--终见贼人
[30624] = function(idx)
	--[PathArea,凌云道,10007,300016]
	if idx == 0 then
		CALL("任务点移动",10007,300016)
	end
end,
--回禀师兄
[30625] = function(idx)
	--[PathNpc,笑道人,10007,78]
	if idx == 0 then
		CALL("任务点对话",10118003,"笑道人",30625,0,10007,78)
	end
end,
--剑法进境-同门切磋
[31228] = function(idx)
	--[PathNpc,慕容明,10007,79]
	if idx == 0 then
		CALL("任务功能对话_战斗",31228,nil,"慕容明","[PathNpc,慕容明,10007,79]",10007,79,"慕容明")
	end
end,
--剑派分明-无痕剑意
[31230] = function(idx)
	--[PathNpc,独孤若虚,10007,49]
	if idx == 0 then
		CALL("任务点对话",10118029,"独孤若虚",31230,0,10007,49)
	end
end,
--剑派分明-和光同尘
[31231] = function(idx)
	--[PathNpc,笑道人,10007,78]
	if idx == 1 then
		CALL("任务点对话",10118003,"笑道人",31231,1,10007,78)
	end
end,
--真武之道
[30628] = function(idx)
	--[PathNpc,真武道场,10007,300421]
	if idx == 0 then
		CALL("任务功能对话",30628,16305,"真武道场","[PathNpc,真武道场,10007,300421]",10007,300421)
	end
end,
--天下四盟-掌门叮咛
[31232] = function(idx)
	--[PathNpc,张梦白,10007,2]
	if idx == 0 then
		CALL("任务点对话",10118013,"张梦白",31232,0,10007,2)
	end
end,
--天下四盟-拜别师兄
[31233] = function(idx)
	--[PathNPCLayer,笑道人,10007,1,1]
	if idx == 1 then
		--桥上?
		CALL("任务点对话",10118001,"笑道人",31233,1,10007,1,1)
	end
end,
--天下四盟-拜别师弟
[31234] = function(idx)
	--[PathNPCLayer,凌玄,10007,215,1]
	if idx == 2 then
		--桥上?
		CALL("任务点对话",10118012,"凌玄",31234,2,10007,215,1)
	end
end,
--西湖开屏
[30630] = function(idx)
	--[PathNpc,杭州城内,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,
--小心戒备-碧荷师姐
[31227] = function(idx)
	--[PathNpc,丁碧荷,10007,53]
	if idx == 0 then
		CALL("任务点对话",10118018,"丁碧荷",31227,0,10007,53)
	end
end,
--小心戒备-复兴师叔
[31238] = function(idx)
	--[PathNpc,龙复兴,10007,133]
	if idx == 1 then
		CALL("任务点对话",10118051,"龙复兴",31238,1,10007,133)
	end
end,
--小心戒备-寒松师弟
[31239] = function(idx)
	--[PathNpc,寒松子,10007,216]
	if idx == 2 then
		CALL("任务点对话",10118059,"寒松子",31239,2,10007,216)
	end
end,

--------71,第一回：神威倒卷破胡虏--------
--大漠沙场-铁枪周辉
[1829] = function(idx)
	--找[PathNpc,铁枪周辉,10004,1]
	if idx == 0 then
		CALL("任务点对话",10102001,"铁枪周辉",1829,0,10004,1)
	end
end,
--大漠沙场-沙场点兵
[1832] = function(idx)
	--[PathNpc,程锋飞,10004,3172]
	if idx == 1 then
		CALL("任务点对话",10102170,"程锋飞",1832,1,10004,3172)
	end
end,
--大漠沙场-龙虎兄弟
[1833] = function(idx)
	--[PathNpc,廖虎,10004,3171]
	if idx == 2 then
		CALL("任务点对话",10102169,"廖虎",1833,2,10004,3171)
	end
end,
--天刀大营
[1902] = function(idx)
	--找[PathNpc,大师兄廖龙,10004,2]
	if idx == 0 then
		CALL("任务点对话",10102002,"大师兄廖龙",1902,0,10004,2)
	end
end,
--门规森严-温习门规
[1852] = function(idx)
	--找[PathNpc,韩振天,10004,18]
	if idx == 0 then
		CALL("任务点对话",10102020,"韩振天",1852,0,10004,18)
	end
end,
--门规森严-有客到访
[1853] = function(idx)
	--[PathNpc,师姐周荣,10004,22]
	if idx == 1 then
		CALL("任务点对话",10102019,"师姐周荣",1853,1,10004,22)
	end
end,
--牛刀小试-击破士兵
[1805] = function(idx)
	--击破[PathMon,西夏士兵,10329,100007]
	if idx == 0 then
		local monsters = {"西夏士兵","西夏参将"}
		CALL("任务点杀怪",monsters,10329,100018)
	end
	--击破[PathMon,西夏参将,10329,100018]
	if idx == 1 then
		local monsters = {"西夏参将"}
		CALL("任务点杀怪",monsters,10329,100018)
	end
end,
--牛刀小试-寻找线索
[1806] = function(idx)
	--击破[PathMon,精锐士兵,10329,100040]
	if idx == 2 then
		local monsters = {"精锐士兵","步军参谋"}
		CALL("任务点杀怪",monsters,10329,100040)
	end
	--击破[PathMon,步军参谋,10329,100040]
	if idx == 3 then
		local monsters = {"步军参谋"}
		CALL("任务点杀怪",monsters,10329,100040)
	end
end,
--牛刀小试-强敌入侵
[1807] = function(idx)
	--找[PathNpc,凌飞,10004,8]汇报
	if idx == 4 then
		CALL("任务点对话",10102003,"凌飞",1807,4,10004,8)
	end
end,
--天波微澜-刺杀
[1808] = function(idx)
	--与[PathNpc,凌飞,10004,8]对话进入天刀营，
	if idx == 0 then
		CALL("任务功能对话",1808,15001,"凌飞","与[PathNpc,凌飞,10004,8]对话进入天刀营，",10004,8)
	end
end,
--天波微澜-毫发无损
[1809] = function(idx)
	--找[PathNpc,凌飞,10004,8]复命
	if idx == 1 then
		CALL("任务点对话",10102003,"凌飞",1809,1,10004,8)
	end
end,
--天波微澜-神威双姝
[1813] = function(idx)
	--去找[PathNpc,韩思思,10004,75]
	if idx == 2 then
		CALL("任务点对话",10102006,"韩思思",1813,2,10004,75)
	end
end,
--小心戒备-参见堡主
[1811] = function(idx)
	--参见堡主[PathNpc,韩学信,10004,192]
	if idx == 1 then
		CALL("任务点对话",10102011,"韩学信",1811,1,10004,192)
	end
end,
--小心戒备-传令弟子
[1812] = function(idx)
	--去找[PathNpc,传令弟子,10004,404]
	if idx == 0 then
		CALL("任务点对话",80005,"传令弟子",1812,0,10004,404)
	end
end,
--长枪生辉
[1931] = function(idx)
	--找[PathNpc,铁钟玉,10004,89]
	if idx == 0 then
		CALL("任务点对话",10102027,"铁钟玉",1931,0,10004,89)
	end
end,
--铁甲生寒
[1932] = function(idx)
	--找[PathNpc,路缁衣,10004,90]
	if idx == 0 then
		CALL("任务点对话",10102028,"路缁衣",1932,0,10004,90)
	end
end,
--敏而好学
[1933] = function(idx)
	--找[PathNpc,陆星河,10004,88]
	if idx == 0 then
		CALL("任务点对话",10102026,"陆星河",1933,0,10004,88)
	end
end,
--心有灵犀-调息打坐
[1854] = function(idx)
	--打坐一分钟
	if idx == 0 then
		CALL("等待任务完成",1854)
	end
end,
--心有灵犀-神威一派
[1855] = function(idx)
	--[PathNpc,周荣,10004,22]
	if idx == 1 then
		CALL("任务功能对话",1855,15013,"周荣","[PathNpc,周荣,10004,22]",10004,22)
	end
end,

--------72,第二回：地鞘拒敌斩夷蛮--------
--地鞘遭袭
[1909] = function(idx)
	--跟[PathNpc,韩思思,10004,75]学杀意技
	if idx == 0 then
		CALL("任务功能对话",1909,15002,"韩思思","跟[PathNpc,韩思思,10004,75]学杀意技",10004,75)
	end
end,
--追根问底
[1911] = function(idx)
	--找[PathNpc,韩振天,10004,267]
	if idx == 0 then
		CALL("任务点对话",10102008,"韩振天",1911,0,10004,267)
	end
end,
--白虎势危
[1912] = function(idx)
	--找[PathNpc,白虎岗哨兵,10004,76]
	if idx == 0 then
		CALL("任务点对话",10102009,"白虎岗哨兵",1912,0,10004,76)
	end
end,
--迎头痛击-横扫千军
[1835] = function(idx)
	--击败[PathMon,羽林枪兵,10004,100645]
	if idx == 0 then
		local monsters = {"羽林枪兵","羽林军伏兵","羽林军卫"}
		CALL("任务点杀怪",monsters,10004,100645)
	end
	--击败[PathMon,羽林军伏兵,10004,100645]
	if idx == 1 then
		local monsters = {"羽林军伏兵","羽林军卫"}
		CALL("任务点杀怪",monsters,10004,100645)
	end
	--击败[PathMon,羽林军卫,10004,100645]
	if idx == 2 then
		local monsters = {"羽林军卫"}
		CALL("任务点杀怪",monsters,10004,100645)
	end
end,
--偶现侠踪
[1916] = function(idx)
	--[PathNpc,孙望,10004,150]
	if idx == 0 then
		CALL("任务点对话",10102031,"孙望",1916,0,10004,150)
	end
end,
--又见飞刀
[1917] = function(idx)
	--与[PathNpc,孙望,10004,150]对话
	if idx == 0 then
		CALL("任务功能对话",1917,15003,"孙望","与[PathNpc,孙望,10004,150]对话",10004,150)
	end
end,
--小李传人
[1918] = function(idx)
	--向[PathNpc,叶开,10004,137]致谢
	if idx == 0 then
		CALL("任务点对话",10102010,"叶开",1918,0,10004,137)
	end
end,
--杀狐取图
[1919] = function(idx)
	--击败[PathMon,卫幕山,10004,843]
	if idx == 0 then
		local monsters = {"卫幕山","暗探精英"}
		CALL("任务点杀怪",monsters,10004,100694)
	end
	--击败[PathMon,暗探精英,10004,100694]
	if idx == 1 then
		local monsters = {"暗探精英"}
		CALL("任务点杀怪",monsters,10004,100694)
	end
end,
--拍马回营
[1920] = function(idx)
	--找[PathNpc,韩学信,10004,192]
	if idx == 0 then
		CALL("任务点对话",10102011,"韩学信",1920,0,10004,192)
	end
end,
--策马扬鞭-神威驿站
[19401] = function(idx)
	--去驿站找[PathNpc,神威驿卒,10004,687]
	if idx == 0 then
		CALL("任务点对话",10102060,"神威驿卒",19401,0,10004,687)
	end
end,
--策马扬鞭-围追堵截
[19402] = function(idx)
	--向[PathNpc,神威驿卒,10004,687]借马，之后策马甩开西夏骑兵的追击
	if idx == 1 then
		CALL("任务功能对话",19402,15007,"神威驿卒","向[PathNpc,神威驿卒,10004,687]借马，之后策马甩开西夏骑兵的追击",10004,687)
	end
end,
--寻找暗哨
[1941] = function(idx)
	--寻找失踪的[PathNpc,暗哨,10004,300021]
	if idx == 0 then
		CALL("任务点移动",10004,300021)
	end
end,

--------73,第三回：杨家谷内飞寒芒--------
--祸起萧墙
[1921] = function(idx)
	--找到[PathNpc,江风,10004,401]
	if idx == 0 then
		CALL("任务功能对话",1921,15005,"江风","找到[PathNpc,江风,10004,401]",10004,401)
	end
end,
--青龙索命-情法两难
[1870] = function(idx)
	--询问[PathNpc,韩莹莹,10004,191]。
	if idx == 1 then
		CALL("任务点对话",10102015,"韩莹莹",1870,1,10004,191)
	end
end,
--青龙索命-奇峰迭起
[1871] = function(idx)
	--与[PathNpc,韩莹莹,10004,191]对话。
	if idx == 0 then
		CALL("任务功能对话",1871,15004,"韩莹莹","与[PathNpc,韩莹莹,10004,191]对话。",10004,191)
	end
end,
--青龙索命-青龙现踪
[1880] = function(idx)
	--与[PathNpc,韩学信,10004,192]对话。
	if idx == 2 then
		CALL("任务点对话",10102011,"韩学信",1880,2,10004,192)
	end
end,
--一决死战
[1923] = function(idx)
	--去找[PathNpc,思成,10004,197]
	if idx == 0 then
		CALL("任务点对话",10102016,"思成",1923,0,10004,197)
	end
end,
--挫其锐气
[1924] = function(idx)
	--击败[PathMon,西夏散兵,10004,100711]
	if idx == 0 then
		local monsters = {"西夏散兵","西夏精英"}
		CALL("任务点杀怪",monsters,10004,100702)
	end
	--击败[PathMon,西夏精英,10004,100702]
	if idx == 1 then
		local monsters = {"西夏精英"}
		CALL("任务点杀怪",monsters,10004,100702)
	end
end,
--灭敌粮草-粮草先行
[1864] = function(idx)
	--[PathMon,粮草押运兵,10004,100720]
	if idx == 0 then
		local monsters = {"粮草押运兵","粮草押运官","西夏游击将军"}
		CALL("任务点杀怪",monsters,10004,100722)
	end
	--[PathMon,粮草押运官,10004,100721]
	if idx == 1 then
		local monsters = {"粮草押运官","西夏游击将军"}
		CALL("任务点杀怪",monsters,10004,100722)
	end
	--[PathMon,西夏游击将军,10004,100722]
	if idx == 3 then
		local monsters = {"西夏游击将军"}
		CALL("任务点杀怪",monsters,10004,100722)
	end
end,
--灭敌粮草-迎头痛击
[1865] = function(idx)
	--[PathMon,赫连达飞,10004,844]
	if idx == 2 then
		local monsters = {"赫连达飞"}
		CALL("任务点杀怪",monsters,10004,844)
	end
end,
--毁敌眼目-毙其塔哨
[1830] = function(idx)
	--击败[PathMon,守望塔哨兵,10004,100749]
	if idx == 0 then
		local monsters = {"守望塔哨兵","守望塔指挥官"}
		CALL("任务点杀怪",monsters,10004,100750)
	end
	--击败[PathMon,守望塔指挥官,10004,100750]
	if idx == 1 then
		local monsters = {"守望塔指挥官"}
		CALL("任务点杀怪",monsters,10004,100750)
	end
end,
--万夫莫敌-决战
[1872] = function(idx)
	--找[PathNpc,林烽火,10004,20]参与总攻
	if idx == 0 then
		CALL("任务功能对话",1872,15006,"林烽火","找[PathNpc,林烽火,10004,20]参与总攻",10004,20)
	end
end,
--万夫莫敌-得胜
[1873] = function(idx)
	--去找[PathNpc,林烽火,10004,20]复命
	if idx == 1 then
		CALL("任务点对话",10102017,"林烽火",1873,1,10004,20)
	end
end,
--六识通明
[1928] = function(idx)
	--找[PathNpc,韩学信,10004,192]
	if idx == 0 then
		CALL("任务点对话",10102011,"韩学信",1928,0,10004,192)
	end
end,
--西湖开屏
[1929] = function(idx)
	--去杭州找[PathNpc,韩义国,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,
--蛛丝马迹-众说纷纭
[19421] = function(idx)
	--找[PathNpc,唐青衫,10004,195]
	if idx == 0 then
		CALL("任务点对话",10102013,"唐青衫",19421,0,10004,195)
	end
	--找[PathNpc,韩振天,10004,193]
	if idx == 1 then
		CALL("任务点对话",10102005,"韩振天",19421,1,10004,193)
	end
	--找[PathNpc,韩莹莹,10004,191]
	if idx == 2 then
		CALL("任务点对话",10102015,"韩莹莹",19421,2,10004,191)
	end
end,
--蛛丝马迹-如实上报
[19422] = function(idx)
	--回报[PathNpc,韩学信,10004,192]
	if idx == 3 then
		CALL("任务点对话",10102011,"韩学信",19422,3,10004,192)
	end
end,

--------281,终不忘世外仙姝寂寞林--------
--路不平·素手救武僧
[10201] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--路不平·素手救武僧
[10202] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--找太白师兄
[10209] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--找村民
[10210] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--找佩剑
[10211] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--茶娘
[10213] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--茶娘
[10214] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--龙首山话本
[10216] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------282,羿思月bug临时方案--------
--羿思月提示
[65532] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------481,十九回：江湖似海大悲赋--------
--暂等消息
[14410] = function(idx)
	--[PathNpc,四处走走,10013,410]
	if idx == 0 then
		CALL("任务功能对话",14410,14053,"四处走走","[PathNpc,四处走走,10013,410]",10013,410)
	end
end,

--------482,二十回：世事难料情义绝--------
--护送回营
[14418] = function(idx)
	--[PathNpc,傅红雪,10013,108]
	if idx == 0 then
		CALL("任务功能对话",14418,16104,"傅红雪","[PathNpc,傅红雪,10013,108]",10013,108)
	end
end,

--------483,廿一回：为解奇毒破神武--------
--比试脚力
[14423] = function(idx)
	--[PathNpc,燕南飞,10013,644]
	if idx == 0 then
		CALL("任务功能对话",14423,16105,"燕南飞","[PathNpc,燕南飞,10013,644]",10013,644)
	end
end,
--异域奇蛇
[14429] = function(idx)
	--[PathNpc,蛇夫,10013,511]
	if idx == 0 then
		CALL("任务功能对话",14429,16103,"蛇夫","[PathNpc,蛇夫,10013,511]",10013,511)
	end
end,
--徐海风云
[14435] = function(idx)
	--等级到达70
	if idx == 0 then
		CALL("等级限制",14435)
	end
end,
--界限突破伍
[14436] = function(idx)
	--等待突破：天香传世(70级)
	if idx == 0 then
		CALL("界限突破",14436)
	end
end,

--------900361,??--------
--行商有难-行商求助
[5601] = function(idx)
	--找到[PathNpc,贾正金,10010,55]
	if idx == 0 then
		CALL("任务点对话",10104010,"贾正金",5601,0,10010,55)
	end
end,
--行商有难-物归原主
[5602] = function(idx)
	--取得[PathEnt,行商货物,10010,40044]
	if idx == 1 then
		CALL("任务点采集","行商货物",10010,40044)
	end
end,
--貌合神离-暗流涌动
[5607] = function(idx)
	--与[PathNPC,离玉堂,10010,34]交谈
	if idx == 0 then
		CALL("任务点对话",10104009,"离玉堂",5607,0,10010,34)
	end
end,
--貌合神离-互相牵制
[5608] = function(idx)
	--与[PathNPC,李红渠,10010,11703]交谈
	if idx == 1 then
		CALL("任务点对话",10104109,"李红渠",5608,1,10010,11703)
	end
	--与[PathNPC,上官小仙,10010,11704]交谈
	if idx == 3 then
		CALL("任务点对话",10104111,"上官小仙",5608,3,10010,11704)
	end
	--与[PathNPC,皇甫星,10010,11705]交谈
	if idx == 4 then
		CALL("任务点对话",10104110,"皇甫星",5608,4,10010,11705)
	end
end,
--貌合神离-同盟不易
[5609] = function(idx)
	--与[PathNPC,离玉堂,10010,34]交谈
	if idx == 2 then
		CALL("任务点对话",10104009,"离玉堂",5609,2,10010,34)
	end
end,
--贼寇行凶-流杀门
[5613] = function(idx)
	--教训[PathMon,流杀门喽喽,10010,101698]
	if idx == 0 then
		local monsters = {"流杀门喽喽","流杀门帮众"}
		CALL("任务点杀怪",monsters,10010,101698)
	end
	--教训[PathMon,流杀门帮众,10010,101698]
	if idx == 1 then
		local monsters = {"流杀门帮众"}
		CALL("任务点杀怪",monsters,10010,101698)
	end
end,
--贼寇行凶-行商家丁
[5618] = function(idx)
	--找到呼救的[PathNpc,行商家丁,10010,54]
	if idx == 2 then
		CALL("任务点对话",10104012,"行商家丁",5618,2,10010,54)
	end
end,
--贼寇行凶-林中异响
[5660] = function(idx)
	--[PathArea,慈云林小径,10010,300069]
	if idx == 3 then
		CALL("任务点移动",10010,300069)
	end
end,
--蠢蠢欲动-财神往事
[5661] = function(idx)
	--[PathNPC,黄金生,10010,24]
	if idx == 0 then
		CALL("任务点对话",10104002,"黄金生",5661,0,10010,24)
	end
end,
--蠢蠢欲动-万里飞沙
[5662] = function(idx)
	--[PathNPC,黄元文,10010,11562]
	if idx == 1 then
		CALL("任务点对话",10104097,"黄元文",5662,1,10010,11562)
	end
end,
--当地强龙-询问消息
[5663] = function(idx)
	--[PathNPC,金十八,10010,9848]
	if idx == 1 then
		CALL("任务点对话",10104504,"金十八",5663,1,10010,9848)
	end
end,
--当地强龙-林中异响
[5664] = function(idx)
	--[PathNPC,杭州城外,10010,300018]
	if idx == 0 then
		CALL("任务点移动",10010,300018)
	end
end,
--初涉江湖-仓库
[10129] = function(idx)
	--找[PathNPC,仓库掌柜,10010,9782]谈谈
	if idx == 1 then
		CALL("任务点对话",10104065,"仓库掌柜",10129,1,10010,9782)
	end
end,
--初涉江湖-邮差
[10130] = function(idx)
	--找[PathNPC,大宋邮差,10010,11749]谈谈
	if idx == 0 then
		CALL("任务点对话",20104701,"大宋邮差",10130,0,10010,11749)
	end
end,

--------900362,??--------
--围攻坐实-凤落财神
[5605] = function(idx)
	--查看[PathNpc,刺客尸体,10010,9530]	
	if idx == 0 then
		CALL("任务点对话",10104029,"刺客尸体",5605,0,10010,9530)
	end
end,
--围攻坐实-报讯解围
[5606] = function(idx)
	--火速赶往[PathArea,财神阁,10010,300021]	
	if idx == 1 then
		CALL("任务点移动",10010,300021)
	end
end,
--围攻坐实-入财神阁
[5623] = function(idx)
	--询问[PathNpc,财神阁守卫,10010,74]
	if idx == 2 then
		CALL("任务点对话",10104019,"财神阁守卫",5623,2,10010,74)
	end
end,
--围攻坐实-金眼布防
[5624] = function(idx)
	--与[PathNpc,白言,10010,60]交谈	
	if idx == 3 then
		CALL("任务点对话",10104016,"白言",5624,3,10010,60)
	end
end,
--围攻坐实-江湖助拳
[5625] = function(idx)
	--与[PathNpc,莫川,10010,9543]交谈	
	if idx == 4 then
		CALL("任务点对话",10104017,"莫川",5625,4,10010,9543)
	end
end,
--众志成城-财神武器
[5626] = function(idx)
	--拿取[PathEnt,财神阁武器,10010,40001]
	if idx == 0 then
		CALL("任务点采集","财神阁武器",10010,40001)
	end
end,
--众志成城-分发兵器
[5627] = function(idx)
	--[PathNpc,唐士忠,10010,9990]
	if idx == 1 then
		CALL("任务点对话",10104088,"唐士忠",5627,1,10010,9990)
	end
	--[PathNpc,于百石,10010,9991]
	if idx == 2 then
		CALL("任务点对话",10104089,"于百石",5627,2,10010,9991)
	end
	--[PathNpc,瑞云同,10010,9992]
	if idx == 3 then
		CALL("任务点对话",10104090,"瑞云同",5627,3,10010,9992)
	end
end,
--密钥疑踪-财神秘辛
[5641] = function(idx)
	--与[PathNpc,白言,10010,60]交谈
	if idx == 1 then
		CALL("任务点对话",10104016,"白言",5641,1,10010,60)
	end
end,
--密钥疑踪-银煞指路
[5642] = function(idx)
	--询问[PathNpc,银煞,10010,9845]
	if idx == 0 then
		CALL("任务点对话",10104067,"白邓通",5642,0,10010,9845)
	end
end,

--------900363,??--------
--失而复得-流杀无门
[5644] = function(idx)
	--清理[PathMon,流杀香堂精锐,10010,101859]
	if idx == 0 then
		local monsters = {"流杀香堂精锐","流杀门绿巾刀手"}
		CALL("任务点杀怪",monsters,10010,101859)
	end
	--清理[PathMon,流杀绿巾刀手,10010,101859]
	if idx == 2 then
		local monsters = {"流杀门绿巾刀手"}
		CALL("任务点杀怪",monsters,10010,101859)
	end
end,
--失而复得-天生地养
[5645] = function(idx)
	--清理[PathMon,蒋地生,10010,10569]
	if idx == 1 then
		local monsters = {"流杀门 蒋地生"}
		CALL("任务点杀怪",monsters,10010,10569)
	end
end,
--初入江湖-等级提升
[5646] = function(idx)
	--等级提升至20
	if idx == 1 then
		CALL("等级限制",5646)
	end
end,
--初入江湖-赶赴江南
[5647] = function(idx)
	--前往[PathNpc,江南,10010,11563]
	if idx == 0 then
		CALL("任务点移动",10010,11563)
	end
end,
--护送密钥-九曜天轨
[5649] = function(idx)
	--前往[PathArea,九曜山,10010,300160]天星阁
	if idx == 0 then
		CALL("任务点移动",10010,300160)
	end
end,
--护送密钥-星罗棋布
[5650] = function(idx)
	--肃清[PathMon,流杀黑巾探子,10010,101866]
	if idx == 1 then
		local monsters = {"流杀黑巾探子","流杀黑巾杀手"}
		CALL("任务点杀怪",monsters,10010,101866)
	end
	--肃清[PathMon,流杀黑巾杀手,10010,101866]
	if idx == 2 then
		local monsters = {"流杀黑巾杀手"}
		CALL("任务点杀怪",monsters,10010,101866)
	end
end,
--披荆斩棘-游音入耳
[5651] = function(idx)
	--[PathNpc,路人,10010,10571]
	if idx == 3 then
		CALL("任务点对话",10104102,"商会密探",5651,3,10010,10571)
	end
end,
--披荆斩棘-旁敲侧击
[5652] = function(idx)
	--肃清[PathMon,流杀青衣高手,10010,101901]
	if idx == 0 then
		local monsters = {"流杀青衣高手","流杀青衣刀手","流杀青衣刺客"}
		CALL("任务点杀怪",monsters,10010,101901)
	end
	--肃清[PathMon,流杀青衣刀手,10010,101901]
	if idx == 1 then
		local monsters = {"流杀青衣刀手","流杀青衣刺客"}
		CALL("任务点杀怪",monsters,10010,101901)
	end
	--肃清[PathMon,流杀青衣刺客,10010,101901]
	if idx == 2 then
		local monsters = {"流杀青衣刺客"}
		CALL("任务点杀怪",monsters,10010,101901)
	end
end,
--回报余杭-螳螂捕蝉
[5653] = function(idx)
	--找[PathNPC,黄金生,10010,11583]对话
	if idx == 0 then
		CALL("任务点对话",10104103,"黄金生",5653,0,10010,11583)
	end
end,
--回报余杭-江南连环
[5654] = function(idx)
	--回杭州找[PathNpc,离玉堂,10010,11706]对话
	if idx == 1 then
		CALL("任务点对话",10104116,"离玉堂",5654,1,10010,11706)
	end
end,
--北燕归鸿-物归原主
[5656] = function(idx)
	--把密钥交给[PathNpc,金玉使,10010,10562]
	if idx == 0 then
		CALL("任务点对话",10104099,"金玉使",5656,0,10010,10562)
	end
end,
--北燕归鸿-按图索骥
[5657] = function(idx)
	--找[PathNpc,莫川,10010,10566]
	if idx == 2 then
		CALL("任务点对话",10104104,"莫川",5657,2,10010,10566)
	end
end,
--北燕归鸿-指点迷津
[5658] = function(idx)
	--找[PathNpc,燕南飞,10010,10565]
	if idx == 1 then
		CALL("任务点对话",10104100,"燕南飞",5658,1,10010,10565)
	end
end,

--------301,第十回：始闻万象觅三奇--------
--清永德化
[9004] = function(idx)
	--赶往[Map_X_Y,德化楼,10002,753,3317]
	if idx == 0 then
		CALL("坐标点移动",753,3317,10002)
	end
end,
--清永泰宁
[9005] = function(idx)
	--赶往[Map_X_Y,泰宁楼,10002,876,3242]
	if idx == 0 then
		CALL("坐标点移动",876,3242,10002)
	end
end,
--江湖义举
[9007] = function(idx)
	--[PathMon,东瀛浪客,10002,100431]
	if idx == 0 then
		local monsters = {"东瀛浪客","浪人头领","劫道浪人"}
		CALL("任务点杀怪",monsters,10002,100431)
	end
	--[PathMon,浪人头领,10002,100431]
	if idx == 1 then
		local monsters = {"浪人头领","劫道浪人"}
		CALL("任务点杀怪",monsters,10002,100431)
	end
	--[PathMon,劫道浪人,10002,100431]
	if idx == 2 then
		local monsters = {"劫道浪人"}
		CALL("任务点杀怪",monsters,10002,100431)
	end
end,
--荷塘寻人
[9022] = function(idx)
	--搜寻[PathArea,,夏荷翠池,10002,300035]
	if idx == 0 then
		CALL("任务点移动",10002,300035)
	end
end,

--------302,十一回：藏珍迷局破梦难--------
--三清道观
[9013] = function(idx)
	--搜寻[Map_X_Y,三清观,10002,1796,2470]
	if idx == 0 then
		CALL("坐标点移动",1796,2470,10002)
	end
end,
--湖光山色
[9015] = function(idx)
	--[PathArea,青龙潭,10002,300122]
	if idx == 0 then
		CALL("任务点移动",10002,300122)
	end
end,
--倪氏五少
[9018] = function(idx)
	--[PathMon,倪狄,10002,548]
	if idx == 0 then
		local monsters = {"倪狄"}
		CALL("任务点杀怪",monsters,10002,548)
	end
end,
--再探倪府
[9020] = function(idx)
	--[PathNPC,白丹青,10002,690]
	if idx == 0 then
		CALL("任务功能对话",9020,16005,"白丹青","[PathNPC,白丹青,10002,690]",10002,690)
	end
end,

--------900181,??--------
--天下四盟-盟会介绍
[10178] = function(idx)
	--与[PathNPC,离玉堂,10010,34]交谈
	if idx == 0 then
		CALL("任务点对话",10104009,"离玉堂",10178,0,10010,34)
	end
end,
--天下四盟-帮派介绍
[10179] = function(idx)
	--与[PathNPC,离玉堂,10010,34]交谈
	if idx == 2 then
		CALL("任务点对话",10104009,"离玉堂",10179,2,10010,34)
	end
end,
--天下四盟-测卦入盟
[10180] = function(idx)
	--测卦入盟
	if idx == 1 then
		CALL("加入盟会")
		--LogWarning("未知的任务处理:%s","测卦入盟")
	end
end,
--凤凰集外-一探究竟
[11001] = function(idx)
	--[PathArea,出凤凰集,10010,300109]
	if idx == 0 then
		CALL("任务点移动",10010,300109)
	end
end,
--凤凰集外-不明敌意
[11002] = function(idx)
	--[PathMon,白衣雅奴,10010,101186]
	if idx == 1 then
		local monsters = {"白衣雅奴","雅奴投手","素衣女贼"}
		CALL("任务点杀怪",monsters,10010,101188)
	end
	--[PathMon,雅奴投手,10010,101187]
	if idx == 4 then
		local monsters = {"雅奴投手","素衣女贼"}
		CALL("任务点杀怪",monsters,10010,101188)
	end
	--[PathMon,素衣女贼,10010,101188]
	if idx == 2 then
		local monsters = {"素衣女贼"}
		CALL("任务点杀怪",monsters,10010,101188)
	end
end,
--凤凰集外-微弱呼救
[11003] = function(idx)
	--[PathNpc,陆仁易,10010,10008]
	if idx == 3 then
		CALL("任务点对话",10107004,"陆仁易",11003,3,10010,10008)
	end
end,
--寻消问息-一醉疏狂
[11006] = function(idx)
	--到达[PathArea,一醉轩,10010,300107]
	if idx == 0 then
		CALL("任务点移动",10010,300107)
	end
end,
--寻消问息-翰林名仕
[11007] = function(idx)
	--[PathNpc,钱惟演,10010,10022]
	if idx == 1 then
		CALL("任务点对话",10107008,"钱惟演",11007,1,10010,10022)
	end
end,
--寻消问息-白衣卿相
[11009] = function(idx)
	--[PathNpc,柳永,10010,10024]
	if idx == 2 then
		CALL("任务点对话",10107005,"柳永",11009,2,10010,10024)
	end
end,
--只身问道-禁暴诛乱
[11012] = function(idx)
	--[PathMon,天波叛党新兵,10010,101324]
	if idx == 1 then
		local monsters = {"天波叛党新兵"}
		CALL("任务点杀怪",monsters,10010,101324)
	end
end,
--只身问道-根究着实
[11013] = function(idx)
	--[PathNpc,问道台杂役,10010,10056]
	if idx == 2 then
		CALL("任务点对话",10107015,"问道台杂役",11013,2,10010,10056)
	end
end,
--参透机关-卿本佳人
[11015] = function(idx)
	--[PathNpc,雅奴二十六,10010,10354]
	if idx == 4 then
		CALL("任务点对话",10107032,"雅奴二十六",11015,4,10010,10354)
	end
end,
--参透机关-破军清道
[11016] = function(idx)
	--[PathMon,天波叛党狂徒,10010,101357]
	if idx == 0 then
		local monsters = {"天波叛党狂徒","天波叛党精锐"}
		CALL("任务点杀怪",monsters,10010,101356)
	end
	--[PathMon,天波叛党精锐,10010,101356]
	if idx == 1 then
		local monsters = {"天波叛党精锐"}
		CALL("任务点杀怪",monsters,10010,101356)
	end
end,
--参透机关-射鹰灭眼
[11017] = function(idx)
	--[PathMon,李隼,10010,10072]
	if idx == 2 then
		local monsters = {"李隼"}
		CALL("任务点杀怪",monsters,10010,10072)
	end
end,
--参透机关-弃子一枚
[11018] = function(idx)
	--[PathNpc,离玉堂,10010,10027]
	if idx == 3 then
		CALL("任务点对话",10107018,"离玉堂",11018,3,10010,10027)
	end
end,
--只身问道-片甲不留
[11045] = function(idx)
	--[PathMon,天波叛党巡查,10010,101328]
	if idx == 3 then
		local monsters = {"天波叛党巡查","天波叛党强兵"}
		CALL("任务点杀怪",monsters,10010,101329)
	end
	--[PathMon,天波叛党强兵,10010,101329]
	if idx == 4 then
		local monsters = {"天波叛党强兵"}
		CALL("任务点杀怪",monsters,10010,101329)
	end
end,
--凶恣挠法-山间小曲
[11046] = function(idx)
	--[PathMon,苍衣雅奴,10010,101279]
	if idx == 0 then
		local monsters = {"苍衣雅奴","雅奴剑客","劲装女贼"}
		CALL("任务点杀怪",monsters,10010,101281)
	end
	--[PathMon,雅奴剑客,10010,101280]
	if idx == 3 then
		local monsters = {"雅奴剑客","劲装女贼"}
		CALL("任务点杀怪",monsters,10010,101281)
	end
	--[PathMon,劲装女贼,10010,101281]
	if idx == 1 then
		local monsters = {"劲装女贼"}
		CALL("任务点杀怪",monsters,10010,101281)
	end
end,
--凶恣挠法-意外插曲
[11047] = function(idx)
	--[PathNpc,占沐,10010,10263]
	if idx == 2 then
		CALL("任务点对话",10107030,"占沐",11047,2,10010,10263)
	end
end,
--书不释手-散落书籍
[11048] = function(idx)
	--[PathEnt,散落的书籍,10010,40087]
	if idx == 0 then
		CALL("任务点采集","散落的书籍",10010,40087)
	end
end,
--书不释手-物归原主
[11049] = function(idx)
	--找到[PathNpc,史博科,10010,10264]
	if idx == 1 then
		CALL("任务点对话",10107031,"史博科",11049,1,10010,10264)
	end
end,
--只身问道-长驱直入
[11050] = function(idx)
	--[PathMon,问道台守卫,10010,10286]
	if idx == 0 then
		local monsters = {"问道台守卫","问道台巡守"}
		CALL("任务点杀怪",monsters,10010,10300)
	end
	--[PathMon,问道台巡守,10010,10300]
	if idx == 5 then
		local monsters = {"问道台巡守"}
		CALL("任务点杀怪",monsters,10010,10300)
	end
end,
--无忆所指-寒江之钓
[11053] = function(idx)
	--与[PathNpc,曹修雅,10010,11683]交谈
	if idx == 0 then
		CALL("任务点对话",10107045,"曹修雅",11053,0,10010,11683)
	end
	--与[PathNpc,安笃清,10010,11684]交谈
	if idx == 1 then
		CALL("任务点对话",10107046,"安笃清",11053,1,10010,11684)
	end
	--与[PathNpc,何盼理,10010,11685]交谈
	if idx == 2 then
		CALL("任务点对话",10107047,"何盼理",11053,2,10010,11685)
	end
end,
--无忆所指-神威女杰
[11054] = function(idx)
	--与[PathNpc,韩莹莹,10010,11686]交谈
	if idx == 3 then
		CALL("任务点对话",10107048,"韩莹莹",11054,3,10010,11686)
	end
end,
--故人之约-凤凰集内
[11055] = function(idx)
	--到达[PathArea,凤凰集,10010,300825]
	if idx == 0 then
		CALL("任务点移动",10010,300825)
	end
end,
--故人之约-黑刀浪子
[11056] = function(idx)
	--与[PathNpc,傅红雪,10010,12066]交谈
	if idx == 1 then
		CALL("任务点对话",10107057,"傅红雪",11056,1,10010,12066)
	end
	--与[PathNpc,燕南飞,10010,12067]交谈
	if idx == 2 then
		CALL("任务点对话",10107058,"燕南飞",11056,2,10010,12067)
	end
end,
--故人之约-明月有心
[11057] = function(idx)
	--与[PathNpc,明月心,10010,12068]交谈
	if idx == 3 then
		CALL("任务点对话",10107059,"明月心",11057,3,10010,12068)
	end
end,

--------901,第一回：有唐门地本逍遥--------
--锦衣罗衫
[27010] = function(idx)
	--[PathNpc,唐巧,10005,3]
	if idx == 0 then
		CALL("任务点对话",10116004,"唐巧",27010,0,10005,3)
	end
end,
--泥胎木塑
[27035] = function(idx)
	--跟[PathNpc,唐倪,10005,8]对话
	if idx == 0 then
		CALL("任务功能对话",27035,19033,"唐倪","跟[PathNpc,唐倪,10005,8]对话",10005,8)
	end
end,

--------900182,??--------
--探本穷源-香山古道
[11025] = function(idx)
	--[PathNpc,雅奴二十六,10010,11691]
	if idx == 0 then
		CALL("任务点对话",10107051,"雅奴二十六",11025,0,10010,11691)
	end
end,
--探本穷源-与佳人期
[11026] = function(idx)
	--[PathEnt,桃花,10010,40320]
	if idx == 3 then
		CALL("任务点采集","桃花",10010,40320)
	end
end,
--探本穷源-灼灼其华
[11027] = function(idx)
	--将桃花交给[PathNpc,韩莹莹,10010,11692]
	if idx == 4 then
		CALL("任务点使用物品","桃花",10010,11692)
		--LogWarning("未知的任务处理:%s","将桃花交给[PathNpc,韩莹莹,10010,11692]")
	end
end,
--初入百里


[11028] = function(idx)
	--[PathArea,百里荡,10010,300111]
	if idx == 0 then
		CALL("任务点移动",10010,300111)
	end
end,
--翻浪水寨


[11029] = function(idx)
	--[PathMon,翻浪坞水匪,10010,101452]
	if idx == 1 then
		local monsters = {"翻浪坞水匪","翻浪坞弓手","翻浪坞狂徒"}
		CALL("任务点杀怪",monsters,10010,101453)
	end
	--[PathMon,翻浪坞弓手,10010,101451]
	if idx == 2 then
		local monsters = {"翻浪坞弓手","翻浪坞狂徒"}
		CALL("任务点杀怪",monsters,10010,101453)
	end
	--[PathMon,翻浪坞狂徒,10010,101453]
	if idx == 7 then
		local monsters = {"翻浪坞狂徒"}
		CALL("任务点杀怪",monsters,10010,101453)
	end
end,
--长驱直入


[11030] = function(idx)
	--[PathMon,翻浪坞营地守卫,10010,101504]
	if idx == 3 then
		local monsters = {"翻浪坞营地守卫","翻浪坞营地巡查"}
		CALL("任务点杀怪",monsters,10010,101505)
	end
	--[PathMon,翻浪坞营地巡查,10010,101505]
	if idx == 4 then
		local monsters = {"翻浪坞营地巡查"}
		CALL("任务点杀怪",monsters,10010,101505)
	end
end,
--翻覆水寨


[11031] = function(idx)
	--[PathMon,翻浪坞帮众,10010,101531]
	if idx == 5 then
		local monsters = {"翻浪坞帮众","翻浪坞弟子","翻浪坞精锐"}
		CALL("任务点杀怪",monsters,10010,101533)
	end
	--[PathMon,翻浪坞弟子,10010,101532]
	if idx == 8 then
		local monsters = {"翻浪坞弟子","翻浪坞精锐"}
		CALL("任务点杀怪",monsters,10010,101533)
	end
	--[PathMon,翻浪坞精锐,10010,101533]
	if idx == 6 then
		local monsters = {"翻浪坞精锐"}
		CALL("任务点杀怪",monsters,10010,101533)
	end
end,
--凌水之鲲

[11032] = function(idx)
	--[PathMon,凌鲲,10010,10142]
	if idx == 0 then
		local monsters = {"凌鲲"}
		CALL("任务点杀怪",monsters,10010,10142)
	end
end,
--落云卷岚-杨柳依依
[11034] = function(idx)
	--[PathNpc,止步不前的女子,10010,13265]
	if idx == 4 then
		CALL("任务点对话",10107060,"止步不前的女子",11034,4,10010,13265)
	end
end,
--飞流水寨


[11035] = function(idx)
	--[PathMon,飞流坞营地守卫,10010,101607]
	if idx == 0 then
		local monsters = {"飞流坞营地守卫","飞流坞营地巡查"}
		CALL("任务点杀怪",monsters,10010,101608)
	end
	--[PathMon,飞流坞营地巡查,10010,101608]
	if idx == 1 then
		local monsters = {"飞流坞营地巡查"}
		CALL("任务点杀怪",monsters,10010,101608)
	end
end,
--折冲将军


[11036] = function(idx)
	--[PathMon,飞流坞帮众,10010,101643]
	if idx == 2 then
		local monsters = {"飞流坞帮众","飞流坞精锐"}
		CALL("任务点杀怪",monsters,10010,101645)
	end
	--[PathMon,飞流坞精锐,10010,101645]
	if idx == 3 then
		local monsters = {"飞流坞精锐"}
		CALL("任务点杀怪",monsters,10010,101645)
	end
end,
--野佛横渡-问水逆流
[11037] = function(idx)
	--[PathNpcLayer,问水船夫,10010,10401,1]
	if idx == 1 then
		--桥上?
		CALL("任务点对话",10107033,"问水船夫",11037,1,10010,10401,1)
	end
end,
--野佛横渡-津关险塞
[11038] = function(idx)
	--[PathNpc,黄元文,10010,10098]
	if idx == 0 then
		CALL("任务点对话",10107020,"黄元文",11038,0,10010,10098)
	end
end,
--探本穷源-密信所呈
[11051] = function(idx)
	--与[PathNpc,离玉堂,10010,10097]交谈
	if idx == 1 then
		CALL("任务点对话",10107019,"离玉堂",11051,1,10010,10097)
	end
end,
--探本穷源-终开玉口
[11058] = function(idx)
	--与[PathNpc,韩莹莹,10010,11692]交谈
	if idx == 2 then
		CALL("任务点对话",10107053,"韩莹莹",11058,2,10010,11692)
	end
end,
--探本穷源-古法显形
[11059] = function(idx)
	--稍等片刻
	if idx == 5 then
		CALL("等待任务完成",11059)
	end
end,
--探本穷源-静候佳音
[11060] = function(idx)
	--询问[PathNpc,韩莹莹,10010,11692]
	if idx == 6 then
		CALL("任务点对话",10107053,"韩莹莹",11060,6,10010,11692)
	end
end,
--探本穷源-装神弄鬼
[11061] = function(idx)
	--与[PathNpc,离玉堂,10010,10097]交谈
	if idx == 7 then
		CALL("任务点对话",10107019,"离玉堂",11061,7,10010,10097)
	end
end,
--亡羊补牢

[11062] = function(idx)
	--[PathArea,落云滩,10010,300113]
	if idx == 1 then
		CALL("任务点移动",10010,300113)
	end
end,
--覆寂飞流-泥泽难跃
[11063] = function(idx)
	--[PathMon,戎跃泽,10010,10382]
	if idx == 0 then
		local monsters = {"戎跃泽"}
		CALL("任务点杀怪",monsters,10010,10382)
	end
end,
--覆寂飞流-静候回音
[11066] = function(idx)
	--等待离玉堂回音
	if idx == 1 then
		CALL("等待任务完成",11066)
	end
end,
--待时而动-联络烟花
[11075] = function(idx)
	--[PathEnt,箱子,10010,40335]
	if idx == 0 then
		CALL("任务点采集","箱子",10010,40335)
	end
end,
--待时而动-万里传讯
[11076] = function(idx)
	--[PathNpc,上南小桥,10010,301084]
	if idx == 1 then
		CALL("任务点使用物品","联络烟花",10010,301084)
		--LogWarning("未知的任务处理:%s","[PathNpc,上南小桥,10010,301084]")
	end
end,

--------304,千面江湖任逍遥--------
--游历杭州
[9021] = function(idx)
	--[PathNPC,江湖传奇 司空央,10010,13675]
	if idx == 0 then
		CALL("任务点对话",10205104,"司空央",9021,0,10010,13675)
	end
end,
--乐伶
[9040] = function(idx)
	--[PathNPC,绝代红伶 舒音,10010,9998]
	if idx == 0 then
		CALL("任务点对话",10205109,"舒音",9040,0,10010,9998)
	end
end,
--市井
[9041] = function(idx)
	--[PathNPC,八面玲珑 张三,10010,9997]
	if idx == 0 then
		CALL("任务点对话",10205107,"张三",9041,0,10010,9997)
	end
end,
--商贾
[9042] = function(idx)
	--[PathNPC,四海巨贾 端木金,10010,10004]
	if idx == 0 then
		CALL("任务点对话",10205120,"端木金",9042,0,10010,10004)
	end
end,
--猎户
[9043] = function(idx)
	--[PathNPC,百步穿杨 苗胜天,10010,10001]
	if idx == 0 then
		CALL("任务点对话",10205116,"苗胜天",9043,0,10010,10001)
	end
end,
--镖师
[9044] = function(idx)
	--[PathNPC,一镖千金 万全,10010,9995]
	if idx == 0 then
		CALL("任务点对话",10205105,"万全 ",9044,0,10010,9995)
	end
end,
--游侠
[9045] = function(idx)
	--[PathNPC,独行千里 迟英,10010,13676]
	if idx == 0 then
		CALL("任务点对话",10205112,"迟英",9045,0,10010,13676)
	end
end,
--杀手
[9046] = function(idx)
	--[PathNPC,三绝无命 杜枫,10010,10000]
	if idx == 0 then
		CALL("任务点对话",10205113,"杜枫",9046,0,10010,10000)
	end
end,
--身份抉择
[9047] = function(idx)
	--获得江湖身份
	if idx == 0 then
		CALL("身份选择")
	end
end,
--悬眼
[9048] = function(idx)
	--[PathNPC,钦定悬眼 宁武,10010,9996]
	if idx == 0 then
		CALL("任务点对话",10205106,"宁武",9048,0,10010,9996)
	end
end,
--捕快
[9049] = function(idx)
	--[PathNPC,京城名捕 云中燕,10010,10002]
	if idx == 0 then
		CALL("任务点对话",10205118,"云中燕",9049,0,10010,10002)
	end
end,
--文士
[9050] = function(idx)
	--[PathNPC,翰林书院 小师妹,10010,11989]
	if idx == 0 then
		CALL("任务点对话",20107731,"董倩文",9050,0,10010,11989)
	end
end,

--------902,第二回：万青竹海藏锋芒--------
--悠然忘忧
[27065] = function(idx)
	--相助[PathNpc,唐倪,10005,300023]
	if idx == 0 then
		CALL("任务功能对话",27065,19037,"唐倪","相助[PathNpc,唐倪,10005,300023]",10005,300023)
	end
end,
--万点繁星
[27080] = function(idx)
	--与[PathNpc,唐倪,10005,28]对话进入傀儡室
	if idx == 0 then
		CALL("任务功能对话",27080,19036,"唐倪","与[PathNpc,唐倪,10005,28]对话进入傀儡室",10005,28)
	end
end,
--飞越绝壁
[27090] = function(idx)
	--请[PathNpc,唐端,10005,24]相助
	if idx == 0 then
		CALL("任务点移动",10005,24)
	end
end,
--神出鬼没
[27095] = function(idx)
	--[PathNpc,唐倪,10005,19]
	if idx == 0 then
		CALL("任务点对话",10116029,"唐倪",27095,0,10005,19)
	end
end,

--------900183,??--------
--东京汴梁-黄金百万
[10186] = function(idx)
	--拜访[PathNpc,金百万,10012,94]
	if idx == 0 then
		CALL("任务点对话",10112064,"金百万",10186,0,10012,94)
	end
end,
--东京汴梁-拍卖行
[10187] = function(idx)
	--与[PathNpc,金百万,10012,94]聊聊
	if idx == 1 then
		CALL("任务点对话",10112064,"金百万",10187,1,10012,94)
	end
end,
--凶相毕露


[11039] = function(idx)
	--[PathMon,天绝禅院巡守,10010,101657]
	if idx == 0 then
		local monsters = {"天绝禅院巡守","天绝禅院武僧"}
		CALL("任务点杀怪",monsters,10010,101658)
	end
	--[PathMon,天绝禅院武僧,10010,101658]
	if idx == 1 then
		local monsters = {"天绝禅院武僧"}
		CALL("任务点杀怪",monsters,10010,101658)
	end
end,
--激浊扬清 


[11040] = function(idx)
	--[PathMon,丁浑,10010,10123]
	if idx == 2 then
		local monsters = {"丁浑","丁浑侍卫"}
		CALL("任务点杀怪",monsters,10010,10404)
	end
	--[PathMon,丁浑侍卫,10010,10404]
	if idx == 3 then
		local monsters = {"丁浑侍卫"}
		CALL("任务点杀怪",monsters,10010,10404)
	end
end,
--游尘土梗


[11042] = function(idx)
	--[PathNpc,禅院侍僧,10010,10105]
	if idx == 4 then
		CALL("任务点对话",10107023,"禅院侍僧",11042,4,10010,10105)
	end
end,
--天绝独塔


[11043] = function(idx)
	--[PathArea,天绝塔,10010,300114]
	if idx == 0 then
		CALL("任务点移动",10010,300114)
	end
end,
--无名老僧


[11044] = function(idx)
	--[PathNpc,扫地僧,10010,10109]
	if idx == 1 then
		CALL("任务点对话",10107025,"扫地僧",11044,1,10010,10109)
	end
end,
--神秘高人-一臂之力
[11052] = function(idx)
	--[PathArea,东平郡王府,10010,300755]
	if idx == 2 then
		CALL("任务点移动",10010,300755)
	end
end,

--------305,千面江湖任逍遥--------
--市井营生
[9052] = function(idx)
	--[PathNPC,八面玲珑 张三,10010,9997]
	if idx == 0 then
		CALL("任务点对话",10205107,"八面玲珑 张三",9052,0,10010,9997)
	end
end,
--商海沉浮
[9053] = function(idx)
	--[PathNPC,四海巨贾 端木金,10010,10004]
	if idx == 0 then
		CALL("任务点对话",10205120,"四海巨贾 端木金",9053,0,10010,10004)
	end
end,
--暗藏锐眼
[9059] = function(idx)
	--[PathNPC,钦定悬眼 宁武,10010,9996]
	if idx == 0 then
		CALL("任务点对话",10205106,"钦定悬眼 宁武",9059,0,10010,9996)
	end
end,

--------903,第三回：情深不寿梦一场--------
--诛灭黑衣
[27120] = function(idx)
	--[PathMon,龙纹刽子手,10005,100204]
	if idx == 0 then
		local monsters = {"龙纹刽子手","龙纹黑衣","刺探队长"}
		CALL("任务点杀怪",monsters,10005,100204)
	end
	--[PathMon,龙纹黑衣,10005,100092]
	if idx == 1 then
		local monsters = {"龙纹黑衣","刺探队长"}
		CALL("任务点杀怪",monsters,10005,100204)
	end
	--[PathMon,刺探队长,10005,100204]
	if idx == 2 then
		local monsters = {"刺探队长"}
		CALL("任务点杀怪",monsters,10005,100204)
	end
end,
--杀入侧殿
[27130] = function(idx)
	--找躲在台阶上的[PathNpc,唐倪,10005,21]
	if idx == 0 then
		CALL("任务点对话",10116033,"唐倪",27130,0,10005,21)
	end
end,
--碎星奇变
[27135] = function(idx)
	--找[PathNpc,唐倪,10005,21]进入碎星楼
	if idx == 0 then
		CALL("任务功能对话",27135,19038,"唐倪","找[PathNpc,唐倪,10005,21]进入碎星楼",10005,21)
	end
end,
--情根深种
[27145] = function(idx)
	--找[PathNpc,唐端,10005,25]对话进入傀儡房
	if idx == 0 then
		CALL("任务功能对话",27145,15009,"唐端","找[PathNpc,唐端,10005,25]对话进入傀儡房",10005,25)
	end
end,
--御风神行
[27150] = function(idx)
	--找[PathNpc,唐端,10005,25]
	if idx == 0 then
		CALL("任务点对话",10116034,"唐端",27150,0,10005,25)
	end
end,
--四盟大势
[27155] = function(idx)
	--[PathNpc,唐老太,10005,5]
	if idx == 0 then
		CALL("任务点对话",10116002,"唐老太",27155,0,10005,5)
	end
end,
--前往杭州
[27160] = function(idx)
	--去往杭州寻找[PathNpc,唐鑫,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,

--------306,十二回：白驹过隙情永存--------
--藏珍阁内
[9107] = function(idx)
	--[PathNpc,严泽,10002,1908]
	if idx == 0 then
		CALL("任务功能对话",9107,16008,"严泽","[PathNpc,严泽,10002,1908]",10002,1908)
	end
end,

--------121,第一回：太白沉剑风波起--------
--趁手武器
[3603] = function(idx)
	--与[PathNpc,铁正阳,10009,253]交谈
	if idx == 0 then
		CALL("任务点对话",10103010,"铁正阳",3603,0,10009,253)
	end
end,
--沉剑之试
[3606] = function(idx)
	--跟[PathNpc,于青,10009,11]交谈参加沉剑之试
	if idx == 0 then
		CALL("任务功能对话",3606,18000,"于青","跟[PathNpc,于青,10009,11]交谈参加沉剑之试",10009,11)
	end
end,
--日常司职
[3608] = function(idx)
	--和[PathNpc,穆清,10009,25]交谈
	if idx == 0 then
		CALL("任务点对话",10103012,"穆清",3608,0,10009,25)
	end
end,
--整衣敛容
[3626] = function(idx)
	--与[PathNpc,柏芊芊,10009,254]交谈
	if idx == 0 then
		CALL("任务点对话",10103017,"柏芊芊",3626,0,10009,254)
	end
end,
--指点武学
[3627] = function(idx)
	--与[PathNpc,余淮,10009,7]交谈
	if idx == 0 then
		CALL("任务点对话",10103018,"余淮",3627,0,10009,7)
	end
end,
--心有灵犀
[3632] = function(idx)
	--找[PathNpc,公孙剑,10009,1103]交谈
	if idx == 0 then
		CALL("任务点对话",20103036,"公孙剑",3632,0,10009,1103)
	end
end,

--------321,东越见闻前置--------
--晨曦
[9601] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--晨午
[9602] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--悬赏令
[9603] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--子桑不寿
[9604] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--青龙潭未完成
[9605] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--江洋大盗1
[9606] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--江洋大盗2
[9607] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--江洋大盗3
[9608] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,
--戒空隐藏任务
[9610] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------122,第二回：寻双剑追凶踏雪--------
--守护剑池
[3612] = function(idx)
	--赶赴[PathArea,沉剑池入口,10009,300017]
	if idx == 0 then
		CALL("任务点移动",10009,300017)
	end
end,
--寻迹而追
[3614] = function(idx)
	--沿着山道往[PathArea,弑剑阁,10009,300038]的方向追去
	if idx == 0 then
		CALL("任务点移动",10009,300038)
	end
end,
--受伤弟子
[3616] = function(idx)
	--劝回[PathNpc,赵正清,10009,169]
	if idx == 0 then
		CALL("任务点对话",10103038,"赵正清",3616,0,10009,169)
	end
end,
--高丽武痴
[3617] = function(idx)
	--与[PathNpc,赵正清,10009,169]交谈，让他放心回去
	if idx == 0 then
		CALL("任务功能对话",3617,18003,"赵正清","与[PathNpc,赵正清,10009,169]交谈，让他放心回去",10009,169)
	end
end,

--------900001,??--------

--------123,第三回：斩奸佞誓破青龙--------
--摧山颓势
[3621] = function(idx)
	--[PathMon,摧山营帮众,10009,100643]
	if idx == 0 then
		local monsters = {"摧山营帮众","摧山营精锐"}
		CALL("任务点杀怪",monsters,10009,100643)
	end
	--[PathMon,摧山营精锐,10009,100643]
	if idx == 1 then
		local monsters = {"摧山营精锐"}
		CALL("任务点杀怪",monsters,10009,100643)
	end
end,
--凶牙不敌
[3622] = function(idx)
	--[PathMon,凶牙营帮众,10009,100696]
	if idx == 0 then
		local monsters = {"凶牙营帮众","凶牙营精锐","凶牙营香主"}
		CALL("任务点杀怪",monsters,10009,100696)
	end
	--[PathMon,凶牙营精锐,10009,100696]
	if idx == 1 then
		local monsters = {"凶牙营精锐","凶牙营香主"}
		CALL("任务点杀怪",monsters,10009,100696)
	end
	--[PathMon,凶牙营香主,10009,100696]
	if idx == 2 then
		local monsters = {"凶牙营香主"}
		CALL("任务点杀怪",monsters,10009,100696)
	end
end,
--逆徒末路
[3623] = function(idx)
	--[PathMon,地剑逆徒,10009,100673]
	if idx == 0 then
		local monsters = {"地剑逆徒","天剑逆徒","天剑精锐"}
		CALL("任务点杀怪",monsters,10009,100673)
	end
	--[PathMon,天剑逆徒,10009,100673]
	if idx == 1 then
		local monsters = {"天剑逆徒","天剑精锐"}
		CALL("任务点杀怪",monsters,10009,100673)
	end
	--[PathMon,天剑精锐,10009,100673]
	if idx == 2 then
		local monsters = {"天剑精锐"}
		CALL("任务点杀怪",monsters,10009,100673)
	end
end,
--夺剑之战
[3624] = function(idx)
	--速赶往[Map_X_Y,古祭坛,10009,3132,2956]
	if idx == 0 then
		CALL("坐标点移动",3132,2956,10009)
	end
end,
--西湖开屏
[3628] = function(idx)
	--去往杭州寻找[PathNpc,徐雪寒,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,

--------1121,第一回：独立天下第一香--------
--回到天香
[32401] = function(idx)
	--与[PathNpc,夏语冰,10002,2374]交谈
	if idx == 0 then
		CALL("任务点对话",10119001,"夏语冰",32401,0,10002,2374)
	end
end,
--进入谷中
[32402] = function(idx)
	--[PathArea,入谷查探,10002,300690]
	if idx == 0 then
		CALL("任务点移动",10002,300690)
	end
end,
--不明骚动
[32403] = function(idx)
	--[PathArea,寻找嘈杂之声来源,10002,300691]
	if idx == 0 then
		CALL("任务点移动",10002,300691)
	end
end,
--支骨描面
[32407] = function(idx)
	--[PathNpc,蒋婳笙,10002,2396]
	if idx == 0 then
		CALL("任务点对话",10119004,"蒋画笙",32407,0,10002,2396)
	end
end,
--珠歌翠舞
[32408] = function(idx)
	--[PathNpc,卢文锦,10002,2399]
	if idx == 0 then
		CALL("任务点对话",10119007,"琵琶行 卢文锦",32408,0,10002,2399)
	end
end,
--心有灵犀
[32427] = function(idx)
	--[PathNpc,柳扶风,10002,2400]
	if idx == 0 then
		CALL("任务点对话",10119008,"柳扶风",32427,0,10002,2400)
	end
end,

--------900601,??--------

--------1122,第二回：东风不过乱红处--------
--重华乱舞
[32412] = function(idx)
	--[PathArea,药田,10002,300688]
	if idx == 0 then
		CALL("任务点移动",10002,300688)
	end
end,
--星罗棋布
[32414] = function(idx)
	--[PathNpc,星罗居弟子,10002,2414]
	if idx == 0 then
		CALL("任务功能对话",32414,18022,"星罗居弟子","[PathNpc,星罗居弟子,10002,2414]",10002,2414)
	end
end,
--白鹭为洲
[32416] = function(idx)
	--[PathNpc,白鹭洲,10002,2419]
	if idx == 0 then
		CALL("任务功能对话",32416,18023,"乱红弦 白鹭洲","[PathNpc,白鹭洲,10002,2419]",10002,2419)
	end
end,
--密云不雨
[32418] = function(idx)
	--[PathArea,观海台,10002,300689]
	if idx == 0 then
		CALL("任务点移动",10002,300689)
	end
end,

--------900602,??--------

--------1123,第三回：夜露轻弹花映天--------
--香伞玄机
[32421] = function(idx)
	--[PathNpc,唐青铃,10002,2407]
	if idx == 0 then
		CALL("任务功能对话",32421,18025,"飞灵伞 唐青铃","[PathNpc,唐青铃,10002,2407]",10002,2407)
	end
end,
--百花争望
[32423] = function(idx)
	--[PathNpc,柳扶风,10002,2400]
	if idx == 0 then
		CALL("任务功能对话",32423,18026,"柳扶风","[PathNpc,柳扶风,10002,2400]",10002,2400)
	end
end,
--春风化雨
[32424] = function(idx)
	--[PathNpc,梁知音,10002,2401]
	if idx == 0 then
		CALL("任务点对话",10119000,"空谷留香 梁知音",32424,0,10002,2401)
	end
end,
--西湖开屏
[32425] = function(idx)
	--[PathNpc,云文君,10010,300771]
	if idx == 0 then
		CALL("任务点移动",10010,300771)
	end
end,

--------901001,??--------

--------900603,??--------

--------901002,??--------

--------901003,??--------

--------901601,??--------

--------530,徐海见闻条件--------
--奔马堂01
[16100] = function(idx)
	--
	if idx == 0 then
		LogWarning("未知的任务处理:%s","")
	end
end,

--------341,教学壹--------
--江湖见闻
[10701] = function(idx)
	--[PathNPC,天波府试炼,10010,9541]
	if idx == 0 then
		CALL("任务功能对话",10701,20011,"天波府执事","[PathNPC,天波府试炼,10010,9541]",10010,9541)
	end
end,
--江湖试炼
[10703] = function(idx)
	--通关杭州：[PathNPC,白邓通,10010,9845]普通试炼
	if idx == 0 then
		CALL("任务点普通试炼","白邓通",10010,9845,0x26)
	end
end,

--------342,教学贰--------
--传信
[10710] = function(idx)
	--[PathNPC,江湖传奇 司空央,10010,13675]
	if idx == 0 then
		CALL("任务点对话",10205104,"司空央",10710,0,10010,13675)
	end
end,
--制造
[10711] = function(idx)
	--制造任意一件装备
	if idx == 0 then
		CALL("教学任务-制造装备","衣带")
		--LogWarning("未知的任务处理:%s","制造任意一件装备")
	end
end,
--分解
[10712] = function(idx)
	--装备分解
	if idx == 0 then
		CALL("教学任务-装备分解")
	end
end,
--精工
[10713] = function(idx)
	--精工任意一件装备
	if idx == 0 then
		CALL("教学任务-装备精工")
		--LogWarning("未知的任务处理:%s","精工任意一件装备")
	end
end,
--经脉冲穴
[10714] = function(idx)
	--完成一次冲穴
	if idx == 0 then
		CALL("教学任务-经脉冲穴")
		--LogWarning("未知的任务处理:%s","完成一次冲穴")
	end
end,
--心法装备
[10715] = function(idx)
	--心法装备
	if idx == 0 then
		CALL("教学任务-心法装备")
		--LogWarning("未知的任务处理:%s","心法装备")
	end
end,
--心法突破
[10716] = function(idx)
	--完成一次心法突破
	if idx == 0 then
		CALL("教学任务-心法突破")
		--LogWarning("未知的任务处理:%s","完成一次心法突破")
	end
end,
--前辈指点
[10720] = function(idx)
	--[PathNPC,江湖传奇 司空央,10010,13675]
	if idx == 0 then
		CALL("任务点对话",nil,"司空央",10720,0,10010,13675)
	end
end,

--------900421,??--------

--------900023,??--------

--------900422,??--------

--------900423,??--------

--------361,第四回：孔雀开屏江湖动--------
--齐聚杭州
[5401] = function(idx)
	--去找师兄[PathNpc,徐雪寒,10010,28]
	if idx == 0 then
		CALL("任务点对话",10104003,"徐雪寒",5401,0,10010,28)
	end
end,
--流杀盘踞
[5409] = function(idx)
	--找到[PathNpc,贾正福,10010,56]
	if idx == 0 then
		CALL("任务功能对话",5409,14001,"贾正福","找到[PathNpc,贾正福,10010,56]",10010,56)
	end
end,
--齐聚杭州
[5411] = function(idx)
	--去找师兄[PathNpc,史晴龙,10010,29]
	if idx == 0 then
		CALL("任务点对话",10104004,"史晴龙",5411,0,10010,29)
	end
end,
--齐聚杭州
[5412] = function(idx)
	--去找师兄[PathNpc,韩义国,10010,30]
	if idx == 0 then
		CALL("任务点对话",10104005,"韩义国",5412,0,10010,30)
	end
end,
--齐聚杭州
[5413] = function(idx)
	--去找师兄[PathNpc,唐鑫,10010,31]
	if idx == 0 then
		CALL("任务点对话",10104006,"唐鑫",5413,0,10010,31)
	end
end,
--再遇蔷薇
[5414] = function(idx)
	--去找桥头的[PathNpc,燕南飞,10010,10561]
	if idx == 0 then
		CALL("任务点对话",10104098,"燕南飞",5414,0,10010,10561)
	end
end,
--古城名胜
[5415] = function(idx)
	--找[PathNpc,守卫队长,10010,10644]打听商会位置
	if idx == 0 then
		CALL("任务点对话",10104096,"守卫队长",5415,0,10010,10644)
	end
end,
--江湖传闻
[5416] = function(idx)
	--[PathNPC,金十八,10010,9848]
	if idx == 0 then
		CALL("任务功能对话",5416,17007,"金十八","[PathNPC,金十八,10010,9848]",10010,9848)
	end
end,
--齐聚杭州
[5417] = function(idx)
	--去找师兄[PathNpc,谢天生,10010,12063]
	if idx == 0 then
		CALL("任务点对话",10104117,"真武 谢天生",5417,0,10010,12063)
	end
end,
--齐聚杭州
[5418] = function(idx)
	--去找师姐[PathNpc,云文君,10010,12064]
	if idx == 0 then
		CALL("任务点对话",10104118,"天香 云文君",5418,0,10010,12064)
	end
end,
--齐聚杭州
[5419] = function(idx)
	--去找师姐[PathNpc,乌云珠,10010,14164]
	if idx == 0 then
		CALL("任务点对话",10121049,"乌云珠",5419,0,10010,14164)
	end
end,
--危机初现
[5421] = function(idx)
	--询问[PathNpc,慕情,10010,9542]
	if idx == 0 then
		CALL("任务点对话",10104027,"慕情",5421,0,10010,9542)
	end
end,

--------362,第五回：妖魔小丑忙跳梁--------
--围攻凤凰
[5427] = function(idx)
	--将物资交给凤凰集[PathNpc,唐余福,10010,91]
	if idx == 0 then
		CALL("任务功能对话",5427,14002,"唐余福","将物资交给凤凰集[PathNpc,唐余福,10010,91]",10010,91)
	end
end,
--取道凤凰
[5428] = function(idx)
	--从[PathArea,枫荷桥,10010,300070]取道凤凰集
	if idx == 0 then
		CALL("任务点移动",10010,300070)
	end
end,
--余孽滋扰
[5442] = function(idx)
	--清理[PathMon,流杀门眼目,10010,101760]
	if idx == 0 then
		local monsters = {"流杀门眼目","堵截打手","流杀门刀手"}
		CALL("任务点杀怪",monsters,10010,101760)
	end
	--清理[PathMon,堵截打手,10010,101760]
	if idx == 1 then
		local monsters = {"堵截打手","流杀门刀手"}
		CALL("任务点杀怪",monsters,10010,101760)
	end
	--清理[PathMon,流杀门刀手,10010,101760]
	if idx == 2 then
		local monsters = {"流杀门刀手"}
		CALL("任务点杀怪",monsters,10010,101760)
	end
end,
--盘龙惩凶
[5443] = function(idx)
	--进入[PathArea,盘龙镇,10010,300101]
	if idx == 0 then
		CALL("任务点移动",10010,300101)
	end
end,
--盘龙敌踪
[5444] = function(idx)
	--找到呼救的[pathNpc,镇民,10010,302]
	if idx == 0 then
		CALL("任务点对话",10104023,"盘龙镇村民",5444,0,10010,302)
	end
end,
--蛛丝马迹
[5447] = function(idx)
	--清理[PathMon,流杀追击手,10010,101796]
	if idx == 0 then
		local monsters = {"流杀追击手","流杀门香主"}
		CALL("任务点杀怪",monsters,10010,101796)
	end
	--清理[PathMon,流杀门香主,10010,101796]
	if idx == 1 then
		local monsters = {"流杀门香主"}
		CALL("任务点杀怪",monsters,10010,101796)
	end
end,
--沉玉寻踪
[5449] = function(idx)
	--清理[PathMon,流杀门侍应,10010,101830]
	if idx == 0 then
		local monsters = {"流杀门侍应","流杀门刀手","流杀门高手"}
		CALL("任务点杀怪",monsters,10010,101830)
	end
	--清理[PathMon,流杀门刀手,10010,101830]
	if idx == 1 then
		local monsters = {"流杀门刀手","流杀门高手"}
		CALL("任务点杀怪",monsters,10010,101830)
	end
	--清理[PathMon,流杀门高手,10010,101830]
	if idx == 2 then
		local monsters = {"流杀门高手"}
		CALL("任务点杀怪",monsters,10010,101830)
	end
end,

--------900241,??--------

--------363,第六回：青衣黄雀捕螳螂--------
--坤宫倒转
[5450] = function(idx)
	--到宁王庄外找[PathNpc,金玉使,10010,10562]交谈
	if idx == 0 then
		CALL("任务点对话",10104099,"金玉使",5450,0,10010,10562)
	end
end,
--淬剑之殇
[5453] = function(idx)
	--去[PathArea,淬剑谷,10010,300133]找名匠孔雀
	if idx == 0 then
		CALL("任务点移动",10010,300133)
	end
end,
--真相大白
[5454] = function(idx)
	--找[PathNpc,燕南飞,10010,10570]对话
	if idx == 0 then
		CALL("任务点对话",10104101,"燕南飞",5454,0,10010,10570)
	end
end,
--夜照天星
[5457] = function(idx)
	--找[PathNpc,莫川,10010,9776]
	if idx == 0 then
		CALL("任务功能对话",5457,14005,"莫川","找[PathNpc,莫川,10010,9776]",10010,9776)
	end
end,
--东京梦华
[5460] = function(idx)
	--[PathArea,开封,10012,300558]
	if idx == 0 then
		CALL("任务点移动",10012,300558)
	end
end,

--------900043,??--------

--------900044,??--------

--------901241,??--------

--------900246,??--------

--------901242,??--------

--------901243,??--------

--------900248,??--------

--------181,第十三回：凤凰集内燕归来--------
--极恶女贼
[10804] = function(idx)
	--[PathMon,乌鸢,10010,10009]
	if idx == 0 then
		local monsters = {"乌鸢"}
		CALL("任务点杀怪",monsters,10010,10009)
	end
end,
--马不停蹄
[10805] = function(idx)
	--[PathMon,素巾雅奴,10010,101207]
	if idx == 0 then
		local monsters = {"素巾雅奴","锦衣女贼","雅奴狂徒"}
		CALL("任务点杀怪",monsters,10010,101208)
	end
	--[PathMon,锦衣女贼,10010,101209]
	if idx == 1 then
		local monsters = {"锦衣女贼","雅奴狂徒"}
		CALL("任务点杀怪",monsters,10010,101208)
	end
	--[PathMon,雅奴狂徒,10010,101208]
	if idx == 2 then
		local monsters = {"雅奴狂徒"}
		CALL("任务点杀怪",monsters,10010,101208)
	end
end,
--讲武学堂
[10808] = function(idx)
	--[PathNpc,李隼,10010,10262]
	if idx == 0 then
		CALL("任务功能对话",10808,18011,"李隼","[PathNpc,李隼,10010,10262]",10010,10262)
	end
end,
--红颜祸水
[10810] = function(idx)
	--[PathMon,搜寻侧院,10010,301083]
	if idx == 0 then
		CALL("任务点移动",10010,301083)
		--LogWarning("未知的任务处理:%s","[PathMon,搜寻侧院,10010,301083]")
	end
end,
--剑试蔷薇
[10831] = function(idx)
	--等级达到40
	if idx == 0 then
		CALL("等级限制",10831)
	end
end,
--界限突破贰
[10832] = function(idx)
	--等待突破:剑试蔷薇(40级)
	if idx == 0 then
		CALL("界限突破",10832)
	end
end,
--孔雀迷踪
[10833] = function(idx)
	--[PathArea,杭州涌金门,10010,300116]
	if idx == 0 then
		CALL("任务点移动",10010,300116)
	end
end,

--------182,第十四回：飞流翻浪现连环--------
--枯花落月
[10813] = function(idx)
	--[PathArea,进入灵峰湾,10010,300108]
	if idx == 0 then
		CALL("任务点移动",10010,300108)
	end
end,
--百里激荡-初入百里

--百里激荡-翻浪水寨

--百里激荡-长驱直入

--百里激荡-翻覆水寨

--掩风平浪-凌水之鲲
--掩风平浪-亡羊补牢
--落云卷岚-飞流水寨

--落云卷岚-折冲将军


--------900061,??--------

--------183,第十五回：皇图霸业梦一场--------
--天绝禅院
[10820] = function(idx)
	--询问[PathNpc,禅院守卫,10010,10642]
	if idx == 0 then
		CALL("任务功能对话",10820,18012,"禅院守卫","询问[PathNpc,禅院守卫,10010,10642]",10010,10642)
	end
end,
--似禅非禅-凶相毕露

--似禅非禅-激浊扬清 

--似禅非禅-游尘土梗

--四大金刚
[10822] = function(idx)
	--[PathMon,持国天,10010,10119]
	if idx == 0 then
		local monsters = {"持国天","增长天","广目天","多闻天"}
		CALL("任务点杀怪",monsters,10010,10122)
	end
	--[PathMon,增长天,10010,10120]
	if idx == 1 then
		local monsters = {"增长天","广目天","多闻天"}
		CALL("任务点杀怪",monsters,10010,10122)
	end
	--[PathMon,广目天,10010,10121]
	if idx == 2 then
		local monsters = {"广目天","多闻天"}
		CALL("任务点杀怪",monsters,10010,10122)
	end
	--[PathMon,多闻天,10010,10122]
	if idx == 3 then
		local monsters = {"多闻天"}
		CALL("任务点杀怪",monsters,10010,10122)
	end
end,
--不如归去
[10823] = function(idx)
	--[PathMon,胡不归,10010,10425]
	if idx == 0 then
		local monsters = {"胡不归"}
		CALL("任务点杀怪",monsters,10010,101694)
	end
	--[PathMon,天绝禅院罗汉,10010,101693]
	if idx == 1 then
		local monsters = {"天绝禅院罗汉","天悦禅院恶僧"}
		CALL("任务点杀怪",monsters,10010,101694)
	end
	--[PathMon,天悦禅院恶僧,10010,101694]
	if idx == 2 then
		local monsters = {"天悦禅院恶僧"}
		CALL("任务点杀怪",monsters,10010,101694)
	end
end,
--神秘高人-天绝独塔

--神秘高人-无名老僧

--东平涤荡
[10825] = function(idx)
	--[PathArea,小心侦查,10010,300127]
	if idx == 0 then
		CALL("任务点移动",10010,300127)
	end
end,
--静心养气
[10827] = function(idx)
	--等级达到50
	if idx == 0 then
		CALL("等级限制",10827)
	end
end,
--界限突破叁
[11074] = function(idx)
	--等待突破：天下盟会(50级)
	if idx == 0 then
		CALL("界限突破",11074)
	end
end,

--------900062,??--------

--------900063,??--------

--------900661,??--------

--------900064,??--------

--------900662,??--------

--------901061,??--------

--------900663,??--------

--------901062,??--------

--------900664,??--------

--------901063,??--------

--------900071,??--------

--------900072,??--------

--------900073,??--------
}