module("msk.npc",package.seeall)
local print = _G.printf
local prints = _G.prints
local sleep = msk.sleep
local function testout(...)
	if g_testModule then
		return print(...)
	end
end

local manInstanceIdOff = g_sig.manInstanceIdOff
local entityIdOff = g_sig.entityIdOff
local dataBase = g_sig.dataBase
local npcBase = msk.qcall(g_sig.GetNpcBase,0x12345678,2)
local mainPlayerBase = g_sig.mainPlayerBase
local openNpcCall = g_sig.OpenNpc
local manActorPlayerBase = g_sig.manActorPlayerBase
local npcDataXorKey = 0xA59CF5B6--加密KEY
local callGetMainPlayerId = g_sig.GetMainPlayerId
local callGetNpcAddressById = g_sig.GetNpcAddressById
local callGetBuffDataById = g_sig.GetBuffDataById
local g_cacheData = g_cacheData
--Actor
local getManIdCallOff = g_sig.getManIdCallOff
local mapActorMgnOff
local actorNameOff = g_sig.actorNameOff

--man mgn 偏移 --从dataBase索引
local manMgnOffset = {
	manNpc = 0x18,
	manOther = 0x1c,
	monsterNpc = 0x20,
}

local actorOffset = {
	posData = 0xa0,
}

local entityOffset = {
	id1 = 0xe0,
	id2 = 0xe4,
}

--man 偏移
local manOffset = {
	actorData = 0x110,
	tarid1 = g_sig.manTarIdOff+0x10,
	tarid2 = g_sig.manTarIdOff+0x14,
}

local playerXiuWeiOff = g_sig.playerXiuWeiOff
local baseof = g_sig.npcOffBase--相对上一次特征码的更新
local npc_offset_dword = {
	name = 0xa8+baseof,--UTF8 指针
	qixue = 0xe8+baseof,
	max_qixue = 0xec+baseof,
	shayi = 0xf0+baseof,--杀�?
	max_shayi = 0xf4+baseof,
	neixi = 0xf8+baseof,--内息 �?
	max_neixi = 0xfc+baseof,
	dingli = 0x10c+baseof,--定力
	max_dingli = 0x110+baseof,
	nengliang = 0x114+baseof,--剑意
	nengliang2 = 0x118+baseof,--剑意
	gongli = 0x134+baseof,--功力
	max_gongli = 0x138+baseof,
	lv = 0xd0+baseof,
	--150可能是一个结�?
	--x1 = 0x154,
	--y1 = 0x158,

	h = 0xd4,
	v = 0xd8,
	
	x = 0x90,
	y = 0x94,
	z = 0x98,

	--qs.CheckTargetIsFriend 用这个找
	buff = g_sig.npcDataBuffEndOff,
}

local npc_buff_off = {
	layerNum = 0x15,--byte
	time = 0x78,--float
}


function GetManPlayerActorData()
	return msk.dwords(manActorPlayerBase,0x14)
end
local manPlayer
function InitManDataIdOff()
	manPlayer = msk.gdata.GetDataBase("ManPlayer")
	local vptr = msk.dword(manPlayer+0x10)
	local call = msk.dword(vptr+g_sig.getManIdCallOff)
	manOffset.id1 = msk.dword(call+2)+0x10
	manOffset.id2 = msk.dword(call+8)+0x10
end
----[[

function GetManPlayerData()
	return msk.gdata.GetDataBase("ManPlayer")
end


local npc_mt = {}
npc_mt.__index = function (t,key)
	local man_
	if rawget(t,"m_") then--主玩家
		if key == "xiuwei" then
			return msk.dword(t.addr_+playerXiuWeiOff-4)
		elseif key == "lv" then
			return qs.GetMainplayerLevel()
		elseif key == "id" then
			return qs.GetMainPlayerId()
		elseif key == "name" then
			return qs.GetMainPlayerName()
		end
		-- if key == "h" or key == "v" then
			-- return msk.float(GetManPlayerActorData()+npc_offset_dword[key])
		-- elseif key == "x" then
			-- return msk.float(GetManPlayerActorData()+npc_offset_dword["x1"])
		-- elseif key == "y" then
			-- return msk.float(GetManPlayerActorData()+npc_offset_dword["y1"])
		-- end
		man_ = GetManPlayerData()
	else
		man_ = rawget(t,"man_")
	end
	--来源于actor和man的数据
	if key == "id1" then
		return msk.dword(man_+manOffset.id1)
	elseif key == "type" then
		return msk.str(msk.dword(man_+4))
	elseif key == "id2" then
		return msk.dword(man_+manOffset.id2)
	elseif key == "tarid1" then
		return msk.dword(man_+manOffset.tarid1)
	elseif key == "tarid2" then
		return msk.dword(man_+manOffset.tarid2)
	elseif key == "instanceId" then
		return msk.dword(man_+manInstanceIdOff)
	end	
		
	local off = npc_offset_dword[key]
	if not off then 
		return rawget(npc_mt,key)
	end
	if key == "x" or key == "y" or key == "z" or key == "h" or key == "v" then
		local actorData = msk.dword(man_+manOffset.actorData)
		if actorData == 0 then
			actorData = msk.dword(man_+manOffset.actorData - 4)
		end
		if actorData ~= 0 then
			local posData = msk.dword(actorData+actorOffset.posData)
			return posData ~= 0 and msk.float(posData+off) or 0
		else
			return 0
		end
	end
	if key == "name" then
		off = msk.dword(t.addr_+off)
		return msk.str(off)
	elseif key == "lv" then
		local lv = msk.byte(t.addr_+off)
		lv = lv + 0x25
		if lv > 255 then
			lv = lv - 256
		end
		return lv
	end
	--LogDebuging("xxx:%s",key)
	return bit.bxor(npcDataXorKey,msk.dword(t.addr_+off))
end

npc_mt.__newindex = function (t,key,v)
	local man_ = rawget(t,"m_") and GetManPlayerData() or rawget(t,"man_")
	if key == "tarid1" then
		return msk.sdword(t.man_+manOffset.tarid1,v)
	elseif key == "tarid2" then
		return msk.sdword(t.man_+manOffset.tarid2,v)
	end
	local off = npc_offset_dword[key]
	if off then
		if key == "x" or key == "y" or key == "z" or key == "h" or key == "v" then--面向 仅限于玩家
			local actorData = msk.dword(man_+manOffset.actorData)
			if actorData == 0 then
				actorData = msk.dword(man_+manOffset.actorData - 4)
			end
			local posData = msk.dword(actorData+actorOffset.posData)
			msk.sfloat(posData+off,v)
			--msk.sfloat(GetManPlayerData()+off,v)
			return
		end
	end
	--print("rawset:%s",key)
	rawset(t,key,v)
end

function npc_mt:IsNear(x,y,dis)
	dis = dis or 500
	return math.abs(self.x-x) < dis and math.abs(self.y-y) < dis
end

function npc_mt:Select(id1,id2)
	--do return end
	--printf("Select %x,%x",id1,id2)
	self.tarid1 = id1
	self.tarid2 = id2
end

local msqrt = math.sqrt
function npc_mt:FaceTo(x,y)
	local dx,dy = x-self.x,y-self.y
	local xb = msqrt(dx*dx+dy*dy)
	local h = dx/xb
	local v = dy/xb
	--prints("FaceTo",h,v)
	self.h = h
	self.v = v
	return h,v,xb
end
local _mabs = math.abs
function npc_mt:IsFaceTo(x,y)
	local h,v = self.h,self.v
	local xx1,xx2--象限
	local dx,dy = x-self.x,y-self.y
	local xb = msqrt(dx*dx+dy*dy)
	local th = dx/xb
	local tv = dy/xb
	if th >= 0 and tv >= 0 then
		xx1 = 1
	elseif th <= 0 and tv >= 0 then
		xx1 = 2
	elseif th <= 0 and tv <= 0 then
		xx1 = 3
	else
		xx1 = 4
	end
	if h >= 0 and v >= 0 then
		xx2 = 1
	elseif h <= 0 and v >= 0 then
		xx2 = 2
	elseif h <= 0 and v <= 0 then
		xx2 = 3
	else
		xx2 = 4
	end
	if xx1 == xx2 then return true end
	if _mabs(xx2-xx1) == 2 then return false end
	if (xx1 <= 2 and xx2 <= 2) or  (xx1 <= 4 and xx2 <= 4 and xx1 >= 3 and xx2 >= 3) then
		return _mabs(th)+_mabs(h) < 1
	else
		return _mabs(tv)+_mabs(v) < 1
	end
end 
                                                                                                                                                                                               
function npc_mt:Dis(x,y)
	return math.sqrt((self.x-x)*(self.x-x)+(self.y-y)*(self.y-y))
end

function npc_mt:MonitorBuff(skip)
	skip = skip or {}
	local ed = msk.dword(self.addr_+npc_offset_dword.buff)
	local bg = msk.dword(self.addr_+npc_offset_dword.buff-4)
	--printx()
	print("开始监控...")
	local timeinter = 0.2
	local layer,tm,id
	local bInit
	local orzBuff = {}
	local curBuff = {}
	local tbuf
	for i=bg,ed-1,0x80 do
		id = msk.dword(i)
		layer = msk.byte(i+0x15)
		orzBuff[id] = layer
	end
	msk.sleep(timeinter)
	local change = false
	local name,desc
	while true do
		--printf("check buff.....")
		ed = msk.dword(self.addr_+npc_offset_dword.buff)
		bg = msk.dword(self.addr_+npc_offset_dword.buff-4)
		for i=bg,ed-1,0x80 do
			id = msk.dword(i)
			layer = msk.byte(i+0x15)
			tm = msk.float(i+0x78)
			curBuff[id] = tm
			--printf("buff:%x",id)
		end
		for k,v in pairs(curBuff) do
			if not orzBuff[k] then
				if not skip[k] then
					name,desc = msk.api.GetBuffNameDesc(k)
					printf("新增BUFF:%x,%s-%s %.2fs",k,name,desc,v)
				end
			else
				orzBuff[k] = nil
			end
		end
		for k,v in pairs(orzBuff) do
			if not skip[k] then 
				name,desc = msk.api.GetBuffNameDesc(k)
				printf("失去BUFF:%x,%s-%s %.2fs",k,name,desc,v) 
			end
		end
		orzBuff = curBuff
		curBuff = {}
		msk.sleep(timeinter)
	end
end	


function npc_mt:MonitorTargetBuff(skip)
	local tid1,tid2,npc
	while true do
		LogDebuging("等待目标")
		while self.tarid1 == 0 do
			sleep(1)
		end
		tid1,tid2 = self.tarid1,self.tarid2
		npc = GetNpcById(tid1,tid2)
		skip = skip or {}
		local ed = msk.dword(npc.addr_+npc_offset_dword.buff)
		local bg = msk.dword(npc.addr_+npc_offset_dword.buff-4)
		--printx()
		print("开始监控:%s",npc.name)
		local timeinter = 0.2
		local layer,tm,id
		local bInit
		local orzBuff = {}
		local curBuff = {}
		local tbuf
		for i=bg,ed-1,0x80 do
			id = msk.dword(i)
			layer = msk.byte(i+0x15)
			--name,desc = msk.api.GetBuffNameDesc(id)
			LogDebuging("原有BUFF:%x %s-%s",id,msk.api.GetBuffNameDesc(id))
			orzBuff[id] = layer
		end
		msk.sleep(timeinter)
		local change = false
		local name,desc
		while self.tarid1 == tid1 and self.tarid2 == tid2 do
			--printf("check buff.....")
			ed = msk.dword(npc.addr_+npc_offset_dword.buff)
			bg = msk.dword(npc.addr_+npc_offset_dword.buff-4)
			for i=bg,ed-1,0x80 do
				id = msk.dword(i)
				layer = msk.byte(i+0x15)
				tm = msk.float(i+0x78)
				curBuff[id] = tm
				--printf("buff:%x",id)
			end
			for k,v in pairs(curBuff) do
				if not orzBuff[k] then
					if not skip[k] then
						name,desc = msk.api.GetBuffNameDesc(k)
						printf("新增BUFF:%x,%s-%s %.2fs",k,name,desc,v)
					end
				else
					orzBuff[k] = nil
				end
			end
			for k,v in pairs(orzBuff) do
				if not skip[k] then 
					name,desc = msk.api.GetBuffNameDesc(k)
					printf("失去BUFF:%x,%s-%s %.2fs",k,name,desc,v) 
				end
			end
			orzBuff = curBuff
			curBuff = {}
			msk.sleep(timeinter)
		end
	end
end	

function npc_mt:Open(id1,id2)
	return qs.TalkToNpc(self.instanceId)
	--return msk.api.OpenNpc(self.id1,self.id2)
end

function npc_mt:PrintAllBuff()
	local ed = msk.dword(self.addr_+npc_offset_dword.buff)
	local bg = msk.dword(self.addr_+npc_offset_dword.buff-4)
	--printx()
	local layer,tm,id,name,desc
	for i=bg,ed-1,0x80 do
		id = msk.dword(i)
		layer = msk.byte(i+0x15)
		tm = msk.float(i+0x78)
		name,desc = msk.api.GetBuffNameDesc(id)
		printf("  buffer:%x-%s %d %d %s",id,name or "??",layer,tm,desc or "??")
	end
end

function npc_mt:CheckHasBuffs(...)
	local tbuf = {...}
	local ed = msk.dword(self.addr_+npc_offset_dword.buff)
	local bg = msk.dword(self.addr_+npc_offset_dword.buff-4)
	--printx()
	local layer,tm,id
	local maxnum = 20
	for i=bg,ed-1,0x80 do
		for j=1,#tbuf do
			if msk.dword(i) == tbuf[i] then
				return true
			end
		end
		if maxnum < 0 then break end
		maxnum = maxnum - 1
	end
end

function npc_mt:CheckHasBuff(tid,layer)
	local ed = msk.dword(self.addr_+npc_offset_dword.buff)
	local bg = msk.dword(self.addr_+npc_offset_dword.buff-4)
	--printx()
	local layer,tm,id
	local maxnum = 20
	for i=bg,ed-1,0x80 do
		if msk.dword(i) == tid then
			return layer == nil or msk.byte(i+0x15) == layer
		end
		if maxnum < 0 then break end
		maxnum = maxnum - 1
	end
end

local function __CreateNpc(addr,m,actorData,newNpc)
	assert(addr and addr ~=0 and actorData and actorData ~= 0,(addr == 0 and "addr!") or (actorData == 0 and "actorData!"))
	newNpc = newNpc or {}
	newNpc.addr_ = addr
	newNpc.m_=m
	newNpc.actor_ = actorData
	setmetatable(newNpc,npc_mt)
	return newNpc
end

local mainPlayer
function GetMainPlayer()
	--if not mainPlayer then
	mainPlayer = __CreateNpc(msk.dword(mainPlayerBase),true,GetManPlayerActorData(),mainPlayer)
	mainPlayer.man_ = GetManPlayerData()	
	--end
	return mainPlayer
end

local mainPlayerIdMem 
function GetMainPlayerId()
	mainPlayerIdMem = mainPlayerIdMem or msk.new(8)
	msk.qcall(callGetMainPlayerId,0x12345678,0,mainPlayerIdMem)
	return msk.dword(mainPlayerIdMem),msk.dword(mainPlayerIdMem+4)
end

local function GetNpcAddress(id1,id2)
	return msk.qcall(callGetNpcAddressById,0x12345678,2,id2,id1)
end

local function __GetNpcById(id1,id2,data,t)
	local addr = GetNpcAddress(id1,id2)
	if addr ~= 0 then
		local newnpc = __CreateNpc(addr,false,data,t)
		--newnpc.id1 = id1
		--newnpc.id2 = id2
		return newnpc
	end
end

function OpenNpc(id1,id2)
	return msk.api.OpenNpc(id1,id2)
	--return msk.qcall(openNpcCall,0x12345678,2,id2,id1)
end

function PrintAllExternData()
	local bg = msk.dword(npcBase+0x4)
	local ed = msk.dword(npcBase+0x8)
	local ecx,maxnum,num,id1,id2,node,data
	local idx = 0
	local sidx
	for i=bg,ed-1,4 do
		ecx = msk.dword(i)
		maxnum = msk.dword(ecx+0xc)
		num  = msk.dword(ecx+0x10)
		ecx = msk.dword(ecx+0x8)--数组
		for i=0,maxnum-1 do
			node = msk.dword(ecx+i*4)
			id1 = msk.dword(node)
			id2 = msk.dword(node+4)
			sidx = 0
			while node ~= 0 and id1 ~= 0 do--数组的每一个位置都是一个链衿求余,多余数据插入链表
				printf("extern %d:%d:%d %x-%x %x",idx,i,sidx,id1,id2,node)
				sidx = sidx + 1
				node = msk.dword(node+0x10)
				id1 = msk.dword(node)
				id2 = msk.dword(node+4)
			end
		end
		idx = idx + 1
	end
end
--tp 1:血量等数据 4:初始化ID
function GetInstanceId(id1,tp)
	do return GetByInstanceId(id1) end
end

function GetNpcByInstanceId(insid,notnear,tp)
	do return GetByInstanceId(insid,notnear,tp) end
end

local function CheckSingleName(name,tname,notfit)
	return (notfit and string.find(name,tname)) or (not notfit and name == tname)
end

local function CheckTableName(name,tname,notfit)
	for i=1,#tname do
		if (notfit and string.find(name,tname[i])) or (not notfit and name == tname[i]) then
			return true
		end
	end
	return false
end

function PrintAll(tname,notfit,notnear,tid1,tid2,curnpc,filter)
	--prints("PrintAlls",tname,notfit,notnear,tid1,tid2,curnpc,filter)
	if mapActorMgnOff == nil then
		local _,jwoff = msk.gdata.GetDataBase("QSJianwenManager")
		mapActorMgnOff = jwoff+8--g_dataMaxOffset--通常是最后一个
	end
	local GeMapActorManager = msk.dwords(dataBase,mapActorMgnOff,0x8)
	testout("GeMapActorManager = %x",GeMapActorManager)
	local name = msk.dword(GeMapActorManager+0x4)
	assert(msk.str(name) == "GeMapActorManager")
	local bg = msk.dword(GeMapActorManager+0x10)
	local ed = msk.dword(GeMapActorManager+0x14)
	local id1,id2,posdata,x,y,z,data,npc
	local useIns = type(tname) == "number"
	local CheckNameFit = CheckSingleName
	local nameIsTb = false
	if type(tname) == "table" then
		CheckNameFit = CheckTableName
		useIns = type(tname[1]) == "number"
		nameIsTb = true
	end
	local lastnpc,lastdis,tmpnpc,lastSkipLv
	curnpc = curnpc or {}--省点内存 不要老是分配
	local tnpc = curnpc--用来重复使用
	local curdis,cskipLv
	local pl = GetMainPlayer()
	local tfit
	local iid
	local nearListDis = g_cacheData.nearListDis or 1000
	testout("total:%d",(ed-bg)/4)
	for i=bg,ed,4 do
		data = msk.dword(i)
		testout("data:%x",data)
		id1 = msk.dword(data+manOffset.id1)
		id2 = msk.dword(data+manOffset.id2)
		testout("id:%x-%x",id1,id2)
		posdata = msk.dword(data+0xa0)
		if id1 ~= 0 and id2 ~= 0 then
			if posdata ~= 0 then 
				curnpc = __GetNpcById(id1,id2,data,tnpc)
				if curnpc then
					if filter and filter[curnpc.addr_] then--过滤掉
						tnpc = curnpc
						curnpc = nil
					else
						testout("data:%x addr:%x id:%x-%x",data,curnpc.addr_,id1,id2)
					end
					
				end
			end
			if tname then--根据名字或者instanceId搜索
				cskipLv = curnpc and g_cacheData.skipNpcAddrs[curnpc.id1] or 0
				if curnpc and curnpc.qixue > 0 --已存在NPC并且血量大于0
					and (--排除列表的NPC优先级最低
						not lastdis--没有其他可选
						or notnear--无距离要求
						or ( cskipLv <= lastSkipLv )--否则过滤掉在排除列表的NPC
					)  then
					tfit = false
					if useIns then--tname 代表ID
						iid = curnpc.instanceId
						if nameIsTb then
							for n=1,#tname do
								if tname[n] == iid then
									tfit = true
									break
								end
							end
						else
							tfit = iid == tname
						end
						if tfit then
							--print("check:%s %d ok",curnpc.name,iid)
						end
					else--tname 代表名字
						tfit = CheckNameFit(curnpc.name,tname,notfit)
					end
					if tfit then
						if notnear then return curnpc end
						curdis = pl:Dis(curnpc.x,curnpc.y)
						if curdis < nearListDis then return curnpc end
						if not lastdis 
							or (lastSkipLv == nil or cskipLv < lastSkipLv)--或者已有的优先级太低
							or (
								curdis < lastdis --距离比已有的近
								and  cskipLv == lastSkipLv
							) then
							tmpnpc = curnpc
							curnpc = lastnpc
							lastnpc = tmpnpc
							lastdis = curdis
							tnpc = curnpc
							lastSkipLv = cskipLv
						end
					end
				else
					if curnpc then
						testout("check:%s failed",curnpc.name)
					end
				end
			elseif tid1 then--根据ID 搜索
				if curnpc and id1 == tid1 and id2 == tid2 then
					testout("get npc:%x,%x",id1,id2)
					return curnpc
				else
					--printf("skip:%x,%x",id1,id2)
				end
			elseif curnpc then--打印
			--x,y,z = msk.float(posdata+0x90),msk.float(posdata+0x94),msk.float(posdata+0x98)
				printf("%s:%d %d/%d %d %x-%x  %.2f,%.2f,%.2f %d addr_:%x actor_:%x man:%x",curnpc.name,curnpc.lv,curnpc.qixue,curnpc.max_qixue,GetInstanceId(id1) or 0,id1,id2,curnpc.x,curnpc.y,curnpc.z,pl:Dis(curnpc.x,curnpc.y),curnpc.addr_,curnpc.actor_,curnpc.man_ or 0)
			else
				testout("skip:%x-%x",id1,id2)
			end
		end
	end
	if lastnpc then
		LogDebuging("找到符合要求NPC:%s:%d %d/%d %x %x-%x  %.2f,%.2f,%.2f %d",lastnpc.name,lastnpc.lv,lastnpc.qixue,lastnpc.max_qixue,GetInstanceId(id1) or 0,id1,id2,lastnpc.x,lastnpc.y,lastnpc.z,pl:Dis(lastnpc.x,lastnpc.y)/100)
	end
	return lastnpc
end

function GetNpcByName(tname,notfit,notnear,filter)
	return GetByName(tname,notfit,notnear)
	--return PrintAll(tname,notfit,notnear,nil,nil,nil,filter)
end

function GetNpcById(id1,id2,t)
	return GetMonsterById(id1,id2,t)
	--printf("GetNpcById:%x,%x",id1,id2)
	--return PrintAll(nil,nil,nil,id1,id2,t)
end

function GetById(id1,id2,t)
	return GetNpcById(id1,id2,t)
end

local __cacheIdName = {}
function GetEntityNameById(id)
	local tname = __cacheIdName[id]
	if tname then return tname end
	local base = msk.gdata.GetDataBase("Data.Table.EntityTable.EntityTable_Retail")
	local hd,ed = msk.dword(base+0x40),msk.dword(base+0x44)
	for addr=hd,ed-1,0x130 do
		if msk.dword(addr) == id then
			tname = msk.dword(addr+4)
			tname = msk.str(tname)
			break
		end
	end
	tname = tname or ""
	__cacheIdName[id] = tname
	return tname
end

function GetEntityByName(name,notfit,notnear)
	local base = msk.gdata.GetDataBase("AxEntityMng")
	--printf("base:%x",base)
	local head = msk.dword(base+0x14)
	--printf("head:%x",head)
	local pl = GetMainPlayer()
	local maxnum = msk.dword(base+0x18)
	local node,data,id1,id2,pname,implData,x,y,z,posData,off,dis,id
	local lastnode,lastdata,lastid1,lastid2,lastpname,lastimplData,lastx,lasty,lastz,lastposData,lastoff,lastdis
	for i=0,maxnum-1 do
		node = msk.dword(head+i*4)
		--printf("node:%x",node)
		while node ~= 0 do
			data = msk.dword(node+0x8)
			--printf("data:%x",data)
			if data ~= 0 then
				id = msk.dword(data+entityIdOff)
				pname = GetEntityNameById(id)
				id1 = msk.dword(data+entityOffset.id1)
				--printf("id1:%x",id1)
				id2 = msk.dword(data+entityOffset.id2)
				--pname = msk.dword(data+actorNameOff)
				--pname = msk.str(pname) or "/??"
				if not name or (name == pname and not notfit) or (string.find(pname,name) and notfit) then
					implData = msk.dword(data+manOffset.actorData)
					if implData == 0 then
						implData = msk.dword(data+manOffset.actorData - 4)
					end
					posData = msk.dword(implData+actorOffset.posData)
					off = npc_offset_dword['x']
					x = msk.float(posData+off)
					off = npc_offset_dword['y']
					y = msk.float(posData+off)
					off = npc_offset_dword['z']
					z = msk.float(posData+off)
					dis = pl:Dis(x,y)
					if not name then
						printf("%x:%s %x-%x %.2f,%.2f,%.2f -- %d",data,pname,id1,id2,x,y,z,dis/100)
					elseif not lastdis or lastdis > dis then
						printf("get %s",pname)
						lastnode,lastdata,lastid1,lastid2,lastpname,lastimplData,lastx,lasty,lastz,lastposData,lastoff,lastdis = node,data,id1,id2,pname,implData,x,y,z,posData,off,dis
						if notnear then
							break
						end
					else
						--printf("skip %s",pname)
					end
				end
			end
			node = msk.dword(node+0x10)
		end
	end
	if not lastdis then return end
	local entityObj = {}
	entityObj.name = lastpname
	entityObj.x = lastx
	entityObj.y = lasty
	entityObj.z = lastz
	entityObj.id1 = lastid1
	entityObj.id2 = lastid2
	entityObj.dis = lastdis
	return entityObj
end

function PrintAllMan(mtype)
	print(mtype.."............")
	local base = msk.dwords(dataBase,manMgnOffset[mtype])
	--local base = 0x13063680--msk.gdata.GetDataBase("ManMng")
	local head = msk.dword(base+0x58)
	local max_num = msk.dword(base+0x5c)
	local num = msk.dword(base+0x60)
	local node,data,id1,id2,name,tp,actorData,tarid1,tarid2,instanceId
	print("total:%d",num)
	local t = {}
	local npc,addr
	for i=0,max_num-1 do
		node = msk.dword(head+i*4)
		while node ~= 0 do
			id1 = msk.dword(node)
			id2 = msk.dword(node+4)
			data = msk.dword(node+0x8)
			actorData = msk.dword(data+manOffset.actorData)
			if actorData == 0 then
				actorData = msk.dword(data+manOffset.actorData - 4)
			end
			addr = GetNpcAddress(id1,id2)
			if addr == 0 or actorData == 0 then
				LogDebuging("get error:%x,%x %x-%x",id1,id2,addr,actorData)
			else
				npc = __CreateNpc(addr,false,actorData,t)
				npc.man_ = data
				printf("%s lv:%d id:%x,%x insid:%x hp:%d/%d tid:%x,%x pos:%.2f,%.2f",npc.name,npc.lv,npc.id1,npc.id2,npc.instanceId,npc.qixue,npc.max_qixue,npc.tarid1,npc.tarid2,npc.x,npc.y)		
			end
			node = msk.dword(node+0x10)
		end
	end
end

function GetMonster(filterFun,...)
	local base = msk.dwords(dataBase,manMgnOffset["manNpc"])
	local head = msk.dword(base+0x58)
	local max_num = msk.dword(base+0x5c)
	local num = msk.dword(base+0x60)
	local node,data,id1,id2,tp,actorData,addr
	local t = {}
	local npc,lv
	local cSkipLv,lSkipLv
	local lastnpc,lastlv = nil,99999
	local filterNpc,lastFilterNpc
	local pl = GetMainPlayer()
	for i=0,max_num-1 do
		node = msk.dword(head+i*4)
		while node ~= 0 do
			id1 = msk.dword(node)
			id2 = msk.dword(node+4)
			--LogDebuging("Check:%x-%x",id1,id2)
			data = msk.dword(node+0x8)
			tp = msk.str(msk.dword(data+4)) or "??"
			if tp:upper() == "MANNPC" then
				actorData = msk.dword(data+manOffset.actorData)
				if actorData == 0 then
					actorData = msk.dword(data+manOffset.actorData - 4)
				end
				addr = GetNpcAddress(id1,id2)
				if actorData ~= 0 and addr ~= 0 then	
					npc = __CreateNpc(addr,false,actorData,t)
					npc.man_ = data
					lv = filterFun(pl,npc,...)
					cSkipLv = g_cacheData.skipNpcAddrs[id1] or 0		
					if lv == 0 and cSkipLv == 0 then return npc end
					if lv ~= 99999 and (lastlv == 99999 or (lSkipLv == nil or cSkipLv < lSkipLv) or (lastlv > lv and cSkipLv == lSkipLv )) then
						lastlv = lv
						lSkipLv = cSkipLv
						t = lastnpc or {}
						lastnpc = npc
					elseif g_cacheData.printSkip then
						LogWarning("跳过:%s-%d lastlv:%s lSkipLv:%s cSkipLv:%s lv:%s",npc.name,npc.qixue,
							tostring(lastlv),
							tostring(lSkipLv),
							tostring(cSkipLv),
							tostring(lv))
					end
				else
					--npc = __CreateNpc(addr,false,actorData,t)
					LogDebuging("??? %s",tp)
				end
			elseif g_cacheData.printSkip then
				addr = GetNpcAddress(id1,id2)
				actorData = msk.dword(data+manOffset.actorData)
				if actorData == 0 then
					actorData = msk.dword(data+manOffset.actorData - 4)
				end
				if addr ~= 0 and actorData ~= 0 then
					npc = __CreateNpc(addr,false,actorData,t)
					npc.man_ = data
					LogWarning("跳过2:%s-%s-%d lastlv:%s lSkipLv:%s cSkipLv:%s lv:%s",tp,npc.name,npc.qixue,
								tostring(lastlv),
								tostring(lSkipLv),
								tostring(cSkipLv),
								tostring(lv))
				else
					LogWarning("跳过??:%s lastlv:%s lSkipLv:%s cSkipLv:%s lv:%s",tp,
								tostring(lastlv),
								tostring(lSkipLv),
								tostring(cSkipLv),
								tostring(lv))
				end
			end
			node = msk.dword(node+0x10)
		end
	end
	if lastlv ~= 99999 then
		LogDebuging("Check:%s ",lastnpc.name)
		return lastnpc
	else
		--LogDebuging("lastlv:%d %s",lastlv or -1,lastnpc and lastnpc.name or "??")
	end
end

local function __checkNameFit(npc,name,notfit)
	if name == nil then return true end
	local tp = type(name)
	if tp == "table" then
		for i=1,#name do
			if (not notfit and name[i] == npc.name) or (notfit and string.find(npc.name,name[i])) then
				return true
			end
		end
	else
		if (not notfit and name == npc.name) or (notfit and string.find(npc.name,name)) then
			return true
		end
	end
end

local function __talkNpcFilter(pl,npc)
	if msk.api.GetNpcAnyTalkId(npc.instanceId) == nil then  return 99999 end
	local dis = pl:Dis(npc.x,npc.y)/100
	return dis
end

local function __monsterNameFilter(pl,npc,name,notfit,notnear)
	if npc.qixue == 0 then return 99999 end
	if 	__checkNameFit(npc,name,notfit) then --判断名字是否匹配的上
		if notnear then return 0 end
		local dis = pl:Dis(npc.x,npc.y)/100
		if dis < 10 then return 0 end--就在旁边
		return dis
	end
	return 99999
end

local function __PrintSkipNpc(pl,npc,name)
	local namestr = type(name) == "table" and table.concat(name,",") or tostring(name)
	if npc.qixue == 0 then 
		LogWarning("打怪跳过:%s-%s %d",npc.name,namestr,npc.qixue)
		return 99999 
	end
	if 	__checkNameFit(npc,name,notfit) then --判断名字是否匹配的上
		if notnear then return 0 end
		local dis = pl:Dis(npc.x,npc.y)/100
		if dis < 10 then return 0 end--就在旁边
		return dis
	end
	LogWarning("打怪跳过:%s-%s %d",npc.name,namestr,npc.qixue)
	return 99999
end

local function __monsterInstanceIdFilter(pl,npc,insid,notnear,tp)
	if npc.qixue == 0 then return 99999 end
	if 	npc.instanceId == insid then
		if notnear then return 0 end
		local dis = pl:Dis(npc.x,npc.y)/100
		if dis < 10 then return 0 end--就在旁边
		return dis
	end
	return 99999
end

local function __GetMonsterById(tid1,tid2,mtp,t)
	local base = msk.dwords(dataBase,manMgnOffset[mtp])
	local head = msk.dword(base+0x58)
	local max_num = msk.dword(base+0x5c)
	local num = msk.dword(base+0x60)
	local node,data,id1,id2,tp,actorData,addr
	t = t or {}
	local npc,lv
	local lastnpc,lastlv = nil,99999
	local filterNpc,lastFilterNpc
	local pl = GetMainPlayer()
	for i=0,max_num-1 do
		node = msk.dword(head+i*4)
		while node ~= 0 do
			id1 = msk.dword(node)
			id2 = msk.dword(node+4)
			--LogDebuging("Check:%x-%x",id1,id2)
			data = msk.dword(node+0x8)
			tp = msk.str(msk.dword(data+4)) or "??"
			if id1 == tid1 and id2 == tid2 then
				actorData = msk.dword(data+manOffset.actorData)
				if actorData == 0 then
					actorData = msk.dword(data+manOffset.actorData - 4)
				end
				addr = GetNpcAddress(id1,id2)
				if actorData ~= 0 and addr and addr ~= 0 then	
					npc = __CreateNpc(addr,false,actorData,t)
					npc.man_ = data
					return npc
				end
			end
			node = msk.dword(node+0x10)
		end
	end
end

function PrintSkipNpc(name)
	GetMonster(__PrintSkipNpc,name)
end

function GetMonsterById(tid1,tid2,t)
	return __GetMonsterById(tid1,tid2,"manNpc",t) or __GetMonsterById(tid1,tid2,"monsterNpc",t) or __GetMonsterById(tid1,tid2,"manOther",t)
end

function GetByInstanceId(insid,notnear,tp)
	insid = tonumber(insid)
	if insid == nil or insid == 0 then return nil end
	return GetMonster(__monsterInstanceIdFilter,insid,notnear,tp)
end

local __lastName,__lastNameCount = nil,0
function GetByName(name,notfit,notnear)
	if __lastName == name then
		__lastNameCount = __lastNameCount + 1
	else
		__lastName = name
		__lastNameCount = 1
	end
	local npc = GetMonster(__monsterNameFilter,name,notfit,notnear)
	if npc == nil and __lastNameCount > 18 then
		msk.api.BackAndEnterWorld()
		__lastNameCount = 0
	elseif npc ~= nil then
		__lastNameCount = 0
	end
	return npc
end

function GetNearTalkNpc()
	return GetMonster(__talkNpcFilter)
end

function TestMainPlayer()
	print("test main player")
	local pl = GetMainPlayer()
	printf("%s lv:%d hp:%d/%d mp:%d/%d 杀:%d/%d 定:%d/%d 气：%d,%d 功:%d pos:%.2f,%.2f,%.2f face:%.2f,%.2f",
		pl.name,pl.lv,pl.qixue,pl.max_qixue ,pl.neixi,pl.max_neixi,pl.shayi,pl.max_shayi,pl.dingli,pl.max_dingli,pl.nengliang,
		pl.nengliang2,pl.gongli,pl.x,pl.y,pl.z,pl.h,pl.v
	)
	pl:PrintAllBuff()
end

function Test()
	--PrintAll()
	TestMainPlayer()
	print("now test entity")
	GetEntityByName()
	PrintAllMan("manNpc")
	PrintAllMan("monsterNpc")
	PrintAllMan("manOther")
end

printf("msk.npc ok")