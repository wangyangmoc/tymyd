
local datiBase = g_sig.datiBase
function PrintAllDatiData()
	local datiData = {}
	local data = msk.dword(datiBase)
	datiData.count_down_sec = msk.dword(data+0x20)
	datiData.left_npc = msk.dword(data+0x24)
	datiData.left_count = msk.dword(data+0x24)
	datiData.left_total_count = msk.dword(data+0x28)
	datiData.right_count = msk.dword(data+0x30)
	datiData.right_total_count = msk.dword(data+0x34)
	datiData.add_time = msk.dword(data+0x38)
	datiData.desc = msk.dword(data+0x3c)
	datiData.desc = msk.str(datiData.desc)
	datiData.info = qs.GetGameString("AS_QG_QUESTION_QIYU_INFO")
	local npcDataBg = msk.dword(data+0x60)
	local npcDataEd = msk.dword(data+0x64)
	local npcData = {}
	for addr = npcDataBg,npcDataEd-1,8 do
		npcData[#npcData+1] = {msk.dword(addr),msk.dword(addr+4)}
	end
	datiData.npcIds = npcData
	
	npcDataBg = msk.dword(data+0x70)
	npcDataEd = msk.dword(data+0x74)
	npcData = {}
	for addr = npcDataBg,npcDataEd-1,4 do
		npcData[#npcData+1] = {msk.dword(addr)}
	end
	datiData.npcLeftCounts = npcData
	
	npcDataBg = msk.dword(data+0x70)
	npcDataEd = msk.dword(data+0x74)
	npcData = {}
	for addr = npcDataBg,npcDataEd-1,4 do
		npcData[#npcData+1] = {msk.dword(addr)}
	end
	datiData.npcLeftCounts = npcData
	
	npcDataBg = msk.dword(data+0x90)
	npcDataEd = msk.dword(data+0x94)
	npcData = {}
	for addr = npcDataBg,npcDataEd-1,8 do
		npcData[#npcData+1] = {msk.dword(addr),msk.dword(addr+4)}
	end
	datiData.visitedNpcs = npcData
	
	local mapDataBase=  msk.dword(data+0x4c)
	local mapData = {}
	mapData.map = msk.dword(mapDataBase+8)
	mapData.mapPos = msk.dword(mapDataBase+0xc)
	return datiData
end
	
function MonitorDati()
	local data = msk.dword(datiBase)
	markNpcs = markNpcs or {}
	markNpcTypes = markNpcTypes or {}
	local vistNpcs
	local npcDataBg,npcDataEd,id1,id2,npc
	local idx,leftCount,markType
	local npcVistBg,npcVistEd,npcLeftBg,npcLeftEd
	local lastIn = false
	while true do
		if msk.dword(data+0x20) > 0 then
			lastIn = true
			npcLeftBg = msk.dword(data+0x70)
			npcLeftEd = msk.dword(data+0x74)
			npcDataBg = msk.dword(data+0x60)
			npcDataEd = msk.dword(data+0x64)
			idx = 0
			npcVistBg = msk.dword(data+0x90)
			npcVistEd = msk.dword(data+0x94)
			vistNpcs = {}
			for addr = npcVistBg,npcVistEd-1,8 do
				vistNpcs[msk.dword(addr)] = true
			end
			for k,v in pairs(markNpcs) do
				DgnEventRemoveObjectToMinimap(markNpcTypes[k], 100+v) 
			end
			markNpcs = {}
			markNpcTypes = {}
			for addr = npcDataBg,npcDataEd-1,8 do
				id1,id2 = msk.dword(addr),msk.dword(addr+4)
				leftCount = msk.dword(npcLeftBg+idx*4)
				npc = msk.npc.GetNpcById(id1,id2)
				markType = leftCount == 0 and "Light" or "Boss"
				if vistNpcs[id1] then
					markType = "FriendNPC"
				end
				if npc then
					markNpcTypes[id1] = markType
					markNpcs[id1] = idx
					DgnEventAddObjectToMinimap(markType,npc.x,npc.y,100+idx ) 
				end
				idx = idx + 1
			end
		elseif lastIn then
			lastIn = false
			for k,v in pairs(markNpcs) do
				DgnEventRemoveObjectToMinimap(markNpcTypes[k], 100+v) 
			end
			markNpcs = {}
			markNpcTypes = {}
		end
		sleep(3)
	end
end