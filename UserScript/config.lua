module("msk.config",package.seeall)

fightDis = 50--战斗范围
pathDis = 5--寻路范围
pathTimeOut = 240--寻路超时
bagNum = 64

local identitys = {"杀手","猎户"}
local pros = {"真武","天香"}
local menhuis = {"系统推荐","帝王州","寒江城","水龙吟","万里杀"}
local allCounfigs = {
	{"identity",tonumber,"默认配置 %t\n默认身份: %l|杀手|",0},--猎户|
	{"pro",tonumber,"默认职业: %l|真武|天香|",0},
	{"menhuiName",tonumber,"默认盟会: %l|系统推荐|帝王州|寒江城|水龙吟|万里杀|",0},
	--{"anshaCount",tonumber,"暗杀配置(星级奖励为41-60阶段) %t\n暗杀次数: %i[1,15]",12},
	--{"anshaReward",tonumber,"暗杀最低奖励(41级): %i[40000,60000]",60000},
	{"shengsiQianMoney",tonumber,"生死签最高价: %i[0,60000]",40000},
	{"anshaPinlv",tonumber,"暗杀榜刷新频率(秒): %r[0.2,3.0,0.1]",1.0},
	{"lockLv41",tonumber,"41后不升级: %b",1},
	{"invertPage",tonumber,"倒序翻页(0为不倒序): %i[0,250]",250},
	{"anshaStar1",tonumber,"★(4金): %b[不接,接取]",1},
	{"anshaStar2",tonumber,"★★(5金): %b[不接,接取]",1},
	{"anshaStar3",tonumber,"★★★(6金): %b[不接,接取]",1},
	{"anshaStar4",tonumber,"★★★★(5纯银): %b[不接,接取]",0},
	{"anshaStar5",tonumber,"★★★★★(10纯银): %b[不接,接取]",0},
	{"dailyLiveNess",tonumber,"每日配置 %t\n每日活跃: %b",1},
	{"buyDailyGrift",tonumber,"会员每日礼包: %b",1},
	{"pointDaily",tonumber,"买减负令做日常: %b",1},
	{"ipAccountNum",tonumber,"拨号配置 %t\n单IP上号数量: %i",1000},
	{"bhAccount",tostring,"账号: %s",""},
	{"bhPassword",tostring,"密码: %s",""},
	{"beVipAfter41",tonumber,"其他配置 %t\n自动加入VIP: %b",1},
	{"teamNum",tonumber,"组队人数: %i[1,5]",5},
	--{"ipAccountNum",tonumber,"完成指定账号数量后重拨: %i",1000},
}

local vals = g_client:hgetall("configVal")
local val,t,k
for i=1,#allCounfigs do
	t = allCounfigs[i]
	k = t[1]
	val = t[2](vals[k]) or t[4]
	msk.config[k] = val
end

msk.config.identity = identitys[msk.config.identity+1]
msk.config.defaultPro = pros[msk.config.pro+1]
msk.config.defaultMenhuiName = menhuis[msk.config.menhuiName+1]