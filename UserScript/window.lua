local Offset_Control1=0x0000004C
local Offset_Control2=0x0000002C
local Offset_Control3=0x0000002C
local Offset_ControlVisible=0x0000003E
local Offset_ControlMark=0x0000006E
local Offset_ControlShl=0x0000000E 
local Offset_ControlObj=0x0000007C
local Offset_ControlName=0x00000044
local Offset_UI_IsText=0x000000A8
local RM_DWORD = msk.dword
local RM_BYTE = msk.byte
local RM_WORD = msk.word
local windowMt = {}
local windowProOff = {
	vis = 0x0000003E,
	mark = 0x0000006E,
	name = 0x00000044,
	isText = 0x000000A8,
	obj = 0x0000007C,
	
}
windowMt.__index = function(t,key)
	local PObject_PPTR = rawget(t,"obj_")
	if key == "visible" then
		return bit.rshift(RM_WORD(PObject_PPTR + Offset_ControlVisible),Offset_ControlShl)
	elseif key == "enable" then
		return RM_WORD(PObject_PPTR + 0xDC)
	elseif key == "name" then
		local dwNameAddr = RM_DWORD(PObject_PPTR + Offset_ControlName)
		if dwNameAddr ~= 0 then
			dwNameAddr = RM_DWORD(dwNameAddr + 0x08)
			dwNameAddr = RM_DWORD(dwNameAddr)
			return msk.str(dwNameAddr)
		else
			return ""
		end
	elseif key == "text" then
		if RM_DWORD(PObject_PPTR + Offset_UI_IsText) == -1 then
			local dwDescAddr = RM_DWORD(PObject_PPTR + Offset_UI_Text) + 7;
			if dwDescAddr ~= 0 and bit.band(dwDescAddr, 0xFFFF0000) ~= 0 then --[[字符串&0xFFFF0000 不为0--]]
				return msk.str(dwDescAddr)
			end
		end
		return "nil"
	end
end

function __PrintWindow(dwBase, nObjIndex)
    if RM_BYTE(dwBase + Offset_ControlVisible) < 0x80 or RM_WORD(dwBase + Offset_ControlMark) ~= 0xFFFF then
        return;
    end

    --控件是否可見
    if bit.rshift(RM_WORD(dwBase + Offset_ControlVisible), Offset_ControlShl) ~= 1 then
        return;
    end

    local PPTR = 0;
    local PStartAddr = 0;
    local dwArrayNum = 0;

    --[[对象基址--]]
    PPTR = dwBase;
    --[[对象起始地址--]]
    PStartAddr = RM_DWORD(PPTR + Offset_ControlObj + 0x00);
    --[[对象数量--]]
    dwArrayNum = RM_DWORD(PPTR + Offset_ControlObj + 0x04);
    local nName
    local nDesc

    --[[遍历数组--]]
    local dwArrayAddr = 0;
    local PArrayAddr = 0;
	local wnd = {}
	setmetatable(wnd,windowMt)
	local PObject_PPTR
	local dwNameAddr
    for nIndex = 0, dwArrayNum do
        PArrayAddr = PStartAddr + nIndex * 0x0C;
        --[[判断该地址是否可读--]]
        if PArrayAddr ~= 0 then
            --[[对象指针--]]
            PObject_PPTR = RM_DWORD(PArrayAddr)
            if (PObject_PPTR ~= 0) then
                --[[保存对象--]]
                wnd.obj_ = PObject_PPTR
                nObjIndex = nObjIndex + 1
				LogDebuging("%s-%s",wnd.name,wnd.text)
                __PrintWindow(PObject_PPTR, nObjIndex)
            end
        end
    end
end

function PrintAllWindow()
	local Base_Control = 0x02143AD8;
	local PPTR = 0;
    local PPTR = RM_DWORD(Base_Control);
    PPTR = RM_DWORD(PPTR + 9 * 4);
    PPTR = RM_DWORD(PPTR + Offset_Control1);
    PPTR = RM_DWORD(PPTR + Offset_Control2);
    PPTR = RM_DWORD(PPTR + Offset_Control3);
    PPTR = RM_DWORD(PPTR + 0x04);

    local nObjIndex = 0;
	__PrintWindow(PPTR, nObjIndex);
end