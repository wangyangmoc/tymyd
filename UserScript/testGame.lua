
local sleep = msk.sleep
local bin2hex = msk.tools.bin2hex
local hex2bin = msk.tools.hex2bin
local hex2bin2= msk.tools.hex2bin2
local mconfig = msk.config
local SendStr = msk.pack.SendStr
local Sendf = msk.pack.Sendf
local Send = msk.pack.Send

--更新时测试一下
local function TestAllModule()
	g_testModule = true
	print("--------------------test ui--------------------")
	msk.ui.Test()
	print("--------------------test skill--------------------")
	msk.skill.Test()
	--print("--------------------test hook--------------------")
	--msk.hook.Test()
	print("--------------------test npc--------------------")
	msk.npc.Test()
	print("--------------------test api--------------------")
	msk.api.Test()
	print("--------------------test data--------------------")
	msk.gdata.Test()
	print("--------------------test pack--------------------")
	msk.pack.Test()
	print("--------------------test item--------------------")
	msk.item.Test()
	g_testModule = false
end
--各种传送到达的地点
local function PrintTransferData(tid)
	local base = msk.gdata.GetDataBase("Data.Table.TransferTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,map,x,y,name
	for addr = hd,ed-1,0x2c do
		id = msk.dword(addr)
		map = msk.dword(addr+4)
		if tid == nil or tid == id then
			x = msk.dword(addr+0xc)
			y = msk.dword(addr+0x10)
			name = msk.dword(addr+0x1c)
			name = msk.str(name)
			if tid then
				return map,x,y,name
			end
			LogDebuging("%x %x:%s %s:%d,%d",addr,id,name,qs.GetMapRealName(map),x/100,y/100)
		end
	end
end

local function PrintTeleportData()
	local base = msk.gdata.GetDataBase("Data.Table.MapTeleportTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,map,x,y,name,id2
	for addr = hd,ed-1,0x74 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+4)
		map,x,y,name = PrintTransferData(id2)
		if x then
			LogDebuging("%x %d,%d %s",addr,x/100,y/100,name)
		end
	end
end

local function PrintMapUiInst()
	--StationPoint 传送
	local base = msk.gdata.GetDataBase("Data.ClientTable.MapTable.MapUiInstNewTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,id2,map,x,y,name,funtext,funtype
	local cmap = msk.api.GetCurrentMapId()
	for addr = hd,ed-1,0x54 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+0x8)
		if id ~= 0 then
			map = msk.dword(addr+4)
			name = msk.dword(addr+0x1c)
			name = msk.str(name)
			funtext = msk.dword(addr+0x2c)
			funtext = msk.str(funtext)
			funtype = msk.dword(addr+0x3c)
			funtype = msk.str(funtype)
			if cmap == map then
				x,y = msk.api.GetMapInsData(map,id2)
				x = x or 0
				y = y or 0
				LogDebuging("%x:%s:%x %s:%s:%s %d,%d",addr,qs.GetMapRealName(map),id2,funtype,funtext,name,x/100,y/100)
			else
				--LogDebuging("%s:%x %s:%s:%s",qs.GetMapRealName(map),id2,funtype,funtext,name)
			end
		end
	end
end
local function FindFunText(node,tid,ted)
	local id = msk.dword(node+0x10)
	if tid > id then
		return FindFunText(msk.dword(node),tid,ted)
	elseif tid < id then
		return FindFunText(msk.dword(node+4),tid,ted)
	end
	local name = msk.dword(node+0x18)
	return msk.str(name) or "nil"
end
local __printedItemIds
local function PrintTreeCfg(node,cid,ced,tar)
	if node == nil then 
		local cbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTreeCfgTable_Retail")
		ced = cbase+0x44
		node = msk.dword(cbase+0x4c)
	end
	tar = tar or 2
	if node == 0 or node == ced then 
		LogDebuging("find error:%x",cid)
		return 
	end
	local id = msk.dword(node+0x10)
	if cid > id then
		return PrintTreeCfg(msk.dword(node),cid,ced,tar)
	elseif cid < id then
		return PrintTreeCfg(msk.dword(node+4),cid,ced,tar)
	end
	local cdata = node+0x14
	local tcname = msk.dword(cdata+0xc)
	local tcname = msk.str(tcname)
	LogDebuging(string.rep(" ",tar).."treecfg:%x %s %x",id,tcname,cdata)
	__printedItemIds[id] = true
	local tcid
	for i=0,9 do
		tcid = msk.dword(cdata+0x14+i*4)
		if tcid ~= 0 and __printedItemIds[tcid] == nil then
			PrintTreeCfg(nil,tcid,ced,tar+2)
		end
	end
end
local function PrintFunEntry(node,nid,ed,tar)
	if node == 0 or node == ed  then 
		LogDebuging("找不到:%x %x %x",nid,node,ed)
		return 
	end
	local id = msk.dword(node+0x10)
	--LogDebuging("PrintFunEntry:%x-%x %x",nid,id,node)
	if nid > id then
		return PrintFunEntry(msk.dword(node),nid,ed,tar)
	elseif nid < id then
		return PrintFunEntry(msk.dword(node+4),nid,ed,tar)
	end
	--treecfg
	local cbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTreeCfgTable_Retail")
	local ced = cbase+0x44
	local cbg = msk.dword(cbase+0x4c)
	--text
	local tbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTextTable_Retail")
	local ted = tbase+0x44
	local tbg = msk.dword(tbase+0x4c)
	
	local fid,talkid,talkstr,fname
	local data = node+0x14
	--talkid = msk.dword(data+4)
	--talkstr = FindFunText(tbg,talkid,ted)
	--LogDebuging("%x %s %x",id,talkstr,node)
	for i=0,9 do
		fid = msk.dword(data+0x10+i*4)
		if fid ~= 0 then
			LogDebuging("%x fid:%x",id,fid)
			PrintTreeCfg(cbg,fid,ced,tar)
		end
	end
end
local function PrintAllFunEntry(nid)
	__printedItemIds = {}
	local base = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncEntryTable_Retail")
	local ed = base+0x44
	local bg = msk.dword(base+0x4c)
	PrintFunEntry(bg,nid,ed)
end

local function PrintDataBase(dname)
	local base = msk.gdata.GetDataBase(dname)
	LogDebuging("%s:%x",dname,base)
end

local function PrintUiName()
	local uiAddr = msk.ui.GetPaneByName("QSUISelectCharacterPanel",true)
	local base = msk.dword(uiAddr+0x74)
	base = msk.dword(base)
	local p0h = msk.dword(base+4)
	local ppv = msk.dword(p0h+0xc)
	local pjh = msk.dword(ppv+0x1bc)
	local map = msk.dword(pjh+0x8)
	local maxnum = msk.dword(map+4)
	map = map + 8
	local addr,nxt,pt,name,hash,len,oty,ref
	local f = io.open("xxx.txt","w")
	for i=0,maxnum do
		addr = map+i*8
		nxt = msk.dword(addr)
		if nxt ~= -2 then
			pt = msk.dword(addr+4)
			name = msk.dword(pt)
			len = msk.dword(pt+0x14)
			hash = msk.dword(pt+0x10)
			ref = msk.dword(pt+0xc)
			name = msk.str(name)
			--LogDebuging("%d:%s-%s",i,oty or "nil",name or "nil")
		end
	end
	f:close()
	--while
end

local identityBase = 0x2171AA0
local identityInfoOffset = 0x1120
function GetIdSkilllv(tiid,tsid)
	local num = msk.dword(identityBase+0x1a4)
	 if num == 0 then
		return 0
	 end
	 local esi = identityBase+0x1b3
	 local iid,skillNum,sillId,skillAddr,skillLv,skillAddrHead
	 local idx = 1
	 for i=0,num-1 do
		esi= esi+ i*0xd5
		iid = msk.byte(esi-0xa)
		if iid == tiid or tiid == nil then
			skillNum = msk.word(esi)
			skillAddrHead = esi+2
			for j=0,skillNum-1 do
				skillAddr= skillAddrHead+j*2
				sillId = msk.byte(skillAddr)
				if tsid == sillId then
					skillLv = msk.byte(skillAddr+1)
					return skillLv
				elseif tsid == nil then
					skillLv = msk.byte(skillAddr+1)
					LogDebuging("%d:%x %d",iid,sillId,skillLv)
				end
			end
		end
	 end
	 return 0
end

local __identitySkillMt = {}
__identitySkillMt.__index = function(t,k)
	local addr_ = rawget(t,"addr_")
	if k == "name" then
		return msk.str(msk.dword(addr_+0x10))
	elseif k == "maxLv" then
		return msk.dword(addr_+8)
	elseif k ==  "lv" then
		return GetIdSkilllv(identityId,t.id)
	end
end
local function ConsumeSatisfied(id,count,type)
    if(type == qs.CONT_SLOT_MONEY) then
        --print("GetCurMoney",qs.GetCurMoney(),count)
        return qs.GetCurMoney() >= count
    elseif (type == qs.CONT_SLOT_ITEM) then
        return qs.HasItem(id,count)        
    elseif (type == qs.CONT_SLOT_TALENT_EXP) then
        return qs.GetIdentityExp() >= count
    elseif (type == qs.CONT_SLOT_EXP) then
        return qs.GetCurExp() >= count
    end
    return false
end
function __identitySkillMt:LiveUp()
	 
end
function __identitySkillMt:CanLiveUp()
	local lv = t.lv+1
	local addr_ = self.addr_
	local subBase = msk.dword(addr_+0x58)
	subNum = msk.dword(addr_+0x5c)
	local rmoney,rxiuwei,rlilian,rid,ritem
	for m=0,subNum -1 do
		subAddr= msk.dword(subBase+m*4)
		while subAddr ~= 0 do
			subLv = msk.dword(subAddr)
			if subLv == lv then
				subAddr = subAddr + 4
				if qs.GetMainplayerLevel() < msk.dword(subAddr+0x10c) then
					return false
				end
				rmoney = msk.dword(subAddr+0x10c)
				rxiuwei = msk.dword(subAddr+0x138)
				rlilian = msk.dword(subAddr+0x13c)
				if not ConsumeSatisfied(nil,rmoney,5) or not ConsumeSatisfied(nil,rxiuwei,4) or not ConsumeSatisfied(nil,rlilian,0xb) then
					return false
				end
				for itemAddr = subAddr+0x110,subAddr+0x138-1,8 do
					rid = msk.dword(itemAddr)
					if rid ~= 0 and not ConsumeSatisfied(rid,msk.dword(itemAddr+4),2) then
						return false
					end
				end
				return true
			end
			subAddr = msk.dword(subAddr+0x1fc)
		end
	end
end


function PrintAllIdentity(tiid)
	local idNum = msk.dword(identityBase+identityInfoOffset+8)
	local base = msk.dword(identityBase+identityInfoOffset+4)
	local offset = tiid%idNum
	local idAddr = msk.dword(base+offset*4)
	while idAddr ~= 0 and msk.dword(idAddr) ~= tiid do
		idAddr = msk.dword(idAddr+0xd0)
	end
	if idAddr == 0 then return end
	local skillAddr,skillId,skillBase,skilNum,skillName,skillMaxLv,skillLv,skillData
	local subAddr,subLv,subDesc,subBase,subNum,subId
	identityId = msk.dword(idAddr)
	idName = msk.dword(idAddr+8)
	idName = msk.str(idName)
	LogDebuging("%x:%d-%s",idAddr,identityId,idName)
	skillBase = msk.dword(idAddr+0xb0)
	skilNum = msk.dword(idAddr+0xb4)
	for j=0,skilNum-1 do
		skillAddr = msk.dword(skillBase+j*4)
		while skillAddr ~= 0 do
			skillId = msk.dword(skillAddr)
			skillMaxLv = msk.dword(skillAddr+8)
			skillName = msk.dword(skillAddr+0x10)
			skillName = msk.str(skillName)
			skillLv = GetIdSkilllv(identityId,skillId)
			skillData = msk.api.GetCareerData(identityId,skillId)
			LogDebuging("  %x:%d-%s lv:%d/%d %s",skillAddr,skillId,skillName,skillLv,skillMaxLv,(skillData.state == 3 or skillData.state == 2) and "可学" or "不可学")
			--[[
			subBase = msk.dword(skillAddr+0x58)
			subNum = msk.dword(skillAddr+0x5c)
			for m=0,subNum -1 do
				subAddr= msk.dword(subBase+m*4)
				while subAddr ~= 0 do
					subLv = msk.dword(subAddr)
					subId = msk.dword(subAddr+4)
					subDesc = msk.dword(subAddr+0x48)
					subDesc = msk.str(subDesc)
					LogDebuging("    %x:%x:%d-%s",subAddr,subId,subLv,subDesc)
					subAddr = msk.dword(subAddr+0x1fc)
				end
			end
			--]]
			skillAddr = msk.dword(skillAddr+0x74)
		end
	end
	LogDebuging("log over")
end

ScriptExit = function(msg) sleep(0.1) LogDebuging("终止:%s",msg) msk.fireEvent("scriptCmd",msg) coroutine.yield() end
g_Exit = function(msg) LogDebuging("终止2:%s",msg) end
local pl
if msk.api.GetCurrentMapId() ~= 10050 and msk.api.GetCurrentMapId() ~= 0 then
	pl = msk.npc.GetMainPlayer()
	msk.npc.InitManDataIdOff()
end

mskg.HookRecv()
mskg.HookSend()
mskg.UnHookOnUiMsg()
mskg.UnHookKeyEvent()
g_changingMap = false
g_changingMap = false
g_statusThread = nil
g_allQuests = nil
g_mainId = qs.GetMainPlayerId()

local anshaDataBase = g_sig.anshaDataBase
local anshaUseCountOff= g_sig.anshaUseCountOff
local anshaCountOff = g_sig.anshaCountOff
local ui = msk.ui.GetPaneByName("QSUIAssassinationInfoPanel",true)
local base = msk.dword(anshaDataBase)
printx(base,anshaCountOff)
--RunScriptFile("shashou")
--msk.ui.Test(true)
--prints(msk.api.GetPlayerInMainLine())
--msk.require("script1",true)
--printx(pl.addr_,pl.man_)
--local ui = msk.ui.GetPaneByName("QSUIDialogPanel")
--UIShowWindow("QSUIDialogPanel",false)
--msk.qcall(0x00F7C380,ui,0,0)
--msk.api.DeleteAllMail()
--msk.ui.UIMsgCall("QSUIDialogPanel","CMD_OK")
--msk.ui.UIMsgCall("QSUIDialogPanel","CMD_CANCEL")
--local txt = msk.ui.GetPanelData("QSUITopPanel","mainWidget","mRightArea",1,12,5,"textField","text")
--printf(txt)
--pl:PrintAllBuff()
--msk.api.MoveDown(2254,2910,msk.npc.GetMainPlayer(),14000)
--msk.api.SimpleMoveToPositon(229400,294500,10)
--
--msk.ui.Test(true)
--g_client:hset("ansha_atime",g_mainId,0)
--[4228] msk_td->function: 133255E8
printx(g_sig.SendData2,g_sig.sendDataHookLen,g_sig.ChangeLine)

--TestAllModule()
do return end

--]]
--pl:MonitorBuff()
--printf(qs.GetGameString)
--qs.GetGameString("AS_QG_QUESTION_QIYU_INFO")
--PrintDataBase("Data.ClientTable.GameStringTable.GameStringTable_Retail")
--local str = "啊…咳，组织将于2分钟后挂上新的悬赏令，各位冷血杀手们还在等什么？速速"
--printf(bin2hex(str))
do return end
--)
local npc
repeat
	msk.api.Heal()
	npc = msk.npc.GetByName("周俊白")
	
	if npc then
		msk.api.FightNpc(npc)
	end
	sleep(0.1)
until not npc
--PrintAllIdentity(10)
--printf(bin2hex(str))
--GetIdSkilllv()
--printr())
--PrintAllIdentity()
--printf(qs.IDENTITY_TYPE_SHA_SHOU)
--[[
local QSUISkillInfoPanelSlotBase = g_sig.QSUISkillPanelSlotInfoBase--msk.gdata.GetUISlotBase("QSUISkillPanel")
local g_skillSlotOff =  msk.dword(QSUISkillInfoPanelSlotBase+0x8)
local sid,slot = msk.api.GetSlotIdByName("寒林疏芳")
if msk.api.UseSlot(slot) then
	sleep(0.4)
	printf(msk.api.UseSlot(slot))
	sleep(0.4)
	printf(msk.api.UseSlot(slot))
	sleep(0.3)
	printf(msk.api.UseSlot(slot))
	sleep(0.3)
end
do return end
--]]