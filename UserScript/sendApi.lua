local printf = _G.printf
local sleep = msk.sleep
local Send = msk.pack.Send
local Sendf = msk.pack.Sendf
local mconfig = msk.config
local hex2bin2 = msk.tools.hex2bin2
local hex2bin = msk.tools.hex2bin
local bin2hex = msk.tools.bin2hex
local buyItemCall = g_sig.BuyItem
--任务
function GeneralTest(id1,id2,id3)
	id3 = id3 or 0x24--普通
	Sendf(0x49d6,"LLL",id1,id2,id3)--?未测试
end
function SubmitQuestById(questid,npcid1,npcid2,ed)
	ed = ed or 0
	local sdata = string.pack("LLLL",questid,npcid1,npcid2,ed)
	local datastr = bin2hex(sdata,"")
	LogDebuging("SubmitQuestById:"..datastr)
	Send(0x1bef,sdata)
end
local _lastnid1,_lastnid2,_lastid = 0,0,0
function SubNpcWenTalk(npcid1,npcid2,id,idx1,idx2,idx3,idx4,idx5,idx6)
	if _lastnid1 == npcid1 and _lastnid2 == npcid2 and _lastid == id then
		sleep(5)
		_lastnid1,_lastnid2,_lastid = 0,0,0
		return
	end
	idx1 = idx1 or 0
	idx2 = idx2 or 0
	idx3 = idx3 or 0
	idx4 = idx4 or 0
	idx5 = idx5 or 0
	idx6 = idx6 or 0
	local sdata = string.pack("LLLLLLLLL",npcid1,npcid2,id,idx1,idx2,idx3,idx4,idx5,idx6)
	Send(0x151a,sdata)
	_lastnid1,_lastnid2,_lastid = npcid1,npcid2,id
end
function Say(str,channel)
	channel = channel or 6--普通 4:地区 2:世界
	local c0 = string.char(0)
	if #str > 150 then
		str = str:sub(1,150)
	elseif #str < 150 then
		str = str..string.rep(c0,150-#str)
	end
	str = string.char(channel)..string.rep(c0,36)..str
	str = str..string.rep(c0,3629-#str)
	Send(0x1330,str)
	--print(#str)
end

--身份
function __TransferIdentity(iid)
	Send(0xc27,string.char(iid,1))
end

--拍卖
function __BuyPaiMaiItem(id1,id2,num,nid1,nid2)
	local data= string.pack("LLLbLL",id1,id2,num,0,nid1,nid2)
	Sendf(0x4992,"LLLbLL",id1,id2,num,0,nid1,nid2)
end
local __paimaiValue = {
	tp = {
		["全部"] = 0xffff8fff,
	},
	pro = {
		["全部"] = 0xff,
	},
	qualityLv = {
		["全部"] = 0x1f,
	},
}
local __paimaiEnd = hex2bin2("030000008000000000000000000000000000000000000000")
function SearchAuction(nid1,nid2,name,tp,pro,qualityLv,minLv,maxLv,askNum)
	tp = tp or "全部"
	pro = pro or "全部"
	qualityLv = qualityLv or "全部"
	tp = __paimaiValue["tp"][tp]
	pro = __paimaiValue["pro"][pro]
	qualityLv = __paimaiValue["qualityLv"][qualityLv]
	
	minLv = minLv or 1
	maxLv = maxLv or 120
	askNum = askNum or 10
	local c0 = string.char(0)
	local data = c0..name..string.rep(c0,32-#name)
	local data2 = string.pack("LHbbLL",tp,0,pro,qualityLv,minLv,maxLv)
	data = data..data2..__paimaiEnd..string.pack("LbLL",askNum,0,nid1,nid2)
	Send(0x4990,data)
end

--暗杀
function AnshaWunian2(id1,id2)
	if qs.CheckHasBuff(0x49c) == false then
		Sendf(0x487b,"LL",id1,id2)
	end
end
local anshaMoneyId = g_sig.anshaMoneyId or 0xb4
function __AcceptAnSha(id1,id2,nid1,nid2)
	local data = string.pack("LLLLLL",id1,id2,0,0,nid1,nid2)
	LogDebuging("接受暗杀:%s",bin2hex(data,""))
	Send(0x487a,data)
end

function GetAnShaShuJu(bg,num)
	--local data= string.pack("LLLLLLLLLb",0,8,0,0x2130ab0,0,2,0x28e2b31,0x200202,0,1)
	bg = bg or 0
	num = num or 8
	Sendf(0x4878,"LLLLLLLLLb",bg,num,0,0x2130ab0,0,2,0x28e2b31,0x20202,0,1)
	--printf(bin2hex(data,""))
end

--商城
function BuyShangChengItem(id,num,money)
	UIShowWindow("QSUIShopPanel",true)
	sleep(1)
	local shopPanel= msk.ui.GetPaneByName("QSUIShopPanel",true)
	if shopPanel == nil then
		LogWarning("无法打开系统商店")
		return false
	end
	msk.qcall(buyItemCall,shopPanel,0,num,id)
	UIShowWindow("QSUIShopPanel",false)
end


--商店
function __BuyItem(id1,id2,shopId,tid,buyCount)
	return Sendf(0x37de,"bLLLLL",2,id1,id2,shopId,tid,buyCount)
end
function RepairAll(id1,id2,shopId)
	Sendf(0x37de,"bLLLLL",6,id1,id2,shopId,0,0)
end

--经脉
function LearnAcupoint(chanel,pos)
	Send(0x458c,string.char(0,chanel,pos))
end
function EquipXinFa(pos)
	pos = pos or 0
	local data = string.char(0,0,pos)
	Send(0x45af,data)
end
function TuPoXinFa(pos)
	pos = pos or 0
	local data = string.char(0,pos)
	Send(0x45a1,data)
end

--创角
local function __GetRandomName(pro,sex)
	local xstr = math.random(1,#msk.names.LAST_NAME)
	local FirstNameT
	if sex == "女" then
		FirstNameT = msk.names.FEMALE_FIRST_NAME
	else
		FirstNameT = msk.names.MALE_FIRST_NAME
	end
	local mstr = math.random(1,#FirstNameT)
	local name
	name = msk.names.LAST_NAME[xstr]..FirstNameT[mstr]
	return name..g_names[math.random(1,#g_names)]
end
function CreateChar(name,pro,sex)
	local data
	pro = pro or mconfig.defaultPro
	sex = "女"--只支持女的 。。。除非出少林
	name = name or __GetRandomName(pro,sex)
	if not name then
		msk.api.QuitGame("创建角色取名失败")
		return
	end
	if #name > 21 then
		LogWarning("名字过长,截断...")
		name = name:name(1,21)
	end
	local otherData
	if pro == "天香" then
		g_cacheData.txCreateCharData = g_cacheData.txCreateCharData or hex2bin2("1D188868681700936B17010000006D000000690000000000000000000000FFFFFFFF350100000D0100000000803F0100000084030000C201000086030000850300008803000089030000530300000000003F0000803F260200000000000000000000000000000000803F0000803F000000009A99193E0000803F0000803F0000803F0000803F0000803F0000803F0000803F1A000000050000000000000000000000000000000000000000000000000000000000000000000000000000000A000000000000000000000000000000000000000000000000000000000000000000000000000000140000000000000000000000000000000000000000000000000000000000000000000000000000001E00000000000000000000000000000000000000000000000000000000000000000000000000000028000000000000000000000000000000000000000000000000000000000000000000000000000000320000000000000000000000000000000000000000000000000000000000000000000000000000003C000000000000000000000000000000000000006891ED3C000000000000000000000000000000004600000000000000000000000000000000000000000000000000000000000000000000000000000050000000000000000000000000000000000000006DE75B3F000000000000000000000000000000005A000000000000000000000000000000000000000000000000000000000000000000000000000000640000000000000000000000000000000000000000000000000000000000000000000000000000006E00000000000000000000000000000000000000000000000000000000000000000000000000000078000000000000000000000000000000000000000000000000000000000000000000000000000000820000000000000000000000000000000000000000000000000000000000000000000000000000008C0000004A0CC23E000000000000000000000000000000000000000000000000000000000000000096000000000000000000000000000000000000000000000000000000000000000000000000000000A0000000000000000000000000000000000000000000000000000000000000000000000000000000AA000000000000000000000000000000000000000000000000000000000000000000000000000000B4000000000000000000000000000000000000000000000000000000000000000000000000000000BE000000000000000000000000000000000000000000000000000000000000000000000000000000C8000000000000000000000000000000000000000000000000000000000000000000000000000000D2000000000000000000000000000000000000000000000000000000000000000000000000000000DC000000000000000000000000000000000000000000000000000000000000000000000000000000E6000000000000000000000000000000000000000000000000000000000000000000000000000000F0000000000000000000000000000000000000000000000000000000000000000000000000000000FA000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000007000000")
		otherData = g_cacheData.txCreateCharData
	elseif pro == "真武" then
		if sex == "女" then
			g_cacheData.zwCreateCharData = g_cacheData.zwCreateCharData or hex2bin2("31184A13000000000000010000006D000000690000000000000000000000FFFFFFFF350100000D0100000000803F0100000084030000C201000086030000850300008803000089030000530300000000003F0000803F260200000000000000000000000000000000803F0000803F000000009A99193E0000803F0000803F0000803F0000803F0000803F0000803F0000803F1A000000050000000000000000000000000000000000000000000000000000000000000000000000000000000A000000000000000000000000000000000000000000000000000000000000000000000000000000140000000000000000000000000000000000000000000000000000000000000000000000000000001E00000000000000000000000000000000000000000000000000000000000000000000000000000028000000000000000000000000000000000000000000000000000000000000000000000000000000320000000000000000000000000000000000000000000000000000000000000000000000000000003C000000000000000000000000000000000000006891ED3C000000000000000000000000000000004600000000000000000000000000000000000000000000000000000000000000000000000000000050000000000000000000000000000000000000006DE75B3F000000000000000000000000000000005A000000000000000000000000000000000000000000000000000000000000000000000000000000640000000000000000000000000000000000000000000000000000000000000000000000000000006E00000000000000000000000000000000000000000000000000000000000000000000000000000078000000000000000000000000000000000000000000000000000000000000000000000000000000820000000000000000000000000000000000000000000000000000000000000000000000000000008C0000004A0CC23E000000000000000000000000000000000000000000000000000000000000000096000000000000000000000000000000000000000000000000000000000000000000000000000000A0000000000000000000000000000000000000000000000000000000000000000000000000000000AA000000000000000000000000000000000000000000000000000000000000000000000000000000B4000000000000000000000000000000000000000000000000000000000000000000000000000000BE000000000000000000000000000000000000000000000000000000000000000000000000000000C8000000000000000000000000000000000000000000000000000000000000000000000000000000D2000000000000000000000000000000000000000000000000000000000000000000000000000000DC000000000000000000000000000000000000000000000000000000000000000000000000000000E6000000000000000000000000000000000000000000000000000000000000000000000000000000F0000000000000000000000000000000000000000000000000000000000000000000000000000000FA000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000000000000")
			otherData = g_cacheData.zwCreateCharData
		end
	else
		ScriptExit("不支持的职业:"..pro)
	end
	local c0 = string.char(0)
	data = name..string.rep(c0,22-#name)
	local qid = msk.dword(g_sig.QQBase)
	qid = string.pack("L",qid)
	data = qid..data
	data = data..otherData
	Send(0x2f8,data)
	return ret
end

--盟会
function ChooseManor(mname)
	local midx
	mname = mname or mconfig.defaultMenhuiName
	Send(0x4f16,string.char(0))
	sleep(1)
	if not mname or mname == "系统推荐" then
		mname = "万里杀"
	end
	if midx then
	elseif mname == "帝王州" then
		midx = 1
	elseif mname == "寒江城" then
		midx = 2
	elseif mname == "水龙吟" then
		midx = 3
	elseif mname == "万里杀" then
		midx = 4
	else
		LogWarning("未知的盟会:%s",mname or "nil")
		return false
	end
	Send(0x4eeb,string.char(1,midx))
	sleep(3)
	UIShowWindow("QSUIMenghuiMainPanel",false)
end

--技能
function __FlyMap(pos)
	local data = string.pack("L",pos)
	Send(0x4e25,data)
end
function Collect(id1,id2)
	local pl = msk.npc.GetMainPlayer()
	local sdata = string.pack("LLLL",id1,id2,pl.id1,pl.id2)
	Send(0xbce,sdata)
end
function OpenNpc(id1,id2)
	Sendf(0xcdc,"<LL",id1,id2)
end
function CloseNpc(id1,id2)
	Sendf(0xcf0,"<LL",id1,id2)
end
function SelectNpc(id1,id2)
	Sendf(0xbeb,"<LL",id1,id2)
end
function DecomposeItem(pos)
	if os.time() - g_cacheData.logTime <= 180 then
		return false
	end
	Send(0x4334,string.char(2,pos,0))
end
function __LearnIdentitySkill(identityId,skillId,lv)
	lv = lv or 1
	Sendf(0xc4f,"bbb",identityId,skillId,lv)--?未测试
end

--邮件
function ExtractMail(nid1,nid2,id1,id2)
	Sendf(0x1420,"LLLLLLL",1,nid1,nid2,id1,id2,2,0)
end
function DeleteMail(id1,id2)
	do return end
	local sdata = string.pack("LLLLLL",0,0,0,1,id1,id2)
	sdata = sdata..string.rep(string.char(0),416-#sdata)
	Send(0x14de,sdata)
end

--背包操作
function Split(pos1,pos2,num)
	num = num or 1
	local data = string.char(1,2,pos1,0,2,pos2,0,num)
	data = data..string.rep(string.char(0),1290-#data)
	Send(0x3e80,data)
end
function __EnterDangKou(lv,id1,id2)
	Sendf(0x55d5,"bLL",lv,id1,id2)
end
function StrongEquip2(equippos,pos)
	do return end
	if equippos == nil then
		equippos = 0--默认为主武器
		pos = msk.item.SplitItemByName("1级精金石") or msk.item.SplitItemByName("一级火炼金")
	end
	if equippos == nil then
		equippos = 3
		pos = msk.item.SplitItemByName("一级火浣纱")
	end
	if pos == nil then return false end
	local data = string.char(1,equippos,0,1,2,pos)..string.rep(string.char(0),13)
	printf(bin2hex(data,""))
	Send(0x4330,data)
	return true
end



--队伍
function InviteTeam(id1,id2,name)
	local idstr = string.pack("LL",id1,id2)
	local itstr = idstr..name..hex2bin("004000A055B21644B4D513090000000500000016022000","(.)(.)")
	Send(0x232f,itstr)
end
function LeveaTeam()
	local pl = msk.npc.GetMainPlayer()
	local idstr = string.pack("LL",pl.id1,pl.id2)
	Send(0x233d,idstr)
	Send(0x2338,idstr)
end
function AgreeInvite(id1,id2)
	Sendf(0x2332,"LLLLL",0,id1,id2,0,0)
end
function LearXinFaTianFu(pos)
	Send(0x458c,string.char(1,0,pos))
end