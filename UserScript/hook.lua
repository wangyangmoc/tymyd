module("msk.hook",package.seeall)
local print = _G.printf
local bin2hex = msk.tools.bin2hex
local hex2bin = msk.tools.hex2bin
local hex2bin2= msk.tools.hex2bin2

local CommonSkipTable = {
	Data = true,
	ModuleName = true,
}

g_QsHookTable = g_QsHookTable or {}
function IsBaseFun(f)
	if type(f) ~= 'function' then
		return false
	end
	if f == _G.require then return true end
	if f == _G.module then return true end
	if f == _G.printr then return true end
	if f == _G.printf then return true end
	if f == _G.prints then return true end
	if f == _G.printx then return true end
	if f == _G.printt then return true end
	if f == _G.OnGameRecv then return true end
	if f == _G.OnGameSend then return true end
	if f == _G.warning then return true end
	if f == _G.debuging then return true end
	
	if f == _G.prettytostring then return true end
	if f == _G.render then return true end
	if f == _G.metamethod then return true end
	
    if f == _G.assert then return true end
    if f == _G.collectgarbage then return true end
   -- if f == _G.dofile then return true end
    if f == _G.error then return true end
    if f == _G.getfenv then return true end
    if f == _G.getmetatable then return true end
    if f == _G.ipairs then return true end
   -- if f == _G.load then return true end
   -- if f == _G.loadfile then return true end
    --if f == _G.loadstring then return true end
    if f == _G.module then return true end
    if f == _G.next then return true end
    if f == _G.pairs then return true end
    if f == _G.pcall then return true end
    if f == _G.print then return true end
    if f == _G.rawequal then return true end
    if f == _G.rawget then return true end
    if f == _G.rawset then return true end
    if f == _G.require then return true end
    if f == _G.select then return true end
    if f == _G.setfenv then return true end
    if f == _G.setmetatable then return true end
    if f == _G.tonumber then return true end
    if f == _G.tostring then return true end
    if f == _G.type then return true end
    if f == _G.unpack then return true end
    if f == _G.xpcall then return true end
	return false
end

function IsBaseTable(t)
	if t == _G.msk then return true end
	if t == _G.coroutine then return true end
	if t == _G.package then return true end
	if t == _G.string then return true end
	if t == _G.math then return true end
	if t == _G.table then return true end
	if t == _G.io then return true end
	if t == _G.os then return true end
	if t == _G.debug then return true end
	for k,v in pairs(_G.msk) do
		if v == t then return true end
	end
	return false
end

function UnHookQs()

	for k,v in pairs(qs) do
        if type(v) == 'function' then
            if g_QsHookTable[v] then
                qs[k] = g_QsHookTable[v]
                g_QsHookTable[v] = nil
            end
        end
    end
end

function HookQs(skip)
    UnHookQs()
	for k,v in pairs(qs) do
        if (not skip or not skip[k]) and type(v) == 'function' then
            local newCloure = function (...)
                prints(k,'(',...)
                return v(...)
            end
            g_QsHookTable[newCloure] = v
            qs[k] = newCloure
        else
            --prints(k,' = ',type(v),v)
        end
    end
end
g_HookedTable = g_HookedTable or {}
g_HookedTableFun = g_HookedTableFun or {}
g_HookedTableFunNum = g_HookedTableFunNum or {}
g_ReHookedTableFun = g_ReHookedTableFun or {}
local curHookedTable = {}
function UnHookTable11(t)
    if  g_HookedTable[t] == nil then
        return
    end
	prints("UnHookTable",g_HookedTable[t])
	local keys,vals = {},{}
	local nf--原来的函�?
    for k,v in pairs(t) do
        if g_HookedTableFun[v] then
			nf = g_HookedTableFun[v]
			keys[#keys+1] = k
			vals[#vals+1] = g_HookedTableFun[v]
            --t[k] = g_HookedTableFun[v]
            g_HookedTableFunNum[nf] = g_HookedTableFunNum[nf] -1
            if g_HookedTableFunNum[nf] < 1 then--没有引用�?
                g_HookedTableFunNum[nf] = nil
                g_HookedTableFun[v] = nil
				g_ReHookedTableFun[nf] = nil
            end
        end
    end
	for i=1,#keys do
		t[keys[i]] = vals[i]
	end
    g_HookedTable[t] = nil
end
--loop 是否递归进去
function HookTable11(t,tname,skp)
    if not t or curHookedTable[t] then return end
    tname = tname or ""
    curHookedTable[t] = true
    UnHookTable(t)
    local hookself = not skp[tname]
	if hookself then
		printf("HookTable:%s",tname)
		g_HookedTable[t] = tname
	else
		printf("HookTable skip:%s",tname)
	end
	local fullvname
	for k,v in pairs(t) do
		fullvname = tname..'.'..tostring(k)
        if hookself and type(v) == 'function' then
			if g_ReHookedTableFun[v] then
				g_HookedTableFunNum[v] = g_HookedTableFunNum[v] + 1
				t[k] = g_ReHookedTableFun[v]
			else
			    local newCloure = function (...)
					if g_Print then
						prints(fullvname..'(',...)
					end
					return v(...)
				end
				g_ReHookedTableFun[v] = newCloure
				g_HookedTableFun[newCloure] = v
				t[k] = newCloure
				g_HookedTableFunNum[v] = 1
			end
		elseif type(v) == "table" then
			HookTable(v,fullvname,skp)
			HookTable(getmetatable(v),fullvname..".__index",skp)
		elseif type(v) == "userdata" then
			HookTable(getmetatable(v),fullvname..".__index",skp)
		end
    end
end

function HookFireEvent()
    g_OrgFireEvent = g_OrgFireEvent or EventSystem.fireEvent
    local skipedEvent = {
        
    }
    function MyFireEvent(ev,...)
        if not skipedEvent[ev] then
            prints('FireEvent(',...)
        end
        return g_OrgFireEvent(ev,...)
    end
    EventSystem.fireEvent = MyFireEvent
end

function HookQS()
    local skipFun = {
        GetCurrentEventID = true,
    }

    HookQs(skipFun)
end

function HookAllTable11()
    local skipTable = {
        _G,
        _G.string,
        _G.io,
        _G.coroutine,
        _G.table,
        _G.math,
        _G.os,
        _G.debug,
        _G.msk,
        _G.package.loaded.package,
        _G.package,
    }
    HookTable(_G,"_G",skipTable)
end

function PrintEvent(ev,name)
    if type(ev) ~= 'table' then
        return
    end
    if ev.slots then
        printf(name)
    else
        for k,v in pairs(ev) do
            PrintEvent(v,name..'.'..tostring(k))
        end
    end
end

function HookDgnEventInstance()
    
end

g_DeHookTable = g_DeHookTable or {}
function UnHookDgnEventInstance()
     local t = Logic.DgnEventSystem.DgnEventInstance.__oriIndex
	for k,v in pairs(t) do
        if type(v) == 'function' then
            if g_DeHookTable[v] then
                t[k] = g_DeHookTable[v]
                g_DeHookTable[v] = nil
            end
        end
    end
end

function HookDgnEventInstance(skip)
    UnHookDgnEventInstance()
    local t = Logic.DgnEventSystem.DgnEventInstance.__oriIndex
	for k,v in pairs(t) do
        if (not skip or not skip[k]) and type(v) == 'function' then
            local newCloure = function (self,...)
                prints(self.__name,k,...)
                return v(...)
            end
            g_DeHookTable[newCloure] = v
            t[k] = newCloure
        else
            --prints(k,' = ',type(v),v)
        end
    end
end

g_HookTable = g_HookTable or {}

function UnHookTable(t)
	if not t then--全部
		for k,v in pairs(g_HookTable) do
			UnHookTable(k)
		end
		return
	end
	local funs = g_HookTable[t]
	if not funs then return end
	local needChangeT = {}
	local needChangeK = {}
	for k,v in pairs(t) do
        if funs[v] then
			needChangeT[#needChangeT+1] = v
			needChangeK[#needChangeK+1] = k
        end
    end
	for i=1,#needChangeT do
		local f = needChangeT[i]
		local k = needChangeK[i]
		t[k] = funs[f]
		funs[f] = nil
	end
	g_HookTable[t] = nil
end

g_HookFun = g_HookFun or {}
function UnHookFunction(t,fname)
	local f = t[fname]
	if not  g_HookFun[f] then
		return
	end
	t[fname] = g_HookFun[f]
	g_HookFun[f] = nil
end
function HookFunction(t,fname,newf)
	UnHookFunction(t,fname)
	local f = t[fname]
	if not f or type(f) ~= 'function' then
		return
	end
	newf = newf or function (...) 
		prints('fcall-'..tostring(fname),...)
		--return f(...) 
	end
	local trueNewF = function(...) newf(...) return f(...)  end
	t[fname] = trueNewF
	g_HookFun[trueNewF] = f
end

skipTables = {
	["_G"] = {
		IsQuestAccepted = true,
		dostring = true,
		IsAchvReqComplete = true,
		CheckActorMinLevel = true,
		DgnEventFind = true,
	},
	["qs"] = {
		GetCurrentEventID = true,
		SetCommonTipsWidth = true,
		SetCommonTipsName = true,
		AddCommonTipsText = true,
		CompleteCommonTips = true,
		IsQuestAccepted = true,
		IsAchvReqComplete = true,
		GetMainplayerLevel = true,
	},
	["MenghuiDailyQuestUI"] = {
		ModuleName = true,
	},
	['Loader'] = {
		LoadString = true,
	},
	["Logic.GamePlay.Quest"] = {
		get_event_request_monsters = true,
		IsDataEmpty = true,
	},

	["Logic.GamePlay.Yunbiao"] = {
		
		is_in_npc_jiebiao_plane = true,
		is_in_single_yabiao_plane = true,
		is_in_multi_yabiao_plane = true,
		is_in_raid_yabiao_plane = true,
		is_in_monor_yabiao_plane = true,
		
		get_jiebiao_exit_plane = true,
		get_jiebiao_rob_all_dead = true,
		get_yabiao_player_all_dead = true,
		
		is_in_event_array = true,
		is_in_player_jiebiao_plane = true,
		is_in_event_array = true,
		is_in_jiebiao_plane = true,
		
		get_jiebiao_out_of_range_warning = true,
		get_yabiao_out_of_range_warning = true,
		is_jiebiao = true,
		is_yabiao = true,
		
		is_in_yabiao_plane = true,
		
		get_yabiao_countdown = true,
		get_jiebiao_ready_countdown = true,
		
	},
	["QSConfig.DailyGamePlatData"] = {
		Get_MPDZ_ExpData = true,
	},
	["QSConfig.MailCfg"] = {
		GetClearCacheCD = true,
	},
	["Logic.GamePlay.CommandQueue"] = {
		update = true,
		update_list = true,
	},
	["TrackMgr"] = {
		Update = true,
		RemoveModule = true,
		Render = true,
	},
	["DatiUI"] = {
		RemoveDati = true,
		ModuleName = true,
	},
	["SeizeUI"] = {
		RemoveSeize = true,
		ModuleName = true,
	},
	["QSConfig.SkillTips"] = {
		SetSimpleSkillTips = true,
		HideSkillTips = true,
		GetLevel = true,
		GetChineseStr = true,
		GetChineseStr_Single = true,
		GetBlank = true,
		GetSkillValue = true,
		GetSkillType = true,
		GetValue = true,
		GetSimpleTips = true,
		SetDecoTips = true,
		SetXinfaLevelUpTips = true,
	},
	["JianwenUI"] = {
		Data = true,
	},
	["ComposeMovieClip"] = {
		IsSameData = true,
	},
	["DgnEventSystem.DgnEventInstance"] = {
	},
	["Logic.DgnEventSystem"] = {
		FindEventInstace = true,
		GetColorByActorId = true,
	},
}


function HookTable(t,skip,tname)
	printf("try Hook:%s",tname)
    UnHookTable(t)
	skip = skip or skipTables[tname]
	local funs = {}
	tname = tname or ""
	g_HookTable[t] = funs
	local needChangeK = {}
	for k,v in pairs(t) do
        if type(v) == 'function' and not IsBaseFun(v) and (not skip or not skip[k]) and not CommonSkipTable[k] then
			needChangeK[#needChangeK+1] = k
        end
    end
	for i=1,#needChangeK do
		local k = needChangeK[i]
		local v = t[k]
		local newCloure = function (...)
			local rt = {v(...)}
			if #rt > 0 then
				prints(tname..'#'..tostring(k),'->',rt[1],'<-',...)
				return unpack(rt)
			else
				prints(tname..'#'..tostring(k),'->nil<-',...)
			end
		end
		funs[newCloure] = v
		t[k] = newCloure
	end
end

function HookAllTableFuns(force)
	if not force and g_TableHooked then return end
	g_TableHooked = true
	HookTable(PveAnShaUI,skipTables["PveAnShaUI"],"PveAnShaUI")
	HookTable(Logic.GamePlay.QuestConfig,skipTables["Logic.GamePlay.QuestConfig"],"Logic.GamePlay.QuestConfig")
	HookTable(QSConfig.BuffTips,skipTables["QSConfig.BuffTips"],"QSConfig.BuffTips")
	HookTable(QSConfig.CareerIconConfig,skipTables["QSConfig.CareerIconConfig"],"QSConfig.CareerIconConfig")
	HookTable(QSConfig.MenghuiTips,skipTables["QSConfig.MenghuiTips"],"QSConfig.MenghuiTips")
	HookTable(QSConfig.PetUI3DAvatarConfig,skipTables["QSConfig.PetUI3DAvatarConfig"],"QSConfig.PetUI3DAvatarConfig")
	HookTable(QSConfig.CreateCharacterTips,skipTables["QSConfig.CreateCharacterTips"],"QSConfig.CreateCharacterTips")
	HookTable(QSConfig.DailyGamePlatData,skipTables["QSConfig.DailyGamePlatData"],"QSConfig.DailyGamePlatData")
	HookTable(QSConfig.ShiLianConfig,skipTables["QSConfig.ShiLianConfig"],"QSConfig.ShiLianConfig")
	HookTable(AllianceDartUI,skipTables["AllianceDartUI"],"AllianceDartUI")
	HookTable(QuestUITemplate,skipTables["QuestUITemplate"],"QuestUITemplate")
	HookTable(GameConfig.LevelUp,skipTables["GameConfig.LevelUp"],"GameConfig.LevelUp")
	HookTable(RaidDartUI,skipTables["RaidDartUI"],"RaidDartUI")
	HookTable(QSConfig.EventRewardConfig,skipTables["QSConfig.EventRewardConfig"],"QSConfig.EventRewardConfig")
	HookTable(QSConfig.EventRewardConfig.EventRewardFuncs,skipTables["QSConfig.EventRewardConfig.EventRewardFuncs"],"QSConfig.EventRewardConfig.EventRewardFuncs")
	HookTable(QSConfig.SkillTips,skipTables["QSConfig.SkillTips"],"QSConfig.SkillTips")
	HookTable(QSConfig.TeamTips,skipTables["QSConfig.TeamTips"],"QSConfig.TeamTips")
	HookTable(QSConfig.FamilyDazuoTips,skipTables["QSConfig.FamilyDazuoTips"],"QSConfig.FamilyDazuoTips")
	HookTable(QSConfig.Tips,skipTables["QSConfig.Tips"],"QSConfig.Tips")
	HookTable(StringBuilder,skipTables["StringBuilder"],"StringBuilder")
	HookTable(QSQA,skipTables["QSQA"],"QSQA")
	HookTable(QSConfig.GrowUpRecommendData,skipTables["QSConfig.GrowUpRecommendData"],"QSConfig.GrowUpRecommendData")
	HookTable(QSConfig.GiftWhiteList,skipTables["QSConfig.GiftWhiteList"],"QSConfig.GiftWhiteList")
	HookTable(ScriptCustom,skipTables["ScriptCustom"],"ScriptCustom")
	HookTable(QSConfig.HotUpdateConfig,skipTables["QSConfig.HotUpdateConfig"],"QSConfig.HotUpdateConfig")
	HookTable(QSConfig.FamilyUICfg,skipTables["QSConfig.FamilyUICfg"],"QSConfig.FamilyUICfg")
	HookTable(QSConfig.MenghuiUICfg,skipTables["QSConfig.MenghuiUICfg"],"QSConfig.MenghuiUICfg")
	HookTable(QSConfig.NetConfig,skipTables["QSConfig.NetConfig"],"QSConfig.NetConfig")
	HookTable(QSConfig.TeamCfg,skipTables["QSConfig.TeamCfg"],"QSConfig.TeamCfg")
	--HookTable(QSConfig.PetUI3DAvatarConfig.actorDir,skipTables["QSConfig.PetUI3DAvatarConfig.actorDir"],"QSConfig.PetUI3DAvatarConfig.actorDir")
	HookTable(QSConfig.ShopConfig,skipTables["QSConfig.ShopConfig"],"QSConfig.ShopConfig")
	HookTable(Event,skipTables["Event"],"Event")
	--HookTable(QSConfig.PetUI3DAvatarConfig.actorPos,skipTables["QSConfig.PetUI3DAvatarConfig.actorPos"],"QSConfig.PetUI3DAvatarConfig.actorPos")
	--HookTable(QSConfig.PetUI3DAvatarConfig.cameraUp,skipTables["QSConfig.PetUI3DAvatarConfig.cameraUp"],"QSConfig.PetUI3DAvatarConfig.cameraUp")
	--HookTable(QSConfig.PetUI3DAvatarConfig.cameraPos,skipTables["QSConfig.PetUI3DAvatarConfig.cameraPos"],"QSConfig.PetUI3DAvatarConfig.cameraPos")
	--HookTable(QSConfig.PetUI3DAvatarConfig.cameraDir,skipTables["QSConfig.PetUI3DAvatarConfig.cameraDir"],"QSConfig.PetUI3DAvatarConfig.cameraDir")
	--HookTable(QSConfig.PetUI3DAvatarConfig.cameraDir..fn,skipTables["QSConfig.PetUI3DAvatarConfig.cameraDir..fn"],"QSConfig.PetUI3DAvatarConfig.cameraDir..fn")
	HookTable(NormalDartUI,skipTables["NormalDartUI"],"NormalDartUI")
	HookTable(Logic.LevelLogic,skipTables["Logic.LevelLogic"],"Logic.LevelLogic")
	HookTable(SeizeUI,skipTables["SeizeUI"],"SeizeUI")
	HookTable(Logic.GamePlay.CommandQueue,skipTables["Logic.GamePlay.CommandQueue"],"Logic.GamePlay.CommandQueue")
	HookTable(BangpaiQuestUI,skipTables["BangpaiQuestUI"],"BangpaiQuestUI")
	HookTable(QuestionUI,skipTables["QuestionUI"],"QuestionUI")
	HookTable(Logic.GamePlay.HuntingInfoUI,skipTables["Logic.GamePlay.HuntingInfoUI"],"Logic.GamePlay.HuntingInfoUI")
	HookTable(HuodongQuestUI,skipTables["HuodongQuestUI"],"HuodongQuestUI")
	HookTable(TeamTreasureUI,skipTables["TeamTreasureUI"],"TeamTreasureUI")
	HookTable(CareerIcon,skipTables["CareerIcon"],"CareerIcon")
	HookTable(AnShaUI,skipTables["AnShaUI"],"AnShaUI")
	HookTable(ManorQuestUI,skipTables["ManorQuestUI"],"ManorQuestUI")
	HookTable(Logic.GamePlay.Quest,skipTables["Logic.GamePlay.Quest"],"Logic.GamePlay.Quest")
	HookTable(GamePlay.ai,skipTables["GamePlay.ai"],"GamePlay.ai")
	HookTable(Logic.GamePlay.SkillName,skipTables["Logic.GamePlay.SkillName"],"Logic.GamePlay.SkillName")
	HookTable(BattleFieldUI,skipTables["BattleFieldUI"],"BattleFieldUI")
	HookTable(Logic.Camera,skipTables["Logic.Camera"],"Logic.Camera")
	HookTable(Logic.DgnEventSystem,skipTables["Logic.DgnEventSystem"],"Logic.DgnEventSystem")
	HookTable(DatiUI,skipTables["DatiUI"],"DatiUI")
	HookTable(Logic.GamePlay.UIColorConfig,skipTables["Logic.GamePlay.UIColorConfig"],"Logic.GamePlay.UIColorConfig")
	HookTable(Logic.GamePlay.Other,skipTables["Logic.GamePlay.Other"],"Logic.GamePlay.Other")
	HookTable(Logic.GamePlay.TongtianTower,skipTables["Logic.GamePlay.TongtianTower"],"Logic.GamePlay.TongtianTower")
	HookTable(Logic.GamePlay.NpcTalk,skipTables["Logic.GamePlay.NpcTalk"],"Logic.GamePlay.NpcTalk")
	HookTable(ComposeMovieClip,skipTables["ComposeMovieClip"],"ComposeMovieClip")
	HookTable(HuabenReward,skipTables["HuabenReward"],"HuabenReward")
	HookTable(Logic.GamePlay.Yunbiao,skipTables["Logic.GamePlay.Yunbiao"],"Logic.GamePlay.Yunbiao")
	HookTable(QSConfig.FirstDoCfg,skipTables["QSConfig.FirstDoCfg"],"QSConfig.FirstDoCfg")
	HookTable(Logic.UICore,skipTables["Logic.UICore"],"Logic.UICore")
	HookTable(Loader,skipTables["Loader"],"Loader")
	HookTable(QSConfig.MailCfg,skipTables["QSConfig.MailCfg"],"QSConfig.MailCfg")
	HookTable(QSConfig.UIMovieResConfig,skipTables["QSConfig.UIMovieResConfig"],"QSConfig.UIMovieResConfig")
	HookTable(QSConfig.LivenessCfg,skipTables["QSConfig.LivenessCfg"],"QSConfig.LivenessCfg")
	HookTable(QSConfig.AcupointCfg,skipTables["QSConfig.AcupointCfg"],"QSConfig.AcupointCfg")
	HookTable(QSConfig.EventComplete,skipTables["QSConfig.EventComplete"],"QSConfig.EventComplete")
	HookTable(QSConfig.FamilyDazuoCfg,skipTables["QSConfig.FamilyDazuoCfg"],"QSConfig.FamilyDazuoCfg")
	HookTable(QSConfig.LingxiCfg,skipTables["QSConfig.LingxiCfg"],"QSConfig.LingxiCfg")
	HookTable(IdentityDailyQuestUI,skipTables["IdentityDailyQuestUI"],"IdentityDailyQuestUI")
	HookTable(QSConfig.MenghuiCfg,skipTables["QSConfig.MenghuiCfg"],"QSConfig.MenghuiCfg")
	HookTable(JiangYangDaDaoUI,skipTables["JiangYangDaDaoUI"],"JiangYangDaDaoUI")
	HookTable(JianwenUI,skipTables["JianwenUI"],"JianwenUI")
	HookTable(EventSystem,skipTables["EventSystem"],"EventSystem")
	HookTable(QSConfig.FamilyTips,skipTables["QSConfig.FamilyTips"],"QSConfig.FamilyTips")
	HookTable(QSConfig.UIHelp,skipTables["QSConfig.UIHelp"],"QSConfig.UIHelp")
	HookTable(TrackMgr,skipTables["TrackMgr"],"TrackMgr")
	HookTable(MenghuiDailyQuestUI,skipTables["MenghuiDailyQuestUI"],"MenghuiDailyQuestUI")
	HookTable(QSConfig.ScheduleUIHelper,skipTables["QSConfig.ScheduleUIHelper"],"QSConfig.ScheduleUIHelper")
	HookTable(QSConfig.GamePlayTips,skipTables["QSConfig.GamePlayTips"],"QSConfig.GamePlayTips")
	HookTable(QSConfig.FamilyCfg,skipTables["QSConfig.FamilyCfg"],"QSConfig.FamilyCfg")
	HookTable(QSConfig.LocalizationCfg,skipTables["QSConfig.LocalizationCfg"],"QSConfig.LocalizationCfg")
	HookTable(qs,skipTables["qs"],"qs")
	HookTable(QSConfig.CareerTips,skipTables["QSConfig.CareerTips"],"QSConfig.CareerTips")
	HookTable(QSConfig.ArenaTips,skipTables["QSConfig.ArenaTips"],"QSConfig.ArenaTips")
	HookTable(QSConfig.BigLingxiTips,skipTables["QSConfig.BigLingxiTips"],"QSConfig.BigLingxiTips")
	HookTable(ArenaDailyQuestUI,skipTables["ArenaDailyQuestUI"],"ArenaDailyQuestUI")
	HookTable(QSConfig.ItemTips,skipTables["QSConfig.ItemTips"],"QSConfig.ItemTips")
	HookTable(QSConfig.AcupointTips,skipTables["QSConfig.AcupointTips"],"QSConfig.AcupointTips")
	HookTable(QSConfig.ShilianMapConfig,skipTables["QSConfig.ShilianMapConfig"],"QSConfig.ShilianMapConfig")
	HookTable(QSConfig.SafeConfig,skipTables["QSConfig.SafeConfig"],"QSConfig.SafeConfig")
	
	--HookTable(_G.package.loaded['Logic.DgnEventSystem'].DgnEventInstance,skipTables["DgnEventSystem.DgnEventInstance"],"DgnEventSystem.DgnEventInstance")
	
	HookTable(_G,skipTables["_G"],"_G")
end

function Test(...)
	HookAllTableFuns(...)
end


local function VoidFun(...) 
end

g_skipFun = g_skipFun or {}
local function TrySkipFun(fname,t)
	if not fname or not t then return end
	g_skipFun[t] = g_skipFun[t] or {}
	if g_skipFun[t][fname] then return end
	--printf("skip %s",fname)
	g_skipFun[t][fname] = t[fname]
	t[fname] = VoidFun
end

local function VoidStrFun(...) return "" end
function skipEvent()
	do return end
	LogDebuging("skipEvent...")
	TrySkipFun("SceneCameraStartCurveOnly",qs)
	TrySkipFun("SceneCameraStopCurveOnly",qs)
	TrySkipFun("SceneCameraStartCurve",qs)
	TrySkipFun("SceneCameraStopCurve",qs)
	TrySkipFun("CameraNearOff",qs)
	TrySkipFun("CameraDofOff",qs)
	TrySkipFun("CameraEffectOff",qs)
	TrySkipFun("CameraNearOn",qs)
	TrySkipFun("CameraDofOn",qs) 
	TrySkipFun("CameraEffectTargetOn",qs)
	TrySkipFun("CameraEffectPosOn",qs)
	TrySkipFun("AudioPlay3DSound",qs)
	TrySkipFun("DgnEventHidePosFx",qs)
	TrySkipFun("DgnEventShowPosFx",qs)
	TrySkipFun("AddNpcPaoPaoEffect",qs)
	TrySkipFun("AudioModifyParam",qs)
	TrySkipFun("AudioPlay2DSound",_G)
	TrySkipFun("DgnEventGuide",_G)
	TrySkipFun("AudioModifyParam",_G)
	TrySkipFun("PlayMovieSubtitle",qs)
	TrySkipFun("PlayEmotionAnimation",qs)
	TrySkipFun("ShowPoemPanel",qs)
	TrySkipFun("HidePoemPanel",qs)
	TrySkipFun("PlaySequenceEmotionAnim",qs)
	TrySkipFun("StopEmotionAnimation",qs)
	TrySkipFun("ClearMovieSubtitle",qs)
	TrySkipFun("PlayFullScreenMovie",qs)
	TrySkipFun("PlayTutorialMovie",qs)
	--TrySkipFun("StartFlowOnEntity",qs)
	--TrySkipFun("EndFlowOnEntity",qs)
	--TrySkipFun("DgnEventSetEnvHour",qs)
	TrySkipFun("DgnTakeOverTimeControl",qs)
	TrySkipFun("DgnReleaseTimeControl",qs)
	
	for k,v in pairs(ScriptCustom) do
		if type(v) == "function" then
			ScriptCustom[k] = VoidFun
		end
	end
end

local function unSkipFunction()
	for k,v in pairs(g_skipFun) do
		for fname,f in pairs(v) do
			k[fname] = f
		end
	end
	g_skipFun = {}
end


local skipPack = {
	[4] = 0,--移动
	[5] = 0,--上升和下降
	[0xa] = 0,--转向
	
	[0x172] = 1,--技能
	
	[0xbe6] = 0,--周围玩家和一些其他数据
	[0xbeb] = 0,--选中
	[0xbf5] = 1,--玩家傀儡相关
	[0x13a6] = 0,--打开地图
	[0x1f4c] = 0,--验证?
	[0x1f6a] = 0,--心跳
	
	[0x233d] = 0,--向服务器请求队友数据
	[0x3783] = 1,--一直6W多长度? 有问题!
	
	[0x3e80] = 1,--使用物品
	
	--[0x4814] = 1,--定时申请
	[0x4fe4] = 1,--没有数据
	[0x5015] = 1,--打开地图
	[0x5368] = 0,--对方信息（团员？）
}


--[=[
--拍卖行无关
	[0x4fbb] = 0,
	[0xf] = 0,
	[0xbea] = 0,
	[0xbed] = 0,
	[0xbe7] = 0,
	[0xbe8] = 0,
	[0xdad] = 0,
	[0x1343] = 0,
	[0x583] = 0,
	[0x5088] = 0,
	[0x10d1] = 0,
	[0x547] = 0,
	[0x597] = 0,
	[0xbec] = 0,
	[0x264d] = 0,
	[0x3783] = 0,
	[0x58d] = 0,
	[0x17e] = 0,
	[0x11c6] = 0,
	[0xcef] = 0,
	[0x11ca] = 0,
	[0x4fb3] = 0,
	[0x4faa] = 0,
	[0x4fb0] = 0,
	[0x4fb4] = 0,
	[0x168] = 0,
	
	[0x4f0b] = 0,--玩法推荐
	
	--按M后
	[0x5018] = 0,
	[0x502c] = 0,
]=]


buffSkip = {
	[0x44e] = 0,--打坐
	[0xbb8] = 0,--打坐
	[0xd4c] = 0,--被攻击
}
buffDesc = {
	[0xc1d] = "战斗",
	[0xc81] = "剧情",
	[0x3ed] = "压制",
	[0x3ea] = "僵直",
	[0x3e9] = "倒地",
	[0x3ec] = "浮空",
	[0x3ff] = "技能特效",
	[0x423] = "闪避",
	[0xe3e] = "屏幕锁定",
	[0x70a] = "佑护",
	
	[0x2b16] = "读条",--可移动
	[0x2b02] = "低空",
	[0x2b04] = "中空",
	[0x2b05] = "高空",
	[0x2b16] = "骑乘",
	[0x2b14] = "加速1",
	[0x2b2b] = "加速2",
}
local dv = 0
local skipRecvPack = {
	[7] = 0,--移动的返回
	[0xb] = 0,--转向
	[0xf] = 0,--关务相关

	[0x15f] = dv,--玩家放技能
	[0x161] = 0,--跳起 状态改变
	[0x162] = 0,--回落
	[0x168] = dv,--
	[0x17e] = dv,--连招中断
	[0x489] = 1,--死亡
	--[0x515] = dv,--技能相关
	[0x533] = dv,--
	[0x583] = dv,--玩家功能
	[0x58d] = dv,--大量坐标
	[0x597] = dv,--
	[0xbe7] = dv,--周围玩家或者怪物行为
	[0xbe8] = dv,--周围玩家或者怪物行为
	[0xbea] = 0,--周围玩家或者怪物行为
	[0xbec] = 0,--选中
	[0xbed] = 0,--周围玩家或者怪物行为
	
	[0xd37] = dv,--怪物被攻击后
	[0xd38] = dv,--怪物被攻击后
	[0xd3b] = dv,--怪物被攻击后
	[0xd3c] = dv,--怪物被攻击后
	
	[0xda3] = 0,--玩家相关 技能?
	[0xdad] = 0,--玩家怪物 技能?
	[0xfbf] = dv,--怪物相关
	
	[0x1069] = 0,--玩家 移动?
	[0x11c6] = 0,
	[0x1339] = 0,--聊天信息
	[0x1343] = 1,--系统公告
	[0x138c] = dv,--事件触发
	[0x138d] = dv,--事件触发
	[0x139f] = dv,--事件触发
	[0x13a7] = dv,--回应包
	[0x13ab] = 0,--NPC? 较频繁
	[0x1f55] = 0,--较频繁
	[0x1f73] = 0,--较频繁
	
	[0x233c] = 0,--客户端请求的目标数据 组队
	[0x3783] = 0,--？？
	[0x3e81] = 1,--使用物品
	[0x3e82] = 1,--使用物品
	[0x4268] = dv,--技能相关
	--[0x4815] = dv,--回应包
	[0x4fbb] = dv,--打坐相关
	[0x5016] = dv,--回应包
	[0x5188] = dv,--回应包
	[0x5369] = 0,--对方信息
}
local uiMsgSkip = {
	["MSG_MOUSE_MOVE"] = 0,
}

g_tempSkipSendRecv = g_tempSkipSendRecv or {}
local GetNpcById = msk.npc.GetNpcById
function OnGameSend(head,data,len)
	--do return end
	--printf("send-%x",head)
	if skipPack[head] == 0 then return end
	local rlen = len
	if rlen > 300 and head ~= 0x2f8 then--创建角色的包
		--printf("太长")
		rlen = 300
	end
	msk.fireEvent(head,data,len)
	local dataaddr = data
	data = msk.data(data,rlen)
	if false and head == 0x172 then
		local pos,_,id = data:unpack("LL")
		if id == 0x1103dd then
			printf("替换")
			msk.sdata(dataaddr+4,0x10e821)
			data = msk.data(dataaddr,rlen)
			pos,_,id = data:unpack("LL")
		end
		printf("技能:%x-%s",id,qs.GetSpellName(id) or "??")
	end
	printf("send-%x:%d:%s",head,len,bin2hex(data,""))
	if head == 0x151a and not msk.g_issue then
		local pos,pid1,pid2,id,i1,i2,i3,i4,i5,i6 = data:unpack("LLLLLLLLL")
		local t = {}
		t.args = {id,i1,i2,i3,i4,i5,i6}
		t.eid = msk.api.GetCurrentEventID()
		--t.instanceId = msk.dword(0x020a1090)
		if  g_questInfo and msk.api.GetCurrentEventID() ~= -1 then
			msk.quest.FixEventInfo(g_questInfo)
			t.desc = msk.quest.GetRunTraceDesc(g_questInfo)
		else
			local desc = msk.quest.GetCurQuestDesc()
			t.desc = desc:match("(%[.-%])") or desc
		end
		local cur_group_id = qs.GetCurrentQuestTrackGroupId()
		local cur_state = qs.GetQuestGroupInfo(cur_group_id)
		t.qid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
		local f= io.open("qiaoduan_talk.lua","a")
		f:write(prettytostring(t),",\n")
		f:close()
	end
	if false and head == 0x1bef then
		local pos,tid,id1,id2,idx = data:unpack("LLLL")
		--local npc = GetNpcById(id1,id2)
		--local tname = npc and npc.name or "??"
		local tname = "??"
		local f= io.open("talk.txt","a")
		local cur_group_id = qs.GetCurrentQuestTrackGroupId()
		local cur_state = qs.GetQuestGroupInfo(cur_group_id)
		f:write('--',cur_state.quest_group_name:c_str(),'\n')
		f:write("    {",cur_group_id,",",cur_state.current_quest_id,",",cur_state.include_qiaoduan and "true" or "false",",",cur_state.current_qiaoduan_id,",",tid,",",idx,",'",tname,"',",cur_state.latest_quests_id:size(),"},\n")
		f:close()
	end
end

local IsAdd = false
function OnGameRecv(head,data,len,len2,len3)
	--do return end
	if len == 0 then 
		return
	end
	if skipRecvPack[head] == 0 then return end
	len = len - 6
	data = data + 6
	msk.fireEvent(head,data,len)
	local dataaddr = data
	local rlen = len
	if rlen > 300 then
		rlen = 300
	end
	--printf("recv-%x:%d:%d:%d",head,len,len2,len3)
	--do return end
	data = msk.data(data,rlen)
	if false and head == 0x15f then
		local pos,_,sid1,sid2,tid1,tid2 = data:unpack("LLLLL")
		printf("%x,%x recv释放技能:%x-%s",tid1,tid2,sid1,qs.GetSpellName(sid1) or "??")
	elseif head == 0x47f9 then
		local midPosData = data:sub(21,28)--msk.data(data+20,8)
		local pos,midx,midy = midPosData:unpack("LL")
		local otherData = data:sub(61,100)--msk.data(data+60,40)
		local pos,x1,y1,x2,y2,x3,y3,x4,y4,x5,y5 = otherData:unpack("LLLLLLLLLL")
		local t = {x1,y1,x2,y2,x3,y3,x4,y4,x5,y5}
		if g_addMap then
			for i=1,5 do
				DgnEventRemoveObjectToMinimap("Light", 1000+i) 
			end
		end
		g_addMap = true
		for i=1,5 do
			DgnEventAddObjectToMinimap("Light",t[i*2-1],t[i*2],1000+i ) 
		end
	elseif false and head == 0x4888 then
		local pos,_,_,qid1,qid2,tid1,tid2,x,y = data:unpack("LLLLLLLL")
		if g_addMapAnsha then
			DgnEventRemoveObjectToMinimap("Light", 2000) 
		end
		g_addMapAnsha = true
		DgnEventAddObjectToMinimap("Light",x,y,2000 ) 
		printf("recv-%x:%d:%d:%s",head,len,len2,bin2hex(data,""))
	elseif head == 0x3bc then
		local pos,_,_,_,tm,_,tm2 = data:unpack("LLLLLL")
		local tmdif = tm-tm2
		local fhMsg
		if tmdif > 365*24*60*60 then
			local year = math.floor(tmdif/(365*24*60*60))
			fhMsg = string.format("封停-%d年",year)
		else
			local day = math.floor(tmdif/(24*60*60))
			fhMsg = string.format("封停-%d天",day)
		end
		LogWarning("封号了-----%d,%d %s",tm,tm2,fhMsg)
		msk.fireEvent(0x2ef,0,fhMsg,tm)
	else
		printf("recv-%x:%d:%d:%s",head,len,len2,bin2hex(data,""))
	end
end


function OnGameUiMsg2(arg1,arg2,arg3,ECX)
	local msg = arg3--1

	local uiName = msk.str(arg2)
	local paranum = arg1-arg3
	paranum = paranum/4
	local ecx,eax,edx
	local addr
	local params = {}
	for i=0,paranum-1 do
		addr = arg3+i*0xc
		ecx = msk.dword(addr)--2
		eax = msk.dword(ecx+0x8)--3
		edx = eax
		if eax == 0 then
			edx = msk.dword(ecx)
			eax = edx
		end
		edx = msk.dword(eax+8)
		eax = msk.dword(edx)
		params[i+1] = eax
	end
	
	local msgName = msk.str(params[1])
	if uiMsgSkip[msgName] == 0 then return end
	LogDebuging("UI消息:%s --->%s"..string.rep("%x,",paranum-1),uiName,msgName,select(2,unpack(params)))
end

function OnGameUiMsg(arg1,arg2,arg3,ECX)
	local msg = arg3--1

	local uiName = msk.str(arg2)
	local ecx = msk.dword(msg)--2
	local eax = msk.dword(ecx+0x8)--3
	local edx = eax
	if eax == 0 then
		edx = msk.dword(ecx)
		eax = edx
	end
	if edx == 0 then
		LogWarning("空的UI消息:%s",uiName)
		return 0
	end
	ecx = msk.dword(eax+4)
	--LogDebuging("3ecx:%d",ecx)
	ecx = bit.rshift(ecx,6)
	--LogDebuging("4ecx:%x",ecx)
	if ecx == 1 then
		edx = msk.dword(eax+8)
		eax = msk.dword(edx)
	else
		eax = msk.dword(eax+8)
	end
	--LogDebuging("5eax:%x",eax)
	if eax == 0 then
		LogWarning("空的UI消息:%s",uiName)
		return
	end
	local msgName = msk.str(eax)
	if uiMsgSkip[msgName] == 0 then return end
	LogDebuging("UI消息:%s --->%s",uiName,msgName)
end

function OnGameKeyEvent(arg1,arg2,arg3,arg4)
	printf("OnGameKeyEvent:%x,%x,%x",arg2,arg3,arg4)
end

_G.OnGameSend = OnGameSend
_G.OnGameRecv = OnGameRecv
_G.OnGameKeyEvent = OnGameKeyEvent
_G.OnGameUiMsg = OnGameUiMsg

printf("msk.hook ok")