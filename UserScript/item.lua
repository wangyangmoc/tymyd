module("msk.item",package.seeall)
local prints = prints
local sleep = msk.sleep
local bagBase = msk.qcall(g_sig.GetNpcBase,0x12345678,2)
local mgnBase
local QSUIEquipSlotPanelSlotBase = g_sig.QSUIEquipSlotPanelSlotBase--msk.gdata.GetUISlotBase("QSUIEquipSlotPanel")
local equipSlotOff =  msk.dword(QSUIEquipSlotPanelSlotBase+0x8)
local bagSlotOff = g_sig.slotPosOff and g_sig.slotPosOff.bagSlot
local bagBagIdx = g_sig.bagBagIdx or 120
local slotOff = g_sig.slotOff

local item_off0 = g_sig.item_off0
local item_off1 = g_sig.item_off1
local item_off2 = g_sig.item_off2
local item_mt = {}
item_mt.__index = function (t,key)
	--printf("get:%s",key)
	local data = rawget(t,"data_")
	if key == "name" then
		data = msk.dword(data+0xc)
		return msk.str(msk.dword(data+4))
	end
	local off = item_off0[key]
	local ret
	if off then return msk.dword(data+off) end
	off = item_off1[key]
	data = msk.dword(data+0xc)
	if off then return msk.dword(data+off) end
	off = item_off2[key]
	if not off then return nil end
	data = msk.dword(data+off)
	return msk.dword(data)
end

function item_mt:Use()
	--printf("get:%s",key)
	msk.api.UseSlotById(self.id,0)
	sleep(0.8)
end

function FindByIdx(tid,t)
	mgnBase = mgnBase or msk.gdata.GetDataBase("XYItemManager")
	local head = msk.dword(mgnBase+0x1c)
	local node = head
	local id,data
	while node ~= 0 do
		id = msk.dword(node+0x10)
		if id == tid then
			data = msk.dword(node+0x14)
			if data ~= 0 then
				t = t or {}
				t.data_ = data
				setmetatable(t,item_mt)
				return t
			end
		elseif id < tid then
			node = msk.dword(node)
		else
			node = msk.dword(node+4)
		end
	end
end

function FindById(tid,node,t)
	mgnBase = mgnBase or msk.gdata.GetDataBase("XYItemManager")
	node = node or msk.dword(mgnBase+0x1c)
	if node == 0 then return end
	local data = msk.dword(node+0x14)
	local id = msk.dword(data+0x20)
	if id == tid then 
		t = t or {}
		t.data_ = data
		setmetatable(t,item_mt)
		return t
	end
	return FindById(tid,msk.dword(node),t) or FindById(tid,msk.dword(node+4),t)
end

local function PrintItemData(bdata,oidx1,oidx2)
	local iid,quality,name,data,equipLv,qualityLv,tp,dura,inum
	local idx2,idx1,id1,id2
	local sex,menpai,pos,ipos
	if bdata ~= 0 then
		data = msk.dword(bdata+0xc)
		idx1 = msk.byte(bdata+0x10)
		idx2 = msk.dword(bdata+0x14)
		id1,id2 = msk.byte(bdata+0x18),msk.byte(bdata+0x1c)
		tp = msk.dword(bdata+0x28)
		if tp == 2 then
			dura = msk.dword(bdata+0x70)
			inum = 1
		else
			dura = 0
			inum = msk.dword(bdata+0x68)
		end
		quality = msk.dword(data+8)
		sex = msk.dword(data+item_off1.sex)
		sex =  QSConfig.ItemTips.QSSex2Desc(sex)
		--QSConfig.ItemTips.QSEquipPosition2Desc()
		menpai = msk.dword(data+item_off2.menpai)
		menpai = msk.dword(menpai)
		menpai = QSConfig.ItemTips.QSMartialArt2Desc(menpai)
		if tp == 2 then
			pos = msk.dword(data+item_off2.pos)
			pos = msk.dword(pos)
			ipos = pos
			--if msk.dword(data+0xfc) == 2 then
				--pos = "职业!!!!!!!!!!!!!:"..QSConfig.ItemTips.QSIndentyPosition2Desc(pos)
			--else
				pos = QSConfig.ItemTips.QSEquipPosition2Desc(pos)
			--end
		else
			ipos = 0
			pos = "物品"
		end
		name = msk.dword(data+4)
		name = msk.str(name)
		iid = msk.dword(data)
		qualityLv =  msk.dword(data+0xc)
		equipLv = msk.dword(data+0x10)
		printf("pos:%d-%d-%d id:%x 型:%d 数:%d 品:%d 装:%d 等:%d 耐:%d 名:%s smp:%s,%s,%d,%s data:%x,%x",idx1,idx2,oidx2,iid,tp,inum,quality,equipLv,qualityLv,dura,name,sex,menpai,ipos,pos,bdata,data)
	end
end

local function PrintitemLeave(node)
	if node == 0 then return end
	PrintitemLeave(msk.dword(node+4))
	local data = msk.dword(node+0x14)
	PrintItemData(data,msk.dword(node+0xc),msk.dword(node+0x10))
	PrintitemLeave(msk.dword(node))
end

function PrintUserItem2()
	LogDebuging("打印用户物品")
	mgnBase = mgnBase or msk.gdata.GetDataBase("XYItemManager")
	local head = msk.dword(mgnBase+0x1c)
	PrintitemLeave(head)
end

local function checkSLot(t,k,v)
	if t[k] ~= v then
		prints("检测失败:",k,t[k],v)
	end
end

local _et,_pos,_tt
local function CheckItemLeave(node)
	if node == 0 then return end
	CheckItemLeave(msk.dword(node+4))
	local data = msk.dword(node+0x14)
	_tt = _tt or {}
	_tt.data_ = data
	setmetatable(_tt,item_mt)
	if _tt.type == 2 and _pos == _tt.pos and qs.GetPlayerLevel() >= _tt.lv and (qs.GetPlayerCareer() == _tt.menpai or _tt.menpai == 8 ) and (_tt.sex == 0 or _tt.sex == qs.GetPlayerGender() + 1) then
		--printf("当前装备符合要求,检测品质:%s",t.name)
		if not _et or (_tt.quality >= _et.quality and _tt.qualityLv > _et.qualityLv) then
			local tt = _et
			_et = _tt
			_tt = tt
			--printf("找到一个符合要求的装备:%s",_et.name)
		end
	--elseif t.type == 2 then
		--printf("失败:%s pos:%d lv:%d menpai:%d sex:%d",t.name,t.pos,t.lv,t.menpai,t.sex)
	end
	CheckItemLeave(msk.dword(node))
end

function GetBagNum(bagName)
	if bagName == "包袱" then return 4 end
	if bagName == "江湖行囊" then return 6 end
	if bagName == "五里袱" then return 8 end
	if bagName == "十里袱" then return 10 end
	return 0
end


function GetMoney()
	local plbag = msk.dwords(bagBase+4,0x20)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pl = msk.npc.GetMainPlayer()
	local plNode,plData
	for i=0,maxnum-1 do
		plNode = msk.dword(head+i*4)
		if plNode ~= 0 and msk.dword(plNode) == pl.id1 and msk.dword(plNode+4) == pl.id2 then
			plData = msk.dword(plNode+0x8)
			--LogDebuging("plNode:%x",plNode)
			return msk.dword(plData+0xc),msk.dword(plData+0x10),msk.dword(plData+0x14),msk.dword(plData+0x18)
		elseif plNode ~= 0 then
			--LogDebuging("slip:%x,%x-%x,%x",msk.dword(plNode),msk.dword(plNode+4),pl.id1,pl.id2)
		end
	end
	return 0,0,0,0
end

local __itemData
local function HookTips(data)
	__itemData = data
end
function GetItemData(addr,id,idx)
	if addr == nil then
		local t
		if id then
			t = FindById(id)
		elseif idx then
			t=  FindByIdx(idx)
		end
		addr = t and t.data_
	end
	if not addr then 
		LogDebuging("没有找到")
		return nil 
	end
	__itemData= nil
	local old1 = QSConfig.ItemTips.SetEquipTips
	local old2 = QSConfig.ItemTips.SetNullTips
	QSConfig.ItemTips.SetEquipTips = HookTips
	QSConfig.ItemTips.SetNullTips = HookTips
	--msk.qcall(GetItemDataCall,)
	QSConfig.ItemTips.SetEquipTips = old1
	QSConfig.ItemTips.SetNullTips = old2
end

function GetItemByName(iname)
	local plbag = msk.dwords(bagBase+4,0x18)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pldata,bgdata
	local bg,ed,bid,bdata,iid,quality,name,data,equipLv,qualityLv,tp,durability,inum
	local idx
	local t = {}
	local nt
	for i=0,maxnum-1 do
		pldata = msk.dword(head+i*4)
		if pldata ~= 0 then
			bgdata = msk.dword(pldata+0x8)
			bg = msk.dword(bgdata+0xc)
			ed = msk.dword(bgdata+0x10)
			idx = 0
			for j=bg+0x64,ed-1,4 do
				if idx >= bagBagIdx then
					break
				end
				bid = msk.dword(j)
				if bid ~= 0 then
					nt = FindByIdx(bid,t)
					if nt and nt.name == iname then
						return idx,nt
					end
				end
				idx = idx + 1
			end
		end
	end
	--LogWarning("无法找到指定物品：%s",iname)
end

function UseItemByName(iname)
	local idx = GetItemByName(iname)
	if idx ~= nil then
		msk.api.UseSlotByPos(idx)
		sleep(2.1)
		return true
	end
	return false
end


function GetBestEquip(pos,equip)
	mgnBase = mgnBase or msk.gdata.GetDataBase("XYItemManager")
	_et = equip
	_pos = pos
	_tt = {}
	local head = msk.dword(mgnBase+0x1c)
	--printf("sex:%d lv:%d pos:%d art:%d",qs.GetPlayerGender()+1,qs.GetPlayerLevel(),_pos,qs.GetPlayerCareer())
	CheckItemLeave(head)
	return _et
end
local _curEquips,_curBags,_curBagIds
local _tEquip,_tBag,_citt
local _citt
local __lastEquip = 0
local function CheckEquipProcess(t,pos,sec,bagpos)
	for i=1,#g_skipEquip do
		if g_skipEquip[i] == t.name then
			return false
		end
	end
	pos = pos or t.pos
	local slot = msk.api.GetSlotByPos(pos,equipSlotOff)
	if slot ~= 0 and msk.dword(slot) == t.id then return false end--同一件装备
	local orgPos = t.pos
	if slot == 0 or msk.dword(slot+0x3c) == 0 then
		LogDebuging("装备可用:%d %s",pos,t.name)
		msk.api.UseSlotById(t.id)
		sleep(0.8)
		return true
	end
	local tid = msk.dword(slot+0x3c)
	local _tt = t
	local _et = FindById(tid)--原有的装备
	if qs.GetPlayerLevel() >= _tt.lv and (qs.GetPlayerCareer() == _tt.menpai or _tt.menpai == 8 ) and (_tt.sex == 0 or _tt.sex == qs.GetPlayerGender() + 1) then--可用
		if not _et or (_tt.quality >= _et.quality and _tt.qualityLv > _et.qualityLv) then
			LogDebuging("装备可用:%d %s",pos,t.name)
			msk.api.UseSlotById(_tt.id,0)
			sleep(0.8)
			return true
		end
	end
	if not sec and orgPos == 10 then
		LogDebuging("戒指 检测另一个位置:%s",t.name)
		return CheckEquipProcess(t,11,true,bagpos)
	end
	if t.lv < g_unUseEquip.lv and t.quality < g_unUseEquip.quality and __lastEquip > 0 then--留一件 好分解
		LogDebuging("%s:%s",g_unUseEquip.measure,t.name)
		if g_unUseEquip.measure == "分解" then
			msk.api.DecomposeItem(bagpos)
		elseif g_unUseEquip.measure == "丢弃" then
			msk.api.DisCardItem(bagpos)
		end
		sleep(0.8)
	else
		__lastEquip = __lastEquip + 1
		return false
	end
	return true
end

function GetEquipByPos(pos)
	local slot = msk.api.GetSlotByPos(pos,equipSlotOff)
	if slot == 0 then return nil end
	local tid = msk.dword(slot+0x3c)
	return FindById(tid)
end

local function CheckBagItem(t,pos)
	local bnum = GetBagNum(t.name)
	if bnum <= 0 then return false end
	local bslot,tid
	for i=0,3 do
		bslot = msk.api.GetSlotByPos(i,bagSlotOff)
		tid = msk.dword(bslot+0x3c)
		if bslot == 0 or tid == 0 then--这个位置是空的
			LogDebuging("指定位置无背包 可替换:%d,%s",i,t.name)
			msk.api.UseSlotById(t.id,0)
			sleep(0.8)
			return true
		end
		local orgbg = FindById(tid)--原有的装备
		if GetBagNum(orgbg.name) < bnum then
			LogDebuging("比原有背包更大:%d %s",i,t.name)
			msk.api.UseSlotById(t.id,0)
			sleep(0.8)
			return true
		end
	end
	LogDebuging("丢弃背包:%s",t.name)
	msk.api.DisCardItem(pos)--没用的背包 丢掉
	sleep(0.8)
	return true
	--LogDebuging("背包太小:%s",t.name)
end

local function CheckUseItem(t,pos)
	if qs.GetPlayerLevel() < t.lv then--检测等级
		return false
	end
	local pstr = g_itemProcess[t.name]
	if pstr == "使用" then
		LogDebuging("使用 位:%d 名:%s",pos,t.name)
		local slot = msk.api.GetSlotByPos(pos)
		msk.api.UseSlot(slot)
		sleep(0.8)
	elseif pstr == "丢弃" then
		LogDebuging("丢弃 位:%d 名:%s",pos,t.name)
		msk.api.DisCardItem(pos)--没用的背包 丢掉
		sleep(0.8)
	else
		return false
	end
	return true
end

function CheckBag(ProcessAllEquip,count)
	count= count or 0
	if count > 5 then return end
	if qs.IsPlayerInBattle() then return end--战斗中不处理
	local plbag = msk.dwords(bagBase+4,0x18)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pldata,bgdata
	local bg,ed,bid,bdata,iid,quality,name,data,equipLv,qualityLv,tp,durability,inum
	local idx
	local t = {}
	local nt
	local useItem = false
	__lastEquip = ProcessAllEquip and 1 or 0
	for i=0,maxnum-1 do
		pldata = msk.dword(head+i*4)
		if pldata ~= 0 then
			bgdata = msk.dword(pldata+0x8)
			bg = msk.dword(bgdata+0xc)
			ed = msk.dword(bgdata+0x10)
			idx = 0
			for j=bg+0x64,ed-1,4 do
				if idx >= bagBagIdx then
					break
				end
				bid = msk.dword(j)
				if bid ~= 0 then
					nt = FindByIdx(bid,t)
					if nt then
						
					end
					if not nt then
						--LogWarning("无法找到指定物品:%d,%x",idx,slot)
					elseif t.type == 2 then
						--LogDebuging("处理装备:%d %d:%s",idx,t.pos,t.name)
						useItem = useItem or CheckEquipProcess(t,t.pos,nil,idx)
					elseif t.type == 3 then
						--LogDebuging("处理物品:%d %s",idx,nt.name)
						if not CheckBagItem(t,idx) then 
							useItem = useItem or CheckUseItem(t,idx) 
						else
							useItem = true
						end
					else
						--LogWarning("未知的物品:%d %s",t.type,t.name)
					end
				end
				idx = idx + 1
			end
		end
	end
	UIShowWindow("QSUIGiftNotifyPanel",false)
	UIShowWindow("QSUIPetInfoPanel",false)
	UIShowWindow("QSUIXinfaPanel",false)
	if useItem then return CheckBag(ProcessAllEquip,count+1) end
end

local function PrintUserItem()
	local plbag = msk.dwords(bagBase+4,0x18)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pldata,bgdata
	local bg,ed,bid,bdata,iid,quality,name,data,equipLv,qualityLv,tp,durability,inum
	local idx
	local t = {}
	for i=0,maxnum-1 do
		pldata = msk.dword(head+i*4)
		if pldata ~= 0 then
			bgdata = msk.dword(pldata+0x8)
			bg = msk.dword(bgdata+0xc)
			ed = msk.dword(bgdata+0x10)
			idx = 0
			for j=bg+0x64,ed-1,4 do
				bid = msk.dword(j)
				if bid ~= 0 then
					bdata = FindByIdx(bid,t)
					if bdata then
						printf("%d lv:%d 质:%d 名:%s data:%x",idx,t.lv,t.qualityLv,t.name,t.data_)
					else
						printf("错误的物品ID:%x",bid)
					end
				end
				idx = idx + 1
			end
		end
	end
end


function SplitItemByName(iname,snum)
	snum = snum or 1
	local plbag = msk.dwords(bagBase+4,0x18)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pldata,bgdata
	local bg,ed,bid,bdata,iid,quality,name,data,equipLv,qualityLv,tp,durability,inum
	local idx
	local t = {}
	local lastt
	local emptyPos,findPos,eid,fid
	local lastDif,savet
	local hasCheckNum = false
	for i=0,maxnum-1 do
		pldata = msk.dword(head+i*4)
		if pldata ~= 0 then
			bgdata = msk.dword(pldata+0x8)
			bg = msk.dword(bgdata+0xc)
			ed = msk.dword(bgdata+0x10)
			idx = 0
			for j=bg+0x64,ed-1,4 do
				bid = msk.dword(j)
				if bid ~= 0 then
					bdata = FindByIdx(bid,t)
					if t.name == iname then
						if t.num == snum then
							LogDebuging("已有指定数量的物品 不需要拆分")
							return bdata
						end
						if findPos == nil or lastDif > math.abs(t.num-snum) then
							savet = lastt
							findPos = idx
							lastDif = math.abs(t.num-snum)
							lastt = bdata
							t = savet or {}
						end
					end
				elseif emptyPos == nil then
					emptyPos = idx
				end
				idx = idx + 1
			end
		end
	end
	if findPos == nil then
		LogWarning("无法找到拆分物品：%s",iname)
	else
		return lastt
	end
end

function SplitItemByName2(iname,snum)
	snum = snum or 1
	local plbag = msk.dwords(bagBase+4,0x18)
	local head = msk.dword(plbag+0x8)
	local maxnum = msk.dword(plbag+0xc)
	local num = msk.dword(plbag+0x10)
	local pldata,bgdata
	local bg,ed,bid,bdata,iid,quality,name,data,equipLv,qualityLv,tp,durability,inum
	local idx
	local t = {}
	local emptyPos,findPos,eid,fid
	local hasCheckNum = false
	for i=0,maxnum-1 do
		pldata = msk.dword(head+i*4)
		if pldata ~= 0 then
			bgdata = msk.dword(pldata+0x8)
			bg = msk.dword(bgdata+0xc)
			ed = msk.dword(bgdata+0x10)
			idx = 0
			for j=bg+0x64,ed-1,4 do
				bid = msk.dword(j)
				if bid ~= 0 then
					bdata = FindByIdx(bid,t)
					if t.name == iname then
						if t.num == snum then
							LogDebuging("已有指定数量的物品 不需要拆分")
							return idx
						end
						if findPos == nil then
							findPos = idx
						end
					end
				elseif emptyPos == nil then
					emptyPos = idx
				end
				idx = idx + 1
			end
		end
	end
	if emptyPos==nil or findPos == nil then
		LogWarning("无法找到拆分物品：%s",iname)
	else
		LogDebuging("拆分:%d %d %d",findPos,emptyPos,snum)
		msk.api.Split(findPos,emptyPos,snum)
		sleep(0.8)
		return emptyPos
	end
	return false
end

function PrintAllSlot()
	LogDebuging("打印用户格子")
	local base = msk.dword(g_sig.QSSlotManagerBase)
	--printx(base)
	local hd = msk.dword(base+0x2c)
	local ed = msk.dword(base+0x30)
	LogDebuging("total:%x hd:%x ed:%x",(ed-hd)/4,hd,ed)
	local id,num,tp,tm,max_tm,name,bdata--0x24 0x28
	local idx = 0
	local slot
	for addr = hd,ed-1,4 do
		slot=msk.dword(addr)
		if slot ~= 0 then
			tp = msk.dword(slot+slotOff.type)
			id = msk.dword(slot+slotOff.id)
			if tp ~= 0 then
				if tp == 3 then
					name = qs.GetSpellName(id)
				elseif tp == 2 or tp == 1 then
					bdata = FindById(id)
					if bdata then
						name = bdata.name
					else
						name= qs.GetItemName(id)
						if name == "" then name = "oo" end
					end
				else
					name = "xx"
				end
				num = msk.dword(slot+slotOff.num)
				printf("pos:%x addr;%x data:%x tp:%d id:%x num:%d name:%s",idx,addr,slot,tp,id,num,name)
			else
				printf("pos:%x addr;%x data:%x tp:%d id:%x",idx,addr,slot,tp,id)
			end
		end
		idx = idx + 1
	end
end


function Test()
	return PrintUserItem2()
end

printf("msk.item ok")
--[[
pl bag
+0 pl id1
+4 pl id2
+8 data
+0x10 next

从itemmgn 找到+64的值...data
data
+0x20 id
找到后 +0x14 就是data

mgn
+0x1c head
--]]
--[[
itemnode
+0 pre
+4 next
+0x10 1d

item
+14 id
+0x18 id1
0x1c id2
--]]