--test.lua
--g_wgDir 目前是控制台路径 处理到主目录
math.randomseed(os.time())

g_wgDir = g_wgDir:match([[(.+\.-\).-]])
package.path = ';'..g_wgDir..[[lua\?.lua;]]..g_wgDir..[[UserScript\?.lua]]

--g_changingMap = true
g_globalTbl = {}
g_sendDataBuff = msk.new(4096)

local lvInfo = 0
local lvDebug = 1
local lvWarn = 2
local lvError = 3
g_LvPrint = msk.g_issue and lvWarn or lvInfo
g_LvPrint = 0
local g_LvPrint = g_LvPrint
local g_pauseAddr = g_pauseAddr
local coroutine = coroutine
local timerInter = 0.1
g_gamePause = false
local g_gamePause = g_gamePause
--打印系列函数
function printf(fmt,...)
	fmt = tostring(fmt)
    local str = string.format("msk_td->"..fmt,...)
	msk.print(str,g_LvPrint)
end

local print = printf
function prints(...)
    local t= {...}
    for i=1,#t do
        t[i] = tostring(t[i])
    end
    local str = 'msk_td->'..table.concat(t,', ')
    msk.print(str,g_LvPrint)
end
function printt(t,tname)
    tname = tname or ""
	printf("table:%s",tname)
	for k,v in pairs(t) do
		printf("  %s = %s",tostring(k),tostring(v))
	end
	printf("\n")
end
function hex(num)
	local nnum = tonumber(num)
	if not nnum then return num end
	return string.format("%#x",nnum)
end
function printx(...)
	local t = {...}
	for i=1,#t do
		if type(t[i]) == "number" then
			t[i] = string.format("%#x",t[i])
		end
	end
	printf(table.concat(t,", "))
end

function LogInfo(...)
	if g_LvPrint > lvInfo then
		return
	end
	printf("**信息**:"..fmt,...)
end

function LogWarning(fmt,...)
	if g_LvPrint > lvWarn then
		return
	end
	fmt = tostring(fmt)
    local str = string.format("msk_td->**警告**:"..fmt,...)
	msk.print(str,g_LvPrint)
	local f = io.open(g_wgDir..[[Temp\]]..g_qq..".txt",'a')
	f:write(os.date("%c")," ",str,"\n")
	f:close()
end

function LogDebuging(fmt,...)
	if g_LvPrint > lvDebug then
		return
	end
	printf("**调试**:"..fmt,...)
end
function LogError(fmt,...)
	if g_LvPrint > lvError then
		return
	end
	printf("**错误**:"..fmt,...)
end
local function coresume(f,...)
	local ok,err = coroutine.resume(f,...)
	if not ok then
		if err and err:sub(1,2) ~= "__" then--__是中断提示
			err = err.."\n"..debug.traceback(f)
			err = err:gsub("\n","\nmsk_td->")
			LogWarning("线程执行出错:%s",err)
		else
			g_client:hset("log",g_qq.."_"..qs.GetServerTime(),err)
			LogWarning("scriptCmd:%s",err)
			msk.fireEvent("scriptCmd",err)
		end
	end
	return ok,err
end


local INDENT = "    "
local tinsert = table.insert
local tconcat = table.concat
function printr(root)
    local cache = { [root] = "." }
    local function _dump(t, indent, name)
        indent = indent or ""
        name = name or ""

        local temp = {}
        if t == nil then
            tinsert(temp, indent .. "nil")
        elseif type(t) == "table" then
            local next_indent = indent .. INDENT
            tinsert(temp, "{")

            for k, v in pairs(t) do
                local key = tostring(k)

                if cache[v] then
                    tinsert(temp, next_indent .. "+" .. key .. "=>{" .. cache[v] .. "}")
                else
                    local new_key = name .. "." .. key
                    if type(v) == "table" then
                        cache[v] = new_key
                    end
                    tinsert(temp, next_indent .. "+" .. key .. _dump(v, next_indent, new_key))
                end
            end

            tinsert(temp, indent .. "}")
        else
            tinsert(temp, ":" .. tostring(t) .. "")
        end

        return tconcat(temp, "\nmmsk_td->")
    end

   print(_dump(root))
end

function msk.dwords(...)
	local t = {...}
	local v = 0
	for i=1,#t do
		v = msk.dword(v+t[i])
	end
	return v
end

local sleepThreads = {}
local sleepThreadTimes = {}
local waitEvents = {}
local highSleepThreads = {}
local highSleepThreadTimes = {} 
local jq = 0
local hq = 0
function msk.sleep(tm,high)
	if tm <= 0 then return end
	local tt,tmt = sleepThreads,sleepThreadTimes
	if high == true then
		tt,tmt = highSleepThreads,highSleepThreadTimes
		hq = hq + tm
	else
		hq = 0
	end
	tt[#tt+1] = coroutine.running()
	tmt[#tmt+1] = tm
	coroutine.yield()
	
	if g_changingMap or qs.CheckHasBuff(0xc81) then--切图或者剧情
		printf("切图或剧情")
		if jq > 10 then
			g_client:hset("time",g_qq,os.time())
			jq = 0
		end
		jq = jq + 5
		return msk.sleep(5)
	elseif qs.CheckHasBuff(0xc80) then
		UIShowWindow("QSUINoviciateTutorialMoviePanel",false)
		if jq > 10 then
			g_client:hset("time",g_qq,os.time())
			jq = 0
		end
		jq = jq + 5
		return msk.sleep(5)
	elseif hq > 10 then
		g_client:hset("time",g_qq,os.time())
		hq = 0
	else
		jq = 0
	end
end

function msk.waitEvent(event,tm,th)
	tm = tm or -1
	local t= waitEvents[event] or {}
	table.insert(t,1,{coroutine.running(),tm,th})
	waitEvents[event] = t 
	return coroutine.yield()
end

function msk.fireEvent(event,...)
	local t = waitEvents[event]
	if not t or #t == 0 then return false end
	local th
	local cth = coroutine.running()
	for i=1,#t do
		if t[i][3] == cth then
			th = t[i]
			table.remove(t,i)
			break
		end
	end
	if th == nil then
		th = t[#t]
		t[#t] = nil
	end
	--waitEvents[event] = nil
	local ok,err = coroutine.resume(th[1],...)
	if not ok then
		if err and err:sub(1,2) ~= "__" then--__是中断提示
			err = err.."\n"..debug.traceback(th[1])
			err = err:gsub("\n","\nmmsk_td->")
			LogWarning("线程执行出错:%s",err)
		end
	end
	return ok,err
end

function msk.newThread(f,...)
	f = coroutine.create(f)
	local ok,err = coresume(f,...)
	return err or f
end

function msk.pcall(f,...)
	f = coroutine.create(f)
	return coresume(f,...)
end

function msk.ensure(con,msg)
	if con then return end
	--记录错误
	LogWarning("出错了:%s",msg or "??")
	error(msg,0)
	--msk.api.QuitGame()
end

local loadedmodule = {}
function msk.require(mname,force)
	if not force and loadedmodule[mname] then return end
	--[==[
	mname = mname:gsub("%.","\\")
	local fname = g_wgDir..[[lua\]]..mname..".lua"
	local f,ferr = msk.loadfile(fname)
	if not f then
		fname = g_wgDir..[[UserScript\]]..mname..".lua"
		f,ferr = msk.loadfile(fname)
	end
	--]==]
	local f,ferr = mskg.loadfile(mname)
	if not f then
		local err = string.format("require wrong file:%s err:%s",mname,tostring(ferr))
		error(err,0)
	end
	local ret = f() or true
	if ret then 
		if not force then
			loadedmodule[mname] = ret
		end
	end
	return ret 
end

local __loadfile = NoReload("_G.loadfile", _G.loadfile)
local g_lastRunFile
local function SysCall(cmd,arg)
    repeat
		printf("syscall:%s-%s",cmd,arg)
        if cmd == 'runfile' then
			if g_lastRunFile then
				for i=1,#sleepThreads do
					if sleepThreads[i] == g_lastRunFile then
						if sleepThreadTimes[i] <= 0 then
							table.remove(sleepThreads,i)
							table.remove(sleepThreadTimes,i)
							break
						end
					end
				end
			end
			f ,err= mskg.loadfile(arg)
			if f then
				f = coroutine.create(f)
				g_lastRunFile = f
				coresume(f)
			else
				printf("执行脚本 错误:%s %s",arg,err)
			end
			--]]
		elseif cmd == "stopall" then
			sleepThreads = {}
			sleepThreadTimes = {}
			waitEvents = {}
			highSleepThreads = {}
			highSleepThreadTimes = {} 
		elseif cmd == "pause" then
			g_gamePause = not g_gamePause
			if g_gamePause then
				AddSelfTalkTime("已暂停",3,3,"")
			else
				AddSelfTalkTime("已继续",3,3,"")
			end
        end
        cmd,arg = coroutine.yield()
    until false
end

local systhread
if g_issue then
	 msk.syscall = function()
		if msk.dword(g_pauseAddr) == 1 and g_gamePause == false then
			msk.sdword(g_pauseAddr,0)
			return "pause",""
		elseif msk.dword(g_pauseAddr) == 2 and g_gamePause == true then
			g_gamePause = true
			msk.sdword(g_pauseAddr,0)
			return "pause",""
		end
	 end
else
	local __getcount = 0
	msk.syscall = function()
		if __getcount < 5 then
			__getcount = __getcount + 1
			return nil
		end
		__getcount = 0
		local cmd = g_client:hget("sysflag",g_qq)
		if cmd then
			g_client:hdel("sysflag",g_qq)
			return cmd:match("(%w+):(.+)")
		end
		if msk.dword(g_pauseAddr) == 1 and g_gamePause == false then
			msk.sdword(g_pauseAddr,0)
			return "pause",""
		elseif msk.dword(g_pauseAddr) == 2 and g_gamePause == true then
			g_gamePause = true
			msk.sdword(g_pauseAddr,0)
			return "pause",""
		end
	end
end
local function mskTimeout()
	--print("mskTimeout")
	local cmd,arg = msk.syscall()
	local ok,err
    if cmd then
		printf("执行命令:%s,%s",cmd,arg)
        ok,err = coresume(systhread,cmd,arg)
        if not ok then
            systhread = coroutine.create(SysCall)
        end
    end
	if g_gamePause then return end
	if #highSleepThreadTimes > 0 then--高优先级别的线程
		highSleepThreadTimes[1] = highSleepThreadTimes[1] - timerInter
		if highSleepThreadTimes[1] < 0 then
			local th = table.remove(highSleepThreads,1)
			table.remove(highSleepThreadTimes,1)
			coresume(th)
		end
		return
	end
	----[[
	for i=1,#sleepThreadTimes do
		sleepThreadTimes[i] = sleepThreadTimes[i]-timerInter
	end
    local runList = {}
	for i=#sleepThreadTimes,1,-1 do
		if sleepThreadTimes[i] <= 0 then
			table.insert(runList,table.remove(sleepThreads,i))
			table.remove(sleepThreadTimes,i)
		end
	end
	for k,v in pairs(waitEvents) do
		for i=#v,1,-1 do
			if v[i][2] ~= -1 then--带超时时间的事件
				v[i][2] = v[i][2] - timerInter
				if v[i][2] <= 0 then
					table.insert(runList,v[i][1])
					table.remove(v,i)
				end
			end
		end
	end
	if #runList > 0 then
		--print("run sleep data")
		for i=1,#runList do
			coresume(runList[i])
		end
	end
	--]]
end

local Counters
local Names
local SkipFuncs

local function getnname(n)
	if n.what == "C" then
		return n.name or "?"
	end
	local lc  = string.format("[%s]:%s",n.short_src,n.linedefined)
	if n.namewhat ~= "" then
		return string.format("%s (%s)",lc,n.name or "??")
	else
		return lc or "???"
	end
end

local function hook()
	local f = debug.getinfo(2,"f").func
	if SkipFuncs then 
		if not SkipFuncs[f] then
			printf("%s",getnname(debug.getinfo(2,"Sn")))
		end
		return 
	end
	if Counters[f] == nil then
		Counters[f] = 1
		Names[f] = debug.getinfo(2,"Sn")
	else
		Counters[f] = Counters[f] + 1
	end
end

local function getname(func)
	local n = Names[func]
	if n.what == "C" then
		return n.name or "??"
	end
	local lc  = string.format("[%s]:%s",n.short_src,n.linedefined)
	if n.namewhat ~= "" then
		return string.format("%s (%s)",lc,n.name or "??")
	else
		return lc or "??"
	end
end

function HookAll()
	debug.sethook()--移除其他的
	printf("HookAll...")
	Counters = {}
	Names = {}
	debug.sethook(hook,"c")
end

local function SortFuncNames(t1,t2)
	return t1[2] < t2[2]
end

function ShowHookInfo()
	printf("ShowHookInfo...")
	local t = {}
	for func,count in pairs(Counters) do
		table.insert(t,{getname(func),count})
	end
	table.sort(t,SortFuncNames)
	for i=1,#t do
		printf("%s %d",t[i][1],t[i][2])
	end
end

function SkipFunc(skip)
	if skip == nil then skip = true end
	if skip then
		SkipFuncs = Counters
		Counters = {}
		Names = {}
	else
		SkipFuncs = nil 
	end
end

function g_hset(k,f,t)
	t = json.encode(t)
	g_client:hset(k,f,t)
end

function g_hget(k,f)
	local t = g_client:hget(k,f)
	return t and json.decode(t)
end

--初始化一些数据
if mskTimer then mskTimer:Destory() end
mskTimer = Timer(timerInter, mskTimeout,-1)
mskTimer:start()
print("test.lua run ok")
systhread = coroutine.create(SysCall)--启动测试线程
coresume(systhread,'runfile',"GameTest")--调用一次脚本进行HOOK 
