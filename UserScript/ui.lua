module("msk.ui",package.seeall)
local print = _G.printf
local panelBase = g_sig.panelBase
local uiMsgCallOffset = g_sig.uiMsgCallOffset or 0x9c

local Offset_Control1=0x0000004C
local Offset_Control2=0x0000002C
local Offset_Control3=0x0000002C
local Offset_ControlVisible=0x0000003E
local Offset_ControlMark=0x0000006E
local Offset_ControlShl=0x0000000E 
local Offset_ControlObj=0x0000007C
local Offset_ControlName=0x00000044
local Offset_UI_IsText=0x000000A8
local RM_DWORD = msk.dword
local RM_BYTE = msk.byte
local RM_WORD = msk.word

local uiMsg1 = msk.new(0xc)--1
local uiMsg2 = msk.new(0x18)--2
local uiMsg3 = msk.new(0xc)--3
local uiMsgArg1 = msk.new(0x100)--3
local uiMsgArgStringAddr = msk.new(0xc)--3
local uiMsgArgString = msk.new(0x100)--3
msk.sdword(uiMsgArgStringAddr,uiMsgArgString)
msk.sdword(uiMsgArgStringAddr+4,0)
msk.sdword(uiMsgArgStringAddr+8,0)
msk.sdword(uiMsgArgStringAddr+0xc,0)

local uiMsg4 = msk.new(4)--4
local uiMsgString = msk.new(100)--消息字符串
local uiMsgUiName = msk.new(100)--窗口名
--先初始化第1层的数据
msk.sdword(uiMsg1,uiMsg2)
msk.sdword(uiMsg1+4,uiMsg2+0x18)
msk.sdword(uiMsg1+8,uiMsg2+0x18)
--第2层
msk.sdword(uiMsg2,uiMsg3)
msk.sdword(uiMsg2+4,0)
msk.sdword(uiMsg2+8,0)

msk.sdword(uiMsg2+0xc,uiMsgArg1)
msk.sdword(uiMsg2+0x10,0)
msk.sdword(uiMsg2+0x14,0)
--第3层
msk.sdword(uiMsg3,0)
msk.sdword(uiMsg3+4,0x46)
msk.sdword(uiMsg3+8,uiMsg4)

msk.sdword(uiMsgArg1,0)
--第4层
msk.sdword(uiMsg4,uiMsgString)
function UIMsgCall(uiName,Msg,arg1)
	local ui,cuiName = msk.ui.GetPaneByName(uiName,true)
	if not ui then
		LogWarning("找不到指定UI:%s",uiName or nil)
		return false
	end
	if  uiName == "QSUIDialogPanel" then --Msg == "CMD_OK" then
		arg1 = arg1 or '0'
		msk.sdword(uiMsgArg1+4,0)
		msk.sdword(uiMsgArg1+8,uiMsgArgStringAddr)
		msk.sdata(uiMsgArgString,arg1,arg1)
		msk.sbyte(uiMsgArgString+#arg1,0)
	elseif arg1 then
		msk.sdword(uiMsgArg1+4,2)
		msk.sdword(uiMsgArg1+8,arg1)
	end
	if arg1 then
		msk.sdword(uiMsg1+4,uiMsg2+0x18)
		msk.sdword(uiMsg1+8,uiMsg2+0x18)
	else
		msk.sdword(uiMsg1+4,uiMsg2+0xc)
		msk.sdword(uiMsg1+8,uiMsg2+0xc)
	end
	msk.sdata(uiMsgUiName,cuiName)
	msk.sbyte(uiMsgUiName+#cuiName,0)
	msk.sdata(uiMsgString,Msg)
	msk.sbyte(uiMsgString+#Msg,0)
	local vptr = msk.dword(ui)
	local call = msk.dword(vptr+uiMsgCallOffset)
	--LogDebuging("uicall:%x",call)
	msk.qcall(call,ui,0,uiMsg1,uiMsgUiName,uiMsg1+0x10)
	return true
end

local function PrintPane(node,head,ed)
	if node == 0 or node == head then return end
	--PrintPane(msk.dword(node),head)
	local data,name
	data = msk.dword(node+8)
	name = msk.dword(data+0xc)
	name = msk.str(name)
	printf("%x %s",data,name)
	return PrintPane(msk.dword(node+4),head)
end

function PrintAllPanel(tname,notfit,onlyvis)
	local windowRoot = panelBase
	windowRoot = msk.dword(windowRoot)
	local head = windowRoot+0x3c
	local node = msk.dword(head)
	--PrintPane(node,head)
	--do return end
	local data,name
	local maxloop = 500
	while node ~= 0 and node ~= head and maxloop > 0 do
		data = msk.dword(node+8)
		name = msk.dword(data+0xc)
		name = msk.str(name)
		if tname and ((not notfit and name == tname ) or (notfit and string.find(name,tname) )) then
			return data,name
		elseif not tname then
			name = name:sub(1,-2)
			if not onlyvis or (onlyvis and UIIsVisible(name)) then
				printf("%x %s",data,name)
			end
		end
		node = msk.dword(node)
		maxloop = maxloop - 1
	end
end

function GetPaneByName(name,fit)
	return PrintAllPanel(name,fit)
end

function Test(onlyvis)
	PrintAllPanel(nil,nil,onlyvis)
end

--代码来源其他人
local R_DW = msk.dword
local R_W = msk.word
local R_B = msk.byte
function GET_UI_IS_TEXT(o)		
	return (R_DW(o + 0xA8) == -1)
end
function GET_UI_TEXT(o)		
	local dwDescAddr = R_DW(o + 0x94) + 7
	if dwDescAddr ~= 0 and bit.band(dwDescAddr, 0xFFFF0000) ~= 0 then --[[字符串&0xFFFF0000 不为0--]]
		return msk.str(dwDescAddr) or ""
	else
		LogWarning("!!!!!!!!!!!!!!错误的文本")
		return ""
	end
end
function GET_UI_TEXT(o)		
	local dwDescAddr = R_DW(o + 0x94) + 7
	return msk.str(dwDescAddr)
end
function GET_UI_OBJ_IS_VISIBLE(o)	
	return (bit.rshift(R_W(o + 0x3e),0xe) == 1)
end
function GET_UI_OBJ_HAS_SUB(o)	
	return (R_B(o + 0x3E) >= 0x80 and R_W(o + 0x6e) == 0xffff)
end
function GET_UI_OBJ_ENABLE(o)
	return R_DW(o + 0xDC) ~= 0
end
function GET_UI_NAME(o)
	local dwNameAddr = R_DW(o + 0x44)
	if dwNameAddr ~= 0 and bit.bor(dwNameAddr,0xFFFF0000) ~= 0 then
		dwNameAddr = R_DW(dwNameAddr + 0x08)
		dwNameAddr = R_DW(dwNameAddr)
		return msk.str(dwNameAddr) or ""
	else
		return ""
	end
end
local windowMt = {}
windowMt.__index = function(t,key)
	--LogDebuging("get %s",key)
	local PObject_PPTR = rawget(t,"obj_")
	if key == "visible" then
		return GET_UI_OBJ_IS_VISIBLE(PObject_PPTR)
	elseif key == "enable" then
		return GET_UI_OBJ_ENABLE(PObject_PPTR)
	elseif key == "name" then
		return GET_UI_NAME(PObject_PPTR)
	elseif key == "text" then
		if RM_DWORD(PObject_PPTR + Offset_UI_IsText) == -1 then
			return GET_UI_TEXT(PObject_PPTR)
		end
		return ""
	elseif type(key) == "number" then
		local dwBase = PObject_PPTR
		local dwUISubBaseAddr = msk.dword(dwBase + 0x7C)
		local  dwNum = msk.dword(dwBase + 0x80);
		if dwNum > key and key >= 0 then--
			dwObj = R_DW(dwUISubBaseAddr + key * 0xc)
			local newWnd = {obj_ = dwObj}
			setmetatable(newWnd,windowMt)
			return newWnd
		end
		return nil
	else
		local dwBase = PObject_PPTR
		local dwUISubBaseAddr = msk.dword(dwBase + 0x7C)
		local  dwNum = msk.dword(dwBase + 0x80);
		local dwObj
		for i=0,dwNum-1 do
			dwObj = R_DW(dwUISubBaseAddr + i * 0xc)
			if GET_UI_NAME(dwObj) == key then
				local newWnd = {obj_ = dwObj}
				setmetatable(newWnd,windowMt)
				return newWnd
			end
		end
	end
end

function PrintUiText(pObj,layer)
	local dwVFTable = R_DW(pObj + 0x0)
	local uiName = GET_UI_NAME(pObj)
	local IsText = GET_UI_IS_TEXT(pObj)
	local enable = GET_UI_OBJ_ENABLE(pObj)
	if IsText then
		local lpName2 = GET_UI_TEXT(pObj)
		LogDebuging(layer..":%s-%s enable:%s %x", uiName,lpName2 or "nil",enable and "true" or "false",pObj)
	else
		LogDebuging(layer..":%s-nil enable:%s %x", uiName,enable and "true" or "false",pObj)
	end
end

function PrintUi(dwBase,layer,ProcessF)
	layer = layer or 0
	local	dwUISubBaseAddr = msk.dword(dwBase + 0x7C)
	local  dwNum = msk.dword(dwBase + 0x80);
	if dwUISubBaseAddr ~= 0 and GET_UI_OBJ_HAS_SUB(dwBase) and GET_UI_OBJ_IS_VISIBLE(dwBase) then--
		for i = 0,dwNum-1 do
			dwObj = R_DW(dwUISubBaseAddr + i * 0xc)
			ProcessF(dwObj,layer.."."..i)
			PrintUi(dwObj,layer.."."..i,ProcessF);
		end
	end
end

function ProcessAllUi(ProcessF)
	local CWindowUIControlAddr = g_sig.uiBase
	local dwBase = R_DW(R_DW(R_DW(R_DW(R_DW(R_DW(CWindowUIControlAddr) + 9 * 4) + 0x4c) + 0x2c) + 0x2c) + 4)
	PrintUi(dwBase,0,ProcessF)
end

function PrintAllUi()
	ProcessAllUi(PrintUiText)
end

local function __GetChildByName(ui,name,notfit)
	local dwUISubBaseAddr = msk.dword(ui + 0x7C)
	local dwNum = msk.dword(ui + 0x80);
	local dwObj,cname
	--LogDebuging("get %s",name)
	if dwUISubBaseAddr ~= 0 then--
		for i = 0,dwNum-1 do
			dwObj = R_DW(dwUISubBaseAddr + i * 0xc)
			cname = GET_UI_NAME(dwObj)
			if (not notfit and  cname == name) or (notfit and cname:find(name)) then
				--LogDebuging("get ok %s",cname)
				return dwObj
			else
				--LogDebuging("get skip %s",cname)
			end
		end
	end
	--LogDebuging("get error %s",name)
end

local __panelRoot
function GetPanel(pname,fit)
	if __panelRoot == nil then
		local CWindowUIControlAddr = g_sig.uiBase
		local dwBase = R_DW(R_DW(R_DW(R_DW(R_DW(R_DW(CWindowUIControlAddr) + 9 * 4) + 0x4c) + 0x2c) + 0x2c) + 4)
		__panelRoot = __GetChildByName(dwBase,"root1")
	end
	local dwUISubBaseAddr = msk.dword(__panelRoot + 0x7C)
	local dwNum = msk.dword(__panelRoot + 0x80);
	local dwObj,cname,pbase
	for i = 0,dwNum-1 do
		dwObj = R_DW(dwUISubBaseAddr + i * 0xc)
		pbase = __GetChildByName(dwObj,pname,not fit)
		if pbase then
			local newWnd = {obj_ = pbase}
			setmetatable(newWnd,windowMt)
			return newWnd
		end
	end
end

function GetPanelData(pname,sub1,...)
	local _ui = GetPanel(pname)
	if not _ui then return end
	if sub1 then
		local subs = {sub1,...}
		for i=1,#subs do
			LogDebuging("获取:%s",tostring(subs[i]))
			_ui = _ui[subs[i]]
			if not _ui then return end
			if #subs ~= i then
				LogDebuging("获取到:%s %x",_ui.name,_ui.obj_)
			end
		end
	end
	return _ui
end
function GetVisiblePanelData(pname,sub1,...)
	local _ui = GetPanel(pname)
	if not _ui then return end
	if sub1 then
		local subs = {sub1,...}
		for i=1,#subs do
			LogDebuging("获取:%s",tostring(subs[i]))
			_ui = _ui[subs[i]]
			if not _ui or not _ui.visible then return end
			if #subs ~= i then
				LogDebuging("获取到:%s %x",_ui.name,_ui.obj_)
			end
		end
	end
	return _ui
end

printf("msk.ui ok")

--[[
UiTemplete
01329113    B9 F81E0202     mov ecx,WuXia_Cl.
+24 head

UINode
+8 data
+10 next

UIList
01329123    8B0D 18CF0802   mov ecx,dword ptr ds:[0x208CF18]
+0x3c head

UINode
+0 next
+8 data

UIData 目标窗口+194是内�?
vptr +0 �?
+c name
+1c templeteName
+2c 模板文件
--]]


