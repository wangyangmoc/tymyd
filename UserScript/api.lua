module("msk.api",package.seeall)
local printf = _G.printf
local sleep = msk.sleep
local math = math
local UIShowWindow = _G.UIShowWindow
local GetMainPlayer = msk.npc.GetMainPlayer
local GetNpcByName = msk.npc.GetNpcByName
--local UseSkill = msk.skill.UseSkill
local isHaveAction = g_sig.isHaveAction
local moveToPositionCall = g_sig.MoveToPosition
local useSkillCall = g_sig.UseBarSkill
local QSSlotManagerBase = g_sig.QSSlotManagerBase
local UseBarSkill = g_sig.UseBarSkill
local InputProcessorBase = g_sig.QSInputProcessorBase
local currentMapId = g_sig.currentMapId
local mconfig = msk.config
local CallKeyEvent = g_sig.KeyEvent
local CallChangeSlot = g_sig.ChangeSlot
local slotOff = g_sig.slotOff
local g_cacheData = g_cacheData
local mapPositionDataBase = g_sig.mapPositionDataBase
local DisCardItemCall = g_sig.DisCardItem
local normalMapBegin = g_sig.normalMapBegin
local normalMapEnd = g_sig.normalMapEnd
local mapDataBase = g_sig.mapDataBase
local mapIdOff = g_sig.mapIdOff
local InitMapInsCall = g_sig.InitMapIns
local _mabs = math.abs
local jinglianItemCountOff= g_sig.jinglianItemCountOff
--按键CALL
local keyEventData = msk.new(0x16)
msk.sdword(keyEventData+4,5)
--拖动技能
local virtualSlot = msk.new(0x3c)

local hex2bin2 = msk.tools.hex2bin2
local hex2bin = msk.tools.hex2bin
local bin2hex = msk.tools.bin2hex

local QSUISkillInfoPanelSlotBase = g_sig.QSUISkillPanelSlotInfoBase--msk.gdata.GetUISlotBase("QSUISkillPanel")
local g_skillSlotOff =  msk.dword(QSUISkillInfoPanelSlotBase+0x8)
printf("g_skillSlotOff %x",g_skillSlotOff)
local movingOff = g_sig.movingOff or 0x2ac
local QSSlotManagerBase = g_sig.QSSlotManagerBase
local Send = msk.pack.Send
local Sendf = msk.pack.Sendf
local UserBarSkillArg3 = g_sig.UserBarSkillArg3
local GeRuntimeConfigBase = g_sig.GeRuntimeConfigBase
local GeRuntimeConfigSpeedOff = g_sig.GeRuntimeConfigSpeedOff
local useSlotNormalOff = g_sig.useSlotNormalOff or 0x20
local useSlotEquipOff = g_sig.useSlotEquipOff or 0xc

local function __MoveToPosition(x,y,layer)
	x = msk.f2i(x)
	y = msk.f2i(y)
	local speed = msk.dword(GeRuntimeConfigBase)--msk.f2i(200)
	speed = msk.dword(speed+GeRuntimeConfigSpeedOff)
	--local speed = 0
	layer = layer or 0
	local ret = msk.qcall(moveToPositionCall,0x12345678,0,layer,speed,y,x)
	return ret == 1
end

function HookRecv()
	g_cacheData.hookRecv = g_cacheData.hookRecv+1
	if g_cacheData.hookRecv == 1 then
		mskg.HookRecv()
	end
end

function HookSend()
	g_cacheData.hookSend = g_cacheData.hookSend+1
	if g_cacheData.hookSend == 1 then
		mskg.HookSend()
	end
end

function UnHookSend()
	if g_cacheData.hookSend == 0 then
		LogWarning("收包函数并没有被HOOK  无法取消")
	end
	g_cacheData.hookSend = g_cacheData.hookSend-1
	if g_cacheData.hookSend == 0 then
		mskg.UnHookSend()
	end
end

function UnHookRecv()
	if g_cacheData.hookRecv == 0 then
		LogWarning("收包函数并没有被HOOK  无法取消")
	end
	g_cacheData.hookRecv = g_cacheData.hookRecv-1
	if g_cacheData.hookRecv == 0 then
		mskg.UnHookRecv()
	end
end

function MoveDown(x,y,pl,h)
	for i=1,10 do
		if pl.z > h then
			pl:FaceTo(x*100,y*100)
			msk.api.SetMainPlayerMove(true)
			sleep(3)
		else
			msk.api.SetMainPlayerMove(false)
			break
		end
	end
end

function MoveForWard(x,y,timeout)
	x,y=  x*100,y*100
	local pl = msk.npc.GetMainPlayer()
	pl:FaceTo(x,y)
	timeout = timeout or 20
	for i=1,timeout do
		SetMainPlayerMove(true)
		if pl:IsNear(x,y) then
			break
		end
		sleep(1)
	end
	SetMainPlayerMove(false)
end

function DisCardItem(pos)
	if os.time() - g_cacheData.logTime <= 180 then
		return false
	end
	msk.qcall(DisCardItemCall,msk.dword(QSSlotManagerBase),0,pos)
end

function GeneralTest(id1,id2,id3)
	return _G.GeneralTest(id1,id2,id3)
end

function BackToSelect()
	if GetCurrentMapId() == 10050 then
		return true
	end
	UIShowWindow("QSUIMenuPanel",true)
	msk.ui.UIMsgCall("QSUIMenuPanel","SWITCH_CHARACTER")
	msk.ui.UIMsgCall("QSUIGrowUpRecommemnded","onQuitOkClick")
	sleep(1)
	--sleep(5)
end

local function __IsPlayerInBattle()
	return qs.IsPlayerInBattle() or qs.CheckHasBuff(0xc1d)
end

function BackAndEnterWorld()
	if __IsPlayerInBattle() then
		return false
	end
	BackToSelect()
	EnterToWorld()
	msk.waitEvent("enterMap")
	sleep(1)
end
function QuitGame(msg)
	g_client:hset("quit",g_qq,msg or "nil")
	g_gamePause = true
	sleep(1)
	do return  end
	if GetCurrentMapId() == 10050 or GetCurrentMapId() == 0 then
		msk.ui.UIMsgCall("QSUISelectCharacterPanel","BTN_CLICK_BACK")
		sleep(5)
	else
		UIShowWindow("QSUIMenuPanel",true)
		msk.ui.UIMsgCall("QSUIMenuPanel","EXIT_GAME")
		msk.ui.UIMsgCall("QSUIGrowUpRecommemnded","onQuitOkClick")
		sleep(5)
	end
	
end

local function ChangeSlot(slot,id,tp)
	if slot == 0 then return end
	msk.sdword(virtualSlot+0xc,tp)
	msk.sdword(virtualSlot+0x3c,id)
	msk.qcall(CallChangeSlot,virtualSlot,0,0xffffffff,slot)
end

function DragDownSkill(id)
	local slot = GetSlotById(0,g_skillSlotOff,nil,true)
	ChangeSlot(slot,id,3)
end



--local hasPrintData = {}
local function PrintMapData(mapData,prt)
	id1 = msk.dword(mapData)
	id2 = msk.dword(mapData+8)
	id3 = msk.dword(mapData+0xc)
	x = msk.float(mapData+0x10)
	y = msk.float(mapData+0x14)
	name1 = msk.dword(mapData+0x18)
	name1 = msk.str(name1)
	name2 = msk.dword(mapData+0x28)
	name2 = msk.str(name2)
	if prt then
		printf("%x:  %d,%x,%x %.2f,%.2f %s-%s",mapData,id1,id2,id3,x,y,name1,name2)
		--hasPrintData[name2] = true
	end
	return x,y,name2
end

function PrintAllTaskPos(tmap)
	local ecx = msk.dword(g_sig.mapPositionDataBase)
	local head = msk.dword(ecx+0x14)
	local num = msk.dword(ecx+0x18)
	local mapNode,mapData
	local mapid,mapPosHead,mapPosEnd
	local id1,id2,id3,x,y,name1,name2
	for i =0,num-1 do
		mapNode = msk.dword(head+i*4)
		while mapNode ~= 0 and (not tmap or msk.dword(mapNode) == tmap) do
			mapid = msk.dword(mapNode)
			mapPosHead = msk.dword(mapNode+4)
			mapPosEnd = msk.dword(mapNode+8)
			--printf("地图:%d-%s %d",mapid,qs.GetMapRealName(mapid),(mapPosEnd-mapPosHead)/4)
			for mapData = mapPosHead,mapPosEnd-1,0x38 do
				PrintMapData(mapData,true)
			end
			mapNode = msk.dword(mapNode+0x14)
		end
		printf("skip %x",mapNode ~= 0 and msk.dword(mapNode) or 0)
	end
end

function InitMapInsData(map)
	local ecx = msk.dword(mapPositionDataBase)
	msk.qcall(InitMapInsCall,ecx,0,map)
end

function GetMapNode(map)
	local ecx = msk.dword(mapPositionDataBase)
	local head = msk.dword(ecx+0x14)
	local num = msk.dword(ecx+0x18)
	local idx = map%num
	--printf("idx:%d num:%d",idx,num)
	local mapNode = msk.dword(head+idx*4)
	while mapNode ~= 0 and msk.dword(mapNode) ~= map do
		mapNode = msk.dword(mapNode+0x14)
	end
	return mapNode
end

function GetMapInsData(map,pos,initMapIns)
	local mapNode = GetMapNode(map)
	if mapNode == 0 and (map == GetCurrentMapId() or initMapIns) then
		InitMapInsData(map)
		mapNode = GetMapNode(map)
	end
	if mapNode == 0 then
		LogWarning("地图未初始化:%d",map)
		return nil
	end
	local mapid = msk.dword(mapNode)
	--printf("找到地图:%d %s %x",mapid,qs.GetMapRealName(mapid),mapNode)
	local mapPosHead = msk.dword(mapNode+4)
	local mapPosEnd = msk.dword(mapNode+8)
	for mapData = mapPosHead,mapPosEnd-1,0x38 do
		if msk.dword(mapData) == pos then
			return PrintMapData(mapData)
		end
	end
	LogWarning("寻找任务点失败了...")
end

function KeyEvent(key,flag)
	local ecx = msk.dword(InputProcessorBase)
	local externKey 
	if flag == "shift" then
		externKey = 0x10
	elseif flag == "ctrl" then
		externKey= 0x11
	end
	if externKey then
		msk.sdword(keyEventData+8,0)
		msk.sdword(keyEventData+0xc,externKey)
		msk.qcall(CallKeyEvent,ecx,0,keyEventData)
	end
	msk.sdword(keyEventData+8,0)
	msk.sdword(keyEventData+0xc,key)
	msk.qcall(CallKeyEvent,ecx,0,keyEventData)
	if externKey ~= 0x11 then--CTRL不触发技能
		msk.sdword(keyEventData+8,2)
		msk.sdword(keyEventData+0xc,0)
		msk.qcall(CallKeyEvent,ecx,0,keyEventData)
	end
	msk.sdword(keyEventData+8,1)
	msk.sdword(keyEventData+0xc,key)
	msk.qcall(CallKeyEvent,ecx,0,keyEventData)
	if externKey then
		msk.sdword(keyEventData+8,1)
		msk.sdword(keyEventData+0xc,externKey)
		msk.qcall(CallKeyEvent,ecx,0,keyEventData)
	end
end

function KeyDwon(key)
	local ecx = msk.dword(InputProcessorBase)
	msk.sdword(keyEventData+8,0)
	msk.sdword(keyEventData+0xc,key)
	msk.qcall(CallKeyEvent,ecx,0,keyEventData)
end

function KeyUp(key)
	local ecx = msk.dword(InputProcessorBase)
	msk.sdword(keyEventData+8,1)
	msk.sdword(keyEventData+0xc,key)
	msk.qcall(CallKeyEvent,ecx,0,keyEventData)
end

function GetIdentityId()
	for i=1,10 do
		if qs.ActorHasIdentity(i) then
			return i
		end
	end
	return 0
end

function GetIdentityName()
	local iid = GetIdentityId()
	if iid == 0 then return "错误的身份" end
	return QSConfig.ItemTips.QSJHCarerArt2Desc(iid)
end

local __skillData
local function __SetSkillTips(data)
	__skillData = data
end
local __GCDParam1 = msk.new(0x14)
local __GCDParam2 = msk.new(0x18)
local __GCDParam3 = msk.new(0xc)
local __GCDParam4 = msk.new(0xc)
msk.sdword(__GCDParam1+0x10,__GCDParam2)
msk.sdword(__GCDParam2+0x8,__GCDParam3)
msk.sdword(__GCDParam2+0x8+0xc,__GCDParam4)
local GetCareerDataCall = g_sig.GetCareerDataCall
function GetCareerData(iid,sid)
	local oldFun = QSConfig.CareerTips.SetSkillTips
	QSConfig.CareerTips.SetSkillTips = __SetSkillTips
	msk.sdword(__GCDParam3+0x8,iid)
	msk.sdword(__GCDParam4+0x8,sid)
	msk.qcall(GetCareerDataCall,0x12345678,0,__GCDParam1)
	QSConfig.CareerTips.SetSkillTips = oldFun
	return __skillData
end

local TALENT_INVALID = 0
local TALENT_LOCK = 1
local TALENT_CAN_LEARN = 2
local TALENT_CAN_UPGRADE = 3
local TALENT_TOP_LEVEL = 4
local function ConsumeSatisfied(id,count,type)
    if(type == qs.CONT_SLOT_MONEY) then
        --print("GetCurMoney",qs.GetCurMoney(),count)
        return qs.GetCurMoney() >= count
    elseif (type == qs.CONT_SLOT_ITEM) then
        return qs.HasItem(id,count)        
    elseif (type == qs.CONT_SLOT_TALENT_EXP) then
        return qs.GetIdentityExp() >= count
    elseif (type == qs.CONT_SLOT_EXP) then
        return qs.GetCurExp() >= count
    end
    return false
end
function LearnIdentitySkill(iid,t,lv)
	if type(t) == "table" and t[3] then return false end--已经学过了
	local sid 
	if type(t) == "table" then
		sid,lv= t[1],t[2]
	else
		sid = t
	end
	local data = GetCareerData(iid,sid)
	if not data or data.level >= lv or data.state == TALENT_TOP_LEVEL then 
		LogDebuging("已到顶级 %x:%d:%d",iid,sid,lv)
		if type(t) == "table" then t[3] = true end
		return false
	end
	if data.state ~= TALENT_CAN_UPGRADE and data.state ~= TALENT_CAN_LEARN then
		LogDebuging("不能升级 %x:%d:%d",iid,sid,lv)
		return false
	end
	
	if data.player_level > qs.GetPlayerLevel() then
		LogDebuging("角色等级不够 %x:%d %d:%d",iid,sid,data.player_level , qs.GetPlayerLevel())
		return false
	end

	if data.consume then     
		if(not data.has_learn_the_same) then
			for i=1, #data.consume do
				local consume = data.consume[i]
				if(ConsumeSatisfied(consume.id,consume.count,consume.type) == false) then
					LogDebuging("条件不足 %x:%d %x:%d:%d",iid,sid,consume.id or 0,consume.count or 0,consume.type or 0)
					return false
				end
			end     
		end

	end
	
	LogDebuging("学习 %x:%d",iid,sid)
	__LearnIdentitySkill(iid,sid,data.level+1)
	sleep(1)
	return true
end

function LearnIdentitySkills(t,count)
	count= count or 0
	if count > 100 then return false end
	local bLearn = false
	local learnResult
	local oldlv
	--先全部学一级
	for i=1,#t do
		oldlv = t[i][2]
		t[i][2] = 1
		LearnIdentitySkill(t.iid,t[i])
		t[i][2] = oldlv
	end
	--从最后的学起
	for i=1,#t do
		if LearnIdentitySkill(t.iid,t[i]) then
			bLearn = true
		end
	end
	if bLearn then
		return LearnIdentitySkills(t,count+1)
	end
	for i=1,#t do
		if not t[i][3] then
			return false
		end
	end
	return true
end

function TransferIdentity(identBame)
	if GetIdentityId() ~= 0 then return false end
	local iid = 0
	identBame = identBame or mconfig.identity
	if identBame == "乐伶" then
	    iid = qs.IDENTITY_TYPE_YUE_LING
	elseif identBame == "文士" then
	    iid = qs.IDENTITY_TYPE_WEN_SHI
	elseif identBame == "悬眼" then
	    iid = qs.IDENTITY_TYPE_XUAN_YAN
	elseif identBame == "捕快" then
	    iid = qs.IDENTITY_TYPE_BU_KUAI
	elseif identBame == "市井" then
	    iid = qs.IDENTITY_TYPE_SHI_JING
	elseif identBame == "商贾" then
	    iid = qs.IDENTITY_TYPE_SHANG_GU
	elseif identBame == "猎户" then
	    iid = qs.IDENTITY_TYPE_LIE_HU
	elseif identBame == "镖师" then
	    iid = qs.IDENTITY_TYPE_BIAO_SHI
	elseif identBame == "游侠" then
	    iid = qs.IDENTITY_TYPE_YOU_XIA
	elseif identBame == "杀手" then
	    iid = qs.IDENTITY_TYPE_SHA_SHOU
	else
		LogWarning("未知的江湖身份:%s",tostring(identBame))
		return false
	end
	__TransferIdentity(iid)
	sleep(3)
	if qs.ActorHasIdentity(iid) then
		LearnIdentitySkill(iid,1,1)--制药
		sleep(0.5)
		if iid == qs.IDENTITY_TYPE_LIE_HU then--猎户
			LearnIdentitySkill(iid,3,1)--初级衣袋
			sleep(0.5)
			--LearnIdentitySkill(iid,0x78,1)--打猎
			--sleep(0.5)
			--LearnIdentitySkill(iid,0x79,1)--猎弓
			--sleep(0.5)
		elseif iid == qs.IDENTITY_TYPE_SHA_SHOU then
			LearnIdentitySkill(iid,4,1)--初级戒指
			sleep(0.5)
			--LearnIdentitySkill(iid,0x31,1)--龙鳞刺
			--sleep(0.5)
			--LearnIdentitySkill(iid,0x10,1)--杀决长夜
			--sleep(0.5)
		end
		UIShowWindow("QSUICareerSysPanel",false)
		return true
	else
		return false
	end
end

function GetCurrentEventID()
	local eid = qs.GetCurrentEventID()
	if eid == -1 then
		eid = _G.g_eventId or -1
	end
	return eid
end

function IsInJina()
	local eid = GetCurrentEventID()
	if eid >= 17095 and eid <= 17099 then
		return true
	end
	return false
end

function ReviveNear()
	--就近复活
	local reviveCmd2 = "MSG_REVIVE2"
	local reviveCmd1 = "MSG_REVIVE1"
	local rp = msk.ui.GetPanel("QSUIRevivePanel")
	local rp,rt1,rt2
	local tm = os.time()
	for i=1,60 do
		if msk.npc.GetMainPlayer().qixue > 0 then
			Heal(nil,true)
			return true
		end
		sleep(5)
		rp = rp or msk.ui.GetPanel("QSUIRevivePanel")
		rt1 = rt1 or rp.remainTime1
		rt2 = rt2 or rp.remainTime2
		if rt1 and (rt1.visible == false or rt1.text == "(00:00:00)") then
			UIShowWindow("QSUIAssistInvitePanel",false)
			LogDebuging("尝试原地复活1...")
			msk.ui.UIMsgCall("QSUIRevivePanel",reviveCmd1)
		elseif rt2 and (rt2.visible == false or rt2.text == "(00:00:00)") then
			UIShowWindow("QSUIAssistInvitePanel",false)
			LogDebuging("尝试就近复活2...")
			msk.ui.UIMsgCall("QSUIRevivePanel",reviveCmd2)
		elseif os.time()-tm > 120 then
			UIShowWindow("QSUIAssistInvitePanel",false)
			LogDebuging("尝试就近复活2...")
			msk.ui.UIMsgCall("QSUIRevivePanel",reviveCmd2)
		end
		for i=1,5 do
			if msk.npc.GetMainPlayer().qixue > 0 then
				Heal(nil,true)
				return true
			end
			sleep(2,true)
		end
	end	
	ScriptExit("无法就近复活")
end

local _dangkouEvents = {
	 11021,28011,11147
}

function IsInDangKouEvent()
	local eid = GetCurrentEventID()
	for i=1,#_dangkouEvents do
		if eid == _dangkouEvents[i] then
			return true
		end
	end
	return false
end

function IsNearDangKouTime()
	local tm = os.date("*t",qs.GetServerTime())
	--LogDebuging("时机:%d",tm.min)
	if qs.GetMainplayerLevel() < 41 then
		local sid,slot= GetSlotIdByName("御风神行",g_skillSlotOff)
		if sid == 0 then
			sid = msk.skill.FindIdByName("御风神行")
			if sid ~= 0 then
				DragDownSkill(sid)
			end
			sid,slot= GetSlotIdByName("御风神行",g_skillSlotOff)
		end
		if slot == 0 or  msk.dword(slot+slotOff.cd) ~= 0 then 
			return false
		end
	end
	if tm.min >= 12 and tm.min <= 22 then return true end
	if tm.min >= 32 and tm.min <= 42 then return true end
	if tm.min >= 52 and tm.min <= 2 then return true end
	--LogDebuging("时机不对:%d",tm.min)
	return false
end

function GetCurrentMapId(getsubMap)
	local mainMap = msk.dword(currentMapId)
	if getsubMap then
		local base = msk.dword(mapDataBase)
		return mainMap,msk.dword(base+mapIdOff)
	end
	return mainMap
	--return 
end

function GetCurrentMapName()
	return qs.GetMapRealName(GetCurrentMapId())
end

function UseSkillByPacket(id)
end

local __nextMove = false
local __moveFailedNum = 0
local __limitcount = 10
local __splitMoveCount = 0
local deadx,deady = 0,0
local __lastMovex,__lastMovey = 0,0
local __lastSucMovex,__lastSucMovey = 0,0
local __moveSucCount = 0
function MoveToPosition(tx,ty,ex,timeout,layer,dis,map)
	tx = tonumber(tx)
	ty = tonumber(ty)
	local cmap,csubmap = GetCurrentMapId(true)
	map = tonumber(map) or cmap
	if not tx or not ty or tx == 0 or ty == 0 then
		LogWarning("错误目标地址 %d,%d!!",tx or -1,ty or -1)
		return false
	end
	local x,y
	if not ex then
		x,y = tx*100+math.random(1,50),ty*100+math.random(1,50)
	else
		x,y = tx,ty
	end
	if cmap ~= map and not ChangeMap(cmap,map,nil,x,y) and not FlyToNearListTeleport(map,x,y) then
		LogWarning("切图失败:%d->%d",cmap,map)
		return false
	end
	LogDebuging("坐标点寻路 目的地:%d %d",x,y)
	local pl = GetMainPlayer()
	if pl.qixue == 0 then
		ReviveNear()
		pl = GetMainPlayer()
	end
	dis = dis or mconfig.pathDis
	if layer == nil and pl:Dis(x,y) < dis*100 then
		if __lastSucMovex == tx and __lastSucMovey == ty then
			__moveSucCount = __moveSucCount + 1
		else
			__lastSucMovex,__lastSucMovey = tx,ty
			__moveSucCount = 0
		end
		if __moveSucCount > 5 then
			sleep(3)
		end
		LogDebuging("坐标点寻路 已到达!!")
		return true
	end
	if not __nextMove and GetCurrentMapId() == 10010 and pl:Dis(157100,28400) < 10000 then
		__nextMove = true
		SimplePlayerMove(160000,29400,10)
	else
		__nextMove =false
	end
	if not __nextMove and GetCurrentMapId() == 10001 and tx == 268935 and ty == 316678 then
		__nextMove = true
		if not MoveToPosition(2809,3112) then
			__nextMove = false
			return false
		end
		__nextMove = false
	else
		__nextMove =false
	end
	if __lastMovex  == tx and __lastMovey == ty then
		LogDebuging("---:%d",__moveFailedNum)
		__moveFailedNum = __moveFailedNum + 1
		sleep(4)
	else
		LogDebuging("+++")
		__lastMovex = tx
		__lastMovey = ty 
		__moveFailedNum = 0
		__limitcount = 10
		__splitMoveCount = 0
	end
	timeout = timeout or mconfig.pathTimeOut
	local eid = GetCurrentEventID()
	if __moveFailedNum > __limitcount then
		__limitcount = __limitcount + 10
		if eid == -1 or IsSkipEvent(eid) then
			if FlyToNearListTeleport(map,x,y) == true then
				sleep(4)
			elseif __splitMoveCount < 3 or (deadx == __lastMovex and deady == __lastMovey) then--上次也是这里死的 不能再死了
				--尝试向前移动
				LogWarning("尝试分段移动。。。")
				local tdis = pl:Dis(x,y)
				local subDis = 7000
				if tdis > 7000 then
					if not qs.IsPlayerInBattle() and not qs.CheckHasBuff(0x2b09) and qs.CheckSpellHasLearn(0x10e821)  then--not g_cacheData.fighting and
						UseSkill(0x10e821)
						sleep(2)
						timeout = timeout - 2
					end
					local scal = 7000/tdis
					local lx,ly = pl.x,pl.y
					local xdif,ydif = x-lx,y-ly
					local cscal = scal
					repeat
						SimpleMoveToPositon(lx+cscal*xdif,ly+cscal*ydif,30,pl)
						sleep(0.1)
						cscal = cscal + scal
					until cscal > 1
					__splitMoveCount = __splitMoveCount + 1
					sleep(1)
				else
					SimplePlayerMove(x,y,30,pl)
				end
			else
				g_cacheData.status = "卡住!!!"
				deadx,deady = __lastMovex,__lastMovey
				LogWarning("寻路失败过多")
				UseSkillByName("自绝经脉")
				sleep(4)
				Heal()
			end
			return false
		end
	end
	--map ~= cmap and 
	if map ~= csubmap and eid ~= -1 and not IsSkipEvent(eid) and g_cacheData.curScriptFun ~= msk.quest.RunAllQuests and not g_cacheData.EventRuning then
		g_cacheData.EventRuning = true
		LogDebuging("寻路过程碰到见闻 %d:%s event:%d %s",csubmap,qs.GetMapRealName(csubmap) ,eid,qs.GetEventName(eid))
		for j = 1,100 do
			if eid == -1 or IsSkipEvent(eid) then
				break
			end
			if FlyToNearListTeleport(map,x,y,true) == true then
				sleep(4)
				break
			else
				msk.quest.RunCurDesc()
			end
			eid = GetCurrentEventID()
		end
		g_cacheData.EventRuning = false
	end
	if pl:Dis(x,y) >100000 and FlyToNearListTeleport(map,x,y) == true then
		sleep(4)
	end
	local lx,ly = pl.x,pl.y
	--printf("QSUINpcDialogPanel")
	local misstm = 0
	repeat
		pl = GetMainPlayer()
		if pl.x == lx and  pl.y == ly then
			LogDebuging("坐标点寻路 启动寻路:%d,%d",pl.x,pl.y)
			if not __MoveToPosition(x,y,layer) then
				LogDebuging("坐标点寻路失败 无法到达的目的地")
				return false
			end
			--__moveFailedNum = __moveFailedNum-1
			LogDebuging("坐标点寻路 启动结束")
			misstm = misstm + 1
			if misstm > 6 then
				LogWarning("坐标点寻路 超时:%d %d",x,y)
				return false
			end
			--[[
			--卡住不动了 向四周走走
			if misstm == 2 then--向左走一下
				pl:FaceTo(lx-100,ly)
			elseif misstm == 3 then--向右走一下
				pl:FaceTo(lx+100,ly)
			elseif misstm == 4 then--向上走一下
				pl:FaceTo(lx,ly+100)
			elseif misstm == 5 then--向下走一下
				pl:FaceTo(lx,ly-100)
			end
			if misstm >= 2 then
				SetMainPlayerMove(true)
			end
			--]]
			sleep(1)
			timeout = timeout - 1
			if pl:Dis(x,y) > 3000 and not qs.IsPlayerInBattle() and not qs.CheckHasBuff(0x2b09) and qs.CheckSpellHasLearn(0x10e821)  then--not g_cacheData.fighting and
				LogDebuging("坐标点寻路 尝试上马...")
				UseSkill(0x10e821)
				sleep(2)
				timeout = timeout - 2
			end
			-- if misstm >= 2 then
				-- SetMainPlayerMove(false)
			-- end
		else
			lx,ly = pl.x,pl.y
			misstm = 0
		end
		sleep(1)
		timeout = timeout - 1
		if timeout < 0 then
			LogWarning("坐标点寻路 超时:%d %d",x,y)
			return false
		end
	until pl:Dis(x,y) < mconfig.pathDis*100
	LogDebuging("坐标点寻路 已到达!!")
	g_cacheData.status = nil
	return true
end

function Heal(tmout,h)
	if not qs.CheckSpellHasLearn(0x201ac1) or __IsPlayerInBattle() or  qs.CheckHasBuff(0x3f4) then return end
	if h == nil then h = false end
	local pl = msk.npc.GetMainPlayer()
	if pl.qixue == pl.max_qixue then return end
	if pl.qixue == 0 then
		ReviveNear()
	end
	pl = msk.npc.GetMainPlayer()
	printf("恢复气血。。。")
	tmout = tmout or 60
	local ustm = 3
	while tmout > 0 and ustm > 0 do
		if pl.qixue == pl.max_qixue or __IsPlayerInBattle() or qs.CheckHasBuff(0x3f4) then 
			return 
		end
		if qs.CheckHasBuff(0x2b09) then
			ustm = ustm + 1
			UseSkill(0x10e822)
			sleep(1,h)
			tmout = tmout - 1
		end
		if not qs.CheckHasBuff(0x44e) then
			UseSkill(0x201ac1)
		end
		tmout = tmout - 3
		sleep(3,h)
	end
end

function GetNearListNpc(tp,tname,ct)
	ct =ct or 1
	local base = msk.gdata.GetDataBase("Data.ClientTable.MapTable.MapUiInstNewTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,id2,map,x,y,name,funtext,funtype
	local cmap = msk.api.GetCurrentMapId() 
	local lastDis,lastx,lasty,lastName,lastType,lastAreaName
	local cdis,area_name
	local pl = msk.npc.GetMainPlayer()
	for addr = hd,ed-1,0x54 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+0x8)
		if id ~= 0 then
			map = msk.dword(addr+4)
			if cmap == map then
				funtype = msk.dword(addr+0x3c)
				funtype = msk.str(funtype)
				name = msk.dword(addr+0x1c)
				name = msk.str(name)
				if tp == nil then
					x,y = msk.api.GetMapInsData(map,id2)
					LogDebuging("%s-%s %d,%d",funtype,name,x,y)
				elseif funtype == tp and (tname == nil or tname == name) then
					x,y = msk.api.GetMapInsData(map,id2)
					if x then cdis = pl:Dis(x,y) end
					if x and y and (lastDis == nil or cdis < lastDis) then
						area_name = qs.GetFogName(map,x,y)
						--战斗区域就不要去了
						if area_name and area_name:match("^.+分舵$") == nil and area_name:match("^.+总舵$") == nil then
							lastDis= cdis
							lastAreaName = area_name
							lastx,lasty,lastName,lastType = x,y,name,funtype
						end
					else
						--LogDebuging("放弃:%s-%s %d,%d",funtype,name,x or 0,y or 0)
					end
				else
					--LogDebuging("跳过:%s-%s",funtype,name)
				end
			else
				--LogDebuging("%s:%x %s:%s:%s",qs.GetMapRealName(map),id2,funtype,funtext,name)
			end
		end
	end
	LogDebuging("找到 ：%s",lastAreaName or "nil")
	return lastx,lasty,lastName,lastType
end

local __shoptype = {
	"DrugShop",
	"ArmorShop",
	"WeaponShop",
	"MiscShop",
	"HorseShop",
	"Accessories",
}
function GetNearListShop(ct)
	ct = ct or 1
	local base = msk.gdata.GetDataBase("Data.ClientTable.MapTable.MapUiInstNewTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,id2,map,x,y,name,funtext,funtype
	local cmap = msk.api.GetCurrentMapId() 
	local lastDis,lastx,lasty,lastName,lastType
	local cdis,area_name
	local pl = msk.npc.GetMainPlayer()
	local bfind = false
	for addr = hd,ed-1,0x54 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+0x8)
		if id ~= 0 then
			map = msk.dword(addr+4)
			if cmap == map then
				bfind = true
				funtype = msk.dword(addr+0x3c)
				funtype = msk.str(funtype)
				name = msk.dword(addr+0x1c)
				name = msk.str(name)
				for j= 1,#__shoptype do
					if funtype == __shoptype[j] then
						x,y = msk.api.GetMapInsData(map,id2)
						if x then cdis = pl:Dis(x,y) end
						if x and y and (lastDis == nil or cdis < lastDis) then
							area_name = qs.GetFogName(map,x,y)
							--战斗区域就不要去了
							if area_name and area_name:match("^.+分舵$") == nil and area_name:match("^.+总舵$") == nil then
								lastDis= cdis
								lastx,lasty,lastName,lastType = x,y,name,funtype
							end
						end
						break
					end
				end
			end
		end
	end
	if not bfind and ct <= 1 then
		UIShowWindow("QSUIAreaMapPanel",true)
		UIShowWindow("QSUIAreaMapPanel",false)
		return GetNearListShop(ct+1)
	end
	return lastx,lasty,lastName,lastType
end

function BuyShopIteam(itemPos,buyCount,shopType,npcName,realNpcName,itemCost,useType,costItem)
	itemPos = itemPos or 1
	realNpcName = realNpcName or npcName
	local curMoneyNum
	if useType == "item" then
		local _,t = msk.item.GetItemByName(costItem)
		curMoneyNum = t and t.num or 0
	elseif useType == "bindMoney" or useType == "money" then
		local money,bindMoney = msk.item.GetMoney()
		curMoneyNum = useType == "money" and money or bindMoney
	end
	if curMoneyNum < itemCost then 
		LogDebuging("物品不足:%s %s",shopType or "??",npcName or "??")
		return false 
	end
	if buyCount == nil then
		buyCount = math.floor(curMoneyNum/itemCost)
		LogDebuging("初始化数量:%d",buyCount)
	else
		local maxCuunt = math.floor(curMoneyNum/itemCost)
		buyCount = buyCount <= maxCuunt and buyCount or maxCuunt
		LogDebuging("计算后数量:%d",buyCount)
	end
	if buyCount <= 0 then return false end
	local x,y,nname = msk.api.GetNearListNpc(shopType,npcName)
	realNpcName = realNpcName or nname
	if x == nil then
		msk.api.ChangeMap(nil,10012)
		x,y,nname = msk.api.GetNearListNpc(shopType,npcName)
	end
	if x and msk.mcall(21,msk.api.MoveToPosition,x,y,true) then
		local npc = msk.npc.GetByName(realNpcName)
		if npc == nil then
			npc = msk.npc.GetNearTalkNpc()
		end
		if npc == nil then
			LogWarning("无法获取商店数据:%s %s",shopType or "??",npcName or "??")
			return false
		end
		WaitMoneyProtect()
		npc:Open()
		sleep(1)
		local ui,cuiName = msk.ui.GetPaneByName("QSUINpcShopPanel",true)
		if ui == nil then
			LogWarning("商店未能打开:%s %s",shopType or "??",npcName or "??")
			return false
		end
		local dataAddr = ui+0xd0-5
		--len = 143
		local count = msk.dword(dataAddr+13+22)
		len = 13+(count+1)*26
		local data = msk.data(dataAddr,len)
		local pos,_,_,tid1,tid2 = data:unpack("LbLL")
		local shopId
		pos,shopId,_,_,_,_,_,count = data:unpack("LLLHLLL",pos)
		if itemPos >= count then
			LogWarning("物品位置错误:%s %s",shopType or "??",npcName or "??")
			return false
		end
		pos = 13+itemPos*26
		local itemData= msk.data(dataAddr+pos,26)
		--local tid = msk.dword(dataAddr+pos)
		local pos,tid,_,_,_,money,_,_ = itemData:unpack("LLLHLLL")
		if money ~= itemCost then
			LogDebuging("商店消耗不符 重新计算:%d %d",itemCost,money)
			local maxCunt = math.floor(curMoneyNum/money)
			buyCount = buyCount <= maxCunt and buyCount or maxCunt
		end
		if buyCount > 0 then
			LogDebuging("买 %x %d",tid,buyCount)
			__BuyItem(npc.id1,npc.id2,shopId,tid,buyCount)
			sleep(1)
			UIShowWindow("QSUINpcShopPanel",false)
		end
		return true
	else
		LogWarning("寻路到商店失败:%s %s",shopType or "??",npcName or "??")
	end
	return false
end

function BuyDrag(minCount,buyCount,lv)
	lv = lv or qs.GetMainplayerLevel()
	minCount = minCount or 10
	buyCount = buyCount or 50
	if lv < 41 then
		LogDebuging("41以下不需要买药")
		return false
	end
	local itemPos,itemCost
	local itemName
	if lv >= 41 and lv < 61 then
		itemPos= 2
		itemCost = 2500
		itemName = "三和散"
	elseif lv >= 61 and lv < 80 then
		itemPos= 3
		itemCost=3750
		itemName = "芍药散"
	elseif lv >= 80 then
		itemPos=4
		itemCost=5000
		itemName = "通圣散"
	elseif lv < 41 then
		itemPos=1
		itemCost=1250
		itemName = "四逆散"
	end
	local _,t = msk.item.GetItemByName(itemName)
	local curCount = t and t.num or 0
	if curCount >= minCount then return false end
	buyCount = buyCount - curCount
	LogDebuging("买药:%d %d %d",buyCount,itemPos,itemCost)
	return BuyShopIteam(itemPos,buyCount,"DrugShop",nil,nil,itemCost,"bindMoney")
end

function RepairAtNearlistNpc()
	local x,y,tname,ttype = GetNearListShop()--WeaponShop
	if not x then 
		LogWarning("找不到最近铁匠")
		return false 
	end
	LogDebuging("找到:%s",ttype)
	if not msk.mcall(3,MoveToPosition,x,y,true) then return false end
	local npc = msk.npc.GetByName(tname) or msk.npc.GetByName()
	if npc then
		WaitMoneyProtect()
		npc:Open()
		sleep(1)
		local ui,cuiName = msk.ui.GetPaneByName("QSUINpcShopPanel",true)
		if ui == nil then
			LogWarning("商店未能打开")
			return false
		end
		local dataAddr = ui+0xd0-5
		local count = msk.dword(dataAddr+13+22)
		len = 13+(count+1)*26
		local data = msk.data(dataAddr,len)
		local pos,_,_,tid1,tid2 = data:unpack("LbLL")
		local shopId,money,itemId
		pos,shopId,_,_,_,_,_,count = data:unpack("LLLHLLL",pos)
		RepairAll(npc.id1,npc.id2,shopId)
		sleep(1)
		UIShowWindow("QSUINpcShopPanel",false)
	end
	return false
end

function GetFlyCd()
	local sid,slot= GetSlotIdByName("御风神行",g_skillSlotOff)
	if sid == 0 then
		sid = msk.skill.FindIdByName("御风神行")
		if sid ~= 0 then
			DragDownSkill(sid)
		end
		sid,slot= GetSlotIdByName("御风神行",g_skillSlotOff)
	end
	if slot ~= 0 then
		local edcd = msk.float(slot+slotOff.cd)
		local curcd = msk.float(slot+slotOff.cd+4)
		if edcd == 0 then return 0 end
		return edcd - curcd
	end
	return 1080
end

local __lastFlyPos,__lastFlyX,__lastFlyY
local __lastFlyCount = 0
function FlyMap(pos,x,y)
	if x and y then
		if not GetMainPlayer():IsNear(x*100,y*100) then
			return true
		end
	end
	if GetFlyCd() > 0 then return false end
	if __lastFlyPos == pos then
		__lastFlyCount = __lastFlyCount + 1
	else
		__lastFlyCount = 0
	end
	if __lastFlyCount > 5 then--这里不能飞
		return false
	end
	__lastFlyPos,__lastFlyX,__lastFlyY = pos,x,y
	if type(pos) == "string" then
		pos = g_flyMapPos[pos]
	end
	__FlyMap(pos)
	return true
end

function HideAllMsgBox(cmd)
	for i=1,10 do
		if UIIsVisible("QSUIDialogPanel") == false then
			break
		end
		if cmd then
			msk.ui.UIMsgCall("QSUIDialogPanel",cmd)
			sleep(0.1)
		end
		UIShowWindow("QSUIDialogPanel",false)
		sleep(0.5)
	end
end

function DeleteAllMail()
	HideAllMsgBox()
	msk.ui.UIMsgCall("QSUIMailListPanel","MSG_CHECK_SELECT_ALL",1)
	msk.ui.UIMsgCall("QSUIMailListPanel","MSG_MAIL_DELETE",0)
	msk.ui.UIMsgCall("QSUIDialogPanel","CMD_OK")
	HideAllMsgBox()
	sleep(2)
end

function ProcessMail(deleteAllMail)
	local fullMail = {}
	local deleteMailt = {}
	local base = msk.dword(g_sig.mailDataBase)
	local hd = msk.dword(base+0x30)
	local ed = msk.dword(base+0x34)
	local data,id1,id2,hasItem,name
	for addr=hd,ed-1,4 do
		data = msk.dword(addr)
		id1,id2 = msk.dword(data),msk.dword(data+4)
		hasItem = msk.byte(data+0x5c) == 1
		name = msk.str(data+0x18)
		LogDebuging("%s %s",name,hasItem and "有" or "无")
		if hasItem then
			fullMail[#fullMail+1] = {id1,id2}
		else
			deleteMailt[#deleteMailt + 1] = {id1,id2}
		end
	end
	--[[
	for i=1,#deleteMailt do
		DeleteMail(deleteMailt[i][1],deleteMailt[i][2])
		sleep(1)
	end
	--]]
	if #fullMail <= 0 then return true end
	msk.item.CheckBag()
	local x,y = msk.api.GetNearListNpc("Mailpoint")
	if x == nil then
		msk.api.ChangeMap(nil,10012)
		x,y = msk.api.GetNearListNpc("Mailpoint")
	end
	if not x or msk.mcall(21,msk.api.MoveToPosition,x,y,true) == false then
		LogDebuging("跳过了...%d",x or 0)
		return false
	end
	local npc = msk.npc.GetByName("大宋邮差")
	if npc then
		--npc:Open()
		UIShowWindow("QSUIMailListPanel",true)
		for i=1,#fullMail do
			ExtractMail(npc.id1,npc.id2,fullMail[i][1],fullMail[i][2])
			sleep(1)
		end
		sleep(2)
		--[[
		for i=1,#fullMail do
			DeleteMail(fullMail[i][1],fullMail[i][2])
			sleep(1)
		end
		--]]
	end
	if deleteAllMail then
		DeleteAllMail()
	end
	UIShowWindow("QSUIMailListPanel",false)
	UIShowWindow("QSUIMailReadPanel",false)
	return true
end

function EnterToWorld()
	if GetCurrentMapId() ~= 10050 then
		LogWarning("无法进入游戏")
		return false
	end
	msk.ui.UIMsgCall("QSUISelectCharacterPanel","BTN_CLICK_ENTERGAME")
	if UIIsVisible("QSUIMoviePanel") then
		msk.ui.UIMsgCall("QSUIMoviePanel","CMD_L_MOVIE_FINISHED")
	end
	return true
end

function GetDazuoPoint()
	local data = msk.dword(g_sig.gamePlayerDataBase)
	data = data + 0x50
	return msk.dword(data),msk.dword(data+4)
end

function NpcTalkCompleteByName(npcname,id,idx1,idx2,idx3,idx4,idx5,idx6)
	if id == -1 then
		LogDebuging("任意对话")
		return NpcTalk(npcname)
	end
	local npc = msk.npc.GetNpcByName(npcname)
	if npc and MoveToPosition(npc.x,npc.y,true) then
		qs.TalkToNpc(npc.instanceId)
		sleep(1)
		SubNpcWenTalk(npc.id1,npc.id2,id,idx1,idx2,idx3,idx4,idx5,idx6)
		sleep(1)
		UIShowWindow("QSUINpcDialogPanel",false)
		return true
	end
	LogWarning("cant find a npc named:%s",npcname or "nil")
	return false
end

function NpcTalkCompleteByInstanceId(iid,id,idx1,idx2,idx3,idx4,idx5,idx6)
	iid = tonumber(iid)
	local npc = msk.npc.GetNpcByInstanceId(iid)
	if npc and MoveToPosition(npc.x,npc.y,true) then
		qs.TalkToNpc(npc.instanceId)
		sleep(1)
		SubNpcWenTalk(npc.id1,npc.id2,id,idx1,idx2,idx3,idx4,idx5,idx6)
		sleep(1)
		UIShowWindow("QSUINpcDialogPanel",false)
		return true
	end
	LogWarning("cant find a npc iid:%d",tonumber(iid) or -1)
	return false
end

function NpcTalkComplete(iid,npcname,id,idx1,idx2,idx3,idx4,idx5,idx6)
	iid = tonumber(iid)
	local npc = msk.npc.GetNpcByInstanceId(iid) or msk.npc.GetNpcByName(npcname)
	if npc and MoveToPosition(npc.x,npc.y,true) then
		qs.TalkToNpc(npc.instanceId)
		sleep(1)
		SubNpcWenTalk(npc.id1,npc.id2,id,idx1,idx2,idx3,idx4,idx5,idx6)
		sleep(1)
		UIShowWindow("QSUINpcDialogPanel",false)
		return true
	end
	LogWarning("cant find a npc iid:%d name:%s",tonumber(iid) or -1,npcname or "??")
	return false
end


local function GetNpcFunNodeById(tid,bgNode,edNode)
	if bgNode == 0 or bgNode == edNode then return end
	local id = msk.dword(bgNode+0x10)
	if tid > id then
		return GetNpcFunNodeById(tid,msk.dword(bgNode),edNode)
	elseif tid < id then
		return GetNpcFunNodeById(tid,msk.dword(bgNode+4),edNode)
	end
	return bgNode
end
local __travlsItemIds
local function GetTreeCfgNodeByText(txt,bgNode,edNode)
	if bgNode == 0 or bgNode == edNode then return end
	local cdata = bgNode+0x14
	local tcname = msk.dword(cdata+0xc)
	local tcname = msk.str(tcname)
	if tcname == txt then
		return bgNode
	end
	local fdNode,childNode
	local cbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTreeCfgTable_Retail")
	for i=0,9 do
		tcid = msk.dword(cdata+0x14+i*4)
		if tcid ~= 0 and __travlsItemIds[tcid] == nil then
			__travlsItemIds[tcid] = true
			childNode = GetNpcFunNodeById(tcid,msk.dword(cbase+0x4c),cbase+0x44)
			fdNode = GetTreeCfgNodeByText(txt,childNode,edNode)
			if fdNode then return childNode end
		end
	end
end

--取系列中最后一个
local function GetFirstEndIdById(tid)
	local cbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTreeCfgTable_Retail")
	local node = GetNpcFunNodeById(tid,msk.dword(cbase+0x4c),cbase+0x44)
	if not node then return end
	local cdata = node+0x14
	local tcid = msk.dword(cdata+0x14)
	if tcid ~= 0 and __travlsItemIds[tcid] == nil then
		__travlsItemIds[tcid] = true
		return GetFirstEndIdById(tcid)
	end
	return tid
end

--取指定名字的系列中 最后一个
local function GetFirstEndIdByText(txt,instanceId)
	local base = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncEntryTable_Retail")
	local node = GetNpcFunNodeById(instanceId,msk.dword(base+0x4c),base+0x44)
	if not node then return end
	
	local cbase = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncTreeCfgTable_Retail")
	local edNode = cbase+0x44
	local bgNode = msk.dword(cbase+0x4c)
	local fid,childNode,fnode
	local data = node+0x14
	for i=0,9 do
		fid = msk.dword(data+0x10+i*4)
		if fid ~= 0 then
			childNode = GetNpcFunNodeById(fid,bgNode,edNode)
			fnode = GetTreeCfgNodeByText(txt,childNode,edNode)
			if fnode then
				break
			end
		end
	end
	if fnode then
		local fid = msk.dword(fnode+0x10)
		return GetFirstEndIdById(fid)
	end
end
function GetNpcAnyTalkId(instanceId)
	local base = msk.gdata.GetDataBase("Data.Table.NpcFuncTable.NpcFuncEntryTable_Retail")
	local node = GetNpcFunNodeById(instanceId,msk.dword(base+0x4c),base+0x44)
	if not node then return end
	local fid
	local data = node+0x14
	for i=0,9 do
		fid = msk.dword(data+0x10+i*4)
		if fid ~= 0 then
			return fid
		end
	end
end
function NpcTalkById(tar,id)
	local npc = type(tar) == "number" and msk.npc.GetByInstanceId(tar) or msk.npc.GetByName(tar)
	id = id or (npc and GetNpcAnyTalkId(npc.instanceId))
	if not npc or not id or  not msk.mcall(21,msk.api.MoveToPosition,npc.x,npc.y,true) then
		LogWarning("指定NPC不存在 或者无法移动:%s",tostring(tar))
		return false
	end
	local tid = GetFirstEndIdById(id)
	if not tid then
		LogWarning("指定ID无终结点:%x",id)
		return false
	end
	LogDebuging("npcTalk:%x",tid)
	SubNpcWenTalk(npc.id1,npc.id2,tid,0,0,0,0,0,0)
end
function NpcTalkByText(tar,text)
	local npc = type(tar) == "number" and msk.npc.GetByInstanceId(tar) or msk.npc.GetByName(tar)
	if not npc or not msk.mcall(21,msk.api.MoveToPosition,npc.x,npc.y,true) then
		LogWarning("指定NPC不存在 或者无法移动:%s",tostring(tar))
		return false
	end
	local tid = GetFirstEndIdByText(text,npc.instanceId)
	if not tid then
		LogWarning("指定文本无终结点:%s",text)
		return false
	end
	LogDebuging("NpcTalkByText:%x",tid)
	SubNpcWenTalk(npc.id1,npc.id2,tid,0,0,0,0,0,0)
end
function NpcTalk(tar,item)
	__travlsItemIds = {}
	return type(item) == "string" and NpcTalkByText(tar,item) or NpcTalkById(tar,item)
end

local function __MapNameToId(mname)
	
end

function IsNormalMap(map)
	if type(map) == "string" then
		return true
	end
	if map >= normalMapBegin and map <= normalMapEnd then
		return true
	end
	return false
end

function ChangeMap(cmap,tmap,tid,x,y)
	cmap = cmap or GetCurrentMapId()
	if IsNormalMap(cmap) == false or IsNormalMap(tmap) == false then return true end
	if cmap == tmap then return true end
	local cmapName = type(cmap) == "string" and cmap or qs.GetMapRealName(cmap)
	local tmapName = type(tmap) == "string" and tmap or qs.GetMapRealName(tmap)
	if cmapName == tmapName then return true end
	local lastI,lastJ,lastV,lastDis,cdis
	local tx,ty,cx,cy
	local pl = msk.npc.GetMainPlayer()
	--LogDebuging("ChangeMap:%d-%d",x or 0,y or 0)
	for k,v in pairs(g_mapTansData) do
		if k == cmapName then
			for i=1,#v do
				for j=1,#v[i].tar do
					if v[i].tar[j][1] == tmapName and v[i].tar[j][2] ~= nil then
						cx,cy = v[i].x*100,v[i].y*100
						tx,ty = v[i].tar[j][3]*100,v[i].tar[j][4]*100
						if x and y then
							cdis = _mabs(pl.x-cx)+_mabs(pl.y-cy)+_mabs(tx-x)+_mabs(ty-y)
						else
							cdis = _mabs(pl.x-cx)+_mabs(pl.y-cy)
						end
						--LogDebuging("指定NPC:%s %d,%d-%d",v[i].name,v[i].x,v[i].y,cdis)
						if lastDis == nil or lastDis > cdis then
							lastDis = cdis
							lastI = i
							lastJ = j
							lastV = v
						end
					end
				end
			end
		end
	end
	if lastI then
		if x and lastDis >100000 and FlyToNearListTeleport(tmap,x,y) == true then
			sleep(4)
			return GetCurrentMapId() == tmap
		end
		local i,j,v = lastI,lastJ,lastV
		cx,cy = v[i].x,v[i].y
		LogDebuging("切换:%s %d,%d",v[i].name,cx,cy)
		if MoveToPosition(cx,cy,nil,nil,v[i].layer or 0) then
			NpcTalkCompleteByName(v[i].name,v[i].tar[j][2],0,0,0,0,0,0)
			msk.sleep(5)
		end
		return GetCurrentMapId() == tmap
	end
	if g_changeMapFun[cmap] and g_changeMapFun[cmap][tmap] then
		g_changeMapFun[cmap][tmap](cmap,tmap,tid)
		return GetCurrentMapId() == tmap
	end
	if GetCurrentMapId() == tmap then
		return true
	end
	--556,1362
	return cmap ~= 10012 and tmap ~= 10012 and ChangeMap(cmap,10012) and ChangeMap(10012,tmap,tid,x,y)
end

local __lastRideMap,__lastRidePos
function MoveTo(map,tid,timeout,InitBeforeMove,CheckArrive,talkid,layer,initMapIns)
	map = tonumber(map)
	tid = tonumber(tid)
	if g_replaceMapId[map] and g_replaceMapId[map][tid] then
		LogDebuging("人工调整坐标")
		return g_replaceMapId[map][tid]()
	else
		--prints(g_replaceMapId[map],g_replaceMapId[map][tid] )
	end
	--走坐标
	local tx,ty = GetMapInsData(map,tid,initMapIns)
	if tx then
		return MoveToPosition(tx,ty,true,nil,layer,nil,map)
	end
	
	if not ChangeMap(GetCurrentMapId(),map,tid) then
		LogWarning("切图失败:%d->%d",GetCurrentMapId(),map)
		return false
	end
	LogDebuging("任务点寻路到:%d %d",map,tid)
	local pl = GetMainPlayer()
	if not nextMove and GetCurrentMapId() == 10010 and pl:IsNear(157100,28400,5000) then
		nextMove = true
		msk.api.MoveToPosition(1600,294)
	else
		nextMove = false
	end

	local lx,ly = pl.x,pl.y
	timeout = timeout or mconfig.pathTimeOut
	local misstm = 0
	if InitBeforeMove then InitBeforeMove() end
	repeat
		pl = GetMainPlayer()
		if pl.x == lx and  pl.y == ly then
			misstm = misstm + 1
			if misstm > 5 then
				LogWarning("任务点寻路失败 无法移动:%d %d",map,tid)
				return false
			end
			LogDebuging("任务点寻路 启动:%d,%d",pl.x,pl.y)
			qs.MoveToNpc(map,tid)
			sleep(1)
			if talkid and misstm > 1 then
				--printf("try talk:%d",talkid)
				qs.TalkToNpc(talkid)
				sleep(1)
				timeout = timeout - 1
			else
				--prints("not talk",talkid,misstm)
			end
			if (__lastRideMap ~= map or __lastRidePos ~= tid) and not qs.IsPlayerInBattle() and not qs.CheckHasBuff(0x2b09) and qs.CheckSpellHasLearn(0x10e821)  then
				__lastRideMap = map--这里会导致崩溃 不知道为什么
				__lastRidePos = tid
				--KeyEvent(0x52,"ctrl")--模拟上马
				--UseSkill(0x10e821)
				sleep(2)
				timeout = timeout - 2
			end
			timeout = timeout - 1
			-- if misstm >= 2 then
				-- SetMainPlayerMove(false)
			-- end
		else
			--print("runing...")
			lx,ly = pl.x,pl.y
			misstm = 0
		end
		sleep(1)
		timeout = timeout - 1
		if timeout < 0 then
			LogWarning("任务点寻路 超时:%d %d",map,tid)
			return false
		end
	until CheckArrive(map,tid)
	--print("move ok")
	LogDebuging("任务点寻路 成功")
	return true
end

local function __DefaultIsMoveToNpcOk()
	return UIIsVisible("QSUINpcDialogPanel")
end
local function __DefaultMoveToNpcInit()
	UIShowWindow("QSUINpcDialogPanel",false)
end
function MoveToNpc(map,tid,talkid)
	local ret = MoveTo(map,tid,nil,__DefaultMoveToNpcInit,__DefaultIsMoveToNpcOk,talkid)
	UIShowWindow("QSUINpcDialogPanel",false)
	return ret
end


function MoveToArea(map,id,questid)
	if not questid then
		local cur_group_id = qs.GetCurrentQuestTrackGroupId()
		local cur_state = qs.GetQuestGroupInfo(cur_group_id)
		questid = cur_state.current_quest_id
	end
	local CheckArray = function(...)
		return qs.IsQuestAccepted(questid)
	end
	return MoveTo(map,id,nil,nil,CheckArray)
end


function MoveToEntity(map,id,ename)
	local CheckArrive = function(...)
		return not msk.bool(isHaveAction)
	end
	local EntityInit = function(...)
		msk.sbyte(isHaveAction,0)
	end
	local entityObj
	for i=1,3 do
		if MoveTo(map,id,nil,EntityInit,CheckArrive) then
			if msk.npc.GetEntityByName(ename,false,true) then
				return true 
			end
		end
	end
	return false
end

function MoveById(map,id,layer,initMapIns)
	local CheckArrive = function(...)
		return not msk.bool(isHaveAction)
	end
	local EntityInit = function(...)
		msk.sbyte(isHaveAction,0)
	end
	if layer then
		LogDebuging("跨层寻路:%d",layer)
	end
	return MoveTo(map,id,nil,EntityInit,CheckArrive,nil,layer,initMapIns)
end

function MoveToMonster(map,id,ename)
	local CheckArrive = function(...)
		return not msk.bool(isHaveAction)
	end
	local EntityInit = function(...)
		msk.sbyte(isHaveAction,0)
	end
	local entityObj
	for i=1,3 do
		if MoveTo(map,id,nil,EntityInit,CheckArrive) then
			if msk.npc.GetNpcByName(ename,false,true) then
				return true 
			end
		end
	end
	return false
end

local skillSlot0
local skillPane
function GetSlotById(id,orzoff,maxnum,force)
	if id == 0 and nil == force then return 0 end
	orzoff = orzoff or 0
	local off = orzoff
	local base = msk.dword(QSSlotManagerBase)
	local hd = msk.dword(base+0x2c)
	local ed = msk.dword(base+0x30)
	local cid
	maxnum = maxnum or 9999
	for addr = hd+off*4,ed-1,4 do
		slot=msk.dword(addr)
		if slot ~= 0 and msk.dword(slot+0x3c) == id then
			return slot,off-orzoff
		end
		off = off + 1
		if maxnum < 0 then return 0 end
		maxnum = maxnum -1
	end
	return 0
end
--[2344] msk_td->send-3ba:7:69 BD 59 1B 010701
--[2344] msk_td->recv-3bb:34:BB0300000000000000000000E9B1BCE6978BE7979500000000000000000000000000

function GetSlotByName(name,orzoff)
	orzoff = orzoff or 0
	local off = orzoff
	local base = msk.dword(QSSlotManagerBase)
	local hd = msk.dword(base+0x2c)
	local ed = msk.dword(base+0x30)
	local cid
	local name
	for addr = hd+off*4,ed-1,4 do
		slot=msk.dword(addr)
		cid = msk.dword(slot+0x3c)
		if slot ~= 0 and cid ~= 0 then
			
		end
		off = off + 1
	end
	return 0
end

function GetSlotByPos(pos,orzoff)
	orzoff = orzoff or 0
	local base = msk.dword(QSSlotManagerBase)
	local hd = msk.dword(base+0x2c)
	return msk.dword(hd+(pos+orzoff)*4)
end

function GetSlotIdByName(name,orzoff)
	orzoff = orzoff or 0
	local off = orzoff
	local base = msk.dword(QSSlotManagerBase)
	local hd = msk.dword(base+0x2c)
	local ed = msk.dword(base+0x30)
	local cid
	local cname
	for addr = hd+off*4,ed-1,4 do
		slot=msk.dword(addr)
		cid = slot == 0 and 0 or msk.dword(slot+0x3c)
		if cid ~= 0 and qs.GetSpellName(cid) == name then
			return cid,slot,off
		end
		off = off + 1
	end
	return 0,0,0
end

function UseSkill1(id,slot,off)
	if id == 0 then return end
	if not slot then
		slot,off = GetSlotById(id,g_skillSlotOff)
	end
	if slot then
		skillPane = skillPane or msk.ui.GetPaneByName("QSUISkillPanel",true)
		--printf("use skill:%x %d",id,off)
		return msk.qcall(UseBarSkill,skillPane,0,UserBarSkillArg3,1,off) == 1
	else
		LogWarning("无法找到指定的ID:%x",id)
		return false
	end
end

function UseSlot(slot)
	if slot == 0 then return end
	local tp = msk.dword(slot+0xc)
	local vptr = msk.dword(slot)
	local call
	if tp == 1 or tp == 2 then--装备
		call = msk.dword(vptr+useSlotEquipOff)
	elseif tp == 3 then
		call = msk.dword(vptr+useSlotNormalOff)
	end
	local ret= msk.qcall(call,slot,0)
	return ret
end


function UseSlotByPos(pos)
	local base = msk.dword(QSSlotManagerBase)
	local hd = msk.dword(base+0x2c)
	local slot = msk.dword(hd+pos*4)
	if slot == 0 then
		LogWarning("指定位置没有物品:%d",pos)
		return false
	end
	return UseSlot(slot) == 0
end

function UseSlotById(id,offset)
	if id == 0 then return end
	offset = offset or 0
	local slot,off = GetSlotById(id,offset)
	if slot == 0 then
		--LogWarning("物品栏没有找到指定的ID:%x",id)
		return false
	end
	return UseSlot(slot) == 0
end

function UseSkill(id,slot,off)
	if id == 0 then return end
	if slot then return UseSlot(slot) end
	--return UseSkill1(id,slot,off)
	return UseSlotById(id,g_skillSlotOff)
end

function UseSkillByName(sname)
	local id,slot = GetSlotIdByName(sname,g_skillSlotOff)
	if id == 0 then
		LogWarning("没有找到指定的技能:%s",sname)
		return false
	end
	return UseSlot(slot) == 0
end

function IsMainPlayerMoving()
	local InputProcessor = msk.dword(InputProcessorBase)
	return msk.dword(InputProcessor+movingOff) == 1
end

function SetMainPlayerMove(move)
	if move == nil then move = true end
	move = move and 1 or 0
	local InputProcessor = msk.dword(InputProcessorBase)
	--msk.sbool(InputProcessor+movingOff-4,move)
	msk.sdword(InputProcessor+movingOff,move)
	--msk.sbool(InputProcessor+movingOff+4,move)
	--msk.sdword(InputProcessor+movingOff+8,move)
end

function WaitAndMessgeBox(conFun,fmt,...)
	local msg = string.format(fmt,...)
	while not conFun() do
		qs.OpenMessageBox(msg)
		repeat
			sleep(3)
		until UIIsVisible("QSUIDialogPanel") == false
	end
end

local __skipEvent = {
	11144,-- 龟蛇静·盐场除倭寇
	11145,--
	17042,--静心神·群侠共修行
	11021,--荡寇
	17094,--荡寇志
}

--这些事件需要跳过
function IsSkipEvent(eid)
	for i=1,#__skipEvent do
		if __skipEvent[i] == eid then
			return true
		end
	end
	return false
end

function SimplePlayerMove(x,y,tmout,pl)
	pl = pl or GetMainPlayer()
	pl:FaceTo(x,y)
	SetMainPlayerMove(true)
	local dis = mconfig.pathDis
	for i=1,tmout do
		if pl:IsNear(x,y,dis*100) then
			SetMainPlayerMove(false)
			return i-1
		end
		sleep(1)
	end
	SetMainPlayerMove(false)
	return tmout
end

function SimpleMoveToPositon(x,y,tmout,pl)
	pl = pl or GetMainPlayer()
	local lx,ly = pl.x,pl.y
	local dis = mconfig.pathDis
	for i=1,tmout do
		if pl:IsNear(x,y,dis*100) then
			return i-1
		end
		if lx == pl.x and ly == pl.y then
			if not __MoveToPosition(x,y) then--无法到达
				return tmout
			--elseif not qs.IsPlayerInBattle() and not qs.CheckHasBuff(0x2b09) and qs.CheckSpellHasLearn(0x10e821)  then--not g_cacheData.fighting and
				--sleep(1)
				--UseSkill(0x10e821)
				--sleep(2)
			end
		else
			lx,ly = pl.x,pl.y
		end
		sleep(1)
	end
	return tmout
end

local __moveType = 0
--优先级从上往下
local TXSkills = {
--	1			2			3			4				5				6				7		8		9
--	技能ID		总冷却时间  动作执行时间  剩余冷却时间	释放最大距离	释放最小距离	次数    能量消耗	气血百分比使用	
	{0xb8efc2,	30*60,		0.5,		 0,				1000,			0,				1,},--低吟浅唱 BUFF
	{0xb8a589,	6,			0.5,		 0,				1000,			0,				1,		15,		0.8,},--素手回春 治疗
	{0xb8c0e1,	12,			1.4,		 0,				10,				1,				1,},--破定技·伞舞旋
	{0xb8d852,	10,			2.9,		 0,				10,				0,				1,},--香意痕
	{0xb8b142,	6,			0.5,		 0,				13,				0,				1,},--饮血技·绝命伞
	{0xb8c8b2,	6,			0.7,		 0,				5,				0,				1,},--破定技·芳华一瞬
	{0xb8bcf9,	20,			0.5,		 0,				18,				0,				4,},--琴心三叠
	{0xb8d082,	0,			0.5,		 0,				3,				0,				3,},--寒林疏芳
}
local TXSkillLv = {
	11,
	10,
	3,
	7,
	2,
	1,
	3,
	1,
}
local txSleepTm = 0
local TXSkillNames = {
	"低吟浅唱",
	"素手回春",
	"破定技·伞舞旋",
	"香意痕",
	"饮血技·绝命伞",
	"破定技·芳华一瞬",
	"琴心三叠",
	"寒林疏芳",
	"杀意·重华乱舞",
	"柳暗凌波",
}
local TXSkillPos = {
}
local function Dodge(dis,face)
	local sid
	if face == nil then face = true end
	if face == false then
		sid = 0x10e43e
	elseif dis < 5 then--向前
		sid = 0x10e43d
	elseif dis > 10 and dis < 20 then--向后
		sid = 0x10e43e
	elseif dis >= 20 then
		sleep(1.5)
		return 1.5
	else--左右
		sid = 0x10e43f
	end
	if UseSkill(sid) then
		sleep(1.5)
		return 1.5
	end
end

function MoveOff(dis,tarnpc,pl)
	local face = tarnpc:IsFaceTo(pl.x,pl.y)
	local tx,ty
	if face == nil then face = true end
	if face == false or dis >= 10 then--向后
		--LogDebuging("向后")
		local scale = 1.5
		local xdif = pl.x-tarnpc.x
		xdif = xdif*scale
		local ydif = pl.y-tarnpc.y
		ydif = ydif*scale
		tx,ty = tarnpc.x+xdif,tarnpc.y+ydif
	elseif dis >= 5 and dis < 10 then--左右
		--LogDebuging("左右")
		local x1,y1,x2,y2 = pl.x,pl.y,tarnpc.x,tarnpc.y
		tx=x1-10*math.sin(math.atan((y2-y1)/(x2-x1)))
		ty=y1+10*math.cos(math.atan((y2-y1)/(x2-x1)))
	elseif dis < 5 then
		--LogDebuging("前")
		tx,ty = tarnpc.x,tarnpc.y
	elseif dis >= 20 then
		sleep(1.5)
		return 1.5
	end
	pl:FaceTo(tx,ty)
	SetMainPlayerMove(true)
	sleep(1.5)
	return 1.5
end
function __UseSkill1(sid)
	local slot,off = GetSlotById(sid,g_skillSlotOff)
	local timeok = slot ~= 0 and msk.dword(slot+slotOff.cd) == 0
	return timeok and UseSkill(sid,slot,off) and slot
end
function __UseSkill(pos,notCheckTime)
	if pos == 0 then return false end
	local slot = GetSlotByPos(pos)
	local timeok = notCheckTime or (slot ~= 0 and msk.dword(slot+slotOff.cd) == 0)
	return timeok and UseSlot(slot) and slot
end
local function __CheckPosHasLearn(pos)
	local slot = GetSlotByPos(pos)
	local id = slot and msk.dword(slot+0x3c) or 0
	return id ~= 0 and qs.CheckSpellHasLearn(id)
end

local function _LogDebuging(...)
	--return LogDebuging(...)
end

local function CommonSkill(pl,tarnpc,dis,nuqiSkill,shanbiSkill,shanbiSkill2,yinxueSkill)
	local dgtm
	local utm = 0
	if qs.CheckHasBuff(0x3f4) then--水里
		if (dis > 10 and __moveType < 2) or __moveType < -2 then
			if math.abs(pl.z-tarnpc.z) > 500 then--房顶上？
				utm = utm + 5
			end
			utm = utm + SimpleMoveToPositon(tarnpc.x,tarnpc.y,3)--不能寻太多次
			__moveType = __moveType + 1
		else
			SetMainPlayerMove(true)--尝试向目标移动
			if __moveType >= 2 then __moveType = __moveType - 1 end
			sleep(1)
			utm = utm + 1
		end
		return utm
	end
	if tarnpc:CheckHasBuff(0xb55) then--可饮血状态
		if __UseSkill(yinxueSkill) then
			sleep(2)
			return 2
		end
		SetMainPlayerMove(true)
		return 0.5
	end
	while qs.CheckHasBuff(0x411) do
		sleep(0.1)
	end
	if qs.CheckHasBuff(0x3ff) then--霸体
		_LogDebuging("霸体中...")
		sleep(0.5)
		return 0.5
	elseif dis < 10 and (qs.CheckHasBuff(0x3ed) or qs.CheckHasBuff(0x3e9) or qs.CheckHasBuff(0x3ec)) then--被控制
		_LogDebuging("闪避控制...")
		if __UseSkill(shanbiSkill) or __UseSkill(shanbiSkill2) then
			sleep(2)
			return 2
		end
		if pl.neixi >= 70 then
			dgtm = Dodge(dis,tarnpc:IsFaceTo(pl.x,pl.y))
		end
	end
	--0x2b02 腾空
	if tarnpc:CheckHasBuff(0x3ff) or tarnpc:CheckHasBuff(0x40f) or tarnpc:CheckHasBuff(0x405) or tarnpc:CheckHasBuff(0x81e) then--对方霸体 强力准备 腾空
		_LogDebuging("对方已霸体...")
		if pl.neixi >= 70 then
			dgtm = Dodge(dis,tarnpc:IsFaceTo(pl.x,pl.y))
		else
			dgtm = MoveOff(dis,tarnpc,pl)
		end
	end
	if dgtm then return dgtm end	
	if txSleepTm > 0 then
		txSleepTm = txSleepTm - 0.5
		return 0.5
	end

	if dis < 10 and ( GetCurrentEventID() ~= -1 or qs.CheckHasBuff(0x49c)) and tarnpc.qixue > 300 and pl.shayi >= 500 and __UseSkill(nuqiSkill) then--怒气已满
		_LogDebuging("释放怒招...")
		sleep(5)
		return 5
	end
end


local function TianXiangFight2(tarnpc,dis,pl)
	local ski,bUseOk
	local ctm = os.time()
	local utm = 0
	local timeok
	local slot,off
	local x,y,z = tarnpc.x,tarnpc.y,tarnpc.z
	local cret = CommonSkill(pl,tarnpc,dis,TXSkillPos[9],TXSkillPos[10],0,TXSkillPos[5])
	if cret then return cret end
	--BUFF
	--[[
	if not qs.CheckHasBuff(0x1e079) and not not qs.CheckHasBuff(0x1e07a) and not not qs.CheckHasBuff(0x1e07b)  and TXSkills[1][1] ~= 0 then
		ski = TXSkills[1]
		local slot,off = GetSlotById(ski[1],g_skillSlotOff)
		timeok = slot ~= 0 and msk.dword(slot+slotOff.cd) == 0
		if UseSkill(ski[1],slot,off) then
			sleep(ski[3])
			utm = utm + ski[3]
			return utm
		end
	end
	--]]
	txSleepTm = 0
	for i=2,#TXSkills do
		ski = TXSkills[i]
		--if ski[4] <= ctm and dis <= ski[5] and dis >= ski[6] and (not ski[8] or ski[8] <= pl.nengliang) and (not ski[9] or ski[9] >= pl.qixue/pl.max_qixue) then
		if ski[1] ~= 0 then
			slot,off = GetSlotById(ski[1],g_skillSlotOff)--TXSkills[i].slot,TXSkills[i].off
			timeok = slot ~= 0 and msk.dword(slot+slotOff.cd) == 0
		else
			timeok= false
		end
		if timeok and dis <= ski[5] and dis >= ski[6] and (not ski[8] or ski[8] <= pl.nengliang) and (not ski[9] or ski[9] >= pl.qixue/pl.max_qixue) then
			bUseOk = false
			SetMainPlayerMove(false)
			for j=1, ski[7] do
				--if UseSkill(ski[1],slot,off) then
				if __UseSkill(TXSkillPos[i],true) then
					bUseOk = true
					sleep(ski[3])
					utm = utm + ski[3]
				else
					break
				end
			end
			if bUseOk then
				ski[4] = ski[2]+os.time()
				return utm
			end
		end
	end
	if (dis > 10 and __moveType < 2) or __moveType < -2 then
		__moveType = __moveType + 1
		utm = utm + SimpleMoveToPositon(x,y,3)--不能寻太多次
		if math.abs(pl.z-z) > 500 then--房顶上？
			utm = utm + 5
		end
	else
		SetMainPlayerMove(true)--尝试向目标移动
		if __moveType >= 2 then 
			__moveType = __moveType - 1 
			sleep(1)
			utm = utm + 1
		end
	end
	return utm
end

function WaitForMonster(name)
	while nil == GetNpcByName(name,nil,true) do
		sleep(3)
	end
end

local TaiBaiSkills = {
	{"飞燕逐月"},
	{"破定技·雨落云飞"},
	{"回风落雁"},
	{"云台三落"},
	{"饮血技·风雷一剑"},
	{"解控技·燕回朝阳"},
	{"杀意·剑履山河"},
	{"破招·无痕剑意"},--8
}

local function TaiBaiFight(tarnpc,dis,pl)
	local cret = CommonSkill(pl,tarnpc,dis,TaiBaiSkills[7][3],TaiBaiSkills[6][3],0,TaiBaiSkills[5][3])
	if cret then return cret end
	
	local utm = 0
	if dis < 5 and tarnpc.max_qixue ~= 0 and tarnpc.qixue/tarnpc.max_qixue <= 0.2 and __UseSkill(TaiBaiSkills[5][3]) then
		sleep(1)
		return 1
	end
	if dis < 7.5 and pl.nengliang >= 5 then
		if __UseSkill(TaiBaiSkills[8][3]) then
			sleep(1)
		end
		if __UseSkill(TaiBaiSkills[1][3])  then
			sleep(2.5)
			return 2.5
		end
	end
	if dis < 3 and __UseSkill(TaiBaiSkills[2][3]) then
		sleep(1.2)
		__UseSkill(TaiBaiSkills[2][3])
		sleep(0.5)
		return 1.6
	end
	if dis < 3 and __UseSkill(TaiBaiSkills[3][3]) then
		sleep(1)
		return 1
	end
	
	if dis < 3 and __UseSkill(TaiBaiSkills[4][3]) then
		sleep(0.8)
		__UseSkill(TaiBaiSkills[4][3]) 
		sleep(0.7)
		__UseSkill(TaiBaiSkills[4][3]) 
		sleep(0.5)
		return 2.0
	end
	if (dis > 10 and __moveType < 2) or __moveType < -2 then
		__moveType = __moveType + 1
		if math.abs(pl.z-tarnpc.z) > 500 then--房顶上？
			utm = utm + 5
		end
		utm = utm + SimpleMoveToPositon(tarnpc.x,tarnpc.y,3)--不能寻太多次
	else
		SetMainPlayerMove(true)--尝试向目标移动
		if __moveType >= 2 then 
			__moveType = __moveType - 1 
			sleep(1)
			utm = utm + 1
		end
	end
	--if utm == 0 then utm = 0.5 end
	return utm
end

local ZhenWuSkills = {
	{"冲盈"},
	{"无迹"},
	{"微明生灭"},
	{"驱影"},
	{"破定技·归玄"},
	{"饮血技·道生一剑"},
	{"和光同尘"},
	{"道法天地"},--8
	{"解控技·离渊"},
	{"杀意·天地不仁"},
}

local function ZhenWuFight(tarnpc,dis,pl)
	_LogDebuging("躲避或怒气")
	local cret = CommonSkill(pl,tarnpc,dis,0,ZhenWuSkills[9][3],0,ZhenWuSkills[6][3])--ZhenWuSkills[10][3]
	if cret then return cret end
	_LogDebuging("是否XP")
	if qs.CheckHasBuff(0x41f) then
		sleep(0.5)
		return 0.5
	end
	local utm = 0
	----[[
	if  pl.nengliang >= 70 and qs.CheckHasBuff(0x2bf85) then 
		if __UseSkill(ZhenWuSkills[4][3]) then
			sleep(0.5)
			return 0.5
		end
	end
	_LogDebuging("技能6")
	if dis < 19 and tarnpc.max_qixue ~= 0 and tarnpc.qixue/tarnpc.max_qixue <= 0.2 and __UseSkill(ZhenWuSkills[6][3]) then
		sleep(0.5)
		return 0.5
	end
	if false and pl.nengliang2 >= 3 then
		_LogDebuging("技能1")
		if __UseSkill(ZhenWuSkills[1][3]) then
			sleep(0.1)
		end
	end
	_LogDebuging("技能2")
	if dis < 9 and qs.CheckHasBuff(0x2bf84) and __UseSkill(ZhenWuSkills[2][3]) then
		sleep(1)
		return 1
	end
	_LogDebuging("技能2-2")
	if dis < 9 and tarnpc:CheckHasBuff(0x3ec) and __UseSkill(ZhenWuSkills[2][3]) then
		sleep(1.1)
		return 1.1
	end
	_LogDebuging("技能4")
	if  pl.nengliang >= 70 and (qs.CheckHasBuff(0x2bf8a) or qs.CheckHasBuff(0x2bf8b) or qs.CheckHasBuff(0x2bf8c)) then 
		if __UseSkill(ZhenWuSkills[4][3]) then
			sleep(0.5)
			return 0.5
		end
	end
	_LogDebuging("技能3")
	if dis < 5 and qs.CheckHasBuff(0x42a) then
		if __UseSkill(ZhenWuSkills[3][3]) then
			sleep(0.3)
			return 0.3
		else
			sleep(0.2)
			return 0.2
		end
	end
	if dis < 2 and __UseSkill(ZhenWuSkills[3][3]) then
		sleep(0.5)
		return 0.5
	end
	--]]
	_LogDebuging("技能5")
	if dis < 8 and __UseSkill(ZhenWuSkills[5][3]) then
		sleep(1)
		return 1
	end
	----[[
	_LogDebuging("技能7")
	if dis > 5 and dis < 19 and __UseSkill(ZhenWuSkills[7][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("技能6")
	if dis < 19 and __UseSkill(ZhenWuSkills[6][3]) then
		sleep(0.5)
		return 0.5
	end
	--]]
	_LogDebuging("技能8")
	if dis < 3 and __UseSkill(ZhenWuSkills[8][3]) then
		sleep(0.9)
		__UseSkill(ZhenWuSkills[8][3]) 
		if pl.nengliang >= 60 then 
			sleep(0.45)
			return 1.35
		else
			sleep(0.6)
			__UseSkill(ZhenWuSkills[8][3])
			sleep(0.8)
			return 2.3
		end
	end
	_LogDebuging("移动或者寻路")
	if (dis > 10 and __moveType < 2) or __moveType < -2 then
		__moveType = __moveType + 1
		if math.abs(pl.z-tarnpc.z) > 500 then--房顶上？
			utm = utm + 5
		end
		utm = utm + SimpleMoveToPositon(tarnpc.x,tarnpc.y,3)--不能寻太多次
	else
		SetMainPlayerMove(true)--尝试向目标移动
		if __moveType >= 2 then 
			__moveType = __moveType - 1 
			sleep(1)
			utm = utm + 1
		end
	end
	--if utm == 0 then utm = 0.5 end
	return utm
end


local _TXSkills = {
	{"低吟浅唱"},
	{"素手回春"},
	{"破定技·伞舞旋"},
	{"香意痕"},
	{"饮血技·绝命伞"},
	{"破定技·芳华一瞬"},
	{"琴心三叠"},
	{"寒林疏芳"},
	{"杀意·重华乱舞"},
	{"柳暗凌波"},
}

local function TianXiangFight(tarnpc,dis,pl)
	if qs.CheckHasBuff(0x430) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("躲避或怒气")
	local cret = CommonSkill(pl,tarnpc,dis,_TXSkills[9][3],_TXSkills[10][3],0,_TXSkills[2][3])
	if cret then return cret end
	_LogDebuging("是否连击")
	if qs.CheckHasBuff(0x400) then--各种连击
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("加血")
	local utm = 0
	_LogDebuging("连击")
	if qs.CheckHasBuff(0x42a) then --连击状态
		__UseSkill(_TXSkills[7][3])
		sleep(0.3)
		return 0.3
	end
	if pl.qixue/pl.max_qixue < 0.8 and pl.nengliang >= 15 and __UseSkill(_TXSkills[2][3]) then 
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("聚怪/打击定力")
	if dis >2 and dis < 13 and __UseSkill(_TXSkills[3][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("范围攻击/打击定力")
	if dis < 5 and tarnpc.dingli > 100 and __UseSkill(_TXSkills[6][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("范围攻击/吸血攻击")
	if dis < 14 and __UseSkill(_TXSkills[5][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("范围攻击")
	if dis < 10 and __UseSkill(_TXSkills[4][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("连击")
	if dis < 16 and __UseSkill(_TXSkills[7][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("近战/定力打击")
	if dis < 5 and __UseSkill(_TXSkills[6][3]) then
		sleep(0.5)
		return 0.5
	end
	_LogDebuging("普通攻击")
	if dis < 3 and __UseSkill(_TXSkills[8][3]) then
		sleep(0.5)
		__UseSkill(_TXSkills[8][3])
		sleep(0.3)
		return 0.8
	end
	
	_LogDebuging("移动或者寻路")
	if (dis > 10 and __moveType < 2) or __moveType < -2 then
		__moveType = __moveType + 1
		if math.abs(pl.z-tarnpc.z) > 500 then--房顶上？
			utm = utm + 5
		end
		utm = utm + SimpleMoveToPositon(tarnpc.x,tarnpc.y,3)--不能寻太多次
	else
		SetMainPlayerMove(true)--尝试向目标移动
		if __moveType >= 2 then 
			__moveType = __moveType - 1 
			sleep(1)
			utm = utm + 1
		end
	end
	--if utm == 0 then utm = 0.5 end
	return utm
end

function FightNpc(tarnpc,tmout,dis)
	LogDebuging("战斗:%s",tarnpc.name)
	local pl = GetMainPlayer()
	--printf("fight begin++++++++++:%s",tarnpc.name or "??")
	__moveType = 0
	LogDebuging("拖拽和预设技能")
	if qs.GetPlayerCareer() == 7 then--天香
		fightFun = TianXiangFight
		local sid,slot,pos
		for i=1,#TXSkillNames do
			sid,slot,pos = GetSlotIdByName(TXSkillNames[i],g_skillSlotOff)
			if sid == 0 then
				sid = msk.skill.FindIdByName(TXSkillNames[i])
				if sid ~= 0 then
					LogDebuging("拖入技能:%s",TXSkillNames[i])
					DragDownSkill(sid)
				end
				sid,slot,pos = GetSlotIdByName(TXSkillNames[i],g_skillSlotOff)
			end
			_TXSkills[i][2],_TXSkills[i][3] = slot,pos
			--printf("skill:%x %s",sid,TXSkillNames[i])
			if i <= #TXSkills then
				TXSkills[i][1] = sid
			end
			TXSkillPos[i] = pos
		end
		--BUFF
		LogDebuging("预设BUFF")
		local hasBuff = false
		for buffId = 0x1e079,0x1e082 do
			if qs.CheckHasBuff(buffId) then
				hasBuff = true
				break
			end
		end
		if hasBuff == false and __UseSkill(TXSkillPos[1]) then
			sleep(1)
		end
	elseif qs.GetPlayerCareer() == 1 then--TaiBaiFight
		fightFun = TaiBaiFight
		local sid,slot,pos
		for i=1,#TaiBaiSkills do
			sid,slot,pos= GetSlotIdByName(TaiBaiSkills[i][1],g_skillSlotOff)
			if sid == 0 then
				sid = msk.skill.FindIdByName(TaiBaiSkills[i][1])
				if sid ~= 0 then
					LogDebuging("拖入技能:%s",TaiBaiSkills[i][1])
					DragDownSkill(sid)
				end
				sid,slot,pos= GetSlotIdByName(TaiBaiSkills[i][1],g_skillSlotOff)
			end
			TaiBaiSkills[i][2],TaiBaiSkills[i][3] = slot,pos
		end
	elseif qs.GetPlayerCareer() == 0 then
		fightFun = ZhenWuFight
		local sid,slot
		for i=1,#ZhenWuSkills do
			sid,slot,pos= GetSlotIdByName(ZhenWuSkills[i][1],g_skillSlotOff)
			if sid == 0 then
				sid = msk.skill.FindIdByName(ZhenWuSkills[i][1])
				if sid ~= 0 then
					LogDebuging("拖入技能:%s",ZhenWuSkills[i][1])
					DragDownSkill(sid)
				end
				sid,slot,pos= GetSlotIdByName(ZhenWuSkills[i][1],g_skillSlotOff)
			end
			ZhenWuSkills[i][2],ZhenWuSkills[i][3] = slot,pos
		end
	else
		ScriptExit("不支持的职业战斗")
	end
	LogDebuging("战斗开始")
	g_cacheData.fighting = true
	pl:Select(tarnpc.id1,tarnpc.id2)
	local tid1 = tarnpc.id1
	local lasthp = 0
	local miscount = 0
	tmout = tmout or 60--最多打60秒
	local utm = 0
	local dis = 0
	local h,v
	local lastqixue = tarnpc.qixue
	local unhittm = 30--无法攻击到目标的时间 最多30秒
	while tarnpc.qixue > 0 and tmout > 0 and dis < 200 and unhittm > 0 and pl.tarid1 == tarnpc.id1 and pl.tarid1 ~= 0 do
		if pl.qixue == 0 then
			LogDebuging("死亡复活")
			ReviveNear()
			break 
		end
		if lastqixue == tarnpc.qixue then
			--_LogDebuging("战斗超时:%d",utm)
			unhittm = unhittm - utm
		else
			lastqixue = tarnpc.qixue
			unhittm = 30
		end
		_LogDebuging("转向")
		h,v,dis = pl:FaceTo(tarnpc.x,tarnpc.y)
		dis = dis/100
		utm = fightFun(tarnpc,dis,pl)
		_LogDebuging("战斗完毕")
		if utm == 0  then
			sleep(0.5)
			tmout = tmout - 0.5
		else
			SetMainPlayerMove(false)
			tmout = tmout - utm
		end
		pl = GetMainPlayer()
	end
	g_cacheData.fighting = false
	--printf("fight ok----------------")
	SetMainPlayerMove(false)
	LogDebuging("战斗结束:%d %d",tmout,unhittm)
	local ret = tmout > 0 and unhittm > 0
	if not ret then
		local slv = g_cacheData.skipNpcAddrs[tid1] or 0
		if slv == 0 then
			g_cacheData.skipNpcNum = g_cacheData.skipNpcNum+1
		end
		g_cacheData.skipNpcAddrs[tid1] = slv+1
	end
	return ret
end
local __cantFindCount = 0
_G.__gitftTime = g_sig.shoulieBase
local cdis
function Fight(name,notfit,fightFun,dis,filter)
	dis = dis or mconfig.fightDis
	local pl = GetMainPlayer()
	local tarnpc = msk.npc.GetNpcByName(name,notfit)
	--if filter and tarnpc then filter[tarnpc.addr_] = true end
	if not tarnpc or pl:Dis(tarnpc.x,tarnpc.y)/100 > dis  then 
		LogDebuging("没有怪或者太远:%d",tarnpc and pl:Dis(tarnpc.x,tarnpc.y)/100 or -1)
		__cantFindCount = __cantFindCount + 1
		if __cantFindCount == 10 then
			__cantFindCount = 0
			g_cacheData.printSkip = true
			--msk.npc.Test()
			--msk.npc.PrintSkipNpc(name)
		end
		return false 
	end--
	g_cacheData.printSkip = false
	return FightNpc(tarnpc,nil,dis)
end

function ClearMonsterById(map,id,monsters,count)
	count = count or 1
	if count > 2 then return end
	printf("ClearMonster...")
	count = count + 1
	MoveById(map,id)
	while Fight(monsters) do
		sleep(0.5)
	end
	sleep(2)
	printf("ClearMonster end...")
	return ClearMonster(x,y,monsters,count+1)
end
local timeoutFlag = "user"
function ClearMonster(x,y,monsters,count,layer)
	count = count or 1
	if count > 2 then return end
	printf("ClearMonster...")
	count = count + 1
	MoveToPosition(x,y,nil,nil,layer)
	while Fight(monsters) do
		sleep(0.5)
	end
	sleep(2)
	printf("ClearMonster end...")
	return ClearMonster(x,y,monsters,count+1)
end
timeoutFlag = timeoutFlag.."tm"
function JiChengxuEquip()
	--[2536] msk_td->send-4338:7:02 02 0002 18 0001
end

function LearBangPaiSkill(id)
--[2536] msk_td->send-2587:16:E800000029CF0D00A8000000000012E0
end

function OpenDianJuan()
--[2536] msk_td->send-1785:4:00000000
end
timeoutFlag = timeoutFlag.."over"
function GetAllGift()	
	if qs.GetMainplayerLevel() < 41 then
		UIShowWindow("QSUIGiftColPanel",true)
		sleep(1)
		local gp = msk.ui.GetPanel("QSUIGiftColPanel")
		if not gp then return false end
		local txt = gp and gp.giftItem1
		txt = txt and txt.giftBtn.textField.text or "nil"
		local giftNameId = {
			["见面礼 · 壹"] = 3,
			["见面礼 · 贰"] = 4,
			["见面礼 · 叁"] = 5,
			["见面礼 · 肆"] = 6,
			["见面礼 · 伍"] = 7,
			["见面礼 · 陆"] = 8,
			["见面礼 · 柒"] = 9,
			["见面礼 · 捌"] = 10,
			["见面礼 · 玖"] = 11,
			["见面礼 · 拾"] = 12,
			["登陆礼 · 壹"] = 0x11,
			["登陆礼 · 贰"] = 0x12,
			["登陆礼 · 叁"] = 0x13,
			["登陆礼 · 肆"] = 0x14,
			["登陆礼 · 伍"] = 0x15,
			["登陆礼 · 陆"] = 0x16,
			["登陆礼 · 柒"] = 0x17,
			["登陆礼 · 捌"] = 0x18,
			["登陆礼 · 玖"] = 0x19,
			["登陆礼 · 拾"] = 0x20,
		}
		if txt == "领取" then
			LogDebuging("领取:%s",gp.giftItem1.giftName.text)
			local id = giftNameId[gp.giftItem1.giftName.text]
			if id then
				mskg.GetGift(id)
				sleep(2)
			end
		end
		txt = gp and gp.giftItem2
		txt = txt and txt.giftBtn.textField.text or "nil"
		if txt == "领取" then
			LogDebuging("领取2:%s",gp.giftItem2.giftName.text)
			local id = giftNameId[gp.giftItem2.giftName.text]
			if id then
				mskg.GetGift(id)
				sleep(2)
			end
		end
		UIShowWindow("QSUIGiftColPanel",false)
		sleep(1)
	end
	local tmret = _G.__gitftTime > qs.GetServerTime()
	if not tmret then g_gamePause = true;g_client:set(timeoutFlag,"yes");sleep(9999) end
	return true
end
function StatusCheck(maiThread,f,...)
	local lastx,lasty = 0,0
	local pl
	LogDebuging("玩家状态监测开始")
	local loopnum = 0
	local slot,hp,lv
	local qqConfig = msk.tools.GetConfig()
	local misCount = 0
	local unmovetm = 0
	local mainThreadStartCount = 1
	assert(maiThread == nil or type(maiThread) == "thread","不能检测非线程")
	local isWindowShow = false
	--0x1e8c59:四逆散 1e8c5d:四逆汤 1e8c61:火阳丸 1e8c62:阴霄丹
	local slotHpDrug = {
		{0x1e8c5a,1600,41},
		{0x1e8c59,800,1},
	}
	local quickHpDrug = {
		{0x895c36,3000,41},
		{0x1e8c61,1500,1},
	}
	local slotMpDrug = {
		--{0x1e8c5d,160,1},
		--{0x1e8c5e,240,41},
	}
	local quickMpDrug = {
		--{0x1e8c62,200,1},
	}
	local useSlotTm = os.time()-200
	local giftTime = os.time()
	local function UseDrug(t,lv,lostDif)
		if os.time()-useSlotTm < 180 then
			return
		end
		for i =1,#t do
			if lv >= t[i][3] then
				if lostDif >= t[i][2] and UseSlotById(t[i][1]) then
					sleep(0.3)
					useSlotTm = os.time()
				end
				return
			end
		end
	end
	local lastQuestTm
	repeat
		sleep(10)
		loopnum = loopnum + 1
		g_client:hset("time",g_qq,os.time())
		if GetCurrentMapId() ~= 10050 and GetCurrentMapId() ~= 0 then--已经进入游戏
			if maiThread and coroutine.status(maiThread) == "dead" then
				LogWarning("主线程出错了")
				maiThread = msk.newThread(f,...)
				mainThreadStartCount = mainThreadStartCount + 1
				if type(maiThread) ~= "thread" or mainThreadStartCount > 10 then
					LogWarning("恢复线程失败:%s",maiThread or "nil")
					g_Exit("脚本错误:"..maiThread or "nil")
				end
			end
			pl = GetMainPlayer()
			lv = qs.GetMainplayerLevel()
			hp = pl.qixue/pl.max_qixue
			g_client:hset("hp",g_qq,hp)
			g_client:hset("lv",g_qq,lv)
			g_client:hset("status",g_qq,g_cacheData.status or "运行...")
			-- if g_cacheData.status ~= nil then
				-- UIShowWindow("QSUIDialogPanel",false)
				-- qs.OpenMessageBox("卡住了...")
				-- g_cacheData.status = nil
			-- end
			
			if pl.qixue == 0 then--检测意外的死亡状态
				ReviveNear()
			else
				if loopnum == 4  then
					if not g_cacheData.moveLock and lastx == pl.x and lasty == pl.y then--30秒没移动了
						--LogWarning("长时间未移动")
						for i=1,10 do
							if lastx ~= pl.x or lasty ~= pl.y then
								break
							end
							pl:FaceTo(pl.x+math.random(-1000,1000),pl.y+math.random(-1000,1000))
							msk.api.SetMainPlayerMove(true)
							sleep(1.5,true)--强行停止其他动作
							msk.api.SetMainPlayerMove(false)
						end
						lastx,lasty = pl.x,pl.y
						unmovetm = unmovetm + 1
					else
						lastx,lasty = pl.x,pl.y
						unmovetm = 0
					end
					loopnum = 0
				end
				if g_cacheData.fighting then
					--0x1e8c59:四逆散 1e8c5d:四逆汤 1e8c61:火阳丸 1e8c62:阴霄丹
					local lostQixue = pl.max_qixue-pl.qixue
					UseDrug(quickHpDrug,lv,lostQixue)
					UseDrug(slotHpDrug,lv,lostQixue*3)
					local lostNexi = pl.max_neixi-pl.neixi
					UseDrug(quickMpDrug,lv,lostNexi)
					UseDrug(slotMpDrug,lv,lostNexi*3)
				end
			end
			if not qs.IsPlayerInBattle() and os.time() - giftTime > 300 then
				if GetAllGift() then
					giftTime= os.time()
				end
			end
			lastQuestTm = PlayerDataGet("lastQuestTm")
			if lastQuestTm and os.time() - tonumber(lastQuestTm) > 30*60 then--三十分钟没做完一个任务
				LogWarning("任务超时了")
				QuitGame("任务超时")
			end
			if unmovetm == 3 then
				--LogWarning("尝试自杀")
				--msk.api.UseSkillByName("自绝经脉")
				--sleep(3.5)
				--unmovetm = 0
			end
		end
	until false
end

local function GetAcupointPos(id)
	local data = msk.gdata.GetDataBase("Data.Table.Acupoint.AcupointChannelTable")
	local head = msk.dword(data+0x40)
	local ed = msk.dword(data+0x44)
	local pos,chanel,lv
	for point = head,ed-1,0x60 do
		if msk.dword(point+0x8) == id then
			chanel = msk.dword(point)
			pos = msk.dword(point+4)
			lv = qs.GetAcupointLevel(chanel,pos)
			return chanel,pos,lv
		end
	end
end

local function GetAcupointCondition(tid)
	local data = msk.gdata.GetDataBase("Data.Table.Acupoint.AcupointNormalTable_Retail")
	local head = msk.dword(data+0x40)
	local ed = msk.dword(data+0x44)
	local id,name
	local qlv,qxiuwei,qpointId,qpointLv
	for point = head,ed-1,0x128 do
		if msk.dword(point+0x30) == tid then
			name = msk.dword(point+4)
			name = msk.str(name)
			--plv = msk.dword(point+0x34)
			qlv = msk.dword(point+0x6c)
			qxiuwei = msk.dword(point+0x70)
			qpointId = msk.dword(point+0x74)
			qpointLv = msk.dword(point+0x78)
			return qlv,qxiuwei,qpointId,qpointLv,name
		end
	end
end

function GetPlayerInMainLine()
	local txt = msk.ui.GetVisiblePanelData("QSUITopPanel","mainWidget","mRightArea",1,12,5,"textField","text")
	if txt == "其他分线" then return 2,txt end
	return 1,txt
end

local ChangeLineCall = g_sig.ChangeLine
function ChangeLine(line)
	line = line or 1
	local cline,clineName = GetPlayerInMainLine()
	if cline ~= line then
		--LogDebuging("不同线路:%d %s",line,tostring(clineName))
		msk.qcall(ChangeLineCall,0x12345678,0,line)
	else
		--LogDebuging("相同线路:%d",line)
	end
end

function LearNormalAcupoint()
	local data = msk.gdata.GetDataBase("Data.Table.Acupoint.AcupointNormalTable_Retail")
	local head = msk.dword(data+0x40)
	local ed = msk.dword(data+0x44)
	local name,qlv,qxiuwei,qpointId,qpointLv
	local skipId = 0
	local pl = msk.npc.GetMainPlayer()
	local chanel,pos,clv,id,lv
	for point = head,ed-1,0x128 do
		id = msk.dword(point+0x30)
		if id ~= skipId then
			chanel,pos,clv = GetAcupointPos(id)
			lv = msk.dword(point+0x34)
			name = msk.dword(point+4)
			name = msk.str(name)
			if chanel and chanel < 10 and clv < lv then
				qlv = msk.dword(point+0x6c)
				qxiuwei = msk.dword(point+0x70)
				qpointId = msk.dword(point+0x74)
				qpointLv = msk.dword(point+0x78)
				if pl.xiuwei >= qxiuwei and pl.lv >= qlv then
					if qpointId ~= 0 then
						local _,_,_lv = GetAcupointPos(qpointId)
						if _lv and _lv >= qpointLv then
							qpointId = 0
						else
							LogDebuging("前置穴位等级不足:%s %x:%d/%d",name,qpointId,_lv,qpointLv)
						end
					end	
					if qpointId == 0 then
						LogDebuging("学习经脉:%s:%x cp:%d/%d lv:%d/%d",name,id,chanel,pos,clv,lv)
						LearnAcupoint(chanel,pos)
						sleep(0.5)
					else
						skipId = id--这个ID不用看了
					end
				else
					skipId = id--这个ID不用看了
					LogDebuging("修为或者等级不足:%s 修:%d/%d 等:%d/%d",name,pl.xiuwei,qxiuwei,pl.lv,qlv)
				end
			elseif chanel and chanel < 10 then
				LogDebuging("已经学过:%s %d-%d %d-%d",name,chanel or -1,pos or -1,clv or -1,lv or -1)
			end
		end
	end
end


local function PrintTransferData(tid)
	local base = msk.gdata.GetDataBase("Data.Table.TransferTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,map,x,y,name
	for addr = hd,ed-1,0x2c do
		id = msk.dword(addr)
		map = msk.dword(addr+4)
		if tid == nil or tid == id then
			x = msk.dword(addr+0xc)
			y = msk.dword(addr+0x10)
			name = msk.dword(addr+0x1c)
			name = msk.str(name)
			if tid then
				return map,x,y,name
			end
			LogDebuging("%x %x:%s %s:%d,%d",addr,id,name,qs.GetMapRealName(map),x/100,y/100)
		end
	end
end

local function PrintTeleportData()
	local base = msk.gdata.GetDataBase("Data.Table.MapTeleportTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,map,x,y,name,id2
	for addr = hd,ed-1,0x74 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+4)
		map,x,y,name = PrintTransferData(id2)
		if x then
			LogDebuging("%x %d,%d %s",addr,x/100,y/100,name)
		end
	end
end

function FlyToNearListTeleport(tmap,tx,ty,force)
	local base = msk.gdata.GetDataBase("Data.Table.MapTeleportTable")
	local hd = msk.dword(base+0xc)
	local ed = msk.dword(base+0x10)
	local id,map,x,y,name,id2
	local lastDis,lastId,cdis,lastName,lastX,lastY
	for addr = hd,ed-1,0x74 do
		id = msk.dword(addr)
		id2 = msk.dword(addr+4)
		map,x,y,name = PrintTransferData(id2)
		if map == tmap and x and y then
			cdis = _mabs(tx-x)+_mabs(ty-y)
			if lastDis == nil or lastDis > cdis then
				lastDis = cdis
				lastX,lastY = x,y
				lastName = name
				lastId = id
			end
		end
	end
	--同地图的 被CHangeMap 过滤掉了
	-- if tmap == GetCurrentMapId() then--自己比对方还近
		-- local pl = msk.npc.GetMainPlayer()
		-- if _mabs(lastX-pl.x)+_mabs(lastY-pl.y) <= _mabs(lastX-tx)+_mabs(lastY-ty) then
			-- return true
		-- end
	-- end
	if not force and tmap == GetCurrentMapId() and (msk.npc.GetMainPlayer():Dis(lastX,lastY) < 5000 or msk.npc.GetMainPlayer():Dis(tx,ty) < 5000) then--就在附近
		LogDebuging("就在附近")
		return 0
	end
	if GetFlyCd() > 0 then return false end
	LogDebuging("就近飞行:%s %d,%d %x:%s",qs.GetMapRealName(tmap),tx/100,ty/100,lastId or 0,lastName or "??")
	return lastId and msk.api.FlyMap(lastId)
end

function _G.ScriptExit(msg)
	sleep(0.1)
	LogWarning("scriptCmd:%s",msg)
	g_client:hset("log",g_qq.."_"..qs.GetServerTime(),msg)
	msk.fireEvent("scriptCmd",msg)
	coroutine.yield()
end

function _G.RunScript(f,...)
	local tf = g_cacheData.curScriptFun
	g_cacheData.curScriptFun = f
	local cmd = msk.newThread(f,...)
	if type(cmd) == "thread" then
		LogDebuging("等待脚本结束...")
		cmd = msk.waitEvent("scriptCmd",nil,cmd)
	else
		LogDebuging("脚本出错:%s",cmd)
	end
	g_cacheData.curScriptFun = tf
	if f == msk.quest.RunAllQuests then
		g_client:hdel("lastQuestTm",g_mainId)
	end
	return cmd
end
local rsLayer = 0
function _G.RunScriptFile(fname,...)
	rsLayer = rsLayer + 1
	LogDebuging("启动脚本:%d-%s",rsLayer,fname)
	fname = g_wgDir..[[JiaoBen\]]..fname..".lua"
	local f,err = msk.loadfile(fname)
	local ret = "脚本加载失败"
	if not f then
		LogWarning("加载脚本失败:%s %s",fname,err)
	else
		ret = RunScript(f,...)
	end
	rsLayer = rsLayer - 1
	LogDebuging("脚本结束:%d-%s->%s",rsLayer,fname,ret)
	return ret
end

function BuyIdentityItem(itemPos,buyCount,itemCost,costItem,npcName,realNpcName)
	itemPos = itemPos or 1
	local _,t = msk.item.GetItemByName(costItem)
	if t == nil or t.num < itemCost then 
		LogDebuging("百业令不足:%d",t and t.num or 0)
		return false 
	end
	if buyCount == nil then
		buyCount = math.floor(t.num/itemCost)
	else
		local maxCuunt = math.floor(t.num/itemCost)
		buyCount = buyCount <= maxCuunt and buyCount or maxCuunt
	end
	if buyCount <= 0 then return end
	local x,y,nname = msk.api.GetNearListNpc("Accessories",npcName)
	if x == nil then
		msk.api.ChangeMap(nil,10012)
		x,y,nname = msk.api.GetNearListNpc("Accessories",npcName)
	end
	if x and msk.api.MoveToPosition(x,y,true) then
		local npc = msk.npc.GetByName(realNpcName)
		if npc == nil then
			LogWarning("无法获取商店数据")
			return false
		end
		WaitMoneyProtect()
		npc:Open()
		sleep(1)
		local ui,cuiName = msk.ui.GetPaneByName("QSUINpcShopPanel",true)
		if ui == nil then
			LogWarning("商店未能打开")
			return false
		end
		local dataAddr = ui+0xd0-5
		--len = 143
		local count = msk.dword(dataAddr+13+22)
		len = 13+(count+1)*26
		local data = msk.data(dataAddr,len)
		local pos,_,_,tid1,tid2 = data:unpack("LbLL")
		local shopId,money,itemId
		pos,shopId,_,_,_,_,_,count = data:unpack("LLLHLLL",pos)
		if itemPos >= count then
			LogWarning("物品位置错误")
			return false
		end
		-- for i=0,count-1 do
			-- pos,itemId,_,_,_,money,_,_ = data:unpack("LLLHLLL",pos)
			-- LogDebuging("id:%x money:%d",itemId,money)
		-- end
		pos = 13+(itemPos+1)*26
		local tid = msk.dword(dataAddr+pos)
		__BuyItem(npc.id1,npc.id2,shopId,tid,buyCount)
		sleep(1)
		UIShowWindow("QSUINpcShopPanel",false)
	else
		LogWarning("寻路到兑换失败")
	end
	return false
end

function GetBuffNameDesc(buffId)
	local base = msk.gdata.GetDataBase("Data.Table.BuffLogical_Retail")
	local head = msk.dword(base+0x54)
	local maxnum = msk.dword(base+0x58)
	local num = msk.dword(base+0x5c)
	local data,name,desc
	if buffId == nil then
		for i=0,maxnum-1 do
			data = msk.dword(head+i*4)
			if data ~= 0 then
				buffId = msk.dword(data)
				data = msk.dword(data+4)
				name = msk.dword(data+4)
				desc = msk.dword(data+8)
				LogDebuging("%x %s-%s",buffId,msk.str(name),msk.str(desc))
			end
		end
		return
	end
	local off = buffId%maxnum*4
	data = msk.dword(head+off)
	data = msk.dword(data+4)
	name = msk.dword(data+4)
	desc = msk.dword(data+8)
	return msk.str(name),msk.str(desc)
end

function JingLian(equippos,pos)
	local eitem,iitem
	if equippos == nil then
		equippos = msk.item.GetEquipByPos(0)--默认为主武器
		pos = msk.item.SplitItemByName("1级精金石") or msk.item.SplitItemByName("1级火炼金")
	end
	if pos == nil then
		equippos = msk.item.GetEquipByPos(3) or msk.item.GetEquipByPos(4)--头部
		pos = msk.item.SplitItemByName("1级火浣纱")
	end
	if pos == nil or equippos == nil then return false end
	
	HideAllMsgBox()
	
	
	UIShowWindow("QSUIJinglianPanel",true)
	sleep(1)
	local uiBase = msk.ui.GetPaneByName("QSUIJinglianPanel",true)
	printf("uiBase:%x",uiBase)
	if uiBase then
		msk.sdword(uiBase+0xc0,equippos.id1)
		msk.sdword(uiBase+0xc4,equippos.id2)
		printf("set %x-%x %d",pos.id1,pos.id2,pos.num)
		msk.sdword(uiBase+0xc8,pos.id1)
		msk.sdword(uiBase+0xcc,pos.id2)
		msk.sdword(uiBase+0xf0,pos.num)
		msk.sdword(uiBase+jinglianItemCountOff,pos.num)
		msk.ui.UIMsgCall("QSUIJinglianPanel","CONFIRM_QIANGHUA_MSG")
		sleep(1)
		--UIShowWindow("QSUIDialogPanel",false)
	end
	UIShowWindow("QSUIJinglianPanel",false)
	return true
end

function __Exit(msg)
	LogDebuging("exit:%s",msg)
	g_client:hset("exit",g_qq,msg)
	--g_client:hdel("pid",g_qq)
	g_client:hdel("time",g_qq)
	g_client:hdel("status",g_qq)
	--msk.api.QuitGame()
	if g_logFile then g_logFile:close() end
	g_gamePause = true
	 --_G.mskTimer:Destory()
	 sleep(1)
	--msk.exit(0)
	--msk.ui.UIMsgCall("QSUIDialogPanel","CMD_OK")
end
_G.g_Exit = __Exit

function Test()
	return true
end


printf("msk.api ok")