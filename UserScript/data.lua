module("msk.gdata",package.seeall)

local print = printf
local dataBase = g_sig.dataBase
dataCache = {}
offsetCache = {}
dataInit = false
UISlotInfoCache = {}

function GetUISlotBase(name)
	if UISlotInfoCache[name] then
		return UISlotInfoCache[name]
	end
	local data,name
	for i=g_sig.UISlotInfoBase,g_sig.UISlotInfoBase+g_sig.UISlotInfoMax,4 do
		data = msk.dword(i)
		name = msk.dword(data+4)
		if name ~= 0 then
			name= msk.str(name)
			if name ~= "" then
				UISlotInfoCache[name] = data
			end
		end
	end
	return UISlotInfoCache[name]
end

function DataInit()
	dataInit = false
end

function GetDataBase(dname,prt)
	local bgAddr = msk.dword(dataBase)
	local off = g_sig[dname]
	if not off then
		LogWarning("基址不存在:%s",dname)
		return 0,0
	end
	do return msk.dword(bgAddr+off),off end
end

function Get(dname)
	if dataInit then
		return dataCache[dname],offsetCache[dname]
	end
end

function PrintALlDataBase()
	--LogDebuging("开始输出数据...")
	dataInit = true
	local bgAddr = msk.dword(dataBase)
	local addr,name
	for i=0x10,0x700,4 do
		addr = msk.dword(bgAddr+i)
		if addr ~= 0 then
			name = msk.dword(addr+0x4)
			if name ~= 0 then
				name = msk.str(name)
				if name and #name > 0 then
					dataCache[name] = addr
					offsetCache[name] = i
					LogDebuging("基础数据 %x:%s:%x",i,name,addr)
				else
					--LogDebuging("无名对象1 %x",i)
				end
			else
				--LogDebuging("无名对象2 %x",i)
			end
		else
			--LogDebuging("跳过 %x",i)
		end
	end
end

function Test()
	PrintALlDataBase("",true)
end
printf("msk.gdata ok")