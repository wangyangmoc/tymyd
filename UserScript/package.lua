module("msk.pack",package.seeall)
local print = _G.printf
local bin2hex = msk.tools.bin2hex
local hex2bin = msk.tools.hex2bin

local SendData = g_sig.SendData
local sendLenAddr
local curDataLen = 0
local curDataAddr = g_sendDataBuff
local sendDataBase = g_sig.sendDataBase
function Send(head,data)
	local ecx = msk.dword(sendDataBase)
	--ecx = msk.dword(ecx+0x14)
	--sendLenAddr = sendLenAddr or msk.new(4)
	assert(#data < 4096,"无法发送这么大的包")
	-- if curDataLen < #data then
		-- if curDataAddr then msk.delete(curDataAddr) end
		-- curDataAddr = msk.new(#data)
		-- curDataLen = #data
	-- end
	msk.sdata(curDataAddr,data,#data)
	--printf("send: ecx:%x head:%x,data:%x,len:%d,outaddr:%x",ecx,head,curDataAddr,#data,sendLenAddr)
	return msk.qcall(SendData,ecx,0,#data,curDataAddr,head)
	--return msk.dword(sendLenAddr) == #data
end
function Sendf(head,fmt,...)
	local data = string.pack(fmt,...)
	--printf(hex2bin(data))
	Send(head,data)
end
function SendStr(head,sdata)
	local data = hex2bin(sdata,"(.)(.)")
	Send(head,data)
end

function Test()
end

printf("msk.pack ok")