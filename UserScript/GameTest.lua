--GameTest.lua

require "socket"
local redis = require "redis"
local json = require "dkjson"
local __reload = false
if msk.g_issue then
	msk.dofile(g_wgDir..[[tools\sigs.txt]])--加载特征码
else
	__reload = true
	msk.dofile(g_wgDir..[[tools\sigs2.txt]])--加载特征码
end
local __sigs = _g_sig
local sigsMt = {}
sigsMt.__index = function(t,k)
	local v = __sigs[k]
	if type(v) == "number" then
		v = mskg.decodeSig(v)
	end
	return v
end
g_sig = {}
setmetatable(g_sig,sigsMt)
g_client = redis.connect('127.0.0.1', 6379)
g_qq = g_qq or g_client:get("creatingqq")
g_client:del("creatingqq")
g_client:hset("pid",g_qq,msk.pid())
printf("g_pauseAddr---------:%x",g_pauseAddr)
g_client:hset("pause",g_qq,g_pauseAddr)
g_client:hset("dir",msk.pid(),msk.currentdir():upper())
g_client:hset("time",g_qq,os.time())
g_client:hset("status",g_qq,"登入...")

msk.require("global_tables",__reload)
msk.require("config",__reload)
msk.require("data",__reload)
msk.require("item",__reload)
msk.require("tools",__reload)
msk.require("ui",__reload)--ui需要放在skill之前加载
msk.require("skill",__reload)
msk.require("npc",__reload)
msk.require("hook",__reload)
msk.require("package",__reload)
msk.require("api",__reload)
msk.require("html",__reload)
msk.require("events",__reload)
msk.require("quest",__reload)
msk.require("call_back",__reload)
msk.require("npc_talks",__reload)
msk.require("team",__reload)
msk.require("reload",__reload)
msk.require("paimai",__reload)
msk.require("sendApi",__reload)
msk.require("gameApi",__reload)
msk.require("aide",__reload)
msk.require("name",__reload)
--msk.require("window",__reload)
local print = printf
g_Print = true --控制HOOK
g_LvPrint = 1--不要打印到对话框 否则卡

LogDebuging("set g_blockPairAddr.............")
--初始化
msk.sbool(g_blockPairAddr,0)
if (msk.api.GetCurrentMapId() == 10050 or msk.api.GetCurrentMapId() == 0) then--选择角色
	printf("选择角色中...不执行脚本")
	msk.require("script1")
	return
end

printf("脚本开始...:%d",msk.api.GetCurrentMapId())
g_testMode = true
--g_testMode = false
g_testModule = false
g_LvPrint = 0

g_cacheData.logTime = os.time()-200
if g_testMode then
	msk.require("testGame",true)
else
	g_changingMap = false
	g_statusThread = nil
	g_allQuests = nil
	msk.require("script1",true)
end
printf("运行结束了")