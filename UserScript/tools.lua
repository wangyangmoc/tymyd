module("msk.tools",package.seeall)
local json = require "dkjson"

function bin2hex(s,flag)
	local t= {}
	for i=1,#s do
		t[#t+1] = string.format("%02X",string.byte(s,i,i))
	end
    return table.concat(t,flag or ' ')
end

local h2b = {
    ["0"] = 0,
    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["A"] = 10,
    ["B"] = 11,
    ["C"] = 12,
    ["D"] = 13,
    ["E"] = 14,
    ["F"] = 15
}


function hex2bin( hexstr,flt )
	flt = flt or "(.)(.)"
    local s = string.gsub(hexstr,flt , function ( h, l )
         return string.char(h2b[h]*16+h2b[l])
    end)
    return s
end

function hex2bin2(hstr)
	return hex2bin(hstr,"(.)(.)")
end


function toSerial(s,f,name)
   if type(s)=="number"  then
          f:write(s)
   elseif  type(s)=="string" then
          f:write(string.format("%q",s))
   elseif type(s)=="table" then
		  if name then
			f:write(name,"=")
		  end
          f:write('{\n')
          for i, v in pairs(s) do
               f:write('[') toSerial(i,f) f:write(']=')
               toSerial(v,f)
               f:write(',\n')
          end
          f:write('}\n')
	elseif type(s) == "boolean" then  
        f:write( s and "true" or "false" ) 
	else
		error("未知的类型"..type(s))
   end
end

local function __IsFileExit(fname)
	local f = io.open(fname)
	if f then
		f:close()
		return true
	end
	return false
end

local function __CreateFile(fname)
	local f = io.open(fname,"w")
	f:close()
end

local function __CreateDir(dirname)
	os.execute("md "..dirname)
end


function LoadMainConfig()
	local qid = g_qq
	local dir = g_wgDir..[[Temp\]]
	local fname = dir..qid
	if not __IsFileExit(fname) then
		__CreateFile(fname)
		return nil
	end
end

function LoadConfig()
	local qid = msk.dword(g_sig.QQBase)
	local dir = g_wgDir..[[Temp\]]..os.date("%Y_%m_%d")
	local fname = dir.."\\"..qid
	if not __IsFileExit(fname) then
		__CreateDir(dir)
		__CreateFile(fname)
		return nil
	end
	dofile(fname)
	return true
end

function GetCache()
	local qq = g_qq
	local str = g_client:hget("cache",qq)
	return str and json.decode(str)
end

function SetCache(t)
	local str = json.encode(t)
	local qq = g_qq
	g_client:hset("cache",qq,str)
end

local __config
function GetConfig()
	if __config then return __config end
	local str = g_client:hget("config",g_qq)
	__config = str and json.decode(str)
	return __config
end

function SetConfig(t)
	t = t or __config
	local str = json.encode(t)
	local qq = msk.dword(g_sig.QQBase)
	g_client:hset("config",qq,str)
end

function GetDailyData(key)
	return g_client:hget(key.."_"..os.date("%Y_%m_%d",qs.GetServerTime()),g_mainId) 
end

function SetDailyData(key,v)
	return g_client:hset(key.."_"..os.date("%Y_%m_%d",qs.GetServerTime()),g_mainId,v) 
end

function GetData(key,filed)
	local str = g_client:hget(key,filed)
	return str and json.decode(str)
end

function SetData(key,filed,t)
	local str = json.encode(t)
	g_client:hset(key,filed,t)
end

function table.clone (t, nometa)
  local u = {}
  if not nometa then
    setmetatable (u, getmetatable (t))
  end
  for i, v in pairs (t) do
    u[i] = v
  end
  return u
end

function _G.metamethod (x, n)
  local _, m = pcall (function (x)
                        return getmetatable (x)[n]
                      end,
                      x)
  if type (m) ~= "function" then
    m = nil
  end
  return m
end

function _G.render (x, open, close, elem, pair, sep, roots)
  local function stopRoots (x)
    if roots[x] then
      return roots[x]
    else
      return render (x, open, close, elem, pair, sep, table.clone (roots))
    end
  end
  roots = roots or {}
  local s
  if type (x) ~= "table" or metamethod (x, "__tostring") then
    s = elem (x)
  else
    s = open (x)
    roots[x] = elem (x)
    local i, v = nil, nil
    for j, w in pairs (x) do
      s = s .. sep (x, i, v, j, w) .. pair (x, j, w, stopRoots (j), stopRoots (w))
      i, v = j, w
    end
    s = s .. sep(x, i, v, nil, nil) .. close (x)
  end
  return s
end

if _G.g_orzTostring == nil then
	_G.g_orzTostring = _G.g_orzTostring or tostring
	local _tostring = tostring
	function _G.tostring (x)
	  return render (x,
					 function () return "{" end,
					 function () return "}" end,
					 _tostring,
					 function (t, _, _, i, v)
					   return i .. "=" .. v
					 end,
					 function (_, i, _, j)
					   if i and j then
						 return ","
					   end
					   return ""
					 end)
	end
end
function _G.prettytostring (t, indent, spacing)
  indent = indent or "\t"
  spacing = spacing or ""
  return render (t,
                 function ()
                   local s = spacing .. "{"
                   spacing = spacing .. indent
                   return s
                 end,
                 function ()
                   spacing = string.gsub (spacing, indent .. "$", "")
                   return spacing .. "}"
                 end,
                 function (x)
                   if type (x) == "string" then
                     return string.format ("%q", x)
                   else
                     return tostring (x)
                   end
                 end,
                 function (x, i, v, is, vs)
                   local s = spacing .. "["
                   if type (i) == "table" then
                     s = s .. "\n"
                   end
                   s = s .. is
                   if type (i) == "table" then
                     s = s .. "\n"
                   end
                   s = s .. "] ="
                   if type (v) == "table" then
                     s = s .. "\n"
                   else
                     s = s .. " "
                   end
                   s = s .. vs
                   return s
                 end,
                 function (_, i)
                   local s = "\n"
                   if i then
                     s = "," .. s
                   end
                   return s
                 end)
end

printf("msk.tools ok")