local print = _G.printf
local sleep = msk.sleep

local function __WaitLeaderInvite(teamId)
	local count = 0
	local data,len
	local id = tostring(qs.GetMainPlayerId())
	local teamIndex = "team_"..teamId
	if qs.IsPlayerInTeam() == false then
		g_client:hset("team_in",id,false)
		LogDebuging("等待队长邀请!")
		local leaderName
		msk.api.HookRecv()
		repeat
			data,len = msk.waitEvent(0x2331,10)
			if not data then
				g_client:hset("team_time",id,os.time())
			else
				data = msk.data(data,len)
				local pos,_,id1,id2,name = data:unpack("LLLz")
				leaderName = g_client:hget(teamIndex,"leaderName")
				if name == leaderName then
					AgreeInvite(id1,id2)
					sleep(2)
				end
				UIShowWindow("QSUIDialogPanel",false)
			end
		until qs.IsPlayerInTeam()
		msk.api.UnHookRecv()
	end
	g_client:hset("team_in",id,true)
	LogDebuging("等待进组成功!")
	return true
end

local function __CheckTeamMem(teamId)
	local teamIndex = "team_"..teamId
	local teamList = "list_"..teamId
	LogDebuging("开始邀请队友")
	g_client:hset(teamIndex,"leaderName",msk.npc.GetMainPlayer().name)
	local t = g_hget(teamIndex,"team_info")
	local memId
	local memLen
	local meminteam,memtime,memt
	local allOk,tm
	repeat
		memLen = g_client:llen(teamList)
		LogDebuging("开始检查:%d/%d",memLen,t.num)
		allOk = memLen >= t.num
		tm = os.time()
		for i=1,memLen-1 do
			memId = g_client:lindex(teamList,i)
			meminteam = g_client:hget("team_in",memId)
			memtime = g_client:hget("team_time",memId)
			if not meminteam then
				if tm-memtime < 20 then
					memt = g_hget(teamIndex,memId)
					LogDebuging("邀请:%s",memt.name)
					InviteTeam(memt.id1,memt.id2,memt.name)
					sleep(0.5)
				else
					LogDebuging("目标似乎不在线:%s",memId)
				end
				allOk = false
			end
		end
		if not allOk then
			sleep(5)
		end
	until allOk
	LogDebuging("邀请完毕 全员到齐")
	return true
end
local notSkipZudui = true
function WaitTeam(tar,num)
	num = num or msk.config.teamNum
	local teamIndex
	local teamList
	local id = tostring(qs.GetMainPlayerId())
	local pl = msk.npc.GetMainPlayer()
	local teamId = g_client:hget("team_id",id)
	local teamTar = g_client:hget("team_tar",id)
	if teamId and teamTar == tar and notSkipZudui then
		LogDebuging("已有队伍:%d",teamId)
		--teamIndex = "team_"..teamId
		g_client:hset("team_time",id,os.time())
		if qs.TeamIsLeader() then
			return __CheckTeamMem(teamId)
		end
		if qs.IsPlayerInTeam() then
			return true
		end
		return __WaitLeaderInvite(teamId)
	end
	if qs.IsPlayerInTeam() then
		LogDebuging("退出已有队伍")
		LeveaTeam()
		sleep(2)
	end
	local t = g_client:hget("team_wait",tar)
	local isLeader = false
	if t then
		t.count = t.count or 0
		t.count = t.count + 1
		teamIndex = "team_"..t.teamId
	end
	if t == nil then
		isLeader = true
		t = {
			count = 0,
			num = num,
			teamId = g_client:incr("team_idins")
		}
		teamIndex = "team_"..t.teamId
		g_hset(teamIndex,"team_info",t)
		LogDebuging("新的队伍%d",t.teamId)
	end

	local playerInfo = {
		id1 = pl.id1,
		id2 = pl.id2,
		name = pl.name,
		time = os.time(),
	}
	g_client:hset("team_id",id,t.teamId)
	g_client:hset("team_tar",id,tar)
	teamList = "list_"..teamIndex
	g_client:rpush(teamList,id)
	--判断队伍是否准备完毕 完毕后从队列去除
	if t.count >= t.num then
		LogDebuging("组队完成了")
		g_client:hdel("team_wait",tar)
	else
		g_hset("team_wait",tar,t)
	end
	local ret
	if isLeader then
		g_hset(teamIndex,id,playerInfo)
		ret = __CheckTeamMem(teamIndex)
	else
		ret = __WaitLeaderInvite(teamIndex,playerInfo)
	end
	g_client:set("team_step",id,0)
	return ret
end

function WaitTeamStep(step,tar)
	local id = tostring(qs.GetMainPlayerId())
	local teamId = g_client:get("team_id",id)
	local teamIndex = "team_"..teamId
	local teamList = "list_"..teamId
	local curStep = g_client:hget(teamIndex,"step")
	curStep = tonumber(curStep) or 0
	g_client:set("team_step",id,curStep)
	local allOk = false
	local memLen,memId,configt
	while not allOk do
		allOk = true
		memLen = g_client:llen(teamList)
		for i=0,memLen-1 do
			memId = g_client:lindex(teamList,i)
			if memId ~= id then
				configt = g_hget("config",memId)
				if not configt.step or tostring(configt.step) < step then
					allOk = false
					break
				end
			end
		end
		if not allOk then
			--处理一下意外错误
			if qs.TeamIsLeader() then
				__CheckTeamMem(teamIndex)
			else
				__WaitLeaderInvite(teamIndex)
			end
			sleep(5)
		end
	end
end

printf("team_ LoadOk")