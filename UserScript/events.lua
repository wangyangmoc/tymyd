local sleep = msk.sleep
local lastDesc
local lastDescAddr
local function GetCurQuestUiDesc()
	local descAddr = msk.dwords(g_sig.curQuestDesc,0x118)
	local descAddr2 = msk.dwords(g_sig.curQuestDesc,0x11c)
	if lastDescAddr ~= descAddr2 then
		lastDescAddr = descAddr2
		lastDesc = msk.str(descAddr)
	end
	return lastDesc
end

function MoveDown(x,y,pl,h)
	for i=1,10 do
		if pl.z > h then
			pl:FaceTo(x*100,y*100)
			msk.api.SetMainPlayerMove(true)
			sleep(3)
		else
			msk.api.SetMainPlayerMove(false)
			break
		end
	end
end

g_EventProcess = {
	--穿越竹林
	[10200] = function (eid)
		local monsters = {"雷堂杀手","雷堂打手","雷霄"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			--LogWarning("trace:%s",trace or "nill")
			if trace == "[PathMon,雷霄,10042,100222]" then
				msk.api.ClearMonster(2702,3425,monsters)
			elseif trace and #trace > 1 then
				msk.api.ClearMonster(2790,3581,monsters)
				msk.api.ClearMonster(2810,3548,monsters)
				msk.api.ClearMonster(2818,3515,monsters)
				msk.api.ClearMonster(2806,3466,monsters)
				msk.api.ClearMonster(2760,3464,monsters)
				msk.api.ClearMonster(2737,3479,monsters)
				msk.api.ClearMonster(2698,3453,monsters)
				msk.api.ClearMonster(2702,3425,monsters)
			else
				LogWarning("trace:%s",trace or "nil")
			end
			sleep(1)
		end
		LogWarning("剧情结束了 ","穿越竹林")
	end,
	----[[
	--云笈水榭
	[10210] = function (eid)
		local desc,cmd,nname,map,insid,insid2
		local pl = msk.npc.GetMainPlayer()
		local monsters = {"翼堂打手","翼堂杀手"}
		while eid == msk.api.GetCurrentEventID() do
			--desc = GetCurQuestUiDesc()
			if pl:IsNear(271700,327100) then
				MoveDown(2735,3250,pl,8000)
			elseif pl:IsNear(271000,318100) then
				MoveDown(2698,3167,pl,8000)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(2)
		end
	end,
	--天香-重华乱舞
	[18021] = function (eid)
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[Map_X_Y,对岸,13901,1261,1020]" then
				msk.api.MoveToPosition(1275,913)
				msk.api.MoveToPosition(1261,1020)
				sleep(5)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--]]
	--茅屋之战
	[10201] = function (eid)
		local jump = false
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,雷震,10043,400248]" then
				msk.api.MoveById(10043,400248)
				if jump == false then
					for i=1,100 do
						if UIIsVisible("QSUIGuidePanel") then--qs.CheckHasBuff(0x43e) then
							msk.api.KeyEvent(0x57,"shift")--shift
							sleep(1)
							jump = true
							break
						end
						sleep(1)
					end
				else
					if UIIsVisible("QSUIGuidePanel") then--qs.CheckHasBuff(0x43e) then
						msk.api.KeyEvent(0x57,"shift")--shift
						sleep(1)
						--msk.api.UseSkill1(0x10e43d)
					end
				end
			end
			msk.quest.RunCurDesc(nil,true)
			sleep(1)
		end
	end,
	--枫桥镇
	--[==[
	[19003] = function (eid)
		local trace,cmd,nname,map,insid
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "去[Map_X_Y,南厢,10910,842,2574]除去沈月魂" then
				msk.api.MoveToPosition(842,2574)
				msk.api.Fight("沈月魂")
			elseif trace and trace:find("PathMon") then
				cmd,nname,map,insid = trace:match("%[(.-),(.-),(.-),([^%]]+)%]")
				nname = GetQuestName(nname)
				msk.api.MoveById(map,insid)
				local npc = msk.npc.GetByName(nname) or msk.npc.GetByName()
				for i=1,50 do
					if npc then 
						msk.api.FightNpc(npc)
					else
						break
					end
					npc = msk.npc.GetByName(nname) or msk.npc.GetByName()
				end
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--]==]
	
	--[[
	--逐梦人·宝剑配英雄
	[17007] = function (eid)
		local desc,cmd,nname,map,insid,insid2
		local pl = msk.npc.GetMainPlayer()
		local monsters = {"流氓","刘大","地痞"}
		local npc
		while eid == msk.api.GetCurrentEventID() do
			npc = msk.npc.GetNpcByName("金十八")
			if npc then
				msk.api.MoveToPosition(npc.x,npc.y,true)
				msk.quest.TrySubmitNpcTalk(nil,nil,"金十八","[PathMoveNpc,金十八,10755,400762,20104550]",true)
			else
				msk.api.ClearMonster(1895,1378,monsters)
			end
			sleep(3)
		end
	end,
	--]]
	--枫荷桥
	[14004] = function (eid)
		if eid ~= msk.api.GetCurrentEventID() then
			LogWarning("事件不匹配:%d-%d",id,msk.api.GetCurrentEventID())
			return false
		end
		local desc,cmd,nname,map,insid,insid2
		local pl = msk.npc.GetMainPlayer()
		local monsters = {"神秘刺客","神秘刺客剑手"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathNPCLayer,神秘刺客,10702,100003,1]" then
				msk.api.ClearMonster(2034,1796,monsters)
				sleep(5)
				msk.api.ClearMonster(2003,1909,monsters)--桥上
				sleep(5)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--天星阁
	[14005] = function (eid)
		--local monsters = {"流杀门 吴孤桐","流杀门 周柏英"}
		local monsters = {"逆水坞刺客","逆水坞喽喽","逆水坞小头目","逆水坞高手","流杀门帮众","流杀门 吴孤桐","流杀门 周柏英"}
		local trace
		local npc
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			npc = msk.npc.GetByName(monsters)
			while npc do
				msk.api.FightNpc(npc)
				sleep(0.5)
				npc = msk.npc.GetByName(monsters)
			end
			--if trace and trace:find("PathMon") then
				--msk.api.ClearMonster(3108,1146,monsters)
			--[[
			if trace == "[PathMon,流杀门桐柏双奇,10703,9989]" or trace == "[PathMon,流杀门帮众,10703,9989]" then
				msk.api.ClearMonster(3106,1117,monsters)
			elseif trace == "[PathMon,逆水坞围攻刺客,10703,300067]" then
				msk.api.ClearMonster(3108,1146,monsters)
			elseif trace == "[PathMon,逆水坞围攻高手,10703,300067]" then
				msk.api.ClearMonster(3108,1146,monsters)
			--]]
			msk.quest.RunCurDesc(nil,true)
			sleep(1)
		end
	end,
	--普通·荡平龙首山
	[11004] = function (eid)
		local monsters = {"校场水贼","殷天翼"}
		local monsters2 = {"护院刀手","谢宏"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == nil then
				trace = msk.quest.GetRunTraceDesc(g_questInfo,true)
				if trace == "[PathMon,护院刀手,10911,300061]" then
					msk.api.ClearMonsterById(10911,300061,monsters2)
				else
					sleep(3)
				end
			elseif trace == "击败[PathMon,殷天翼,10911,300062]" then
				msk.api.ClearMonster(1859,1171,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--俏佳人·青龙潭救美
	[16006] = function (eid)
		local monsters = {"好色毛贼","毛贼头目","铁老大"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace and trace:find("PathMon") then
				msk.api.ClearMonster(1579,2521,monsters)
			elseif trace == nil and msk.quest.GetRunTraceDesc(g_questInfo,true) == "[PathMon,铁老大,11104,1774]" then
				sleep(2)
				msk.api.MoveToPosition(1567,2551)
				eobj = msk.npc.GetEntityByName("包袱")
				if eobj and msk.api.MoveToPosition(eobj.x,eobj.y,true) then
					Collect(eobj.id1,eobj.id2)
					sleep(4.5)
				end
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--山隘伏击
	[16007] = function (eid)
		local monsters = {"廖星","伏击杀手"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,廖星及伏击杀手,11107,1941]" then
				msk.api.ClearMonster(2713,1883,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--宁海之夜
	[16008] = function (eid)
		local monsters = {"严泽","严泽手下","藏珍阁管事"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,严泽及其手下,11106,1973]" then
				msk.api.ClearMonster(2863,2287,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--铸神谷
	[19002] = function (eid)
		local monsters = {"巡游打手","分堂精锐"}
		local monsters2 = {"淫贼","金入木","分堂精锐"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "杀死[PathMon,巡游打手,10901,100778]" then
				msk.api.ClearMonster(1589,2704,monsters)
			elseif trace == "杀死[PathMon,守卫刀手,10901,100785]" then
				msk.api.ClearMonster(1617,2847,monsters2)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--真武-室内位面
	[16305] = function(eid)
		local npc
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathNpcLayer,君佩云,13709,99,1]" then
				msk.api.MoveToPosition(2480,3040,nil,nil,1)
				msk.api.Fight("君佩云")
				sleep(2)
			elseif trace == "[PathNpcLayer,燕南飞,13709,88,1]" then
				msk.api.MoveToPosition(2480,3040,nil,nil,1)
				msk.api.Fight("燕南飞")
				sleep(2)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--道者心·有情亦无情
	[14017] = function(eid)
		local npc
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "与[PathNpc,张天虹,13750,113]比试" then
				msk.api.MoveById(13750,113)
				msk.api.Fight("张天虹")
				sleep(2)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--真武-长生楼事件
	[16303] = function(eid)
		local npc
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if msk.npc.GetMainPlayer().z < 81600 then
				msk.api.MoveToPosition(2409,3624)
				msk.api.SimplePlayerMove(239200,363300,10)
			end
			if trace and trace:find("凌玄") then
				LogDebuging("预备战斗--移动")
				msk.api.MoveById(13705,400227)
				LogDebuging("预备战斗--开打")
				msk.api.Fight("凌玄")
				sleep(1)
			else
				LogDebuging("默认处理:%s",trace or "nil")
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--化清寺佛像
	[26004] = function(eid)
		local npc
		while eid == msk.api.GetCurrentEventID() do
			if msk.npc.GetMainPlayer():IsNear(307500,264200) then
				npc = msk.npc.GetNpcByName("燕南飞")
				if npc and msk.npc.GetMainPlayer():IsNear(npc.x,npc.y,10000) then
					sleep(10,true)
				else
					msk.quest.RunCurDesc(nil,true)
				end
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--静心神·群侠共修行
	[17042] = function(eid)
		local npc
		while eid == msk.api.GetCurrentEventID() do
			if qs.CheckHasBuff(0x2b09) then
				msk.api.UseSkill(0x10e822)
				sleep(1,true)
			end
			if not qs.CheckHasBuff(0x44e) then
				msk.api.UseSkill(0x201ac1)
			end
			sleep(10,true)
		end
	end,
	--荡口
	[11021] = function(eid)
		while eid == msk.api.GetCurrentEventID() do
			--RunScriptFile("dangkou")
			sleep(10)
		end
	end,
	--龙凤劫·援手江湖侣
	[17001] = function (eid)
		local monsters = {"十二连环坞恶贼","十二连环坞打手"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "打败[PathMon,十二连环坞恶贼,10903,735]" then
				msk.api.ClearMonster(1104,2772,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--凤凰集
	[18010] = function (eid)
		local monsters = {"灵峰湾巡查","灵峰湾守卫"}
		local monsters2 = {"新月白衣弟子","灵峰湾雅奴"}
		local monsters3 = {"新月紫衣弟子"}
		local monsters4 = {"花子缎"}
		local monsters5 = {"新月执事弟子","灵峰水榭雅奴"}
		local trace
		--[1748] msk_td->当前目标:
		local qtMoveCount = 0
		local lastTrace,lastTraceTime = "nil",0
		local waterTimeOut = 120
		local otherTimeOut = 300
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if lastTrace == trace and (os.time()-lastTraceTime > otherTimeOut or (qs.CheckHasBuff(0x3f4) and os.time()-lastTraceTime > waterTimeOut))then--10分钟 或者水里2分钟
				LogWarning("水里呆太久了")
				lastTraceTime = os.time()
				msk.api.UseSkillByName("自绝经脉")
				sleep(5)
				msk.api.ReviveNear()
				trace = msk.quest.GetRunTraceDesc(g_questInfo)
			elseif lastTrace ~= trace then
				lastTrace = trace
				lastTraceTime = os.time()
			end
			--LogDebuging("距离上次:%d %s,%s %s",os.time()-lastTraceTime,tostring(lastTrace),tostring(trace),tostring(qs.CheckHasBuff(0x3f4)))
			if msk.npc.GetMainPlayer():IsNear(248100,274200,1000) or msk.npc.GetMainPlayer():IsNear(243200,267800,1000) then
				LogDebuging("卡点1")
				msk.api.MoveToPosition(2367,2823)
			elseif msk.npc.GetMainPlayer():IsNear(226700,273200,1000) then
				LogDebuging("卡点2")
				msk.api.SimplePlayerMove(225000,279400,10)
				msk.api.MoveToPosition(2367,2823)
			end
			if trace == nil then
				local ctrace = msk.quest.GetRunTraceDesc(g_questInfo,true)
				if ctrace == "[Map_X_Y,窃听,10800,2294,2945]" then
					qtMoveCount = qtMoveCount + 1
					if qtMoveCount == 2 then
						qtMoveCount = 0
						MoveDown(2344,2948,msk.npc.GetMainPlayer(),14000)
						msk.api.SimpleMoveToPositon(229400,294500,10)
					else
						msk.api.SimplePlayerMove(229400,294500,5)
					end
					sleep(3)
				elseif ctrace == "[PathMon,花子缎,10800,11721]" then
					if not msk.npc.GetMainPlayer():IsNear(225700,291100,10000) then
						msk.api.MoveToPosition(2367,2823)
					end
					msk.api.ClearMonster(2257,2911,monsters4)
					sleep(3)
				end
			elseif trace == "[PathMon,灵峰湾守卫和巡查,10800,102062]" then
				msk.api.ClearMonster(2586,2863,monsters)
				msk.api.ClearMonster(2572,2793,monsters)
				msk.api.ClearMonster(2529,2749,monsters)
			elseif trace == "[PathMon,新月白衣弟子和灵峰湾雅奴,10800,102071]" then
				msk.api.ClearMonster(2424,2888,monsters2)
			elseif trace == "[PathMon,新月白衣弟子和灵峰湾雅奴,10800,102070]" then
				local maxCount = 0
				msk.api.MoveToPosition(2354,2953,nil,nil,1)
				while msk.api.Fight(monsters2,nil,nil,20) and maxCount < 50 do
					sleep(0.5)
					maxCount = maxCount + 1
				end
				sleep(2)
				maxCount = 0
				msk.api.MoveToPosition(2409,2911)
				while msk.api.Fight(monsters2,nil,nil,20) and maxCount < 50 do
					sleep(0.5)
					maxCount = maxCount + 1
				end
				sleep(2)
				maxCount = 0
				msk.api.MoveToPosition(2370,2911,nil,nil,1)
				while msk.api.Fight(monsters2,nil,nil,20) and maxCount < 50 do
					sleep(0.5)
					maxCount = maxCount + 1
				end
				sleep(2)
				maxCount = 0
				msk.api.MoveToPosition(2361,2925,nil,nil,1)
				sleep(1)
				while msk.api.Fight(monsters2,nil,nil,20) and maxCount < 50 do
					sleep(0.5)
					maxCount = maxCount + 1
				end
				sleep(2)
				--[1748] msk_td->14577.555664063
			elseif trace == "[PathArea,上楼,10800,300831]" then
				if false == msk.api.MoveById(10800,300831) then
					msk.api.MoveToPosition(2426,2881)
				end	
			elseif trace == "[PathMon,花子缎,10800,11721]"   then
				MoveDown(2254,2910,msk.npc.GetMainPlayer(),14000)
				if not msk.npc.GetMainPlayer():IsNear(225700,291100,10000) then
					msk.api.MoveToPosition(2367,2823)
				end
				msk.quest.RunCurDesc(nil,true)
			elseif trace == "[Map_X_Y,新月紫衣弟子,10800,2377,2963]" then
				msk.api.ClearMonster(2377,2963,monsters3)
			elseif trace:find("新月执事弟子和灵峰水榭雅奴") then
				if not msk.npc.GetMainPlayer():IsNear(225700,291100,10000) then
					msk.api.MoveToPosition(2367,2823)
				end
				local map,qid = trace:match(".+,(%d+),(%d+)%]")
				msk.api.ClearMonsterById(map,qid,monsters5)
			elseif trace:find("新月白衣弟子和灵峰湾雅奴") then
				local map,qid = trace:match(".+,(%d+),(%d+)%]")
				msk.api.MoveById(map,qid)
				while msk.api.Fight(monsters2) do
					sleep(0.5)
				end
				sleep(2)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--东越三清观
	[16014] = function (eid)
		local monsters = {"天风流上忍"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,天风流上忍,11112,2490]" then
				msk.api.ClearMonster(1797,2507,monsters)
				msk.api.ClearMonster(1797,2560,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
		
	
	
	--真武-重华技教学
	[16302] = function (eid)
		local monsters = {"滋事剑痞","滋事毛贼","毛贼头目"}
		local trace
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,滋事毛贼,13703,100119]" or trace == "[PathMon,滋事剑痞,13703,100119]" or trace == "[PathMon,滋事头目,13703,100123]" or trace == "[PathMon,滋事剑痞,13703,100112]" then
				msk.api.ClearMonster(2106,3157,monsters)
				msk.api.ClearMonster(2145,3194,monsters)
				msk.api.ClearMonster(2186,3234,monsters)
				msk.api.ClearMonster(2243,3263,monsters)
				msk.api.ClearMonster(2278,3279,monsters)
				msk.api.ClearMonster(2318,3305,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	
	--武林旗·天波府之试
	[20011] = function (eid)
		local monsters = {"卫丘瘦","卫丘胖","校尉总领","卫兵","校尉侍卫"}
		local trace
		local npc
		local notGetNpcCount = 0
		while eid == msk.api.GetCurrentEventID() do
			msk.api.MoveToPosition(2124,1356)
			npc = msk.npc.GetByName(monsters)
			if npc then
				msk.api.FightNpc(npc)
				notGetNpcCount = 0
			else
				LogDebuging("找不到怪...")
				if notGetNpcCount%10 == 5 then
					msk.npc.PrintSkipNpc(name)
				end
				notGetNpcCount = notGetNpcCount + 1
			end
			if notGetNpcCount > 150 then
				LogWarning("太多次无法找到怪物....")
				msk.api.QuitGame("天波府-找怪太多次")
			end
			sleep(1)
		end
	end,
	--杭州江湖试练
	[12120] = function(eid)
		local npcs = {{"于百石",2192,1920},{"白邓通",2177,1929}}
		local trace,npc
		local monsters = {}
		local fx,fy
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			--local JianwenUI.Data()
			if trace then
				fx,fy = nil,nil
				for i=1,#npcs do
					if trace:find(npcs[i][1]) then
						monsters[1] = npcs[i][1]
						fx,fy = npcs[i][2],npcs[i][3]
					end
				end
				if fx and msk.api.MoveToPosition(fx,fy) then
					npc = msk.npc.GetByName(monsters)
					if npc then msk.api.FightNpc(npc) end
					--msk.api.ClearMonster(fx,fy,monsters)
				end
			end
			sleep(3)
		end
		UIShowWindow("QSUIShiLianRewardPanel",false)
		UIShowWindow("QSUIShilianEntryPanel",false)
	end,
	--天绝禅院
	[18012] = function(eid)
		local trace
		local monsters = {"蒙面刺客","禅院守卫"}
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == "[PathMon,蒙面刺客,10802,10163]" then
				msk.api.ClearMonsterById(10802,10163,monsters)
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end,
	--东平涤荡
	[10825] = function(eid)
		local trace,ctrace
		local monsters = {"蒙面刺客","禅院守卫"}
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			msk.quest.RunCurDesc(nil,true)
			sleep(1)
		end
	end,
	--东平郡王府孔雀
	[18013] = function(eid)
		local trace,ctrace
		local monsters = {"蒙面刺客","禅院守卫"}
		while eid == msk.api.GetCurrentEventID() do
			trace = msk.quest.GetRunTraceDesc(g_questInfo)
			if trace == nil then
				ctrace = msk.quest.GetRunTraceDesc(g_questInfo,true)
				if ctrace == "[PathMon,仔细查看,10803,300133]" then
					msk.api.SimplePlayerMove(164234,357116,10)
					msk.api.MoveById(10803,300133)
					sleep(3)
				end
			else
				msk.quest.RunCurDesc(nil,true)
			end
			sleep(1)
		end
	end
}
printf("msk.events ok")
