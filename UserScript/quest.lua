module("msk.quest",package.seeall)
local printf = _G.printf
local sleep = msk.sleep
local GetMainPlayer = msk.npc.GetMainPlayer
local GetNpcByInstanceId = msk.npc.GetNpcByInstanceId
local bin2hex = msk.tools.bin2hex
local CallManuFacture = g_sig.CallManuFacture
local Send = msk.pack.Send
local Heal = msk.api.Heal
local UseSkill = msk.api.UseSkill
local lastDesc
local lastDescAddr
function GetCurQuestDesc()
	local descAddr = msk.dwords(g_sig.curQuestDesc,0x118)
	local descAddr2 = msk.dwords(g_sig.curQuestDesc,0x11c)
	if lastDescAddr ~= descAddr2 then
		lastDescAddr = descAddr2
		lastDesc = msk.str(descAddr)
	end
	return lastDesc
end

function GetRunTraceDesc(event_info,getCompleteTrace)
	if not event_info then return nil end
	--if not event_info.fix then
		FixEventInfo(event_info)
	--end
	local trace
    for i = 1 , #event_info.trace_array do
        trace = event_info.trace_array[i]
		if getCompleteTrace then return trace.desc,trace end
		if trace.target == nil then
			return trace.desc,trace
		elseif  trace.count and trace.count < trace.target then
			return trace.desc,trace
		end
    end
end

function FixEventInfo(event_info)
    local event_instance = _G.DgnEventFind(msk.api.GetCurrentEventID())
	if not event_instance then return end
	event_info.fix = true
    for i = 1 , #event_info.trace_array do
        local trace = event_info.trace_array[i]
		if(trace.slot_type == nil or trace.slot == nil or trace.target == nil) then
		else
			local count = nil
			if(trace.slot_type == nil) then
				count = event_instance:GetInt(trace.slot)
			else
				local slot_type = trace.slot_type
		   
				if(slot_type == 1) then
					count = event_instance:GetInt(trace.slot)
				elseif(slot_type == 2) then
					count = event_instance:GetActorTable(qs.GetMainPlayerId(),trace.slot)
				end
			end
			trace.count = count
		end        
    end
end

function SubmitQuestByInscanceId(insid,questid,idx)
	local npc = msk.npc.GetNpcByInstanceId(insid)
	if npc then
		SubmitQuestById(questid,npc.id1,npc.id2,idx)
		return true
	end
	LogWarning("cant find a npc instanced:%d",insid)
	return false
end

function SubmitQuestByName(npcname,questid,idx)
	local npc = msk.npc.GetNpcByName(npcname)
	if npc then
		SubmitQuestById(questid,npc.id1,npc.id2,idx)
		return true
	end
	LogWarning("cant find a npc named:%s",npcname or "nil")
	return false
end
function SubmitQuest(insid,nname,questid,idx)
	local npc = insid and msk.npc.GetNpcByInstanceId(insid) or msk.npc.GetNpcByName(nname)
	if npc then
		SubmitQuestById(questid,npc.id1,npc.id2,idx)
		return true
	end
	LogWarning("cant find a npc %s-%d",nname or "??",insid or 0)
	return false
end

--[[
function SubmitCurrentQuestByName(npcname)
	if not npcname then
		local str = GetCurQuestDesc()
		if str:find("PathNPC") == nil then
		LogWarning("SubmitCurrentQuestByName failed,not get PathNPC")
			return false
		end
		local cmd,nname,map,insid = str:match("%[(.+),(.+),(%d+),(%d+)%]")
		if not nname then
		LogWarning("SubmitCurrentQuestByName failed,parse desc failed:%s",str or "nil")
			return false
		end
		local tittle,truename = nname:match("([^ ]+)%s+([^ ]+)")
		npcname = truename or nname
	end
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local tid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	LogDebuging("尝试完场当前任务对话:%s %x",npcname,tid)
	SubmitQuestByName(npcname,tid)
end
--]]
local function __DefaultIsMoveToNpcOk()
	return UIIsVisible("QSUINpcDialogPanel")
end
local function __DefaultMoveToNpcInit()
	UIShowWindow("QSUINpcDialogPanel",false)
end

function WaitForNextQuest()
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local lgid = cur_group_id
	local lqid = cur_state.current_quest_id
	local lqdid = cur_state.current_qiaoduan_id
	local liqd = cur_state.include_qiaoduan
	while lgid == cur_group_id 
		and lqid == cur_state.current_quest_id 
		and liqd == cur_state.include_qiaoduan 
		and lqdid == cur_state.current_qiaoduan_id do
		sleep(3)
		cur_group_id = qs.GetCurrentQuestTrackGroupId()
		cur_state = qs.GetQuestGroupInfo(cur_group_id)
	end
	prints("next quest ok",lgid,cur_group_id,lqid,cur_state.current_quest_id )
end

function GetQuestNode(qid)
	local base = msk.gdata.GetDataBase("QSQuestInstanceManager")
	local head = msk.dword(base+0x20)
	local num = msk.dword(base+0x24)
	local off = qid%num*4
	local node = msk.dword(head+off)
	while node ~= 0 and msk.dword(node) ~= qid do
		node = msk.dword(node+8)
	end
	return node ~= 0 and node or nil
end

local function CollectEnt(desc,curdata,force)
	local cmd,nname,map,insid = desc:match("%[(.-),(.-),(.-),(.-)%]")
	cmd = cmd:upper()
	if not force and cmd ~= "PATHENT" then return false end
	nname = GetQuestName(nname,msk.api.GetCurrentEventID())
	prints("采集",desc,nname,map,insid)
	if msk.api.MoveById(map,insid) then
		sleep(1)
		local eobj
		for i=1,20 do
			eobj = msk.npc.GetEntityByName(nname)
			if eobj and msk.api.MoveToPosition(eobj.x,eobj.y,true) then
				Collect(eobj.id1,eobj.id2)
				sleep(4.5)
			else
				LogWarning("找不到采集物:%s",nname)
				sleep(1)
			end	
			if not curdata or msk.dword(curdata) >= msk.dword(curdata+4) then
				break
			end
		end
		return true
	else
	LogWarning("采集失败:寻路失败:%s",desc)
	end
end

local __lasttsqid,__lasttsid
function TrySubmitNpcTalk(qid,id,nname,desc,try)
	if not qid then
		local cur_group_id = qs.GetCurrentQuestTrackGroupId()
		cur_state = qs.GetQuestGroupInfo(cur_group_id)
		qid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	end
	if __lasttsqid == qid and __lasttsid == id then
		sleep(5)
		__lasttsqid,__lasttsid = 0,0
		return
	else
		__lasttsqid,__lasttsid = qid,id
	end
	
	local t
	desc = desc:match("(%[.-%])") or desc
	for i=1,#g_npcTalks do
		t = g_npcTalks[i]
		if t.qid == qid and t.desc == desc and (not t.eid or msk.api.GetCurrentEventID() == t.eid) then
			if t.instanceId and t.instanceId ~= id then
				LogWarning("不一致的 instanceId:%d-%d",t.instanceId,id or -1)
			end
			return msk.api.NpcTalkComplete(t.instanceId or id,nname,unpack(t.args))
		end
	end
	if not try then
		LogWarning("找不到对应的谈话NPC %x,%d,%s,%s",qid,id or -1,nname or "??",desc or "??")
	end
end

function RunCurDesc(ct,onece)
	ct = onece and 10 or ct or 1
	if ct > 10 then return end
	local id = msk.api.GetCurrentEventID()
	if id ~= -1 and g_questInfo then FixEventInfo(g_questInfo) end
	if not onece and g_EventProcess[id] then return g_EventProcess[id](id)  end
	if id == -1 and not g_questInfo then sleep(3) LogWarning("没有事件") return end
	if id ~= -1 then
		LogDebuging("剧情副本 %d %s",id,qs.GetEventName(id))
	else
		LogDebuging("见闻!!")
	end
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local qid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	local desc,cmd,nname,map,insid,insid2,trace,isMonsterFind
	local pl = msk.npc.GetMainPlayer()
	--desc = GetCurQuestDesc()
	--printr(g_questInfo)
	desc,trace = GetRunTraceDesc(g_questInfo)
	if not desc then 
		LogDebuging("无描述...")
		sleep(3) 
		return 
	end
	LogDebuging("当前目标:%s",desc or "??")
	cmd,nname,map,insid = desc:match("%[(.-),(.-),(.-),([^%]]+)%]")
	if not cmd then sleep(3) return end
	cmd = cmd:upper()
	nname = GetQuestName(nname)
	if cmd == "PATHAREA" then
		msk.api.MoveById(map,insid)
	elseif cmd == "PATHMON" then
		local mname = {}
		local mps = {}
		for i=1,#g_questInfo.trace_array do
			trace = g_questInfo.trace_array[i]
			if trace.count and trace.target and trace.count < trace.target then
				cmd,nname,map,insid = trace.desc:match("%[(.-),(.-),(.-),([^%]]+)%]")
				if cmd:upper() == "PATHMON" then
					--if not isMonsterFind then--ID里没有找到对应的怪物
						nname = GetQuestName(nname,msk.api.GetCurrentEventID())
						LogDebuging("加新怪物:%s",nname)
						mname[#mname+1] = nname
					--end
					mps[#mps+1] = {map,insid}
				else
				end
			else
			end
		end
		--循环清理怪物
		if #mname > 0 then
			Heal()
			local npc
			for i=1,#mps do
				npc = msk.npc.GetNpcByName(mname)
				if npc then
					msk.api.FightNpc(npc)
				else
					msk.api.MoveById(mps[i][1],mps[i][2])
				end
				while msk.api.Fight(mname,nil,nil,100) do
					sleep(0.5)
				end
			end
		else
			sleep(3)
		end
	elseif cmd == "PATHMOVENPC" then
		if not trace.target or (trace.count and trace.target and trace.count < trace.target) then
			insid,insid2 = insid:match("(%d+),%s*(%d+)")
			if msk.api.MoveToNpc(map,insid,insid2) then
				TrySubmitNpcTalk(qid,insid2,nname,desc)
				sleep(2)
			end
		else
			sleep(3)
		end
	elseif cmd == "MAP_X_Y" then
		insid,insid2 = insid:match("(%d+),%s*(%d+)")
		msk.api.MoveToPosition(insid,insid2)
		if TrySubmitNpcTalk(qid,insid2,nname,desc,true) then
			sleep(2)
		elseif trace.desc == "去[Map_X_Y,南厢,10910,842,2574]除去沈月魂" then
			Heal()
			while msk.api.Fight("沈月魂",nil,nil,100) do
				sleep(0.5)
			end
		elseif desc == "在一旁聆听[MAP_X_Y,叶知秋,10930,1871,1367]到访所为何事" then
			local oldt = msk.api.GetCurrentEventID()
			for i=1,100 do
				if oldt ~= msk.api.GetCurrentEventID() or msk.api.GetCurrentEventID() == -1 then
					break
				end
				sleep(5)
			end
		else
			sleep(3)
		end
	elseif cmd == "PATHENT" then
		CollectEnt(trace.desc)
	elseif cmd == "PATHNPC" then
		if id == 11004 and (desc == "[PathNpc,唐二,10911,1158]" or desc == "[PathNpc,唐二,10911,300923]") then
			msk.api.MoveById(map,insid)
			local npc = msk.npc.GetNpcByName("唐二")
			local oldt = g_questInfo
			if msk.api.MoveToPosition(1850,1146,nil,nil,nil,5) then
			--msk.api.OpenNpc(npc.id1,npc.id2)
				local oldt = msk.api.GetCurrentEventID()
				for i=1,100 do
					if oldt ~= msk.api.GetCurrentEventID() or msk.api.GetCurrentEventID() == -1 then
						break
					end
					sleep(5)
				end
			end
		else
			if  msk.api.MoveToNpc(map,insid) then
				TrySubmitNpcTalk(qid,nil,nname,desc)
				sleep(2)
				--[[
				if TrySubmitNpcTalk(qid,nil,nname,desc) then
					sleep(2)
				elseif msk.api.NpcTalk(nname) then
					sleep(2)
				else
					local npc = msk.npc.GetNearTalkNpc()
					if npc then
						msk.api.NpcTalk(npc.instanceId)
						sleep(2)
					end
				end
				--]]
			end
			--return--暂不支持对话
		end
	elseif cmd == "PATHNPCLAYER" then
		insid,insid2 = insid:match("(%d+),%s*(%d+)")
		msk.api.MoveById(map,insid,insid2)
		--[[
		local npc = msk.npc.GetNearTalkNpc()
		if npc then
			msk.api.NpcTalk(npc.instanceId)
			sleep(2)
		end
		--]]
	else
		LogDebuging("未知的命名:%s",desc)
	end
end

local function IsQuestSlotComplete(qid,idx)
	local qnode = GetQuestNode(qid)
	if qnode then
		local data = msk.dword(qnode+4)
		local curdatanodehd = msk.dword(data+0x2c)
		local curdata = msk.dword(curdatanodehd+idx*4)
		return msk.dword(curdata) >= msk.dword(curdata+4)
	end
	return true
end

function RunCurrentQuest(count)
	if msk.api.GetCurrentEventID() ~= -1 then
		return RunCurDesc()
	end
	count = count or 1
	if count > 10 then return end
	quest_group = quest_group or {}
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local qid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	local qnode = GetQuestNode(qid)
	if not qnode then sleep(3) return RunCurrentQuest(count+1) end
	local data = msk.dword(qnode+4)
	local rundatanode = msk.dword(data+0x24)
	local curdatanodehd = msk.dword(data+0x2c)
	local rundata = msk.dword(rundatanode+0xc)
	
	local name = msk.str(msk.dword(rundata+0x2c))
	LogDebuging("RunCurrentQuest:%d %s",qid,name)
	
	local subdata_hd = msk.dword(rundata+0x14)
	local subdata_ed = msk.dword(rundata+0x18)
	local idx = 0
	local sidx = 1
	local curdata,qidx,tp,curnum,num,desc,id
	local tnname,cmd,nname,map,insid,x,y
	local id2,id3,monsters,monsternames,bRun
	local pl = GetMainPlayer()
	for i=subdata_hd,subdata_ed-1,0x2c do
		qidx = msk.dword(i)
		tp = msk.dword(i+4)
		id = msk.dword(i+8)
		id2 = msk.dword(i+0xc)
		id3 = msk.dword(i+0x10)
		desc = msk.str(msk.dword(i+0x18))
		curdata = msk.dword(curdatanodehd+idx*4)
		curnum = msk.dword(curdata)
		num = msk.dword(curdata+4)
		if curnum ~= num then
			bRun = true
			LogDebuging("%x:%d:%d %s %d/%d --%x,%d,%d,%d %#x",idx,qidx,tp,desc,curnum,num,qid,id,id2,id3,data)
			--do return end
			if g_speciaQuestBefireRun[qid] and g_speciaQuestBefireRun[qid][tp] then
				LogDebuging("特殊任务处理")
				g_speciaQuestBefireRun[qid][tp](qid,pl)
			end
			if tp == 1 or tp == 2 then--杀怪
				if CollectEnt(desc,curdata) then return RunCurrentQuest(count+1) end
				cmd,nname,map,insid = desc:match("%[(.-),(.-),(.-),(.-)%]")
				nname = GetQuestName(nname)
				monsternames = monsternames or {}
				monsternames[#monsternames+1] = id--nname
				monsters = monsters or {}
				monsters[#monsters+1] = {map,insid,idx}
			elseif tp == 3 then--带选项的对话 或者采集
				local force = qid == 0x1d50
				if CollectEnt(desc,curdata,force) then return RunCurrentQuest(count+1) end
				nname,map,insid = desc:match("%[.-,(.-),(.-),(.-)%]")
				nname = GetQuestName(nname)
				if msk.api.MoveToNpc(map,insid) then
					TrySubmitNpcTalk(qid,id,nname,desc)
					sleep(2)
				end	
			elseif tp == 7 then--NPC对话
				cmd,nname,map,insid = desc:match("%[(.-),(.-),(.-),(.+)%]")
				nname = GetQuestName(nname)
				cmd = cmd:upper()
				if qid == 0x7f76 and pl:IsNear(182100,145100) then
					msk.api.FlyMap(5)--传送点 通往天香谷
					sleep(3)
				elseif qid == 0x7f73 and pl:IsNear(175800,71400) then
					if msk.api.NpcTalkCompleteByName("柳扶风",1011901001,0,0,0,0,0,0) then
						sleep(15)
					end
				end
				if cmd == "PATHNPCLAYER" then--很多层 比如桥
					if desc == '[PathNpcLayer,白衣人,10002,2413,1]' then
						if msk.api.MoveToPosition(1035,890,nil,nil,1) then
							SubmitQuest(id,nname,qid,qidx)
						end
					else
						insid,y = insid:match("(%d+),(%d+)")
						if msk.api.MoveToNpc(map,insid) then
							SubmitQuest(id,nname,qid,qidx)
						end
					end
				else		
					if msk.api.MoveToNpc(map,insid) then
						--SubmitQuestByName(nname,qid,qidx)
						--LogDebuging("xx %d-%d",map,insid)
						SubmitQuest(id,nname,qid,qidx)
					end
				end
			elseif tp == 8 then--坐标
				cmd,nname,map,insid = desc:match("%[(.-),(.-),(.-),(.+)%]")
				nname = GetQuestName(nname)
				cmd = cmd:upper()
				LogDebuging("%s:%s",cmd,insid)
				if cmd == "PATHNPC" or cmd == "PATHAREA" then
					msk.api.MoveById(map,insid)
				else
					x,y = insid:match("(%d+),(%d+)")
					if x and y then
						msk.api.MoveToPosition(x,y,nil,nil,nil,nil,map)
					end
				end
			elseif tp == 10 then--采集
				CollectEnt(desc,curdata,true)
				return RunCurrentQuest(count+1)
			elseif tp == 19 then--??
				if desc == "与[PathNpc,叶知秋,10011,333]对话" then--tp = 19
					if GetMainPlayer().z < 20000 then
						if msk.api.MoveToPosition(1466,2819) then
							msk.api.NpcTalkCompleteByName("上官小仙",1010501901,0,0,0,0,0,0)
							sleep(5)
						else
						LogWarning("无法上塔找 叶知秋:与[PathNpc,叶知秋,10011,333]对话")
						end
					end
					msk.api.NpcTalkCompleteByName("叶知秋",1010500706,0,0,0,0,0,0)
				end
			elseif tp == 20 then--等时间就好了
				local cqid
				if qs.CheckHasBuff(0x2b09) then
					UseSkill(0x10e822)
					sleep(1)
				end
				UseSkill(0x201ac1)
				for i=1,120,5 do
					sleep(5)
					cur_group_id = qs.GetCurrentQuestTrackGroupId()
					cur_state = qs.GetQuestGroupInfo(cur_group_id)
					cqid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
					if cqid ~= qid then
						break
					end
				end
			elseif tp == 21 then--剧情副本
				if msk.api.GetCurrentEventID() ~= id then
					cmd,nname,map,insid = desc:match(".*%[(.-),(.-),(.-),(.+)%]")
					nname = GetQuestName(nname)
					cmd = cmd:upper()
					if cmd == "MAP_X_Y" then
						x,y = insid:match("(%d+),(%d+)")
						msk.api.MoveToPosition(x,y)
					elseif cmd == "PATHNPC" then
						if msk.api.MoveToNpc(map,insid) then
							TrySubmitNpcTalk(qid,id,nname,desc)
						end
					elseif cmd == "PATHAREA" then
						--[PathArea,,夏荷翠池,10002,300035] 写歪了的
						if tonumber(insid) == nil then
							map,insid = insid:match("(%d+),(%d+)")
						end
						msk.api.MoveById(map,insid)
					else
						error("不支持的指令:"..cmd,0)
					end
					sleep(1)
				end
				LogDebuging("开始剧情:%d-%s",id,qs.GetEventName(id))
				return RunCurDesc()
			elseif tp == 26 then --选择身份
				msk.api.TransferIdentity(msk.config.identity)
				sleep(5)
			elseif tp == 28 then --界限突破
				sleep(5)
				cur_state = qs.GetQuestGroupInfo(cur_group_id)
				local cqid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
				if cqid == qid then
					ScriptExit("界限突破")
				end
			else
				ScriptExit("不支持的任务类型:"..tp)
				--error("不支持的任务类型:"..tp,0)
			end
			if not monsters then
				sleep(1)
				return RunCurrentQuest(count+1)
			end
		end
		idx = idx + 1
	end
	if monsters then
		Heal()
		for i=1,#monsters do
			if msk.api.MoveById(monsters[i][1],monsters[i][2]) then
				--while msk.dword(monsters[i][4]) < monsters[i][3] do
				while IsQuestSlotComplete(qid,monsters[i][3]) == false do
					msk.api.Fight(monsternames)
					sleep(1)
				end
			end
		end
	end
	if not bRun then
		sleep(5)
	end
	return RunCurrentQuest(count+1)
end
	
local function UseItemByQuestPos(itemName,map,qpos,tarname)	
	if tarname and msk.api.MoveToNpc(map,qpos) then 
		local npc = msk.npc.GetByName(tarname)
		if npc and msk.api.MoveToPosition(npc.x,npc.y,true) then
			msk.npc.GetMainPlayer():Select(npc.id1,npc.id2)
			return  msk.item.UseItemByName(itemName)
		end
	else
		return msk.api.MoveToNpc(map,qpos) and msk.item.UseItemByName(itemName)
	end
	return false
end
local function TalkWithNpcByQuestPos(nid,nname,qid,qidx,map,qpos,layer)
	g_cacheData.nearListDis = 300
	local ret = msk.api.MoveById(map,qpos,layer) and SubmitQuest(nid,nname,qid,qidx)
	g_cacheData.nearListDis = nil
	return ret
end
local function TalkWithNpcByMapPos(nid,nname,qid,qidx,x,y,map,layer)
	return msk.api.MoveToPosition(x,y,nil,nil,layer,nil,map) and SubmitQuest(nid,nname,qid,qidx)
end
local function NpcFunTalkByMapPos(npcname,id,idx1,idx2,idx3,idx4,idx5,idx6,x,y,map,h)
	if h and h*100 <= GetMainPlayer().z then return true end
	return msk.api.MoveToPosition(x,y,nil,nil,nil,nil,map) and msk.api.NpcTalkCompleteByName(npcname,id,idx1,idx2,idx3,idx4,idx5,idx6)
end
local function NpcFunTalkByQuest(qid,id,nname,desc,map,qpos)
	return msk.api.MoveToNpc(map,qpos) and TrySubmitNpcTalk(qid,id,nname,desc)
end
local function NpcFunTalkByQuestAndFight(qid,id,nname,desc,map,qpos,tnpcName)
	local mapMap,smap = msk.api.GetCurrentMapId(true)
	if smap == map then
		msk.api.MoveToNpc(map,qpos) 
		TrySubmitNpcTalk(qid,id,nname,desc)
		sleep(1)
	end
	if smap ~= map then
		return msk.api.Fight(tnpcName)
	end
	return false
end
local function MoveByMapPos(x,y,map,layer)
	return msk.api.MoveToPosition(x,y,nil,nil,layer,nil,map)
end
local function FightNpcByQuestPos(monsters,map,qpos,layer)
	Heal()
	if layer == nil then
		local npc = msk.npc.GetNpcByName(monsters)
		if npc then return msk.api.FightNpc(npc),0.5 end
	end
	return msk.api.MoveById(map,qpos,layer) and msk.api.Fight(monsters),0.5
end
local function GeneralTestByQuestPos(npcname,map,qpos,slid)
	if msk.api.MoveById(map,qpos) then
		local npc = msk.npc.GetNpcByName(npcname)
		if npc then
			local preId = g_TestPre[slid]
			if preId then
				msk.api.NpcTalkCompleteByName(npcname,preId,0,0,0,0,0,0)
				sleep(1)
			end
			msk.api.GeneralTest(npc.id1,npc.id2,slid)
			return true
		end
	end
	return false
end
local function FightNpcByMapPos(monsters,x,y,map,layer)
	Heal()
	local npc = msk.npc.GetNpcByName(monsters)
	if npc then return FightNpc(npc),0.5 end
	return msk.api.MoveToPosition(x,y,nil,nil,layer,nil,map) and Fight(monsters),0.5
end

local function CollectByQuestPos(nname,map,qpos)
	local npc = msk.npc.GetEntityByName(nname)
	if npc and msk.api.MoveToPosition(npc.x,npc.y,true,nil,nil,2) then
		Collect(npc.id1,npc.id2)
		sleep(4.5)
		return true
	end
	if msk.api.MoveById(map,qpos) then
		npc = msk.npc.GetEntityByName(nname)
		if npc and msk.api.MoveToPosition(npc.x,npc.y,true,nil,nil,2) then
			Collect(npc.id1,npc.id2)
			sleep(4.5)
			return true
		end
	end
	return false
end

local function CollectByMapPos(nname,x,y,map,layer)
	local npc = msk.npc.GetEntityByName(nname)
	if npc and msk.api.MoveToPosition(npc.x,npc.y,true,nil,nil,2) then
		Collect(npc.id1,npc.id2)
		sleep(4.5)
		return true
	end
	if msk.api.MoveToPosition(x,y,nil,nil,layer,2,map) then
		npc = msk.npc.GetEntityByName(nname)
		if npc and msk.api.MoveToPosition(npc.x,npc.y,true,nil,nil,2) then
			Collect(npc.id1,npc.id2)
			sleep(4.5)
			return true
		end
	end
	return false
end

local function WaitUntilQuestComplete(qid)
	local cqid,cur_group_id,cur_state
	--先下马
	if qs.CheckHasBuff(0x2b09) then
		UseSkill(0x10e822)
		sleep(1)
	end
	for i=1,120,5 do
		if not qs.IsPlayerInBattle() and not qs.CheckHasBuff(0x44e) then
			UseSkill(0x201ac1)
		end
		sleep(5)
		cur_group_id = qs.GetCurrentQuestTrackGroupId()
		cur_state = qs.GetQuestGroupInfo(cur_group_id)
		cqid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
		if cqid ~= qid then
			return true
		end
	end
	return false--太久了 2分钟了
end

local function FlyToMapPos(pos)
	return msk.api.FlyMap(pos),3
end

local function LimitBreak(qid)
	sleep(5)
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local cqid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	if cqid == qid then
		ScriptExit("界限突破")
	end
	return true
end

local function LvLimit(qid)
	sleep(5)
	local cur_group_id = qs.GetCurrentQuestTrackGroupId()
	local cur_state = qs.GetQuestGroupInfo(cur_group_id)
	local cqid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
	if cqid == qid then
		ScriptExit("等级限制")
	end
	return true
end

local function NpcFunTalk(npcname,id,idx1,idx2,idx3,idx4,idx5,idx6,x,y)
	if x and y and not GetMainPlayer():IsNear(x*100,y*100) then return false end
	return msk.api.NpcTalkCompleteByName(npcname,id,idx1,idx2,idx3,idx4,idx5,idx6)
end
local function MakeEquip(tp)
	local iid = msk.api.GetIdentityId()
	if iid == 0 then
		msk.api.TransferIdentity()
		iid = msk.api.GetIdentityId()
	end
	if iid == 0 then
		ScriptExit("无法获取身份")
	end
	if iid == qs.IDENTITY_TYPE_LIE_HU then
		__LearnIdentitySkill(iid,1,1)--制药
		sleep(0.5)
		__LearnIdentitySkill(iid,3,1)--初级衣带
		sleep(0.5)
		WaitMoneyProtect()
		UIShowWindow("QSUIManufacturePanel",true)
		local manuFacturePane = msk.ui.GetPaneByName("QSUIManufacturePanel",true)
		msk.qcall(CallManuFacture,manuFacturePane,0,0,0,0,0x1b59)--洗影青莲带
		sleep(3)
		UIShowWindow("QSUIManufacturePanel",false)
	elseif iid == qs.IDENTITY_TYPE_SHA_SHOU then
		__LearnIdentitySkill(iid,1,1)--制药
		sleep(0.5)
		__LearnIdentitySkill(iid,4,1)--初级项链 :2
		sleep(0.5)
		WaitMoneyProtect()
		UIShowWindow("QSUIManufacturePanel",true)
		local manuFacturePane = msk.ui.GetPaneByName("QSUIManufacturePanel",true)
		msk.qcall(CallManuFacture,manuFacturePane,0,0,0,0,0x2af9)--洗影青莲带
		sleep(3)
		UIShowWindow("QSUIManufacturePanel",false)
	else
		ScriptExit("不支持的身份")
	end
	
	return true
end
local function StrongEquip()
	do return msk.api.JingLian() end
	local pos = msk.item.SplitItemByName("1级精金石")
	if pos == nil then 
		ScriptExit("需要1级精金石")
	end
	if msk.api.StrongEquip() then
		sleep(0.3)
		return true
	end
	return false
end

local function XinfaZhuangBei()
	msk.item.CheckBag()
	msk.api.EquipXinFa(0)
	sleep(0.3)
	return true
end

local function XinfaTuPo()
	LearXinFaTianFu(6)
	sleep(0.5)
	LearXinFaTianFu(1)
	sleep(0.5)
	LearXinFaTianFu(4)
	sleep(0.5)
	msk.api.TuPoXinFa(0)
	sleep(0.5)
	return true
end

local function TutrialBreakEquip()
	msk.item.CheckBag(true)
	sleep(1)
	return true
end

local function JingMaiChongXue()
	msk.api.LearNormalAcupoint()
	sleep(1)
	return true
end

local call_FunTable = {
	["任务点对话"] = TalkWithNpcByQuestPos,
	["坐标点对话"] = TalkWithNpcByMapPos,
	["坐标点功能对话"] = NpcFunTalkByMapPos,
	["任务功能对话"] = NpcFunTalkByQuest,
	["任务功能对话_战斗"] = NpcFunTalkByQuestAndFight,
	["任务点移动"] = msk.api.MoveById,
	["坐标点移动"] = MoveByMapPos,
	["任务点杀怪"] = FightNpcByQuestPos,
	["任务点普通试炼"] = GeneralTestByQuestPos,
	["坐标点杀怪"] = FightNpcByMapPos,
	["任务点采集"] = CollectByQuestPos,
	["坐标点采集"] = CollectByMapPos,
	["等待任务完成"] = WaitUntilQuestComplete,
	["界限突破"] = LimitBreak,
	["等级限制"] = LvLimit,
	["御风神行"] = FlyToMapPos,
	["NPC对话"] = NpcFunTalk,
	["身份选择"] = msk.api.TransferIdentity,
	["任务点使用物品"] = UseItemByQuestPos,
	["加入盟会"] = function (...) return ChooseManor(...) end,
	["教学任务-制造装备"] = MakeEquip,
	["教学任务-装备分解"] = TutrialBreakEquip,
	["教学任务-装备精工"] = StrongEquip,
	["教学任务-心法装备"] = XinfaZhuangBei,
	["教学任务-心法突破"] = XinfaTuPo,
	["教学任务-经脉冲穴"] = JingMaiChongXue,
}

function CALL(cmd,...)
	LogDebuging("开始执行功能:%s",cmd)
	if not call_FunTable[cmd](...) then
		LogWarning("执行功能失败:%s",cmd)
		sleep(2)
	else
		LogDebuging("执行功能成功:%s",cmd)
	end
end

function RunAllQuests(stopid)
	_G.CALL = CALL
	local f,err = mskg.loadfile("quests")
	if not f then
		f,err = mskg.loadfile("quests_t")
	end
	if not f then
		LogDebuging("load quests failed:%s",err)
		ScriptExit("任务加载失败")
	end
	f()
	local cur_group_id,cur_state,tid,qnode
	local GetCurrentEventID = msk.api.GetCurrentEventID
	local IsSkipEvent = msk.api.IsSkipEvent
	local GetQuestNode = msk.quest.GetQuestNode
	local data,rundatanode,rundata,name,subdata_hd,subdata_ed,idx,sidx
	local curdata,qidx,tp,curnum,num,desc,id
	local tnname,cmd,nname,map,insid,x,y
	local id2,id3,monsters,monsternames,bRun
	local pl,weaPon,lastqid
	local repairCount = 0
	while true do
		if g_cacheData.skipNpcNum ~= 0 then
			g_cacheData.skipNpcAddrs = {}
			g_cacheData.skipNpcNum = 0
		end
		if stopid and (qs.IsQuestAccepted(stopid) or qs.IsQuestCommitted(stopid)) then
			ScriptExit("指定任务终止")
		end
		msk.item.CheckBag()--每次做完任务 都处理一下背包
		if GetCurrentEventID() ~= -1 and not IsSkipEvent(GetCurrentEventID()) then
			msk.quest.RunCurDesc()
		else
			cur_group_id = qs.GetCurrentQuestTrackGroupId()
			cur_state = qs.GetQuestGroupInfo(cur_group_id)
			tid = cur_state.include_qiaoduan and cur_state.current_qiaoduan_id or cur_state.current_quest_id
			weaPon = msk.item.GetEquipByPos(0)
			if weaPon and weaPon.dura <= 50 and (lastqid ~= tid or repairCount < 15) then--耐久太低
				LogDebuging("就近修理装备:%d",weaPon.dura)
				msk.api.RepairAtNearlistNpc()
				if lastqid == tid then
					repairCount = repairCount + 1
				else
					repairCount = 0
				end
			end
			if lastqid ~= tid then
				PlayerDataSet("lastQuestTm",os.time())
			end
			lastqid = tid
			qnode = GetQuestNode(tid)
			if qnode == nil then
				LogWarning("任务数据未找到%d!!",tid)
				sleep(3)
			else
				if tid == stopid then ScriptExit("指定任务终止") end
				data = msk.dword(qnode+4)
				rundatanode = msk.dword(data+0x24)
				curdatanodehd = msk.dword(data+0x2c)
				rundata = msk.dword(rundatanode+0xc)
				
				name = msk.str(msk.dword(rundata+0x2c))
				LogDebuging("任务开始:%d %s",tid,name)
				
				subdata_hd = msk.dword(rundata+0x14)
				subdata_ed = msk.dword(rundata+0x18)
				idx = 0
				sidx = 1
				--pl = GetMainPlayer()
				for i=subdata_hd,subdata_ed-1,0x2c do
					qidx = msk.dword(i)
					tp = msk.dword(i+4)
					id = msk.dword(i+8)
					id2 = msk.dword(i+0xc)
					id3 = msk.dword(i+0x10)
					desc = msk.str(msk.dword(i+0x18))
					curdata = msk.dword(curdatanodehd+idx*4)
					if msk.dword(curdata) < msk.dword(curdata+4) then
						LogDebuging("任务目标开始:%d %d %s",qidx,tp,desc or "nil")
						if g_speciaQuestBefireRun[tid] and g_speciaQuestBefireRun[tid][tp] then
							LogDebuging("特殊任务处理")
							g_speciaQuestBefireRun[tid][tp](tid,msk.npc.GetMainPlayer(),desc)
						end
					end
					local ret,flag
					for i=1,20 do
						if msk.dword(curdata) < msk.dword(curdata+4) and (GetCurrentEventID() == -1 or IsSkipEvent(GetCurrentEventID())) then
							ret,flag = g_allQuests[tid](qidx)
							flag = flag or 1.5
							sleep(flag)
						else
							break
						end
					end
					idx = idx + 1
				end
				
			end
		end
		sleep(1)
	end
	ScriptExit("任务成功")
end


function RunTargetQuests(qid)
	if qs.IsQuestCommitted(qid) or qs.IsQuestAccepted(qid) == false then return false end
	_G.CALL = CALL
	if g_allQuests == nil then
		local f,err = mskg.loadfile("quests")
		if not f then
			f,err = mskg.loadfile("quests_t")
		end
		if not f then
			return false
		end
		f()
	end
	local cur_group_id,cur_state,tid,qnode
	local GetCurrentEventID = msk.api.GetCurrentEventID
	local IsSkipEvent = msk.api.IsSkipEvent
	local GetQuestNode = msk.quest.GetQuestNode
	local data,rundatanode,rundata,name,subdata_hd,subdata_ed,idx,sidx
	local curdata,qidx,tp,curnum,num,desc,id
	local tnname,cmd,nname,map,insid,x,y
	local id2,id3,monsters,monsternames,bRun
	local pl
	for i=1,300 do
		if g_cacheData.skipNpcNum ~= 0 then
			g_cacheData.skipNpcAddrs = {}
			g_cacheData.skipNpcNum = 0
		end
		msk.item.CheckBag()--每次做完任务 都处理一下背包
		if GetCurrentEventID() ~= -1 and not IsSkipEvent(GetCurrentEventID()) then
			msk.quest.RunCurDesc()
		else
			tid = qid
			qnode = GetQuestNode(tid)
			if qnode == nil then
				LogWarning("任务数据未找到%d!!",tid)
				sleep(3)
			else
				data = msk.dword(qnode+4)
				rundatanode = msk.dword(data+0x24)
				curdatanodehd = msk.dword(data+0x2c)
				rundata = msk.dword(rundatanode+0xc)
				
				name = msk.str(msk.dword(rundata+0x2c))
				LogDebuging("任务开始:%d %s",tid,name)
				
				subdata_hd = msk.dword(rundata+0x14)
				subdata_ed = msk.dword(rundata+0x18)
				idx = 0
				sidx = 1
				--pl = GetMainPlayer()
				for i=subdata_hd,subdata_ed-1,0x2c do
					qidx = msk.dword(i)
					tp = msk.dword(i+4)
					id = msk.dword(i+8)
					id2 = msk.dword(i+0xc)
					id3 = msk.dword(i+0x10)
					desc = msk.str(msk.dword(i+0x18))
					curdata = msk.dword(curdatanodehd+idx*4)
					if msk.dword(curdata) < msk.dword(curdata+4) then
						LogDebuging("任务目标开始:%d %d %s",qidx,tp,desc or "nil")
						if g_speciaQuestBefireRun[tid] and g_speciaQuestBefireRun[tid][tp] then
							LogDebuging("特殊任务处理")
							g_speciaQuestBefireRun[tid][tp](tid,msk.npc.GetMainPlayer(),desc)
						end
					end
					local ret,flag
					for i=1,20 do
						if msk.dword(curdata) < msk.dword(curdata+4) and (GetCurrentEventID() == -1 or IsSkipEvent(GetCurrentEventID())) then
							ret,flag = g_allQuests[tid](qidx)
							flag = flag or 1.5
							sleep(flag)
						else
							break
						end
					end
					idx = idx + 1
				end
				
			end
		end
		sleep(1)
		if qs.IsQuestCommitted(qid) or qs.IsQuestAccepted(qid) == false then break end
	end
	return true
end


LogDebuging("msk.quest ok")

--[[
QSQuestInstanceManager
+0x20 head
+0x24 max_num
+0x28 num

QuestNode
+0 id
+4 data
+0x8 next

QuestData
+0x18 id
+0x1c 任务状态
+0x24 QuestContent
+2c 已经完成的状态

QuestContent
+4 id
+8 QuestData
+0xc QuestDesc

QuestDesc
+0x2c name
+0x30 ?

QuestGroup
+0x8 questbegin
+0xc questend
+0x18 id
+0x1c name

QSQuestInstanceManager 基准
+0x4 QSQuestTrigManager
+0xc QSQuestGroupManager
+0x10 QSJianwenManager
--]]