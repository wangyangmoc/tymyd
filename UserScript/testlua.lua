local data = string.char(0x1,0x2,0x3,0x25,0x27,1,1,0x25,0x27,0,0)
local sig = "25 27 00 00"


------------特征码字串转换为lua string-------------
local keyWord = {"(",")",".","%","+","-","*","?","[","]","^","$"}
local function IsPatkeyWord(c)
  for _,v in ipairs(keyWord) do--替换掉lua的模式关键字 不然lua会按照模式匹配进行查找 这个版本暂不支持模式匹配
		if c == v then
      return true
    end
	end
  return false
end
function transSignatureToByte(sig)
	sig = string.gsub(sig,' ','')--去掉空格
	if false and sig:len()%2 ~= 0 then 
		error('特征码长度必须为2的倍数')
	else
		local str = ""
    local c,hx
	local strLen
    local ssig = ""
		for i= 1,sig:len() do
      c = sig:sub(i,i)
      if IsPatkeyWord(c) == false then--非模式匹配
        ssig = ssig..c
        if #ssig == 2 then
          hx = tonumber("0x"..ssig)
          ssig = ""
          c = string.char(hx)
          if hx == 0 then
            c = "%z"
          elseif IsPatkeyWord(c) then
            c = '%'..c
          end
          str = str..c
        end
      else
          str = str..c
      end
		end
    assert(ssig == "",'特征码长度必须为2的倍数') 
		--local strLen = str:len()
		-- for _,v in ipairs(keyWord) do--替换掉lua的模式关键字 不然lua会按照模式匹配进行查找 这个版本暂不支持模式匹配
			-- local f = string.find(str,"%"..v)
			-- str = string.gsub(str,"%"..v,"%%%"..v)
		-- end
		return str,strLen
	end
end

sig = transSignatureToByte(sig)
print(string.find(data,sig))