-----------------------------
-------游戏事件触发---------
-----------------------------
--进入地图
local __lastMapName
local function OnEnerMap(mapName)
	g_changingMap = false
	local mapId = qs.GetMapId(mapName)
	local realMapName= qs.GetMapRealName(mapId)
	LogDebuging("进入地图:%s-%s",mapName,realMapName)
	--开始处理
	if __lastMapName == "ActorCreationScreen" then--选择角色界面
		g_cacheData.logTime = os.time()
		--statusCheckThread = statusCheckThread or msk.newThread(StatusCheck)
	elseif realMapName == "杭州" then
	end
	__lastMapName = mapName
	msk.fireEvent("enterMap",mapName,realMapName)
	g_cacheData.manPlayer = nil
	msk.npc.GetMainPlayer()
end
--退出地图
local function OnLeaveMap(mapName)
	g_changingMap = true
	local mapId = qs.GetMapId(mapName)
	local realMapName= qs.GetMapRealName(mapId)
	LogDebuging("离开地图:%s-%s",mapName,realMapName)
	msk.fireEvent("leaveMap",mapName,realMapName)
	--g_cacheData.manPlayer = nil
end
--升级
local function OnLevelUp(lv)
	--处理一些东西
	return false
end
--任务剧情更新
local function OnQuest_gene_trace_text(quest_info) 
	--LogDebuging("新任务数据:%s",event_info.qiaoduan_desc or "nil")
	_G.g_questInfo = quest_info 
	_G.g_questId = nil
end
--任务更新
local function OnQuest_begin_request_desc(quest_id) 
	--LogDebuging("新剧情ID:%d",tonumber(quest_id) or -1)
	_G.g_questInfo = nil 
	_G.g_questId = quest_id
end
--触发见闻
local function OnQuest_enter_event(event_id) 
	LogDebuging("触发见闻:%d %s",tonumber(event_id) or 0,qs.GetEventName(event_id))
	_G.g_eventId = event_id
end
--见闻结束
local function OnQuest_leave_event(event_id) 
	LogDebuging("退出见闻:%d %s",tonumber(event_id) or 0,qs.GetEventName(event_id))
	_G.g_eventId = nil
end

g_listenList = g_listenList or {}
local function _ListListenEvent(name,fun)
	if g_listenList[name] then return end
	ListenEvent(name,fun)
end
--注册监听函数

_ListListenEvent("Event.Logic.LevelLogic.Start", OnEnerMap)
_ListListenEvent("Event.Logic.LevelLogic.End", OnLeaveMap)
msk.hook.HookFunction(GameConfig.LevelUp,"OnLevelUp",OnLevelUp)

--下面是任务相关的监听
msk.hook.HookFunction(Logic.GamePlay.Quest,"gene_trace_text",OnQuest_gene_trace_text)
msk.hook.HookFunction(Logic.GamePlay.Quest,"begin_request_desc",OnQuest_begin_request_desc)
msk.hook.HookFunction(Logic.GamePlay.Quest,"enter_event",OnQuest_enter_event)
msk.hook.HookFunction(Logic.GamePlay.Quest,"leave_event",OnQuest_leave_event)

printf("call back ok")