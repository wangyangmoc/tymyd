

local skipQs = {
	GetCurrentEventID = true,
	CheckCurrentMap = true,
	GetMapName = true,
	GetMapRealName = true,
	--链接提示
	SetCommonTipsWidth = true,
	SetCommonTipsName = true,
	AddCommonTipsText = true,
	CompleteCommonTips = true,
	GetSpellName = true,
	GetItemName = true,
	--AudioModifyParam = true,
	--FamilyCanDutyBeSet = true,
	--DgnEventTestFx = true,
	--GetLastOnlineTime = true,
	--fog = true,
	--test_define = true,
	-- SetCustomPanelSize = true,
	-- CheckIsChineseGBK = true,
	-- FamilyGetZDGuaKaoState = true,
	-- SetCustomPanelTitle = true,
	-- AttachPetBuff = true,
	-- QSPrint = true,
	-- AddTreasureDetection = true,

}

local skipQuest = {
	get_event_request_monsters = true,
	IsDataEmpty = true,
}

local skipYunBiao = {
	
	is_in_npc_jiebiao_plane = true,
	is_in_single_yabiao_plane = true,
	is_in_multi_yabiao_plane = true,
	is_in_raid_yabiao_plane = true,
	is_in_monor_yabiao_plane = true,
	
	get_jiebiao_exit_plane = true,
	get_jiebiao_rob_all_dead = true,
	get_yabiao_player_all_dead = true,
	
	is_in_event_array = true,
	is_in_player_jiebiao_plane = true,
	is_in_event_array = true,
	is_in_jiebiao_plane = true,
	
	get_jiebiao_out_of_range_warning = true,
	get_yabiao_out_of_range_warning = true,
	is_jiebiao = true,
	is_yabiao = true,
	
	is_in_yabiao_plane = true,
	
	get_yabiao_countdown = true,
	get_jiebiao_ready_countdown = true,
	
}
local skipMail = {
	GetClearCacheCD = true,
}
local skipCQ = {
	update = true,
	update_list = true,
}
local skipTrackMgr = {
	Update = true,
	RemoveModule = true,
}
local skipDati = {
	RemoveDati = true,
	ModuleName = true,
}
local skipSU = {
	RemoveSeize = true,
	ModuleName = true,
}
local skipSkillTip = {
	SetSimpleSkillTips = true,
	HideSkillTips = true,
	GetLevel = true,
	GetChineseStr = true,
	GetChineseStr_Single = true,
	GetBlank = true,
	GetSkillValue = true,
	GetSkillType = true,
	GetValue = true,
	GetSimpleTips = true,
	SetDecoTips = true,
	SetXinfaLevelUpTips = true,
}
g_TableHooked = false
if not g_TableHooked then
	HookFunction(EventSystem,'fireEvent')
	HookTable(qs,skipQs,"qs")
	HookTable(_G,nil,"_G")
	--HookTable(Logic.GamePlay.Other,nil,"action")
	
	HookTable(Logic.GamePlay.QuestConfig,nil,"Logic.GamePlay.QuestConfig")
	HookTable(QSConfig.BuffTips,nil,"QSConfig.BuffTips")
	HookTable(QSConfig.CareerIconConfig,nil,"QSConfig.CareerIconConfig")
	HookTable(QSConfig.MenghuiTips,nil,"QSConfig.MenghuiTips")
	HookTable(QSConfig.PetUI3DAvatarConfig,nil,"QSConfig.PetUI3DAvatarConfig")
	HookTable(QSConfig.CreateCharacterTips,nil,"QSConfig.CreateCharacterTips")
	HookTable(QSConfig.DailyGamePlatData,nil,"QSConfig.DailyGamePlatData")
	HookTable(QSConfig.ShiLianConfig,nil,"QSConfig.ShiLianConfig")
	HookTable(AllianceDartUI,nil,"AllianceDartUI")
	HookTable(QuestUITemplate,nil,"QuestUITemplate")
	HookTable(GameConfig.LevelUp,nil,"GameConfig.LevelUp")
	HookTable(RaidDartUI,nil,"RaidDartUI")
	HookTable(QSConfig.EventRewardConfig,nil,"QSConfig.EventRewardConfig")
	HookTable(QSConfig.EventRewardConfig.EventRewardFuncs,nil,"QSConfig.EventRewardConfig.EventRewardFuncs")
	HookTable(QSConfig.SkillTips,skipSkillTip,"QSConfig.SkillTips")
	HookTable(QSConfig.TeamTips,nil,"QSConfig.TeamTips")
	HookTable(QSConfig.FamilyDazuoTips,nil,"QSConfig.FamilyDazuoTips")
	HookTable(QSConfig.Tips,nil,"QSConfig.Tips")
	HookTable(QSQA,nil,"QSQA")
	HookTable(QSConfig.GrowUpRecommendData,nil,"QSConfig.GrowUpRecommendData")
	HookTable(QSConfig.GiftWhiteList,nil,"QSConfig.GiftWhiteList")
	HookTable(QSConfig.HotUpdateConfig,nil,"QSConfig.HotUpdateConfig")
	HookTable(QSConfig.FamilyUICfg,nil,"QSConfig.FamilyUICfg")
	HookTable(QSConfig.MenghuiUICfg,nil,"QSConfig.MenghuiUICfg")
	HookTable(QSConfig.NetConfig,nil,"QSConfig.NetConfig")
	HookTable(QSConfig.TeamCfg,nil,"QSConfig.TeamCfg")
	HookTable(QSConfig.ShopConfig,nil,"QSConfig.ShopConfig")
	HookTable(NormalDartUI,nil,"NormalDartUI")
	HookTable(Logic.LevelLogic,nil,"Logic.LevelLogic")
	HookTable(SeizeUI,skipSU,"SeizeUI")
	HookTable(Logic.GamePlay.CommandQueue,skipCQ,"Logic.GamePlay.CommandQueue")
	HookTable(BangpaiQuestUI,nil,"BangpaiQuestUI")
	HookTable(QuestionUI,nil,"QuestionUI")
	HookTable(Logic.GamePlay.HuntingInfoUI,nil,"Logic.GamePlay.HuntingInfoUI")
	HookTable(HuodongQuestUI,nil,"HuodongQuestUI")
	HookTable(TeamTreasureUI,nil,"TeamTreasureUI")
	HookTable(CareerIcon,nil,"CareerIcon")
	HookTable(AnShaUI,nil,"AnShaUI")
	HookTable(ManorQuestUI,nil,"ManorQuestUI")
	HookTable(Logic.GamePlay.Quest,skipQuest,"Logic.GamePlay.Quest")
	HookTable(GamePlay.ai,nil,"GamePlay.ai")
	HookTable(Logic.GamePlay.SkillName,nil,"Logic.GamePlay.SkillName")
	HookTable(BattleFieldUI,nil,"BattleFieldUI")
	HookTable(Logic.Camera,nil,"Logic.Camera")
	HookTable(DatiUI,skipDati,"DatiUI")
	HookTable(Logic.GamePlay.UIColorConfig,nil,"Logic.GamePlay.UIColorConfig")
	HookTable(Logic.GamePlay.Other,nil,"Logic.GamePlay.Other")
	HookTable(Logic.GamePlay.TongtianTower,nil,"Logic.GamePlay.TongtianTower")
	HookTable(Logic.GamePlay.NpcTalk,nil,"Logic.GamePlay.NpcTalk")
	HookTable(ComposeMovieClip,nil,"ComposeMovieClip")
	HookTable(HuabenReward,nil,"HuabenReward")
	HookTable(Logic.GamePlay.Yunbiao,skipYunBiao,"Logic.GamePlay.Yunbiao")
	HookTable(QSConfig.FirstDoCfg,nil,"QSConfig.FirstDoCfg")
	HookTable(Logic.UICore,nil,"Logic.UICore")
	HookTable(QSConfig.MailCfg,skipMail,"QSConfig.MailCfg")
	HookTable(QSConfig.UIMovieResConfig,nil,"QSConfig.UIMovieResConfig")
	HookTable(QSConfig.LivenessCfg,nil,"QSConfig.LivenessCfg")
	HookTable(QSConfig.AcupointCfg,nil,"QSConfig.AcupointCfg")
	HookTable(QSConfig.EventComplete,nil,"QSConfig.EventComplete")
	HookTable(QSConfig.FamilyDazuoCfg,nil,"QSConfig.FamilyDazuoCfg")
	HookTable(QSConfig.LingxiCfg,nil,"QSConfig.LingxiCfg")
	HookTable(IdentityDailyQuestUI,nil,"IdentityDailyQuestUI")
	HookTable(QSConfig.MenghuiCfg,nil,"QSConfig.MenghuiCfg")
	HookTable(JiangYangDaDaoUI,nil,"JiangYangDaDaoUI")
	HookTable(JianwenUI,nil,"JianwenUI")
	HookTable(QSConfig.FamilyTips,nil,"QSConfig.FamilyTips")
	HookTable(QSConfig.UIHelp,nil,"QSConfig.UIHelp")
	HookTable(TrackMgr,skipTrackMgr,"TrackMgr")
	HookTable(MenghuiDailyQuestUI,nil,"MenghuiDailyQuestUI")
	HookTable(QSConfig.ScheduleUIHelper,nil,"QSConfig.ScheduleUIHelper")
	HookTable(QSConfig.GamePlayTips,nil,"QSConfig.GamePlayTips")
	HookTable(QSConfig.FamilyCfg,nil,"QSConfig.FamilyCfg")
	HookTable(QSConfig.LocalizationCfg,nil,"QSConfig.LocalizationCfg")
	HookTable(QSConfig.CareerTips,nil,"QSConfig.CareerTips")
	HookTable(QSConfig.ArenaTips,nil,"QSConfig.ArenaTips")
	HookTable(QSConfig.BigLingxiTips,nil,"QSConfig.BigLingxiTips")
	HookTable(ArenaDailyQuestUI,nil,"ArenaDailyQuestUI")
	HookTable(QSConfig.ItemTips,nil,"QSConfig.ItemTips")
	HookTable(QSConfig.AcupointTips,nil,"QSConfig.AcupointTips")
	HookTable(QSConfig.ShilianMapConfig,nil,"QSConfig.ShilianMapConfig")
	HookTable(QSConfig.SafeConfig,nil,"QSConfig.SafeConfig")
	
	g_TableHooked = true
end