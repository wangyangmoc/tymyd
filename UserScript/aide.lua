local sleep = msk.sleep
function msk.mcall(count,f,...)
	for i=1,count do
		if f(...) then return true end
	end
	return false
end

function WaitMoneyProtect()
	--do return end
	local ltm = g_cacheData.logTime
	if os.time() - ltm > 180 then return end
	LogDebuging("等待财产保护时间...")
	repeat
		sleep(5,true)
	until os.time() - ltm > 180
	msk.item.CheckBag()
end

function PlayerDataSet(k,v)
	g_client:hset(k,g_mainId,v)
end

function PlayerDataGet(k)
	g_client:hget(k,g_mainId)
end