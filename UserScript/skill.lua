module("msk.skill",package.seeall)
local print = _G.printf

local g_dataBase = g_sig.dataBase
local skillSlotOff = g_sig.skillSlotOff
local cdOff = 0xc4

local descOff = g_sig.skillStringOff.desc
local poemOff = g_sig.skillStringOff.poem
local nameOff = g_sig.skillStringOff.name
local iconOff = g_sig.skillStringOff.icon
local typeOff = g_sig.skillStringOff.type
--1016048C  12D99BC0  ASCII "QSSlotManager"
--011D43EA    8B0D E88A1202          mov ecx,dword ptr ds:[0x2128AE8]
local maxnum = 0
--鎵撳嵃鎶€鑳芥爲鐨勫彾瀛愯妭鐐?
local function PrintSkillLeave(node,ed)
	if node == 0 or node == ed then
		return
	end
	maxnum = maxnum + 1
	if maxnum > 10000 then return end
	PrintSkillLeave(msk.dword(node),ed)--鍙宠竟
	local id = msk.dword(node+0x10)
	local data = msk.dword(node+0x14)
	local name = msk.dword(data+nameOff)
	print("%x:%d:%x  %s",node,maxnum,id,name ~= 0 and msk.str(name) or "??")
	return PrintSkillLeave(msk.dword(node+4),ed)--宸﹁竟
end

local function PrintSkillInfo(node,ed,tid)
	if node == 0 or node == ed then
		return
	end
	local id = msk.dword(node+0x10)
	if tid == id then
		local data = msk.dword(node+0x14)
		local name = msk.dword(data+nameOff)
		print("%d:%x:%x %s %x",maxnum,id,node,name ~= 0 and msk.str(name) or "??",data)
	elseif tid > id then
		return PrintSkillInfo(msk.dword(node),ed,tid)--宸﹁竟
	elseif tid < id then
		return PrintSkillInfo(msk.dword(node+4),ed,tid)--鍙宠竟
	end
end

function PrintAllSkill(tid)--GetSpellName
	local skillTbl = msk.dwords(g_dataBase,0x98)
	local ed = skillTbl+0x54
	local head = msk.dword(skillTbl+0x5c)
	if tid then
		PrintSkillInfo(head,ed,tid)
	else
		PrintSkillLeave(head,ed)
	end
end
	
local function __FindIdByName(node,ed,tname)
	if node == 0 or node == ed then
		return
	end
	local data = msk.dword(node+0x14)
	local name = msk.dword(data+nameOff)
	name = msk.str(name)
	local id = msk.dword(node+0x10)
	printf("检测:%x %s",id,name)
	if name == tname then
		return id
	end
	return __FindIdByName(msk.dword(node),ed,tname) or __FindIdByName(msk.dword(node+4),ed,tname)
end	
	
function FindIdByName(tname)
	local _,skillOff = msk.gdata.GetDataBase("Data.Table.TitleCfg")
	skillOff = skillOff - 4
	local userSkillMgn = msk.dwords(g_dataBase,skillOff)
	local hd,ed = msk.dword(userSkillMgn),msk.dword(userSkillMgn+4)
	local id
	local maxnum = 500
	while hd ~= ed and maxnum > 0 do
		maxnum = maxnum - 1
		id = msk.dword(hd)
		if qs.GetSpellName(id) == tname then
			return id
		end
		hd = hd + 0xc
	end
	return 0
end

function PrintUserSkill()
	local _,skillOff = msk.gdata.GetDataBase("Data.Table.TitleCfg")
	skillOff = skillOff - 4
	local userSkillMgn = msk.dwords(g_dataBase,skillOff)
	local hd,ed = msk.dword(userSkillMgn),msk.dword(userSkillMgn+4)
	local id
	local maxnum = 500
	while hd ~= ed and maxnum > 0 do
		maxnum = maxnum - 1
		id = msk.dword(hd)
		PrintAllSkill(id)
		hd = hd + 0xc
	end
end

function Test()
	print("玩家技能")
	PrintUserSkill()
end

printf("msk.skill ok")


--[[
userSkillMgn
+0 head
+4 last
+4c head --宸插鎶€鑳絀D
+50 last

SkillNode 0xc澶у皬
+0 id

SkillTreeNode
+0 pre
+4 next
+0x10 id


local t = {
[6648] msk_td->send-172:65:0000000021E81000A70000000000008000BBFD0100000021E00100000000000000000000ECDD180014DE1800F8B19501FF8CDA0B40DE1800E80000000000000000
[6648] msk_td->send-172:65:0000000022E81000A80000000000008000BBFD0100000021E001000000000000000000009CF01800C4F01800F8B19501FF6CB40E40F01800E80000000000000000
[6648] msk_td->send-172:65:0000000021E81000A90000000000008000BBFD0100000021E001000000000000000000009CF01800C4F01800F8B19501FF6CB40E40F01800E80000000000000000

	0x10ccd2,
	0x10ccd3,
	0x10ceb2,
	0x10cebd,--跳得高
	0x10cf21,
	0x111b42,--矮一点
	0x10cfe8,
}
--]]