module("quest_group.qg1061",package.seeall)
name = "太极三清迎开元"
id = 1061
--归山寒暄-门内寒暄
[31200] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118047,
        id2 = 0,
        id3 = 0,
        pid = 30601,
        desc = "[PathNpc,凌玄,10007,81]",
	}
}
--归山寒暄-太极道场
[31242] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80032,
        id3 = 0,
        pid = 30601,
        desc = "[PathArea,太极道场,10007,300044]",
	}
}
--归山寒暄-师兄师弟
[31237] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118056,
        id2 = 0,
        id3 = 0,
        pid = 30601,
        desc = "[PathNpc,潇湘子,10007,195]",
	}
}
--归山寒暄-严厉师叔
[31201] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118015,
        id2 = 0,
        id3 = 0,
        pid = 30601,
        desc = "[PathNpc,一云子,10007,3]",
	}
}
--归山寒暄-拜见师尊
[31250] = {
	[1] = {
        idx = 4,
        type = 21,--21
        id1 = 16307,
        id2 = 0,
        id3 = 0,
        pid = 30601,
        desc = "[PathNpc,一云子,10007,3]",
	}
}
--太极三清-回禀师尊
[31203] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118013,
        id2 = 0,
        id3 = 0,
        pid = 30602,
        desc = "[PathNpc,张梦白,10007,2]",
	}
}
--太极三清-真武剑道
[31248] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118013,
        id2 = 0,
        id3 = 0,
        pid = 30602,
        desc = "[PathNpc,张梦白,10007,2]",
	}
}
--太极三清-悟剑之法
[31204] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118001,
        id2 = 0,
        id3 = 0,
        pid = 30602,
        desc = "[PathNPCLayer,笑道人,10007,1,1]",
	}
}
--添置新衣
[30603] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118034,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,萧萧,10007,4]",
	}
}
--循序渐进
[30604] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118043,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPCLayer,广宁子,10007,83,1]",
	}
}
--开元真意-巡视道场
[31243] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80066,
        id3 = 0,
        pid = 30605,
        desc = "[PathNPCLayer,巡视道场,10007,300423,1]",
	}
}
--开元真意-珠帘洞内
[31241] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16300,
        id2 = 0,
        id3 = 0,
        pid = 30605,
        desc = "[PathArea,真武道场,10007,300008]",
	}
}
--继续巡视
[30606] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80009,
        id3 = 0,
        desc = "[PathArea,道场,10007,300009]",
	}
}
--灵犀一动
[30607] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "在道场上打坐调息。",
	}
}
--再遇蔷薇
[30608] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118005,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,燕南飞,10007,7]",
	}
}
--云海有悟
[30609] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16306,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,燕南飞,10007,7]",
	}
}
--书海无涯
[30610] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16301,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,山海楼,10007,300012]",
	}
}
