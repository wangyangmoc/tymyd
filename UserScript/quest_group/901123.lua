module("quest_group.qg901123",package.seeall)
name = "??"
id = 901123
--不成气候
[32625] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20119030,
        id2 = 6,
        id3 = 0,
        desc = "[PathMon,天风流强兵,10002,102006]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20119031,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,天风流残部,10002,102012]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20119032,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,天风流余孽,10002,102030]",
	}
}
--青蓝冰水
[32627] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119019,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,唐青铃,10002,2407]",
	}
}
--悬灯结彩
[32628] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10119008,
        id2 = 5,
        id3 = 0,
        desc = "[PathEnt,花神灯,10002,40107]",
	}
}
--死性不改
[32629] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119019,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,唐青铃,10002,2407]",
	}
}
--携灯而返
[32630] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10119000,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,梁知音,10002,2401]",
	}
}
--话不投机
[32632] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119010,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,柳扶风,10002,2409]",
	}
}
