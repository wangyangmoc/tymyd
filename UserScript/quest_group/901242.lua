module("quest_group.qg901242",package.seeall)
name = "??"
id = 901242
--五毒天华
[36510] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10121037,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,草鬼婆,10006,1160]",
	}
}
--诡异内核
[36511] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121007,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,百里研阳,10006,1435]",
	}
}
--步履云间
[36512] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80048,
        id3 = 0,
        desc = "前往[PathArea,百草间,10006,300419]",
	}
}
--龙池飘香
[36513] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121032,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,柳凤兰,10006,1195]",
	}
}
--直上青云
[36514] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20121020,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,精锐弟子,10006,100067]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20121019,
        id2 = 8,
        id3 = 0,
        desc = "[PathMon,影月弟子,10006,100088]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20121018,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,青焰弟子,10006,100094]",
	}
}
--千日醉魂
[36515] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10121034,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,红料,10006,1192]",
	}
}
--红紫乱朱
[36516] = {
	[1] = {
        idx = 4,
        type = 1,--1
        id1 = 20121021,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,朱焰弟子,10006,100107]",
	}
	[2] = {
        idx = 5,
        type = 1,--1
        id1 = 20121022,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,苍焰弟子,10006,100112]",
	}
}
--研阳奉月
[36526] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10121006,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,百里研阳,10006,1167]",
	}
}
--蜃月乱谷
[36527] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20121017,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,莲月弟子,10006,100205]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20121016,
        id2 = 16,
        id3 = 0,
        desc = "[PathMon,黑焰弟子,10006,100212]",
	}
}
