module("quest_group.qg901003",package.seeall)
name = "??"
id = 901003
--辞行
[28840] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117044,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,35]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117045,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江匡,10003,36]",
	}
}
--杭州
[28841] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10010,
        id2 = 80122,
        id3 = 0,
        desc = "去杭州找[PathNpc,史晴龙,10010,300771]",
	}
}
--茶和鱼饵
[28848] = {
	[1] = {
        idx = 2,
        type = 3,--3
        id1 = 10501032,
        id2 = 1,
        id3 = 0,
        desc = "找[PathNpc,淘淘,10003,220]要鱼饵",
	}
	[2] = {
        idx = 3,
        type = 10,--10
        id1 = 10101012,
        id2 = 1,
        id3 = 0,
        desc = "给[PathNpc,炉灶,10003,40025]添火",
	}
}
--回复帮主
[28849] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10117035,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江匡,10003,19]",
	}
}
--参见帮主
[28891] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80005,
        id3 = 0,
        desc = "[Map_X_Y,隐湖,10003,1279,916]",
	}
}
--回禀内情
[28892] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117035,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江匡,10003,19]",
	}
}
--汇聚
[28894] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117036,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,莫奇,10003,32]",
	}
}
--赶赴
[28895] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80006,
        id3 = 0,
        desc = "去往[Map_X_Y,红梅小筑,10003,945,647]",
	}
}
--硬闯
[28896] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20117020,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹松堂高手,10003,100172]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20117021,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹松堂弟子,10003,100172]",
	}
}
--奔赴
[28897] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80007,
        id3 = 0,
        desc = "[Map_X_Y,前往丐祖祠,10003,1409,728]",
	}
}
--摆脱
[28898] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 201170341,
        id2 = 5,
        id3 = 0,
        desc = "[PathMon,竹抱堂高手,10003,100285]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20117033,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹抱堂精锐,10003,100285]",
	}
	[3] = {
        idx = 3,
        type = 1,--1
        id1 = 20117034,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹抱堂弟子,10003,100285]",
	}
}
--摆脱
[29983] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80002,
        id3 = 0,
        desc = "[PathNpc,循声寻人,10003,32]",
	}
}
