module("quest_group.qg23",package.seeall)
name = "盟会建造（非随机）"
id = 23
--采集石料-寻觅巨石
[21000] = {
	[1] = {
        idx = 0,
        type = 10,--10
        id1 = 10104513,
        id2 = 1,
        id3 = 0,
        pid = 42301,
        desc = "寻觅[PathEnt,秦川巨石,10009,40311](每天前10个任务可以获得4倍奖励，建筑经验不翻倍)",
	}
}
--采集石料-采集巨石
[21001] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10104514,
        id2 = 1,
        id3 = 0,
        pid = 42301,
        desc = "拾取石料",
	}
}
--寻觅良材-寻觅良材
[21002] = {
	[1] = {
        idx = 0,
        type = 10,--10
        id1 = 10104515,
        id2 = 1,
        id3 = 0,
        pid = 42303,
        desc = "寻觅[PathEnt,云滇良材,10006,40113](每天前10个任务可以获得4倍奖励，建筑经验不翻倍)",
	}
}
--寻觅良材-采集良材
[21003] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10104516,
        id2 = 1,
        id3 = 0,
        pid = 42303,
        desc = "拾取良材",
	}
}
