module("quest_group.qg901243",package.seeall)
name = "??"
id = 901243
--枫香圣露
[36517] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10121033,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpcLayer,蒙天方,10006,1194,1]",
	}
}
--事不宜迟
[36518] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80051,
        id3 = 0,
        desc = "前往[PathArea,六圣居后寨,10006,300422]",
	}
}
--追风逐日
[36519] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20121024,
        id2 = 3,
        id3 = 0,
        desc = "[PathMon,苍月刺客,10006,100139]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20121023,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,赤焰刺客,10006,100145]",
	}
}
--奇局腹地
[36520] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80052,
        id3 = 0,
        desc = "前往[PathArea,六圣居后寨,10006,300423]",
	}
}
--蜃楼海市
[36521] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20121027,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,猎手精锐,10006,100154]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20121026,
        id2 = 8,
        id3 = 0,
        desc = "[PathMon,莲月猎手,10006,100162]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20121025,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,青焰猎手,10006,100157]",
	}
}
--御风神行
[36522] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121000,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,方玉峰,10006,1050]",
	}
}
--怅然若失
[36523] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10121008,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,百里研阳,10006,1441]",
	}
}
--宛若新生
[36524] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10121072,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,蓝奉月,10006,2149]",
	}
}
--拜过掌门
[36525] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121000,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,方玉峰,10006,1050]",
	}
}
