module("quest_group.qg1001",package.seeall)
name = "回师门路遇挑衅"
id = 1001
--君山脚下
[28801] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117001,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,李阳白,10003,1]",
	}
}
--寻衅上门
[28802] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80054,
        id3 = 0,
        desc = "[PathNpc,前去比试,10003,26]",
	}
	[2] = {
        idx = 1,
        type = 2,--2
        id1 = 20117001,
        id2 = 1,
        id3 = 0,
        desc = "[PathMon,挑衅高手,10003,26]",
	}
}
--回到总舵-辞行
[28851] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12206,
        id2 = 0,
        id3 = 0,
        pid = 28803,
        desc = "[PathNpc,李阳白,10003,1]",
	}
}
--回到总舵-叮嘱
[28852] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117002,
        id2 = 0,
        id3 = 0,
        pid = 28803,
        desc = "[PathNpc,秦玲,10003,2]",
	}
}
--帮内小憩-关怀
[10192] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117003,
        id2 = 0,
        id3 = 0,
        pid = 28804,
        desc = "[PathNpc,解俊,10003,3]",
	}
}
--帮内小憩-关怀
[10193] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10117005,
        id2 = 0,
        id3 = 0,
        pid = 28804,
        desc = "[PathNpc,苏紫菡,10003,5]",
	}
}
--帮内小憩-关怀
[10194] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117004,
        id2 = 0,
        id3 = 0,
        pid = 28804,
        desc = "[PathNpc,冉敬宏,10003,4]",
	}
}
--授艺师兄-督促技艺
[28853] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117006,
        id2 = 0,
        id3 = 0,
        pid = 28805,
        desc = "[PathNpc,裘墨阳,10003,6]",
	}
}
--授艺师兄-心有灵犀
[28854] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        pid = 28805,
        desc = "[PathNpc,打坐一分钟,10003,6]",
	}
}
--师门复命
[28806] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117007,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,骆子渔,10003,7]",
	}
}
--青竹来意-疑问
[28855] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117007,
        id2 = 0,
        id3 = 0,
        pid = 28807,
        desc = "[PathNpc,骆子渔,10003,7]",
	}
}
--青竹来意-求证
[28856] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117008,
        id2 = 0,
        id3 = 0,
        pid = 28807,
        desc = "[PathNpc,莫奇,10003,8]",
	}
}
--执法弟子
[28808] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117009,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,王林,10003,9]",
	}
}
--寻找江山
[28809] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12200,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,10]",
	}
}
--事出有因
[28810] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117014,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,10]",
	}
}
