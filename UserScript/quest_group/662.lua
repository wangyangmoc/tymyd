module("quest_group.qg662",package.seeall)
name = "黑云遮天藏旧案"
id = 662
--客栈奇遇
[19811] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16401,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,偷听,10012,300009]",
	}
}
--暗中调查-询问小二
[20419] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112017,
        id2 = 0,
        id3 = 0,
        pid = 19812,
        desc = "[PathNpc,店小二,10012,7]",
	}
}
--暗中调查-搜寻罪证
[20420] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 10512009,
        id2 = 1,
        id3 = 0,
        pid = 19812,
        desc = "[PathArea,上锁的箱子,10012,40001]",
	}
}
--天波府内
[19813] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112004,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,杨延玉,10012,9]",
	}
}
--青龙所谋
[19814] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16402,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,杨延玉,10012,9]",
	}
}
--黑街名号-知己知彼
[20422] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112003,
        id2 = 0,
        id3 = 0,
        pid = 19815,
        desc = "[PathNpc,笑道人,10012,10]",
	}
}
--黑街名号-意外相逢
[20423] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112002,
        id2 = 0,
        id3 = 0,
        pid = 19815,
        desc = "[PathNpc,唐青枫,10012,305]",
	}
}
--黑街名号-引路之人
[20491] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80011,
        id3 = 0,
        pid = 19815,
        desc = "[PathArea,丰乐楼,10012,300012]",
	}
}
--四处走动-闻者噤声
[20424] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112020,
        id2 = 0,
        id3 = 0,
        pid = 19816,
        desc = "[PathNpc,伙计,10012,12]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112021,
        id2 = 0,
        id3 = 0,
        pid = 19816,
        desc = "[PathNpc,账房先生,10012,11]",
	}
}
--四处走动-胆大之人
[20425] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112022,
        id2 = 0,
        id3 = 0,
        pid = 19816,
        desc = "[PathNpc,布布,10012,13]",
	}
}
--四处走动-东巷美人
[20426] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10112018,
        id2 = 0,
        id3 = 0,
        pid = 19816,
        desc = "[PathNpc,兰美人,10012,14]",
	}
}
--美人要求-兰芷芬芳
[20427] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10512002,
        id2 = 1,
        id3 = 0,
        pid = 19817,
        desc = "[PathEnt,芝兰仙,10012,40002]",
	}
	[2] = {
        idx = 1,
        type = 3,--3
        id1 = 10512003,
        id2 = 1,
        id3 = 0,
        pid = 19817,
        desc = "[PathEnt,伽南香,10012,40003]",
	}
}
--美人要求-珍藏宝物
[20428] = {
	[1] = {
        idx = 2,
        type = 3,--3
        id1 = 10512010,
        id2 = 1,
        id3 = 0,
        pid = 19817,
        desc = "[PathNpc,齐总管,10012,15]",
	}
}
--美人要求-燃香为引
[20429] = {
	[1] = {
        idx = 3,
        type = 10,--10
        id1 = 10112004,
        id2 = 1,
        id3 = 0,
        pid = 19817,
        desc = "[PathArea,香炉,10012,300487]",
	}
}
--姐妹情深-婕妤召见
[20430] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112019,
        id2 = 0,
        id3 = 0,
        pid = 19818,
        desc = "[PathNpc,韩婕妤,10012,20]",
	}
}
--姐妹情深-婕妤之请
[20431] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 10512011,
        id2 = 1,
        id3 = 0,
        pid = 19818,
        desc = "[PathEnt,木箱,10012,40013]",
	}
}
--姐妹情深-烟火传情
[20432] = {
	[1] = {
        idx = 2,
        type = 4,--4
        id1 = 10512011,
        id2 = 1,
        id3 = 0,
        pid = 19818,
        desc = "[PathArea,荷花池边,10012,300477]",
	}
}
--真人露相
[19819] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16403,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,擂台,10012,300013]",
	}
}
--龙潭虎穴
[19820] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16404,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,吴振英,10012,856]",
	}
}
