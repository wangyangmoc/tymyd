module("quest_group.qg42",package.seeall)
name = "天香笺之金风玉露"
id = 42
--齐聚开封
[1301] = {
	[1] = {
        idx = 0,
        type = 23,--23
        id1 = 30,
        id2 = 0,
        id3 = 0,
        desc = "等级达到30级",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10101112,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,赵月芳,10012,1776]交谈",
	}
}
--空白信笺
[1302] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10501034,
        id2 = 1,
        id3 = 0,
        desc = "获得空白天香笺",
	}
}
--永结同心
[1303] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10501035,
        id2 = 1,
        id3 = 0,
        desc = "写满祝福的天香笺",
	}
}
--有情线索
[1304] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101113,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,宇文竹,10012,1777]交谈",
	}
}
