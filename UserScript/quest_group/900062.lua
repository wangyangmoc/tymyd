module("quest_group.qg900062",package.seeall)
name = "??"
id = 900062
--瀚海小屋
[2056] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80137,
        id3 = 0,
        desc = "[PathNpc,找到老人,10004,301091]",
	}
}
--燕云血战
[2057] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102124,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,沈笠翁,10004,1600]",
	}
}
--真相大白
[2058] = {
	[1] = {
        idx = 2,
        type = 21,--21
        id1 = 15015,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10004,1837]",
	}
}
--返回绝尘
[2061] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80116,
        id3 = 0,
        desc = "[PathNpc,绝尘镇,10004,301066]",
	}
}
--再起波澜
[2062] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 21,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,了解情况,10004,1585]",
	}
}
--残忍西夏
[2063] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10102111,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,李娃,10004,1587]",
	}
	[2] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10102109,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,李保正,10004,1585]",
	}
	[3] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10102118,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,贺兰雪,10004,1594]",
	}
}
--保护村民
[2066] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20102122,
        id2 = 4,
        id3 = 0,
        desc = "击破[PathMon,西夏刽子手,10317,100031]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20102123,
        id2 = 16,
        id3 = 0,
        desc = "击破[PathMon,西夏兵卒,10317,100030]",
	}
	[3] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10102112,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,慕容敏,10317,4]",
	}
	[4] = {
        idx = 6,
        type = 7,--PathNpc
        id1 = 10102123,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,元坚,10317,7]",
	}
}
--再护村民
[2067] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10102122,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,细封宁丛,10317,6]",
	}
	[2] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10102116,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,赫连人圭,10317,5]",
	}
	[3] = {
        idx = 7,
        type = 1,--1
        id1 = 20102122,
        id2 = 4,
        id3 = 0,
        desc = "击破[PathMon,西夏刽子手,10317,100031]",
	}
	[4] = {
        idx = 8,
        type = 1,--1
        id1 = 20102123,
        id2 = 16,
        id3 = 0,
        desc = "击破[PathMon,西夏兵卒,10317,100030]",
	}
}
--先诛首恶
[2068] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20102124,
        id2 = 1,
        id3 = 0,
        desc = "击破[PathMon,精英队长,10317,1]",
	}
}
--绝尘如昔
[2071] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102109,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,李保正,10004,1585]",
	}
}
--前往神威
[2072] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80117,
        id3 = 0,
        desc = "[PathNpc,神威堡千里营,10004,301067]",
	}
}
--堡主驾临
[2076] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102125,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,韩学信,10004,1601]",
	}
}
--义字当头
[2077] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102093,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,夏玺,10004,1557]",
	}
}
--燕归大漠
[2081] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102126,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,燕南飞,10004,1602]",
	}
}
--万里飞沙
[2082] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102127,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,离玉堂,10004,1603]",
	}
}
--武林贩子
[2083] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80118,
        id3 = 0,
        desc = "[PathNpc,剑意居,10004,301068]",
	}
}
--查无此人
[2086] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102128,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,客栈掌柜,10004,1610]",
	}
}
--以武偿德
[2087] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20102088,
        id2 = 16,
        id3 = 0,
        desc = "教训[PathMon,西夏逃兵,10004,101010]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20102089,
        id2 = 4,
        id3 = 0,
        desc = "教训[PathMon,西夏兵痞,10004,101022]",
	}
}
--再询掌柜
[2088] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10102128,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,客栈掌柜,10004,1610]",
	}
}
--有违道义
[2091] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80119,
        id3 = 0,
        desc = "去[PathNpc,绿洲互市,10004,301069]",
	}
}
--有所不为
[2092] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102128,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,客栈掌柜,10004,1610]",
	}
}
