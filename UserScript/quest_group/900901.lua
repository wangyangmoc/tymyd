module("quest_group.qg900901",package.seeall)
name = "??"
id = 900901
--蜀地唐门
[27002] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116001,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐慧,10005,1]",
	}
}
--轻而易举
[27006] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116022,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐笑,10005,16]",
	}
}
--工欲其事
[27007] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116003,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐云,10005,2]",
	}
}
--唐门绝技
[27016] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116005,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐花雨,10005,4]",
	}
}
--观山悟心
[27017] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10005,
        id2 = 80007,
        id3 = 0,
        desc = "去[PathNpc,后面台阶,10005,300007]",
	}
}
--追魂别离
[27026] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10116021,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐离,10005,54]",
	}
	[2] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10116020,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐别,10005,53]",
	}
}
--唐门嫡传
[27027] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10116051,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐青青,10005,47]",
	}
}
--心存怨恨
[27028] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10116101,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,万仙儿,10005,388]",
	}
}
--如诗如雨
[27029] = {
	[1] = {
        idx = 6,
        type = 7,--PathNpc
        id1 = 10116063,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐诗雨,10005,69]",
	}
}
--端倪难辨
[27031] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116008,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,广场那人好像就是唐倪,10005,12]",
	}
}
--遍寻端倪
[27032] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116006,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐倪,10005,8]",
	}
}
--蜀地唐门
[27036] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10005,
        id2 = 80001,
        id3 = 0,
        desc = "返回[PathNpc,议事堂,10005,300001]",
	}
}
--唐门老太
[27037] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116002,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐老太,10005,5]",
	}
}
--心有余悸
[27041] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116082,
        id2 = 0,
        id3 = 0,
        desc = "问问[PathNpc,唐翔,10005,264]",
	}
}
--唐翔其人
[27042] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116100,
        id2 = 0,
        id3 = 0,
        desc = "问问[PathNpc,唐倪,10005,352]",
	}
}
--就地打坐
[27170] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "打坐一分钟",
	}
}
--蜀地唐门
[27171] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 15014,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐文山,10005,64]",
	}
}
