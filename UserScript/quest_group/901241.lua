module("quest_group.qg901241",package.seeall)
name = "??"
id = 901241
--古道西风
[36501] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80054,
        id3 = 0,
        desc = "[PathNpcLayer,四遗堂,10006,300425,1]",
	}
}
--意外插曲
[36502] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121010,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,漆雕小五,10006,1061]",
	}
}
--欣然相助
[36503] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10121061,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,向阿莎,10006,1426]",
	}
}
--青龙再现
[36504] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10121000,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,方玉峰,10006,1050]",
	}
}
--风雨际会
[36505] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121000,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,方玉峰,10006,1050]",
	}
}
--小心翼翼
[36506] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80101,
        id3 = 0,
        desc = "[PathArea,巫月坛,10006,300479]",
	}
}
--立地销魂
[36507] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10121031,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,卯今生,10006,1185]",
	}
}
--心有灵犀
[36508] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "打坐修行",
	}
}
--谢过长老
[36509] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10121031,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,卯今生,10006,1185]",
	}
}
