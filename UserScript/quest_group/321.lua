module("quest_group.qg321",package.seeall)
name = "东越见闻前置"
id = 321
--晨曦
[9601] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10106131,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--晨午
[9602] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10106132,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--悬赏令
[9603] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10106133,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--子桑不寿
[9604] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10106023,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--青龙潭未完成
[9605] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16006,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--江洋大盗1
[9606] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16010,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--江洋大盗2
[9607] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16011,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--江洋大盗3
[9608] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16012,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--戒空隐藏任务
[9610] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 14023,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
