module("quest_group.qg1002",package.seeall)
name = "洗清白同门联手"
id = 1002
--打探消息-龙头阁
[28871] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80003,
        id3 = 0,
        pid = 28811,
        desc = "[Map_X_Y,龙头阁,10003,2027,1579]",
	}
}
--打探消息-拦路人
[28872] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117015,
        id2 = 0,
        id3 = 0,
        pid = 28811,
        desc = "[PathNPCLayer,守山弟子,10003,11,1]",
	}
}
--打探消息-碰头
[28873] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10117017,
        id2 = 0,
        id3 = 0,
        pid = 28811,
        desc = "[PathNpc,江山,10003,13]",
	}
}
--联手闯阁
[28812] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12201,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,13]",
	}
}
--冲破拦截
[28813] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80051,
        id3 = 0,
        desc = "前往[PathNpc,龙头阁,10003,100202]前",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20117005,
        id2 = 5,
        id3 = 0,
        desc = "[PathMon,竹坞堂高手,10003,100202]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20117007,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹坞堂打手,10003,100202]",
	}
	[4] = {
        idx = 3,
        type = 1,--1
        id1 = 20117006,
        id2 = 15,
        id3 = 0,
        desc = "[PathMon,竹坞堂弟子,10003,100202]",
	}
}
--寻找江山-会合
[28845] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117022,
        id2 = 0,
        id3 = 0,
        pid = 28814,
        desc = "[PathNpc,江山,10003,14]",
	}
}
--寻找江山-疗伤
[28846] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10101011,
        id2 = 5,
        id3 = 0,
        pid = 28814,
        desc = "[PathNpc,散瘀兰,10003,40010]",
	}
}
--寻找江山-送药
[28847] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10117022,
        id2 = 0,
        id3 = 0,
        pid = 28814,
        desc = "[PathNpc,秦岭,10003,14]",
	}
}
--夺青竹令-剪除羽翼
[28874] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20117008,
        id2 = 5,
        id3 = 0,
        pid = 28815,
        desc = "[PathMon,竹笑堂精锐,10003,100034]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20117009,
        id2 = 20,
        id3 = 0,
        pid = 28815,
        desc = "[PathMon,竹笑堂打手,10003,100034]",
	}
}
--夺青竹令-夺回令牌
[28875] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80050,
        id3 = 0,
        pid = 28815,
        desc = "找到[PathMon,方必穹,10003,31]",
	}
	[2] = {
        idx = 3,
        type = 2,--2
        id1 = 20117010,
        id2 = 1,
        id3 = 0,
        pid = 28815,
        desc = "夺回[PathNpc,青竹令,10003,31]",
	}
	[3] = {
        idx = 4,
        type = 10,--10
        id1 = 10101013,
        id2 = 1,
        id3 = 0,
        pid = 28815,
        desc = "取得[PathNpc,酒仙酿,10003,40026]",
	}
}
--寻找江山
[28816] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117023,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,15]",
	}
}
--破围而出
[28817] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12202,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,15]",
	}
}
--商量对策
[28818] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117029,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,17]",
	}
}
--入夜回帮
[28819] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12205,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,江山,10003,17]",
	}
}
--商量对策-下落
[10198] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117024,
        id2 = 0,
        id3 = 0,
        pid = 28820,
        desc = "[PathNpc,江山,10003,33]",
	}
}
