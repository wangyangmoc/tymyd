module("quest_group.qg900361",package.seeall)
name = "??"
id = 900361
--行商求助
[5601] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104010,
        id2 = 1,
        id3 = 0,
        desc = "找到[PathNpc,贾正金,10010,55]",
	}
}
--物归原主
[5602] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 10504001,
        id2 = 4,
        id3 = 0,
        desc = "取得[PathEnt,行商货物,10010,40044]",
	}
}
--暗流涌动
[5607] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104009,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNPC,离玉堂,10010,34]交谈",
	}
}
--互相牵制
[5608] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104109,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNPC,李红渠,10010,11703]交谈",
	}
	[2] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10104111,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNPC,上官小仙,10010,11704]交谈",
	}
	[3] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10104110,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNPC,皇甫星,10010,11705]交谈",
	}
}
--同盟不易
[5609] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10104009,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNPC,离玉堂,10010,34]交谈",
	}
}
--流杀门
[5613] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20104006,
        id2 = 12,
        id3 = 0,
        desc = "教训[PathMon,流杀门喽喽,10010,101698]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20104007,
        id2 = 12,
        id3 = 0,
        desc = "教训[PathMon,流杀门帮众,10010,101698]",
	}
}
--行商家丁
[5618] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10104012,
        id2 = 0,
        id3 = 0,
        desc = "找到呼救的[PathNpc,行商家丁,10010,54]",
	}
}
--林中异响
[5660] = {
	[1] = {
        idx = 3,
        type = 21,--21
        id1 = 17004,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,慈云林小径,10010,300069]",
	}
}
--财神往事
[5661] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104002,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,黄金生,10010,24]",
	}
}
--万里飞沙
[5662] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104097,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,黄元文,10010,11562]",
	}
}
--询问消息
[5663] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104504,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,金十八,10010,9848]",
	}
}
--林中异响
[5664] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10010,
        id2 = 80018,
        id3 = 0,
        desc = "[PathNPC,杭州城外,10010,300018]",
	}
}
--仓库
[10129] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104065,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNPC,仓库掌柜,10010,9782]谈谈",
	}
}
--邮差
[10130] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104701,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNPC,大宋邮差,10010,11749]谈谈",
	}
}
