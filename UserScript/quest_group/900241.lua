module("quest_group.qg900241",package.seeall)
name = "??"
id = 900241
--初来江南
[7202] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105525,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,李红渠,10011,861]询问",
	}
}
--打探敌情
[7203] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10105530,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,庐北川,10011,872]",
	}
}
--惊魂稍定
[7204] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10105001,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,铸神谷管家,10011,150]",
	}
}
--风铃古道
[7206] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80019,
        id3 = 0,
        desc = "[PathArea,风铃道,10011,300030]",
	}
}
--银铃玉佩
[7207] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10105006,
        id2 = 1,
        id3 = 0,
        desc = "找[PathEnt,线索,10011,40001]",
	}
}
--盘龙七
[7216] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 19000001,
        id2 = 5,
        id3 = 0,
        desc = "找草药[PathEnt,盘龙七,10011,40002]",
	}
}
--齐落梅
[7217] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105003,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,齐落梅,10011,186]",
	}
}
--横渡大江
[7221] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105003,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,齐落梅,10011,186]对话",
	}
}
--翻江踏浪
[7222] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80117,
        id3 = 0,
        desc = "渡江潜入[PathNpc,枫桥镇,10011,300228]",
	}
}
--偶遇怪人
[7223] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10105070,
        id2 = 0,
        id3 = 0,
        desc = "渡江后找[PathNpc,说话的人,10011,873]",
	}
}
--齐中原
[7231] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105015,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,齐中原,10011,362]",
	}
}
--斜云小径
[7232] = {
	[1] = {
        idx = 4,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80118,
        id3 = 0,
        desc = "穿过[PathArea,斜云小径,10011,300229]前往铸神谷",
	}
}
--铸神弟子
[7233] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10105555,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,铸神弟子,10011,1057]",
	}
}
--齐瑶
[7234] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10104545,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,齐瑶,10011,1039]",
	}
}
--寻迹
[7235] = {
	[1] = {
        idx = 3,
        type = 21,--21
        id1 = 17001,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,寻找钟怡,10011,300226]",
	}
}
--来龙去脉
[7242] = {
	[1] = {
        idx = 0,
        type = 19,--19
        id1 = 11024,
        id2 = 1,
        id3 = 0,
        desc = "与[PathNpc,叶知秋,10011,333]对话",
	}
}
--飞渡
[7243] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 18,
        id2 = 0,
        id3 = 0,
        desc = "前往[PathArea,龙井茶园,10011,300231]",
	}
}
--铸神弟子
[7249] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10105556,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,铸神弟子,10011,1058]",
	}
}
