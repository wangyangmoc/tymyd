module("quest_group.qg664",package.seeall)
name = "青龙弄潮为奇书"
id = 664
--镇国古寺
[19831] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16407,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,相国寺,10012,300015]",
	}
}
--相国寺内-入寺礼仪
[20472] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10512006,
        id2 = 1,
        id3 = 0,
        pid = 19832,
        desc = "[PathNpc,迎客僧,10012,29]",
	}
}
--相国寺内-净手入寺
[20473] = {
	[1] = {
        idx = 1,
        type = 4,--4
        id1 = 10512006,
        id2 = 1,
        id3 = 0,
        pid = 19832,
        desc = "[PathArea,净手,10012,300475]",
	}
}
--红尘之外-悟法大师
[20454] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10512004,
        id2 = 1,
        id3 = 0,
        pid = 19833,
        desc = "[PathNpc,悟法大师,10012,23]",
	}
}
--红尘之外-焚香礼佛
[20455] = {
	[1] = {
        idx = 1,
        type = 4,--4
        id1 = 10512004,
        id2 = 1,
        id3 = 0,
        pid = 19833,
        desc = "[PathArea,香烛,10012,300476]",
	}
}
--红尘之外-暂抛红尘
[20456] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80046,
        id3 = 0,
        pid = 19833,
        desc = "[PathNpc,罗汉殿,10012,300488]",
	}
}
--红尘之外-静心凝神
[20474] = {
	[1] = {
        idx = 3,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        pid = 19833,
        desc = "打坐一分钟。",
	}
}
--红尘难断-四大皆空
[20475] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112008,
        id2 = 0,
        id3 = 0,
        pid = 19834,
        desc = "[PathNpc,戒空大师,10012,24]",
	}
}
--红尘难断-塔顶观景
[20477] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 10,
        id2 = 0,
        id3 = 0,
        pid = 19834,
        desc = "在塔顶俯瞰开封城。",
	}
}
--红尘难断-所见所闻
[20478] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112008,
        id2 = 0,
        id3 = 0,
        pid = 19834,
        desc = "[PathNpc,戒空大师,10012,24]",
	}
}
--前尘过往
[19835] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16408,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,戒空大师,10012,24]",
	}
}
--龙首真面-一观画卷
[20460] = {
	[1] = {
        idx = 0,
        type = 4,--4
        id1 = 10512012,
        id2 = 1,
        id3 = 0,
        pid = 19836,
        desc = "画卷",
	}
}
--龙首真面-惊人发现
[20461] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112097,
        id2 = 0,
        id3 = 0,
        pid = 19836,
        desc = "[PathNpc,燕南飞,10012,56]",
	}
}
--回禀将军
[19837] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16409,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,杨延玉,10012,9]",
	}
}
--奇书所在
[19838] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16410,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,皇城,10012,300017]",
	}
}
--迟来一步
[19839] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80047,
        id3 = 0,
        desc = "[PathArea,护龙河岸,10012,300489]",
	}
}
--见证之人
[19840] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112002,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐青枫,10012,305]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112003,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,笑道人,10012,10]",
	}
}
--东京梦华
[19841] = {
	[1] = {
        idx = 0,
        type = 23,--23
        id1 = 75,
        id2 = 0,
        id3 = 0,
        desc = "等级提升至75",
	}
}
