module("quest_group.qg900903",package.seeall)
name = "??"
id = 900903
--龙纹青衣
[27127] = {
	[1] = {
        idx = 0,
        type = 2,--2
        id1 = 20116016,
        id2 = 1,
        id3 = 0,
        desc = "[PathMon,青龙头目,10005,401]",
	}
}
--镜花水月
[27141] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10116007,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,唐翔,10005,78]对话",
	}
}
--终有始末
[27142] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116033,
        id2 = 0,
        id3 = 0,
        desc = "飞出[PathNpc,碎星楼,10005,78]，找[PathNpc,唐倪,10005,21]汇报",
	}
}
--飞跃对岸
[27143] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10005,
        id2 = 80006,
        id3 = 0,
        desc = "请[PathNpc,唐倪,10005,21]帮忙",
	}
}
--叛徒下场
[27144] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10116034,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,唐端,10005,25]询问",
	}
}
