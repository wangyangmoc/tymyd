module("quest_group.qg901",package.seeall)
name = "第一回：有唐门地本逍遥"
id = 901
--返回师门-蜀地唐门
[27002] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116001,
        id2 = 0,
        id3 = 0,
        pid = 27001,
        desc = "[PathNpc,唐慧,10005,1]",
	}
}
--傀儡化形-轻而易举
[27006] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116022,
        id2 = 0,
        id3 = 0,
        pid = 27005,
        desc = "[PathNpc,唐笑,10005,16]",
	}
}
--傀儡化形-工欲其事
[27007] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116003,
        id2 = 0,
        id3 = 0,
        pid = 27005,
        desc = "[PathNpc,唐云,10005,2]",
	}
}
--锦衣罗衫
[27010] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116004,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐巧,10005,3]",
	}
}
--花雨飞星-唐门绝技
[27016] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116005,
        id2 = 0,
        id3 = 0,
        pid = 27015,
        desc = "[PathNpc,唐花雨,10005,4]",
	}
}
--花雨飞星-观山悟心
[27017] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10005,
        id2 = 80007,
        id3 = 0,
        pid = 27015,
        desc = "去[PathNpc,后面台阶,10005,300007]",
	}
}
--心有灵犀-就地打坐
[27170] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        pid = 27020,
        desc = "打坐一分钟",
	}
}
--心有灵犀-蜀地唐门
[27171] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 15014,
        id2 = 0,
        id3 = 0,
        pid = 27020,
        desc = "[PathNpc,唐文山,10005,64]",
	}
}
--内外之争-追魂别离
[27026] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10116021,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,唐离,10005,54]",
	}
	[2] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10116020,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,唐别,10005,53]",
	}
}
--内外之争-唐门嫡传
[27027] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10116051,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,唐青青,10005,47]",
	}
}
--内外之争-心存怨恨
[27028] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10116101,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,万仙儿,10005,388]",
	}
}
--内外之争-如诗如雨
[27029] = {
	[1] = {
        idx = 6,
        type = 7,--PathNpc
        id1 = 10116063,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,唐诗雨,10005,69]",
	}
}
--内外之争-蜀地唐门
[27036] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10005,
        id2 = 80001,
        id3 = 0,
        pid = 27025,
        desc = "返回[PathNpc,议事堂,10005,300001]",
	}
}
--内外之争-唐门老太
[27037] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116002,
        id2 = 0,
        id3 = 0,
        pid = 27025,
        desc = "[PathNpc,唐老太,10005,5]",
	}
}
--傀儡新术-端倪难辨
[27031] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116008,
        id2 = 0,
        id3 = 0,
        pid = 27030,
        desc = "[PathNpc,广场那人好像就是唐倪,10005,12]",
	}
}
--傀儡新术-遍寻端倪
[27032] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116006,
        id2 = 0,
        id3 = 0,
        pid = 27030,
        desc = "[PathNpc,唐倪,10005,8]",
	}
}
--泥胎木塑
[27035] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 19033,
        id2 = 0,
        id3 = 0,
        desc = "跟[PathNpc,唐倪,10005,8]对话",
	}
}
--暗流涌动-心有余悸
[27041] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10116082,
        id2 = 0,
        id3 = 0,
        pid = 27040,
        desc = "问问[PathNpc,唐翔,10005,264]",
	}
}
--暗流涌动-唐翔其人
[27042] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116100,
        id2 = 0,
        id3 = 0,
        pid = 27040,
        desc = "问问[PathNpc,唐倪,10005,352]",
	}
}
