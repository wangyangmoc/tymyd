module("quest_group.qg25",package.seeall)
name = "盟会日常"
id = 25
--镇守盟会
[674] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500036,
        id2 = 12,
        id3 = 50,
        desc = "镇守 [PathNpc,护卫,mapId,instId] 12分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[675] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500017,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,左锐进,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[676] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500018,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,充阳平,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[677] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500019,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,葛敏才,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[678] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500020,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,边博雅,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[679] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500021,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,荀沭,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[680] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500022,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,孙松,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[681] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500023,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,于高,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[682] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500024,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,向群,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[683] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500025,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,蔺南,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--保护访客
[684] = {
	[1] = {
        idx = 0,
        type = 27,--27
        id1 = 2500026,
        id2 = 8,
        id3 = 30,
        desc = "保护 [PathNpc,张平,mapId,instId] 8分钟[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找护卫
[685] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500027,
        id2 = 0,
        id3 = 0,
        desc = "找到巡逻的万高明[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找护卫
[686] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500028,
        id2 = 0,
        id3 = 0,
        desc = "找到巡逻的薄和泰[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找护卫
[687] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500029,
        id2 = 0,
        id3 = 0,
        desc = "找到巡逻的贺阳朔[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--收集物资
[688] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 2500001,
        id2 = 20,
        id3 = 0,
        desc = "获取军备物资[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[689] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 0,
        desc = "击败敌盟 [PathMon,左锐进,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[690] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 1,
        desc = "击败敌盟 [PathMon,充阳平,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[691] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 2,
        desc = "击败敌盟 [PathMon,葛敏才,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[692] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 3,
        desc = "击败敌盟 [PathMon,边博雅,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[693] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 4,
        desc = "击败敌盟 [PathMon,荀沭,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[694] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 5,
        desc = "击败敌盟 [PathMon,孙松,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[695] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 6,
        desc = "击败敌盟 [PathMon,于高,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[696] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 7,
        desc = "击败敌盟 [PathMon,向群,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[697] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 8,
        desc = "击败敌盟 [PathMon,蔺南,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--击败访客
[698] = {
	[1] = {
        idx = 0,
        type = 38,--38
        id1 = 0,
        id2 = 1,
        id3 = 9,
        desc = "击败敌盟 [PathMon,张平,mapId,instId][ENEMY]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找情报探子
[699] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500030,
        id2 = 0,
        id3 = 0,
        desc = "找到闵奉[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找情报探子
[700] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500031,
        id2 = 0,
        id3 = 0,
        desc = "找到廖宁[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
--寻找情报探子
[701] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 2500032,
        id2 = 0,
        id3 = 0,
        desc = "找到慕极[FRIEND]",
	}
	[2] = {
        idx = 1,
        type = 30,--30
        id1 = 15,
        id2 = 0,
        id3 = 0,
        desc = "或 击败15名 非本盟玩家",
	}
}
