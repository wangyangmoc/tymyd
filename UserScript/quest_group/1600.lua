module("quest_group.qg1600",package.seeall)
name = "帮派建造"
id = 1600
--建设寻人
[42001] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104632,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封诸葛俞,10012,1903]交谈",
	}
}
--建设寻人
[42002] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104633,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封雪秋千,10012,1904]交谈",
	}
}
--建设寻人
[42003] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104634,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封澜八月,10012,1905]交谈",
	}
}
--建设寻人
[42004] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104635,
        id2 = 1,
        id3 = 0,
        desc = "与[PathEnt,开封赤名,10012,1913]交谈",
	}
}
--建设寻人
[42005] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104636,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封海岳,10012,1914]交谈",
	}
}
--建设寻人
[42006] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104637,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封风枪却,10012,1915]交谈",
	}
}
--建设寻人
[42007] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104638,
        id2 = 1,
        id3 = 0,
        desc = "和[PathEnt,开封三余,10012,1916]交谈",
	}
}
--建设寻人
[42008] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104639,
        id2 = 1,
        id3 = 0,
        desc = "寻人：找到[PathEnt,开封梓景逸,10012,1917]交谈",
	}
}
--建设寻人
[42009] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104640,
        id2 = 1,
        id3 = 0,
        desc = "寻人：找到[PathEnt,开封尤焕申,10012,1918]交谈",
	}
}
--建设寻人
[42010] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104641,
        id2 = 1,
        id3 = 0,
        desc = "寻人：找到[PathEnt,开封周景,10012,1919]交谈",
	}
}
