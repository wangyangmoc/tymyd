module("quest_group.qg900043",package.seeall)
name = "??"
id = 900043
--寻访周婷
[1320] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10109046,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,周婷,10013,92]交谈",
	}
}
--我为君歌
[1321] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 12122,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,周婷,10013,92]交谈",
	}
}
--君心我心
[1322] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10109046,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,周婷,10013,92]交谈",
	}
}
--前往东越
[1323] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10106022,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,楚天璇,10002,1761]交谈",
	}
}
--出海归来
[1324] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10106023,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,子桑不寿,10002,1762]交谈",
	}
}
--海外奇物
[1325] = {
	[1] = {
        idx = 3,
        type = 3,--3
        id1 = 10501039,
        id2 = 1,
        id3 = 0,
        desc = "找到邬雪",
	}
}
--开封寻人
[1326] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112003,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,笑道人,10012,10]交谈",
	}
}
--静以待之
[1327] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "等待笑道人想词",
	}
}
--君心我心
[1328] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112003,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,笑道人,10012,10]交谈",
	}
}
--前往杭州
[1329] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10107048,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,韩莹莹,10010,11686]交谈",
	}
}
--武试美人
[1330] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 12123,
        id2 = 0,
        id3 = 0,
        desc = "战胜[PathNpc,韩莹莹,10010,11686]",
	}
}
--君心我心
[1331] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10107048,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,韩莹莹,10010,11686]交谈",
	}
}
--杭州寻人
[1332] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101116,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,王喜金,10010,9827]交谈",
	}
}
--笔墨纸砚
[1333] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 40103016,
        id2 = 1,
        id3 = 0,
        desc = "乌丸墨",
	}
}
--君心我心
[1334] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10101116,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,王喜金,10010,9827]交谈",
	}
}
--开封寻人
[1335] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112264,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,雅奴二十七,10012,952]交谈",
	}
}
--采花贼人
[1336] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20101110,
        id2 = 1,
        id3 = 0,
        desc = "[PathMon,采花贼,10012,1791]",
	}
}
--君心我心
[1337] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112264,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,雅奴二十七,10012,952]交谈",
	}
}
--近在眼前
[1338] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101112,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,赵月芳,10012,1776]交谈",
	}
}
--远在开封
[1339] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112004,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,杨延玉,10012,9]交谈",
	}
}
--我为卿狂
[1340] = {
	[1] = {
        idx = 3,
        type = 10,--10
        id1 = 10101019,
        id2 = 1,
        id3 = 0,
        desc = "采摘[PathEnt,帝女花,10012,40020]",
	}
}
--君心我心
[1347] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10101112,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,赵月芳,10012,1776]交谈",
	}
}
--君心我心
[1348] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10106022,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,楚天璇,10002,1761]交谈",
	}
}
