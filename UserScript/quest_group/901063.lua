module("quest_group.qg901063",package.seeall)
name = "??"
id = 901063
--循迹而去
[31221] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80015,
        id3 = 0,
        desc = "[PathNpc,山道,10007,300015]",
	}
}
--贼人衣物
[31222] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118068,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,夜行衣,10007,461]",
	}
}
--江湖散客
[31223] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118046,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,赵都安,10007,66]",
	}
}
--真武弟子
[31224] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118023,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,吴涯,10007,47]",
	}
}
--青龙恶徒
[31225] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20118018,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,龙尾先锋·归堂,10007,100023]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20118019,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,龙尾帮众·归堂,10007,100023]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20118020,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,龙尾精锐·归堂,10007,100023]",
	}
}
--化险为夷
[31226] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10118024,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,陈有,10007,48]",
	}
}
--碧荷师姐
[31227] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118018,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,丁碧荷,10007,53]",
	}
}
--同门切磋
[31228] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20118023,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,慕容明,10007,79]",
	}
}
--无痕剑意
[31230] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118029,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,独孤若虚,10007,49]",
	}
}
--和光同尘
[31231] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118003,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,笑道人,10007,78]",
	}
}
--掌门叮咛
[31232] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118013,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,张梦白,10007,2]",
	}
}
--拜别师兄
[31233] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118001,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPCLayer,笑道人,10007,1,1]",
	}
}
--拜别师弟
[31234] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118012,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPCLayer,凌玄,10007,215,1]",
	}
}
--复兴师叔
[31238] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118051,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,龙复兴,10007,133]",
	}
}
--寒松师弟
[31239] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118059,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,寒松子,10007,216]",
	}
}
