module("quest_group.qg900122",package.seeall)
name = "??"
id = 900122
--提醒楚鸿
[3905] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103019,
        id2 = 0,
        id3 = 0,
        desc = "驻山弟子[PathNpc,楚鸿,10009,26]",
	}
}
--提醒齐川
[3906] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103020,
        id2 = 0,
        id3 = 0,
        desc = "驻山弟子[PathNpc,齐川,10009,27]",
	}
}
--提醒许广
[3907] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10103036,
        id2 = 0,
        id3 = 0,
        desc = "驻山弟子[PathNpc,许广,10009,28]",
	}
}
--不速之客
[3909] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20103009,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,山道神秘人,10009,100626]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20103010,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,神秘镖客,10009,100626]",
	}
	[3] = {
        idx = 4,
        type = 1,--1
        id1 = 20103051,
        id2 = 3,
        id3 = 0,
        desc = "[PathMon,神秘高手,10009,100626]",
	}
}
--非同小可
[3912] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103051,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,唐林,10009,347]交谈",
	}
}
--追上凌霄
[3914] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80043,
        id3 = 0,
        desc = "赶赴[PathArea,凌霄道,10009,300057]追踪盗剑人",
	}
}
--必经之路
[3921] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80033,
        id3 = 0,
        desc = "到[Map_X_Y,丁字路口,10009,3379,3559]查探",
	}
}
--事实真相
[3922] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103006,
        id2 = 0,
        id3 = 0,
        desc = "回报[PathNpc,五爷,10009,78]",
	}
}
--提醒刘云
[3929] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80016,
        id3 = 0,
        desc = "驻山弟子[PathNpc,刘云,10009,300018]",
	}
}
--速查探
[3930] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80017,
        id3 = 0,
        desc = "[PathArea,上山查探,10009,300019]",
	}
}
--急问询
[3931] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103004,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,独孤若虚,10009,30]交谈",
	}
}
--多加防范
[3932] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10103014,
        id2 = 0,
        id3 = 0,
        desc = "爬上巨石，与[PathNpc,王青丘,10009,62]交谈",
	}
}
--重伤弟子
[3934] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103048,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,重伤的巡山弟子,10009,334]交谈",
	}
}
--伤者所指
[3935] = {
	[1] = {
        idx = 4,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80056,
        id3 = 0,
        desc = "[PathArea,继续追查,10009,300060]",
	}
}
--是敌是友
[3938] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103077,
        id2 = 0,
        id3 = 0,
        desc = "听[PathNpc,车昊顺,10009,167]诉说",
	}
}
--暗观察
[3941] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80044,
        id3 = 0,
        desc = "[PathArea,查探前方情况,10009,300058]",
	}
}
--路口寻迹
[3944] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103076,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,王青丘,10009,1044]",
	}
}
--接踵而来
[3945] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10103009,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,易山,10009,90]交谈",
	}
}
--急事相告
[3948] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10103007,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,公孙剑,10009,323]交谈",
	}
}
