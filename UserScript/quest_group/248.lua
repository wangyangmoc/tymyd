module("quest_group.qg248",package.seeall)
name = "第九回：龙首山前青鳞现"
id = 248
--一触即发-铸神落竹
[7407] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10105534,
        id2 = 0,
        id3 = 0,
        pid = 7405,
        desc = "找[PathNpc,齐落竹,10011,920]",
	}
}
--一触即发-大鹏展翅
[7408] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10105536,
        id2 = 0,
        id3 = 0,
        pid = 7405,
        desc = "找[PathNpc,霍天鹏,10011,919]",
	}
}
--一触即发-铸神寒梅
[7409] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105535,
        id2 = 0,
        id3 = 0,
        pid = 7405,
        desc = "找[PathNpc,齐落梅,10011,918]",
	}
}
--扫穴擒渠-扫穴擒渠
[7416] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 11004,
        id2 = 0,
        id3 = 0,
        pid = 7415,
        desc = "找[PathNpc,孟长风,10011,959]",
	}
}
--真武修行
[7420] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104740,
        id2 = 0,
        id3 = 0,
        desc = "前往真武寻找修行师傅[PathNPC,莫千瑞,10007,433]",
	}
}
--江湖百业
[7421] = {
	[1] = {
        idx = 0,
        type = 23,--23
        id1 = 30,
        id2 = 0,
        id3 = 0,
        desc = "等级提升至30",
	}
}
--水波龙吟
[7422] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105173,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐青枫,10011,1792]",
	}
}
--五毒修行
[7423] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 1300173,
        id2 = 0,
        id3 = 0,
        desc = "前往五毒寻找修行师傅[PathNPC, 袁明哲,10006,2352]",
	}
}
--霹雳旧址-长江分舵
[7466] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80122,
        id3 = 0,
        pid = 7465,
        desc = "前往[PathArea,长江分舵,10011,300259]杀龙丘青",
	}
}
--霹雳旧址-突遇变故
[7467] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105139,
        id2 = 0,
        id3 = 0,
        pid = 7465,
        desc = "找[PathNpc,霍天鹏,10011,1172]",
	}
}
--霹雳旧址-大战在即
[7468] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20105118,
        id2 = 6,
        id3 = 0,
        pid = 7465,
        desc = "击破[PathMon,连环坞喽啰,10011,101059]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20105117,
        id2 = 2,
        id3 = 0,
        pid = 7465,
        desc = "击破[PathMon,连环坞头目,10011,1379]",
	}
	[3] = {
        idx = 4,
        type = 1,--1
        id1 = 20105116,
        id2 = 9,
        id3 = 0,
        pid = 7465,
        desc = "击破[PathMon,连环坞飞贼,10011,101059]",
	}
}
--霹雳旧址-唐门高手
[7469] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10105140,
        id2 = 0,
        id3 = 0,
        pid = 7465,
        desc = "去找[PathNpc,唐二,10011,1141]",
	}
}
--霹雳长风-疾驰飞奔
[7486] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80123,
        id3 = 0,
        pid = 7485,
        desc = "前往[PathArea,长风林,10011,300260]求援",
	}
}
--霹雳长风-捕风捉影
[7487] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20105082,
        id2 = 6,
        id3 = 0,
        pid = 7485,
        desc = "击退[PathMon,连环坞捕风,10011,101080]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20105097,
        id2 = 8,
        id3 = 0,
        pid = 7485,
        desc = "击退[PathMon,连环坞捉影,10011,101080]",
	}
}
--霹雳长风-霹雳长风
[7488] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10105141,
        id2 = 0,
        id3 = 0,
        pid = 7485,
        desc = "找[PathNpc,孟长风,10011,1131]",
	}
}
--鸣雷激战-鸣雷深谷
[7501] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20105036,
        id2 = 9,
        id3 = 0,
        pid = 7500,
        desc = "击退[PathMon,连环坞暗青子,10011,101108]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20105037,
        id2 = 9,
        id3 = 0,
        pid = 7500,
        desc = "击退[PathMon,连环坞爪牙,10011,101108]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20105105,
        id2 = 3,
        id3 = 0,
        pid = 7500,
        desc = "击退[PathMon,连环坞伏击长,10011,101108]",
	}
}
--鸣雷激战-奇踪再现
[7502] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10105150,
        id2 = 0,
        id3 = 0,
        pid = 7500,
        desc = "找[PathNpc,唐二,10011,1173]",
	}
}
--鸣雷激战-大鹏折翅
[7503] = {
	[1] = {
        idx = 4,
        type = 2,--2
        id1 = 20105112,
        id2 = 1,
        id3 = 0,
        pid = 7500,
        desc = "击败[PathMon,欧顺,10011,1397]",
	}
}
--鸣雷激战-霹雳雷火
[7504] = {
	[1] = {
        idx = 5,
        type = 3,--3
        id1 = 19000002,
        id2 = 1,
        id3 = 0,
        pid = 7500,
        desc = "拿到火器[PathNpc,匣子,10011,40032]",
	}
}
--鸣雷激战-大功告成
[7506] = {
	[1] = {
        idx = 6,
        type = 7,--PathNpc
        id1 = 10105150,
        id2 = 0,
        id3 = 0,
        pid = 7500,
        desc = "找[PathNpc,唐二,10011,1173]",
	}
}
--擒贼擒王-一青二白
[7507] = {
	[1] = {
        idx = 0,
        type = 2,--2
        id1 = 20105038,
        id2 = 1,
        id3 = 0,
        pid = 7505,
        desc = "除掉[PathMon,龙丘青,10011,418]",
	}
}
--擒贼擒王-首恶伏诛
[7508] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20105083,
        id2 = 9,
        id3 = 0,
        pid = 7505,
        desc = "[PathMon,连环坞飘子,10011,101134]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20105084,
        id2 = 12,
        id3 = 0,
        pid = 7505,
        desc = "[PathMon,分舵守卫,10011,101134]",
	}
}
--擒贼擒王-杀出分舵
[7509] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80168,
        id3 = 0,
        pid = 7505,
        desc = "冲到[PathArea,分舵外围,10011,300922]",
	}
}
--四明居士-青枫传讯
[7626] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105159,
        id2 = 0,
        id3 = 0,
        pid = 7625,
        desc = "找[PathNpc,唐二,10011,1263]",
	}
}
--四明居士-江南四明
[7627] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80121,
        id3 = 0,
        pid = 7625,
        desc = "前往[PathArea,四明居,10011,300258]",
	}
}
--四明居士-盟主再现
[7629] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10105152,
        id2 = 0,
        id3 = 0,
        pid = 7625,
        desc = "找[PathNpc,叶知秋,10011,1242]",
	}
}
--四明居士-传讯指路
[7634] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10105143,
        id2 = 0,
        id3 = 0,
        pid = 7625,
        desc = "找[PathNpc,上官小仙,10011,1194]",
	}
}
--四明居士-孤军深入
[7636] = {
	[1] = {
        idx = 4,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80018,
        id3 = 0,
        pid = 7625,
        desc = "前往[PathArea,天池分舵,10011,300029]",
	}
}
--四明居士-唐门少俊
[7637] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10105146,
        id2 = 0,
        id3 = 0,
        pid = 7625,
        desc = "找[PathNpc,唐二,10011,1197]",
	}
}
--四明居士-鲲鹏长风
[7638] = {
	[1] = {
        idx = 6,
        type = 7,--PathNpc
        id1 = 10105142,
        id2 = 0,
        id3 = 0,
        pid = 7625,
        desc = "找[PathNpc,孟长风,10011,1196]",
	}
}
--声东击西-震惊百里
[7631] = {
	[1] = {
        idx = 0,
        type = 2,--2
        id1 = 20105113,
        id2 = 1,
        id3 = 0,
        pid = 7630,
        desc = "干掉[PathMon,百里信,10011,1443]",
	}
}
--声东击西-以暴制暴
[7632] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20105088,
        id2 = 6,
        id3 = 0,
        pid = 7630,
        desc = "[PathMon,天池分舵武师,10011,101193]",
	}
	[2] = {
        idx = 4,
        type = 1,--1
        id1 = 20105090,
        id2 = 6,
        id3 = 0,
        pid = 7630,
        desc = "[PathMon,天池分舵暗哨,10011,101193]",
	}
	[3] = {
        idx = 5,
        type = 1,--1
        id1 = 20105089,
        id2 = 2,
        id3 = 0,
        pid = 7630,
        desc = "[PathMon,天池分舵精锐,10011,101193]",
	}
}
--声东击西-一箭双雕
[7633] = {
	[1] = {
        idx = 2,
        type = 2,--2
        id1 = 20105111,
        id2 = 1,
        id3 = 0,
        pid = 7630,
        desc = "干掉[PathMon,文太白,10011,1448]",
	}
	[2] = {
        idx = 3,
        type = 2,--2
        id1 = 20105086,
        id2 = 1,
        id3 = 0,
        pid = 7630,
        desc = "干掉[PathMon,龙丘复阳,10011,2233]",
	}
}
--义字坡头
[7635] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10011,
        id2 = 80124,
        id3 = 0,
        desc = "前往[PathArea,义字坡,10011,300261]",
	}
}
--界限突破壹
[7645] = {
	[1] = {
        idx = 0,
        type = 28,--28
        id1 = 1,
        id2 = 0,
        id3 = 0,
        desc = "等待突破：江湖百业(30级)",
	}
}
--丐帮修行
[7646] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104737,
        id2 = 0,
        id3 = 0,
        desc = "前往丐帮寻找修行师傅[PathNPC,柏仪礼,10003,500]",
	}
}
--唐门修行
[7647] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104738,
        id2 = 0,
        id3 = 0,
        desc = "前往唐门寻找修行师傅[PathNPC,唐诗云,10005,138]",
	}
}
--天香修行
[7648] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104739,
        id2 = 0,
        id3 = 0,
        desc = "前往天香寻找修行师傅[PathNPC,宓瑾,10002,2981]",
	}
}
--神威修行
[7649] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104736,
        id2 = 0,
        id3 = 0,
        desc = "前往神威寻找修行师傅[PathNPC,佟之行,10004,1535]",
	}
}
--太白修行
[7650] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20104735,
        id2 = 0,
        id3 = 0,
        desc = "前往太白寻找修行师傅[PathNPC,常衡,10009,1098]",
	}
}
