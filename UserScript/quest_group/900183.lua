module("quest_group.qg900183",package.seeall)
name = "??"
id = 900183
--黄金百万
[10186] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112064,
        id2 = 0,
        id3 = 0,
        desc = "拜访[PathNpc,金百万,10012,94]",
	}
}
--拍卖行
[10187] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112064,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,金百万,10012,94]聊聊",
	}
}
--凶相毕露

[11039] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20107045,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,天绝禅院巡守,10010,101657]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20107046,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,天绝禅院武僧,10010,101658]",
	}
}
--激浊扬清 

[11040] = {
	[1] = {
        idx = 2,
        type = 2,--2
        id1 = 20107047,
        id2 = 1,
        id3 = 0,
        desc = "[PathMon,丁浑,10010,10123]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20107048,
        id2 = 5,
        id3 = 0,
        desc = "[PathMon,丁浑侍卫,10010,10404]",
	}
}
--游尘土梗

[11042] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10107023,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,禅院侍僧,10010,10105]",
	}
}
--天绝独塔

[11043] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10010,
        id2 = 80076,
        id3 = 0,
        desc = "[PathArea,天绝塔,10010,300114]",
	}
}
--无名老僧

[11044] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10107025,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,扫地僧,10010,10109]",
	}
}
--一臂之力
[11052] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10010,
        id2 = 80115,
        id3 = 0,
        desc = "[PathArea,东平郡王府,10010,300755]",
	}
}
