module("quest_group.qg900073",package.seeall)
name = "??"
id = 900073
--毙其塔哨
[1830] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20102021,
        id2 = 9,
        id3 = 0,
        desc = "击败[PathMon,守望塔哨兵,10004,100749]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20102030,
        id2 = 9,
        id3 = 0,
        desc = "击败[PathMon,守望塔指挥官,10004,100750]",
	}
}
--粮草先行
[1864] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20102020,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,粮草押运兵,10004,100720]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20102041,
        id2 = 9,
        id3 = 0,
        desc = "[PathMon,粮草押运官,10004,100721]",
	}
	[3] = {
        idx = 3,
        type = 1,--1
        id1 = 20102040,
        id2 = 3,
        id3 = 0,
        desc = "[PathMon,西夏游击将军,10004,100722]",
	}
}
--迎头痛击
[1865] = {
	[1] = {
        idx = 2,
        type = 2,--2
        id1 = 20102026,
        id2 = 1,
        id3 = 0,
        desc = "[PathMon,赫连达飞,10004,844]",
	}
}
--情法两难
[1870] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102015,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,韩莹莹,10004,191]。",
	}
}
--奇峰迭起
[1871] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 15004,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,韩莹莹,10004,191]对话。",
	}
}
--决战
[1872] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 15006,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,林烽火,10004,20]参与总攻",
	}
}
--得胜
[1873] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102017,
        id2 = 0,
        id3 = 0,
        desc = "去找[PathNpc,林烽火,10004,20]复命",
	}
}
--青龙现踪
[1880] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10102011,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,韩学信,10004,192]对话。",
	}
}
--众说纷纭
[19421] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102013,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,唐青衫,10004,195]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102005,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,韩振天,10004,193]",
	}
	[3] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10102015,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,韩莹莹,10004,191]",
	}
}
--如实上报
[19422] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10102011,
        id2 = 0,
        id3 = 0,
        desc = "回报[PathNpc,韩学信,10004,192]",
	}
}
