module("quest_group.qg901001",package.seeall)
name = "??"
id = 901001
--关怀
[10192] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117003,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,解俊,10003,3]",
	}
}
--关怀
[10193] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10117005,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,苏紫菡,10003,5]",
	}
}
--关怀
[10194] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117004,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,冉敬宏,10003,4]",
	}
}
--辞行
[28851] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 12206,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,李阳白,10003,1]",
	}
}
--叮嘱
[28852] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117002,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,秦玲,10003,2]",
	}
}
--督促技艺
[28853] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117006,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,裘墨阳,10003,6]",
	}
}
--心有灵犀
[28854] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,打坐一分钟,10003,6]",
	}
}
--疑问
[28855] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10117007,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,骆子渔,10003,7]",
	}
}
--求证
[28856] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10117008,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,莫奇,10003,8]",
	}
}
