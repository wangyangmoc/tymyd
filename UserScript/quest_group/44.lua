module("quest_group.qg44",package.seeall)
name = "天香笺之悄无声息"
id = 44
--相见恨晚-铸神谷主
[1341] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105165,
        id2 = 0,
        id3 = 0,
        pid = 1317,
        desc = "和[PathNpc,齐落竹,10011,1627]交谈",
	}
}
--相见恨晚-等待片刻
[1342] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        pid = 1317,
        desc = "等待齐落竹",
	}
}
--相见恨晚-君心何在
[1343] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10105165,
        id2 = 0,
        id3 = 0,
        pid = 1317,
        desc = "和[PathNpc,齐落竹,10011,1627]交谈",
	}
}
--江天一色
[1318] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101010,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,明月心,10001,927]交谈",
	}
}
--白纸无痕-寻找佳人
[1344] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119068,
        id2 = 0,
        id3 = 0,
        pid = 1319,
        desc = "找[PathNpc,白云轩,10002,2929]交谈",
	}
}
--白纸无痕-才思敏捷
[1345] = {
	[1] = {
        idx = 1,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        pid = 1319,
        desc = "等待白云轩写完",
	}
}
--白纸无痕-君心我心
[1346] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10119068,
        id2 = 0,
        id3 = 0,
        pid = 1319,
        desc = "找[PathNpc,白云轩,10002,2929]交谈",
	}
}
