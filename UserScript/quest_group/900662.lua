module("quest_group.qg900662",package.seeall)
name = "??"
id = 900662
--询问小二
[20419] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112017,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,店小二,10012,7]",
	}
}
--搜寻罪证
[20420] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 10512009,
        id2 = 1,
        id3 = 0,
        desc = "[PathArea,上锁的箱子,10012,40001]",
	}
}
--知己知彼
[20422] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112003,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,笑道人,10012,10]",
	}
}
--意外相逢
[20423] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112002,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐青枫,10012,305]",
	}
}
--闻者噤声
[20424] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112020,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,伙计,10012,12]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112021,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,账房先生,10012,11]",
	}
}
--胆大之人
[20425] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112022,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,布布,10012,13]",
	}
}
--东巷美人
[20426] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10112018,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,兰美人,10012,14]",
	}
}
--兰芷芬芳
[20427] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10512002,
        id2 = 1,
        id3 = 0,
        desc = "[PathEnt,芝兰仙,10012,40002]",
	}
	[2] = {
        idx = 1,
        type = 3,--3
        id1 = 10512003,
        id2 = 1,
        id3 = 0,
        desc = "[PathEnt,伽南香,10012,40003]",
	}
}
--珍藏宝物
[20428] = {
	[1] = {
        idx = 2,
        type = 3,--3
        id1 = 10512010,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,齐总管,10012,15]",
	}
}
--燃香为引
[20429] = {
	[1] = {
        idx = 3,
        type = 10,--10
        id1 = 10112004,
        id2 = 1,
        id3 = 0,
        desc = "[PathArea,香炉,10012,300487]",
	}
}
--婕妤召见
[20430] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112019,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,韩婕妤,10012,20]",
	}
}
--婕妤之请
[20431] = {
	[1] = {
        idx = 1,
        type = 3,--3
        id1 = 10512011,
        id2 = 1,
        id3 = 0,
        desc = "[PathEnt,木箱,10012,40013]",
	}
}
--烟火传情
[20432] = {
	[1] = {
        idx = 2,
        type = 4,--4
        id1 = 10512011,
        id2 = 1,
        id3 = 0,
        desc = "[PathArea,荷花池边,10012,300477]",
	}
}
--引路之人
[20491] = {
	[1] = {
        idx = 2,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80011,
        id3 = 0,
        desc = "[PathArea,丰乐楼,10012,300012]",
	}
}
