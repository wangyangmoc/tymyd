module("quest_group.qg304",package.seeall)
name = "千面江湖任逍遥"
id = 304
--游历杭州
[9021] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205104,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,江湖传奇 司空央,10010,13675]",
	}
}
--乐伶
[9040] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205109,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,绝代红伶 舒音,10010,9998]",
	}
}
--市井
[9041] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205107,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,八面玲珑 张三,10010,9997]",
	}
}
--商贾
[9042] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205120,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,四海巨贾 端木金,10010,10004]",
	}
}
--猎户
[9043] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205116,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,百步穿杨 苗胜天,10010,10001]",
	}
}
--镖师
[9044] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205105,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,一镖千金 万全,10010,9995]",
	}
}
--游侠
[9045] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205112,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,独行千里 迟英,10010,13676]",
	}
}
--杀手
[9046] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205113,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,三绝无命 杜枫,10010,10000]",
	}
}
--身份抉择
[9047] = {
	[1] = {
        idx = 0,
        type = 26,--26
        id1 = 0,
        id2 = 1,
        id3 = 0,
        desc = "获得江湖身份",
	}
}
--悬眼
[9048] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205106,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPC,钦定悬眼 宁武,10010,9996]",
	}
}
--捕快
[9049] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10205118,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,京城名捕 云中燕,10010,10002]",
	}
}
--文士
[9050] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20107731,
        id2 = 1,
        id3 = 0,
        desc = "[PathNPC,翰林书院 小师妹,10010,11989]",
	}
}
