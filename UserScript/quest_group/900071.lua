module("quest_group.qg900071",package.seeall)
name = "??"
id = 900071
--击破士兵
[1805] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20102003,
        id2 = 8,
        id3 = 0,
        desc = "击破[PathMon,西夏士兵,10329,100007]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20102016,
        id2 = 8,
        id3 = 0,
        desc = "击破[PathMon,西夏参将,10329,100018]",
	}
}
--寻找线索
[1806] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20102043,
        id2 = 4,
        id3 = 0,
        desc = "击破[PathMon,精锐士兵,10329,100040]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20102044,
        id2 = 1,
        id3 = 0,
        desc = "击破[PathMon,步军参谋,10329,100040]",
	}
}
--强敌入侵
[1807] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10102003,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,凌飞,10004,8]汇报",
	}
}
--刺杀
[1808] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 15001,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,凌飞,10004,8]对话进入天刀营，",
	}
}
--毫发无损
[1809] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102003,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,凌飞,10004,8]复命",
	}
}
--参见堡主
[1811] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102011,
        id2 = 0,
        id3 = 0,
        desc = "参见堡主[PathNpc,韩学信,10004,192]",
	}
}
--传令弟子
[1812] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 80005,
        id2 = 0,
        id3 = 0,
        desc = "去找[PathNpc,传令弟子,10004,404]",
	}
}
--神威双姝
[1813] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10102006,
        id2 = 0,
        id3 = 0,
        desc = "去找[PathNpc,韩思思,10004,75]",
	}
}
--铁枪周辉
[1829] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102001,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,铁枪周辉,10004,1]",
	}
}
--沙场点兵
[1832] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102170,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,程锋飞,10004,3172]",
	}
}
--龙虎兄弟
[1833] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10102169,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,廖虎,10004,3171]",
	}
}
--温习门规
[1852] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10102020,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,韩振天,10004,18]",
	}
}
--有客到访
[1853] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10102019,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,师姐周荣,10004,22]",
	}
}
--调息打坐
[1854] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "打坐一分钟",
	}
}
--神威一派
[1855] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 15013,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,周荣,10004,22]",
	}
}
