module("quest_group.qg900001",package.seeall)
name = "??"
id = 900001
--循迹追凶
[17] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80025,
        id3 = 0,
        desc = "前往[Map_X_Y,九华后山,10001,2918,3079] ",
	}
}
--受伤男子
[18] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10101022,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,孟延贤,10001,133]交谈",
	}
}
--除恶务尽
[19] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20101023,
        id2 = 2,
        id3 = 0,
        desc = "[PathMon,炎堂高手,10001,100363]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20101024,
        id2 = 8,
        id3 = 0,
        desc = "[PathMon,炎堂弟子,10001,100364]",
	}
}
--意外之声
[20] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10101023,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,孟延信,10001,134]交谈",
	}
}
--往北而去
[21] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80026,
        id3 = 0,
        desc = "继续[PathNpc,上山,10001,212]追寻孟怀楚",
	}
}
--报信僧人
[22] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101028,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,僧人,10001,212]对话",
	}
}
--小憩
[31] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101010,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,明月心,10001,927]交谈",
	}
}
--打探
[32] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10101003,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,南宫无灭,10001,136]交谈",
	}
	[2] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10101025,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,司徒振廷,10001,143]交谈",
	}
}
