module("quest_group.qg901121",package.seeall)
name = "??"
id = 901121
--师妹相迎
[32600] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 18028,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,天香小师妹,10002,3513]交谈",
	}
}
--再会师姐
[32601] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10119002,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,左梁雨,10002,2392]",
	}
}
--锦衣绮罗
[32602] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119003,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,夏洛,10002,2395]",
	}
}
--绝圣弃智
[32605] = {
	[1] = {
        idx = 2,
        type = 3,--3
        id1 = 10511006,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,轩辕十四,10002,2383]",
	}
}
--巴蜀来客
[32606] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10119041,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,唐依伊,10002,2386]",
	}
}
--风尘仆仆
[32607] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119042,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,盛岚,10002,2387]",
	}
}
--筹备花会
[32631] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10119000,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,梁知音,10002,2401]",
	}
}
--心有灵犀
[32634] = {
	[1] = {
        idx = 0,
        type = 20,--20
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "在灵犀状态下打坐一分钟",
	}
}
--非如其名
[32635] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10119008,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,柳扶风,10002,2400]",
	}
}
--青龙往事
[32636] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10119000,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,梁知音,10002,2401]",
	}
}
