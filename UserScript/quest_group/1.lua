module("quest_group.qg1",package.seeall)
name = "楔子：初出茅庐上九华"
id = 1
--茶寮小憩-小憩
[31] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101010,
        id2 = 0,
        id3 = 0,
        pid = 1,
        desc = "找[PathNpc,明月心,10001,927]交谈",
	}
}
--茶寮小憩-打探
[32] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10101003,
        id2 = 0,
        id3 = 0,
        pid = 1,
        desc = "找[PathNpc,南宫无灭,10001,136]交谈",
	}
	[2] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10101025,
        id2 = 0,
        id3 = 0,
        pid = 1,
        desc = "找[PathNpc,司徒振廷,10001,143]交谈",
	}
}
--穿越竹林
[2] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 10200,
        id2 = 0,
        id3 = 0,
        desc = "找[PathNpc,卢铁涵,10001,117]带路",
	}
}
--竹林之战
[3] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101027,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,凌沐阳,10001,146]对话",
	}
}
--云笈水榭
[4] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 10210,
        id2 = 0,
        id3 = 0,
        desc = "进入[Map_X_Y,云笈水榭,10001,2703,3293]",
	}
}
--山路追凶-循迹追凶
[17] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80025,
        id3 = 0,
        pid = 6,
        desc = "前往[Map_X_Y,九华后山,10001,2918,3079] ",
	}
}
--山路追凶-受伤男子
[18] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10101022,
        id2 = 0,
        id3 = 0,
        pid = 6,
        desc = "和[PathNpc,孟延贤,10001,133]交谈",
	}
}
--山路追凶-除恶务尽
[19] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20101023,
        id2 = 2,
        id3 = 0,
        pid = 6,
        desc = "[PathMon,炎堂高手,10001,100363]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20101024,
        id2 = 8,
        id3 = 0,
        pid = 6,
        desc = "[PathMon,炎堂弟子,10001,100364]",
	}
}
--山路追凶-意外之声
[20] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10101023,
        id2 = 0,
        id3 = 0,
        pid = 6,
        desc = "和[PathNpc,孟延信,10001,134]交谈",
	}
}
--继续搜寻
[7] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 10201,
        id2 = 0,
        id3 = 0,
        desc = "[Map_X_Y,搜索院子,10001,3034,2987] ",
	}
}
--向山而行-往北而去
[21] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80026,
        id3 = 0,
        pid = 8,
        desc = "继续[PathNpc,上山,10001,212]追寻孟怀楚",
	}
}
--向山而行-报信僧人
[22] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10101028,
        id2 = 0,
        id3 = 0,
        pid = 8,
        desc = "和[PathNpc,僧人,10001,212]对话",
	}
}
--山寺之战
[9] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 26003,
        id2 = 0,
        id3 = 0,
        desc = "到达[Map_X_Y,化清寺,10001,2952,2757]",
	}
}
--扑朔迷离
[10] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 26004,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,燕南飞,10001,137]交谈",
	}
}
--返回师门
[11] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103001,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,许长生,10009,1]交谈",
	}
}
--返回师门
[12] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10004,
        id2 = 80160,
        id3 = 0,
        desc = "[PathArea,神威堡,10004,301136]",
	}
}
--返回师门
[13] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10003,
        id2 = 80049,
        id3 = 0,
        desc = "[PathNpc,聚义厅,10003,1]",
	}
}
--返回师门
[14] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10116001,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPCLayer,唐慧,10005,1,1]",
	}
}
--返回师门
[15] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80007,
        id3 = 0,
        desc = "[PathArea,真武山,10007,300007]",
	}
}
--返回师门
[16] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10002,
        id2 = 80209,
        id3 = 0,
        desc = "[PathNpc,返回天香,10002,2374]",
	}
}
--返回师门
[23] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10006,
        id2 = 80105,
        id3 = 0,
        desc = "[PathNpc,返回五毒,10006,1422]",
	}
}
