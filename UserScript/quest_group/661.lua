module("quest_group.qg661",package.seeall)
name = "龙首成谜追渊源"
id = 661
--辞别故友-明月无心
[20400] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10109135,
        id2 = 0,
        id3 = 0,
        pid = 19801,
        desc = "[PathNpc,燕南飞,10013,642]",
	}
}
--辞别故友-徒增伤感
[20401] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10109037,
        id2 = 0,
        id3 = 0,
        pid = 19801,
        desc = "[PathNpc,傅红雪,10013,91]",
	}
}
--初入汴京-飞霞野渡
[20402] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80005,
        id3 = 0,
        pid = 19802,
        desc = "到达[PathArea,开封飞霞驿,10012,300006]",
	}
}
--初入汴京-天波故人
[20403] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112006,
        id2 = 0,
        id3 = 0,
        pid = 19802,
        desc = "[PathNpc,黄元文,10012,3]",
	}
}
--初入汴京-寻找悬眼
[20479] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112289,
        id2 = 0,
        id3 = 0,
        pid = 19802,
        desc = "[PathNpc,悬眼甲子,10012,1701]",
	}
}
--初入汴京-眼线众多
[20480] = {
	[1] = {
        idx = 3,
        type = 3,--3
        id1 = 10512015,
        id2 = 1,
        id3 = 0,
        pid = 19802,
        desc = "[PathNpc,悬眼乙卯,10012,1702]",
	}
}
--初入汴京-一路向前
[20481] = {
	[1] = {
        idx = 4,
        type = 3,--3
        id1 = 10512014,
        id2 = 1,
        id3 = 0,
        pid = 19802,
        desc = "[PathNpc,悬眼丙辰,10012,1703]",
	}
}
--初入汴京-悬眼所指
[20482] = {
	[1] = {
        idx = 5,
        type = 3,--3
        id1 = 10512013,
        id2 = 1,
        id3 = 0,
        pid = 19802,
        desc = "[PathNpc,悬眼丁亥,10012,1704]",
	}
}
--河岸小镇-村口问路
[20483] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112305,
        id2 = 0,
        id3 = 0,
        pid = 19803,
        desc = "[PathNpc,路边书生,10012,1812]",
	}
}
--河岸小镇-举手之劳
[20484] = {
	[1] = {
        idx = 1,
        type = 10,--10
        id1 = 10112002,
        id2 = 3,
        id3 = 0,
        pid = 19803,
        desc = "[PathEnt,翠一品,10012,40026]",
	}
}
--河岸小镇-书生指路
[20485] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10112305,
        id2 = 0,
        id3 = 0,
        pid = 19803,
        desc = "[PathNpc,朱肖肖,10012,1812]",
	}
}
--小径遇刺
[19804] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16400,
        id2 = 0,
        id3 = 0,
        desc = "赶往[PathArea,朱仙镇,10012,300007]",
	}
}
--暗潮之下-万里扬沙
[20404] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112005,
        id2 = 0,
        id3 = 0,
        pid = 19805,
        desc = "[PathNpc,离玉堂,10012,4]",
	}
}
--暗潮之下-黑帮头目
[20405] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112010,
        id2 = 0,
        id3 = 0,
        pid = 19805,
        desc = "[PathNpc,陆十八,10012,5]",
	}
}
--情报买卖-狮子开口
[20406] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112005,
        id2 = 0,
        id3 = 0,
        pid = 19806,
        desc = "[PathNpc,离玉堂,10012,4]",
	}
}
--情报买卖-审时度势
[20407] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112010,
        id2 = 0,
        id3 = 0,
        pid = 19806,
        desc = "[PathNpc,陆十八,10012,5]",
	}
}
--剪除耳目-乔装之人
[20408] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20112001,
        id2 = 8,
        id3 = 0,
        pid = 19807,
        desc = "[PathMon,朱仙镇地痞,11511,401]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20112002,
        id2 = 4,
        id3 = 0,
        pid = 19807,
        desc = "[PathMon,地痞头目,11511,406]",
	}
}
--剪除耳目-香主真面
[20409] = {
	[1] = {
        idx = 2,
        type = 2,--2
        id1 = 20112034,
        id2 = 1,
        id3 = 0,
        pid = 19807,
        desc = "[PathMon,千机子,11511,426]",
	}
}
--西水关外-速回报信
[20410] = {
	[1] = {
        idx = 5,
        type = 3,--3
        id1 = 10512008,
        id2 = 1,
        id3 = 0,
        pid = 19808,
        desc = "[PathNpc,离玉堂,10012,4]",
	}
}
--西水关外-前往汴京
[20411] = {
	[1] = {
        idx = 6,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80043,
        id3 = 0,
        pid = 19808,
        desc = "[PathArea,离开朱仙镇,10012,300482]",
	}
}
--西水关外-霜堂阻路
[20412] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20112003,
        id2 = 12,
        id3 = 0,
        pid = 19808,
        desc = "[PathMon,龙尾帮众,10012,100145]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20112004,
        id2 = 12,
        id3 = 0,
        pid = 19808,
        desc = "[PathMon,龙尾刺客,10012,100139]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20112005,
        id2 = 8,
        id3 = 0,
        pid = 19808,
        desc = "[PathMon,龙尾精锐,10012,100021]",
	}
}
--西水关外-关外恶战
[20476] = {
	[1] = {
        idx = 3,
        type = 1,--1
        id1 = 20112008,
        id2 = 12,
        id3 = 0,
        pid = 19808,
        desc = "[PathMon,龙爪精锐,10012,100041]",
	}
	[2] = {
        idx = 4,
        type = 1,--1
        id1 = 20112007,
        id2 = 12,
        id3 = 0,
        pid = 19808,
        desc = "[PathMon,龙爪刺客,10012,100038]",
	}
}
--东京梦华-繁华之都
[20413] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10012,
        id2 = 80007,
        id3 = 0,
        pid = 19809,
        desc = "[PathArea,开封西门,10012,300008]",
	}
}
--东京梦华-古怪委托
[20414] = {
	[1] = {
        idx = 1,
        type = 4,--4
        id1 = 10512008,
        id2 = 1,
        id3 = 0,
        pid = 19809,
        desc = "[PathNpc,包袱,10012,300008]",
	}
}
--江湖有情-手足之情
[20415] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10112015,
        id2 = 0,
        id3 = 0,
        pid = 19810,
        desc = "[PathNpc,陆山川,10012,6]",
	}
}
--江湖有情-孝子探亲
[20416] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10112016,
        id2 = 0,
        id3 = 0,
        pid = 19810,
        desc = "[PathNpc,陆大娘,10012,8]",
	}
}
