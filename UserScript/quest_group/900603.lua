module("quest_group.qg900603",package.seeall)
name = "??"
id = 900603
--担惊受怕
[18340] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111057,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,惊慌的行商,10009,545]",
	}
}
--言颠语倒
[18341] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10111059,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,太白弟子,10009,547]",
	}
}
--罪魁祸首
[18342] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20111045,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,无极会精英,10009,101157]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20111044,
        id2 = 8,
        id3 = 0,
        desc = "[PathMon,无极会密探,10009,101157]",
	}
}
--天山往事
[18343] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111031,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,钟舒文,10009,558]",
	}
}
--虚张声势
[18344] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10111008,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,公孙剑,10009,559]",
	}
}
--单刀赴会
[18345] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20111056,
        id2 = 3,
        id3 = 0,
        desc = "[PathMon,青龙会狂徒,10009,101203]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20111054,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,青龙会杀手,10009,101203]",
	}
	[3] = {
        idx = 4,
        type = 1,--1
        id1 = 20111055,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,青龙会刺客,10009,101203]",
	}
}
--长驱深入
[18346] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80086,
        id3 = 0,
        desc = "[PathArea,醉白池前,10009,300609]",
	}
}
--云飞烟灭
[18347] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20111057,
        id2 = 6,
        id3 = 0,
        desc = "[PathMon,青龙会精锐,10009,101237]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20111058,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,青龙会斥候,10009,101237]",
	}
}
--水落归槽
[18348] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111010,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,公孙剑,10009,561]",
	}
}
--不遑宁息
[18349] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10111013,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,独孤若虚,10009,562]",
	}
}
--太白驿站
[18350] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10111033,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,何荐华,10009,568]",
	}
}
