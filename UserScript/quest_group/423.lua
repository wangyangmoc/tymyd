module("quest_group.qg423",package.seeall)
name = "第十八回·血衣秘境侠影忙"
id = 423
--先到一步
[12612] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108057,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,江山,10001,464]交谈",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10108056,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,江婉儿,10001,463]交谈",
	}
	[3] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10108058,
        id2 = 0,
        id3 = 0,
        desc = "和[PathNpc,韩振松,10001,465]交谈",
	}
}
--再遇二人-叙旧
[12731] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108054,
        id2 = 0,
        id3 = 0,
        pid = 12613,
        desc = "找到[PathNpc,燕南飞,10001,344]",
	}
	[2] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10108053,
        id2 = 0,
        id3 = 0,
        pid = 12613,
        desc = "找到[PathNpc,明月心,10001,342]",
	}
}
--再遇二人-采芦
[12732] = {
	[1] = {
        idx = 2,
        type = 10,--10
        id1 = 10101008,
        id2 = 10,
        id3 = 0,
        pid = 12613,
        desc = "采摘[PathNpc,芦苇,10001,40045]",
	}
}
--再遇二人-归来
[12733] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10108053,
        id2 = 0,
        id3 = 0,
        pid = 12613,
        desc = "找到[PathNpc,明月心,10001,342]",
	}
}
--保障后方-意外之变
[12687] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108063,
        id2 = 0,
        id3 = 0,
        pid = 12614,
        desc = "询问[PathNpc,唐青枫,10001,346]",
	}
}
--保障后方-刺探情报
[12691] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 12112,
        id2 = 0,
        id3 = 0,
        pid = 12614,
        desc = "找回[Map_X_Y,探子,10001,1640,1805]",
	}
}
--加入战局-化解危机
[12694] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20108046,
        id2 = 12,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,先锋杀手,10001,101850]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20108047,
        id2 = 12,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,先锋刺客,10001,101850]",
	}
	[3] = {
        idx = 7,
        type = 1,--1
        id1 = 20108048,
        id2 = 12,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,杀手高手,10001,101457]",
	}
	[4] = {
        idx = 2,
        type = 1,--1
        id1 = 20108049,
        id2 = 9,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,刺客高手,10001,101457]",
	}
}
--加入战局-局势缓解
[12695] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10108071,
        id2 = 0,
        id3 = 0,
        pid = 12615,
        desc = "找到[PathNpc,唐青枫,10001,367]",
	}
}
--加入战局-救援·壹
[12696] = {
	[1] = {
        idx = 4,
        type = 1,--1
        id1 = 20108031,
        id2 = 18,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,雷堂弟子,10001,101483]",
	}
	[2] = {
        idx = 5,
        type = 1,--1
        id1 = 20108032,
        id2 = 18,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,雷堂精锐,10001,101483]",
	}
	[3] = {
        idx = 6,
        type = 1,--1
        id1 = 20108033,
        id2 = 6,
        id3 = 0,
        pid = 12615,
        desc = "[PathMon,雷堂高手,10001,101483]",
	}
}
--攻其不备-侧面击破
[12701] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20108055,
        id2 = 16,
        id3 = 0,
        pid = 12616,
        desc = "[PathMon,血衣楼守卫,10001,101670]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20108056,
        id2 = 12,
        id3 = 0,
        pid = 12616,
        desc = "[PathMon,血衣楼守卫高手,10001,101670]",
	}
}
--攻其不备-雷堂堂主
[12803] = {
	[1] = {
        idx = 2,
        type = 2,--2
        id1 = 20108050,
        id2 = 1,
        id3 = 0,
        pid = 12616,
        desc = "[PathMon,屠越龙,10001,414]",
	}
}
--攻其不备-不幸消息
[12702] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10108073,
        id2 = 0,
        id3 = 0,
        pid = 12616,
        desc = "[PathNpc,唐青枫,10001,369]",
	}
}
--攻其不备-迷魂解药
[12703] = {
	[1] = {
        idx = 4,
        type = 3,--3
        id1 = 10501020,
        id2 = 4,
        id3 = 0,
        pid = 12616,
        desc = "[PathMon,鬼点头,10001,416]",
	}
}
--攻其不备-解救同道
[12704] = {
	[1] = {
        idx = 5,
        type = 4,--4
        id1 = 10501020,
        id2 = 4,
        id3 = 0,
        pid = 12616,
        desc = "营救[PathNpc,正派高手,10001,375]",
	}
}
--意外之变-暗号之声
[12705] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108076,
        id2 = 0,
        id3 = 0,
        pid = 12617,
        desc = "找到[PathNpc,江山,10001,466]",
	}
}
--意外之变-往西而去
[12706] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10108074,
        id2 = 0,
        id3 = 0,
        pid = 12617,
        desc = "找到[PathNpc,唐青枫,10001,370]",
	}
}
--意外之变-投石问路
[12707] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20108059,
        id2 = 16,
        id3 = 0,
        pid = 12617,
        desc = "[PathMon,禁地拳师,10001,101702]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20108060,
        id2 = 12,
        id3 = 0,
        pid = 12617,
        desc = "[PathMon,禁地高手,10001,101702]",
	}
}
--战事先锋-血衣探子
[12692] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20108028,
        id2 = 15,
        id3 = 0,
        pid = 12724,
        desc = "[PathMon,血衣楼探子,10001,101379]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20108029,
        id2 = 15,
        id3 = 0,
        pid = 12724,
        desc = "[PathMon,血衣楼刺客,10001,101379]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20108030,
        id2 = 5,
        id3 = 0,
        pid = 12724,
        desc = "[PathMon,探子头目,10001,101379]",
	}
}
--战事先锋-回报八荒
[12693] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10108063,
        id2 = 0,
        id3 = 0,
        pid = 12724,
        desc = "和[PathNpc,唐青枫,10001,346]交谈",
	}
}
--战事先锋-向北而行
[12712] = {
	[1] = {
        idx = 4,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80043,
        id3 = 0,
        pid = 12724,
        desc = "赶往[Map_X_Y,嘉荫镇后门,10001,1835,1533]",
	}
}
--阵地中枢-救援·贰
[12697] = {
	[1] = {
        idx = 0,
        type = 1,--1
        id1 = 20108034,
        id2 = 6,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,炎堂弟子,10001,101519]",
	}
	[2] = {
        idx = 1,
        type = 1,--1
        id1 = 20108035,
        id2 = 12,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,炎堂精锐,10001,101519]",
	}
	[3] = {
        idx = 2,
        type = 1,--1
        id1 = 20108036,
        id2 = 3,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,炎堂高手,10001,101519]",
	}
}
--阵地中枢-救援·叁
[12698] = {
	[1] = {
        idx = 3,
        type = 1,--1
        id1 = 20108037,
        id2 = 15,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,影堂弟子,10001,101573]",
	}
	[2] = {
        idx = 4,
        type = 1,--1
        id1 = 20108038,
        id2 = 15,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,影堂精锐,10001,101573]",
	}
	[3] = {
        idx = 5,
        type = 1,--1
        id1 = 20108039,
        id2 = 5,
        id3 = 0,
        pid = 12725,
        desc = "[PathMon,影堂高手,10001,101573]",
	}
}
--深入敌营-侦查归来
[12699] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108072,
        id2 = 0,
        id3 = 0,
        pid = 12726,
        desc = "找到[PathNpc,唐青枫,10001,368]",
	}
}
--深入敌营-寻得捷径
[12700] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80039,
        id3 = 0,
        pid = 12726,
        desc = "找到[Map_X_Y,秘密入口,10001,2648,1637]",
	}
}
--攻入总舵-强弱悬殊
[12708] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10108074,
        id2 = 0,
        id3 = 0,
        pid = 12727,
        desc = "和[PathNpc,唐青枫,10001,370]对话",
	}
}
--攻入总舵-深入总舵
[12709] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10108175,
        id2 = 0,
        id3 = 0,
        pid = 12727,
        desc = "和[PathNpc,叶知秋,10001,55116]对话",
	}
}
--攻入总舵-加入战斗
[12735] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20108080,
        id2 = 8,
        id3 = 0,
        pid = 12727,
        desc = "[PathMon,总舵精锐,10001,101888]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20108079,
        id2 = 16,
        id3 = 0,
        pid = 12727,
        desc = "[PathMon,总舵杀手,10001,101888]",
	}
	[3] = {
        idx = 4,
        type = 1,--1
        id1 = 20108081,
        id2 = 16,
        id3 = 0,
        pid = 12727,
        desc = "[PathMon,总舵死士,10001,101888]",
	}
}
--攻入总舵-稍作休憩
[12736] = {
	[1] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10108175,
        id2 = 0,
        id3 = 0,
        pid = 12727,
        desc = "和[PathNpc,叶知秋,10001,55116]对话",
	}
}
--攻入总舵-总舵内部
[12710] = {
	[1] = {
        idx = 6,
        type = 21,--21
        id1 = 12116,
        id2 = 0,
        id3 = 0,
        pid = 12727,
        desc = "找到[PathNpc,唐青枫,10001,300734]",
	}
}
--江湖唏嘘-返回镇上
[12737] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10001,
        id2 = 80155,
        id3 = 0,
        pid = 12728,
        desc = "返回[PathNpc,嘉荫镇,10001,300835]",
	}
}
--江湖唏嘘-故人相遇
[12689] = {
	[1] = {
        idx = 1,
        type = 21,--21
        id1 = 12114,
        id2 = 0,
        id3 = 0,
        pid = 12728,
        desc = "和[PathNpc,傅红雪,10001,300834]辞别",
	}
}
--江湖唏嘘-四盟相聚
[12739] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10108181,
        id2 = 0,
        id3 = 0,
        pid = 12728,
        desc = "和[PathNpc,叶知秋,10001,55155]",
	}
	[2] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10108182,
        id2 = 0,
        id3 = 0,
        pid = 12728,
        desc = "和[PathNpc,唐青枫,10001,55156]",
	}
	[3] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10108183,
        id2 = 0,
        id3 = 0,
        pid = 12728,
        desc = "和[PathNpc,离玉堂,10001,55157]",
	}
	[4] = {
        idx = 5,
        type = 7,--PathNpc
        id1 = 10108184,
        id2 = 0,
        id3 = 0,
        pid = 12728,
        desc = "和[PathNpc,曲无忆,10001,55158]",
	}
}
--精进武艺
[12729] = {
	[1] = {
        idx = 0,
        type = 23,--23
        id1 = 60,
        id2 = 0,
        id3 = 0,
        desc = "等级到达60",
	}
}
--界限突破肆
[12730] = {
	[1] = {
        idx = 0,
        type = 28,--28
        id1 = 4,
        id2 = 0,
        id3 = 0,
        desc = "等待突破：九华叠翠(60级)",
	}
}
--九华叠翠
[12750] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10013,
        id2 = 80087,
        id3 = 0,
        desc = "寻[PathNpc,车夫,10001,947]前往[PathNpc,古陶驿站,10013,2]",
	}
}
