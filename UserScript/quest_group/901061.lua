module("quest_group.qg901061",package.seeall)
name = "??"
id = 901061
--门内寒暄
[31200] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118047,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,凌玄,10007,81]",
	}
}
--严厉师叔
[31201] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118015,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,一云子,10007,3]",
	}
}
--回禀师尊
[31203] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10118013,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,张梦白,10007,2]",
	}
}
--悟剑之法
[31204] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10118001,
        id2 = 0,
        id3 = 0,
        desc = "[PathNPCLayer,笑道人,10007,1,1]",
	}
}
--师兄师弟
[31237] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118056,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,潇湘子,10007,195]",
	}
}
--珠帘洞内
[31241] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 16300,
        id2 = 0,
        id3 = 0,
        desc = "[PathArea,真武道场,10007,300008]",
	}
}
--太极道场
[31242] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80032,
        id3 = 0,
        desc = "[PathArea,太极道场,10007,300044]",
	}
}
--巡视道场
[31243] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10007,
        id2 = 80066,
        id3 = 0,
        desc = "[PathNPCLayer,巡视道场,10007,300423,1]",
	}
}
--真武剑道
[31248] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10118013,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,张梦白,10007,2]",
	}
}
--拜见师尊
[31250] = {
	[1] = {
        idx = 4,
        type = 21,--21
        id1 = 16307,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,一云子,10007,3]",
	}
}
