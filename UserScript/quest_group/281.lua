module("quest_group.qg281",package.seeall)
name = "终不忘世外仙姝寂寞林"
id = 281
--路不平·素手救武僧
[10201] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105433,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--路不平·素手救武僧
[10202] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10002,
        id2 = 80131,
        id3 = 0,
        desc = "",
	}
}
--找太白师兄
[10209] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105439,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--找村民
[10210] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10105440,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--找佩剑
[10211] = {
	[1] = {
        idx = 0,
        type = 10,--10
        id1 = 10107509,
        id2 = 1,
        id3 = 0,
        desc = "",
	}
}
--茶娘
[10213] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10002,
        id2 = 80132,
        id3 = 0,
        desc = "",
	}
}
--茶娘
[10214] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 20106120,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
--龙首山话本
[10216] = {
	[1] = {
        idx = 0,
        type = 21,--21
        id1 = 11001,
        id2 = 0,
        id3 = 0,
        desc = "",
	}
}
