module("quest_group.qg900362",package.seeall)
name = "??"
id = 900362
--凤落财神
[5605] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104029,
        id2 = 0,
        id3 = 0,
        desc = "查看[PathNpc,刺客尸体,10010,9530]	",
	}
}
--报讯解围
[5606] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10010,
        id2 = 80021,
        id3 = 0,
        desc = "火速赶往[PathArea,财神阁,10010,300021]	",
	}
}
--入财神阁
[5623] = {
	[1] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10104019,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,财神阁守卫,10010,74]",
	}
}
--金眼布防
[5624] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10104016,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,白言,10010,60]交谈	",
	}
}
--江湖助拳
[5625] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10104017,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,莫川,10010,9543]交谈	",
	}
}
--财神武器
[5626] = {
	[1] = {
        idx = 0,
        type = 3,--3
        id1 = 10504004,
        id2 = 5,
        id3 = 0,
        desc = "拿取[PathEnt,财神阁武器,10010,40001]",
	}
}
--分发兵器
[5627] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104088,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,唐士忠,10010,9990]",
	}
	[2] = {
        idx = 2,
        type = 7,--PathNpc
        id1 = 10104089,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,于百石,10010,9991]",
	}
	[3] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10104090,
        id2 = 0,
        id3 = 0,
        desc = "[PathNpc,瑞云同,10010,9992]",
	}
}
--财神秘辛
[5641] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10104016,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,白言,10010,60]交谈",
	}
}
--银煞指路
[5642] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10104067,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,银煞,10010,9845]",
	}
}
