module("quest_group.qg900601",package.seeall)
name = "??"
id = 900601
--秦川冻原
[18300] = {
	[1] = {
        idx = 1,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80082,
        id3 = 0,
        desc = "[PathArea,赶往秦川,10009,300605]",
	}
}
--太白弟子
[18301] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111001,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,孙晓倩,10009,499]",
	}
}
--青龙扰世
[18302] = {
	[1] = {
        idx = 2,
        type = 1,--1
        id1 = 20111001,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,寒露殿探子,10009,100806]",
	}
	[2] = {
        idx = 3,
        type = 1,--1
        id1 = 20111002,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,寒露殿斥候,10009,100806]",
	}
}
--抵达桥边
[18303] = {
	[1] = {
        idx = 0,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80083,
        id3 = 0,
        desc = "[PathArea,桥边,10009,300606]",
	}
}
--密探尸体
[18304] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10111017,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,霜堂探子尸体,10009,500]",
	}
}
--血洒湖畔
[18305] = {
	[1] = {
        idx = 2,
        type = 10,--10
        id1 = 10111005,
        id2 = 5,
        id3 = 0,
        desc = "[PathEnt,血迹,10009,40008]",
	}
}
--巧遇师姐
[18306] = {
	[1] = {
        idx = 3,
        type = 7,--PathNpc
        id1 = 10111070,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,江婉儿,10009,1210]",
	}
}
--飞鸿印雪
[18307] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111070,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,江婉儿,10009,1210]",
	}
}
--寒露为霜
[18308] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20111004,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,寒露殿刺客,10009,100815]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20111003,
        id2 = 6,
        id3 = 0,
        desc = "[PathMon,寒露殿镖客,10009,100815]",
	}
}
--囊血射天
[18309] = {
	[1] = {
        idx = 3,
        type = 1,--1
        id1 = 20111006,
        id2 = 4,
        id3 = 0,
        desc = "[PathMon,寒露殿剑客,10009,100876]",
	}
	[2] = {
        idx = 4,
        type = 1,--1
        id1 = 20111005,
        id2 = 16,
        id3 = 0,
        desc = "[PathMon,寒露殿伏击手,10009,100876]",
	}
}
--金玉其外
[18310] = {
	[1] = {
        idx = 5,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80080,
        id3 = 0,
        desc = "[PathArea,玉泉院,10009,300603]",
	}
}
--微弱呼救
[18311] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111019,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,重伤的道士,10009,501]",
	}
}
--心狠手辣
[18312] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20111007,
        id2 = 6,
        id3 = 0,
        desc = "[PathMon,寒露殿投手,10009,100902]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20111008,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,寒露殿女贼,10009,100902]",
	}
}
--后院杀机
[18313] = {
	[1] = {
        idx = 3,
        type = 1,--1
        id1 = 20111009,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,寒露殿弟子,10009,100919]",
	}
	[2] = {
        idx = 4,
        type = 1,--1
        id1 = 20111010,
        id2 = 3,
        id3 = 0,
        desc = "[PathMon,寒露殿精锐,10009,100919]",
	}
}
--太白云海
[18314] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111061,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,公孙剑,10009,579]",
	}
}
--有始有终
[18316] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10111020,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,唐林,10009,528]",
	}
}
--青竹丹枫
[18317] = {
	[1] = {
        idx = 4,
        type = 7,--PathNpc
        id1 = 10111021,
        id2 = 1,
        id3 = 0,
        desc = "[PathNpc,唐青枫,10009,529]",
	}
}
--北斗坪深
[18318] = {
	[1] = {
        idx = 3,
        type = 8,--PathArea
        id1 = 10009,
        id2 = 80137,
        id3 = 0,
        desc = "[PathArea,出去查探,10009,300868]",
	}
}
--流风回雪
[18351] = {
	[1] = {
        idx = 4,
        type = 10,--10
        id1 = 10111006,
        id2 = 5,
        id3 = 0,
        desc = "[PathEnt,雪里飞,10009,40170]",
	}
}
--青龙过隙
[18352] = {
	[1] = {
        idx = 1,
        type = 1,--1
        id1 = 20111023,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,乾坤会伏兵,10009,100949]",
	}
	[2] = {
        idx = 2,
        type = 1,--1
        id1 = 20111022,
        id2 = 12,
        id3 = 0,
        desc = "[PathMon,乾坤会刺客,10009,100949]",
	}
}
