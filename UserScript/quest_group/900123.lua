module("quest_group.qg900123",package.seeall)
name = "??"
id = 900123
--询问楚鸿
[3915] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103019,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,楚鸿,10009,26]",
	}
}
--询问石傅心
[3916] = {
	[1] = {
        idx = 1,
        type = 7,--PathNpc
        id1 = 10103040,
        id2 = 0,
        id3 = 0,
        desc = "询问[PathNpc,石傅心,10009,209]",
	}
}
--一臂之力
[3936] = {
	[1] = {
        idx = 1,
        type = 19,--19
        id1 = 11026,
        id2 = 1,
        id3 = 0,
        desc = "与[PathNpc,公孙剑,10009,323]交谈",
	}
}
--急相告
[3937] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103002,
        id2 = 0,
        id3 = 0,
        desc = "速下山寻掌门[PathNpc,风无痕,10009,21]",
	}
}
--御风神行
[3943] = {
	[1] = {
        idx = 0,
        type = 7,--PathNpc
        id1 = 10103002,
        id2 = 0,
        id3 = 0,
        desc = "与[PathNpc,风无痕,10009,21]交谈",
	}
}
