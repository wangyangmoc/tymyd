local sleep= msk.sleep

function RunTutral()
	if qs.GetMainplayerLevel() < 40 then return end
	g_allQuests = nil
	local isContinue = false
	for i=10701,10720 do
		if i ~= 10702 and msk.quest.RunTargetQuests(i) then
			isContinue = true
		end
	end
	sleep(1)
	if isContinue then return RunTutral() end
end

local function BuyVip()
	LogDebuging("检测会员")
	local money,bindMoney,bindPoint,tsJifen = msk.item.GetMoney()
	if  msk.config.beVipAfter41 == 1 and qs.CheckActorVip(0) == false and bindPoint >= 1280  then
		WaitMoneyProtect()
		LogDebuging("准备开会员")
		BuyShangChengItem(0x74,1,1280)
		sleep(2)
		msk.item.CheckBag()
		sleep(1)
	else
		LogDebuging("开VIP失败:配置:%s 已经是VIP:%s 绑定点券:%d",msk.config.beVipAfter41 == 1 and "是" or "否",qs.CheckActorVip(0) and "是" or "否",bindPoint)
	end
end
--LogDebuging("测试内存读写")
--msk.dword(0x123)
local qqConfig =  msk.tools.GetConfig()
if not qqConfig then
	LogDebuging("没有指定的脚本配置:%s",g_qq)
	g_Exit("没有指定的脚本配置")
	--msk.api.QuitGame()
	return
end

msk.hook.skipEvent()

if msk.api.GetCurrentMapId() == 0 or msk.api.GetCurrentMapId() == 10050 then
	LogDebuging("游戏已启动...")
	local map,mname,inlineNum
	repeat
		map,mname = msk.waitEvent("enterMap",10)
		if map ~= nil then break end
		if UIIsVisible("QSUIDialogPanel") then
			local dp = msk.ui.GetPanel("QSUIDialogPanel")
			local txt = dp and dp[1]
			txt = txt and txt.msgtext.text or "nil"
			LogDebuging("txt:%s",txt)
			if txt == "<p></p>服务器断开连接，请重新登录游戏" then
				if g_client:hincrby("disConnect",g_qq,1) > 10 then
					g_Exit("服务器拒绝登录")
				else
					msk.api.QuitGame("连接失败")
				end
			else
				local ftyear,ftmon,ftday,fthour,ftmin,ftsec = txt:match("(%d+)年(%d+)月(%d+)日(%d+)点(%d+)分(%d+)秒")
				if ftyear then
					local fttm = {
						year = ftyear,month = ftmon,day = ftday,			
						hour = fthour,min = ftmin, sec = ftsec,
					}
					fttm = os.time(t)
					g_client:hset("fttm",g_qq,fttm)
				end
				g_Exit("可能封号")
			end
		end
	until false
	LogDebuging("已进入选角界面...")
	assert(map == "ActorCreationScreen")
	while UIIsVisible("QSUICreateCharacterPanel") do
		CreateChar()
		sleep(3)
		UIShowWindow("QSUIDialogPanel",false)
	end
	LogDebuging("准备进入游戏...")
	repeat
		sleep(1)
	until UIIsVisible("QSUISelectCharacterPanel")
	LogDebuging("进入游戏")
	msk.api.EnterToWorld()
	map,mname = msk.waitEvent("enterMap")
	assert(map ~= "ActorCreationScreen")
	UIShowWindow("QSUIDailyCheckInPanel",false)
	sleep(5)
	LogDebuging("初始化数据")
end
msk.hook.skipEvent()
msk.api.GetAllGift()
LogDebuging("InitManDataIdOff")
msk.npc.InitManDataIdOff()
g_mainId = qs.GetMainPlayerId()
LogDebuging("开始运行...")
g_client:hset("time",g_qq,os.time())
g_client:hset("status",g_qq,"运行...")
g_client:hset("money",g_qq,qs.GetCurMoney())
g_client:hset("name",g_qq,qs.GetMainPlayerName())
if g_statusThread == nil or coroutine.status(g_statusThread) == "dead" then
	g_statusThread = msk.newThread(msk.api.StatusCheck,coroutine.running(),msk.require,"script1",true)
end

--41以后做一下每日任务
if qs.GetMainplayerLevel() >= 41 then
	if qs.CheckActorVip(0) and msk.tools.GetDailyData("qiandao") ~= "yes" then
		UIShowWindow("QSUIDailyCheckInPanel",true)
		if UIIsVisible("QSUIDailyCheckInPanel") then
			msk.ui.UIMsgCall("QSUIDailyCheckInPanel","A2C_GET_TODAY_AWARD")
			sleep(1)
			UIShowWindow("QSUIDailyCheckInPanel",false)
			sleep(1)
			msk.tools.SetDailyData("qiandao","yes")
		end
	end
	--if msk.config.dailyLiveNess and qqConfig.script ~= "liveup" and msk.tools.GetDailyData("dangkou_liveness") ~= "yes" then
		--RunScriptFile("dangkou","liveness")--刷点功绩
		--msk.tools.SetDailyData("dangkou_liveness","yes")
	--end
	--财产保护时间
	local money,bindMoney,bindPoint,tsJifen = msk.item.GetMoney()
	if  msk.config.beVipAfter41 == 1 and qs.CheckActorVip(0) == false and bindPoint >= 1280  then
		BuyShangChengItem(0x74,1,1280)
		sleep(1)
		msk.item.CheckBag()
		sleep(1)
	end
end

msk.api.ReviveNear()
LogDebuging("执行脚本:%s",qqConfig.script)
g_cacheData.script = qqConfig.script
if qs.GetMainplayerLevel() >= 41 then
	msk.newThread(BuyVip)
end
local exitData = RunScriptFile(qqConfig.script)
--41以后做一下每日任务
if qs.GetMainplayerLevel() >= 41 then
	BuyVip()
	if msk.config.dailyLiveNess == 1 and msk.tools.GetDailyData("dangkou_liveness") ~= "yes" then
		RunScriptFile("dangkou","liveness")--刷点功绩
		msk.tools.SetDailyData("dangkou_liveness","yes")
	end
end
if msk.tools.GetDailyData("qiandao") ~= "yes" then
	UIShowWindow("QSUIDailyCheckInPanel",true)
	if UIIsVisible("QSUIDailyCheckInPanel") then
		msk.ui.UIMsgCall("QSUIDailyCheckInPanel","A2C_GET_TODAY_AWARD")
		sleep(1)
		UIShowWindow("QSUIDailyCheckInPanel",false)
		sleep(1)
		msk.tools.SetDailyData("qiandao","yes")
	end
end
if  msk.config.buyDailyGrift == 1 and qs.CheckActorVip(0) and qs.GetMainplayerLevel() >= 30 and msk.tools.GetDailyData("daily_gift") ~= "yes" then
	WaitMoneyProtect()
	msk.api.BuyShangChengItem(0x64,1,0)
	sleep(1)
	msk.tools.SetDailyData("daily_gift","yes")
end
msk.api.GetAllGift()
msk.item.CheckBag()
msk.api.ProcessMail(true)
LogDebuging("结束了。。。")
g_client:hset("name",g_qq,qs.GetMainPlayerName())
g_client:hset("lv",g_qq,qs.GetMainplayerLevel())
g_client:hset("money",g_qq,qs.GetCurMoney())
return g_Exit(exitData)