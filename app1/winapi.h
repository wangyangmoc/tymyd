#pragma once
#include <Windows.h>

auto SEFJmp = (decltype(SetUnhandledExceptionFilter) *)((DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "SetUnhandledExceptionFilter") + 11);
_declspec(naked) LPTOP_LEVEL_EXCEPTION_FILTER
WINAPI orgSetUnhandledExceptionFilter(void* lpTopLevelExceptionFilter)
{
	__asm {
		mov edi, edi
		push ebp
		mov ebp, esp
		sub esp, 0x220
		jmp SEFJmp
	}
}