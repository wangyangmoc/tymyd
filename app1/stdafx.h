// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>



// TODO: reference additional headers your program requires here
#include <WinSock2.h>
using byte = unsigned char;
#define _UTF_STR_//��ϷUI��UTF8�ִ�
#include "../common/define.h"
#include "../lua51/lua.hpp"
#include "../mhook-lib/mhook.h"
#define HOOKAPIPRE(md,fun) auto true##fun = (decltype(fun) *)GetProcAddress(GetModuleHandleA(md),#fun)
#define HOOKAPI(fun) Mhook_SetHook((LPVOID *)& true##fun, my##fun)
#define UNHOOKAPI(fun) Mhook_Unhook((LPVOID *)& true##fun)
#define HOOKAPINEW(fun) decltype(fun) my##fun
#define CAST(tp,v) (tp)(v)
#define SET(var,val) var = CAST(decltype(var),(val))

#ifndef _ISSUE1
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#define PRTW(fmt,...) {wchar_t msg##__LINE__[4096]; swprintf_s(msg##__LINE__,L"msk_td->"fmt,__VA_ARGS__); OutputDebugStringW(msg##__LINE__);}
#else
#define PRT(fmt,...)
#define PRTW(fmt,...)
#endif
#define WM_MSG WM_USER+1
extern bool g_bSysFlag;
extern HANDLE g_hCallEvent;
extern const char *g_strCallEvent;
extern const char *g_strCallArgs;
extern char g_strCmd[32];
extern char g_strArg[2046];
extern HWND g_hMsgWnd;
extern DWORD g_dwSafeCallJmp;
extern lua_State *g_cfgL;
extern lua_State *g_gameLuaState;
extern DWORD g_pause;
int glm_decodeSig(lua_State *L);
int GetConfig(const char* key);
int GetConfigTime(const char* key);
void inline_hook(int hookaddr, int newaddr, int codelen, char *code);
void inline_unhook(int hookaddr, char *code, int codelen);

#define Base_001                                0x0111D0BC
#define call_Base_001                           0x0071FAC0
#define base_002                                0x0111F398
#define Base_UI                                 0x0191F818
#define offset_FishingState                     0x00000200
#define call_FishingPaoGan                      0x006DCB60
#define call_FishingLaGan                       0x006DCBB0
extern DWORD addr_RetAddr;
extern DWORD addr_Ret4Addr;
extern DWORD addr_Ret8Addr;
extern DWORD addr_RetCAddr;
extern DWORD addr_Ret10Addr;
extern DWORD addr_Ret14Addr;
extern DWORD addr_Ret18Addr;
extern DWORD addr_Ret1CAddr;
extern DWORD addr_Ret20Addr;