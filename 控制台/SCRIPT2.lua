package.path = g_dir.."/lua/?.lua"
package.cpath = g_dir.."/clibs/?.dll"
require "std"
require "base64"
require "des56"
local redis = require "redis"
local http = require "socket.http"
local socket = require("socket")
local gvHost = false and "192.168.2.103" or "192.199.254.116"
local gvPort = 10001
local serverVertions,oldServerVertions,serverTable
function print(...)
	local str = ""
	local t= {...}
	for i=1,#t do
		str = str..tostring(t[i]).." "
	end
	MskPrint(str)
end

local function ServerAssert(v,err,f,...)
	if v then return v end
	if f then f(...) end
	error({result = 'failed',data = err},0)
end

local function ConnectServer(sdata)
	ServerAssert(nil ~= sdata.user:match("^[^']+$") and #sdata.user > 0,"输入单引号(')以外的任意字符")
	ServerAssert(nil ~= sdata.pwd:match("^[^']+$") and #sdata.pwd > 0,"输入单引号(')以外的任意字符")
	local c = ServerAssert(socket.connect(gvHost, gvPort),'连接服务器失败')
	local curHost,curPort = c:getpeername()
	ServerAssert(curHost==gvHost and curPort==gvPort,'连接服务器失败 '..curHost..' '..gvHost..' '..curPort..' '..gvPort)
	c:settimeout(5)
	--sdata.script = g_client:get("lastScript")
	--sdata.daqu = g_client:get("lastDaqu")
	--sdata.fuwuqi = g_client:get("lastFuwuqi")
	sdata = pickle(sdata)
	sdata = base64.encode(sdata)..'\n'
	ServerAssert(c:send(sdata),'尝试登录失败',c.close,c)
	local data = ServerAssert(c:receive(),'无法接受信息',c.close,c)
	c:close()
	data = base64.decode(data)
	local ret = eval(data)
	gtServerData = ret
	if ret.data.sig then
		local sigStr = "_g_sig = "..prettytostring(ret.data.sig)
		local f= io.open(g_dir.."/../tools/sigs.txt","w")
		f:write(sigStr)
		f:close()
	end
	return ret
end
function AskData(sSata)
	local ret,err = pcall(ConnectServer,sSata)
	if not ret and type(err) == 'string' then
		err = {result = 'failed',data = "脚本错误:"..err}
	end
	--print('ret',ret,err)
	return err
end

function OnExit()
	g_client = redis.connect('127.0.0.1', 6379)
	if g_client then
		g_client:shutdown()
	end
end
require "mskco"
