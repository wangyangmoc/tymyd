package.cpath = "?.dll;?51.dll;clibs/?51.dll;clibs/?.dll;"
require( "iuplua" )
require( "iupluacontrols")
local redis = require"redis"
local g_client
local function __SCRIPT5()
	g_client = redis.connect('127.0.0.1', 6379)
	local txt = iup.text {
			multiline = "YES",
			size = "200X200",
		}
	local vbox = iup.vbox {txt}
	local timer = iup.timer{time=2000}
	local __lastlen = 0
	local function __getLogLen()
		return g_client:llen("controlLog")
	end
	function timer:action_cb()
		local ok,curLoglen = pcall(__getLogLen)
		if not ok then return iup.CLOSE end
		curLoglen = curLoglen or 0
		if curLoglen ~= __lastlen then
			for i=__lastlen,curLoglen-1 do
				txt.value = txt.value..g_client:lindex("controlLog",i).."\n"
			end
			__lastlen = curLoglen
		end
	end
	local dlg = iup.dialog{vbox; title="��־"}
	dlg:show()
	timer:action_cb()
	timer.run = "YES"
	if (iup.MainLoopLevel()==0) then
	  iup.MainLoop()
	end
end

local ok,err = pcall(__SCRIPT5)
if not ok then
	iup.Message("����", err or "nil")
end