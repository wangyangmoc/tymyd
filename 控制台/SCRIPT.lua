package.path = g_dir.."/lua/?.lua"
package.cpath = g_dir.."/clibs/?.dll"
function printlmp(fmt,...)
	cPrint(string.format(tostring(fmt),...).."\n")
end
print = printlmp
g_Test = false
local Test
if g_Test then
	Test = print
else
	Test =  function () end
end
local redis = require "redis"
local json = require "dkjson"
local http = require "socket.http"
local html = require "html"

local config
local cf = io.open("config.ini")
if cf then
	data = cf:read("*all")
	config = json.decode(data)
	cf:close()
else
	config = {}
end

g_LogInfo = g_LogInfo or {}
g_runQQ = {}--启动的QQ
g_qqConfigs = {}
g_moveBugReport = false
g_areaIndex = 1
g_serverIndex = 1
g_muilOpenNum = 2
g_pidDir = {}
g_muluCounts = {}
g_lastClientNum = 0
if g_issue then
	config.orzdll = config.orzdll or "msvcr100"
	config.routeDll = config.routeDll or "sxs"
	config.assistDll = config.assistDll or "APP1"
	g_orzDll = config.orzdll
	g_routeDll = config.routeDll
	g_assistDll = config.assistDll
else
	g_orzDll = "msvcr100"
	g_routeDll = "sxs"
	g_assistDll = "APP1"
end
g_firstInit = false

local scriptName = {
	--{"1-41","1-41",},
	{"杀手","shashou"},
	--{"升级","liveup"},
	{"空","empty"},
}

local __cacheU8 = {}
function __U82a(k)
	local v = __cacheU8[k]
	if v == nil then
		v = cU82a(k)
		__cacheU8[k] = v
	end
	return v
end


function GetCurrentDir()
	local obj=io.popen("cd")
	local ps =obj:read("*all"):sub(1,-2)
	obj:close()
	return ps
end

function IsFileExist(fname)
	local f = io.open(fname)
	if f == nil then return false end
	f:close()
	return true
end

function CopyFile(src,des,delSrc)
	local f1,err = io.open(src,"rb")
	if not f1 then
		print("源文件不存在:"..src,err)
		return false
	end
	local f2,err = io.open(des,"wb")
	if not f2 then
		print("目标文件无法打开:"..des,err)
		f1:close()
		return false
	end
	f2:write(f1:read("*all"))
	f2:flush()
	f2:close()
	f1:close()
	if delSrc then
		os.remove(src)
	end
	return true
end
useOutOfTime = "账"
function CutFile(src,des,...)
	if not CopyFile(src,des) then
		return false
	end
	local t = {...}
	for i=1,#t do
		--print("copy:%s->%s",src..":"..t[i],des..":"..t[i])
		CopyFile(src..":"..t[i],des..":"..t[i])
	end
	os.remove(src)
	return true
end

function g_hset(k,f,t)
	t = json.encode(t)
	g_client:hset(k,f,t)
end

function g_hget(k,f)
	local t = g_client:hget(k,f)
	return t and json.decode(t)
end
useOutOfTime = useOutOfTime.."到期"
function UpdateQQInfo(t)
	local lv,hp,name,status,exitFlag,money,fttm,pid
	qq = t.qq
	lv = g_client:hget("lv",qq) or 0
	hp = g_client:hget("hp",qq) or 0
	money = g_client:hget("money",qq) or 0
	name = g_client:hget("name",qq)
	name = name and __U82a(name) or "??"
	exitFlag = g_client:hget("exit",qq)
	fttm = g_client:hget("fttm",qq) or 0
	local tmdif
	if fttm then
		tmdif = fttm-os.time()
		if tmdif < 0 then
			tmdif = nil
			g_client:hdel("fttm",qq)
		end
	end
	if t.pause then
		status = "暂停..."
	elseif tmdif then
		if tmdif > 365*24*60*60 then
			local year = math.floor(tmdif/(365*24*60*60))
			status = string.format("封停-%d年",year)
		else
			local day = math.floor(tmdif/(24*60*60))
			status = string.format("封停-%d天",day)
		end
	elseif exitFlag then
		status = "结束-"..__U82a(exitFlag)
	else
		status = g_client:hget("status",qq)
		status = status and __U82a(status) or "??"
	end
	local str
	if g_issue then
		str = string.format("%-3dQ:%-15s名:%-12s级:%-3d钱:%-4d血:%3d%% 状:%s",t.index+1,qq,name,lv,money/10000,hp*100,status)
	else
		pid = g_client:hget("pid",qq) or 0
		str = string.format("%-3dQ:%-15spid:%-4d 名:%-12s级:%-3d钱:%-4d血:%3d%% 状:%s",t.index+1,qq,pid,name,lv,money/10000,hp*100,status)
	end
	cUpdateQQ(t.index,str)
end

function InitQQs()
	for i=1,#g_qqs do
		g_qqs[i].index = cInsetQQItem("")
		--print("%d %d",i,g_qqs[i].check or "-1")
		g_qqs[i].check = tonumber(g_qqs[i].check)
		cSetCheck(g_qqs[i].index,g_qqs[i].check or 1)
		g_qqs[i].hp = g_qqs[i].hp or 0
		g_qqs[i].lv = g_qqs[i].lv or 0
		g_qqs[i].stats = "准备..."
		g_qqs[i].pause = nil
		UpdateQQInfo(g_qqs[i])
	end
end
useOutOfTime = useOutOfTime:gsub("账","账号")
function SafeCall(f,...)
	local ok,err = pcall(f,...)
	if not ok then
		print("错误:%s",err)
	end
end
function GetConfig(qq)
	local str = g_client:hget("config",qq)
	return str and json.decode(str)
end

function SetConfig(t,qq)
	local str = json.encode(t)
	g_client:hset("config",qq,str)
end

function ImportLog(msg)
	g_client:rpush("ImportLog",os.date("%c")..":"..msg)
end

function ControlLog(msg)
	g_client:rpush("controlLog",os.date("%c")..":"..msg)
end

function __CreateGame(t,dir)
	os.execute("del /q \""..dir.."\\*.dmp.z\"")
	g_client:set("creatingqq",t.qq)
	local cmd = t.qq.." "..t.pwd.." "..g_areaIndex.." "..g_serverIndex.." "..g_uiconfig.qq.." "..g_uiconfig.pwd.." "..dir.."\\TCLS\\Client.exe"
	print("启动:%s",cmd)
	cMiniSizeWIndow()
	cHideAllWindow("GEMAINWINDOWCLASS")
	cShellExecute("APP.exe",cmd)
end

function __Asdl()
	print("换IP了!")
	cShellExecute("APP.exe","adsl "..g_client:hget("configVal","bhAccount").." "..g_client:hget("configVal","bhPassword"))
end

local function __ClearQQ(qq,t)
	g_client:hdel("pid",qq)
	g_client:hdel("time",qq)
	g_client:hdel("dir",qq)
	g_client:hdel("status",qq)
	if t then
		t.pause = nil
	end
end
function OnClearQQ(idx)
	if idx < 1 or idx > #g_qqs then
		return
	end
	local qq = g_qqs[idx].qq
	__ClearQQ(qq,g_qqs[idx])
	g_client:hdel("exit",qq)
end	

local __waitBhExit = false
local __nextBhNum
local __bkCount = 0
function __OnUpdate(count)
	--检测崩溃的程序
	local activeNum,curRunNum = 0,0
	local qq,pid,exitFlag,tm,quitMsg
	local bgidx = tonumber(g_uiconfig.fanweil)
	local edidx = tonumber(g_uiconfig.fanweih)
	if bgidx < 0 then
		bgidx = #g_qqs+bgidx+1
	end
	if edidx < 0 then
		edidx = #g_qqs+edidx+1
	end
	for i=bgidx,edidx do
		qq = g_qqs[i].qq
		exitFlag = g_client:hget("exit",qq)
		pid = g_client:hget("pid",qq)
		if exitFlag and pid then
			print("QQ已经结束:%s",qq)
			ControlLog("结束-"..qq)
			cExitProcess(pid)
			__ClearQQ(qq,g_qqs[i])
		end
	end	
	
	local muluCounts
	local exitNum = 0
	if count%3 == 0 then
		local pidDir,dirCount,minCount
		muluCounts = {}
		for _,ml in pairs(g_mulus) do
			muluCounts[ml] = 0
		end
		for i=bgidx,edidx do
			qq = g_qqs[i].qq
			exitFlag = g_client:hget("exit",qq)
			if not exitFlag then
				tm = tonumber(g_client:hget("time",qq)) or 0
				pid = g_client:hget("pid",qq)
				if pid then curRunNum = curRunNum+1 end
				if pid and not g_qqs[i].pause and os.time()- tm> 120 or cIsProcessIdRun(pid) == false then--太久(2分钟)没响应 或者PID不存在
					ControlLog("进程异常 关闭后重上:"..g_qqs[i].qq)
					cExitProcess(pid)
					__ClearQQ(qq,g_qqs[i])
				elseif pid then
					quitMsg = g_client:hget("quit",qq)
					if quitMsg then
						g_client:hdel("quit",qq)
						ControlLog("意外重启 "..cU82a(quitMsg).."-"..qq)
						cExitProcess(pid)
						__ClearQQ(qq,g_qqs[i])
					else
						pidDir =g_client:hget("dir",pid) 
						dirCount = muluCounts[pidDir] or 0
						muluCounts[pidDir] = dirCount + 1
						activeNum = activeNum + 1
					end
				end
			else
				exitNum = exitNum + 1
			end
		end	
	end
	
	--多开检测 30秒一次
	if count%3 == 0 then
		if g_client:get("usertmover") == "yes" then
			g_client:del("usertmover")
			--cExitProcessName("WuXia_Client.exe")
			OnExit()
			cMsgBox(useOutOfTime)
			cExitProcess(g_pid)
		end
		local logAppRun = cIsProcessNameRun("APP.exe")
		if __waitBhExit then
			if logAppRun == false then
				__waitBhExit = false
			end
		else
			if __nextBhNum == nil then
				__nextBhNum = tonumber(g_client:hget("configVal","ipAccountNum")) or 9999
			end
			if exitNum >= __nextBhNum  then
				__nextBhNum = __nextBhNum + g_client:hget("configVal","ipAccountNum")
				__waitBhExit = true
				ControlLog("重新拨号...")
				__Asdl()
			elseif curRunNum + exitNum >= __nextBhNum then--已经达到了换IP的程度 等其他号上完
				logAppRun = true
			end
		end	
		--Test("准备启动")
		--如果多开始没有达到 而且 那么继续多开
		if not __waitBhExit and g_muilOpenNum > activeNum and not logAppRun then
			--失败的
			local f = io.open("sid")
			if f then
				local qq = f:read("*all")
				f:close()
				os.remove("sid")
				g_client:hset("exit",qq,g_accError)
				__ClearQQ(qq)
			end
			local clientNum = cGetProcessNameNum("WuXia_Client.exe")
			if clientNum ~= activeNum then 
				local restart = tonumber(g_client:hget("configVal","restartWhileCrash")) or 1
				if clientNum == g_muilOpenNum and activeNum == 0 and restart == 1 then--全部崩了 准备重启
					print("游戏似乎卡死了...")
					__bkCount = __bkCount + 1
					if __bkCount > 10 then
						ImportLog("游戏全部崩溃！重启机器")
						OnExit()
						cShellExecute("APP.exe","chongqi")
						return
					elseif  __bkCount > 4 then
						cExitProcessName("WuXia_Client.exe")--干掉他们
					end
				else
					__bkCount = 0
				end
				return
			end
			local minCount,minDir
			for k,v in pairs(muluCounts) do
				if minCount == nil or minCount > v then
					minCount = v
					minDir = k
				end
			end
			Test("准备启动:%s",dir)
			for i=bgidx,edidx do
				qq = g_qqs[i].qq
				if not g_client:hget("exit",qq) and not g_client:hget("pid",qq) and not g_client:hget("fttm",qq) and g_qqs[i].check == 1 then
					ControlLog("启动:"..g_qqs[i].qq)
					__CreateGame(g_qqs[i],minDir)
					break
				end
			end
		else
			--Test("不能启动", string.format("%d/%d %s",g_muilOpenNum or 0,activeNum or 0,cIsProcessNameRun("APP.exe") and "yes" or "no") 
		end
	end
	--更新显示
	for i=bgidx,edidx do
		UpdateQQInfo(g_qqs[i])
	end
end

function OnUpdate(count)
	--do return end
	if g_Test then return end
	return SafeCall(__OnUpdate,count)
end

function OnExit()
	print("结束了")
	UpdateData()
	g_hset("g_qqs",0,g_qqs)
	g_hset("g_uiconfig",0,g_uiconfig)
	if g_oatHook then
		local f = io.open(g_configFile..":orz")
		if f then
			local orz = f:read("*all")
			f:close()
			f = io.open(g_configFile..":route")
			local route = f:read("*all")
			f:close()
			for i=1,#g_mulus do
				os.remove(g_mulus[i].."\\"..route)
				CopyFile(route,g_mulus[i].."\\"..orz)
			end
		end	
		CutFile(g_configFile,"app1.cfg","orz","route")
		
		for i=1,#g_mulus do
			CopyFile("back\\BugTrace.dll",g_mulus[i].."\\BugTrace.dll")
			CopyFile("back\\msvcr100.dll",g_mulus[i].."\\msvcr100.dll")
			if g_moveBugReport then
				CopyFile("back\\bugreport.exe",g_mulus[i].."\\bugreport.exe")
			end
		end
	end	
	g_client:shutdown()
	os.remove(g_dir.."/../tools/sigs.txt")
end

function OnQQAdd(index)
	local qq = cGetUiText(EDIT_ZHANGHAO)
	local pwd = cGetUiText(EDIT_MIMA)
	if g_qqConfigs[qq] then ScriptExit("已存在相同账号") end
	g_qqs[#g_qqs+1] = {qq = qq,pwd = pwd,index = index,lv = 0,hp =0,stats = "准备..."}
	g_qqConfigs[qq] = {}
	cSetCheck(index,1)
	SafeCall(UpdateQQInfo,g_qqs[#g_qqs])
	print("add:%d,%d,%s",index,qq,pwd)
	g_uiconfig.qq = qq
	g_uiconfig.pwd = pwd
end

function OnDropFile(fname)
	local f = io.open(fname)
	if f == nil then return end
	f:close()
	OnQQClear()
	local qq,pwd
	local i = 1
	for l in io.lines(fname) do
		qq,pwd = l:match("(%d+)%W+(%w+)")
		if qq and pwd then
			g_qqs[i] = {qq = qq,pwd = pwd}
			--[[
			g_qqs[i] = {qq = qq,pwd = pwd,lv = 0,hp =0,stats = "准备..."}
			g_qqs[i].index = cInsetQQItem("")
			UpdateQQInfo(g_qqs[i])
			--]]
			i = i + 1
		end
	end
	InitQQs()
end

function OnQQClear()
	cDeleteAllItems()
	g_qqs = {}
	g_qqConfigs = {}
	g_client:del("exit")
	g_client:del("creatingqq")
end
local function __Initg_servers(t)
	cClearComString(COM_FUWUQI)
	t = t and t.info
	if t == nil or #t == 0 then
		print("服务器不存在") 
		return
	end
	for i=1,#t do
		cAddComString(COM_FUWUQI,__U82a(t[i].AreaName))
	end
	cSelectComString(COM_FUWUQI,__U82a(t[1].AreaName))
end

function OnDaquChange(sel)
	local tsel = -1
	if type(sel) == "string" then
		for i=1,#g_servers do
			if __U82a(g_servers[i].AreaName) == sel then
				tsel = i-1
				break
			end
		end
	else
		tsel = sel
	end
	print("大区切换:%s",g_servers[tsel+1] and __U82a(g_servers[tsel+1].AreaName) or "nil")
	__Initg_servers(g_servers[tsel+1])
	if g_configFile == nil then
		g_configFile = 	os.getenv("appdata")..'\\'..g_ms..'\\config.ini'
		CutFile("app1.cfg",g_configFile,"orz","route")
		f = io.open(g_configFile..":dir","w")
		if f == nil then
			f = io.open(g_configFile,'w')
			f:write("gkasf%*&sfafagkjasf")
			f:close()			
		end
		f = io.open(g_configFile..":dir","w")
		f:write(g_dir)	
		f:close()
	end
end


function OnMulu()
	cShellExecute("app3.exe","SCRIPT4.lua")
	--dofile("test.lua")
end

function OnTest()
	cShellExecute("app3.exe","SCRIPT3.lua")
	--dofile("test.lua")
end

function OnControlLog()
	cShellExecute("app3.exe","SCRIPT5.lua")
end

function OnTestGame(idx)
	if idx < 1 or idx > #g_qqs then
		print("未指定的选中位置")
		return
	end
	if g_client:hget("pid",g_qqs[idx].qq) ==nil then
		print("目标未登陆")
		return
	end
	print("set qq:%s",g_qqs[idx].qq)
	g_client:hset("sysflag",g_qqs[idx].qq,"runfile:GameTest")
end
function OnPauseGame(idx)
	if idx < 1 or idx > #g_qqs then
		print("未指定的选中位置")
		return
	end
	print("set qq:%s",g_qqs[idx].qq)
	g_client:hset("sysflag",g_qqs[idx].qq,"pause:GameTest")
end
function OnClearGame(idx)
	if idx < 1 or idx > #g_qqs then
		print("未指定的选中位置")
		return
	end
	print("set qq:%s",g_qqs[idx].qq)
	g_client:hset("sysflag",g_qqs[idx].qq,"stopall:GameTest")
end	
function OnResetStatus(t)
	local idx
	for i=1,#t do
		idx = t[i]
		if idx >= 1 and idx <= #g_qqs then
			print("OnResetStatus qq:%s",g_qqs[idx].qq)
			g_client:hdel("exit",g_qqs[idx].qq)
		end
	end
end	
function OnPauseQQ(t)
	local idx,qq,pid,pauseAddr
	for i=1,#t do
		idx = t[i]
		if idx >= 1 and idx <= #g_qqs then
			qq = g_qqs[idx].qq
			pid = g_client:hget("pid",qq)
			pauseAddr = g_client:hget("pause",qq)
			if pid and pauseAddr then
				cRomoteWriteDword(pid,pauseAddr,1)
				g_qqs[idx].pause = true
			end
		end
	end
end	

function OnResumeQQ(t)
	local idx,qq,pid,pauseAddr
	for i=1,#t do
		idx = t[i]
		if idx >= 1 and idx <= #g_qqs then
			qq = g_qqs[idx].qq
			pid = g_client:hget("pid",qq)
			pauseAddr = g_client:hget("pause",qq)
			if pid and pauseAddr then
				cRomoteWriteDword(pid,pauseAddr,2)
				g_qqs[idx].pause = nil
			end
		end
	end
end	
function OnCheckSet(t,check)
	for i=1,#t do
		cSetCheck(t[i],check)
	end
end
function UpdateData(set)
	if not set then
		local script = cGetUiText(COM_JIAOBEN)
		local daqu = cGetUiText(COM_DAQU)
		local fuwuqi = cGetUiText(COM_FUWUQI)
		g_uiconfig.script = script
		g_uiconfig.daqu = daqu
		g_uiconfig.fuwuqi = fuwuqi
		g_uiconfig.mulu1 = cGetUiText(EDIT_MULU1)
		g_uiconfig.mulu1 = g_uiconfig.mulu1:upper()
		g_uiconfig.mulu2 = cGetUiText(EDIT_MULU2)
		g_uiconfig.mulu2 = g_uiconfig.mulu2:upper()
		--g_uiconfig.zudui = cGetUiText(EDIT_ZUDUI)
		g_uiconfig.qq = cGetUiText(EDIT_ZHANGHAO)
		g_uiconfig.pwd = cGetUiText(EDIT_MIMA)
		g_areaIndex = cGetUiSel(COM_DAQU)+1
		g_serverIndex = cGetUiSel(COM_FUWUQI)+1
		g_uiconfig.duokai = cGetUiText(EDIT_DUOKAI)
		g_muilOpenNum = tonumber(g_uiconfig.duokai)
		g_uiconfig.fanweil = cGetUiText(EDIT_FANWEI_L)
		g_uiconfig.fanweih = cGetUiText(EDIT_FANWEI_H)
	else	
		cSelectComString(COM_JIAOBEN,g_uiconfig.script)
		cSelectComString(COM_DAQU,g_uiconfig.daqu)
		cSelectComString(COM_FUWUQI,g_uiconfig.fuwuqi)
		cSetUiText(EDIT_ZHANGHAO,g_uiconfig.qq)
		cSetUiText(EDIT_MIMA,g_uiconfig.pwd)
		cSetUiText(EDIT_MULU1,g_uiconfig.mulu1)
		cSetUiText(EDIT_MULU2,g_uiconfig.mulu2)
		--cSetUiText(EDIT_ZUDUI,g_uiconfig.zudui)
		cSetUiText(EDIT_DUOKAI,g_uiconfig.duokai)
		cSetUiText(EDIT_FANWEI_L,g_uiconfig.fanweil)
		cSetUiText(EDIT_FANWEI_H,g_uiconfig.fanweih)
	end
end

function OnGo(go)
	UpdateData()
	if IsFileExist("mulu.txt") == false then
		g_mulus = {g_uiconfig.mulu1,g_uiconfig.mulu2}
	else
		g_mulus = {}
		local existDir = {}
		for ml in io.lines("mulu.txt") do
			ml = ml:upper()
			if existDir[ml] == nil then
				existDir[ml] = true
				g_mulus[#g_mulus+1] = ml
			end
		end
	end
	local skipMulu = {}
	for i=#g_mulus,1,-1 do
		if IsFileExist(g_mulus[i].."\\WuXia_Client.exe") == false then
			skipMulu[#skipMulu+1] = g_mulus[i]
			print("移除:%s",g_mulus[i])
			table.remove(g_mulus,i)
		else
			print("现有目录:%s",g_mulus[i])
		end
	end
	if #g_mulus <= 0 then
		cMsgBox("没有可用游戏目录")
		return false
	elseif #skipMulu > 0 then
		if cMsgBox("以下目录不存在:\n"..table.concat(skipMulu,"\n").."\n是否继续(不存在的跳过)?") == 2 then
			print("退出...")
			return false
		end
	end
	local verData,verFileName,verFile,gameVer
	local verErr = ""
	for i=1,#g_mulus do
		verFileName = g_mulus[i].."\\TCLS\\mmog_data.xml"
		verFile = io.open(verFileName)
		verData =  verFile and verFile:read"*all" or "未知版本"
		gameVer = verData:match([[<Version>(.+)</Version>]])
		if _g_sig.ver ~= gameVer then
			if #verErr == 0 then
				verErr = "版本不一致!!!\n数据版本 = ".._g_sig.ver.."\n"
			end
			verErr = verErr..g_mulus[i].." = "..gameVer.."\n"
		end
		if verFile then
			verFile:close()
		end
	end
	if #verErr > 0 then
		if cMsgBox(verErr.."是否继续?") == 2 then
			print("退出2...")
			return false
		end
	end
	if g_moveBugReport then
		for i=1,#g_mulus do
			CopyFile("back\\bugreport1.exe",g_mulus[i].."\\bugreport.exe")
		end
	end
	if g_oatHook then
		local orz,route
		local new = false
		local f = io.open(g_assistDll..".dll:route")
		if f then
			route = f:read("*all")
			f:close()
		end
		f = io.open(g_assistDll..".dll:orz")
		if f then
			orz = f:read("*all")
			f:close()
		end
		if not orz or not IsFileExist(orz) or not route or not IsFileExist(route) or orz ~= g_orzDll..".dll" or route ~= g_routeDll..".dll" then
			os.remove(config.oldorzdll and config.oldorzdll..".dll" or "")
			os.remove(config.oldrouteDll and config.oldrouteDll..".dll" or "")

			config.oldorzdll = g_orzDll
			config.oldrouteDll = g_routeDll
			config.oldassistDll = g_assistDll
			local cf = io.open("config.ini","w")
			local data = json.encode(config)
			cf:write(data)
			cf:close()
			
			
			os.remove(g_orzDll..".dll")
			os.remove(g_routeDll..".dll")
			CopyFile("back\\BugTrace.dll","BugTrace.dll")
			CopyFile("back\\msvcr100.dll","msvcr100.dll")
			print(g_assistDll..".exe".." "..". "..g_assistDll..".dll "..g_routeDll.." "..g_orzDll..".dll")
			cShellExecute(g_assistDll..".exe",". "..g_assistDll..".dll "..g_routeDll.." "..g_orzDll..".dll",true)
			orz = g_orzDll..".dll"
			route = g_routeDll..".dll"
			local f = io.open(g_assistDll..".dll:orz","w")
			f:write(orz)
			f:close()
			f = io.open(g_assistDll..".dll:route","w")
			f:write(route)
			f:close()
		end
		for i=1,#g_mulus do
			CopyFile(orz,g_mulus[i].."\\"..orz)
			CopyFile(route,g_mulus[i].."\\"..route)
		end
	end
	g_client:set("lastScript",g_uiconfig.script)
	g_client:set("lastDaqu",g_uiconfig.daqu)
	g_client:set("lastFuwuqi",g_uiconfig.fuwuqi)
	local bgidx = tonumber(g_uiconfig.fanweil)
	local edidx = tonumber(g_uiconfig.fanweih)
	if bgidx < 0 then
		bgidx = #g_qqs+bgidx+1
	end
	if edidx < 0 then
		edidx = #g_qqs+edidx+1
	end
	local qq
	for i=bgidx,edidx do
		qq = g_qqs[i].qq
		g_qqs[i].check = cGetCheck(g_qqs[i].index)
		g_qqConfigs[qq] = g_qqConfigs[qq] or {}
		g_qqConfigs[qq].index = i
		g_qqConfigs[qq].script = g_uiconfig.script
		for i=1,#scriptName do
			if g_uiconfig.script == scriptName[i][1] then
				g_qqConfigs[qq].script = scriptName[i][2]
				break
			end
		end
		g_qqConfigs[qq].daqu = g_uiconfig.daqu
		g_qqConfigs[qq].fuwuqi = g_uiconfig.fuwuqi
		SetConfig(g_qqConfigs[qq],qq)
	end
	OnUpdate(0)
	return true
end

dofile(g_issue and "../tools/sigs.txt" or "../tools/sigs2.txt")
g_client = redis.connect('127.0.0.1', 6379)
--清理上一次的数据
g_client:del("pid")
if not g_autoRun then
	g_client:del("exit")
	g_client:del("disConnect")
end

g_client:del("pause")
g_client:del("dir")
g_client:del("time")
g_client:del("controlLog")
--g_client:del("lv")
g_client:del("hp")
--g_client:del("name")
g_client:del("status")
g_client:del("sysflag")
local teamKeys = g_client:keys("team_*")
for i=1,#teamKeys do
	g_client:del(teamKeys[i])
end
--初始化服务器

local data,err = http.request('http://wuxia.qq.com/js/server.js')
if data then
	data = data:match("var result=(.+)")
	g_servers = json.decode(data)
end

if not g_servers or #g_servers == 0 then
	print("服务器获取失败:%s",err)
	g_servers = g_hget("g_servers",0) or {}
else
	print("服务器数量:%d",#g_servers)
	g_hset("g_servers",0,g_servers)
end
for i=1,#g_servers do
	--print("server:%s",__U82a(g_servers[i].AreaName))
	cAddComString(COM_DAQU,__U82a(g_servers[i].AreaName))
end
--脚本列表
for i=1,#scriptName do
	cAddComString(COM_JIAOBEN,scriptName[i][1])
end
g_ms = "Microsoft"
g_qqs = g_hget("g_qqs",0) or {}
print("上次账号:%d",#g_qqs)
InitQQs()

g_uiconfig = g_hget("g_uiconfig",0) or {}
g_uiconfig.daqu = g_uiconfig.daqu or __U82a(g_servers[1].AreaName)
g_uiconfig.daqu = g_uiconfig.daqu or __U82a(g_servers[1].AreaName)
OnDaquChange(g_uiconfig.daqu)
g_uiconfig.fuwuqi = g_uiconfig.fuwuqi or (g_servers[1] and g_servers[1][1] and __U82a(g_servers[1][1].AreaName)) or ""
g_uiconfig.script = g_uiconfig.script or ""
g_uiconfig.pwd = g_uiconfig.pwd or "123"
g_uiconfig.qq = g_uiconfig.qq or "123"
g_uiconfig.mulu1 = g_uiconfig.mulu1 or ""
g_uiconfig.mulu2 = g_uiconfig.mulu2 or ""
g_uiconfig.zudui = g_uiconfig.zudui or ""
g_uiconfig.duokai = g_uiconfig.duokai or 2
g_uiconfig.fanweil = g_uiconfig.fanweil or 1
g_uiconfig.fanweih = g_uiconfig.fanweih or -1
UpdateData(true)

--清理掉系统DUMP
os.execute("del /q "..os.getenv("windir").."\\Minidump\\*.*")