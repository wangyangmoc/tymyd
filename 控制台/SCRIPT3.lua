package.cpath = "?.dll;?51.dll;clibs/?51.dll;clibs/?.dll;"
require( "iuplua" )
require( "iupluacontrols")
local cmds = {...}
local redis = require"redis"
local g_client = redis.connect('127.0.0.1', 6379)
local allCounfigs = {
	{"identity",tonumber,"默认配置 %t\n默认身份: %l|杀手|",0},--猎户|
	{"pro",tonumber,"默认职业: %l|真武|天香|",0},
	{"menhuiName",tonumber,"默认盟会: %l|系统推荐|帝王州|寒江城|水龙吟|万里杀|",0},
	--{"anshaCount",tonumber,"暗杀配置(星级奖励为41-60阶段) %t\n暗杀次数: %i[1,15]",12},
	--{"anshaReward",tonumber,"暗杀最低奖励(41级): %i[40000,60000]",60000},
	{"shengsiQianMoney",tonumber,"生死签最高价: %i[0,60000]",40000},
	{"anshaPinlv",tonumber,"暗杀榜刷新频率(秒): %r[0.2,3.0,0.1]",1.0},
	{"lockLv41",tonumber,"41后不升级: %b",1},
	{"invertPage",tonumber,"倒序翻页(0为不倒序): %i[0,250]",250},
	{"anshaStar1",tonumber,"★(4金): %b[不接,接取]",1},
	{"anshaStar2",tonumber,"★★(5金): %b[不接,接取]",1},
	{"anshaStar3",tonumber,"★★★(6金): %b[不接,接取]",1},
	{"anshaStar4",tonumber,"★★★★(5纯银): %b[不接,接取]",0},
	{"anshaStar5",tonumber,"★★★★★(10纯银): %b[不接,接取]",0},
	{"dailyLiveNess",tonumber,"每日配置 %t\n每日活跃: %b",1},
	{"buyDailyGrift",tonumber,"会员每日礼包: %b",1},
	{"pointDaily",tonumber,"买减负令做日常: %b",1},
	{"ipAccountNum",tonumber,"拨号和重启 %t\n单IP上号数量: %i",1000},
	{"bhAccount",tostring,"账号: %s",""},
	{"bhPassword",tostring,"密码: %s",""},
	{"restartWhileCrash",tostring,"游戏进程卡死后重启机器: %b",1},
	{"beVipAfter41",tonumber,"其他配置 %t\n自动加入VIP: %b",1},
	{"teamNum",tonumber,"组队人数: %i[1,5]",5},
	--{"ipAccountNum",tonumber,"完成指定账号数量后重拨: %i",1000},
}

local function __SCRIPT3()
	local t,k,v
	local ts = {}
	local descStr = ""
	local vv
	for i=1,#allCounfigs do
		t = allCounfigs[i]
		k = t[1]
		vv = g_client:hget("configVal",k)
		ts[i] = vv and t[2](vv) or t[4]
		descStr = descStr..t[3]
		if t[3] ~= "" then descStr = descStr.."\n" end
	end
	--iup.Message("错误", descStr or "nil")
	--iup.Message("错误", table.concat(ts," \n") or "nil")
	local nt = {iup.GetParam("配置", nil,descStr,unpack(ts))}
	if (not nt[1]) then
		return
	end
	assert(#nt == #allCounfigs+1,"数量错误")
	for i=1,#allCounfigs do
		t = allCounfigs[i]
		k = t[1]
		g_client:hset("configVal",k,nt[i+1])
	end
end
local cfgkeys = g_client:keys("defaultConfig:*")
local cfgkey
local newkeys = {}
for i=1,#cfgkeys do
	cfgkey = cfgkeys[i]:match("defaultConfig:(.+)")
	g_client:hset("configVal",cfgkey,g_client:get(cfgkeys[i]))
	g_client:del(cfgkeys[i])
end

local ok,err = pcall(__SCRIPT3)
if not ok then
	iup.Message("错误", err or "nil")
end