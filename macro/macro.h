#ifndef _MACRO_H_
#define _MACRO_H_
#include <functional>
#include <exception>
#include <process.h>
//资源管理
namespace macro
{
	class CAutoSection
	{
	public:
		CAutoSection(CRITICAL_SECTION &cs) :m_csLock(cs){ EnterCriticalSection(&m_csLock); }
		virtual ~CAutoSection(void){ LeaveCriticalSection(&m_csLock); }
	private:
		CRITICAL_SECTION &m_csLock;
	};
	class CAutoConsole
	{
	public:
		CAutoConsole(){
			AllocConsole();
			freopen("CONIN$", "r+t", stdin);
			freopen("CONOUT$", "w+t", stdout);
		}
		~CAutoConsole(){
			fclose(stdin);
			fclose(stdout);
			FreeConsole();
		}

	private:

	};

	class ScopeGuard
	{
	public:
		explicit ScopeGuard(std::function<void()> onExitScope, bool dismissed = false)
			: onExitScope_(onExitScope), dismissed_(dismissed)
		{ }
		ScopeGuard(){ dismissed_ = true; }
		ScopeGuard(ScopeGuard &SG)
		{
			onExitScope_ = SG.onExitScope_;
			dismissed_ = SG.dismissed_;
		}

		~ScopeGuard()
		{
			if (!dismissed_)
			{
				onExitScope_();
			}
		}

		void Replace(std::function<void()> onExitScope)
		{
			onExitScope_ = onExitScope;
		}

		void Dismiss(bool dismissed = true)
		{
			dismissed_ = dismissed;
		}

	private:
		std::function<void()> onExitScope_;
		bool dismissed_;

	private: // noncopyable
		//ScopeGuard(ScopeGuard const&);
		//ScopeGuard& operator=(ScopeGuard const&);
	};

#define LINENAME_CAT(name, line) name##line
#define LINENAME(name, line) LINENAME_CAT(name, line)
#define NOP(nop_size) BYTE LINENAME(nop,__LINE__)[nop_size]
#define MUTEXLOCK(X) CAutoMutex LINENAME(amt,__LINE__)(X)
#define ON_SCOPE_EXIT(callback) ScopeGuard LINENAME(EXIT, __LINE__)(callback)
#define SCOPE_EXIT(callback) ScopeGuard LINENAME(EXIT, __LINE__)([&](){callback;})
#define SCOPE_EXIT_CHANGE(callback) g_sgCall.Replace([&](){callback})
#define COPY_SCOPE_EXIT(SG) ScopeGuard LINENAME(EXIT, __LINE__)(SG)
#define COPY_SCOPE_EXIT_AND_UNDIS(SG) ScopeGuard LINENAME(EXIT, __LINE__)(SG);\
	LINENAME(EXIT, __LINE__).Dismiss(false)
}

//调试输出
namespace macro
{

#define ASSERT_AND_RETURN(expr,rt) if(expr);\
									else {return rt;}
};

#endif