// testWin32.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

#include <vector>
#include <process.h>
#include <Windows.h>
#include <cstdio>
#include <map>
#include <string>
#include <list>
using namespace std;
#include "../memoryModule/MemoryModule.h"
typedef BOOL(*_DllInit)();
void fun(int p[10]){
	printf("%p", p);
}
class testInit
{
public:
	testInit() = default;
	testInit(const testInit& bb) = delete;
protected:
private:
	int a;
	string b;
	float c;
	
};
int _tmain(int argc, _TCHAR* argv[])
{
	testInit a;
	system("pause");
	return 0;
}

/*
00401052    8A85 F8FEFFFF   mov al,byte ptr ss:[ebp-0x108]
00401058    83C4 04         add esp,0x4
0040105B    8977 68         mov dword ptr ds:[edi+0x68],esi
0040105E    84C0            test al,al
00401060    74 20           je short WuXia_Cl.00401082
00401062    33C9            xor ecx,ecx
00401064    8D95 F8FEFFFF   lea edx,dword ptr ss:[ebp-0x108]
0040106A    8D9B 00000000   lea ebx,dword ptr ds:[ebx]
00401070    6BC9 43         imul ecx,ecx,0x43
00401073    0FBEC0          movsx eax,al
00401076    42              inc edx
00401077    03C8            add ecx,eax
00401079    8A02            mov al,byte ptr ds:[edx]
0040107B    84C0            test al,al

*/