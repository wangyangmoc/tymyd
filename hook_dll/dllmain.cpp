// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include <Psapi.h>
#include <stdio.h>
#include "../mhook-lib/mhook.h"
typedef int(*_luaL_loadfilex)(void *L, const char *filename,
	const char *mode);
typedef int(*_luaL_loadbufferx)(void *L, const char *buff, size_t size,
	const char *name, const char *mode);
typedef void* (*_luaL_newstate)();
typedef void* (*_lua_newstate)(void *, void *);


_luaL_newstate luaL_newstate;
_lua_newstate lua_newstate;
_luaL_loadfilex luaL_loadfilex;
_luaL_loadbufferx luaL_loadbufferx;

void* myluaL_newstate()
{
	auto ret = luaL_newstate();
	char msg[100] = { 0 };
	sprintf(msg, "++++++++++++++lua_newstate:0x%x", (DWORD)ret);
	OutputDebugStringA(msg);
	return ret;
}
void* mylua_newstate(void *v1, void *v2)
{
	auto ret = lua_newstate(v1, v2);
	char msg[100] = { 0 };
	sprintf(msg, "++++++++++++++lua_newstate:0x%x", (DWORD)ret);
	OutputDebugStringA(msg);
	return ret;
}

int myluaL_loadfilex(void *L, const char *filename,
	const char *mode){
	OutputDebugStringA("++++++++++++++++++++++++++++");
	OutputDebugStringA(filename);
	return luaL_loadfilex(L, filename,mode);
}

int myluaL_loadbufferx(void *L, const char *buff, size_t size,
	const char *name, const char *mode){
	OutputDebugStringA("--------------------");
	OutputDebugStringA(name);
	return luaL_loadbufferx(L, buff, size, name, mode);
}
//
//BOOL
//__stdcall
//CreateProcessInternalW(HANDLE hToken,
//LPCWSTR lpApplicationName,
//LPWSTR lpCommandLine,
//LPSECURITY_ATTRIBUTES lpProcessAttributes,
//LPSECURITY_ATTRIBUTES lpThreadAttributes,
//BOOL bInheritHandles,
//DWORD dwCreationFlags,
//LPVOID lpEnvironment,
//LPCWSTR lpCurrentDirectory,
//LPSTARTUPINFOW lpStartupInfo,
//LPPROCESS_INFORMATION lpProcessInformation,
//PHANDLE hNewToken);
//
//auto g_CreateProcessInternalW = CreateProcessInternalW;
//BOOL
//WINAPI
//CreateProcessInternalW_Hook(HANDLE hToken,
//LPCWSTR lpApplicationName,
//LPWSTR lpCommandLine,
//LPSECURITY_ATTRIBUTES lpProcessAttributes,
//LPSECURITY_ATTRIBUTES lpThreadAttributes,
//BOOL bInheritHandles,
//DWORD dwCreationFlags,
//LPVOID lpEnvironment,
//LPCWSTR lpCurrentDirectory,
//LPSTARTUPINFOW lpStartupInfo,
//LPPROCESS_INFORMATION lpProcessInformation,
//PHANDLE hNewToken)
//{
//	BOOL RetVal = CreateProcessInternalW(
//		hToken,
//		lpApplicationName,
//		lpCommandLine,
//		lpProcessAttributes,
//		lpThreadAttributes,
//		bInheritHandles,
//		dwCreationFlags,
//		lpEnvironment,
//		lpCurrentDirectory,
//		lpStartupInfo,
//		lpProcessInformation,
//		hNewToken);
//
//	if (RetVal == FALSE)
//		return RetVal;
//	PRTW(L"CreateProcess:%s %s", lpApplicationName == NULL ? L"nil" : lpApplicationName, lpCommandLine == NULL ? L"nil" : lpCommandLine);
//	return RetVal;
//}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		MessageBoxA(NULL, "", "测试一下", MB_OK);
		//Mhook_SetHook((PVOID*)&g_CreateProcessInternalW, CreateProcessInternalW_Hook);
		break;
		char dir[MAX_PATH] = { 0 };
		
		GetModuleFileNameA(NULL, dir, MAX_PATH);
		OutputDebugStringA(dir);
		_strupr(dir);
		if (strstr(dir, "WUXIA_CLIENT.EXE") == NULL)
			return TRUE;
		char msg[100] = { 0 };
		OutputDebugStringA("now hookbegin++++++++++++++++++++++++++++++++++++++++");
		HMODULE  hFind = GetModuleHandleA("WuXia_ClientBase.dll");
		MODULEINFO mi;
		memset(&mi, 0, sizeof(MODULEINFO));
		GetModuleInformation(GetCurrentProcess(), hFind, &mi, sizeof(mi));
		sprintf(msg, "WuXia_ClientBase:0x%x", hFind);
		OutputDebugStringA(msg);
		luaL_loadfilex = (_luaL_loadfilex)(DWORD(hFind) + 0x0f70c000 - 0x0f640000);
		sprintf(msg, "luaL_loadfilex:0x%x", luaL_loadfilex);
		OutputDebugStringA(msg);
		luaL_loadbufferx = (_luaL_loadbufferx)(DWORD(hFind) + 0x0F70C2A0 - 0x0f640000);
		sprintf(msg, "luaL_loadbufferx:0x%x", luaL_loadbufferx);
		OutputDebugStringA(msg);

		luaL_newstate = (_luaL_newstate)(DWORD(hFind) + 0x0F44CA00 - 0x0f380000);
		sprintf(msg, "luaL_newstate:0x%x", luaL_newstate);
		OutputDebugStringA(msg);

		lua_newstate = (_lua_newstate)(DWORD(hFind) + 0x0F44D060 - 0x0f380000);
		sprintf(msg, "lua_newstate:0x%x", luaL_loadbufferx);
		OutputDebugStringA(msg);

		Mhook_SetHook((PVOID*)&luaL_loadfilex, myluaL_loadfilex);
		Mhook_SetHook((PVOID*)&luaL_loadbufferx, myluaL_loadbufferx);
		Mhook_SetHook((PVOID*)&luaL_newstate, myluaL_newstate);
		Mhook_SetHook((PVOID*)&lua_newstate, mylua_newstate);
		OutputDebugStringA("now hookend-----------------------------------------");
		break;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

