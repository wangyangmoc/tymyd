// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"
#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件: 
#include <windows.h>



// TODO:  在此处引用程序需要的其他头文件
#include <stdlib.h>

#ifndef _ISSUE
#define PRT(fmt,...) {char msg##__LINE__[4096]; sprintf_s(msg##__LINE__,"msk_td->"fmt,__VA_ARGS__); OutputDebugStringA(msg##__LINE__);}
#define PRTW(fmt,...) {wchar_t msg##__LINE__[4096]; swprintf_s(msg##__LINE__,L"msk_td->"fmt,__VA_ARGS__); OutputDebugStringW(msg##__LINE__);}
#else
#define PRT(fmt,...)
#define PRTW(fmt,...)
#endif